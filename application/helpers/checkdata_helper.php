<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**------------------------------------------------------------------------------------------------
 * @author SYZ <yjcin13@kinx.net>
 * 파라메터 데이터 확인 helper
--------------------------------------------------------------------------------------------------*/


/**
 * 요청 데이터에서 숫자형 데이터 확인
 */
if ( ! function_exists('chk_param_numeric'))
{
    function chk_param_numeric($chk_data)
    {
        $chktype = "(^[+-]?[\d,]*(\.?\d*)$)";
        return (empty($chk_data) || !preg_match($chktype, $chk_data)) ? FALSE : $chk_data;
    }
}



/**
 * 요청 데이터에서 숫자+underbar 형식 데이터 확인
 */
if ( ! function_exists('chk_param_numunderbar'))
{
    function chk_param_numunderbar($chk_data)
    {
        $chktype = "(^(\d)*(\_?\d)*(\d)$)";
        return (empty($chk_data) || !preg_match($chktype, $chk_data)) ? FALSE : $chk_data;
    }
}




/**
 * 요청 데이터에서 Email형 데이터 확인
 *
 * @param array $chk_email : Email 확인 필드명
 * @param array $request_data : 요청 받은 데이터. 1차 배열형태
 *
 * @return array $request_data
 */
if ( ! function_exists('chk_param_email'))
{
    function chk_param_email($chk_data)
    {
        $chktype = "/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i"; // Email 타입확인
        return (empty($chk_data) || !preg_match($chktype, $chk_data)) ? FALSE : $chk_data;
    }
}


/* End of file checkdata_helper.php */
/* Location: ./application/helpers/checkdata_helper.php */