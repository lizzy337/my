<?php
/**
 * @author BahamuT
 * @version 1.0.0
 * @license copyright by GABIA_BahamuT
 * @since 11. 5. 3 오후 4:10 ~
 */
function rm_rf($file){
	if (file_exists($file)) {
		@chmod($file,0777);
		if (is_dir($file)) {
			$handle = opendir($file);
			while($filename = readdir($handle)) {
				if ($filename != "." && $filename != "..")
					rm_rf($file."/".$filename);
			}
			closedir($handle);
			rmdir($file);
		} else {
			unlink($file);
		}
	}
}


function mkdir_tree($path,$chmod=0707){
	$tempArray = array();
	if(!is_dir($path)){

		$tempArray = explode("/",$path);
		$cnt = count($tempArray);

		$dirName = ".";
		for($i=1;$i<$cnt;$i++){
			$dirName .= "/".$tempArray[$i];
			@mkdir($dirName, $chmod);
			@chmod($dirName, $chmod);
		}

	}else{
		return TRUE;
	}
}

function cutstr($str, $len, $suffix="…") {
	$s = substr($str, 0, $len);
	$cnt = 0;
	for ($i=0; $i<strlen($s); $i++) {
		if (ord($s[$i]) > 127) {
			$cnt++;
		}
	}
	$s = substr($s, 0, $len - ($cnt % 3));
	if (strlen($s) >= strlen($str)) $suffix = "";
	return $s . $suffix;
}

function get_curl($url, $params=null) {

	$fields_string = "";
	if(is_array($params)) {
		foreach($params as $key=>$value) { $fields_string .= $key.'='.urlencode($value).'&'; }
		rtrim($fields_string, '&');

		$fields_string = "?".$fields_string;
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
	curl_setopt($ch, CURLOPT_URL, $url.$fields_string);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
}

function post_curl($url, $params){
	//url-ify the data for the POST
	$fields_string = "";
	foreach($params as $key=>$value) { $fields_string .= $key.'='.urlencode($value).'&'; }
	rtrim($fields_string, '&');
	$result = "";

	//open connection
	$ch = curl_init();

	//set the url, number of POST vars, POST data
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_POST, count($params));
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

	//execute post
	$result = curl_exec($ch);

	//close connection
	curl_close($ch);
	return $result;
}

function get_unexpired_term($targetDate) {
	$tMktime = strtotime($targetDate);
	$nMktime = time();
	return floor(($tMktime - $nMktime) / 86400);
}


/**
 * ENCRYPTIT
 *
 * 암호화 함수
 * @param string $str		//암호화할 내용
 * @param string $key		//고정값
 * @return string
 */
if ( !function_exists('encryptIt'))
{
	function encryptIt($string, $key="2013kinxintranet!@#$")
	{
		$returnString = "";
		$charsArray = str_split("e7NjchMCEGgTpsx3mKXbVPiAqn8DLzWo_6.tvwJQ-R0OUrSak954fd2FYyuH~1lIBZ");
		$charsLength = count($charsArray);
		$stringArray = str_split($string);
		$keyArray = str_split(hash('sha256',$key));
		$randomKeyArray = array();
		while(count($randomKeyArray) < $charsLength){
			$randomKeyArray[] = $charsArray[random_int(0, $charsLength-1)];
		}
		for ($a = 0; $a < count($stringArray); $a++){
			$numeric = ord($stringArray[$a]) + ord($randomKeyArray[$a%$charsLength]);
			$returnString .= $charsArray[floor($numeric/$charsLength)];
			$returnString .= $charsArray[$numeric%$charsLength];
		}
		$randomKeyEnc = '';
		for ($a = 0; $a < $charsLength; $a++){
			$numeric = ord($randomKeyArray[$a]) + ord($keyArray[$a%count($keyArray)]);
			$randomKeyEnc .= $charsArray[floor($numeric/$charsLength)];
			$randomKeyEnc .= $charsArray[$numeric%$charsLength];
		}
		return $randomKeyEnc.hash('sha256',$string).$returnString;
	}
}

/**
 * DECRYPTIT
 *
 * 복호화 함수
 * @param string $str	//복호화할 내용
 * @param string $key 		//고정값
 * @return string
 */
if ( !function_exists('decryptIt'))
{
	function decryptIt($string, $key="2013kinxintranet!@#$")
	{

		$returnString = "";
		$charsArray = str_split("e7NjchMCEGgTpsx3mKXbVPiAqn8DLzWo_6.tvwJQ-R0OUrSak954fd2FYyuH~1lIBZ");
		$charsLength = count($charsArray);
		$keyArray = str_split(hash('sha256',$key));
		$stringArray = str_split(substr($string,($charsLength*2)+64));
		$sha256 = substr($string,($charsLength*2),64);
		$randomKeyArray = str_split(substr($string,0,$charsLength*2));
		$randomKeyDec = array();
		if(count($randomKeyArray) < 132) return false;
		for ($a = 0; $a < $charsLength*2; $a+=2){
			$numeric = array_search($randomKeyArray[$a],$charsArray) * $charsLength;
			$numeric += array_search($randomKeyArray[$a+1],$charsArray);
			$numeric -= ord($keyArray[floor($a/2)%count($keyArray)]);
			$randomKeyDec[] = chr($numeric);
		}
		for ($a = 0; $a < count($stringArray); $a+=2){
			$numeric = array_search($stringArray[$a],$charsArray) * $charsLength;
			$numeric += array_search($stringArray[$a+1],$charsArray);
			$numeric -= ord($randomKeyDec[floor($a/2)%$charsLength]);
			$returnString .= chr($numeric);
		}
		if(hash('sha256',$returnString) != $sha256){
			return false;
		}else{
			return $returnString;
		}

	}
}


/**
 * input: 원금, 부가세적용(국내) 해외는 미적용
 * output: 부가세포함가, 부가세 (배열)
 * 부가세 절상,절사에 관한 내용
 *	소수점 2자리에서 반올림한 값을 저장하고, 1.1 곱할때는 소수점 이하 버림
 *	1. 원금 : 350205
 *	2. 세전가 : 318369(318368.19->318368.2) - 절상(소수점 2자리 반올림)
 *	3. 부가세 :  31836 - 버림
 *	세전가: 원금 * 1.1 -> 소수점 2자리 반올림
 *	부가세: 세전가 /10 -> 버림
 * @access	public
 * @return	string
 */
if ( ! function_exists('vat'))
{
	function vat($int, $flag=TRUE, $viewflag = FALSE, $nocommer=FALSE)
	{
		$val 	= array();
		switch($flag){
			case FALSE:
				$ptax = 1;
				break;
			case TRUE:
			default:
				$ptax = 1.1;
		}
		if($viewflag)
		{
			$vatprice = floor(round($int*$ptax,2));
            $val = ($nocommer) ? $vatprice : number_format($vatprice);//포함가
		}
		else
		{
			$val[0] = round($int*$ptax,2);//포함가
			$val[1] = floor($int/10);//vat
		}
		return $val;
	}
}


/**
 * 전화번호 ('-' 구분) 형식으로 format 변경
 *
 * @param string		$string		변경할 전화번호 string
 * @param boolean	$hidden		뒤자리 숨김여부
 * @param boolean	$stringflag	문자열로 반환여부, 아니면 array
 * @return NULL|string|array
 */
if ( ! function_exists('format_phone'))
{
	function format_phone($string, $hidden=FALSE, $stringflag=TRUE)
	{
		if($string==null || $string=='' || strlen($string) <8 ) return $string;
		elseif(strlen($string) >11 ) return $string;

		$string = str_replace('-','',$string);

		switch (strlen($string)){
			case 8:
				$num1 = substr($string,0,4);
				$num2 = substr($string,4);
				$num3 = '';
				break;
			case 9:
				$num1 = substr($string,0,2);
				$num2 = substr($string,2,3);
				$num3 = substr($string,5,4);
				break;
			case 10:
				if(substr($string,0,2)=="02"){
					$num1 = substr($string,0,2);
					$num2 = substr($string,2,4);
					$num3 = substr($string,6,4);
				}
				else{
					$num1 = substr($string,0,3);
					$num2 = substr($string,3,3);
					$num3 = substr($string,6,4);
				}
				break;
			default:
				if(substr($string,0,2)=='82') $string= '0'.substr($string,2);
				$num1 = substr($string,0,3);
				$num2 = substr($string,3,4);
				$num3 = substr($string,7,4);
				break;
		}

		if($hidden) $num3 ='****';

		if($stringflag) return ($num3<>'') ? $num1."-".$num2."-".$num3 : $num1."-".$num2;
		else return array($num1, $num2, $num3);
	}
}



/**
 * XSS quot 처리
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('chg_xss_quot'))
{
	 function chg_xss_quot($string)
	{
		$string = str_replace('"', '&quot;', $string);
		$string = str_replace("'", "&#039;", $string);
		return $string;
	}
}




/**
 * 자바스크립트 alert 창 출력
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('jsError'))
{
	function jsError($msg, $url=-1) {
		echo "<HTML>\n";
		echo '<meta charset="utf-8" />';
		echo "<META HTTP-EQUIV=Pragma CONTENT=no-cache>\n";

		$msg = str_replace("'", "\"", trim($msg));
		echo "<script>alert('$msg');</script>\n";


		if(is_int($url)) echo "<script>history.go($url);</script>\n";
		else echo "<script>location.replace('$url');</script>\n";

		echo "</HTML>\n";
		exit;
	}
}

/**
 * 자바스크립트 confirm창 출력
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('jsConfirm'))
{
	function jsConfirm($msg, $url=-1) {
		echo "<HTML>\n";
		echo '<meta charset="utf-8" />';
		echo "<META HTTP-EQUIV=Pragma CONTENT=no-cache>\n";

		$html= "
				<script>
				if (confirm('".$msg."')){
				  location.replace('".$url."');
				}else{
				  history.back(-1);
				}
				</script>
		";
		echo $html;
		echo "</HTML>\n";
		exit;
	}
}


/**
 * MOVE_FILE
 * 템플파일을 실제운영 디렉토리로 이동
 *
 * @param
 * @return 업로드 경로.
 */
if ( !function_exists('move_file'))
{
	function move_file($fileinfo, $save_filepath)
	{
		if(!is_null($fileinfo) && !empty($fileinfo))
		{
			# 템프파일
			$temp_file = UPLOAD_TYPE_TMP.$fileinfo['file_name'];

			# 하위폴더(날짜별 폴더) 생성
			if(!is_dir($save_filepath))
			{
				umask(0);
				if (!mkdir($save_filepath, 0777)) {
					throw new Exception('폴더 생성 중 오류가 발생하였습니다.<br/>[디렉토리 접근권한 없음 '.$save_filepath.']');
					return FALSE;
				}
			}

			# 실저장 경로와 파일명 조합.
			$save_file = $save_filepath.$fileinfo['file_name'];

			# 파일 이동및 템프 파일 삭제
			if ( copy ($temp_file, $save_file) ){
				unlink($temp_file);
				return TRUE;
			}
		}
		return FALSE;

	}
}



/**
 * chgPnumber
 * 사업자 번호 복호화 및 주민번호 암호설정
 * @param string $number 암호 문자열
 * @param booleam $dp   주민번호 뒷자리 노출여부
 * @param string $work   작업구분(d:decode, e:encode)
 */
if ( !function_exists('chgPnumber'))
{
	function chgPnumber($str, $dp=FALSE, $work = "d")
	{
		$CI =& get_instance();
		$CI->load->library('crypto');

		if($work=='d')
		{
			$dstr = $CI->crypto->decrypt($str);
			if(chkPersonalNumber($dstr)) // 개인증번호가 주민번호 일 경우

			{
				$dstr = ($dp) ? trim($dstr) : substr($dstr, 0,6)."-*******";
			}
			else // 개인증번호가 주민번호 아닐 경우 일단 여권번호
			{
				$dstr = ($dp) ? trim($dstr) : substr($dstr, 0,4)."*****";
			}
		}
		elseif($work=='e')
		{
			$str=trim(str_replace("-","",$str));

			if(chkPersonalNumber($str)) // 개인증번호가 주민번호 일 경우
			{
				$str= substr($str,0,6)."-".substr($str,-7);
			}

			$dstr = $CI->crypto->encrypt($str);
		}

		return $dstr;
	}
}

/**
 * 주민번호 여부 확인(검색용으로 활용)
 * @param string $str
 */
if ( !function_exists('chkPersonalNumber'))
{
	function chkPersonalNumber($str)
	{
		$str=trim($str);
		return preg_match("/^(?:[0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12][0-9]|3[01]))([_.-]?)[1-4][0-9]{6}$/", $str);
	}
}


/**
 * post로 접속되었는지 확인한다.
 *
 * @access	public
 * @return	boolean
 */
if ( ! function_exists('isPost'))
{
	function isPost()
	{
		return (strtolower($_SERVER['REQUEST_METHOD']) === "post") ? TRUE : FALSE;
	}
}

/**
 * FILTER_KEYS
 *
 * include_keys에 있는 배열 항목만으로 배열을 구한다.
 * @param array $params         //전달된 배열
 * @param array $include_keys	 //테이블의 필드 배열
 * @param array $flag				 //빈값제외여부
 * @return array
 */
if ( !function_exists('filter_keys'))
{
	function filter_keys($params=array(), $include_keys=array(), $flag=TRUE) {
		$new_arr = array();
		//$filter_params = array_filter($params);
		$filter_params=$params;
		foreach ($filter_params as $key => $val) {
			if($flag && (is_null($val) || $val==''))unset($filter_params[$key]);
			elseif($key != 'mode') {
				if ( in_array($key, $include_keys) ) {
					$new_arr[$key] = $params[$key];
				}
			}
		}
		return $new_arr;
	}
}

/**
 * 변수값(array)을 보여준다.
 * - ENVIRONMENT값이 development 경우만 실행
 *
 * @access	public
 * @return	void
 */
if ( ! function_exists('va'))
{
    function va($array, $flag=FALSE)
    {

        if(ENVIRONMENT=='development' || ENVIRONMENT=='migration'){
            echo "<pre>";
            print_r($array);
            echo "</pre>";
            echo PHP_EOL;

            if($flag) die;
        }
    }
}
if ( ! function_exists('vad'))
{
    function vad($array)
    {

        if(ENVIRONMENT=='development' || ENVIRONMENT=='migration'){
            echo "<pre>";
            print_r($array);
            echo "</pre>";
            echo PHP_EOL;
            die;
        }
    }
}




/**
 * seed
 * 암,복호화
 * @param string $number 암호 문자열
 * @param string $work   작업구분(d:decode, e:encode)
 */
if ( !function_exists('seed'))
{
    function seed($str, $encode = TRUE)
    {
        $CI =& get_instance();
        $CI->load->library('crypto');

        return ($encode) ? $CI->crypto->encrypt($str) : $CI->crypto->decrypt($str);
    }
}


if ( !function_exists('number_format_charge'))
{
	function number_format_charge($charge, $curr) {
		if(($curr=='USD'))
		{
			if($charge<0){
				$charge = abs($charge);
				return '-$'.number_format((float)$charge, 2);
			}
			else{
				return '$'.number_format((float)$charge, 2);
			}
		}
		elseif(is_float($charge))
		{
			return number_format((float)$charge,2).'원';
		}
		else
		{
			return number_format((int)$charge).'원';
		}
	}
}

//금액, 환율 => 금액 comma 환율단위
if ( ! function_exists('get_currency_price'))
{
	function get_currency_price($nPrice, $sCurrency)
	{
            //va($nPrice); va($sCurrency);
            $Price = "0";
            $Currency = "원";
            if($sCurrency=="KRW")
            {
                $Price = number_format($nPrice,0);
                $Currency = "원";
            }
            else
            {
                $Price = number_format($nPrice,2);
                $Currency = "$";
            }

            $rtn['sPrice'] = $Price;
            $rtn['sCurrency'] = $Currency;
            return $rtn;

	}
}


/**
 * json type의 array field 값을 배열형식으로 전환
 * @param string $json          // 배열전환할 json
 * @param string $itmename      // 특정 item 값만 확인할 경우 해당 item명
 */
if ( !function_exists('chg_arrayfield'))
{
	function chg_arrayfield($json, $itmename = NULL)
	{
		$result = json_decode($json, TRUE);
		if(!is_null($itmename))
		{
			if(isset($result[$itmename]))
			{
				return $result[$itmename];
			}
			else { return NULL; }
		}

		return $result;
	}
}


/**
 * 과금유형코드를 한글명으로 변환
 * @param string $billtype         //과금유형코드
  * @return string
 */
if ( !function_exists('get_billtype_name'))
{
    function get_billtype_name($billtype) {
        $new_arr = array(
            'onetime'   => '1회성',
            'sale'      => '판매',
            'lease'     => '임대/서비스이용',
            'fixed'     => '정액제',
            'usage'     => '종량제',
            'time'      => '시간종량',
            'tusage'    => '이용료',
            'floor'     => '요금절삭'
        );

        return isset($new_arr[$billtype])?$new_arr[$billtype]:'';
    }
}

//unit 이 EA 이면 소숫점 자르고 천단위  comma, 그렇지 않으면 소숫점 그대로 유지(2자리), 천단위 콤마
if ( !function_exists('number_format_unit'))
{
	function number_format_unit($qty, $unit) {
		if(($unit=='EA'))//'EA' 이면 무조건.
		{
			return number_format($qty);
		}
		else
		{
			//$qty == string 형태임
			if(strpos($qty,'.'))//소숫점 있으면 소숫점2자리로. 그렇지 않으면 소수점 아래 없음
			{
				return number_format( (float)$qty,2);
			}
			else
			{
				return number_format( (int)$qty);
			}
		}
	}
}


/**
 * NVL
 *
 * 변수내용을 확인하여 NULL이거나 빈값이면 default 값으로 변경.
 */
if ( ! function_exists('nvl'))
{
    function nvl($val, $replace)
    {
            if( is_null($val) || empty($val) )  return $replace;
            else                                return $val;
    }
}



/**
 * 숫자데이터의 뒤 소수점 뒤 0 확인해서 제외 처리.
 */
if ( !function_exists('number_format_clean'))
{
    function number_format_clean($iNumber, $iDecimals = 2)
    {
        $sNumber = number_format($iNumber, $iDecimals);
        $sNumber = rtrim($sNumber, 0);
        $sNumber= rtrim($sNumber, '.');

        return $sNumber;
    }
}




/**
* array 변수 확인 및 empty 확인*
*/
if ( !function_exists('chk_element'))
{
    function chk_element($index, $array, $default=NULL)
    {
        return (isset($array[$index]) && !empty($array[$index])) ? $array[$index] : $default;
    }
}


/* End of file common_helper.php */