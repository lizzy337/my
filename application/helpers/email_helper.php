<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * email header make
 *
 * @access	public
 * @return	string
 */

if ( ! function_exists('mailheader'))
{
	function mailheader() {
		$mailheader	=	"{unwrap}<!DOCTYPE html>";
		$mailheader	.=	"<html lang='ko'>";
		$mailheader	.=	"<head>";
		$mailheader	.=	"<meta charset='utf-8' />";
		$mailheader	.=	"<meta http-equiv='X-UA-Compatible' content='IE=edge' />";
		$mailheader	.=	"<title>KINX - Korea Internet Neutral eXchange</title>";
		$mailheader	.=	"<link rel='stylesheet' type='text/css' href='".EMAIL_SENDBASE_URL."css/common.css' />";
		$mailheader	.=	"</head>";
		$mailheader	.=	"<body leftmargin='0' topmargin='0'>";
		$mailheader	.=	"<div style=\"margin:0;padding:0;font-family:'dotum', 'gulim', 'Arial', sans-serif;font-size:12px;color:#585858;line-height:20px;background:url('".EMAIL_SENDBASE_URL."images/img_uncollected2.gif') repeat-y;width:709px;margin:0 auto}\">";
		$mailheader	.=	"<p><img src='".EMAIL_SENDBASE_URL."wp-content/themes/kinx/images/mail/img_uncollected2.jpg' width='683' height='133' style=\"margin:0;padding:0;border:0;\" alt='' /></p>";
		//$mailheader	.=	"<div style=\"text-align:left;margin:0 50px\">{/unwrap}";
		$mailheader	.=	"<div style=\"text-align:left;margin:0 26px\">{/unwrap}"; //lizzy 141210
		return $mailheader;
	}
}

/* org
if ( ! function_exists('mailfooter'))
{
	function mailfooter() {
		$mailfooter		= "<table width='683' border='0' cellpadding='0' cellspacing='0'>";
		$mailfooter		.= "<tr>";
		$mailfooter		.= "  <td align='center' valign='middle' height='50' colspan='2' bgcolor='#f4f4f4'><font style='font-size:11px; color:#5383ab;'>본 이메일은 (주)케이아이엔엑스에서 회원사 및 관련사에게 보내드리는 정보입니다.";
		$mailfooter		.= "  <br>수신을 원하지 않으실 경우 <a href='mailto:info@kinx.net'>info@kinx.net</a>으로 수신거부요청을 하시기 바랍니다. 감사합니다.</font></td>";
		$mailfooter		.= "</tr>";
		$mailfooter		.= "<tr>";
		$mailfooter		.= "  <td width='37' height='60'></td>";
		$mailfooter		.= "  <td><font style='font-size:11px; color:#434343;'>주식회사 케이아이엔엑스 대표이사 이선영   서울특별시 강남구 남부순환로 2806 군인공제회관 20층   FAX. 02) 526-0998";
		$mailfooter		.= "  <br>24시간 고객지원센터 02) 526-0900 (3번)    COPYRIGHT(c) 2012 KINX INC. ALL RIGHT RESERVED.</font></td>";
		$mailfooter		.= "</tr>";
		$mailfooter		.= "</table>";
		$mailfooter		.=	"</body>";
		$mailfooter		.=	"</html>{/unwrap}";
		return $mailfooter;
	}
}
*/
//modified 2014-06-17
if ( ! function_exists('mailfooter'))
{
	function mailfooter() {
		$mailfooter		= "</div>";
		//$mailfooter		.= "<table width='683' border='0' cellpadding='0' cellspacing='0'>";
		$mailfooter		.= "<table width='683' border='0' cellpadding='0' cellspacing='0' style='margin-top:16px'>";//lizzy 141210
		$mailfooter		.= "<tr>";
		$mailfooter		.= "  <td align='center' valign='middle' height='50' colspan='2' bgcolor='#f4f4f4'><font style='font-size:11px; color:#5383ab;'>본 이메일은 (주)케이아이엔엑스에서 회원사 및 관련사에게 보내드리는 정보입니다.";
		$mailfooter		.= "  <br>수신을 원하지 않으실 경우 <a href='mailto:info@kinx.net'>info@kinx.net</a>으로 수신거부요청을 하시기 바랍니다. 감사합니다.</font></td>";
		$mailfooter		.= "</tr>";
		$mailfooter		.= "<tr>";
		$mailfooter		.= "  <td width='37' height='60'></td>";
		$mailfooter		.= "  <td><font style='font-size:11px; color:#434343;'>주식회사 케이아이엔엑스 대표이사 김지욱   서울시 강남구 강남대로 636 두원빌딩 7층,10층   FAX. 02) 526-0998";
		$mailfooter		.= "  <br>24시간 고객지원센터 02) 526-0900 (2번)    COPYRIGHT(c) 2012 KINX INC. ALL RIGHT RESERVED.</font></td>";
		$mailfooter		.= "</tr>";
		$mailfooter		.= "</table>";
		$mailfooter		.=	"</body>";
		$mailfooter		.=	"</html>{/unwrap}";
		return $mailfooter;
	}
}

/*
if ( ! function_exists('mailheader'))
{
	function mailheader() {
	    $mailheader	=	"{unwrap}<!DOCTYPE html>";
        $mailheader	.=	"<html lang='ko'>";
        $mailheader	.=	"<head>";
        $mailheader	.=	"<meta charset='utf-8' />";
        $mailheader	.=	"<meta http-equiv='X-UA-Compatible' content='IE=edge' />";
        $mailheader	.=	"<title>KINX - Korea Internet Neutral eXchange</title>";
        $mailheader	.=	"<link rel='stylesheet' type='text/css' href='".EMAIL_SENDBASE_URL."css/common.css' />";
        $mailheader	.=	"</head>";
        $mailheader	.=	"<body>";
        $mailheader	.=	"<div style=\"margin:0;padding:0;font-family:'dotum', 'gulim', 'Arial', sans-serif;font-size:12px;color:#585858;line-height:20px;background:url('".EMAIL_SENDBASE_URL."images/img_uncollected2.gif') repeat-y;width:709px;margin:0 auto}\">";
        $mailheader	.=	"<p><img src='".EMAIL_SENDBASE_URL."images/img_uncollected1.gif' width='709' height='236' style=\"margin:0;padding:0;border:0;\" alt='' /></p>";
        $mailheader	.=	"<div style=\"text-align:left;margin:0 50px\">{/unwrap}";
        return $mailheader;
	}
}

if ( ! function_exists('mailfooter'))
{
	function mailfooter() {
        $mailfooter		=	"{unwrap}</div><p style=\"margin:0;padding:0;line-height:0;\"><img src='".EMAIL_SENDBASE_URL."images/img_uncollected3.gif' width='709' height='151'' style=\"margin:0;padding:0;border:0;\" alt='' /></p></div>";
        $mailfooter		.=	"<p style=\"height:25px\"></p>";
        $mailfooter		.=	"</body>";
        $mailfooter		.=	"</html>{/unwrap}";
        return $mailfooter;
	}
}
*/



//start 2017 메일 폼 개선---------------------------------------------------------------

//header
if ( ! function_exists('kinx_mail_header'))
{
	function kinx_mail_header() {
		$mailheader		= "<!doctype html>";
		$mailheader		.= "<html>";
		$mailheader		.= "<head>";
		$mailheader		.= "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
		$mailheader		.= "<title>KINX EMAIL</title>";
		$mailheader		.= "</head>";
		$mailheader		.= "<body>";
		//$mailheader		.= "<!-- 타이틀 -->";
		$mailheader		.= "<table border='0' cellpadding='0' cellspacing='0' width='740'>";
		$mailheader		.= " <tr>";
		$mailheader		.= "  <td><img src='".EMAIL_SENDBASE_URL."kinximg/2019/11/title.jpg' border='0' style='display:block;' usemap='#title' /></td>";
		$mailheader		.= " </tr>";
		$mailheader		.= "</table>";
		$mailheader		.= "<map name='title'>";
		$mailheader		.= "<area shape='rect' coords='30,37,335,80' HREF='https://www.kinx.net/' target='_blank' />";
		$mailheader		.= "<area shape='rect' coords='483,49,503,72' HREF='https://www.kinx.net/service/ix/' target='_blank' />";
		$mailheader		.= "<area shape='rect' coords='508,49,570,72' HREF='https://www.kinx.net/service/colocation/' target='_blank' />";
		$mailheader		.= "<area shape='rect' coords='575,49,647,72' HREF='https://www.kinx.net/service/cloudhub/' target='_blank' />";
		$mailheader		.= "<area shape='rect' coords='653,49,704,72' HREF='https://www.kinx.net/service/cloud/' target='_blank' />";
		$mailheader		.= "</map>";

		return $mailheader;
	}
}

//title - 제목
if ( ! function_exists('kinx_mail_title'))
{
	function kinx_mail_title($title) {
		$mailtitle = "";
		$mailtitle		= "<!-- 본문 시작 -->";
		$mailtitle		.= '<table border="0" cellpadding="0" cellspacing="1" width="740" style="background-color:#cccccc">';
		$mailtitle		.= "<tr>";
		//$mailtitle		.= " <td style='border:0px; margin:0 auto !important;  width:640px !important;' ><br><br>";
		$mailtitle		.= '<td style="border:0px;background-color:white" align="center"><br><br>';

		$mailtitle		.= "<!-- 본문 제목 -->";
		//$mailtitle		.= "<table border='0' cellpadding='0' cellspacing='0' style='margin:0 auto !important; width:640px !important;'>";
		$mailtitle		.= "<table border='0' cellpadding='0' cellspacing='0' width='640'>";
		$mailtitle		.= "<tr>";
		$mailtitle		.= "  <td height='40'><font style='font-family:맑은 고딕,돋움; font-size:24px; letter-spacing:-2px;'>".$title."</font></td>";
		$mailtitle		.= "</tr>";
		$mailtitle		.= "<tr>";
		$mailtitle		.= " <td><hr size='0' color='#222222'/></td>";
		$mailtitle		.= "</tr>";
		$mailtitle		.= "</table><br>";

		return $mailtitle;
	}
}


//고객센터
if ( ! function_exists('kinx_mail_customercenter'))
{
	function kinx_mail_customercenter() {

		$mailcenter      = "<hr style='margin:0 auto; width:640px; height:1px; border:none; background-color:#cccccc;' align='center' />";
		$mailcenter		.= "<!-- 고객센터 -->";
		$mailcenter		.= "<table border='0' cellpadding='0' cellspacing='0' style='margin:0 auto;  width:580px;' align='center'>";
		//$mailcenter		.= "<table border='0' cellpadding='0' cellspacing='0' align='center'>";

		$mailcenter		.= "<tr>";
		$mailcenter		.= " <td height='10' colspan='3'></td>";
		$mailcenter		.= "</tr>";
		$mailcenter		.=	"<tr>";

		$mailcenter		.=	" <td width='250'>";
		$mailcenter		.=	"  <font style='font-family:맑은 고딕,돋움; color:#008d49; font-size:12px; font-weight:bold;'>KINX 고객센터</font><br>";
		$mailcenter		.=	"  <font style='font-family:맑은 고딕,돋움;font-size:11px;'>평일 09:00 ~ 18:00 (토,일,공휴일 휴무)<br>* 장애접수 24시간 365일 가능</font>";
		//$mailcenter		.=	"  <br><br>";
		$mailcenter		.=	"  </td>";

		$mailcenter		.=	" <td width='200'>";
		$mailcenter		.=	"  <font style='font-family:맑은 고딕,돋움; font-size:11px;'><b>서비스 상담</b><br>";
		$mailcenter		.=	"  <a href='mailto:sales@kinx.net'>sales@kinx.net</a> / <a href='http://goto.kakao.com/@kinx' target='_blank'>카카오톡 @KINX</a><br>02-526-0900(1)</font>";
		//$mailcenter		.=	"  <br><br>";
		$mailcenter		.=	"  </td>";
		$mailcenter		.=	" <td>";
		$mailcenter		.=	"   <font style='font-family:맑은 고딕,돋움; font-size:11px;'><b>기술 지원, 장애 접수</b><br>";
		$mailcenter		.=	"   <a href='mailto:tech@kinx.net'>tech@kinx.net</a><br>02-526-0900(2)</font>";
		//$mailcenter		.=	"  <br><br>";
		$mailcenter		.=	"  </td>";
		$mailcenter		.=	"</tr>";
		$mailcenter		.=	"</table><br>";
		$mailcenter		.=	"<!-- end 고객센터 -->";



		return $mailcenter;
	}
}


// footer
//type 1 - 단순 푸터 (수신자 사내용)
//type 2 - 회신 불가 푸터 (발신자가 help)
//type 3 - 일반 푸터 (그 외 모든 메일)
if ( ! function_exists('kinx_mail_footer'))
{
	function kinx_mail_footer($type) {

		$mailfooter	 = "		  </td>";
		$mailfooter	 .= "</tr>";
		$mailfooter	 .= "</table>";
		$mailfooter	 .= "<!-- 본문 끝 -->";


		$mailfooter		.= "<!-- Footer -->";
		$mailfooter		.= "<table border='0' cellpadding='0' cellspacing='0' width='740' bgcolor='#464646'>";
		$mailfooter		.= "<tr>";
		$mailfooter		.= "<td align='center'>";

		//$mailfooter		.= "<table border='0' cellpadding='0' cellspacing='0' style='margin:0 auto !important; width:660px !important;'>";
		$mailfooter		.= "<table border='0' cellpadding='0' cellspacing='0' width='660'>";
		$mailfooter		.= "<tr>";
		$mailfooter		.= " <td colspan='2' height='20'></td>";
		$mailfooter		.= "</tr>";
		$mailfooter		.= "<tr>";
		$mailfooter		.= " <td width='560' style='line-height:15px;;'><font style='font-family:맑은 고딕,돋움; font-size:11px; color:#999999'>";

		switch($type)
		{
			case 1 : //단순 푸터 (수신자 사내용)
				break;
			case 2 : //회신 불가 푸터 (발신자가 help)
				$mailfooter		.= "본 메일은 발신전용으로 회신되지 않으며<br>정보통신망법률 등 관련규정에 의거하여 회원사 및 관련사에 필요한 내용을 담고 있습니다.<br><br>";
				break;
			case 3 : //일반 푸터 (그 외 모든 메일)
			default :
				$mailfooter		.= "본 메일은 정보통신망법률 등 관련규정에 의거하여 회원사 및 관련사에 필요한 내용을 담고 있습니다.<br><br>";
				break;
		}

		$mailfooter		.= "<b>(주)케이아이엔엑스</b> 서울시 강남구 강남대로 636 두원빌딩 7층,10층<br>";
		$mailfooter		.= "사업자등록번호: 101-81-59273ㅣ통신판매업 신고번호: 제 강남-1134호 <br>";
		$mailfooter		.= "© KINX Inc. All Rights Reserved.</font></td>";
		$mailfooter		.= " <td width='100' align='right'><a href='https://www.kinx.net/' target='_blank'><img src='".EMAIL_SENDBASE_URL."kinximg/2016/11/email_kinx_logo.png' border='0' /></a>";
		$mailfooter		.= "</td>";
		$mailfooter		.= "</tr>";
		$mailfooter		.= "<tr>";
		$mailfooter		.= " <td colspan='2' height='20'></td>";
		$mailfooter		.= "</tr>";
		$mailfooter		.= "</table>";

		$mailfooter		.= "</td>";
		$mailfooter		.= "</tr>";
		$mailfooter		.= "</table>";
		//$mailfooter		.= "<br><br>";
		$mailfooter		.= "</body>";
		$mailfooter		.=	"</html>";
		return $mailfooter;
	}
}

/**
 * 메일발송
 * @param unknown $data
 */
if ( ! function_exists('kinxMailSendFnc'))
{
	function kinxMailSendFnc($data, $debug=FALSE, $type=1)
	{
		//test
		//$debug = TRUE;//$debug = FALSE로 해야함

		$CI =& get_instance();
		$CI->load->library('email');
		$CI->load->helper('email');

		if($type==3)//ipv6, peering
		{
			$sMailDocumentBody = $data['message'];
		}
		else
		{
			$sMailDocumentBody = kinx_mail_header();
			$title = ( isset($data['title']) )? $data['title'] : '제목없음';
			$sMailDocumentBody .= kinx_mail_title($title);
			$sMailDocumentBody .= $data['message'];
			$inout = ( isset($data['inout']) )? $data['inout'] : 'in';
			if($inout == 'out')//외부고객 메일이면 고객샌터 추가
				$sMailDocumentBody .= kinx_mail_customercenter();
			$footertype = ( isset($data['footertype']) )? $data['footertype'] :1;
			$sMailDocumentBody .= kinx_mail_footer($footertype);
		}

		$fromemail	= isset($data['fromemail']) ? $data['fromemail'] :  EMAIL_HELP;
		$fromname	= isset($data['fromname']) ? $data['fromname'] :  EMAIL_SENDMAIL_NAME;

		$CI->email->clear(TRUE);
		$CI->email->from($fromemail, $fromname);# 발신
		$CI->email->to($data['to']);# 수신

		if(isset($data['cc']) && !empty($data['cc']))
		{
			$CI->email->bcc($data['cc']);	# 참조
		}

		if(isset($data['bcc']) && !empty($data['bcc']))
		{
			$CI->email->bcc($data['bcc']);	# 숨은참조
		}

		$CI->email->subject($data['subject']);//메일 제목
		$CI->email->message($sMailDocumentBody);//content

		//파일 처리중
		$file_path = '';
		if(isset($data['files']))
		{
			foreach($rowfiles as $filerow) {
				unset($file_path);
				$file_path = BASEPATH.$filerow['sPath'].$filerow['sFileName'];
				$CI->email->attach($file_path, 'attachment', $filerow['sOriginal']);
			}
		}

		$result = $CI->email->send();

		if($debug)
		{
			echo $sMailDocumentBody;
			print_r($data);
			print_r($CI->email->print_debugger());
			exit;
		}
		else return $result;
	}
}

//end 2017 메일 폼 개선---------------------------------------------------------------

?>