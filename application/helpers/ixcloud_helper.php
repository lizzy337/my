<?php defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * 클라우드에서 사용하는 리소스명을 한글로 전환한다. *
 * @param string $resourcetype
 * @param string $type
 * @param string $product
 */
if ( !function_exists('chg_cloud_resource_name'))
{
	function chg_cloud_resource_name($resourcetype = NULL, $type = NULL, $product='ixcloud')
    {
        if($product=='ixcloud')
        {
            $name_array = array(
                'instance'              => array('code'=>'IIS', 'name' => '인스턴스'),
                'volume'                => array('code'=>'IVU', 'name' => '볼륨'),
                'network'               => array('code'=>'INW', 'name' => '네트워크'),
                'router'                => array('code'=>'IRT', 'name' => '라우터'),
                'floating_ip'           => array('code'=>'IFI', 'name' => '공인아이피'),
                'os_image'              => array('code'=>'IIG', 'name' => '이미지'),
                'image_snapshot'        => array('code'=>'ISS', 'name' => '이미지 스냅샷'),
                'volume_snapshot'       => array('code'=>'IVS', 'name' => '볼륨 스냅샷'),
                'traffic'               => array('code'=>'ITF', 'name' => '트래픽'),
                'lbaas'                 => array('code'=>'ILB', 'name' => '로드밸런서'),
                'backup_agent'          => array('code'=>'BKA', 'name' => '부가서비스(백업에이전트)'),
                'backup_storage'        => array('code'=>'BKS', 'name' => '부가서비스(백업스토리지)'),
                'kdx_contract'          => array('code'=>'IKC', 'name' => 'KDX'),
                'kdx_port'              => array('code'=>'IKP', 'name' => 'KDX 포트'),
                'schedule_snapshot'     => array('code'=>'IRS', 'name' => '스케쥴스냅샷'),
                'nas'                   => array('code'=>'INS', 'name' => 'NAS(Standard)'),
                'nas_snapshot'          => array('code'=>'INT', 'name' => 'NAS 스냅샷'),
                'ha_solution'           => array('code'=>'IHA', 'name' => 'HA솔루션'),
                'db_security'           => array('code'=>'ISD', 'name' => 'DB보안상품'),
                'ads_firewall_net'      => array('code'=>'IWN', 'name' => '방화벽'),
                'ads_firewall_web'      => array('code'=>'IWW', 'name' => '웹방화벽'),
                'ads_vaccine'           => array('code'=>'IVC', 'name' => '바이러스 백신'),
                'ads_ransomware'        => array('code'=>'IRW', 'name' => '랜섬웨어 차단'),
                'ads_webshell'          => array('code'=>'IWS', 'name' => '웹쉘 탐지'),
                'ads_vpn'               => array('code'=>'IVN', 'name' => 'SSL VPN'),
                'ads_filecloud_account' => array('code'=>'IFA', 'name' => '파일 클라우드 계정'),
                'ads_filecloud_storage' => array('code'=>'IFS', 'name' => '파일 클라우드 스토리지'),
                'k8s_cluster'           => array('code'=>'ICT', 'name' => '쿠버네티스(클러스터)')
            );
        }
        elseif($product=='multizone')
        {
            $name_array = array(
                'vpc'                     => array('code'=>'MCN', 'name' => '크로스존 네트워크'),
                'klb'                     => array('code'=>'MRB', 'name' => '멀티존 로드벨런서'),
                'mz_kdx_contract'         => array('code'=>'MKX', 'name' => '클라우드 허브(KDX)'),
                'mz_kdx_port'             => array('code'=>'MKP', 'name' => '클라우드 허브(KDX 포트)'),
                'db_security_maintenance' => array('code'=>'MSD', 'name' => 'DB보안상품 유지보수'),
                'icr_registry'            => array('code'=>'MCR', 'name' => '컨테이너 레지스트리')
            );

        }

        if(is_null($resourcetype)){ return $name_array; }
        elseif(!is_null($type)){ return isset($name_array[$resourcetype][$type])?$name_array[$resourcetype][$type]:''; }
        else{ return $name_array[$resourcetype];}

	}
}



/**
 * 계약상세화면의 클라우드 사용 리소스 목록 추출을 위한 array
 * 위 리소스항목 변경에 대한 관리를 위해 이곳에서 정의
 * @param string $resourcecode
 * @param string $type
 * @param string $product
 */
if ( !function_exists('get_cloud_resource_detailset'))
{
	function get_cloud_resource_detailset($resourcecode = NULL, $type = NULL, $product='ixcloud')
    {
        if($product=='ixcloud')
        {
            $detail_setdata = array(
                'IIS' => array("name"=>'인스턴스',      "chkbtn"=>TRUE, "sType"=>'IIS', "colname"=>array('인스턴스 명','타입','가격종류','사용기간'), "listkey"=>array('name||left||4','product||center||2', 'charge||center||3', 'date||center||3'), "list"=>array()),
                'IVU' => array("name"=>'볼륨 스토리지', "chkbtn"=>TRUE, "sType"=>'IVU', "colname"=>array('볼륨 명','타입','크기','사용기간'), "listkey"=>array('name||left||4','vo_type||center||2', 'size||right||3', 'date||center||3'), "list"=>array()),
                'IIG' => array("name"=>'이미지',        "chkbtn"=>TRUE, "sType"=>'IIG', "colname"=>array('이미지 명','사용 인스턴스','사용기간'), "listkey"=>array('product||left||5', 'name||center||4', 'date||center||3'), "list"=>array()),
                'IVS' => array("name"=>'볼륨스냅샷',    "chkbtn"=>TRUE, "sType"=>'IVS', "colname"=>array('볼륨스냅샷 명','크기','사용기간'), "listkey"=>array('name||left||6','size||right||3', 'date||center||3'), "list"=>array()),
                'ISS' => array("name"=>'이미지스냅샷',  "chkbtn"=>TRUE, "sType"=>'ISS', "colname"=>array('이미지스냅샷 명','크기','사용기간'), "listkey"=>array('name||left||6', 'size||right||3', 'date||center||3'), "list"=>array()),
                'IFI' => array("name"=>'공인IP',        "chkbtn"=>TRUE, "sType"=>'IFI', "colname"=>array('공인IP 주소','사용기간'), "listkey"=>array('name||left||9', 'date||center||3'), "list"=>array()),
                'INW' => array("name"=>'네트워크',      "chkbtn"=>TRUE, "sType"=>'INW', "colname"=>array('네트워크 명','사용기간'), "listkey"=>array('name||left||9', 'date||center||3'), "list"=>array()),
                'IRT' => array("name"=>'라우터',        "chkbtn"=>TRUE, "sType"=>'IRT', "colname"=>array('라우터 명','사용기간'), "listkey"=>array('name||left||9', 'date||center||3'), "list"=>array()),
                'ILB' => array("name"=>'로드밸런스',    "chkbtn"=>TRUE, "sType"=>'ILB', "colname"=>array('로드밸런스 명','타입','사용기간'), "listkey"=>array('name||left||6', 'lb_type||center||3', 'date||center||3'), "list"=>array()),
                'ITF' => array("name"=>'트래픽',        "chkbtn"=>FALSE,"sType"=>'ITF', "colname"=>null, "list"=>array()),
                'BKA' => array("name"=>'부가서비스(백업에이전트)', "chkbtn"=>TRUE,"sType"=>'BKA', "colname"=>array('상품 명','사용기간'), "listkey"=>array('name||left||9', 'date||center||3'), "list"=>array()),
                'BKS' => array("name"=>'부가서비스(백업스토리지)', "chkbtn"=>TRUE,"sType"=>'BKS', "colname"=>array('상품 명','사용량','사용기간'), "listkey"=>array('name||left||6', 'size||right||3', 'date||center||3'), "list"=>array()),
                'IKC' => array("name"=>'KDX',           "chkbtn"=>TRUE, "sType"=>'IKD', "colname"=>array('상품 명','사용기간'), "listkey"=>array('name||left||9', 'date||center||3'), "list"=>array()),
                'IKP' => array("name"=>'KDX 포트',      "chkbtn"=>TRUE, "sType"=>'IKP', "colname"=>array('상품 명','타입','사용기간'), "listkey"=>array('name||left||6', 'kp_type||center||3', 'date||center||3'), "list"=>array()),
                'IRS' => array("name"=>'스케쥴스냅샷',   "chkbtn"=>TRUE, "sType"=>'IRS', "colname"=>array('스케쥴스냅샷 명','크기','사용기간'), "listkey"=>array('name||left||6', 'size||right||3', 'date||center||3'), "list"=>array()),
                'INS' => array("name"=>'NAS(Standard)', "chkbtn"=>TRUE, "sType"=>'INS', "colname"=>array('NAS 명','크기','사용기간'), "listkey"=>array('name||left||6', 'size||right||3', 'date||center||3'), "list"=>array()),
                'INT' => array("name"=>'NAS 스냅샷',    "chkbtn"=>TRUE, "sType"=>'INT', "colname"=>array('NAS스냅샷 명','크기','사용기간'), "listkey"=>array('name||left||6', 'size||right||3', 'date||center||3'), "list"=>array()),
                'IHA' => array("name"=>'HA솔루션',      "chkbtn"=>TRUE, "sType"=>'IHA', "colname"=>array('HA솔루션 명','사용기간'), "listkey"=>array('name||left||9', 'date||center||3'), "list"=>array()),
                'ISD' => array("name"=>'DB보안상품',    "chkbtn"=>TRUE, "sType"=>'ISD', "colname"=>array('DB보안상품 명','타입','사용기간'), "listkey"=>array('name||left||6', 'product||center||3', 'date||center||3'), "list"=>array()),
                'IWN' => array("name"=>'방화벽',        "chkbtn"=>TRUE, "sType"=>'IWN', "colname"=>array('방화벽 명','타입','사용기간'), "listkey"=>array('name||left||6', 'wl_type||center||3', 'date||center||3'), "list"=>array()),
                'IWW' => array("name"=>'웹방화벽',      "chkbtn"=>TRUE, "sType"=>'IWW', "colname"=>array('웹방화벽 명','사용기간'), "listkey"=>array('name||left||6', 'date||center||3'), "list"=>array()),
                'IVC' => array("name"=>'바이러스 백신', "chkbtn"=>TRUE, "sType"=>'IVC', "colname"=>array('바이러스 백신 명','사용기간'), "listkey"=>array('name||left||6', 'date||center||3'), "list"=>array()),
                'IRW' => array("name"=>'랜섬웨어 차단', "chkbtn"=>TRUE, "sType"=>'IRW', "colname"=>array('랜섬웨어 차단 명','타입','사용기간'), "listkey"=>array('name||left||6', 'os_type||center||3', 'date||center||3'), "list"=>array()),
                'IWS' => array("name"=>'웹쉘 탐지',     "chkbtn"=>TRUE, "sType"=>'IWS', "colname"=>array('웹쉘 탐지 명','사용기간'), "listkey"=>array('name||left||6', 'date||center||3'), "list"=>array()),
                'IVN' => array("name"=>'SSL VPN',      "chkbtn"=>TRUE, "sType"=>'IVN', "colname"=>array('SSL VPN 명','타입','사용기간'), "listkey"=>array('name||left||6', 'vpn||center||3', 'date||center||3'), "list"=>array()),
                'IFA' => array("name"=>'파일 클라우드 계정',    "chkbtn"=>TRUE, "sType"=>'IFA', "colname"=>array('파일 클라우드 계정 명','수량','사용기간'), "listkey"=>array('name||left||6', 'count||center||3', 'date||center||3'), "list"=>array()),
                'IFS' => array("name"=>'파일 클라우드 스토리지',"chkbtn"=>TRUE, "sType"=>'IFS', "colname"=>array('파일 클라우드 스토리지 명','크기','사용기간'), "listkey"=>array('name||left||6', 'size||center||3', 'date||center||3'), "list"=>array()),
                'ICT' => array("name"=>'쿠버네티스(클러스터)',  "chkbtn"=>TRUE, "sType"=>'ICT', "colname"=>array('클러스터 명','사용기간'), "listkey"=>array('name||left||9', 'date||center||3'), "list"=>array())
            );
        }
        elseif($product=='multizone')
        {
            $detail_setdata = array(
                'MCN' => array("name"=>'크로스존 네트워크',       "chkbtn"=>TRUE, "sType"=>'MCN', "colname"=>array('크로스존 네트워크 명','사용기간'), "listkey"=>array('name||left||9', 'date||center||3'), "list"=>array()),
                'MRB' => array("name"=>'멀티존 로드벨런서',       "chkbtn"=>TRUE, "sType"=>'MRB', "colname"=>array('멀티존 로드벨런서 명','사용기간'), "listkey"=>array('name||left||9','date||center||3'), "list"=>array()),
                'MKX' => array("name"=>'클라우드 허브(KDX)',      "chkbtn"=>TRUE, "sType"=>'MKX', "colname"=>array('클라우드 허브(KDX) 명','사용기간'), "listkey"=>array('product||left||9','date||center||3'), "list"=>array()),
                'MKP' => array("name"=>'클라우드 허브(KDX 포트)', "chkbtn"=>TRUE, "sType"=>'MKP', "colname"=>array('클라우드 허브(KDX 포트) 명','상품타입','사용기간'), "listkey"=>array('name||left||6','kp_type||center||3', 'date||center||3'), "list"=>array()),
                'MSD' => array("name"=>'DB보안상품 유지보수',     "chkbtn"=>TRUE, "sType"=>'MSD', "colname"=>array('DB보안상품 유지보수 명','상품타입','사용기간'), "listkey"=>array('name||left||6','product||center||3', 'date||center||3'), "list"=>array()),
                'MCR' => array("name"=>'컨테이너 레지스트리',     "chkbtn"=>TRUE, "sType"=>'MCR', "colname"=>array('레지스트리 명', '용량(GB)', '사용기간'), "listkey"=>array('name||left||6','size||right||3', 'date||center||3'), "list"=>array())
            );
        }


        if(is_null($resourcecode)){ return $detail_setdata; }
        elseif(!is_null($type)){ return $detail_setdata[$resourcecode][$type]; }
        else{ return $detail_setdata[$resourcecode];}
    }
}



/**
 * 과금상세화면의 클라우드 사용 리소스에 대한 과금조정을 위한 array
 * 위 리소스항목 변경에 대한 관리를 위해 이곳에서 정의
 * @param string $resourcecode
 * @param string $type
 * @param string $product
 */
if ( !function_exists('get_cloud_resource_adjust_detailset'))
{
	function get_cloud_resource_adjust_detailset($resourcecode = NULL, $type = NULL, $product='ixcloud')
    {
        if($product=='ixcloud')
        {
            $detail_setdata = array(
                'IIS' => array("name"=>'인스턴스',                  "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'IIS', "colname"=>array('인스턴스 명','타입','가격종류','사용기간','기준금액', '조정금액'),   "listkey"=>array('name||left||2','product||center||2', 'charge||center||2', 'date||center||2', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'IVU' => array("name"=>'볼륨 스토리지',             "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'IVU', "colname"=>array('볼륨 명','타입','크기','사용기간','기준금액', '조정금액'),           "listkey"=>array('name||left||2','vo_type||center||2', 'size||right||2', 'date||center||2', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'IIG' => array("name"=>'이미지',                    "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'IIG', "colname"=>array('이미지 명','사용 인스턴스','사용기간','기준금액', '조정금액'),       "listkey"=>array('product||left||3', 'name||center||2', 'date||center||3', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'IVS' => array("name"=>'볼륨스냅샷',                "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'IVS', "colname"=>array('볼륨스냅샷 명','크기','사용기간','기준금액', '조정금액'),            "listkey"=>array('name||left||3','size||right||2', 'date||center||3', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'ISS' => array("name"=>'이미지스냅샷',              "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'ISS', "colname"=>array('이미지스냅샷 명','크기','사용기간','기준금액', '조정금액'),          "listkey"=>array('name||left||4', 'size||right||2', 'date||center||2', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'IFI' => array("name"=>'공인IP',                    "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'IFI', "colname"=>array('공인IP 주소','사용기간','기준금액', '조정금액'),                     "listkey"=>array('name||left||4', 'date||center||3', 'price||right||3', 'adjust||right||2'), "list"=>array()),
                'INW' => array("name"=>'네트워크',                  "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'INW', "colname"=>array('네트워크 명','사용기간','기준금액', '조정금액'),                     "listkey"=>array('name||left||4', 'date||center||3', 'price||right||3', 'adjust||right||2'), "list"=>array()),
                'IRT' => array("name"=>'라우터',                    "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'IRT', "colname"=>array('라우터 명','사용기간','기준금액', '조정금액'),                       "listkey"=>array('name||left||4', 'date||center||3', 'price||right||3', 'adjust||right||2'), "list"=>array()),
                'ILB' => array("name"=>'로드밸런스',                "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'ILB', "colname"=>array('로드밸런스 명','타입','사용기간','기준금액', '조정금액'),            "listkey"=>array('name||left||3', 'lb_type||center||2', 'date||center||3', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'ITF' => array("name"=>'트래픽',                    "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'ITF', "colname"=>array('트래픽 명','타입','사용기간','기준금액', '조정금액'),                "listkey"=>array('name||left||3', 'tr_type||center||2', 'date||center||3', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'BKA' => array("name"=>'부가서비스(백업에이전트)',  "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'BKA', "colname"=>array('상품 명','사용기간','기준금액', '조정금액'),                         "listkey"=>array('name||left||4', 'date||center||3', 'price||right||3', 'adjust||right||2'), "list"=>array()),
                'BKS' => array("name"=>'부가서비스(백업스토리지)',  "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'BKS', "colname"=>array('상품 명','사용량','사용기간','기준금액', '조정금액'),                "listkey"=>array('name||left||4', 'size||right||2', 'date||center||2', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'IKC' => array("name"=>'KDX',                      "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'IKC', "colname"=>array('상품 명','사용기간','기준금액', '조정금액'),                         "listkey"=>array('name||left||4', 'date||center||3', 'price||right||3', 'adjust||right||2'), "list"=>array()),
                'IKP' => array("name"=>'KDX 포트',                 "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'IKP', "colname"=>array('상품 명','타입','사용기간','기준금액', '조정금액'),                 "listkey"=>array('name||left||4', 'kp_type||center||2', 'date||center||2', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'IRS' => array("name"=>'스케쥴스냅샷',              "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'IRS', "colname"=>array('스케쥴스냅샷 명','크기','사용기간','기준금액', '조정금액'),          "listkey"=>array('name||left||4', 'size||right||2', 'date||center||2', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'INS' => array("name"=>'NAS(Standard)',            "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'INS', "colname"=>array('NAS 명','크기','사용기간','기준금액', '조정금액'),                  "listkey"=>array('name||left||3','size||right||2', 'date||center||3', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'INT' => array("name"=>'NAS 스냅샷',                "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'INT', "colname"=>array('NAS스냅샷 명','크기','사용기간','기준금액', '조정금액'),            "listkey"=>array('name||left||3','size||right||2', 'date||center||3', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'IHA' => array("name"=>'HA솔루션',                  "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0,  "sType"=>'IHA', "colname"=>array('HA솔루션 명','사용기간','기준금액', '조정금액'),                  "listkey"=>array('name||left||4', 'date||center||3', 'price||right||3', 'adjust||right||2'), "list"=>array()),
                'ISD' => array("name"=>'DB보안상품',                "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0,  "sType"=>'ISD', "colname"=>array('DB보안상품 명','타입','사용기간','기준금액', '조정금액'),          "listkey"=>array('name||left||4', 'product||center||2', 'date||center||2', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'IWN' => array("name"=>'방화벽',                    "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0,  "sType"=>'IWN', "colname"=>array('방화벽 명','타입','사용기간','기준금액', '조정금액'),            "listkey"=>array('name||left||4', 'wl_type||center||2', 'date||center||2', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'IWW' => array("name"=>'웹방화벽',                  "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0,  "sType"=>'IWW', "colname"=>array('웹방화벽 명','사용기간','기준금액', '조정금액'),                 "listkey"=>array('name||left||4', 'date||center||3', 'price||right||3', 'adjust||right||2'), "list"=>array()),
                'IVC' => array("name"=>'바이러스 백신',             "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0,  "sType"=>'IVC', "colname"=>array('바이러스 백신 명','사용기간','기준금액', '조정금액'),             "listkey"=>array('name||left||4', 'date||center||3', 'price||right||3', 'adjust||right||2'), "list"=>array()),
                'IRW' => array("name"=>'랜섬웨어 차단',             "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0,  "sType"=>'IRW', "colname"=>array('랜섬웨어 차단 명','타입','사용기간','기준금액', '조정금액'),      "listkey"=>array('name||left||4', 'os_type||center||2', 'date||center||2', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'IWS' => array("name"=>'웹쉘 탐지',                 "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0,  "sType"=>'IWS', "colname"=>array('웹쉘 탐지 명','사용기간','기준금액', '조정금액'),                "listkey"=>array('name||left||4', 'date||center||3', 'price||right||3', 'adjust||right||2'), "list"=>array()),
                'IVN' => array("name"=>'SSL VPN',                  "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0,  "sType"=>'IVN', "colname"=>array('SSL VPN 명','타입','사용기간','기준금액', '조정금액'),           "listkey"=>array('name||left||4', 'vpn||center||2', 'date||center||2', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'IFA' => array("name"=>'파일 클라우드 계정',         "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'IFA', "colname"=>array('파일 클라우드 계정 명','수량','사용기간','기준금액', '조정금액'),    "listkey"=>array('name||left||4', 'count||center||2', 'date||center||2', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'IFS' => array("name"=>'파일 클라우드 스토리지',     "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'IFS', "colname"=>array('파일 클라우드 스토리지 명','크기','사용기간','기준금액', '조정금액'), "listkey"=>array('name||left||4', 'size||center||2', 'date||center||2', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'ICT' => array("name"=>'쿠버네티스(클러스터)',       "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0,  "sType"=>'ICT', "colname"=>array('클러스터 명','사용기간','기준금액', '조정금액'),                   "listkey"=>array('name||left||4', 'date||center||3', 'price||right||3', 'adjust||right||2'), "list"=>array())

            );
        }
        elseif($product=='multizone')
        {
            $detail_setdata = array(
                'MCN' => array("name"=>'크로스존 네트워크',       "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'MCN', "colname"=>array('크로스존 네트워크 명','사용기간','기준금액', '조정금액'),                 "listkey"=>array('name||left||4', 'date||center||3', 'price||right||3', 'adjust||right||2'), "list"=>array()),
                'MRB' => array("name"=>'멀티존 로드벨런서',       "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'MRB', "colname"=>array('멀티존 로드벨런서 명','사용기간','기준금액', '조정금액'),                 "listkey"=>array('name||left||4','date||center||3', 'price||right||3', 'adjust||right||2'), "list"=>array()),
                'MKX' => array("name"=>'클라우드 허브(KDX)',      "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'MKX', "colname"=>array('클라우드 허브(KDX) 명','사용기간','기준금액', '조정금액'),                "listkey"=>array('product||left||4','date||center||3', 'price||right||3', 'adjust||right||2'), "list"=>array()),
                'MKP' => array("name"=>'클라우드 허브(KDX 포트)', "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'MKP', "colname"=>array('클라우드 허브(KDX 포트) 명','상품타입','사용기간','기준금액', '조정금액'), "listkey"=>array('name||left||4','kp_type||center||2', 'date||center||2', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'MSD' => array("name"=>'DB보안상품 유지보수',     "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'MSD', "colname"=>array('DB보안상품 유지보수 명','상품타입','사용기간','기준금액', '조정금액'),     "listkey"=>array('name||left||4','product||center||2', 'date||center||2', 'price||right||2', 'adjust||right||2'), "list"=>array()),
                'MCR' => array("name"=>'컨테이너 레지스트리',     "chkbtn"=>TRUE, "price"=>0, "discount"=>0, "discount_c"=>0, "discountInfo"=>NULL, "charge"=>0, "adjust"=>0, "sType"=>'MCR', "colname"=>array('레지스트리 명','용량(GB)','사용기간','기준금액', '조정금액'),             "listkey"=>array('name||left||4', 'size||center||2', 'date||center||2', 'price||right||2', 'adjust||right||2'), "list"=>array()),
            );
        }

        if(is_null($resourcecode)){ return $detail_setdata; }
        elseif(!is_null($type)){ return $detail_setdata[$resourcecode][$type]; }
        else{ return $detail_setdata[$resourcecode];}
    }
}




/**
 * mykinx 출력용 array
 * 위 리소스항목 변경에 대한 관리를 위해 이곳에서 정의
 * @param string $resourcecode
 * @param string $type
 * @param string $product
 */
if ( !function_exists('get_cloud_resource_mykinx_detailset'))
{
    function get_cloud_resource_mykinx_detailset($resourcecode = NULL, $type = NULL, $product='ixcloud')
    {
        if($product=='ixcloud')
        {
            $detail_setdata = array(
                'IIS' => array("name"=>'인스턴스',                 "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IIS', "colname"=>array('인스턴스 명','타입','가격종류','사용기간','사용시간', '가격'), "listkey"=>array('name||left||1',     'product||center||1',  'charge||center||1', 'date||center||1', 'hour||center||1', 'price||right||1'), "list"=>array()),
                'IVU' => array("name"=>'볼륨 스토리지',            "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IVU', "colname"=>array('볼륨 명','타입','크기','사용기간','사용시간', '가격'),         "listkey"=>array('name||left||1',     'vo_type||center||1',  'size||center||1',   'date||center||1', 'hour||center||'  , 'price||right||1'), "list"=>array()),
                'IIG' => array("name"=>'이미지',                   "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IIG', "colname"=>array('이미지 명','사용 인스턴스','사용기간','사용시간', '가격'),     "listkey"=>array('product||center||2', 'name||left||1',     'date||center||1',   'hour||center||1', 'price||right||1'), "list"=>array()),
                'IVS' => array("name"=>'볼륨스냅샷',               "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IVS', "colname"=>array('볼륨스냅샷 명','크기','사용기간','사용시간', '가격'),          "listkey"=>array('name||left||2',    'size||center||1',     'date||center||1',   'hour||center||1', 'price||right||1'), "list"=>array()),
                'ISS' => array("name"=>'이미지스냅샷',             "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'ISS', "colname"=>array('이미지스냅샷 명','크기','사용기간','사용시간', '가격'),        "listkey"=>array('name||left||2',    'size||center||1',     'date||center||1',   'hour||center||1', 'price||right||1'), "list"=>array()),
                'IFI' => array("name"=>'공인IP',                   "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IFI', "colname"=>array('공인IP 주소','사용기간','사용시간', '가격'),                 "listkey"=>array('name||left||3',    'date||center||1',     'hour||center||1',   'price||right||1'), "list"=>array()),
                'INW' => array("name"=>'네트워크',                 "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'INW', "colname"=>array('네트워크 명','사용기간','사용시간', '가격'),                  "listkey"=>array('name||left||3',    'date||center||1',     'hour||center||1',   'price||right||1'), "list"=>array()),
                'IRT' => array("name"=>'라우터',                   "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IRT', "colname"=>array('라우터 명','사용기간','사용시간', '가격'),                    "listkey"=>array('name||left||3',    'date||center||1',     'hour||center||1',   'price||right||1'), "list"=>array()),
                'ILB' => array("name"=>'로드밸런스',               "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'ILB', "colname"=>array('로드밸런스 명','타입','사용기간','사용시간', '가격'),          "listkey"=>array('name||left||2',    'lb_type||center||1',  'date||center||1',   'hour||center||1', 'price||right||1'), "list"=>array()),
                'ITF' => array("name"=>'트래픽',                   "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'ITF', "colname"=>array('트래픽 구분','IN(GB)','OUT(GB)','사용기간', '가격'),         "listkey"=>array('tr_type||left||1', 'bytein||center||1',   'byteout||center||1','date||center||1',   'hour||center||1', 'price||right||1'), "list"=>array()),
                'BKA' => array("name"=>'부가서비스(백업에이전트)',  "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'BKA', "colname"=>array('상품 명','사용기간','사용시간', '가격'),                      "listkey"=>array('name||left||3',    'date||center||1',     'hour||center||1',   'price||right||1'), "list"=>array()),
                'BKS' => array("name"=>'부가서비스(백업스토리지)',  "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'BKS', "colname"=>array('상품 명','사용량','사용기간','사용시간', '가격'),              "listkey"=>array('name||left||2',    'size||center||1',     'date||center||1',   'hour||center||1', 'price||right||1'), "list"=>array()),
                'IKC' => array("name"=>'KDX',                      "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IKC', "colname"=>array('상품 명','사용기간', '가격'),                               "listkey"=>array('name||left||4',    'date||center||1',     'hour||center||1',   'price||right||1'), "list"=>array()),
                'IKP' => array("name"=>'KDX 포트',                 "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IKP', "colname"=>array('상품 명','타입','사용기간', '가격'),                         "listkey"=>array('name||left||2',    'kp_type||center||1',  'date||center||1',   'hour||center||1',   'price||right||1'), "list"=>array()),
                'IRS' => array("name"=>'스케쥴스냅샷',              "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IRS', "colname"=>array('스케쥴스냅샷 명','타입','사용기간', '가격'),                  "listkey"=>array('name||left||2',    'size||center||1',     'date||center||1',   'hour||center||1',   'price||right||1'), "list"=>array()),
                'INS' => array("name"=>'NAS(Standard)',            "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'INS', "colname"=>array('NAS 명', '크기','사용기간','사용시간', '가격'),              "listkey"=>array('name||left||2',    'size||center||1',     'date||center||1',   'hour||center||1', 'price||right||1'), "list"=>array()),
                'INT' => array("name"=>'NAS 스냅샷',               "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'INT', "colname"=>array('NAS스냅샷 명','크기','사용기간', '사용시간', '가격'),         "listkey"=>array('name||left||2',    'size||center||1',     'date||center||1',   'hour||center||1', 'price||right||1'), "list"=>array()),
                'IHA' => array("name"=>'HA솔루션',                 "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IHA', "colname"=>array('HA솔루션 명','사용기간', '사용시간','가격'),                  "listkey"=>array('name||left||3',    'date||center||1',     'hour||center||1',   'price||right||1'), "list"=>array()),
                'ISD' => array("name"=>'DB보안상품',               "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'ISD', "colname"=>array('DB보안상품 명','타입','사용기간', '사용시간','가격'),         "listkey"=>array('name||left||2',    'product||center||1',   'date||center||1',   'hour||center||1', 'price||right||1'), "list"=>array()),
                'IWN' => array("name"=>'방화벽',                   "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IWN', "colname"=>array('방화벽 명','타입','사용기간', '사용시간','가격'),             "listkey"=>array('name||left||2',    'wl_type||center||1',   'date||center||1',   'hour||center||1', 'price||right||1'), "list"=>array()),
                'IWW' => array("name"=>'웹방화벽',                 "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IWW', "colname"=>array('웹방화벽 명','사용기간', '사용시간','가격'),                  "listkey"=>array('name||left||3',    'date||center||1',      'hour||center||1',   'price||right||1'), "list"=>array()),
                'IVC' => array("name"=>'바이러스 백신',            "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IVC', "colname"=>array('바이러스 백신 명','사용기간', '사용시간','가격'),              "listkey"=>array('name||left||3',    'date||center||1',      'hour||center||1',   'price||right||1'), "list"=>array()),
                'IRW' => array("name"=>'랜섬웨어 차단',            "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IRW', "colname"=>array('랜섬웨어 차단 명','타입','사용기간', '사용시간','가격'),       "listkey"=>array('name||left||2',    'os_type||center||1',   'date||center||1',   'hour||center||1', 'price||right||1'), "list"=>array()),
                'IWS' => array("name"=>'웹쉘 탐지',                "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IWS', "colname"=>array('웹쉘 탐지 명','사용기간', '사용시간','가격'),                 "listkey"=>array('name||left||3',    'date||center||1',      'hour||center||1',   'price||right||1'), "list"=>array()),
                'IVN' => array("name"=>'SSL VPN',                 "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IVN', "colname"=>array('SSL VPN 명','타입','사용기간', '사용시간','가격'),            "listkey"=>array('name||left||2',    'vpn||center||1',       'date||center||1',   'hour||center||1', 'price||right||1'), "list"=>array()),
                'IFA' => array("name"=>'파일 클라우드 계정',       "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IFA', "colname"=>array('파일 클라우드 계정 명','수량','사용기간', '사용시간','가격'),    "listkey"=>array('name||left||2',    'count||center||1',       'date||center||1',   'hour||center||1', 'price||right||1'), "list"=>array()),
                'IFS' => array("name"=>'파일 클라우드 스토리지',   "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IFS', "colname"=>array('파일 클라우드 스토리지 명','용량','사용기간', '사용시간','가격'), "listkey"=>array('name||left||2',    'size||center||1',       'date||center||1',   'hour||center||1', 'price||right||1'), "list"=>array()),
                'ICT' => array("name"=>'쿠버네티스(클러스터)',     "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IVC', "colname"=>array('클러스터 명','사용기간', '사용시간','가격'),                    "listkey"=>array('name||left||3',    'date||center||1',      'hour||center||1',   'price||right||1'), "list"=>array())
            );
        }
        elseif($product=='multizone')
        {
            $detail_setdata = array(
                'MCN' => array("name"=>'크로스존 네트워크',       "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'MCN', "colname"=>array('크로스존 네트워크 명','사용기간','가격'),                 "listkey"=>array('name||left||4', 'date||center||1', 'price||right||1'), "list"=>array()),
                'MRB' => array("name"=>'멀티존 로드벨런서',       "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'MRB', "colname"=>array('멀티존 로드벨런서 명','사용기간','가격'),                 "listkey"=>array('name||left||4', 'date||center||1', 'price||right||1'), "list"=>array()),
                'MKX' => array("name"=>'클라우드 허브(KDX)',      "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'MKX', "colname"=>array('클라우드 허브(KDX) 명','사용기간','가격'),                "listkey"=>array('name||left||4', 'date||center||1', 'price||right||1'), "list"=>array()),
                'MKP' => array("name"=>'클라우드 허브(KDX 포트)', "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'MKP', "colname"=>array('클라우드 허브(KDX 포트) 명','상품타입','사용기간','가격'), "listkey"=>array('name||left||3', 'kp_type||center||1', 'date||center||1', 'price||right||1'), "list"=>array()),
                'MSD' => array("name"=>'DB보안상품 유지보수',     "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'MSD', "colname"=>array('DB보안상품 유지보수 명','상품타입','사용기간','사용시간', '가격'), "listkey"=>array('name||left||2', 'product||center||1', 'date||center||1', 'hour||center||1', 'price||right||1'), "list"=>array()),
                'MCR' => array("name"=>'컨테이너 레지스트리',      "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'MCR', "colname"=>array('레지스트리 명','용량(GB)','사용기간', '사용시간','가격'),        "listkey"=>array('name||left||2',    'size||center||1',       'date||center||1',   'hour||center||1', 'price||right||1'), "list"=>array())
            );
        }

        if(is_null($resourcecode)){ return $detail_setdata; }
        elseif(!is_null($type)){ return $detail_setdata[$resourcecode][$type]; }
        else{ return $detail_setdata[$resourcecode];}
    }
}




/**
 * 거래명세서 포함되는 엑셀 파일을 위한 array
 * 위 리소스항목 변경에 대한 관리를 위해 이곳에서 정의
 * @param string $resourcecode
 * @param string $type
 * @param string $product
 */
if ( !function_exists('get_cloud_resource_excel_detailset'))
{
    function get_cloud_resource_excel_detailset($resourcecode = NULL, $type = NULL, $product='ixcloud')
    {
        if($product=='ixcloud')
        {
            $detail_setdata = array(
                'IIS' => array("name"=>'인스턴스',                 "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IIS', "colname"=>array('인스턴스 명','타입','가격종류','사용기간','사용시간', '가격'), "listkey"=>array('name||A||',     'product||B||',  'charge||C||', 'date||D||', 'hour||E||', 'price||F||'), "list"=>array()),
                'IVU' => array("name"=>'볼륨 스토리지',            "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IVU', "colname"=>array('볼륨 명','타입','크기','사용기간','사용시간', '가격'),         "listkey"=>array('name||A||',     'vo_type||B||',  'size||C||',   'date||D||', 'hour||E||'  , 'price||F||'), "list"=>array()),
                'IIG' => array("name"=>'이미지',                   "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IIG', "colname"=>array('이미지 명','사용 인스턴스','사용기간','사용시간', '가격'),     "listkey"=>array('product||A||B', 'name||C||',     'date||D||',   'hour||E||', 'price||F||'), "list"=>array()),
                'IVS' => array("name"=>'볼륨스냅샷',               "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IVS', "colname"=>array('볼륨스냅샷 명','크기','사용기간','사용시간', '가격'),          "listkey"=>array('name||A||B',    'size||C||',     'date||D||',   'hour||E||', 'price||F||'), "list"=>array()),
                'ISS' => array("name"=>'이미지스냅샷',             "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'ISS', "colname"=>array('이미지스냅샷 명','크기','사용기간','사용시간', '가격'),        "listkey"=>array('name||A||B',    'size||C||',     'date||D||',   'hour||E||', 'price||F||'), "list"=>array()),
                'IFI' => array("name"=>'공인IP',                   "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IFI', "colname"=>array('공인IP 주소','사용기간','사용시간', '가격'),                 "listkey"=>array('name||A||C',    'date||D||',     'hour||E||',   'price||F||'), "list"=>array()),
                'INW' => array("name"=>'네트워크',                 "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'INW', "colname"=>array('네트워크 명','사용기간','사용시간', '가격'),                  "listkey"=>array('name||A||C',    'date||D||',     'hour||E||',   'price||F||'), "list"=>array()),
                'IRT' => array("name"=>'라우터',                   "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IRT', "colname"=>array('라우터 명','사용기간','사용시간', '가격'),                    "listkey"=>array('name||A||C',    'date||D||',     'hour||E||',   'price||F||'), "list"=>array()),
                'ILB' => array("name"=>'로드밸런스',               "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'ILB', "colname"=>array('로드밸런스 명','타입','사용기간','사용시간', '가격'),          "listkey"=>array('name||A||B',    'lb_type||C||',  'date||D||',   'hour||E||', 'price||F||'), "list"=>array()),
                'ITF' => array("name"=>'트래픽',                   "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'ITF', "colname"=>array('트래픽 구분','IN(GB)','OUT(GB)','사용기간', '가격'),         "listkey"=>array('tr_type||A||B', 'bytein||C||',   'byteout||D||','date||E||', 'price||F||'), "list"=>array()),
                'BKA' => array("name"=>'부가서비스(백업에이전트)',  "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'BKA', "colname"=>array('상품 명','사용기간','사용시간', '가격'),                      "listkey"=>array('name||A||C',    'date||D||',     'hour||E||',   'price||F||'), "list"=>array()),
                'BKS' => array("name"=>'부가서비스(백업스토리지)',  "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'BKS', "colname"=>array('상품 명','사용량','사용기간','사용시간', '가격'),              "listkey"=>array('name||A||B',    'size||C||',     'date||D||',   'hour||E||', 'price||F||'), "list"=>array()),
                'IKC' => array("name"=>'KDX',                      "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IKC', "colname"=>array('상품 명','사용기간', '가격'),                               "listkey"=>array('name||A||D',    'date||E||',     'price||F||'), "list"=>array()),
                'IKP' => array("name"=>'KDX 포트',                 "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IKP', "colname"=>array('상품 명','타입','사용기간', '가격'),                         "listkey"=>array('name||A||C',    'kp_type||D||',  'date||E||',   'price||F||'), "list"=>array()),
                'IRS' => array("name"=>'스케쥴스냅샷',              "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IRS', "colname"=>array('스케쥴스냅샷 명','타입','사용기간', '가격'),                  "listkey"=>array('name||A||C',    'size||D||',     'date||E||',   'price||F||'), "list"=>array()),
                'INS' => array("name"=>'NAS(Standard)',            "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'INS', "colname"=>array('NAS 명', '크기','사용기간','사용시간', '가격'),              "listkey"=>array('name||A||B',    'size||C||',     'date||D||',   'hour||E||', 'price||F||'), "list"=>array()),
                'INT' => array("name"=>'NAS 스냅샷',               "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'INT', "colname"=>array('NAS스냅샷 명','크기','사용기간', '사용시간', '가격'),         "listkey"=>array('name||A||B',    'size||C||',     'date||D||',   'hour||E||', 'price||F||'), "list"=>array()),
                'IHA' => array("name"=>'HA솔루션',                 "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IHA', "colname"=>array('HA솔루션 명','사용기간', '사용시간','가격'),                  "listkey"=>array('name||A||C',    'date||D||',     'hour||E||',   'price||F||'), "list"=>array()),
                'ISD' => array("name"=>'DB보안상품',               "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'ISD', "colname"=>array('DB보안상품 명','타입','사용기간', '사용시간','가격'),         "listkey"=>array('name||A||B',    'product||C||',  'date||D||',   'hour||E||', 'price||F||'), "list"=>array()),
                'IWN' => array("name"=>'방화벽',                   "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IWN', "colname"=>array('방화벽 명','타입','사용기간', '사용시간','가격'),            "listkey"=>array('name||A||B',    'wl_type||C||',  'date||D||',   'hour||E||', 'price||F||'), "list"=>array()),
                'IWW' => array("name"=>'웹방화벽',                 "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IWW', "colname"=>array('웹방화벽 명','사용기간', '사용시간','가격'),                 "listkey"=>array('name||A||C',    'date||D||',   'hour||E||', 'price||F||'), "list"=>array()),
                'IVC' => array("name"=>'바이러스 백신',            "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IVC', "colname"=>array('바이러스 백신 명','사용기간', '사용시간','가격'),             "listkey"=>array('name||A||C',    'date||D||',   'hour||E||', 'price||F||'), "list"=>array()),
                'IRW' => array("name"=>'랜섬웨어 차단',            "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IRW', "colname"=>array('랜섬웨어 차단 명','타입','사용기간', '사용시간','가격'),      "listkey"=>array('name||A||B',    'os_type||C||',  'date||D||',   'hour||E||', 'price||F||'), "list"=>array()),
                'IWS' => array("name"=>'웹쉘 탐지',                "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IWS', "colname"=>array('웹쉘 탐지 명','사용기간', '사용시간','가격'),                "listkey"=>array('name||A||C',    'date||D||',   'hour||E||', 'price||F||'), "list"=>array()),
                'IVN' => array("name"=>'SSL VPN',                 "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IVN', "colname"=>array('SSL VPN 명','타입','사용기간', '사용시간','가격'),           "listkey"=>array('name||A||B',    'vpn||C||',  'date||D||',   'hour||E||', 'price||F||'), "list"=>array()),
                'IFA' => array("name"=>'파일 클라우드 계정',       "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IFA', "colname"=>array('파일 클라우드 계정 명','수량','사용기간', '사용시간','가격'),    "listkey"=>array('name||A||B',    'count||C||',  'date||D||',   'hour||E||', 'price||F||'), "list"=>array()),
                'IFS' => array("name"=>'파일 클라우드 스토리지',   "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IFS', "colname"=>array('파일 클라우드 스토리지 명','용량','사용기간', '사용시간','가격'), "listkey"=>array('name||A||B',    'size||C||',  'date||D||',   'hour||E||', 'price||F||'), "list"=>array()),
                'ICT' => array("name"=>'쿠버네티스(클러스터)',     "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'IVC', "colname"=>array('클러스터 명','사용기간', '사용시간','가격'),                    "listkey"=>array('name||A||C',    'date||D||',   'hour||E||', 'price||F||'), "list"=>array())
            );
        }
        elseif($product=='multizone')
        {
            $detail_setdata = array(
                'MCN' => array("name"=>'크로스존 네트워크',       "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'MCN', "colname"=>array('크로스존 네트워크 명','사용기간','가격'),                 "listkey"=>array('name||A||D', 'date||E||', 'price||F||'), "list"=>array()),
                'MRB' => array("name"=>'멀티존 로드벨런서',       "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'MRB', "colname"=>array('멀티존 로드벨런서 명','사용기간','가격'),                 "listkey"=>array('name||A||D', 'date||E||', 'price||F||'), "list"=>array()),
                'MKX' => array("name"=>'클라우드 허브(KDX)',      "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'MKX', "colname"=>array('클라우드 허브(KDX) 명','사용기간','가격'),                "listkey"=>array('name||A||D', 'date||E||', 'price||F||'), "list"=>array()),
                'MKP' => array("name"=>'클라우드 허브(KDX 포트)', "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'MKP', "colname"=>array('클라우드 허브(KDX 포트) 명','상품타입','사용기간','가격'), "listkey"=>array('name||A||C', 'kp_type||D||', 'date||E||', 'price||F||'), "list"=>array()),
                'MSD' => array("name"=>'DB보안상품 유지보수',     "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'MSD', "colname"=>array('DB보안상품 유지보수 명','상품타입','사용기간','사용시간', '가격'), "listkey"=>array('name||A||B', 'product||C||', 'date||D||', 'hour||E||', 'price||F||'), "list"=>array()),
                'MCR' => array("name"=>'컨테이너 레지스트리',      "price"=>0, "bdiscount"=>0, "adjcharge"=>0, "discount"=>0, "charge"=>0, "sType"=>'MCR', "colname"=>array('레지스트리명 명','용량(GB)','사용기간', '사용시간','가격'),       "listkey"=>array('name||A||B',    'size||C||',  'date||D||',   'hour||E||', 'price||F||'), "list"=>array())
            );
        }

        if(is_null($resourcecode)){ return $detail_setdata; }
        elseif(!is_null($type)){ return $detail_setdata[$resourcecode][$type]; }
        else{ return $detail_setdata[$resourcecode];}
    }
}




/**
 * IXCloud 에서 넘어오는 resouce_type 이 그때그때 다르다. 일단 ISM에서 처리하는 type 명으로 변경한다.
 * 아래 내용외 더 있는 경우 추가한다.
 * @param array $array
 * @param string $product
 */
if ( !function_exists('chg_cloud_resouce_type_name'))
{
    function chg_cloud_resouce_type_name($array, $product='ixcloud')
    {
        if($product=='ixcloud')
        {
            foreach($array as $k=>$v)
            {
                if($v['resource_type']=='image_snapshots'){ $array[$k]['resource_type'] = 'image_snapshot'; }
                if($v['resource_type']=='volumes'){ $array[$k]['resource_type'] = 'volume'; }
                if($v['resource_type']=='volume_snapshots'){ $array[$k]['resource_type'] = 'volume_snapshot'; }
                if($v['resource_type']=='routers'){ $array[$k]['resource_type'] = 'router'; }
                if($v['resource_type']=='instances'){ $array[$k]['resource_type'] = 'instance'; }
                if($v['resource_type']=='lbaas_pools'){ $array[$k]['resource_type'] = 'lbaas'; }
                if($v['resource_type']=='lbaas_pool'){ $array[$k]['resource_type'] = 'lbaas'; }
                if($v['resource_type']=='lbs'){ $array[$k]['resource_type'] = 'lbaas'; }
                if($v['resource_type']=='networks'){ $array[$k]['resource_type'] = 'network'; }
                if($v['resource_type']=='floating_ips'){ $array[$k]['resource_type'] = 'floating_ip'; }
                if($v['resource_type']=='lbs'){ $array[$k]['resource_type'] = 'lbaas'; }
                if($v['resource_type']=='object_store'){ $array[$k]['resource_type'] = 'volume'; }
                if($v['resource_type']=='object_storage'){ $array[$k]['resource_type'] = 'volume'; }
                if($v['resource_type']=='images'){ $array[$k]['resource_type'] = 'os_image'; }
                if($v['resource_type']=='ha_solution'){ $array[$k]['resource_type'] = 'ha_solution';}
                if($v['resource_type']=='db_security'){ $array[$k]['resource_type'] = 'db_security';}
            }
        }
        elseif($product=='multizone')
        {
            foreach($array as $k=>$v)
            {
                if($v['resource_type']=='vpc'){ $array[$k]['resource_type'] = 'vpc'; }
                if($v['resource_type']=='klb'){ $array[$k]['resource_type'] = 'klb'; }
                if($v['resource_type']=='mz_kdx_contract'){ $array[$k]['resource_type'] = 'mz_kdx_contract'; }
                if($v['resource_type']=='mz_kdx_port'){ $array[$k]['resource_type'] = 'mz_kdx_port'; }
                if($v['resource_type']=='db_security_maintenance'){ $array[$k]['resource_type'] = 'db_security_maintenance';}
            }
        }

        return $array;
    }
}





/**
 * Cloud 안정화/할인정보를 추출한다.
 * @param string $contract : tContract 정보
 */
if ( !function_exists('get_cloud_discount_info'))
{
    function get_cloud_discount_info($contract)
    {
        $stabilization = array();  //안정화
        $discount      = array();  //할인정책

        # no_pay : 0 유료, 1 무료    managed : 0 미과금, 1 과금
        $discountopt = element('arDiscountOpt', $contract) ? chg_arrayfield($contract['arDiscountOpt']) : array('stabilization'=>array(), 'discount'=>array(), 'no_pay'=>0, 'managed'=>0);
        $no_pay      = (int)element('no_pay', $discountopt, 0);
        $managed     = (int)element('managed', $discountopt, 0);

        # 유료인 경우만 할인정보 추출
        if($no_pay == 0)
        {
            # ['nProductSeq']==320은 멀티존 상품.
            $product = $contract['nProductSeq']==320 ? 'multizone' : 'ixcloud';

            # IXCloud에서 받아온 resource_type 보정
            $discountopt['stabilization'] = chg_cloud_resouce_type_name($discountopt['stabilization'], $product);
            $discountopt['discount']      = chg_cloud_resouce_type_name($discountopt['discount'], $product);

            # 안정화정보 추출
            $stabilization  = array(
                'flag'    => count($discountopt['stabilization']),
                'date'    => '',
                'enable'  => array(),
                'disable' => array()
            );

            foreach($discountopt['stabilization'] as $srow)
            {
                if($srow['is_active']){ $stabilization['enable'][] = chg_cloud_resource_name($srow['resource_type'],'name', $product); }
                else{ $stabilization['disable'][] = chg_cloud_resource_name($srow['resource_type'], 'name', $product); }

                $stabilization['date'] = $srow['start'].' ~ '.$srow['end'];
            }

            # 할인정보 추출
            $discount = chg_cloud_resource_name(NULL, NULL, $product);
            foreach($discountopt['discount'] as $drow)
            {
                $unitstr = array('currency'=>'원', 'percent'=>'%', 'percentage'=>'%');
                $discount[$drow['resource_type']]['discount'] =  array($drow['description'], number_format($drow['amount']).$unitstr[$drow['unit']], $drow['start'].' ~ '.$drow['end']);
            }
        }

    //    va($contract);
    //    va($detailinfo);
    //    va($discountopt);
    //    va($stabilization);
    //    va($discount);
    //    die;
        return array($no_pay, $stabilization, $discount, $managed);
    }
}

