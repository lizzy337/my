<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2012 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2012 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
//error_reporting(E_ALL);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);


date_default_timezone_set('Asia/Seoul');
//데이터 추출및 확인
if (PHP_SAPI == 'cli') die('This example should only be run from a Web Browser');

// Create new PHPExcel object
$objPHPExcel = new $this->excel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("KINX intranet")
    ->setLastModifiedBy("KINX intranet")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("KINX");

foreach($resource as $wsidx => $res)
{
    if($wsidx > 0){ $objPHPExcel->createSheet(); }
    $objPHPExcel->setActiveSheetIndex($wsidx);
    $objPHPExcel->getActiveSheet()->getStyle('A1:F1000')->getFont()->setSize(10);
    $objPHPExcel->getActiveSheet()->getStyle('A1:F1000')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


    # 셀 너비 설정( 숫자는 엑셀에서 적용되는 숫자, 엑셀을 보면서 설정하면 간단.)
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(22);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(16);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(16);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(16);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(16);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(16);


    #------------------------------------------------------------------------------------------------------------------------------
    #  프로젝트 정보
    #------------------------------------------------------------------------------------------------------------------------------
    # 1) 프로젝트 정보
    # 1-1) label
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '프로젝트 정보');
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(11);
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->mergeCells('A1:F1');


    # 1-2) 기존정보
    $objPHPExcel->getActiveSheet()
        ->setCellValue('A2', '프로젝트 명')
        ->setCellValue('C2', '등록일')
        ->setCellValue('D2', '과금유형')
        ->setCellValue('E2', '청구금액')
    ;

    $objPHPExcel->getActiveSheet()->getStyle('A2:F2')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('A2:F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A2:F2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFD9D9D9');
    $objPHPExcel->getActiveSheet()->mergeCells('A2:B2');
    $objPHPExcel->getActiveSheet()->mergeCells('E2:F2');

    //$arDetailInfo = chg_arrayfield($info[$wsidx]['arDetailInfo']);
    //$arDiscountOpt = chg_arrayfield($info[$wsidx]['arDiscountOpt']);
    $arDetailInfo = $info[$wsidx]['arDetailInfo'];
    $arDiscountOpt = $info[$wsidx]['arDiscountOpt'];
    $lastCharge = $info[$wsidx]['nCharge'] + $info[$wsidx]['nAdjCharge'] + $info[$wsidx]['nDiscount'] + $info[$wsidx]['nVatPrice']+ $info[$wsidx]['nManagedCharge'];


    $objPHPExcel->getActiveSheet()
        ->setCellValue('A3', $info[$wsidx]['sName'])
        ->setCellValue('C3', substr($arDetailInfo['project']['created_at'],0,10))
        ->setCellValue('D3', ($arDiscountOpt['no_pay']==0) ? '유료' : '무료')
        ->setCellValue('E3', $lastCharge)
    ;
    unset($arDetailInfo);
    $objPHPExcel->getActiveSheet()->getStyle('E3')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA);
    $objPHPExcel->getActiveSheet()->getStyle('A2:F3')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->mergeCells('A3:B3');
    $objPHPExcel->getActiveSheet()->mergeCells('E3:F3');


    # 1-3) 리소스별 정보
    # 1-3-1) thead 생성
    $objPHPExcel->getActiveSheet()
        ->setCellValue('A5', '항목')
        ->setCellValue('B5', '기준금액')
        ->setCellValue('C5', '기본할인')
        ->setCellValue('D5', '조정금액')
        ->setCellValue('E5', '할인금액')
        ->setCellValue('F5', '청구금액')
    ;


    $objPHPExcel->getActiveSheet()->getStyle('A5:F5')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('A5:F5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A5:F5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFD9D9D9');


    # 1-3-2) 내용 생성
    $cellrow       = 5;
    $sum_price     = 0;
    $sum_bdiscount = 0;
    $sum_adjcharge = 0;
    $sum_discount  = 0;
    $sum_charge    = 0;
    foreach($res as $res_row)
    {
        $cellrow++;

        $objPHPExcel->getActiveSheet()
            ->setCellValue('A'.$cellrow, $res_row['name'])
            ->setCellValue('B'.$cellrow, $res_row['price'])
            ->setCellValue('C'.$cellrow, $res_row['bdiscount'])
            ->setCellValue('D'.$cellrow, $res_row['adjcharge'])
            ->setCellValue('E'.$cellrow, $res_row['discount'])
            ->setCellValue('F'.$cellrow, $res_row['price']+$res_row['discount']+$res_row['adjcharge']+$res_row['bdiscount'])
        ;

        $sum_price     += $res_row['price'];
        $sum_bdiscount += $res_row['bdiscount'];
        $sum_adjcharge += $res_row['adjcharge'];
        $sum_discount  += $res_row['discount'];
        $sum_charge    += $res_row['price']+$res_row['discount']+$res_row['adjcharge']+$res_row['bdiscount'];
    }

    # 1-3-3) 전체 합계

    $cellrow++;
    $chkborderrow = $cellrow;
    $objPHPExcel->getActiveSheet()
        ->setCellValue('A'.$cellrow, '청구금액합계')
        ->setCellValue('B'.$cellrow, ($sum_price))
        ->setCellValue('C'.$cellrow, ($sum_bdiscount))
        ->setCellValue('D'.$cellrow, ($sum_adjcharge))
        ->setCellValue('E'.$cellrow, ($sum_discount))
        ->setCellValue('F'.$cellrow, ($sum_charge))
    ;

    $cellrow++;
    $objPHPExcel->getActiveSheet()
        ->setCellValue('A'.$cellrow, '추가 조정 금액')
        ->setCellValue('B'.$cellrow, $info[$wsidx]['nAdjCharge'] - $info[$wsidx]['nAddAdjCharge'])
    ;
    $objPHPExcel->getActiveSheet()->mergeCells('B'.$cellrow.':F'.$cellrow);

    if($info[$wsidx]['nManagedCharge'] <> 0)
    {
        $cellrow++;
        $objPHPExcel->getActiveSheet()
            ->setCellValue('A'.$cellrow, '매니지드비용')
            ->setCellValue('B'.$cellrow, $info[$wsidx]['nManagedCharge'])
        ;
        $objPHPExcel->getActiveSheet()->mergeCells('B'.$cellrow.':F'.$cellrow);
    }


    if($info[$wsidx]['nVatPrice']<>0)
    {
        $cellrow++;
        $objPHPExcel->getActiveSheet()
            ->setCellValue('A'.$cellrow, '부가세')
            ->setCellValue('B'.$cellrow, $info[$wsidx]['nVatPrice'])
        ;
        $objPHPExcel->getActiveSheet()->mergeCells('B'.$cellrow.':F'.$cellrow);
    }

    $cellrow++;
    $objPHPExcel->getActiveSheet()
        ->setCellValue('A'.$cellrow, '총 합계')
        ->setCellValue('B'.$cellrow, $lastCharge)
    ;
    $objPHPExcel->getActiveSheet()->getStyle('B'.$cellrow)->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->mergeCells('B'.$cellrow.':F'.$cellrow);


    $objPHPExcel->getActiveSheet()->getStyle('A'.($cellrow-3).':A'.$cellrow)->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('A'.($cellrow-3).':A'.$cellrow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFD9D9D9');
    $objPHPExcel->getActiveSheet()->getStyle('A5:F'.$cellrow)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$chkborderrow.':F'.$chkborderrow)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
    $objPHPExcel->getActiveSheet()->getStyle('B6:F'.$cellrow)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA);


    $cellrow++;
    $cellrow++;
    $cellrow++;




    # 2)리소스별 상세 내용
    foreach($res as $res_row)
    {
        # 2-1) label
        $cellrow++;
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$cellrow, '리소스별 상세내역('.$res_row['name'].')');
        $objPHPExcel->getActiveSheet()->getStyle('A'.$cellrow)->getFont()->setSize(11);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$cellrow)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells('A'.$cellrow.':F'.$cellrow);

        foreach ($res_row['listkey'] as $key=>$res_listkey_row)
        {
            $res_row['listkey'][$key] = explode("||", $res_listkey_row);
        }
        unset($key, $res_listkey_row);


        # 2-2) theade
        $cellrow++;
        $start_cellrow = $cellrow;
        foreach($res_row['colname'] as $key=>$res_colname_row)
        {
            $objPHPExcel->getActiveSheet()->setCellValue($res_row['listkey'][$key][1].$cellrow, $res_colname_row);
            if($res_row['listkey'][$key][2]<>''){
                $objPHPExcel->getActiveSheet()->mergeCells($res_row['listkey'][$key][1].$cellrow.':'.$res_row['listkey'][$key][2].$cellrow);
            }

        }
        unset($key, $res_colname_row);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$cellrow.':F'.$cellrow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);



        # 2-2) tbody
        if(count($res_row['list']) > 0)
        {
            foreach($res_row['list'] as $key=>$res_list_row)
            {
                $cellrow++;
                foreach ($res_row['listkey'] as $res_listkey_row)
                {
                    # 클라우드운영G 요청으로 Supreme IOPS 표시 추가 적용 20201116 SYZ
                    $res_list_val  = $res_list_row[$res_listkey_row[0]];
                    $res_list_val .= ($res_listkey_row[0]=='size' && $res_list_row['product']=='Supreme') ? ' / '.number_format($res_list_row['mxiops']).' IOPS' : '';

                    $objPHPExcel->getActiveSheet()->setCellValue($res_listkey_row[1].$cellrow, $res_list_val);
                    unset($res_list_val);


                    if($res_listkey_row[0]=='price'){
                        $objPHPExcel->getActiveSheet()->getStyle($res_listkey_row[1].$cellrow)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA);
                    }

                    if($res_listkey_row[2]<>''){
                        $objPHPExcel->getActiveSheet()->mergeCells($res_listkey_row[1].$cellrow.':'.$res_listkey_row[2].$cellrow);
                    }
                }
            }
            unset($key, $res_list_row,  $res_listkey_row);
        }
        else{
            $cellrow++;
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$cellrow, '※ 사용중인 '.$res_row['name'].' 상품이 없습니다.');
            $objPHPExcel->getActiveSheet()->getStyle('A'.$cellrow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$cellrow.':F'.$cellrow);
        }

        # 2-3) tfoot
        $cellrow++;
        $chkborderrow = $cellrow;
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$cellrow, '기준 금액 합계')->setCellValue('F'.$cellrow, $res_row['price']);
        $objPHPExcel->getActiveSheet()->getStyle('F'.$cellrow)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA);
        $objPHPExcel->getActiveSheet()->mergeCells('A'.$cellrow.':E'.$cellrow);
        $cellrow++;
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$cellrow, '조정, 할인 금액 합계')->setCellValue('F'.$cellrow, $res_row['bdiscount']+$res_row['adjcharge']+$res_row['discount']);
        $objPHPExcel->getActiveSheet()->getStyle('F'.$cellrow)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA);
        $objPHPExcel->getActiveSheet()->mergeCells('A'.$cellrow.':E'.$cellrow);
        $cellrow++;
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$cellrow, '청구 금액')->setCellValue('F'.$cellrow, $res_row['price'] + $res_row['bdiscount']+$res_row['adjcharge']+$res_row['discount']);
        $objPHPExcel->getActiveSheet()->getStyle('F'.$cellrow)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA);
        $objPHPExcel->getActiveSheet()->mergeCells('A'.$cellrow.':E'.$cellrow);
        $objPHPExcel->getActiveSheet()->getStyle('A'.($cellrow-2).':A'.$cellrow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);



        $objPHPExcel->getActiveSheet()->getStyle('A'.$start_cellrow.':F'.$start_cellrow)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start_cellrow.':F'.$start_cellrow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFD9D9D9');
        $objPHPExcel->getActiveSheet()->getStyle('A'.$start_cellrow.':F'.$cellrow)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$chkborderrow.':F'.$chkborderrow)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);


        $cellrow++;
        $cellrow++;
    }

    for($i=1;$i<$cellrow; $i++)
    {
        $objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(17);
    }


    # 3) worksheet 이름 설정
    $sheet_title = (strlen ($info[$wsidx]['sName']) > 20) ? mb_substr($info[$wsidx]['sName'],0,15).'...' : $info[$wsidx]['sName'];
    $objPHPExcel->getActiveSheet()->setTitle($sheet_title);
}

#------------------------------------------------------------------------------------------------------------------------------
#      마무리.... ^^;;chgPnumber
#------------------------------------------------------------------------------------------------------------------------------
# 엑셀 실행시 default work sheet 설정.
$this->excel->setActiveSheetIndex(0);


header('Content-Type: application/xlsx'); //mime type
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache


//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->setIncludeCharts(TRUE);
//force user to download the Excel file without writing it to server's HD
ob_end_clean();
// die;


$objWriter->save('php://output');


