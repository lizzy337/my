 <?php header('HTTP/1.0 404 Not Found'); ?>
<!DOCTYPE html>
<html lang="En">
<head>
	<title>404 Page Not Found</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta name="Title" content="KINX INC. - Korea Internet Neutral eXchange" />
	<meta name="Subject" content="{cfg.meta}" />
	<meta name="Keywords" content="{cfg.meta}" />
	<meta name="Author" content="KINX INC." />
	<meta name="Classification" content="IX, CDN, CLOUD, IDC, Server Hosting, Colocation, Internetnetworking" />
	
	<!-- link rel="stylesheet" type="text/css" href="/styles/common.css" /-->
	<!--[if IE 7]><link rel="stylesheet" type="text/css" href="/styles/ie7.css" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="/styles/jquery_ui_theme/jquery-ui.css" />
	<link rel="stylesheet" type="text/css" href="/styles/myencommon.css" />
	
	<link rel="shortcut icon" href="/images/kinx.ico" type="image/x-ico"/ >
	
	
	<!--[if lt IE 9]> 
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> 
	<![endif]--> 
	
	<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="//code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
	<script src="/scripts/jquery.form.js"></script>
    <script src="/scripts/jquery.ui.datepicker-ko.js"></script>
    <script src="/scripts/common.js"></script>
	
	<script type="text/javascript" src="/scripts/jquery.validate.js"></script>
	<script src="/scripts/formcheck.js"></script>
	<script src="/scripts/my_payment.js"></script>

	<script>//<![CDATA[
		var url_suffix = "{url_suffix}";
	//]]></script>

</head>
<body>
<div id="wrap">
	<div id="container">

		<div class="error">			
			<ul>
				<li class="tit">404 Page Not Found</li>
				<li class="txt">The page you requested was not found.</li>
				<li class="txt2"></li>
				<li class="link"><a href="javascript:history.back()">Go to the previous page</a>&nbsp; | &nbsp;<a href="/">Go to main page</a></li>
			</ul>
		</div>
	</div>
	<!-- //container -->
	
	<div id="cscenter">
	<div class="cs_box">
    	<span class="tit bold">Customer Center</span>
        <span class="info"><img class="icon" src="/images/my/icon_tel.png">+82-2-526-0900</span> <span class="lgn_txt3">Service consulting(ext.0) Technical supports(ext.2)</span>
        <span class="info"><img class="icon" src="/images/my/icon_mail.png"><a href="mailto:tech@kinx.net">tech@kinx.net</a></span>
    </div>
</div>
	
	<footer id="myfooter">
	<div class="copy">			
		<div class="txt bold">ⓒKINX Inc. All Rights Reserved.</div>
	</div>
	</footer>
</div>
<!-- end wrap -->
 
</body></html>