 <?php header('HTTP/1.0 404 Not Found'); ?>
<!DOCTYPE html>
<html lang="ko">
<head>
	<title>404 Page Not Found</title>
	<meta charset="utf-8" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta name="Title" content="(주)케이아이엔엑스 - Korea Internet Neutral eXchange" />
	<meta name="Subject" content="{cfg.meta}" />
	<meta name="Keywords" content="{cfg.meta}" />
	<meta name="Author" content="(주)케이아이엔엑스" />
	<meta name="Classification" content="회선사업자, IX, CDN, 클라우드, IDC, 코로케이션, 서버호스팅" />
	
	<!-- link rel="stylesheet" type="text/css" href="/styles/common.css" /-->
	<!--[if IE 7]><link rel="stylesheet" type="text/css" href="/styles/ie7.css" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="/styles/jquery_ui_theme/jquery-ui.css" />
	<link rel="stylesheet" type="text/css" href="/styles/mycommon.css" />
	
	<link rel="shortcut icon" href="/images/kinx.ico" type="image/x-ico"/ >
	
	<!--[if lt IE 9]> 
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> 
	<![endif]--> 
	
	<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="//code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
	<script src="/scripts/jquery.form.js"></script>
    <script src="/scripts/jquery.ui.datepicker-ko.js"></script>
    <script src="/scripts/common.js"></script>
	
	<script type="text/javascript" src="/scripts/jquery.validate.js"></script>
	<script src="/scripts/formcheck.js"></script>
	<script src="/scripts/my_payment.js"></script> 

	<script>//<![CDATA[
		var url_suffix = "{url_suffix}";
	//]]></script>

</head>
<body>
<div id="wrap">
	<div id="container">
		<div class="error">			
			<ul>
				<li class="tit">오류(404)</li>
				<li class="txt">찾으려는 페이지가 없습니다.</li>
				<li class="txt2">동일한 문제가 지속될 경우 운영센터에 문의해주십시오.</li>
				<li class="link"><a href="javascript:history.back()">이전페이지</a>&nbsp; | &nbsp;<a href="/">홈으로</a></li>
			</ul>
		</div>
	</div>
	<!-- //container -->
	
	<div id="cscenter">
		<div class="cs_box">
	    	<span class="tit bold">고객센터</span>
	        <span class="info"><img class="icon" src="/images/my/icon_tel.png">02.526.0900</span> <span class="sub">(서비스문의: 내선1 / 기술지원: 내선2)</span>
	        <span class="info"><img class="icon" src="/images/my/icon_mail.png"><a href="mailto:tech@kinx.net">tech@kinx.net</a></span>
	    </div>
	</div>
	<footer id="myfooter">
		<div class="copy">			
			<div class="txt bold">Copyright &copy; 2014 KINX INC. All right reserved.</div>
		</div>
	</footer>
</div><!-- end wrap -->

</body></html>