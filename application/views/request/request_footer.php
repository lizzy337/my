                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">reCAPTCHA</label>
                                <div class="col-sm-10" id="chk-auto-captcha">
                                    <div id="captcha_element" class="g-recaptcha"></div>
                                </div>
                            </div>
                        </div>
                        <div class="ibox-footer text-center">
                            <button type="button" class="btn btn-w-m btn-primary kinx-button-style btn-request" ><i class="fa fa-check-square-o"></i> Request</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div><!-- end page-wrapper -->
</div><!--  end wrapper -->
<div class="footer">
    <div>
        <div style="text-align:center;">ⓒKINX Inc. All Rights Reserved.</div>
    </div>
</div>
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js?hl=en&onload=onloadCallback&render=explicit'></script>
<script  src='/scripts/idcrequest/idcrequest.js'></script>

</body></html>