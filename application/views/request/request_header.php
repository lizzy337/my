<!DOCTYPE html>

<html><head>
<title>KINX (Korea Internet Neutral eXchange) 세상으로 향하는 인터넷의 큰 길</title>
<link rel="icon" type="image/x-icon" href="https://www.kinx.net/kinx_32x32.ico">
<link rel="shortcut icon" type="image/x-icon" href="https://www.kinx.net/kinx_32x32.ico">
<link rel="apple-touch-icon-precomposed" href="https://www.kinx.net/wp-content/uploads/2016/11/kinx152x152.png">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
<link href="/styles/bootstrap/bootstrap.min.css" rel="stylesheet">
<link href="/styles/idcrequest/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="/styles/idcrequest/animate.css" rel="stylesheet">
<link href="/styles/idcrequest/style.css" rel="stylesheet">
<link href="/styles/idcrequest/idcrequest.css" rel="stylesheet">
<link href="/scripts/vendor/sweetalert/dist/sweetalert.css" rel="stylesheet">

</head>

<body class=" pace-done"><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
  <div class="pace-progress-inner"></div>
</div>
<div class="pace-activity"></div></div>
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg" style="margin-left:0px !important; margin-top:50px !important;">
        <div class="row border-bottom">
            <nav class="navbar navbar-fixed-top" role="navigation" style="margin-bottom: 0;background-color:#FFF">
                <div class="navbar-header" style="width:100%">
                    <div class="icon"><i class="fa fa-pencil-square-o" style="line-height:28px"></i></div> <div><strong><?=$title ?></strong></div>
                    <div style="float:right"><img src="/images/kinx_logo.png" style="width:80px" alt="kinx"></div>
                </div>
            </nav>
        </div>

        <div class="div-h15"></div>
        <div class="alert alert-success">
            <?=$notice ?>
        </div>

        <form class="form-horizontal" id="request_form" name="request_form">
            <!-- Requestor information -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3><i class="fa fa-caret-right" style="padding-right:5px"></i> Requestor information <small>Entry required</small></h3>
                        </div>
                        <div class="ibox-content">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Company Name</label>
                                <div class="col-sm-4"><input type="text" class="form-control input-sm" name="companyname" data-parsley-required="true"></div>
                                <label class="col-sm-2 control-label">Requestor Name</label>
                                <div class="col-sm-4"><input type="text" class="form-control input-sm" name="requestorname" data-parsley-required="true"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">E-mail</label>
                                <div class="col-sm-4"><input type="email" class="form-control input-sm" name="email" data-parsley-required="true"></div>
                                <label class="col-sm-2 control-label">Phone number</label>
                                <div class="col-sm-4"><input type="text" class="form-control input-sm" name="phonenumber" data-parsley-required="true"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Detail requirements -------------------------------------------------------------------------------------------------------- -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h3><i class="fa fa-caret-right" style="padding-right:5px"></i> <?=$ctitle; ?></h3>
                        </div>
                        <div class="ibox-content">