<?php if($type=='cross_connect'): ?>
<table style="border: 1px solid #EBEBEB;width:100%;max-width:100%;margin-bottom:20px;background-color:transparent;border-spacing:0;border-collapse:collapse;display: table;">
    <thead style="display:table-header-group;vertical-align: middle;border-color:inherit;">
        <tr style="display: table-row;vertical-align:inherit;border-color:inherit;">
            <th style="border-top: 0;padding: 14px 8px;color: #888;text-align:center;border-bottom-width:1px;line-height:1.42857;vertical-align:middle;border-bottom:1px solid #DDDDDD;border: 1px solid #e7e7e7;background-color: #F5F5F6;position: static;display: table-cell;float: none;width: 25%;">Category</th>
            <th style="border-top: 0;padding: 14px 8px;color: #888;text-align:center;border-bottom-width:1px;line-height:1.42857;vertical-align:middle;border-bottom:1px solid #DDDDDD;border: 1px solid #e7e7e7;background-color: #F5F5F6;position: static;display: table-cell;float: none;width: 16.66666667%;">Classfication</th>
            <th style="border-top: 0;padding: 14px 8px;color: #888;text-align:center;border-bottom-width:1px;line-height:1.42857;vertical-align:middle;border-bottom:1px solid #DDDDDD;border: 1px solid #e7e7e7;background-color: #F5F5F6;position: static;display: table-cell;float: none;width: 58.33333333%;">Detail</th>
        </tr>
    </thead>
    <tbody style="display: table-row-group;vertical-align: middle;border-color: inherit;">
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;" rowspan="2">Demarcation Point</th>
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Rack #</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$cc_input01;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Port</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$cc_input02;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;" rowspan="3">Cable / Transceiver Type</th>
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Optic or UTP</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$cc_input03;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Single or multi</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$cc_input04;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">LC or SC</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$cc_input05;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;" rowspan="3">Contact Information(Tech)</th>
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Technician name</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$cc_input06;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Technician email</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$cc_input07;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Technician direct number</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$cc_input08;?></td>
        </tr>
    </tbody>
</table>
<?php elseif($type=="datacenter_tour"): ?>
<table style="border: 1px solid #EBEBEB;width:100%;max-width:100%;margin-bottom:20px;background-color:transparent;border-spacing:0;border-collapse:collapse;display: table;">
    <thead style="display:table-header-group;vertical-align: middle;border-color:inherit;">
        <tr style="display: table-row;vertical-align:inherit;border-color:inherit;">
            <th style="border-top: 0;padding: 14px 8px;color: #888;text-align:center;border-bottom-width:1px;line-height:1.42857;vertical-align:middle;border-bottom:1px solid #DDDDDD;border: 1px solid #e7e7e7;background-color: #F5F5F6;position: static;display: table-cell;float: none;width: 16.66666667%;">Category</th>
            <th style="border-top: 0;padding: 14px 8px;color: #888;text-align:center;border-bottom-width:1px;line-height:1.42857;vertical-align:middle;border-bottom:1px solid #DDDDDD;border: 1px solid #e7e7e7;background-color: #F5F5F6;position: static;display: table-cell;float: none;width: 25%;">Classfication</th>
            <th style="border-top: 0;padding: 14px 8px;color: #888;text-align:center;border-bottom-width:1px;line-height:1.42857;vertical-align:middle;border-bottom:1px solid #DDDDDD;border: 1px solid #e7e7e7;background-color: #F5F5F6;position: static;display: table-cell;float: none;width: 58.33333333%;">Detail</th>
        </tr>
    </thead>
    <tbody style="display: table-row-group;vertical-align: middle;border-color: inherit;">
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;" colspan="2">Available schedule (Date/hour)</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$dt_input01;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;" rowspan="4">Visitor<br>information 1</th>
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Name</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$dt_input02;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Date of birth</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$dt_input03;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Mobile number</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$dt_input04;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Passport number *</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$dt_input05;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;" rowspan="4">Visitor<br>information 2</th>
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Name</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$dt_input06;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Date of birth</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$dt_input07;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Mobile number</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$dt_input08;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Passport number *</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$dt_input09;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;" rowspan="4">Visitor<br>information 3</th>
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Name</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$dt_input10;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Date of birth</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$dt_input11;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Mobile number</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$dt_input12;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Passport number *</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$dt_input13;?></td>
        </tr>
    </tbody>
</table>
<?php elseif($type=="tech_spec_sheet"): ?>
<table style="border: 1px solid #EBEBEB;width:100%;max-width:100%;margin-bottom:20px;background-color:transparent;border-spacing:0;border-collapse:collapse;display: table;">
    <thead style="display:table-header-group;vertical-align: middle;border-color:inherit;">
        <tr style="display: table-row;vertical-align:inherit;border-color:inherit;">
            <th style="border-top: 0;padding: 14px 8px;color: #888;text-align:center;border-bottom-width:1px;line-height:1.42857;vertical-align:middle;border-bottom:1px solid #DDDDDD;border: 1px solid #e7e7e7;background-color: #F5F5F6;position: static;display: table-cell;float: none;width: 16.66666667%;">Category</th>
            <th style="border-top: 0;padding: 14px 8px;color: #888;text-align:center;border-bottom-width:1px;line-height:1.42857;vertical-align:middle;border-bottom:1px solid #DDDDDD;border: 1px solid #e7e7e7;background-color: #F5F5F6;position: static;display: table-cell;float: none;width: 25%;">Classfication</th>
            <th style="border-top: 0;padding: 14px 8px;color: #888;text-align:center;border-bottom-width:1px;line-height:1.42857;vertical-align:middle;border-bottom:1px solid #DDDDDD;border: 1px solid #e7e7e7;background-color: #F5F5F6;position: static;display: table-cell;float: none;width: 58.33333333%;">Detail</th>
        </tr>
    </thead>
    <tbody style="display: table-row-group;vertical-align: middle;border-color: inherit;">
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;" rowspan="3">Rack</th>
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Qty of racks</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$ts_input01;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">size</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$ts_input02;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Power supply per rack</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$ts_input03;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;" rowspan="2">power</th>
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Total power</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$ts_input04;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Electric power supply method</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$ts_input05;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;" rowspan="2">option</th>
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">UPS redundancy option</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$ts_input06;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;">Carrier-neutral option</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$ts_input07;?></td>
        </tr>
        <tr style="display: table-row;vertical-align: inherit;border-color: inherit;">
            <th style="text-align: center;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;font-weight: bold;" colspan="2">Other requirements</th>
            <td style="text-align: left;vertical-align: middle;border-top: 1px solid #e7eaec;line-height: 1.42857;padding: 8px;border: 1px solid #e7e7e7;"><?=$ts_input08;?></td>
        </tr>
    </tbody>
</table>
<?php endif; ?>
<br/>



