<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/Common.php");
class Kinxidc extends Common{
	public function __construct() {
		parent::__construct();
		if($this->is_login === false) {
            $this->output->set_status_header('410');
			$this->tinyjs->pageRedirect("/", "로그인 후 접근가능합니다");
            exit;
		}
	}

	public function index() {
		redirect("/kinxidc/competent");
	}

	// 담당자 관리 : 리스트
	public function competent() {
		$this->load->model("company_contact_model");
		$this->_set_sec();
		$secParams = $this->_get_sec();
		$secParams["offset"] = isset($secParams["page"]) ? $secParams["page"] : "0";
		$secParams["limit"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		$secParams["orderby"] = "dtcreateDate DESC";

		//start 업체분류(입주업체-M , 협력업체-C). modify by cyn 20200313
		if(isset($secParams["type"]))
		{
			if($secParams["type"]=='M' || $secParams["type"]=='C')
			{
				$secParams['sCompanyType'] = $secParams["type"];
				$data["sCompanyType"] = $secParams["type"];
			}
			else
			{
				$data["sCompanyType"] = 'A';
			}
		}
		else
		{
			$data['sCompanyType'] = '';
		}
        //-end

		$data["cnt"] = $this->company_contact_model->_select_cnt($secParams);
		$data["list"] =  $this->company_contact_model->_select_list($secParams);

		$pager["CNT"] = $data["cnt"];
		$pager["PRPAGE"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$pagerHtm = $this->_set_pager($pager);

		$data["pager"] = $pagerHtm;
		$data["pagerIdx"] = $data["cnt"] - $secParams["offset"];
		$data["secParams"] = $secParams;

		$this->scripts[] = "my_kinxidc.js";
		$this->_print($data);
	}

	// 담당자 관리 : 상세 정보, 추가하기, 수정하기 폼
	public function competent_write($seq=null) {
		// null 일 경우 추가하기입니다.
		if(is_null($seq)) {
			$mode = "write";
		}
		// 아니면 수정하기임
		else {
			$this->load->model("company_contact_model");
			$where["nCompanyContactSeq"] = decryptIt($seq);
			$data["basic"] = $this->company_contact_model->_select_row($where);
			unset($where);
			if($data["basic"]["nContractSeq"] !== $this->member["nContractSeq"]) {
				$this->tinyjs->pageBack("정상적인 경로로 접근해주세요.");
				return false;
				exit;
			}
			$mode = "modify";
		}

		$data["mode"] = $mode;

		$this->scripts[] = "my_kinxidc.js";

		$this->_print($data);
	}

	// 담당자 관련 처리단
	public function competent_proc($mode=null) {
		$result= array();
		$result['url']="";

		// 담당자 추가 또는 수정
		if($mode === "write" || $mode === "modify")
		{
			$this->load->library("form_validation");
			if($this->form_validation->run("contact"))
			{
				$this->load->model("company_contact_model");
				$data = $this->input->post(NULL, TRUE);
				unset($data['privacyassent']);

				$data['sCompanyName']    = chg_xss_quot($data['sCompanyName']);
				$data['sManagerName']    = chg_xss_quot($data['sManagerName']);
				$data['sDepartmentName'] = chg_xss_quot($data['sDepartmentName']);
				$data['sRank']           = chg_xss_quot($data['sRank']);

				$data['sMobilePhone']    = format_phone($data['sMobilePhone']);
				$data['sInternalPhone']  = format_phone($data['sInternalPhone']);
				$data['nCompanySeq']     = $this->member["nCompanySeq"];

				if($mode === "write") {
					$data["nContractSeq"] = $this->member["nContractSeq"];
					$this->company_contact_model->_insert($data);
					$result['code'] = 's';
					$result['msg'] = "추가가 완료되었습니다.";
				} else if ($mode === "modify") {
					if($data["nContractSeq"] !== $this->member["nContractSeq"]) {
						$result['code'] = 'f';
						$result['msg'] ="정상적인 경로로 접근해주세요.";
					}
					$where["nCompanyContactSeq"] = $data["nCompanyContactSeq"];
					unset($data["nCompanyContactSeq"]);
					$this->company_contact_model->_update($data, $where);
					$result['code'] = 's';
					$result['msg'] = "수정이 완료되었습니다.";
				}

				$result['url'] = "/kinxidc/competent";
			}
			else
			{
				$result['code'] = 'f';
				$result['msg'] =strip_tags(validation_errors());
			}
		}
		// 담당자 삭제
		else if ($mode === "delete") {
			$seq = $this->uri->rsegment(4);
			if($seq) {
				$seq = decryptIt($seq);//150316
				$this->load->model("company_contact_model");
				$where["nCompanyContactSeq"] = $seq;
				$data["basic"] = $this->company_contact_model->_select_row($where);

				if($data["basic"]["nContractSeq"] !== $this->member["nContractSeq"]) {
					$result['code'] = 'f';
					$result['msg'] ="정상적인 경로로 접근해주세요.";
				}

				$where["nCompanyContactSeq"] = $data["basic"]["nCompanyContactSeq"];
				$this->company_contact_model->_delete($where);
				$result['code'] = 's';
				$result['msg'] = "삭제가 완료되었습니다.";
				$result['url'] = "/kinxidc/competent";
			}
			else
			{
				$result['code'] = 'f';
				$result['msg'] ="정상적인 경로로 접근해주세요.";
			}
		}

		echo(json_encode($result));
		exit;
	}

	// 방문신청 비지트 어플리케이션 : 리스트
	public function visitapp()
	{
		$this->scripts[] = "my_visitapp.js";
		$this->scripts[] = "my_utility.js";
		$this->load->model("work_request_model");
		$this->load->model("company_visitor_model");
		$this->load->model("company_contact_model");
		$this->_set_sec();
		$secParams = $this->_get_sec();

		$secParams["offset"]       = isset($secParams["page"]) ? $secParams["page"] : "0";
		$secParams["limit"]        = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$secParams["orderby"]      = "dtcreateDate DESC";
		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		//$secParams["nServiceType"] = $this->member["sServiceType"];
		$secParams["sActionType"]  = "VST"; //고객방문
		$secParams['oType']        = "desc";
		$secParams['oKey']         = "dtCreateDate";

		// 오라클(AC-00008D9) 회원의 경우 방문신청 이력을 1년치만 노출 시킨다. 영업2Group 이성주M 요청 20221219
		if($this->member["nContractSeq"] ==2265) {
			$secParams['dtCreateDate'] = date('Y-m-d', strtotime("-1 year", time()));
		}

		$data["cnt"] = $this->work_request_model->_select_cnt($secParams);
		$data["list"] =  $this->work_request_model->_select_list($secParams);
		//print_r($data["list"]);

		$pager["CNT"] = $data["cnt"];
		$pager["PRPAGE"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$pagerHtm = $this->_set_pager($pager);

		$data["pager"] = $pagerHtm;
		#$data["pagerIdx"] = $data["cnt"] - ($secParams["offset"]*$pager["PRPAGE"]);
        $data["pagerIdx"] = $data["cnt"] - ($pager["PRPAGE"]);
		$data["secParams"] = $secParams;
		//start 160603
		//$data["competentList"] =  $this->company_contact_model->_select_list($secParams);
		$data["competentList"] =  $this->company_contact_model->_select_list( );//160603
		//end 160603
		$data["chkmsg"] = (empty($data["competentList"])) ? "※ 현재 등록된 담당자가 없습니다. 담당자관리 페이지에서 담당자 등록 후 방문신청을 하실 수 있습니다." : '';

        //echo "<br /> pageridx ";print_r($data["pagerIdx"]);
		//echo "<br /> PRPAGE ";print_r($pager["PRPAGE"]);
		//$temp = $data["pagerIdx"] - $pager["PRPAGE"];
		//echo "<br /> temp ";print_r($temp);

		//160603
		//for($i=0; $i < $data["pagerIdx"] && $i < $pager["PRPAGE"]; $i++) //before
		//수정
		$listcnt = count($data["list"]);
		if(0 < $data["pagerIdx"] && 0< $pager["PRPAGE"])
		{
			for($i=0; $i < $listcnt ; $i++)
			{
				unset($secParams);
	            $data['nCntVisitor'] = 0;
	            $data['topVisitor'] = '-';

	            $secParams["nWorkRequestSeq"] = $data["list"][$i]["nWorkRequestSeq"];
				$data["nCntVisitor"] = $this->company_visitor_model->_select_cnt($secParams);
				$data["topVisitor"] = $this->company_visitor_model->_select_row($secParams);

				$data["list"][$i]["nCntVisitor"] = $data["nCntVisitor"];
				$data["list"][$i]["sTopVisitorName"] = $data["topVisitor"]["sVisitorName"];
			}
		}

		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		$data["competentList"] =  $this->company_contact_model->_select_list($secParams);

		$this->_print($data);
	}

	public function json($mode=null) {
		if($mode == "registant") {
			$this->load->model("company_contact_model");
			$data = $this->input->post();
			$secParams["nCompanyContactSeq"] = decryptIt($data["nCompanyContactSeq"]);//150313 decryptIt 추가

			//$secParams["nCompanySeq"] = $data["nCompanyContactSeq"]; //2014-05-11 nykim modify
			$data["registant"] = $this->company_contact_model->_select_row($secParams);


			$json = array("sCompanyName"=>$data["registant"]["sCompanyName"],
				"sManagerName"=>$data["registant"]["sManagerName"],
				"sInternalPhone"=>$data["registant"]["sInternalPhone"],
				"sMobilePhone"=>$data["registant"]["sMobilePhone"],
				"sRank"=>$data["registant"]["sRank"],
				"sfax"=>$data["registant"]["sfax"],
				"sEmail"=>$data["registant"]["sEmail"]);
			echo json_encode($json);
		}
	}

	public function jsonbyInit($mode=null) { //150313 decryptIt 없는  추가
		if($mode == "registant") {
			$this->load->model("company_contact_model");
			$data = $this->input->post();
			$secParams["nCompanyContactSeq"] = $data["nCompanyContactSeq"];

			//$secParams["nCompanySeq"] = $data["nCompanyContactSeq"]; //2014-05-11 nykim modify
			$data["registant"] = $this->company_contact_model->_select_row($secParams);


			$json = array("sCompanyName"=>$data["registant"]["sCompanyName"],
					"sManagerName"=>$data["registant"]["sManagerName"],
					"sInternalPhone"=>$data["registant"]["sInternalPhone"],
					"sMobilePhone"=>$data["registant"]["sMobilePhone"],
					"sRank"=>$data["registant"]["sRank"],
					"sfax"=>$data["registant"]["sfax"],
					"sEmail"=>$data["registant"]["sEmail"]);
			echo json_encode($json);
		}
	}

	// 방문신청 작성, 변경
	public function visitapp_write($seq=null) {
		$this->scripts[] = "my_visitalert";
		$this->scripts[] = "my_visitapp.js";
		$this->load->model("company_contact_model");
		$this->load->model("code_model");
		$this->_set_sec();
		$secParams = $this->_get_sec();
		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		$data["competentList"] =  $this->company_contact_model->_select_list($secParams);
		$data["workTypeList"] = $this->code_model->get_code_list_array('T0001', FALSE, null);
		$data["idcList"] = $this->code_model->get_code_list_array('T0013', FALSE, null);//방문위치

		//if(empty($data["competentList"])) jsError("현재 등록된 담당자가 없습니다.\\n담당자관리 페이지에서 담당자 등록 후 방문신청을 하실 수 있습니다.");
		if(empty($data["competentList"]))
		{
			$this->tinyjs->alert("현재 등록된 담당자가 없습니다.\\n담당자관리 페이지에서 담당자 등록 후 방문신청을 하실 수 있습니다.");
			$this->tinyjs->pageRedirect("/kinxidc/visitapp");
			return ;
		}

		// null 일 경우 추가하기입니다.
		if(is_null($seq))
		{
			$mode = "write";
			$data["basic"]["dtExpectStartDate"] = date('Y-m-d');
			$data["basic"]["dtExpectEndDate"] = date('Y-m-d');
		}
		else  // 아니면 수정하기임
		{
			$this->load->model("work_request_model");
			$seq = decryptIt($seq);//150313
			$where["nWorkRequestSeq"] = $seq;
			$data["basic"] = $this->work_request_model->_select_row($where);

			unset($where);

			if($data["basic"]["nContractSeq"] !== $this->member["nContractSeq"]) {
				$this->tinyjs->pageBack("정상적인 경로로 접근해주세요.");
				return false;
				exit;
			}
			$data["basic"]["dtExpectStartTime"] = date('H', strtotime($data["basic"]["dtExpectStartDate"]));
			$data["basic"]["dtExpectEndTime"] = date('H', strtotime($data["basic"]["dtExpectEndDate"]));
			$data["basic"]["dtExpectStartDate"] = date('Y-m-d', strtotime($data["basic"]["dtExpectStartDate"]));
			$data["basic"]["dtExpectEndDate"] = date('Y-m-d', strtotime($data["basic"]["dtExpectEndDate"]));

			//210611 취약점 조치사항
			$data["basic"]["nManagerSeq"] = encryptIt($data["basic"]["nManagerSeq"]);
			//


			//-start 20140903 lizzy. 정보통신기기 반입 신청 - 기존 내용 있으면 나타내기
			if( $data["basic"]["sResultStatus"] == "F")//신청완료시는 check가 아닌 그냥 text로 나타내기
			{
				if( strlen($data["basic"]["sToolInfo"]) > 0 )
				{
					$strTok = explode('|',$data["basic"]["sToolInfo"]);
					$strInfo = "";
					$cnt = count($strTok);
					for($i = 0 ; $i<$cnt ; $i++)
					{
						switch($strTok[$i])
						{
						case 'N':
							$strInfo .= "노트북PC";
							break;
						case 'T':
							$strInfo .= "스마트폰/테블릿";
							break;
						case 'U':
							$strInfo .= "USB/HDD";
							break;
						}
						if( $i < $cnt-1 ) $strInfo .= ", ";
					}
					$data["basic"]['sToolInfo'] = $strInfo;
				}else{
					$data["basic"]['sToolInfo'] = "-";
				}
			}
			else
			{
				$data["basic"]["tool_notebook"] = "";
				$data["basic"]["tool_tablet"] = "";
				$data["basic"]["tool_usb"] = "";
				if( strlen($data["basic"]["sToolInfo"]) > 0 )
				{
					$strTok = explode('|',$data["basic"]["sToolInfo"]);
					$cnt = count($strTok);
					for($i = 0 ; $i<$cnt ; $i++)
					{
						switch($strTok[$i])
						{
						case 'N':
							$data["basic"]["tool_notebook"] = "checked=checked";
							break;
						case 'T':
							$data["basic"]["tool_tablet"] = "checked=checked";
							break;
						case 'U':
							$data["basic"]["tool_usb"] = "checked=checked";
							break;
						}
					}
				}
			}
			//-end

			// 방문자들..
			$this->load->model("company_visitor_model");
			$where["nWorkRequestSeq"] = $seq;
			$data["visitorList"] = $this->company_visitor_model->_select_list($where);

			for($i=0; $i < count($data["competentList"]); $i++) {
				foreach($data["visitorList"] as $visitor) {
					if($visitor["nCompanyContactSeq"] == $data["competentList"][$i]["nCompanyContactSeq"]) {
						$data["competentList"][$i]["checked"] = "Y";
						break;
					}
				}
			}
			$data["topVisitor"] = $this->company_visitor_model->_select_row($where);

			unset($where);
			$mode = "modify";
		}

		$data["mode"] = $mode;

		//debug_var($data);
		//echo "visit app ";print_r($data["basic"]); die;

		$this->scripts[] = "my_kinxidc.js";
		$this->_print($data);
                exit;
	}

	// 방문신청서 처리
	public function visitapp_proc($mode=null)
	{
		$result= array();
		$result['url']="";
		// 방문 신청 또는 수정
		if($mode === "write" || $mode === "modify")
		{
			$this->load->library("form_validation");
			$validResult = false;
			if($mode === "write")
			{
				$validResult = $this->form_validation->run("visitapp");
			}
			else if($mode === "modify")
			{
				$validResult = $this->form_validation->run("visitapp_modify");
			}
			//var_dump($validResult);

			//if($this->form_validation->run("visitapp"))
			if($validResult) //등록 / 수정 일때 validation 조건 나눔. 140903 lizzy
			{
				$this->load->model("company_visitor_model");
				$this->load->model("work_request_model");
				$data = $this->input->post();
                //var_dump($data);die;
				$data["nManagerSeq"] = decryptIt($data["nManagerSeq"]);//150313
				//var_dump($data);
                               // die;

				$this->load->model("company_contact_model");
				$secParam["nCompanyContactSeq"] = $data["nManagerSeq"];
				$resitant =  $this->company_contact_model->_select_row($secParam); // 등록자 정보를 가져온다.
				unset($secParam);

				$data["dtExpectStartDate"] .= " ";
				$data["dtExpectStartDate"] .= $data["dtExpectStartTime"];
				$data["dtExpectStartDate"] .= ":00:00";
				$data["dtExpectEndDate"] .= " ";
				$data["dtExpectEndDate"] .= $data["dtExpectEndTime"];
				$data["dtExpectEndDate"] .= ":00:00";

				if($data["dtExpectStartDate"] > $data["dtExpectEndDate"])
				{
					$result['code'] = 'f';
					$result['msg'] ="방문예정일시와 예상종료일시를 확인해 주세요.";
				}
				else
				{

					$secParam = $data;
					// write, update 공통
					$secParam["sCompanyName"] = $resitant["sCompanyName"];
					$secParam["sManagerName"] = $resitant["sManagerName"];
					$secParam["sDepartmentName"] = $resitant["sDepartmentName"];
					$secParam["sInternalPhone"] = $resitant["sInternalPhone"];
					$secParam["sMobilePhone"] = $resitant["sMobilePhone"];
					$secParam["sRank"] = $resitant["sRank"];
					$secParam["sfax"] = $resitant["sfax"];
					$secParam["sEmail"] = $resitant["sEmail"];
					unset($secParam["nCompanyContactSeq"]);
					//unset($secParam["nWorkType"]);
					unset($secParam["dtExpectStartTime"]);
					unset($secParam["dtExpectEndTime"]);
					unset($secParam["visit_alert"]);//db에 넣지 않음 20200224  by lizzy

					if($mode === "write")
					{
						$secParam["nContractSeq"] = $this->member["nContractSeq"];
						$secParam["nCompanySeq"] = $this->member["nCompanySeq"];
						$secParam["nServiceType"] = $this->member["sServiceType"];
						$secParam["sActionType"] = "VST";
						$secParam["sResultStatus"] = "Y";

						//-start 140901 lizzy
						if(!empty($data["sTooluse"])){
							$secParam["sTooluse"] = $data["sTooluse"];
						}
						if(isset($data["tool_info"]))
						{
							if(count($data["tool_info"]) > 0 ){
								$info = "";
								$cnt = count($data["tool_info"]);
								for($i=0; $i <$cnt ; $i++ ){
									$info .= $data["tool_info"][$i];
									if( $i < $cnt-1 ) $info .= "|";
								}
								$secParam["sToolInfo"] = $info;
							}
							unset($secParam["tool_info"]);
						}
						//-end
						$data["nWorkRequestSeq"] = $this->work_request_model->_insert($secParam);

						//3. email 발송
						$this->sendmail_work($this->member["sContractName"],$data["nWorkRequestSeq"],$data["nWorkType"],"VST",
											$data["dtExpectStartDate"], $data["dtExpectStartTime"]);
						//방문자 관련 추가 하는 작업
						$msg = "신청이 완료되었습니다.";
						$result['code'] = 's';
						$result['msg'] = $msg;
						$result['url'] = "/kinxidc/visitapp";
					}
					else if ($mode === "modify")
					{
						if($data["nContractSeq"] !== $this->member["nContractSeq"])
						{
							$result['code'] = 'f';
							$result['msg'] ="정상적인 경로로 접근해주세요.";
						}
						//-start 140903

						if(isset($data["tool_info"]))
						{
							if(count($data["tool_info"]) > 0 ){
								$info = "";
								$cnt = count($data["tool_info"]);
								for($i=0; $i <$cnt ; $i++ ){
									$info .= $data["tool_info"][$i];
									if( $i < $cnt-1 ) $info .= "|";
								}
								$secParam["sToolInfo"] = $info;
							}
							unset($secParam["tool_info"]);
						}
						else
						{
							$secParam["sToolInfo"] = " ";
						}
						//-end
						$where["nWorkRequestSeq"] = $data["nWorkRequestSeq"];
						$this->work_request_model->_update($secParam, $where);

						$msg = "수정이 완료되었습니다.";
						$result['code'] = 's';
						$result['msg'] = $msg;
						$result['url'] = "/kinxidc/visitapp";

						//방문자 관련 추가 하는 작업
						$this->company_visitor_model->_delete($where);
					}


					foreach($data["nCompanyContactSeq"] as $nCompanyContactSeq) {
						$sContent = $secParam['sContent'];
						unset($secParam);
						$secParam["nCompanyContactSeq"] = $nCompanyContactSeq;
						$contact =  $this->company_contact_model->_select_row($secParam);
						$secParam["nWorkRequestSeq"] = $data["nWorkRequestSeq"];
						$secParam["nContractSeq"] = $contact["nContractSeq"];
						$secParam["nCompanySeq"] = $contact["nCompanySeq"];
						$secParam["sCompanyName"] = $contact["sCompanyName"];
						$secParam["sVisitorName"] = $contact["sManagerName"];
						$secParam["sInternalPhone"] = $contact["sInternalPhone"];
						$secParam["sMobilePhone"] = $contact["sMobilePhone"];
						$secParam["nWorkType"] = $data["nWorkType"];
						$secParam["sContent"] = $sContent;
						$this->company_visitor_model->_insert($secParam);
					}
				}

			}
			else
			{
				$result['code'] = 'f';
				$result['msg'] =strip_tags(validation_errors());
			}
		}

		// 방문 취소
		else if ($mode === "cancel")
		{
			$seq = $this->uri->rsegment(4);

			if($seq)
			{
				$seq = decryptIt($seq);//150316
				$this->load->model("work_request_model");
				$where["nWorkRequestSeq"] = $seq;
				$data["basic"] = $this->work_request_model->_select_row($where);


				if($data["basic"]["nContractSeq"] !== $this->member["nContractSeq"]) {
					$result['code'] = 'f';
					$result['msg'] ="정상적인 경로로 접근해주세요.";
				}

				$where["nWorkRequestSeq"] = $data["basic"]["nWorkRequestSeq"];
				$secParam["sResultStatus"] = "C";
				$this->work_request_model->_update($secParam, $where);
				$msg = "취소가 완료되었습니다.";
				$result['code'] = 's';
				$result['msg'] = $msg;
				$result['url'] = "/kinxidc/visitapp";
			}
			else
			{
				$result['code'] = 'f';
				$result['msg'] ="정상적인 경로로 접근해주세요.";
			}
		}

// 		debug_var($data);
// 		debug_var($result);
// 		die;



		echo(json_encode($result));
		exit;
	}

	// 장비 입반출 exported the equipment 히..히밤 줄일수가 없다.
	public function equipment() {
		$this->load->model("work_request_model");
		$this->load->model("company_contact_model");
		$this->_set_sec();
		$secParams = $this->_get_sec();

		$secParams["offset"] = isset($secParams["page"]) ? $secParams["page"] : "0";
		$secParams["limit"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$secParams["orderby"] = "dtcreateDate DESC";
		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		$secParams["nServiceType"] = $this->member["sServiceType"];
		$secParams["sActionType"] = "EIO"; //장비입반출

		$data["cnt"] = $this->work_request_model->_select_cnt($secParams);
		$data["list"] =  $this->work_request_model->_select_list($secParams);
		$pager["CNT"] = $data["cnt"];
		$pager["PRPAGE"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$pagerHtm = $this->_set_pager($pager);

		$data["pager"] = $pagerHtm;
		$data["pagerIdx"] = $data["cnt"] - $secParams["offset"];
		$data["secParams"] = $secParams;

		$data["secParams"] = $secParams;
		//start 160603
		//$data["competentList"] =  $this->company_contact_model->_select_list($secParams);
		$data["competentList"] =  $this->company_contact_model->_select_list();
		//end
		$data["chkmsg"] = (empty($data["competentList"])) ? "※ 현재 등록된 담당자가 없습니다. 담당자관리 페이지에서 담당자 등록 후 ,신청을 하실 수 있습니다." : '';


		unset($secParams);
		$this->load->model("work_equipment_model");
		for($i=0; $i < count($data["list"]); $i++) {
			$secParams["nWorkRequestSeq"] = $data["list"][$i]["nWorkRequestSeq"];
			$secParams["sWorkType"] = "I";
			$icount = $this->work_equipment_model->_select_cnt($secParams);
			$secParams["sWorkType"] = "O";
			$ocount = $this->work_equipment_model->_select_cnt($secParams);
			if($icount > 0 && $ocount) {
				$workType = "장비 반출/입";
			} else if($icount > 0) {
				$workType = "장비 반입";
			} else if($ocount > 0) {
				$workType = "장비 반출";
			} else {
				$workType = "";
			}
			$data["list"][$i]["workType"] = $workType;
		}

		$this->_print($data);
	}

	// 장비
	public function equipment_write($seq=null) {
		$this->scripts[] = "my_equipment.js";
		$this->load->model("company_contact_model");
		$this->load->model("company_equipment_model");
		$this->_set_sec();
		$secParams = $this->_get_sec();
		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		$data["competentList"] =  $this->company_contact_model->_select_list($secParams);
		//debug_last_query();
		$tempCnt = 0;
		if(isset($data["competentList"]))
		{
			$tempCnt = count($data["competentList"]);
		}

		if(!isset($data["competentList"]) or $tempCnt <= 0)
		{
			$this->tinyjs->alert("현재 등록된 담당자가 없습니다.\\n담당자관리 페이지에서 담당자 등록 후 방문신청을 하실 수 있습니다.");
			$this->tinyjs->pageRedirect("/kinxidc/equipment");
			return ;
		}


		//210611 취약점 조치사항
		for($i = 0 ; $i < $tempCnt ; $i++)
		{
			$data["competentList"][$i]["nCompanyContactSeq_org"] = $data["competentList"][$i]["nCompanyContactSeq"];
			$data["competentList"][$i]["nCompanyContactSeq"] = encryptIt($data["competentList"][$i]["nCompanyContactSeq"]);
		}
		//end


		$secParams["nStatus"] = 0;

		// null 일 경우 추가하기입니다.
		if(is_null($seq))
		{
			$mode = "write";
			$data["basic"]["dtExpectStartDate"] = date('Y-m-d');
			$where = array("sStatus"=>"I", "nCompanySeq"=> $this->member["nCompanySeq"]);
			$data["equipmentList"] = $this->company_equipment_model->_select_list($where);
			//print_r($where);
			//print_r($data); die();
			if(count($data["equipmentList"])>0)
			{
				foreach($data["equipmentList"] as $key=>$val)
				{
					if($val['nQuantity']==1) $data["equipmentList"][$key]['nOption']=$val['nQuantity'];
					else
					{
						$data["equipmentList"][$key]['nOption']='<span class="select quantity" style="width:50px"><select id="#" title="수량">';
						for($i=1,$max=$val['nQuantity'];$i<=$max;$i++)
						{
							$data["equipmentList"][$key]['nOption'] .= "<option value=".$i.">".$i."</option>";
						}
						$data["equipmentList"][$key]['nOption'].="</select></span>";
					}
				}
			}
			unset($where);
		}
		else
		{// 아니면 수정하기임
			$this->load->model("work_request_model");
			$seq = decryptIt($seq);//150313
			$where["nWorkRequestSeq"] = $seq;
			$data["basic"] = $this->work_request_model->_select_row($where);
			unset($where);
			if($data["basic"]["nContractSeq"] !== $this->member["nContractSeq"]) {
				$this->tinyjs->pageBack("정상적인 경로로 접근해주세요.");
				return false;
				exit;
			}
			//210611 취약점 조치사항
			$data["basic"]["nManagerSeq_org"] = $data["basic"]["nManagerSeq"];
			$data["basic"]["nManagerSeq"] = encryptIt($data["basic"]["nManagerSeq"]);
			//-end

			$data["basic"]["dtExpectStartTime"] = date('H', strtotime($data["basic"]["dtExpectStartDate"]));
			$data["basic"]["dtExpectStartDate"] = date('Y-m-d', strtotime($data["basic"]["dtExpectStartDate"]));

			//-start 20140903 lizzy. 정보통신기기 반입 신청 - 기존 내용 있으면 나타내기
			if( $data["basic"]["sResultStatus"] == "F")//신청완료시는 check가 아닌 그냥 text로 나타내기
			{
				if( strlen($data["basic"]["sToolInfo"]) > 0 )
				{
					$strTok = explode('|',$data["basic"]["sToolInfo"]);
					$strInfo = "";
					$cnt = count($strTok);
					for($i = 0 ; $i<$cnt ; $i++)
					{
						switch($strTok[$i])
						{
							case 'N':
								$strInfo .= "노트북PC";
								break;
							case 'T':
								$strInfo .= "스마트폰/테블릿";
								break;
							case 'U':
								$strInfo .= "USB/HDD";
								break;
						}
						if( $i < $cnt-1 ) $strInfo .= ", ";
					}
					$data["basic"]['sToolInfo'] = $strInfo;
				}else{
					$data["basic"]['sToolInfo'] = "-";
				}
			}
			else
			{
				$data["basic"]["tool_notebook"] = "";
				$data["basic"]["tool_tablet"] = "";
				$data["basic"]["tool_usb"] = "";
				if( strlen($data["basic"]["sToolInfo"]) > 0 )
				{
					$strTok = explode('|',$data["basic"]["sToolInfo"]);
					$cnt = count($strTok);
					for($i = 0 ; $i<$cnt ; $i++)
					{
						switch($strTok[$i])
						{
						case 'N':
							$data["basic"]["tool_notebook"] = "checked=checked";
							break;
							case 'T':
							$data["basic"]["tool_tablet"] = "checked=checked";
							break;
							case 'U':
							$data["basic"]["tool_usb"] = "checked=checked";
							break;
						}
					}
				}
			}
			//-end

			unset($where);
			$this->load->model("work_equipment_model");
			$where["nWorkRequestSeq"] = $seq;

			$data["eioList"] = $this->work_equipment_model->_select_list($where);
			//$data["ownList"] = $this->work_equipment_model->_select_list($where);

			$mode = "modify";
		}

		$data["mode"] = $mode;

		//echo "kinx equipment write data['basic']";print_r($data["basic"]);
		//echo "kinx equipment write data['competentList']";print_r($data["competentList"]);
		$this->_print($data);
	}

	// 장비입반출 처리
	public function equipment_proc($mode=null) {
		// 장비입반출 신청 또는 수정
		if($mode === "write" || $mode === "modify")
		{
			$this->load->model("work_request_model");
			$this->load->model("work_equipment_model");
			$data = $this->input->post();
			//var_dump("mod : ".$mode);
			//var_dump($data);die;
			$this->load->model("company_contact_model");
			$secParam["nCompanyContactSeq"] = decryptIt($data["nManagerSeq"]);//150313
			//var_dump("secParam nCompanyContactSeq : ".$secParam["nCompanyContactSeq"] );die;
			$resitant =  $this->company_contact_model->_select_row($secParam); // 등록자 정보를 가져온다.
			//var_dump("resitant : ");var_dump($resitant);die;
			unset($secParam);

			$data["dtExpectStartDate"] .= " ";
			$data["dtExpectStartDate"] .= $data["dtExpectStartTime"];
			$data["dtExpectStartDate"] .= ":00:00";

			//$secParam = $data;
			$secParam["nCompanySeq"] = $this->member["nCompanySeq"];
			$secParam["nManagerSeq"] = decryptIt($data["nManagerSeq"]);
			$secParam["sContent"] = $data["sContent"];
			$secParam["dtExpectStartDate"] = $data["dtExpectStartDate"];
			$secParam["dtExpectEndDate"] = $data["dtExpectStartDate"];


			$secParam["sCompanyName"] = $resitant["sCompanyName"];
			$secParam["sManagerName"] = $resitant["sManagerName"];
			$secParam["sDepartmentName"] = $resitant["sDepartmentName"];
			$secParam["sInternalPhone"] = $resitant["sInternalPhone"];
			$secParam["sMobilePhone"] = $resitant["sMobilePhone"];
			$secParam["sRank"] = $resitant["sRank"];
			$secParam["sfax"] = $resitant["sfax"];
			$secParam["sEmail"] = $resitant["sEmail"];
			unset($secParam["nCompanyContactSeq"]);
			unset($secParam["dtExpectStartTime"]);
			unset($secParam["dtExpectEndTime"]);

// 			debug_var($data);
// 			die;

			if($mode === "write")
			{
				//foreach ($data as $key=>$row) echo "(".$key."/".$row.")";
				$secParam["nContractSeq"] = $this->member["nContractSeq"];
				$secParam["nServiceType"] = $this->member["sServiceType"];
				$secParam["sActionType"] = "EIO";
				$secParam["sResultStatus"] = "Y";
				$secParam["nWorkType"] = 1;

				//-start 140901 lizzy
				if(!empty($data["sTooluse"])){
					$secParam["sTooluse"] = $data["sTooluse"];
				}
				if(isset($data["tool_info"]))
				{
					if(count($data["tool_info"]) > 0 ){
						$info = "";
						$cnt = count($data["tool_info"]);
						for($i=0; $i <$cnt ; $i++ ){
							$info .= $data["tool_info"][$i];
							if( $i < $cnt-1 ) $info .= "|";
						}
						$secParam["sToolInfo"] = $info;
					}
					unset($secParam["tool_info"]);
				}
				//-end
				//var_dump("write insert param : ");var_dump($secParam);die;
				$data["nWorkRequestSeq"] = $this->work_request_model->_insert($secParam);

				//3. email 발송
				$this->sendmail_work($this->member["sContractName"],$data["nWorkRequestSeq"],"","EIO",
						$data["dtExpectStartDate"], $data["dtExpectStartTime"]);

				$msg = "정상적으로 신청되었습니다. 이용해 주셔서 감사합니다.";
			}
			else if ($mode === "modify")
			{
				if($data["nContractSeq"] !== $this->member["nContractSeq"]) {
					$this->tinyjs->pageBack("정상적인 경로로 접근해주세요.");
					return false;
					exit;
				}
				//-start 20140903 lizzy
				$secParam["sTooluse"] = $data["sTooluse"];

				if(isset($data["tool_info"]))
				{
					if(count($data["tool_info"]) > 0 ){
						$info = "";
						$cnt = count($data["tool_info"]);
						for($i=0; $i <$cnt ; $i++ ){
							$info .= $data["tool_info"][$i];
							if( $i < $cnt-1 ) $info .= "|";
						}
						$secParam["sToolInfo"] = $info;
					}
					unset($secParam["tool_info"]);
				}else{
					$secParam["sToolInfo"] = " ";
				}
				//-end

				$where["nWorkRequestSeq"] = $data["nWorkRequestSeq"];
				//var_dump($secParam);
				$this->work_request_model->_update($secParam, $where);
				$this->work_equipment_model->_delete($where);
				$msg = "수정이 완료되었습니다.";
			}
			//print_r($data); die();
			for($i=0; $i<count($data["nCompanyEquipmentSeq"]); $i++) {
				unset($secParam);
				$secParam["nWorkRequestSeq"] = $data["nWorkRequestSeq"];
				$secParam["nContractSeq"] = $this->member["nContractSeq"];
				$secParam["nCompanySeq"] = $this->member["nCompanySeq"];
				if(isset($data["sBrandName"])) $secParam["sBrandName"] = $data["sBrandName"][$i];
				if(isset($data["sModelName"])) $secParam["sModelName"] = $data["sModelName"][$i];
				if(isset($data["sEquipmentName"])) $secParam["sEquipmentName"] = $data["sEquipmentName"][$i];
				if(isset($data["nQuantity"])) $secParam["nQuantity"] = $data["nQuantity"][$i];
				if(isset($data["sRack"])) $secParam["sRack"] = $data["sRack"][$i];
				if(isset($data["sUseIp"])) $secParam["sUseIp"] = $data["sUseIp"][$i];
				if(isset($data["sContent"])) $secParam["sContent"] = $data["sContent2"][$i];
				if(isset($data["sWorkType"])) $secParam["sWorkType"] = $data["sWorkType"][$i];
				if(isset($data["nCompanyEquipmentSeq"])) $secParam["nCompanyEquipmentSeq"] = $data["nCompanyEquipmentSeq"][$i];
					if($secParam["nCompanyEquipmentSeq"] == null || $secParam["nCompanyEquipmentSeq"] == 'undefined' ) $secParam["nCompanyEquipmentSeq"] = 0;//160106
				$secParam["nServiceType"] = $this->member["sServiceType"];
				$this->work_equipment_model->_insert($secParam);
			}
			$this->tinyjs->pageRedirect(site_url("/kinxidc/equipment"), $msg);
		}
		// 장비입반출 취소
		else if ($mode === "cancel") {
			$seq = $this->uri->rsegment(4);
			if($seq) {
				$seq = decryptIt($seq);//150316
				$this->load->model("work_request_model");
				$this->load->model("work_equipment_model");
				$where["nWorkRequestSeq"] = $seq;
				$data["basic"] = $this->work_request_model->_select_row($where);

				if($data["basic"]["nContractSeq"] !== $this->member["nContractSeq"]) {
					$this->tinyjs->pageBack("정상적인 경로로 접근해주세요.");
					return false;
					exit;
				}

				$where["nWorkRequestSeq"] = $data["basic"]["nWorkRequestSeq"];
				$secParam["sResultStatus"] = "C";
				$this->work_request_model->_update($secParam, $where);
				$msg = "취소가 완료되었습니다.";
				$this->tinyjs->pageRedirect(site_url("/kinxidc/equipment"), $msg);
			} else {
				$this->tinyjs->pageBack("정상적인 경로로 접근해주세요");
				return false;
			}
		}
	}

	public function techsupport()
	{
		$this->_set_sec();
		$secParams = $this->_get_sec();
		$secParams["offset"] = isset($secParams["page"]) ? $secParams["page"] : "0";
		$secParams["limit"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		//print_r($secParams);


		$this->load->model("Mykinx_techsupport_model");
		//1-1.Service Type 구하기
		$nContractSeq = $this->member["nContractSeq"];
		$data["nContractSeq"] = $this->member["nContractSeq"];
		$data["nCompanySeq"] = $this->member["nCompanySeq"];
		//db에서 추출한 nServiceCode가 1이면 member["sServiceType"]에 1, 1보다 크면 2로 설정
		//1이면 IX, 2이면 IDC로 판별
		if( $this->member["sServiceType"]==1 )
		{
			$data["nServiceType"]= "IX";
		}else
		{
			$data["nServiceType"]= "IDC";
		}

		//1-2.요청중인 목록 가져오기
		$listCnt = 0;
		$data["strTerm"]=6;
		if($this->member["nContractSeq"]==70) //id: kiti, name : (주)ㅋ케이티이엔에스(KTN)
		{
			$data["strTerm"]= 128;
		}
		$data["workType"] = "ETC";//ActionType
		$listCnt = $this->Mykinx_techsupport_model->getTechSupportListCnt($data,$secParams);//140827 보안강화
		$data["techSupportList"] = $this->Mykinx_techsupport_model->getTechSupportList($data,$secParams);//140827 보안강화

		$this->load->model("company_contact_model");
		$data["competentList"] =  $this->company_contact_model->_select_list($secParams);
		$tempCnt = 0;
		if(isset($data["competentList"]))
		{
			$tempCnt = count($data["competentList"]);
		}

		if(!isset($data["competentList"]) or $tempCnt <= 0)
		{
			$data["chkmsg"] = "※ 현재 등록된 담당자가 없습니다. 담당자관리 페이지에서 담당자 등록 후 ,신청을 하실 수 있습니다.";
		}
		else{
			$data["chkmsg"] = '';
		}

		//page
		$data["cnt"] = $listCnt;
		$pager["CNT"] = $data["cnt"];
		$pager["PRPAGE"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$pagerHtm = $this->_set_pager($pager);

		$data["pager"] = $pagerHtm;
		$data["pagerIdx"] = $data["cnt"] - $secParams["offset"];
		$data["secParams"] = $secParams;
		///

		$this->_print($data);
	}

	public function techsupport_view($nWorkRequestSeq)
	{
		$this->load->model("Mykinx_techsupport_model");
		$nWorkRequestSeq = decryptIt($nWorkRequestSeq);//150313
		$secParams["nWorkRequestSeq"] = $nWorkRequestSeq;
		$secParams["nContractSeq"] = $this->member["nContractSeq"];

		//$data["techSupportData"] = $this->Mykinx_techsupport_model->_select_row($secParams,0); //org
		//111706
		$temp = $this->Mykinx_techsupport_model->_select_Resultstatus($nWorkRequestSeq,0);
		$Status = $temp['sResultStatus'];
		//print_r("status : "); print_r($Status);
		$data["techSupportData"] = $this->Mykinx_techsupport_model->_select_row_status($secParams,0,$Status); //test
		//print_r("techSupportData : "); print_r($data["techSupportData"]);

		$this->_print($data);
	}

	public function techsupport_write()
	{
		$this->scripts[] = "my_techsupport.js";
		$this->_set_sec();
		$secParams = $this->_get_sec();

		$this->load->model("Mykinx_techsupport_model");

		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		//1.등록자 정보 가져오기
		$secParams["oKey"][0] = "sCompanyType";
		$secParams["oType"][0] = "asc";
		$secParams["oKey"][1] = "nSort";
		$secParams["oType"][1] = "asc";
		$secParams["oKey"][2] = "sCompanyName";
		$secParams["oType"][2] = "asc";
		$secParams["oKey"][3] = "sManagerName";
		$secParams["oType"][3] = "asc";
		$data["companycontact"] = $this->Mykinx_techsupport_model->_select_list($secParams,1);

		//2. 작업내용-작업구분 가져오기
		$groupCode = "T0003";//직군 저장된 tSysCode의 그룹코드
		$this->load->model("Code_model");
		$data["workType"] = $this->Code_model->get_code_list_array($groupCode, FALSE, null); //채용직군

		$this->_print($data);
	}

	public function techsupportRequest()
	{
		//print_r($this->member);
		$data = $this->input->post();
		$data["companycontact"] = decryptIt($data["companycontact"]);//150313
		$this->load->model("Mykinx_techsupport_model");

		//1-1.Service Type 구하기
		//db에서 추출한 nServiceCode가 1이면 member["sServiceType"]에 1, 1보다 크면 2로 설정
		//1이면 IX, 2이면 IDC로 판별
		if( $this->member["sServiceType"]==1 )
		{
			$data["nServiceType"]= "IX";
		}else
		{
			$data["nServiceType"]= "IDC";
		}

		$data["dtExpectStartDate"] .= " ";
		$data["dtExpectStartDate"] .= $data["dtExpectStartTime"];
		$data["dtExpectStartDate"] .= ":00:00";

		//2. tWorkRequest에 추가
		$data["nContractSeq"] = $this->member["nContractSeq"];
		$data["nCompanySeq"] = $this->member["nCompanySeq"];

		$data["nWorkRequestSeq"]=$this->Mykinx_techsupport_model->InsertWorkRequest($data);

		if(isset($data["nWorkRequestSeq"])){
            //3. email 발송
            $this->sendmail_work($this->member["sContractName"],$data["nWorkRequestSeq"],$data["techWorkType"],"ETC",
				$data["dtExpectStartDate"], $data["dtExpectStartTime"]);
			//4.Alert . 페이지 전환
            $this->tinyjs->alert("정상적으로 신청되었습니다. 이용해 주셔서 감사합니다.");
		}else{
		   $this->tinyjs->alert("정상적으로 신청되지 않았습니다. 확인 후 다시 신청해주세요.");
		};

		$this->tinyjs->pageRedirect("/kinxidc/techsupport");
	}

	public function sendmail_work($sContactName, $nWorkRequestSeq, $workType, $sActionType, $sDate, $sTime)
	{
		$this->load->helper('email');

		/*
		//방문신청, 기타작업의 경우 bdnoc@ 추가 - 업무협조전 3166
		if($sActionType == "VST" || $sActionType == "ETC")
		{
			if(ENVIRONMENT == 'development')
				$tolist = array("lizzy337@kinx.net","lizzy337@gmail.com");
			else
				$tolist = array(MAIL_WORK_REQUEST,MAIL_WORK_REQUEST_BD);
		}
		else
		{
			$tolist = MAIL_WORK_REQUEST;
		}
		*/
		//기타작업(기술요청 포함)에 서비스 운영그룹 추가 요청 220429
		//type 별로 나눔
		if($sActionType == "VST")
		{
			$tolist = array(MAIL_WORK_REQUEST,MAIL_WORK_REQUEST_BD);
		}
		else if ($sActionType == "ETC")
		{
			$tolist = array(MAIL_WORK_REQUEST,MAIL_WORK_REQUEST_BD,MAIL_SERVICE_MNG_GROUP);//service 추가
		}
		else
		{
			$tolist = MAIL_WORK_REQUEST;
		}
		if(ENVIRONMENT == 'development')
			$tolist = array("lizzy337@kinx.net","lizzy337@gmail.com");

		//print_r('tolist');print_r($tolist);die;

		switch($sActionType)
		{
			case "VST": $title = "방문신청이 접수되었습니다."; break;
			case "EIO":  $title = "장비입반출 신청이 접수되었습니다."; break;
			case "ETC": $title = "기타작업이 접수되었습니다."; break;
		}

		$mailtitle = "[알림]".$title;

		//메일 본문 내용
		$sMailDocumentBody	= '';

		$sMailDocumentBody .= '
				<!-- 본문 내용 -->
			    <table border="0" cellpadding="0" cellspacing="0" width="640">
			   <tr>
			    <td><font style="font-family:맑은 고딕,돋움; font-size:13px;">
			    안녕하세요.<br />
			    인터넷 인프라 전문기업 ㈜케이아이엔엑스입니다.<br />';

		switch($sActionType)
		{
			case "VST": //방문신청
				$sMailDocumentBody .= '
					방문신청이 접수되었습니다.<br />
    				자세한 내용은  <a href="https://'.INTRANET_HOMEPAGE.'/login?code=MANAGE&url=https://intraframe.kinx.net/visitRequest/list" target="_blank">인트라넷&gt;운영관리&gt;네트워크&gt;방문 신청 현황</a>에서 확인할 수 있습니다.</font><br /><br />
						';
				break;
			case "EIO": //장비입반출관리
				$sMailDocumentBody .= '
					장비입반출 신청이 접수되었습니다.<br />
    				자세한 내용은  <a href="https://'.INTRANET_HOMEPAGE.'/login?code=MANAGE&url=https://intraframe.kinx.net/deviceEquipment/list" target="_blank">인트라넷&gt;운영관리&gt;네트워크&gt;장비 입반출현황</a>에서 확인할 수 있습니다.</font><br /><br />
						';
				break;
			case "ETC": //기술요청문의
				$sMailDocumentBody .= '
					기타작업이 접수되었습니다.<br />
    				자세한 내용은  <a href="https://'.INTRANET_HOMEPAGE.'/login?code=MANAGE&url=https://intraframe.kinx.net/techSupport/list" target="_blank">인트라넷&gt;운영관리&gt;네트워크&gt;기술 요청현황</a>에서 확인할 수 있습니다.</font><br /><br />
						';
				break;
		}

		$sMailDocumentBody.='
				    <table border="2" cellpadding="6" cellspacing="0" width="640" bordercolor="#222222" bordercolordark="#222222" bordercolorlight="#222222"  style="border-collapse:collapse;">
				     <tr>
				      <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>계약명</b></font></td>
				      <td width="250"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$sContactName.'</font></td>
				      <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>접수번호</b></font></td>
				      <td width="166"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$nWorkRequestSeq.'</font></td>
				     </tr>
				     <tr>
				      <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>작업유형</b></font></td>';

		$strWorkType = "";
		switch($sActionType)
		{
			case "VST":
				switch($workType)
				{
					case "2":
						$strWorkType = "장비유지보수(S/W)"; break;
					case "3" :
						$strWorkType = "장비유지보수(H/W)"; break;
					case "4" :
						$strWorkType = "장비입반출"; break;
					case "5" :
						$strWorkType = "기술담당자방문"; break;
					case "6" :
						$strWorkType = "영업담당자방문"; break;
					case "7" :
						$strWorkType = "전용선작업"; break;
					case "8" :
						$strWorkType = "장애처리"; break;
					case "9" :
						$strWorkType = "전기점검"; break;
					case "10" :
						$strWorkType = "기타"; break;
				}
				$sMailDocumentBody .=' <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">고객 방문요청('.$strWorkType.')</font></td>';
				break;
			case "EIO" :
				$sMailDocumentBody .='<td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">장비 입반출</font></td>';
				break;
			case "ETC":
				{
					switch($workType)
					{
						case "1":
							$strWorkType = "기술/운영문의"; break;
						case "2" :
							$strWorkType = "상품/요금문의"; break;
						case "3" :
							$strWorkType = "서버리부팅"; break;
						case "4" :
							$strWorkType = "기타문의"; break;
					}
					$sMailDocumentBody .=' <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">기타작업('.$strWorkType.')</font></td>';
				}
				break;
		}
		$sMailDocumentBody.='
				     </tr>
				     <tr>
						<td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>방문(작업)예정일</b></font></td>
				        <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$sDate.'</font></td>
				     </tr>
				      		';
		$sMailDocumentBody	.= '</table><br/>';
		$sMailDocumentBody .= '<br><br>';


		$data['title'] = $title; //내용의 제목
		$data['message'] = $sMailDocumentBody;
		//$data['files'] = $rowfiles;
		$data['fromemail'] = EMAIL_HELP;
		$data['fromname'] = EMAIL_SENDMAIL_NAME;
		$data['to'] = $tolist;
		$data['subject'] = $mailtitle; //메일 제목
		$data['footertype'] = 1;//발신전용
		$data['inout'] = 'in';//in or out // out이면 외부, 고객 발신, 고객센터 내용 추가되어야 함.
		if(ENVIRONMENT == 'development')//20201007 development에만 받도록 추가.
			$data['bcc'] = "lizzy337@kinx.net";

		//메일 발송
		$this->load->helper('email');
		kinxMailSendFnc($data, FALSE);
	}


	public Function visit_alert()
	{
		$this->_print();
		exit;
	}

}