<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/Common.php");
class Payment extends Common{

    private $checkinarray = array(); //ISM의 tAccount.nMyCardPayment 값을 참고함

    public function __construct() {

        parent::__construct();

        $this->load->model("account_ism_model","md_account");
        $this->load->model("customer_ism_model","md_customer");
        $this->load->model("taxinvoice_ism_model","md_taxinvoice");
        $this->load->model("taxinvoice_detail_ism_model","md_taxinvoice_detail");


        $artemp = $this->md_account->get_myCardPayment();
        foreach($artemp as $v)
        {
            array_push($this->checkinarray,$v['sAccountID']);
        }

        //$this->is_login = true;
        if($this->is_login === false)
        {
            $this->output->set_status_header('410');
            $this->tinyjs->pageRedirect("/", "로그인 후 접근가능합니다");
            exit;
        }

        $this->load->helper('common');
    }


    //입금처리 관련하여 금액 추출 부분 수정
    public function index()
    {
        $this->_set_sec();
        $secParams = $this->_get_sec();

        if(!isset($this->member["nAccountSeq"]) || empty($this->member["nAccountSeq"]))
        {
            $this->output->set_status_header('410');
            $this->tinyjs->pageRedirect("/", "로그인 후 접근가능합니다");
            exit;
        }

        $secParams["offset"] = isset($secParams["page"]) ? $secParams["page"] : "0";
        $secParams["limit"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];


        //$secParams["orderby"] = "B.dtChargeDate DESC";
        $secParams["nAccountSeq"] = $this->member["nAccountSeq"];
        $secParams["sDeleteYN"] = "N";


        $data["cnt"] = $this->md_taxinvoice->get_list($secParams,TRUE);
        $data["list"] =  $this->md_taxinvoice->get_list($secParams);

        //billmonth_price : 청구액, billmonth_currency : 단위, income_finance : 받은돈, income_deposit : 예치금, dtIncome : 최종 입금일
        //리스트의 결제일을 tBillMonth의 dtReceipt => tBillMonth의 dtIncome 로 변경

        //va('payment list'); va($data); die;
        ///
        $data['chk_showname'] = FALSE;

        //납일기간 추출, 청구액 - 금액&단위 처리
        for($i=0, $max=count($data["list"]);$i<$max; $i++)
        {
            //청구액
            $artmp = get_currency_price($data["list"][$i]["billmonth_price"],$data["list"][$i]["billmonth_currency"]);
            $sBillMonthPrice = $artmp["sPrice"];
            $sCurrency = $artmp["sCurrency"];
            $data["list"][$i]['sBillMonthPrice'] = $sBillMonthPrice;
            $data["list"][$i]['sCurrency'] = $sCurrency;

            //결제금액
            $tmpIncome = $data["list"][$i]['income_finance'] + $data["list"][$i]['income_deposit'];
            $artmp = get_currency_price($tmpIncome,$data["list"][$i]["billmonth_currency"]);
            $data["list"][$i]['sIncomeprice'] = $artmp["sPrice"];
            $data["list"][$i]['nIncomeprice'] = $tmpIncome;

            //미수잔액 : 청구액 - 결제금액
            $tmpRem = $data["list"][$i]["billmonth_price"] - $tmpIncome;
            $data["list"][$i]['nRemaindPrice'] = $tmpRem;
            $artmp = get_currency_price($tmpRem,$data["list"][$i]["billmonth_currency"]);
            $data["list"][$i]['sRemaindPrice'] = $artmp["sPrice"];
            $data["list"][$i]['nRemaindPrice'] = $tmpRem;

            //납일기간
            $deadline = $data["list"][$i]['sDeadLine'];
            $chargdt = $data["list"][$i]['dtChargeDate'];
            $deaddt = null;

            // 청구명
            if(!empty($data["list"][$i]['sShowName']))
            {
                $data['chk_showname'] = TRUE;
            }

            switch(substr($deadline,0,1))
            {
                case 'A': //금월
                        $mm = substr($chargdt,5,2);
                        $yy = substr($chargdt,0,4);
                        break;
                case 'B': //전월
                        $mm = (substr($chargdt,5,2) == "01") ? 12 : sprintf('%02d',(int)substr($chargdt,5,2) -1);
                        $yy = ($mm==12) ?  substr($chargdt,0,4) -1 : substr($chargdt,0,4);

                        break;
                case 'C': //익월
                        $mm = (substr($chargdt,5,2) == "12") ? 1 : sprintf('%02d',(int)substr($chargdt,5,2) +1);
                        $yy = ($mm==1) ?  substr($chargdt,0,4) +1 : substr($chargdt,0,4);
                        break;
                case 'D': //익익월 add 20200326
                    $mm = (substr($chargdt,5,2) == "12") ? 2 : sprintf('%02d',(int)substr($chargdt,5,2) +2);
                    $yy = ($mm==1) ?  substr($chargdt,0,4) +1 : substr($chargdt,0,4);
                    break;
            }

            $deaddt = $yy."-".$mm;

            switch(substr($deadline,1))
            {
                case 'EE':
                        $deaddt = date("Y-m-t", strtotime($deaddt."-01"));
                        break;
                case '29':
                case '30':
                        if($mm=="02") $deaddt = date("Y-m-t", strtotime($deaddt."-01"));
                        else $deaddt .= "-".substr($deadline,1);
                        break;
                case '31':
                        if($mm=="02" || date("t", strtotime($deaddt."-01"))==30)
                                $deaddt = date("Y-m-t", strtotime($deaddt."-01"));
                        else $deaddt .= "-".substr($deadline,1);
                        break;
                default:
                        $deaddt .= "-".substr($deadline,1);
            }

            //새로운 입금,미수 관련 값 적용
            //$data["list"][$i]['sDeadFlag'] = (($data["list"][$i]['nChargePrice'] - $data["list"][$i]['nReceiptPrice']) >0 && $deaddt < date("Y-m-d")) ? 1 : 0;
            $data["list"][$i]['sDeadFlag'] = (($data["list"][$i]['billmonth_price'] - ($data["list"][$i]['income_finance'] + $data["list"][$i]['income_deposit'])) >0 && $deaddt < date("Y-m-d")) ? 1 : 0;
        }


        $pager["CNT"] = $data["cnt"];
        $pager["PRPAGE"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
        $pagerHtm = $this->_set_pager($pager);

        $data["pager"] = $pagerHtm;
        $data["pagerIdx"] = $data["cnt"] - $secParams["offset"];
        $data["secParams"] = $secParams;
        $data["is_checkin"] = (in_array($this->member['sAccountID'],$this->checkinarray) ) ? TRUE : FALSE;



        $this->_print($data);
    }



    #seqinfo : nTaxInvoiceSeq || nBillMonthSeq
    #금액 : tTaxInvoice
    public function detail($seqinfo=null)
    {
            $arSeq = seed($seqinfo, FALSE);
            $arTmp = explode('||',$arSeq) ;
            $nTaxInvoiceSeq = $arTmp[0];
            $nBillMonthSeq = $arTmp[1];

            $secParams["limit"] = 1;
            $secParams["offset"] = 0;
            $secParams["nAccountSeq"] = $this->member["nAccountSeq"];
            $secParams["sDeleteYN"] = "N";
            $secParams["nTaxInvoiceSeq"] = $nTaxInvoiceSeq;

            $tmp =  $this->md_taxinvoice->get_list($secParams);
            unset($secParams);
            $data["data"] = $tmp[0];

            if(ENVIRONMENT=='development')
                    $data["data"]["sEmail"] = "lizzy337@kinx.net";
            //-start test lizzy337
            /*
            if(ENVIRONMENT=='development')
            {
                $data["data"]['nChargePrice'] = 200;
                $data["data"]['nTaxPrice'] = 20;
                $data["data"]['nRemainderPrice'] = 220;
            }
            */
            //-end
            unset($tmp);

            $secParams["sDeleteYN"] = "N";
            $secParams["nTaxInvoiceSeq"] = $nTaxInvoiceSeq;//150313
            $tmp =  $this->md_taxinvoice_detail->get_list_bySeq($secParams);
            unset($secParams);
            $data["list"] = $tmp;
            unset($tmp);

            $max=count($data["list"]);
            for($i=0 ;$i<$max; $i++)
            {
                $temp =  get_currency_price($data['list'][$i]['nCharge'],$data['data']['sCurrencyType']);
                $data['list'][$i]['sCharge'] = $temp['sPrice'];
                $data['list'][$i]['sCurrency'] = $temp['sCurrency'];
                $temp = get_currency_price($data['list'][$i]['nTaxPrice'],$data['data']['sCurrencyType']);
                $data['list'][$i]['sVatPrice'] = $temp['sPrice'];
            }

            //$where["nAccountSeq"] = $this->member["nAccountSeq"];
            $data["contract"] = $this->md_account->get_account_byId($this->member["sAccountID"]);
            unset($where);

            $data["company"] = $this->md_customer->get_customer_bySeq($data["contract"]["nCustomerSeq"]);
            $data["company"]['arOtherInfo'] = json_decode($data["company"]['arOtherInfo'],true);


            unset($where);

            ///// modified by lizzy - syz source copy
            $temp1 = $_SERVER['SERVER_PORT']!=443?"http":"https";
            $temp2 = $_SERVER['SERVER_PORT']!=443?":7080":":7443";
            $temp3 = (CST_PLATFORM == "test")?$temp2:"";
            $data["temp1"]=$temp1;
            $data["temp3"]=$temp3;


            # 특정 아이디의 경우 결제기능 활성화
            //$data["is_checkin"] = (in_array($this->member['sMemberId'],$this->checkinarray) && $data['data']['nRemainderPrice']>0) ? TRUE : FALSE;
            $data["is_checkin"] = (in_array($this->member['sAccountID'],$this->checkinarray)) ? TRUE : FALSE; //미수액이 있을때 결제버튼 활성화 되는 조건 제외. 170607

            if($data["data"]['sCurrencyType'] != 'KRW') //원화가 아니면 결제버튼 나타내지 않음
               $data["is_checkin"] = FALSE;


            $data["sServiceType"] = $this->member["sServiceType"];
            $data["productinfo"]='';

            $BillMonthInfo = $this->md_taxinvoice_detail->get_BillMonthInfo($nBillMonthSeq);
            //va($BillMonthInfo); die;

            //$data["cardPrice"]=$data['data']['nRemainderPrice'] >0 ? $data['data']['nRemainderPrice'] + $data['data']['nRemainderVat']:0;//old
            $data["cardPrice"] = $BillMonthInfo[0]['billmonth_price'] - ( $BillMonthInfo[0]['income_finance'] + $BillMonthInfo[0]['income_deposit']);

            if(ENVIRONMENT=='development' && $this->member['sAccountID'] == 'test023s')
            {
                $data['cardPrice'] = 220;
            }

            if($BillMonthInfo[0]['billmonth_currency'] == "USD")
                $data["cardPrice_currency"] = "$";
            else
                $data["cardPrice_currency"] = "원";
            if(!empty($data["list"]))
            {
                //$data["productinfo"] = count($data["list"])>1 ? $data["list"][0]['sServiceName'].' 외 '.(count($data["list"])-1).'건' : $data["list"][0]['sServiceName'];
                $data["strServiceTypeEtc"] = count($data["list"])>1 ? $data["list"][0]['sServiceName'].' 외 '.(count($data["list"])-1).'건' : $data["list"][0]['sServiceName'];
            }
            if(!empty($data["data"]))
            {
                $data["productinfo"] = substr($data["data"]["dtChargeDate"],0,7)."세금계산서 발행분";
            }

            $this->scripts = ['my_payment.js'];

            //va("payment detail"); va($data);die;
            $this->_print($data);
	}

    //20200326
    //tTaxInvoice Division 관련 내용 으로 변경 . nDivisionFlag 이용
    #seqinfo : nTaxInvoiceSeq || nBillMonthSeq
	public function detail_new($seqinfo=NULL, $result=NULL)
	{
        //tTaxInvoice 정보 가져오기 - 기간정보
        $arSeq = seed($seqinfo, FALSE);
        $arTmp = explode('||',$arSeq) ;
        $nTaxInvoiceSeq = $arTmp[0];
        $nBillMonthSeq = $arTmp[1];

        $secParams["limit"] = 1;
        $secParams["offset"] = 0;
        $secParams["nAccountSeq"] = $this->member["nAccountSeq"];
        $secParams["sDeleteYN"] = "N";
        $secParams["nTaxInvoiceSeq"] = $nTaxInvoiceSeq;


        # 특정 아이디의 경우 결제기능 활성화
        $data["is_checkin"] = (in_array($this->member['sAccountID'],$this->checkinarray)) ? TRUE : FALSE; //미수액이 있을때 결제버튼 활성화 되는 조건 제외. 170607

        $tmp =  $this->md_taxinvoice->get_list($secParams);
        unset($secParams);


        # 청구 데이터 추출
        $data["data"]                 = $tmp[0];


        # 카드결제 활성화 체크
        $data["is_checkin"] = ($data["data"]["sCurrencyType"] != 'KRW') ? FALSE : $data["is_checkin"]; //원화가 아니면 결제버튼 나타내지 않음
        $data["is_checkin"] = ($data["data"]["sIncomeFlag"]   == '9')   ? FALSE : $data["is_checkin"]; //완납인 경우 결제버튼 나타내지 않음

        $data['charge_result']  = FALSE;
        $data['payment_result'] = FALSE;

        # 카드 결제 완료후 처리
        if($result=='success' && $data["is_checkin"])
        {
            $getdata = $this->input->get(NULL, TRUE);

            $getdata['buyerid']    = $data["data"]['nAccountSeq'];
            $getdata['product_cd'] = $data["data"]['nBillListSeq'];

            $this->load->library('tosscard');
            $data['charge_result'] = $this->tosscard->run_charge($getdata);
        }
        elseif($result=='fail' && $data["is_checkin"])
        {
            $getdata = $this->input->get(NULL, TRUE);
            $data['charge_result'] = [
                'sResultCode'    => $getdata['code'],
                'sResultMessage' => $getdata['message']
            ];
        }




        if(ENVIRONMENT=='development')
        {
            $data["data"]["sEmail"] = "yjcin13@kinx.net";
        }
        unset($tmp);


        $data["data"]['sPhone']       = str_replace("-","",$data["data"]['sPhone']);
        $data["data"]['sMobile']      = str_replace("-","",$data["data"]['sMobile']);
        $data['list'] = array();
        //tTaxInvoiceDivision이 없으면 tTaxInvoiceDetail로 목록 꾸민다.
        //단. tTaxInvoiceDetail.nDivisionFlag 가 1이면 tTaxInvoiceDivision 이용하고 0이면 이용하지 않는다.(tTaxInvoiceDetail만 이용한다.)
        //tTaxInvoiceDetail 구하기
        $secParams["sDeleteYN"] = "N";
        $secParams["nTaxInvoiceSeq"] = $nTaxInvoiceSeq;
        $tmp_detail =  $this->md_taxinvoice_detail->get_list_bySeq($secParams);
        $cnt = count($tmp_detail);
        for($i=0; $i < $cnt ; $i++)
        {
            if($tmp_detail[$i]['nDivisionFlag'])//nDivisionFlag 가 활성화되면 Division 포함된 내용으로 다시 구함.
            {

                $secParams["sDeleteYN"] = "N";
                $secParams["nTaxInvoiceSeq"] = $nTaxInvoiceSeq;
                $secParams["nTaxInvoiceDetailSeq"] = $tmp_detail[$i]['nTaxInvoiceDetailSeq'];
                $secParams["nDivisionFlag"] = $tmp_detail[$i]['nDivisionFlag'];
                $tmp =  $this->md_taxinvoice_detail->get_taxinvoicedivision_byDetailSeq($secParams);
                //va($tmp);die;
                for($j = 0 ; $j < count($tmp) ; $j++)
                {
                    array_push($data['list'], $tmp[$j]);
                }
                unset($secParams);
            }
            else//nDivision이 활성화 되어 있지 않으면 tTaxInvoiceDetail로
            {
                array_push($data['list'],$tmp_detail[$i]);
                //$data["list"] = $tmp_detail[$i];
            }
        }
        //va($data['list']);die;

        //////////////////////////
        $max=count($data["list"]);
        for($i=0 ;$i<$max; $i++)
        {
            $temp =  get_currency_price($data['list'][$i]['nCharge'],$data['data']['sCurrencyType']);

            $data['list'][$i]['detailqes'] =  encryptIt($data['list'][$i]['nBillDetailSeq']);
            $data['list'][$i]['sCharge']   = $temp['sPrice'];
            $data['list'][$i]['sCurrency'] = $temp['sCurrency'];
            //$temp = get_currency_price($data['list'][$i]['nTaxPrice'],$data['data']['sCurrencyType']);
            //$data['list'][$i]['sVatPrice'] = $temp['sPrice'];
            $temp =  get_currency_price($data['list'][$i]['nContractCharge'],$data['data']['sCurrencyType']);
            $data['list'][$i]['sContractCharge'] = $temp['sPrice'];

            //수량 관련 - nQuantity 값이 0이면 나타내지 않는다
            $data['list'][$i]['sQuantity'] = '';
            if($data['list'][$i]['nQuantity'] != 0)
            {
                //$data['list'][$i]['sQuantity'] = $data['list'][$i]['nQuantity'].$data['list'][$i]['sUnit'];
                $data['list'][$i]['sQuantity'] = number_format_unit($data['list'][$i]['nQuantity'],$data['list'][$i]['sUnit']).' '.$data['list'][$i]['sUnit'];
            }
            //세부사항 노출 관련
            //$data['list'][$i]['sBillTypeName'] = '';
            if(isset($data['list'][$i]['sSubject']) )//set 되어 있는 경우는 tTaxInvoiceDivision 에 값이 있는 경우이다
            {
                $data['list'][$i]['sProductDetailInfo'] = $data['list'][$i]['sSubject'];
                if($data['list'][$i]['nContractQty'])
                    $data['list'][$i]['sProductDetailInfo'] .= " <br> ".number_format_unit($data['list'][$i]['nContractQty'],$data['list'][$i]['sContractUnit']);
                if($data['list'][$i]['sContractUnit'])
                    $data['list'][$i]['sProductDetailInfo'] .= " ".$data['list'][$i]['sContractUnit'];
            }
            else
            {
                $data['list'][$i]['sProductDetailInfo'] = '';
            }
        }

        //사업자 정보
        $data["contract"] = $this->md_account->get_account_byId($this->member["sAccountID"]);
        unset($where);

        $data["company"] = $this->md_customer->get_customer_bySeq($data["contract"]["nCustomerSeq"]);
        $data["company"]['arOtherInfo'] = json_decode($data["company"]['arOtherInfo'],true);
        unset($where);

        ///// modified by lizzy - syz source copy
        $temp1 = $_SERVER['SERVER_PORT']!=443?"http":"https";
        $temp2 = $_SERVER['SERVER_PORT']!=443?":7080":":7443";
        $temp3 = (CST_PLATFORM == "test")?$temp2:"";
        $data["temp1"]=$temp1;
        $data["temp3"]=$temp3;



        $data["sServiceType"] = $this->member["sServiceType"];
        $data["productinfo"]='';

        $BillMonthInfo = $this->md_taxinvoice_detail->get_BillMonthInfo($nBillMonthSeq);

        //해당 금액이 결제시 넘어간다.
        $data["cardPrice"] = $BillMonthInfo[0]['billmonth_price'] - ( $BillMonthInfo[0]['income_finance'] + $BillMonthInfo[0]['income_deposit']);

        /*
        if(ENVIRONMENT=='development' && $this->member['sAccountID'] == 'test023s')
        {
            $data['cardPrice'] = 220;
        }
        */

        if($BillMonthInfo[0]['billmonth_currency'] == "USD")
            $data["cardPrice_currency"] = "$";
        else
            $data["cardPrice_currency"] = "원";
        if(!empty($data["list"]))
        {
            $data["strServiceTypeEtc"] = count($data["list"])>1 ? $data["list"][0]['sProuctDetailType'].' 외 '.(count($data["list"])-1).'건' : $data["list"][0]['sProuctDetailType'];
        }
        if(!empty($data["data"]))
        {
            $data["productinfo"] = substr($data["data"]["dtChargeDate"],0,7)."세금계산서 발행분";
        }


        # load script
        $this->scripts = ($data["is_checkin"]) ? ['https://js.tosspayments.com/v1','tosspayment.js','my_payment.js','payment/msgelement.js','payment/payment.js'] : ['my_payment.js'];

        //va("payment detail"); va($data);die;
        $this->_print($data);
    }

    public function detail_new_v1($seqinfo=null)
	{
        //tTaxInvoice 정보 가져오기 - 기간정보
        $arSeq = decryptIt($seqinfo);
        $arTmp = explode('||',$arSeq) ;
        $nTaxInvoiceSeq = $arTmp[0];
        $nBillMonthSeq = $arTmp[1];

        $secParams["limit"] = 1;
        $secParams["offset"] = 0;
        $secParams["nAccountSeq"] = $this->member["nAccountSeq"];
        $secParams["sDeleteYN"] = "N";
        $secParams["nTaxInvoiceSeq"] = $nTaxInvoiceSeq;


        //test
        /*
        $nTaxInvoiceSeq = 56839; //division 없음 (nAccountSeq = 472)
        $secParams["nTaxInvoiceSeq"] = $nTaxInvoiceSeq;
        $secParams["nAccountSeq"] = 472;
        */
        /*
        $nTaxInvoiceSeq = 60029; #60029 => division 있음(nAccountSeq = 1709)
        $secParams["nTaxInvoiceSeq"] = $nTaxInvoiceSeq;
        $secParams["nAccountSeq"] = 1709;
        */
        //-end test

        $tmp =  $this->md_taxinvoice->get_list($secParams);
        unset($secParams);
        $data["data"] = $tmp[0];

        if(ENVIRONMENT=='development')
            $data["data"]["sEmail"] = "lizzy337@kinx.net";
        //-start test lizzy337
        /*
        if(ENVIRONMENT=='development')
        {
            $data["data"]['nChargePrice'] = 200;
            $data["data"]['nTaxPrice'] = 20;
            $data["data"]['nRemainderPrice'] = 220;
        }
        */
        //-end
        unset($tmp);

        //tTaxInvoiceDivision 확인하기
        //tTaxInvoiceDivision 이 있으면 이 정보로 목록 꾸미고 없으면 tTaxInvoiceDetail 값 가져오기
        $secParams["sDeleteYN"] = "N";
        $secParams["nTaxInvoiceSeq"] = $nTaxInvoiceSeq;
        $tmp =  $this->md_taxinvoice_detail->get_taxinvoicedivision_bySeq($secParams);
        unset($secParams);

        if(count($tmp))
        {
            va("Division VVVVVVVVVVVVVVVVVVVVVVVVVV");
            $data['list'] = $tmp;
            unset($tmp);
        }
        else
        {
            va("Detail DDDDDDDDDDDDDDDDDDDDD");
            //tTaxInvoiceDetail
            $secParams["sDeleteYN"] = "N";
            $secParams["nTaxInvoiceSeq"] = $nTaxInvoiceSeq;
            $tmp =  $this->md_taxinvoice_detail->get_list_bySeq($secParams);
            unset($secParams);
            $data["list"] = $tmp;
            unset($tmp);
        }

        //////////////////////////
        $max=count($data["list"]);
        for($i=0 ;$i<$max; $i++)
        {
            $temp =  get_currency_price($data['list'][$i]['nCharge'],$data['data']['sCurrencyType']);
            $data['list'][$i]['sCharge'] = $temp['sPrice'];
            $data['list'][$i]['sCurrency'] = $temp['sCurrency'];
            //$temp = get_currency_price($data['list'][$i]['nTaxPrice'],$data['data']['sCurrencyType']);
            //$data['list'][$i]['sVatPrice'] = $temp['sPrice'];
            $temp =  get_currency_price($data['list'][$i]['nContractCharge'],$data['data']['sCurrencyType']);
            $data['list'][$i]['sContractCharge'] = $temp['sPrice'];

            //수량 관련 - nQuantity 값이 0이면 나타내지 않는다
            $data['list'][$i]['sQuantity'] = '';
            if($data['list'][$i]['nQuantity'] != 0)
            {
                //$data['list'][$i]['sQuantity'] = $data['list'][$i]['nQuantity'].$data['list'][$i]['sUnit'];
                $data['list'][$i]['sQuantity'] = number_format_unit($data['list'][$i]['nQuantity'],$data['list'][$i]['sUnit']).' '.$data['list'][$i]['sUnit'];
            }
            //세부사항 노출 관련
            $data['list'][$i]['sBillTypeName'] = '';
            if(isset($data['list'][$i]['sBillType']) )//set 안되어 있는 경우는 tTaxInvoiceDivision 에 값이 없는 경우이다
            {
                /*
                //test.   sBillType 삭제
                if( $data['list'][$i]['sBillType'] == 'lease')
                    $data['list'][$i]['sBillType'] = '';
                    */

                if( strlen($data['list'][$i]['sBillType']))
                    $data['list'][$i]['sBillTypeName'] = get_billtype_name($data['list'][$i]['sBillType']);
            }

            if($data['list'][$i]['sBillTypeName'] == '') //세부사항 노출 안함
            {
                $data['list'][$i]['sProductDetailInfo'] = '';
            }
            else
            {
                /*
                va("----------------------------");
                va("qty : ".$data['list'][$i]['nContractQty']);
                va("unit : ".$data['list'][$i]['sContractUnit']);
                $result = number_format_unit($data['list'][$i]['nContractQty'],$data['list'][$i]['sContractUnit']);
                va("result : ".$result);
                */
                $data['list'][$i]['sProductDetailInfo'] = $data['list'][$i]['sBillTypeName'];
                if($data['list'][$i]['nContractQty'])
                    $data['list'][$i]['sProductDetailInfo'] .= " <br> ".number_format_unit($data['list'][$i]['nContractQty'],$data['list'][$i]['sContractUnit']);
                if($data['list'][$i]['sContractUnit'])
                    $data['list'][$i]['sProductDetailInfo'] .= " ".$data['list'][$i]['sContractUnit'];
            }
        }

        //사업자 정보
        $data["contract"] = $this->md_account->get_account_byId($this->member["sAccountID"]);
        unset($where);

        $data["company"] = $this->md_customer->get_customer_bySeq($data["contract"]["nCustomerSeq"]);
        $data["company"]['arOtherInfo'] = json_decode($data["company"]['arOtherInfo'],true);
        unset($where);

        ///// modified by lizzy - syz source copy
        $temp1 = $_SERVER['SERVER_PORT']!=443?"http":"https";
        $temp2 = $_SERVER['SERVER_PORT']!=443?":7080":":7443";
        $temp3 = (CST_PLATFORM == "test")?$temp2:"";
        $data["temp1"]=$temp1;
        $data["temp3"]=$temp3;


        # 특정 아이디의 경우 결제기능 활성화
        $data["is_checkin"] = (in_array($this->member['sAccountID'],$this->checkinarray)) ? TRUE : FALSE; //미수액이 있을때 결제버튼 활성화 되는 조건 제외. 170607
        if($data["data"]['sCurrencyType'] != 'KRW') //원화가 아니면 결제버튼 나타내지 않음
            $data["is_checkin"] = FALSE;


        $data["sServiceType"] = $this->member["sServiceType"];
        $data["productinfo"]='';

        $BillMonthInfo = $this->md_taxinvoice_detail->get_BillMonthInfo($nBillMonthSeq);
        //va($BillMonthInfo); die;

        //해당 금액이 결제시 넘어간다.
        $data["cardPrice"] = $BillMonthInfo[0]['billmonth_price'] - ( $BillMonthInfo[0]['income_finance'] + $BillMonthInfo[0]['income_deposit']);

        /*
        if(ENVIRONMENT=='development' && $this->member['sAccountID'] == 'test023s')
        {
            $data['cardPrice'] = 220;
        }
        */

        if($BillMonthInfo[0]['billmonth_currency'] == "USD")
            $data["cardPrice_currency"] = "$";
        else
            $data["cardPrice_currency"] = "원";
        if(!empty($data["list"]))
        {
            $data["strServiceTypeEtc"] = count($data["list"])>1 ? $data["list"][0]['sProuctDetailType'].' 외 '.(count($data["list"])-1).'건' : $data["list"][0]['sProuctDetailType'];
        }
        if(!empty($data["data"]))
        {
            $data["productinfo"] = substr($data["data"]["dtChargeDate"],0,7)."세금계산서 발행분";
        }

        $this->scripts = ['my_payment.js'];

        //va("payment detail"); va($data);die;
        $this->_print($data);
    }




    /**
     * IxCloud 과금 상세정보 조회
     */
    public function clouddetail($seq)
    {
        # cloud 정보 처리를 위한 helper load
        $this->load->helper('ixcloud');

        # nBillDetailSeq복호화
        $billdetailseq = decryptIt($seq);

        # Cloud 계약 기준 정보 추출
        $contractinfo  = $this->md_taxinvoice_detail->get_contract_for_cloud($billdetailseq);

        # Cloud 상품 구분
        $product = ($contractinfo['nProductSeq']==320) ? 'multizone' : 'ixcloud';

        # 리소스별상세 과금정보 추출 및 추가 조정금액 추출
        list($detail_setdata, $chk_adjcharge) = $this->_get_cloud_detail_viewdata($product, $billdetailseq);

        # 추가 조정금액 등록
        $contractinfo['nAddAdjCharge'] = $chk_adjcharge;

        # json 변환
        $contractinfo['arDetailInfo']  = chg_arrayfield($contractinfo['arDetailInfo']);
        $contractinfo['arDiscountOpt'] = chg_arrayfield($contractinfo['arDiscountOpt']);
        $contractinfo['lastCharge']    = $contractinfo['nCharge'] + $contractinfo['nAdjCharge'] + $contractinfo['nDiscount'] + $contractinfo['nVatPrice']+ $contractinfo['nManagedCharge'];

        # 프로젝트 정보
        $data['info'] = $contractinfo;

        # 클라우드 과금 정보를 view 출력용 정보 가공 및 과금금액별(기준금액, 기본할인, 조정, 할인, 청구) 합계 금액 추출
        list($data['resource'], $data['sum']) = $this->_set_cloud_detail_viewdata($detail_setdata);

        $this->_print($data);
    }


    /**
     * IxCloud 과금 상세정보 조회
     */
    public function clouddetail_excel()
    {
        $postdata = $this->input->post(NULL, TRUE);

        if(!element('qesdetailbill', $postdata)){
            throw new Exception("잘못된 접근입니다.", 400);
        }

        # cloud 정보 처리를 위한 helper load
        $this->load->helper('ixcloud');

        # nBillDetailSeq복호화
        $billdetailseq = decryptIt($postdata['qesdetailbill']);

        # Cloud 계약 기준 정보 추출
        $contractinfo  = $this->md_taxinvoice_detail->get_contract_for_cloud($billdetailseq);

        # Cloud 상품 구분
        $product = ($contractinfo['nProductSeq']==320) ? 'multizone' : 'ixcloud';

        # 리소스별상세 과금정보 추출 및 추가 조정금액 추출
        list($detail_setdata, $chk_adjcharge) = $this->_get_cloud_detail_viewdata($product, $billdetailseq, 'excel');

        # 추가 조정금액 등록
        $contractinfo['nAddAdjCharge'] = $chk_adjcharge;

        # json 변환
        $contractinfo['arDetailInfo']  = chg_arrayfield($contractinfo['arDetailInfo']);
        $contractinfo['arDiscountOpt'] = chg_arrayfield($contractinfo['arDiscountOpt']);
        $contractinfo['lastCharge']    = $contractinfo['nCharge'] + $contractinfo['nAdjCharge'] + $contractinfo['nDiscount'] + $contractinfo['nVatPrice']+ $contractinfo['nManagedCharge'];

        # 프로젝트 정보
        $data['info'][]     = $contractinfo;
        $data['resource'][] = $detail_setdata;

        if($product=='ixcloud')
        {
            $data['filename']   = 'IxCloud 과금상세('.$contractinfo['sCustomerName'].'-'.$contractinfo['arDetailInfo']['project']['name'].'-'.substr($contractinfo['dtBilling'],0,7).').xlsx';
        }
        elseif($product=='multizone')
        {
            $data['filename']   = 'IxCloud 과금상세('.$contractinfo['sCustomerName'].'-멀티존-'.substr($contractinfo['dtBilling'],0,7).').xlsx';
        }




        # 엑셀 생성 라이브러리 로드
        $this->load->library('excel');



        # view 페이지 로드
        $this->load->view('payment/excel_for_cloud_view', $data);
    }







    /**
     * 계약 기준 Cloud 리소스들 과금 정보 추출
     */
    private function _get_cloud_detail_viewdata($product, $billdetailseq, $type="view")
    {
        $chk_adjcharge  = 0;

        # 기존 정보
        $detail_setdata = ($type=='excel') ? get_cloud_resource_excel_detailset(NULL, NULL, $product) :  get_cloud_resource_mykinx_detailset(NULL, NULL, $product);

        # 클라우드 리소스 정보 추출
        if($list = $this->md_taxinvoice_detail->get_contract_cloudresource($billdetailseq))
        {
            foreach($list as $list_row)
            {
                $detailinfo = chg_arrayfield($list_row['arDetailInfo']);
                $resoDetail = chg_arrayfield($list_row['arResourceDetail']);

                $basic = array('basic'=>null);
                if(!is_null($list_row['arResourceDetail']))
                {
                    $basic = chg_arrayfield($list_row['arResourceDetail']);
                }

                $size = isset($detailinfo['detail']['size'])?$detailinfo['detail']['size']:0;
                $size = ($list_row['sType']=='IVS' && isset($detailinfo['detail']['volume_size']))?$detailinfo['detail']['volume_size']:$size;
                $size = ($list_row['sType']=='ISS' && $size > 0) ? $size/pow(1024,3) : $size;
                $size = ($list_row['sType']=='BKS' && $size > 0) ? $size/pow(1024,3) : $size;
                $size = ($list_row['sType']=='MCR' && isset($resoDetail['basic']['nAvgSizeGB'])) ? $resoDetail['basic']['nAvgSizeGB'] : $size;

                $trname = isset($detailinfo['detail']['traffic_type'])?$detailinfo['detail']['traffic_type']:'';

                if($trname =='free' ){ $trname = '무료 트래픽'; }
                elseif($trname =='inside'){ $trname = 'KINX 서비스 트래픽'; }
                elseif($trname =='public'){ $trname = '외부 트래픽'; }


                $detail_setdata[$list_row['sType']]['list'][] = array(
                    'name'    => $detailinfo['detail']['display_name'],
                    'date'    => str_replace("-",".", substr($list_row['dtUseStart'],0,10)." ~ ".substr(nvl($list_row['dtUseEnd'],date('Y-m-t')),0,10)),
                    'product' => $list_row['sName'],
                    'tr_type' => $trname,
                    'kp_type' => isset($detailinfo['detail']['bandwidth'])?$detailinfo['detail']['bandwidth'].'Mbps':'-',
                    'vpn'     => isset($detailinfo['detail']['bandwidth_mbps']) ? $detailinfo['detail']['bandwidth_mbps'].'Mbps' : '-',
                    'lb_type' => chk_element('lb_provider', $detailinfo['detail'], ''),
                    'vo_type' => chk_element('volume_type_name', $detailinfo['detail'], ''),
                    'mzport'  => chk_element('bandwidth', $detailinfo['detail'], ''),
                    'wl_type' => chk_element('type', $detailinfo['detail'], ''),
                    'os_type' => chk_element('os', $detailinfo['detail'], ''),
                    'charge'  => chk_element('charge_type', $detailinfo['detail'], ''),
                    'size'    => $size.' Gbyte',
                    'bytein'  => (!is_null($basic) && $list_row['sType']== 'ITF') ? number_format(ceil($basic['basic']['nBytesIn']/1024/1024/1024)) : 0,
                    'byteout' => (!is_null($basic) && $list_row['sType']== 'ITF') ? number_format(ceil($basic['basic']['nBytesOut']/1024/1024/1024)) : 0,
                    'price'   => $list_row['nPrice'],
                    'hour'    => (!is_null($basic)) ? number_format(ceil($basic['basic']['nActiveSec']/60/60)) : 0,
                    'mxiops'  => element('billing_max_IOPS', element('detail', $detailinfo, array()), 0)
                );

                $detail_setdata[$list_row['sType']]['price']     += $list_row['nPrice'];
                $detail_setdata[$list_row['sType']]['bdiscount'] += $list_row['nDiscount'];
                $detail_setdata[$list_row['sType']]['adjcharge'] += $list_row['nAdjCharge'];
                $detail_setdata[$list_row['sType']]['charge']    += $list_row['nCharge'];

                $chk_adjcharge += $list_row['nAdjCharge'];

                unset($basic);
            }
        }


        # 리소스타입별 조정내역 확인하여 조정 금액 추출
        if($adjustlist = $this->md_taxinvoice_detail->get_contract_cloudresource_group($billdetailseq))
        {
            foreach($adjustlist as $adjustlist_row)
            {
                $detail_setdata[$adjustlist_row['sType']]['adjcharge'] += $adjustlist_row['nAdjCharge'];

                $chk_adjcharge += $adjustlist_row['nAdjCharge'];
            }
        }
        unset($adjustlist);


        # 리소스타입별 할인금액 추출
        if($discountlist = $this->md_taxinvoice_detail->get_discount_by_billdetailseq($billdetailseq))
        {
            foreach($discountlist as $discountlist_row)
            {
                $detail_setdata[$discountlist_row['sType']]['discount'] += $discountlist_row['nDiscount'];
            }
        }
        unset($discountlist);


        return array($detail_setdata, $chk_adjcharge);
    }


    /**
     * 클라우드 과금 정보를 view 출력용 정보 가공 및 과금금액별(기준금액, 기본할인, 조정, 할인, 청구) 합계 금액 추출
     */
    private function _set_cloud_detail_viewdata($detail_setdata)
    {
        $sum['sum_price']     = 0;
        $sum['sum_bdiscount'] = 0;
        $sum['sum_adjcharge'] = 0;
        $sum['sum_discount']  = 0;
        $sum['sum_charge']    = 0;

        foreach($detail_setdata as $res_key=>$res_row)
        {
            $sum['sum_price']     += $res_row['price'];
            $sum['sum_bdiscount'] += $res_row['bdiscount'];
            $sum['sum_adjcharge'] += $res_row['adjcharge'];
            $sum['sum_discount']  += $res_row['discount'];
            $sum['sum_charge']    += $res_row['price']+$res_row['discount']+$res_row['adjcharge']+$res_row['bdiscount'];

            foreach ($res_row['listkey'] as $key=>$res_listkey_row)
            {
                $res_row['listkey'][$key] = explode("||", $res_listkey_row);
            }

            foreach ($res_row['listkey'] as $res_listkey_key=>$res_listkey_row)
            {
                $detail_setdata[$res_key]['colname'][$res_listkey_key]= array(
                    'name'    => $res_row['colname'][$res_listkey_key],
                    'colspan' => $res_listkey_row[2]
                );
            }

            if(count($res_row['list']) > 0)
            {
                foreach($res_row['list'] as $key=>$res_list_row)
                {
                    foreach ($res_row['listkey'] as $res_listkey_row)
                    {
                        # 클라우드운영G 요청으로 Supreme IOPS 표시 추가 적용 20201116 SYZ
                        $res_list_val  = $res_list_row[$res_listkey_row[0]];
                        $res_list_val .= ($res_listkey_row[0]=='size' && $res_list_row['product']=='Supreme') ? ' / '.number_format($res_list_row['mxiops']).' IOPS' : '';

                        $detail_setdata[$res_key]['list_data'][$key][] = array(
                            'value'   => $res_list_val,
                            'number'  => (($res_listkey_row[1]=='right') ? 1 : 0),
                            'colspan' => $res_listkey_row[2]
                        );
                    }
                }
            }

        }

        return array($detail_setdata,$sum);
    }
}