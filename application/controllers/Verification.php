<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/Common.php");

class Verification extends Common{

    public function __construct()
    {
        parent::__construct();

        /*
        if($this->is_login === false)
        {
            $this->output->set_status_header('410');
            $this->tinyjs->pageRedirect("/", "로그인 후 접근가능합니다. verification");
            exit;
        }
        */

        $this->load->model("account_ism_model","md_account");
    }


    public function index($key=null)
    {
        if(empty($key))
        {
            //$check_result = 400;
            $this->output->set_status_header('400');//410
            $this->tinyjs->pageRedirect("/auth/sign_out.html", "이미 사용되었거나 잘못된 인증키입니다.");
            exit;
        }
        else
        {
            //1. key 관련 값 가져오기
            $useinfo = $this->md_account->get_accountuseinfo_byKey($key);
            //print_r($key);
            //print_r($useinfo);die;

            if(empty($useinfo))
            {
                //$check_result = 400;
                $this->output->set_status_header('400');//410
                $this->tinyjs->pageRedirect("/auth/sign_out.html", "이미 사용되었거나 잘못된 인증키입니다.");
                exit;
            }
            else
            {
                $check_result = 200;
                $nAccountSeq = $useinfo['nAccountSeq'];// 2662; //test

                //0. 이미 인증이 완료되었는지 확인 - tAccount 조사
                $account = $this->md_account->get_account_bySeq($nAccountSeq);
                if($account['sConfirmUseInfoFlag'] == 'Y')
                {
                    $this->output->set_status_header('400');//410
                    $this->tinyjs->pageRedirect("/auth/sign_out.html", "이미 사용되었거나 잘못된 인증키입니다.");
                    exit;
                }


                //tAccountUseInfo 조사. bDel== 'N'인 최근 입력하나 구해서 비교
                $recent_useinfo = $this->md_account->get_accountuseinfo_recent($nAccountSeq);
                //print_r($recent_useinfo);die;
                if($recent_useinfo['sKey'] != $key)
                {
                    $this->output->set_status_header('400');//410
                    $this->tinyjs->pageRedirect("/auth/sign_out.html", "이미 사용되었거나 잘못된 인증키입니다.");
                    exit;
                }

                //211018 인증기준 : url 클릭 => 비밀번호 설정


                //2. 비밀번호 변경 페이지 노출
                //회원정보 추출
                $data["account"] = $this->md_account->get_account_bySeq($nAccountSeq);
                $data["key"] = $key;
                //print_r($data);die;
                $this->_print($data);
            }

        }


    }


    //비밀번호 설정
    public function proc()
    {
        $result = array();
        $this->load->library("form_validation");
        if($this->form_validation->run("member"))
        {
            $this->load->helper("encryption");

            $postdata = $this->input->post(NULL, TRUE);
            //print_r($postdata);die;

            // 있는지 검사
            $row = $this->md_account->get_account_byId($postdata["sAccountID"]);

            //password 암호화
            //$encpw = password_hash($postdata['sPassword'], PASSWORD_BCRYPT,array("cost" => 10, 'salt'=>PASSWORD_SALT) );
            //php7 버전에서 salt 옵션 제거. 171108
            $encpw = password_hash($postdata['sPassword'], PASSWORD_BCRYPT );

            // 있다면 업데이트
            if(isset($row["nAccountSeq"]))
            {
                //for - ism
                $where = array(
                        'nAccountSeq'=>$postdata["nAccountSeq"]
                );

                $change_data = array(
                        'sPassword'=> $encpw,
                        'rePassword'=>$postdata['rePassword'],
                        'sConfirmuseInfoFlag'=> 'Y' //비밀번호 변경으로 인증완료처리함. 211018
                );

                $this->md_account->update($change_data, $where);

                //start - tAccountUseInfo.sStatus = 2
                unset($where);
                $key = $postdata["key"];
                unset($where);
                $where = array(
                    'sKey'=>$key
                );
                unset($change_data);
                $change_data = array(
                    'sStatus'=> '2',
                    'dtConfirm' => date("Y-m-d H:i:s")
                );
                $this->md_account->update_useinfo($change_data, $where);
                //-end

                //for intranet
                $this->load->model("contract_model");
                $where_intra["nContractSeq"] = $postdata["nAccountSeq"];
                $postdata['sPassword'] = $encpw;

                unset($postdata['sAccountID']);
                unset($postdata['nAccountSeq']);
                unset($postdata['key']);


                $this->contract_model->_update($postdata, $where_intra);
            }
            else// 없다면 에러
            {
                $result['code'] = 'f';
                $result['msg'] ="고객정보가 확인되지 않습니다.";
            }
            $result['code'] = 's';
            $result['msg'] ="정상적으로 설정되었습니다.";
        }
        else
        {
            $result['code'] = 'f';
            $result['msg'] =strip_tags(validation_errors());
        }
        echo(json_encode($result));
        exit;

    }
}