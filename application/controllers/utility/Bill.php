<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/Common.php");
class Bill extends Common
{
    public function __construct() {
        parent::__construct();
    }
    public function index(){
        print_r($this->input->get());
    }
    public function get_billaccount() { //asp 버전. 사용안함.190809 혹시 몰라 인감 이미지 제거
        /*
        |--------------------------------------------------------------------------
        | 세금계산서 KINX 설정
        |--------------------------------------------------------------------------
        |
        */
        define('TAX_COM_BUSINESS_NUMBER',       '101-81-59273');    //사업자등록번호
        define('TAX_COM_SLAVE_BUSINESS_NUMBER', '');    //종사업장번호
        define('TAX_COM_COMPANY_NAME',          '(주)케이아이엔엑스');    //상호
        define('TAX_COM_NAME',                  '이선영');    //대표자
        define('TAX_COM_ADDRESS',               '서울시 강남구 도곡동 467-6 대림아크로텔 5층');    //주소
        define('TAX_COM_BUSINESS_TYPE',         '서비스');    //업태
        define('TAX_COM_SERVICE_ITEM',          '인터넷연동회선접속');    //종목
        define('BANK_ACCOUNT',                  '신한은행 : 140-004-504074 (주)케이아이엔엑스, (구 274-05-013296 입금가능)');    //입금계좌

        $iNo = $this->input->get('iNo');

        if(!empty($iNo)){
            $seq = explode('|',$iNo); // 0-taxseq, 1- contractseq, 2-billlistseq
            $this->db->where('nTaxInvoiceSeq',$seq[0]);
            $query = $this->db->get('vTaxInvoicePreview');
            if ($query->num_rows() > 0){
                $view_data['list'] = $query->result();
                //var_dump($view_data['list']);

                $this->db->where('nTaxInvoiceSeq',$seq[0]);
                $query1 = $this->db->get('vTaxInvoiceServiceList');
                if ($query->num_rows() > 0){
                    $view_data['servicelist'] = $query1->result();
                }

            }else{
                $this->errMsg = "조회된 데이터가 없습니다.";
            }
            # 서비스리스트
            $sMailDocumentBody = "";
            if(!is_null($view_data['servicelist'])):
                if(count($view_data['servicelist'])>6){
                 $cnt = count($view_data['servicelist'])-1;
                }else{
                 $cnt = 6;
                };
                $nTotalContactPrice = 0;
                $nTotalTaxPrice = 0;
                for($i=0;$cnt>=$i;$i++):
                     if(isset($view_data['servicelist'][$i]->sServiceName)){
                         $ser_name = $view_data['servicelist'][$i]->sServiceName;
                     }else{
                         $ser_name = "";
                     }
                     if(isset($view_data['servicelist'][$i]->nContractPrice)){
                         $nprice = number_format($view_data['servicelist'][$i]->nContractPrice);
                         $nTotalContactPrice += $view_data['servicelist'][$i]->nContractPrice;
                     }else{
                         $nprice = "";
                     }
                     if(isset($view_data['servicelist'][$i]->nTaxPrice)){
                         $ntaxprice = number_format($view_data['servicelist'][$i]->nTaxPrice);
                         $nTotalTaxPrice += $view_data['servicelist'][$i]->nTaxPrice;
                     }else{
                         $ntaxprice = "";
                     }
                     if(isset($view_data['servicelist'][$i]->sServiceDetail)){
                         $staxdetail = $view_data['servicelist'][$i]->sServiceDetail;
                     }else{
                         $staxdetail = "";
                     }
                     if(isset($view_data['servicelist'][$i]->nQuantity)){
                         $ntaxquantity = number_format($view_data['servicelist'][$i]->nQuantity);
                     }else{
                         $ntaxquantity = "";
                     }
                     $sMailDocumentBody .= "<tr style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'><th scope='row' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>";
                     $sMailDocumentBody .= $i+1;
                     $sMailDocumentBody .= "</th><td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>".$ser_name."</td>
                     <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 10px 5px;' align='center'>".$staxdetail."</td>
                     <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 10px 5px;' align='center'>".$ntaxquantity."</td>
                     <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: right; margin: 0; padding: 7px 10px 5px;' align='right'>".$nprice."</td>
                     <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: right; margin: 0; padding: 7px 10px 5px;' align='right'>".$ntaxprice."</td></tr>";
                endfor;
            endif;

            $sMailDocumentTop ="<!DOCTYPE html><html lang='ko'><head>
            <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
            <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
            <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1'>
            <meta http-equiv='Content-Script-Type' content='text/javascript' />
            <meta http-equiv='Content-Style-Type' content='text/css' /><title>거래명세서</title>
            <style>
            body,html{margin:0;padding:0;}
            *{margin:0;padding:0;}
            .taxmark{position:absolute; top:180px;left:250px;}
            .taxmark img{width: 80px;height: 77px;}
            .dealbill_wrap {min-width:660px;margin:0 auto;border:0 none;padding:20px}
            .dealbill_wrap h2 {text-align:center;font-size:18px;color:#000;margin-bottom:20px}
            .dealbill_wrap ul {margin-bottom:10px}
            .dealbill_wrap li {font-size:15px;font-weight:bold;color:#666;letter-spacing:-1px}
            .dealbill_wrap li span {font-size:15px;font-weight:bold;color:#000;letter-spacing:-1px}
            .dealbill_wrap table {border:2px solid #000}
            .dealbill_wrap caption {height:0}
            .dealbill_wrap th {border-left:1px solid #000;border-bottom:1px solid #000;text-align:center;padding:7px 0 5px 0;color:#000;line-height:14px}
            .dealbill_wrap td {border-left:1px solid #000;border-bottom:1px solid #000;text-align:left;padding:7px 10px 5px 10px;color:#666;line-height:14px}
            .dealbill_wrap td.center {text-align:center}
            .dealbill_wrap td.right {text-align:right}
            .dealbill_wrap table strong {color:#000}
            .dealbill_wrap .sum {text-align:right;font-size:16px;font-weight:bold;color:#666;line-height:40px}
            .dealbill_wrap .sum span {font-size:16px;font-weight:bold;color:#000;line-height:40px}
            </style>
            </head><body onload='window.print();'><div class='dealbill_wrap popprint' id='taxSendPrt1' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 16px; padding: 0;'>
                <div style='position:relative;font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; width: 660px; margin: 0 auto; padding: 20px; border: 1px solid #000;'>

                    <h2 style='font-family: dotum, gulim, Arial, sans-serif; font-size: 18px; color: #000; line-height: 28px; font-weight: bold; text-align: center; margin: 0 0 20px; padding: 0 0 0 12px;' align='center'>거래명세서</h2>
                    <ul style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; list-style-type: none; margin: 0 0 10px; padding: 0;'>
                        <li style='font-family: dotum, gulim, Arial, sans-serif; font-size: 15px; color: #666; line-height: 20px; font-weight: bold; letter-spacing: -1px; margin: 0; padding: 0;'>
                        <span style='font-family: dotum, gulim, Arial, sans-serif; font-size: 15px; color: #000; line-height: 20px; font-weight: bold; letter-spacing: -1px; margin: 0; padding: 0;'>사용기간 :</span> ".$view_data['list'][0]->sUseDate."</li>
                        <li style='font-family: dotum, gulim, Arial, sans-serif; font-size: 15px; color: #666; line-height: 20px; font-weight: bold; letter-spacing: -1px; margin: 0; padding: 0;'>
                        <span style='font-family: dotum, gulim, Arial, sans-serif; font-size: 15px; color: #000; line-height: 20px; font-weight: bold; letter-spacing: -1px; margin: 0; padding: 0;'>작 성 일  :</span> ".date('Y-m-d', strtotime($view_data['list'][0]->dtChargeDate))."</li>
                        <li style='font-family: dotum, gulim, Arial, sans-serif; font-size: 15px; color: #666; line-height: 20px; font-weight: bold; letter-spacing: -1px; margin: 0; padding: 0;'>
                        <span style='font-family: dotum, gulim, Arial, sans-serif; font-size: 15px; color: #000; line-height: 20px; font-weight: bold; letter-spacing: -1px; margin: 0; padding: 0;'>납 기 일  :</span> ".date("Y-m-d", strtotime($view_data['list'][0]->dtPaymentDate))."</li>
                    </ul>
                    <table summary='공급하는자/공급받는자' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; width: 100%; height: 100%; vertical-align: middle; border-collapse: collapse; margin: 0; padding: 0; border: 2px solid #000;'>
                    <caption style='font-family: dotum, gulim, Arial, sans-serif; font-size: 0; color: #585858; line-height: 0; visibility: hidden; overflow: hidden; width: 1px; height: 0; margin: 0; padding: 0;'>공급하는자/공급받는자</caption>
                    <colgroup style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                        <col width='5%' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                        <col width='12%' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                        <col width='33%' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                        <col width='5%' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                        <col width='12%' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                        <col width='33%' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                    </colgroup>
                    <tbody style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                    <tr style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                        <th scope='row' rowspan='6' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>공<br style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>급<br style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>하<br style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>는<br style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>자<br style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'></th>
                        <th scope='row' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>등록번호</th>
                        <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>".TAX_COM_BUSINESS_NUMBER."</td>
                        <th scope='row' rowspan='6' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>공<br style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>급<br style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>받<br style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>는<br style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>자<br style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'></th>
                        <th scope='row' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>등록번호</th>
                        <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>".$view_data['list'][0]->sBusinessNumber."</td>
                    </tr>
                    <tr style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                         <th scope='row' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>회사명</th>
                        <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>".TAX_COM_COMPANY_NAME."</td>
                        <th scope='row' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>회사명<br style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>(개인명)</th>
                        <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>".$view_data['list'][0]->sCompanyName."</td>
                    </tr>
                    <tr style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                        <th scope='row' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>대표자명</th>
                        <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>".TAX_COM_NAME."</td>
                        <th scope='row' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>대표자명</th>
                        <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>".$view_data['list'][0]->sPresidentName."</td>
                    </tr>
                    <tr style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                        <th scope='row' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>주 소</th>
                        <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>".TAX_COM_ADDRESS."</td>
                        <th scope='row' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>주 소</th>
                        <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>".$view_data['list'][0]->sAddress."</td>
                    </tr>
                    <tr style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                        <th scope='row' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>업 태</th>
                        <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>".TAX_COM_BUSINESS_TYPE."</td>
                        <th scope='row' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>업 태</th>
                        <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>".$view_data['list'][0]->sBusinessType."</td>
                    </tr>
                    <tr style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                        <th scope='row' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>종 목</th>
                        <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>".TAX_COM_SERVICE_ITEM."</td>
                        <th scope='row' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>종 목</th>
                        <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>".$view_data['list'][0]->sItemType."</td>
                    </tr>
                </tbody>
            </table>

            <p style='font-family: dotum, gulim, Arial, sans-serif; font-size: 16px; color: #666; line-height: 40px; text-align: right; font-weight: bold; margin: 0; padding: 0;' align='right'><span style='font-family: dotum, gulim, Arial, sans-serif; font-size: 16px; color: #000; line-height: 40px; font-weight: bold; margin: 0; padding: 0;'>합계 :</span> ".number_format($nTotalContactPrice+$nTotalTaxPrice)."원</p>
            <table summary='거래명세서' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; width: 100%; height: 100%; vertical-align: middle; border-collapse: collapse; margin: 0; padding: 0; border: 2px solid #000;'>
                <caption style='font-family: dotum, gulim, Arial, sans-serif; font-size: 0; color: #585858; line-height: 0; visibility: hidden; overflow: hidden; width: 1px; height: 0; margin: 0; padding: 0;'>거래명세서</caption>
                <colgroup style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                    <col width='5%' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                    <col width='45%' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                    <col width='17%' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                    <col width='9%' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                    <col width='12%' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                    <col width='12%' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                </colgroup>
                <thead style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'><tr style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                <th scope='col' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>No</th>
                    <th scope='col' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>서비스명</th>
                    <th scope='col' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>세부사항</th>
                    <th scope='col' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>수량</th>
                    <th scope='col' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>계약금액</th>
                    <th scope='col' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>세액</th>
                </tr>
                </thead>
                <tbody style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>";

                $sMailDocumentBody .= "</tbody>
                </table>
                <p style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; height: 40px; margin: 0; padding: 0;'></p>
                <table summary='입금계좌안내/합계금액' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; width: 100%; height: 100%; vertical-align: middle; border-collapse: collapse; margin: 0; padding: 0; border: 2px solid #000;'>
                <caption style='font-family: dotum, gulim, Arial, sans-serif; font-size: 0; color: #585858; line-height: 0; visibility: hidden; overflow: hidden; width: 1px; height: 0; margin: 0; padding: 0;'>입금계좌안내/합계금액</caption>
                <colgroup style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                    <col width='' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                    <col width='12%' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                    <col width='12%' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                </colgroup>
                <tbody style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                    <tr style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                        <td rowspan='3' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>
                        <strong style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 20px; margin: 0; padding: 0;'>[입금계좌안내]</strong><br style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                                        ".BANK_ACCOUNT."</td>
                        <th scope='row' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>공급가액</th>
                        <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: right; margin: 0; padding: 7px 10px 5px;' align='right'>".number_format($nTotalContactPrice)."</td>
                    </tr>
                    <tr style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                        <th scope='row' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>부가세</th>
                        <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: right; margin: 0; padding: 7px 10px 5px;' align='right'>".number_format($nTotalTaxPrice)."</td>
                    </tr>
                    <tr style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'>
                        <th scope='row' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>합계</th>
                        <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: right; margin: 0; padding: 7px 10px 5px;' align='right'>".number_format($nTotalContactPrice+$nTotalTaxPrice)."</td>
                    </tr>
                </tbody>
                </table>
                </div>
            </div>";
            $sMailDocument = $sMailDocumentTop.$sMailDocumentBody;
            echo $sMailDocument;
            die;
        }else{
            //잘못된 접근
            $this->errorMsg="잘못된 접근입니다. 데이터가 없습니다.";
        }
        /*
        $this->load->library('rest', 'restAccount');
        $this->rest->initialize(array('server'=>'http://203.246.160.185/'));

        $url = 'down/print/detail_account.asp';
        $ret = $this->rest->post($url, array('iNo'=> $iNo));
        $ret = str_replace("/Lib/style.css", "/styles/old_bill_account.css", $ret);
        $ret = str_replace("/down/print/tax_account_mark.png","/images/old_bill_account/tax_account_mark.png",$ret);

        echo $ret;
        */
    }

    //171016, 수량단위표기, 미수금액 표기 추가. 거래명세서 출력. php
    public function get_billtax()
    {
        /*
        |--------------------------------------------------------------------------
        | 세금계산서 KINX 설정
        |--------------------------------------------------------------------------
        |
        */

        $iNo = $this->input->get('iNo');

        if(!empty($iNo))
        {
            $seq = explode('|',$iNo); // 0-taxseq, 1- contractseq, 2-billlistseq
            if(count($seq)==4 && seed($seq[3],FALSE)=='ismtax') //ism 거래명세서에서 인쇄시 호출
            {
                $seq[0] = seed($seq[0],FALSE);
                $seq[1] = seed($seq[1],FALSE);
                $seq[2] = seed($seq[2],FALSE);
                //va('seq[0] taxinvoice'); va($seq[0]);
                //va('seq[1] contract'); va($seq[1]);
                //va('seq[2] billlistseq'); va($seq[2]);

                //test lizzy 171017 --------------------------
                /*
                //krw
                $seq[0] = 39902;     //nTaxInvoiceSeq
                $seq[1] = 1618;         //nAccountSeq
                $seq[2] = 61423;     //nBillMonthSeq
                */
                /*
                //usd
                $seq[0] = 39877;
                $seq[1] = 1484;
                $seq[2] = 61180;
                */
                //end


                # db 객체 load
                $this->load->model("taxinvoice_ism_model","md_tax");

                # 세금계산서 정보 추출
                $rtn = $this->md_tax->get_taxinvoice_by_seq($seq[0]);

                if(!isset($rtn) || is_null($rtn))
                {
                    echo "<script>alert('거래명세서 정보가 일치하지 않습니다.');</script>";
                    exit();
                }

                $view_data['list'][0] = $rtn;


                // 미수금 처리
                // 미수금 부과세 적용
                //$view_data['list'][0]->nAccountReceivables = ($view_data['list'][0]->nAccountReceivables>0)? (int)vat($view_data['list'][0]->nAccountReceivables,TRUE,TRUE,TRUE):0;
                //미수금의 부과세를 적용하지 않음. 181106

                //명세서 발생유형에 따른 미수금 선택
                if($view_data['list'][0]->sSpecsCurrencyType=='USD')
                {
                    $view_data['list'][0]->nAccountReceivables = $view_data['list'][0]->nUSDReceivables;
                }

                // 미수금 (-)금액일 경우 0으로 표시
                if($view_data['list'][0]->nAccountReceivables < 0)
                {
                    $view_data['list'][0]->nAccountReceivables = 0;
                }


                if($view_data['list'][0]->dtBilling >= '2018-04-01')
                {
                    //이선영 대표님 퇴직으로 인해 대표이사 명 수정 2020-04-10
                    //세금계산서상의 작성일자가 4월 9일를 포함한 이후 날짜는 모두 변경
                    if($view_data['list'][0]->dtSend >= '2020-04-09')
                    {
                        define('TAX_COM_BUSINESS_NUMBER',       '101-81-59273');    //사업자등록번호
                        define('TAX_COM_SLAVE_BUSINESS_NUMBER', '');    //종사업장번호
                        define('TAX_COM_COMPANY_NAME',          '(주)케이아이엔엑스');    //상호
                        define('TAX_COM_NAME',                  '김지욱');    //대표자
                        define('TAX_COM_ADDRESS',               '서울시 서초구 서초대로 396, 21층(서초동, 강남빌딩)');    //주소
                        define('TAX_COM_BUSINESS_TYPE',         '서비스');    //업태
                        define('TAX_COM_SERVICE_ITEM',          '인터넷연동회선접속');    //종목
                        define('BANK_ACCOUNT',                  '신한은행 : 140-004-504074 (주)케이아이엔엑스, (구 274-05-013296 입금가능)');    //입금계좌
                    }
                    else
                    {
                        define('TAX_COM_BUSINESS_NUMBER',       '101-81-59273');    //사업자등록번호
                        define('TAX_COM_SLAVE_BUSINESS_NUMBER', '');    //종사업장번호
                        define('TAX_COM_COMPANY_NAME',          '(주)케이아이엔엑스');    //상호
                        define('TAX_COM_NAME',                  '이선영, 김지욱');    //대표자
                        define('TAX_COM_ADDRESS',               '서울시 서초구 서초대로 396, 21층(서초동, 강남빌딩)');    //주소
                        define('TAX_COM_BUSINESS_TYPE',         '서비스');    //업태
                        define('TAX_COM_SERVICE_ITEM',          '인터넷연동회선접속');    //종목
                        define('BANK_ACCOUNT',                  '신한은행 : 140-004-504074 (주)케이아이엔엑스, (구 274-05-013296 입금가능)');    //입금계좌
                    }
                }
                else{
                    define('TAX_COM_BUSINESS_NUMBER',       '101-81-59273');    //사업자등록번호
                    define('TAX_COM_SLAVE_BUSINESS_NUMBER', '');    //종사업장번호
                    define('TAX_COM_COMPANY_NAME',          '(주)케이아이엔엑스');    //상호
                    define('TAX_COM_NAME',                  '이선영');    //대표자
                    define('TAX_COM_ADDRESS',               '서울시 강남구 도곡동 467-6 대림아크로텔 5층');    //주소
                    define('TAX_COM_BUSINESS_TYPE',         '서비스');    //업태
                    define('TAX_COM_SERVICE_ITEM',          '인터넷연동회선접속');    //종목
                    define('BANK_ACCOUNT',                  '신한은행 : 140-004-504074 (주)케이아이엔엑스, (구 274-05-013296 입금가능)');    //입금계좌
                }

                # 데이터 보정
                $view_data['list'][0]->dtChargeDate     = $view_data['list'][0]->dtSend;
                $view_data['list'][0]->dtPaymentDate    = $view_data['list'][0]->dtPayment;
                $view_data['list'][0]->sBusinessNumber  = $view_data['list'][0]->sBusinessNo;
                $view_data['list'][0]->sCompanyName     = $view_data['list'][0]->sCompanyNm;
                $view_data['list'][0]->sPresidentName   = $view_data['list'][0]->sOwnerNm;

                # 세금계산서 상세 정보 추출
                $view_data['servicelist']=$this->md_tax->get_taxdetail_list($seq[0]);

            }//end if(count ~~~ ismtax)
            else
            {
                echo "<script>alert('서비스 변경으로 인해 2017년 07월 이전 인쇄페이지는 지원하지 않습니다.\\n인쇄페이지가 필요하시면 서비스담당자에게 연락 부탁 드립니다.');</script>";
                exit();
//                $seq[0] = decryptIt($seq[0]);
//                $seq[1] = decryptIt($seq[1]);
//                $seq[2] = decryptIt($seq[2]);
//                $this->db->where('nTaxInvoiceSeq',$seq[0]);
//                $query = $this->db->get('vTaxInvoicePreview');
//                if ($query->num_rows() > 0)
//                {
//
//                        $view_data['list'] = $query->result();
//
//                        # 데이터 가공
//                        $view_data['list'][0]->sRemark= str_replace('[입금계좌안내] 신한은행 : 140-004-504074 (주)케이아이엔엑스, (구 274-05-013296 입금가능)', '', $view_data['list'][0]->sContent);
//                        $view_data['list'][0]->sCurrencyType = 'KRW';
//                        //var_dump($view_data['list']);
//
//                        $this->db->where('nTaxInvoiceSeq',$seq[0]);
//                        $query1 = $this->db->get('vTaxInvoiceServiceList');
//                        if ($query->num_rows() > 0)
//                        {
//                                $view_data['servicelist'] = $query1->result();
//                        }
//                }
//                else
//                {
//                        $this->errMsg = "조회된 데이터가 없습니다.";
//                }
            }


//            va($seq);
//            va($view_data);
//            die;
//            $view_data['list'][0]->sSpecsCurrencyType='USD';

            # 통화유형에 따른 금액 선택
            $chargefield= ($view_data['list'][0]->sCurrencyType <> $view_data['list'][0]->sSpecsCurrencyType) ? 'nContractExcPrice' : 'nContractPrice';
            $fieldname  = ($view_data['list'][0]->sSpecsCurrencyType=='USD') ? 'Eng' : '';


            # 서비스리스트
            $currType = 'KRW'; //171018
            $sMailDocumentBody = "";
            if(!is_null($view_data['servicelist']))
            {
                $cnt = (count($view_data['servicelist'])>6) ? count($view_data['servicelist'])-1 : 6;
                $nTotalContactPrice = 0;
                $nTotalTaxPrice = 0;


                for($i=0;$cnt>=$i;$i++)
                {
                    if(!isset($view_data['servicelist'][$i])){
                        $ser_name     = "";
                        $staxdetail   = "";
                        $sunit        = "";
                        $ntaxpriceStr = "";
                        $npriceStr    = "";
                        $ntaxquantity = "";
                    }
                    else{
                        $ser_name     = ($view_data['list'][0]->sSpecsCurrencyType=='USD') ? $view_data['servicelist'][$i]->sServiceName : $view_data['servicelist'][$i]->sServiceName;
                        $staxdetail   = ($view_data['list'][0]->sSpecsCurrencyType=='USD') ? $view_data['servicelist'][$i]->sServiceDetailEng : $view_data['servicelist'][$i]->sServiceDetail;
                        $ntaxquantity = number_format($view_data['servicelist'][$i]->nQuantity);
                        $sunit        = $view_data['servicelist'][$i]->sUnit;
                        $nprice       = $view_data['servicelist'][$i]->$chargefield;
                        $ntaxprice    = ($view_data['list'][0]->sSpecsCurrencyType=='USD') ? 0 : $view_data['servicelist'][$i]->nTaxPrice;
                        $npriceStr    = (number_format_charge($nprice,$view_data['list'][0]->sSpecsCurrencyType));
                        $ntaxpriceStr = (number_format_charge($ntaxprice,$view_data['list'][0]->sSpecsCurrencyType));


                        $nTotalContactPrice += $nprice;
                        $nTotalTaxPrice     += $ntaxprice;
                    }

                    $tdStyle_center = "style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 10px 5px;' align='center' ";
                    $tdStyle_right = " style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: right; margin: 0; padding: 7px 10px 5px;' align='right' ";

                    $sMailDocumentBody .= "<tr style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;'><th scope='row' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px;' align='center'>";
                    $sMailDocumentBody .= $i+1;
                    $sMailDocumentBody .= "</th><td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>".$ser_name."</td>
                         <td ".$tdStyle_center.">".$staxdetail."</td>
                         <td ".$tdStyle_center.">".$ntaxquantity."  ".$sunit."</td>
                         <td".$tdStyle_right.">".$npriceStr."</td>
                         <td".$tdStyle_right.">".$ntaxpriceStr."</td></tr>
                    ";
                }//end for

                $nTotalPrice  = $nTotalContactPrice + $nTotalTaxPrice + $view_data['list'][0]->nAccountReceivables;
                $nTotalString = number_format_charge($nTotalContactPrice+$nTotalTaxPrice, $view_data['list'][0]->sSpecsCurrencyType);
                $currType     = $view_data['list'][0]->sSpecsCurrencyType;//171018
            }//end if

            //font style
            $font_style = " style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 0; padding: 0;' ";
            //공급자, 공급받는자 th, td style
            $sTop_li_style = " style='font-family: dotum, gulim, Arial, sans-serif; font-size: 15px; color: #666; line-height: 20px; font-weight: bold; letter-spacing: -1px; margin: 0; padding: 0;' ";
            $sTop_span_style = " style='font-family: dotum, gulim, Arial, sans-serif; font-size: 15px; color: #000; line-height: 20px; font-weight: bold; letter-spacing: -1px; margin: 0; padding: 0;' ";
            $row_th_style = " scope='row' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px; background-color:#dddddd;' align='center' ";
            $sTop_row_td_style = " style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left' ";
            //거래명세서 리스트 th style
            $sServicelist_table_th_style = " scope='col' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px; background-color:#dddddd;' align='center' ";

                //$stamp_url = "<div class='taxmark'><img src='https://www.kinx.net/wp-content/themes/kinx/images/company/tax_account_mark.png' alt=''></div>";
                $stamp_url = ""; //인감 이미지 삭제. 190816( dtBilling 2019-08 이후부터 안나오도록 한 것 제거

            $sMailDocumentTop ="<!DOCTYPE html><html lang='ko'><head>
            <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
            <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
            <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1'>
            <meta http-equiv='Content-Script-Type' content='text/javascript' />
            <meta http-equiv='Content-Style-Type' content='text/css' /><title>거래명세서</title>
            <style>
            body,html{margin:0;padding:0;}
            *{margin:0;padding:0;}
            .taxmark{position:absolute; top:180px;left:250px;}
            .taxmark img{width: 80px;height: 77px;}
            .dealbill_wrap {min-width:660px;margin:0 auto;border:0 none;padding:20px}
            .dealbill_wrap h2 {text-align:center;font-size:18px;color:#000;margin-bottom:20px}
            .dealbill_wrap ul {margin-bottom:10px}
            .dealbill_wrap li {font-size:15px;font-weight:bold;color:#666;letter-spacing:-1px}
            .dealbill_wrap li span {font-size:15px;font-weight:bold;color:#000;letter-spacing:-1px}
            .dealbill_wrap table {border:2px solid #000}
            .dealbill_wrap caption {height:0}
            .dealbill_wrap th {border-left:1px solid #000;border-bottom:1px solid #000;text-align:center;padding:7px 0 5px 0;color:#000;line-height:14px}
            .dealbill_wrap td {border-left:1px solid #000;border-bottom:1px solid #000;text-align:left;padding:7px 10px 5px 10px;color:#666;line-height:14px}
            .dealbill_wrap td.center {text-align:center}
            .dealbill_wrap td.right {text-align:right}
            .dealbill_wrap table strong {color:#000}
            .dealbill_wrap .sum {text-align:right;font-size:16px;font-weight:bold;color:#666;line-height:40px}
            .dealbill_wrap .sum span {font-size:16px;font-weight:bold;color:#000;line-height:40px}
            </style>
            </head>
            <body onload='window.print();'>
            <div class='dealbill_wrap popprint' id='taxSendPrt1' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; margin: 16px; padding: 0;'>
                <div style='position:relative;font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; width: 660px; margin: 0 auto; padding: 20px; border: 1px solid #000;'>
                ".$stamp_url."
                    <h2 style='font-family: dotum, gulim, Arial, sans-serif; font-size: 18px; color: #000; line-height: 28px; font-weight: bold; text-align: center; margin: 0 0 20px; padding: 0 0 0 12px;' align='center'>거래명세서</h2>
                    <ul style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; list-style-type: none; margin: 0 0 10px; padding: 0;'>
                        <li ".$sTop_li_style.">
                        <span".$sTop_span_style.">사용기간 :</span> ".$view_data['list'][0]->sUseDate."</li>
                        <li ".$sTop_li_style.">
                        <span".$sTop_span_style.">작 성 일  :</span> ".date('Y-m-d', strtotime($view_data['list'][0]->dtChargeDate))."</li>
                        <li ".$sTop_li_style.">
                        <span".$sTop_span_style.">납 기 일  :</span> ".date("Y-m-d", strtotime($view_data['list'][0]->dtPaymentDate))."</li>
                    </ul>
                    <table summary='공급하는자/공급받는자' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; width: 100%; height: 100%; vertical-align: middle; border-collapse: collapse; margin: 0; padding: 0; border: 1px solid #000;'>
                    <caption style='font-family: dotum, gulim, Arial, sans-serif; font-size: 0; color: #585858; line-height: 0; visibility: hidden; overflow: hidden; width: 1px; height: 0; margin: 0; padding: 0; '>공급하는자/공급받는자</caption>
                    <colgroup ".$font_style.">
                        <col width='5%'  ".$font_style.">
                        <col width='12%' ".$font_style.">
                        <col width='33%' ".$font_style.">
                        <col width='5%'   ".$font_style.">
                        <col width='12%' ".$font_style.">
                        <col width='33%' ".$font_style.">
                    </colgroup>
                    <tbody ".$font_style.">
                    <tr ".$font_style.">
                        <th scope='row' rowspan='6' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px; background-color:#dddddd;' align='center'>공<br ".$font_style.">급<br ".$font_style.">하<br ".$font_style.">는<br ".$font_style.">자<br ".$font_style."></th>
                        <th ".$row_th_style.">등록번호</th>
                        <td ".$sTop_row_td_style.">".TAX_COM_BUSINESS_NUMBER."</td>
                        <th scope='row' rowspan='6' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: center; margin: 0; padding: 7px 0 5px; background-color:#dddddd;' align='center'>공<br ".$font_style.">급<br ".$font_style.">받<br ".$font_style.">는<br ".$font_style.">자<br ".$font_style."></th>
                        <th ".$row_th_style.">등록번호</th>
                        <td ".$sTop_row_td_style.">".$view_data['list'][0]->sBusinessNumber."</td>
                    </tr>
                    <tr ".$font_style.">
                        <th ".$row_th_style.">회사명</th>
                        <td ".$sTop_row_td_style.">".TAX_COM_COMPANY_NAME."</td>
                        <th ".$row_th_style.">회사명<br ".$font_style.">(개인명)</th>
                        <td ".$sTop_row_td_style.">".$view_data['list'][0]->sCompanyName."</td>
                    </tr>
                    <tr ".$font_style.">
                        <th ".$row_th_style.">대표자명</th>
                        <td ".$sTop_row_td_style.">".TAX_COM_NAME."</td>
                        <th ".$row_th_style.">대표자명</th>
                        <td ".$sTop_row_td_style.">".$view_data['list'][0]->sPresidentName."</td>
                    </tr>
                    <tr ".$font_style.">
                        <th ".$row_th_style.">주 소</th>
                        <td ".$sTop_row_td_style.">".TAX_COM_ADDRESS."</td>
                        <th ".$row_th_style.">주 소</th>
                        <td ".$sTop_row_td_style.">".$view_data['list'][0]->sAddress."</td>
                    </tr>
                    <tr ".$font_style.">
                        <th ".$row_th_style.">업 태</th>
                        <td ".$sTop_row_td_style.">".TAX_COM_BUSINESS_TYPE."</td>
                        <th ".$row_th_style.">업 태</th>
                        <td ".$sTop_row_td_style.">".$view_data['list'][0]->sBusinessType."</td>
                    </tr>
                    <tr ".$font_style.">
                        <th ".$row_th_style.">종 목</th>
                        <td ".$sTop_row_td_style.">".TAX_COM_SERVICE_ITEM."</td>
                        <th ".$row_th_style.">종 목</th>
                        <td ".$sTop_row_td_style.">".$view_data['list'][0]->sItemType."</td>
                    </tr>
                </tbody>
            </table>

            <!--p style='font-family: dotum, gulim, Arial, sans-serif; font-size: 16px; color: #666; line-height: 40px; text-align: right; font-weight: bold; margin: 0; padding: 0;' align='right'>
                <span style='font-family: dotum, gulim, Arial, sans-serif; font-size: 16px; color: #000; line-height: 40px; font-weight: bold; margin: 0; padding: 0;'>합계 :</span> ".$nTotalString."
            </p-->
            <br><br>
            <table summary='거래명세서' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; width: 100%; height: 100%; vertical-align: middle; border-collapse: collapse; margin: 0; padding: 0; border: 1px solid #000;'>
                <caption style='font-family: dotum, gulim, Arial, sans-serif; font-size: 0; color: #585858; line-height: 0; visibility: hidden; overflow: hidden; width: 1px; height: 0; margin: 0; padding: 0;'>거래명세서</caption>
                <colgroup ".$font_style.">
                    <col width='4%'  ".$font_style.">
                    <col width='33%' ".$font_style.">
                    <col width='17%' ".$font_style.">
                    <col width='15%'  ".$font_style.">
                    <col width='16%' ".$font_style.">
                    <col width='15%' ".$font_style.">
                </colgroup>
                <thead ".$font_style."><tr ".$font_style.">
                    <th".$sServicelist_table_th_style.">No</th>
                    <th".$sServicelist_table_th_style.">서비스명</th>
                    <th".$sServicelist_table_th_style.">세부사항</th>
                    <th".$sServicelist_table_th_style.">수량</th>
                    <th".$sServicelist_table_th_style.">계약금액</th>
                    <th".$sServicelist_table_th_style.">세액</th>
                </tr>
                </thead>
                <tbody ".$font_style.">";

            $sMailDocumentBody .= "</tbody>";
            /*
                </table>
                <p style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; height: 40px; margin: 0; padding: 0;'></p>
                <table summary='입금계좌안내/합계금액' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #585858; line-height: 20px; width: 100%; height: 100%; vertical-align: middle; border-collapse: collapse; margin: 0; padding: 0; border: 2px solid #000;'>
                <caption style='font-family: dotum, gulim, Arial, sans-serif; font-size: 0; color: #585858; line-height: 0; visibility: hidden; overflow: hidden; width: 1px; height: 0; margin: 0; padding: 0;'>입금계좌안내/합계금액</caption>
                <colgroup ".$font_style.">
                    <col width=''25px".$font_style.">
                    <col width='' ".$font_style.">
                    <col width='12%' ".$font_style.">
                    <col width='12%' ".$font_style.">
                </colgroup>
              ";
              */

            //if($view_data['list'][0]->nAccountReceivables > 0)
            {
              $sMailDocumentBody .= "
                <tfoot ".$font_style.">
                    <tr ".$font_style.">
                        <td rowspan='3' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>
                            <font style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 20px; margin: 0; padding: 0;'>비<br />고</font>
                        </td>
                        <td rowspan='3' colspan='2' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>
                            <font style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 20px; margin: 0; padding: 0;'>".$view_data['list'][0]->sSpecsRemark."</font>
                        </td>
                        <th ".$row_th_style.">공급가액</th>
                        <td colspan='2' ".$tdStyle_right.">".number_format_charge($nTotalContactPrice,$currType)."</td>
                    </tr>
                    <tr ".$font_style.">
                        <th ".$row_th_style.">부가세</th>
                        <td colspan='2' ".$tdStyle_right.">".number_format_charge($nTotalTaxPrice,$currType)."</td>
                    </tr>
                    <tr ".$font_style.">
                        <th ".$row_th_style.">합계</th>
                        <td colspan='2' ".$tdStyle_right.">".number_format_charge($nTotalContactPrice+$nTotalTaxPrice,$currType)."</td>
                    </tr>
                    <tr ".$font_style.">
                        <td rowspan='2' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>
                            <font style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 20px; margin: 0; padding: 0;'>첨<br />부</font>
                        </td>

                        <td rowspan='3' colspan='2' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>
                            <font style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 20px; margin: 0; padding: 0;'>&nbsp;</font>
                        </td>
                        <th ".$row_th_style.">미수액</th>
                        <td colspan='2' ".$tdStyle_right.">".(($view_data['list'][0]->nAccountReceivables >0) ? number_format_charge($view_data['list'][0]->nAccountReceivables,$currType) : number_format_charge(0,$currType) )."</td>
                    </tr>
                    <tr ".$font_style.">
                        <th ".$row_th_style.">총합계</th>
                        <td colspan='2' ".$tdStyle_right.">".number_format_charge($nTotalPrice,$currType)."</td>
                    </tr>
                </tfoot>
                </table>
                 <div style='text-align:left;font-family:맑은 고딕,돋움,verdana; font-size:11px;'>※ 미수액은 ".substr($view_data['list'][0]->dtCreate,0,10)." 기준으로 확인된 내용입니다. </div>
                 <br />

                 <table summary='입금계좌안내' style='border:0px !important; cellpadding:0px !important; cellspacing:0px !important;' width='100%'>
                    <tr>
                        <td style='border-left:0px; border-bottom:0px;'>
                                <strong style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 20px; margin: 0; padding: 0;'>[입금계좌안내]</strong><br ".$font_style.">".BANK_ACCOUNT."
                        </td>
                    </tr>
                </table>

                </div>
            </div>
            ";
            }
            /*
            else
                {
                $sMailDocumentBody .= "
                <tbody ".$font_style.">
                    <tr ".$font_style.">
                        <td rowspan='3' style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; margin: 0; padding: 7px 10px 5px;' align='left'>
                        <strong style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #000; line-height: 20px; margin: 0; padding: 0;'>[입금계좌안내]</strong><br ".$font_style.">
                                        ".BANK_ACCOUNT.($view_data['list'][0]->sRemark<>'' ?'<br/><br/>'.$view_data['list'][0]->sRemark : '')."</td>
                        <th ".$row_th_style.">공급가액</th>
                        <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: right; margin: 0; padding: 7px 10px 5px;' align='right'>".number_format($nTotalContactPrice)."</td>
                    </tr>
                    <tr ".$font_style.">
                        <th ".$row_th_style.">부가세</th>
                        <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: right; margin: 0; padding: 7px 10px 5px;' align='right'>".number_format($nTotalTaxPrice)."</td>
                    </tr>
                    <tr ".$font_style.">
                        <th ".$row_th_style.">합계</th>
                        <td style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; color: #666; line-height: 14px; border-left-style: solid; border-left-color: #000; border-left-width: 1px; border-bottom-color: #000; border-bottom-style: solid; border-bottom-width: 1px; text-align: right; margin: 0; padding: 7px 10px 5px;' align='right'>".number_format($nTotalContactPrice+$nTotalTaxPrice)."</td>
                    </tr>
                </tbody>
                </table>
            </div>";
            }
            */

            $sMailDocument = $sMailDocumentTop.$sMailDocumentBody;
            echo $sMailDocument;
            die;
        }else{
            //잘못된 접근
            redirect('https://www.kinx.net/error');
        }
    }

}