<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/Common.php");
class Zipcode extends Common{
	public function __construct() {
		parent::__construct();
	}
    public function index(){
        if($this->input->is_ajax_request()){
            $word = $this->input->post('word');
            $type = $this->input->post('type');//type 0 -도로명검색, 1-지번검색
            
            $this->load->model('zipcode_model'); 
            if($type)//지번검색
            {
            	$ret = $this->zipcode_model->get_post_code_xml_by_api($word)->return_result('json');
            }
            else //도로명검색
         {
            	$ret = $this->zipcode_model->get_post_code_xml_by_api2($word)->return_result('json');
            }
            
            $this->output->set_content_type('application/json')->set_output($ret);
        }else{
            $data = array();
            $this->scripts = Array('zipcode.js');
            $this->css = Array();
            $data['jsinit'] = Array('zipcode.search.init();');
            $data['style'] = Array(
                "body{font-family:'맑은 고딕';font-size:13px;color:#4d4d4d;letter-spacing:-1px;}",
                "body,p,img,ul{border:none;margin:0;padding:0;text-align:left;list-style:none;}",
            	".tab_menu {position:absolute; height:34px; border-bottom:2px solid #a3a3a3;top:25px;right:10px;}",
				".tab_menu:after {content:''; display:block; clear:both}",
				".tab_menu li {float:left; height:33px; border-top:1px solid #c1c0c1; border-right:1px solid #c0bebf; border-bottom:1px solid #a3a3a3; border-left:1px solid #c0bebf; background:url('/images/include/bg_tab.gif') repeat-x; width:120px; text-align:center; letter-spacing:-1px; margin-left:-1px}",
				".tab_menu li:first-child {margin:0}",
				".tab_menu li a {display:block; height:26px; padding-top:9px; font-weight:bold; color:#999; text-decoration:none}",
				".tab_menu li a:hover {color:#555}",
				".tab_menu .active {border-top:1px solid #276e9c; border-right:1px solid #276e9c; border-bottom:2px solid #276e9c; border-left:1px solid #276e9c; background:#2290db; color:#fff; position:relative;}",
				".tab_menu .active a, .tab_menu .active a:hover {font-weight:bold; color:#fff}",
				".tab_menu .right {position:absolute; margin-top:12px; right:0; font-size:11px}"
            );
            
            $this->_print_uri($data, 'utility/zipcode');
        }
    }
}