<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/Common.php");
class Usage extends Common{

    private $checkinarray = array(); //ISM의 tAccount.nMyCardPayment 값을 참고함

    public function __construct() {

        parent::__construct();

        $this->load->model("bill_detail_ism_model","md_bill_detail");
        $this->load->model("contract_ism_model",   "md_contract");
        $this->load->model("product_ism_model",    "md_product");
        $this->load->model("aggregate_ism_model",  "md_aggregate");

        //$this->is_login = true;
        if($this->is_login === false)
        {
            $this->output->set_status_header('410');
            $this->tinyjs->pageRedirect("/", "로그인 후 접근가능합니다");
            exit;
        }

        $this->load->helper('common');
    }


    /**
     * 청구사용량 목록
     */
    public function index()
    {
        $this->_set_sec();
        $secParams = $this->_get_sec();

        if(!isset($this->member["nAccountSeq"]) || empty($this->member["nAccountSeq"]))
        {
            $this->output->set_status_header('410');
            $this->tinyjs->pageRedirect("/", "로그인 후 접근가능합니다");+
            exit;
        }

        # 파라메터 확인
        $secParams = $this->_get_sec();

        # 페이지 구성 정보 설정
        $secParams["offset"]      = isset($secParams["page"])  ? $secParams["page"]  : "0";
        $secParams["limit"]       = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
        $secParams["nAccountSeq"] = $this->member["nAccountSeq"];

        # 목록 데이터 추출
        $data["cnt"]       = $this->md_bill_detail->get_usage_list($secParams, TRUE);
        $data["list"]      = $this->md_bill_detail->get_usage_list($secParams);

        # 데이터 가공
        for($i=0,$max=count($data["list"]);$i<$max;$i++)
        {
            $data["list"][$i]['dtUse'] = '';

            # 계약설정 json decode
            if($arContract = chg_arrayfield($data["list"][$i]['arContractInfo']))
            {
                # 사용기간 데이터 가공
                list($usestart, $useend) = $this->_set_usage_date($arContract, $data["list"][$i]['dtUseStart'], $data["list"][$i]['dtUseEnd']);
                $data["list"][$i]['dtUse'] = $usestart.' ~ '. $useend;


                # 사용량 가공
                list($data["list"][$i]['sUsageUnit'], $data["list"][$i]['nUsageSize']) = $this->_set_usage_data($arContract, $data["list"][$i]['sUsageUnit'], $data["list"][$i]['nUsageSize']);
            }
            unset($data["list"][$i]['arContractInfo'], $arContract,$usestart, $useend);

        }


        # pager 정보 설정
        $pager["CNT"]      = $data["cnt"];
        $pager["PRPAGE"]   = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
        $pagerHtm          = $this->_set_pager($pager);

        $data["pager"]     = $pagerHtm;
        $data["pagerIdx"]  = $data["cnt"] - $secParams["offset"];
        $data["secParams"] = $secParams;


        # 청구월 검색항목 생성 최근 2년까지만
        for($i=1;$i<25;$i++)
        {
            $date = date('Y-m-01', strtotime("-".$i." month", time()));
            $data['search']['billmonth'][$date] = array(
                'selected' => (isset($secParams['dtBillMonth']) && $secParams['dtBillMonth']==$date)?'selected':'',
                'value'    => substr($date,0,4).'년 '.substr($date,5,2).'월'
            );
        }



        $this->_print($data);
    }









    public function detail($seqinfo=null)
    {
        # 청구정보 확인 ------------------------------------------------------------------------------------
        if(empty($seqinfo))
        {
            $this->tinyjs->pageBack("정상적인 경로로 접근해주세요");
            exit;
        }

        $billdetail_seq = decryptIt($seqinfo);

        $where = array(
            "nAccountSeq"    => $this->member["nAccountSeq"],
            "nBillDetailSeq" => $billdetail_seq
        );

        if(!$data['billing'] =  $this->md_bill_detail->get_bill_usage_by_billdetailseq($where))
        {
            $this->tinyjs->pageBack("청구정보가 조회되지 않습니다.");
            exit;
        }
        unset($where);

        # 계약설정 json decode
        $data["billing"]['dtUse'] = '';
        $arContract_data = '';
        $nBillDataType   = $data["billing"]['nBillDataType'];

        if($arContract = chg_arrayfield($data["billing"]['arContractInfo']))
        {
            # 사용기간 가공
             list($usestart, $useend)= $this->_set_usage_date($arContract, $data["billing"]['dtUseStart'], $data["billing"]['dtUseEnd']);
             $data["billing"]['dtUse'] = $usestart.' ~ '. $useend;

            # 사용량 가공
            list($data["billing"]['sUsageUnit'], $data["billing"]['nUsageSize']) = $this->_set_usage_data($arContract, $data["billing"]['sUsageUnit'], $data["billing"]['nUsageSize']);

            $arContract_data = $arContract['data'];
        }

        $data["billing"]['dataRule'] = $this->_get_strDataDetail($nBillDataType, $arContract_data);




        # 계약정보 추출 ------------------------------------------------------------------------------------
        $nContractSeq  = $data['billing']['nContractSeq'];
        $data["basic"] = $this->md_contract->get_contract_bySeq($nContractSeq);

        # 보안처리 타인의 정보를 볼수 없습니다.
        if($data["basic"]["nAccountSeq"] !== $this->member["nAccountSeq"])
        {
            $this->tinyjs->pageBack("정상적인 경로로 접근해주세요");
            exit;
        }

        # 계약 종료일 체크
        if(!empty($data["basic"]['dtCloseDate']) && $data["basic"]['dtCloseDate'] < $data["basic"]['dtEndDate'])
        {
            $data["basic"]['dtEndDate'] = $data["basic"]['dtCloseDate'];
        }





        # 사용량 정보 추출------------------------------------------------------------------------------------
        $data["service"] = $this->md_product->get_product_bySeq($data["billing"]['nProductSeq']);

        # 일별 데이터 추출
        list($data['usage_list'], $data['unit']) = $this->_get_day_usagedate($data['billing']);


        $this->scripts[] = "highcharts/highcharts.js";
        $this->scripts[] = "my_usage.js";
        $this->_print($data);
    }



    /**
     * 일별 사용량 그래프 생성을 위한 데이터 추출
     * method : post
     */
    public function usage_graph_data($seqinfo=null)
    {
        mysqli_report(MYSQLI_REPORT_STRICT | MYSQLI_REPORT_ALL);

        try{

            if(empty($seqinfo))
            {
                throw new Exception("정상적인 경로로 접근해주세요");
            }

            # 청구정보 추출---------------------------------------------------------------------------
            $billdetail_seq = decryptIt($seqinfo);
            $where = array(
                "nAccountSeq"    => $this->member["nAccountSeq"],
                "nBillDetailSeq" => $billdetail_seq
            );

            if(!$bill_data =  $this->md_bill_detail->get_bill_usage_by_billdetailseq($where))
            {
                throw new Exception("청구정보가 조회되지 않습니다.");
            }
            unset($where, $billdetail_seq);

            if(!$usage_data = $this->_get_day_usagedate($bill_data))
            {
                throw new Exception("일별 사용량 정보를 가져오지 못했습니다.");
            }
            unset($bill_data);

            $grap_data['graph_y_text'] = $usage_data[1];
            foreach($usage_data[0] as $key=>$val)
            {
                $grap_data['graph_day_category'][] = $key;
                $grap_data['graph_day_series'][]   = $val;
            }

            $rtn = array("status"=>TRUE, "data"=>$grap_data);
        }
        catch(mysqli_sql_exception $e)
        {
            $rtn = array("status"=>FALSE, "msg"=>'그래프 데이터를 정상적으로 불러오지 못했습니다.');
            log_message("error", $e);
        }
        catch (Exception $e)
        {
            $rtn = array("status"=>FALSE, "msg"=>'그래프 정보를 불러오지 못했습니다.');
            log_message("error", $e);
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($rtn, JSON_NUMERIC_CHECK));


        # 청구정보 확인 ------------------------------------------------------------------------------------







        // # 사용량 정보 추출------------------------------------------------------------------------------------
        // $data["service"] = $this->md_product->get_product_bySeq($data["billing"]['nProductSeq']);
        // $sProductType  = $data["service"]['sServiceType'];

        // # 사용량 Day 정보 분류
        // if($sProductType=='IX' || $sProductType=='IDC')
        // {
        //     $sServiceType = "M";
        // }
        // elseif($sProductType=='CDN')
        // {
        //     $sServiceType = "C";
        // }
        // elseif($sProductType=='Cloud' && $nBillDataType == '5' )
        // {
        //     $sServiceType = "I";
        // }

        // switch($nBillDataType)
        // {
        //     case 2://대역폭
        //         $table  = ($sServiceType=="C") ? "AGGREGATE.tCdnTrafficDay" : "AGGREGATE.tMrtgDay";
        //         $select = ($sServiceType=="C") ? "nTraffic AS nUdata, dtCreate" : "(CASE WHEN (nTrafficInMax > nTrafficOutMax) THEN nTrafficInMax ELSE nTrafficOutMax end) AS nUdata, dtCreate";
        //         $unit   = ($sServiceType=="C") ? 'Mbps' : 'bps';
        //         break;
        //     case 3://전송량
        //         $table  = "AGGREGATE.tCdnDataDay";
        //         $select = "nTotalSize AS nUdata, dtCreate" ;
        //         $unit   = "Kbyte";
        //         break;
        //     case 4://스토리지
        //         $table  = "AGGREGATE.tCdnStorageDay";
        //         $select = "nTotalSize AS nUdata, dtCreate";
        //         $unit   = "Gbyte";
        //         break;
        //     case 6://건수
        //         $table  = "AGGREGATE.tCdnRequestDay";
        //         $select = "nTotalCount AS nUdata, dtCreate";
        //         $unit   = "EA";
        //         break;
        //     case 7://시간
        //         $table  = "AGGREGATE.tCdnDatetimeDay";
        //         $select = "nTotalSize AS nUdata, dtCreate, unit";
        //         $unit   = "Min";
        //         break;
        //     default://이외는 사용량 추출하지 않는다.
        //         $table  = FALSE;
        // }


        // # 일간 사용 데이터 추출 및 가공
        // $usage_list = NULL;
        // if($table)
        // {
        //     $usage_list = $this->md_aggregate->get_usage_data_list($table, $select, $nContractSeq, $usestart, $useend);
        // }
        // $data['usage_list'] = $this->_set_usage_listdata($usage_list, $usestart, $useend, $unit);

        // # 데이터는 $this->_set_usage_listdata에서 변경하고, 표시 명칭만 요기서 바꾼다.
        // $unit = ($unit=='bps')?'Mbps':$unit;
        // $unit = ($unit=='Kbyte')?'Gbyte':$unit;



        // # 데이터 단위설정
        // $data['unit'] = $unit;
        // if($nBillDataType==7 && !empty($usage_list))
        // {
        //     $data['unit'] = ucfirst(substr($usage_list[0]['unit'],0,3));
        // }

        // # 변수 정리... 휘리릭~
        // unset($usage_list, $usestart, $useend, $table, $select,$unit);


    }














    #========================================================================================
    # 단위처리 정보를 확인하여 사용량 정보를 조정한다.
    #========================================================================================
    private function _set_dataround($usage_amount, $round, $round_digit)
    {
        switch($round)
        {
            case 'round':
                $usage_amount = ($round_digit==99) ? round($usage_amount) : round($usage_amount, $round_digit);
                break;
            case 'floor':
                $usage_amount = ($round_digit==99) ? floor($usage_amount) : floor(floor($usage_amount * pow(10, $round_digit)*10)/10)/pow(10, $round_digit);
                break;
            case 'ceil':
                $usage_amount = ($round_digit==99) ? ceil($usage_amount) : ceil(floor($usage_amount * pow(10, $round_digit)*10)/10)/pow(10, $round_digit);
                break;
        }
        return $usage_amount;
    }



    #========================================================================================
    # 계약정보 기준 사용량 기간 가공
    #========================================================================================
    private function _set_usage_date($arContract, $use_startdate, $use_enddate)
    {
        $use_start = substr($use_startdate,0,10);
        $use_end   = substr($use_enddate,0,10);

        # 계약사용량 시작일이 있는경우 사용일 변경
        if(isset($arContract['day']) && $arContract['day'] > 1)//15, 26 등이 있다.
        {
            $chk_start = date('Y-m-'.$arContract['day'], strtotime('-1 month', strtotime($use_startdate)));
            $chk_end   = date('Y-m-'.($arContract['day']-1), strtotime($use_enddate));

            # 계약시작일, 계약종료일 확인해서 최종 설정
            $use_start = ($arContract['dtStart'] > $chk_start) ? $arContract['dtStart'] : $chk_start;
            $use_end   = ($arContract['dtEnd']   < $chk_end) ? $arContract['dtEnd'] : $chk_end;
        }


        return array($use_start, $use_end);
    }


    #========================================================================================
    # 계약정보 기준 사용량 가공
    #========================================================================================
    private function _set_usage_data($arContract, $usage_unit, $usage_size)
    {
        # 최초단위를 MB로 맞추고, 데이터단위 처리를 실행한다.
        if($usage_unit =='bps')
        {
            # 사업G요청에 의해 MRTG 정보의 경우 Mbps로 전환 시 소수점 인트라넷 화면과 동일하게 소수점 2자리에서 반올림한 값으로 기본
            # 사용량 데이터를 사용한다. 이에 과금 계산과 화면에 노출되는 데이터를 수정한다. 2017-07-15
            $usage_size = $this->_set_dataround(round($usage_size/(1000000),1), $arContract['round'], $arContract['round_digit']);
            $usage_unit = 'Mbps';
        }
        elseif($usage_unit =='Gbyte')
        {
            # Gbyte일 경우만 먼저 데이터단위 처리를 선행한다.
            $usage_size = $this->_set_dataround($usage_size, $arContract['round'], $arContract['round_digit']);
        }
        else
        {
            $usage_size = $this->_set_dataround($usage_size, $arContract['round'], $arContract['round_digit']);
        }

        return array($usage_unit, $usage_size);
    }


    #========================================================================================
    # 화면 표시용 일별 사용량 가공
    #========================================================================================
    private function _set_usage_listdata($data_list, $usestart, $useend, $unit)
    {
        $usage_list = array();
        $day_diff   = $this->_get_date_diff($usestart, $useend);

        if(empty($data_list))
        {
            for($i=0;$i<$day_diff;$i++)
            {
                $day = date('Y-m-d', strtotime('+'.$i.' day', strtotime($usestart)));
                $usage_list[$day] = 0;
            }
        }
        else
        {
            for($i=0;$i<$day_diff;$i++)
            {
                $day = date('Y-m-d', strtotime('+'.$i.' day', strtotime($usestart)));
                $usage_list[$day] = 0;

                foreach($data_list as $ukey=>$uval)
                {
                    if(substr($uval['dtCreate'],0,10)==$day)
                    {
                        if($unit=='bps')
                        {
                            $usage_list[$day] = round($uval['nUdata']/1000000, 2);
                        }
                        elseif($unit=='Kbyte')
                        {
                            $usage_list[$day] = round(($uval['nUdata']/1024)/1024, 2);
                        }
                        else
                        {
                            $usage_list[$day] = $uval['nUdata'];
                        }

                        unset($data_list[$ukey]);
                        break;
                    }
                }
            }
        }

        return $usage_list;

    }




    #========================================================================================
    # 기간 간격 날짜 구하기
    #========================================================================================
    //1~31일 이면 31을 돌려줌(diff 는 원래 30이지만 1을 더해서 반환)
    function _get_date_diff($dtStart, $dtEnd, $flagUnix = FALSE)
    {
        if($flagUnix == FALSE )
        {
            $date1 = new DateTime($dtStart);
            $date2 = new DateTime($dtEnd);
        }
        else
        {
            $date1 = DateTime::createFromFormat( 'U', $dtStart );
            $date2 = DateTime::createFromFormat( 'U', $dtEnd );
        }

        $diff = date_diff($date1, $date2);
        return $diff->days + 1;
    }





    #========================================================================================
    # 데이터 기준 값을 문자열로 나타냄.
    # nBillDataType : 과금 기준(1: 판매/이용, 2: 대역폭,3: 전송량, 4: 스토리지, 5: 시간, 6 : 개수)
    # 이 주에서 2:대역폭인 경우만 value가 있고 나머지는 type만 있다
    #========================================================================================
    private function _get_strDataDetail($dataType, $data)
    {
        $new_arr = array(
            2 =>  array(//대역폭
                1 => '최대값 상위 $0 번째 값',
                2 => '최대값 상위 $0 ~ $1 까지 평균',
                3 => '최대값 상위 $0 번째까지, 최소값 하위 $1 번째까지 제외한 평균 ',
                4 => '최대값 전체 평균',
                5 => '최대값 상위 95% 값(5분단위 값을 기준으로 상위 5% 제외한 최대값)',
                6 => '평균값 상위 95% 값(5분단위 평균값을 기준으로 상위 5% 제외한 최대값)'
            ),
            3 => array(//전송량
                1 => '전송량 전체 합계'
            ),
            4 => array(//스토리지
                1 => '최대값 상위 1번째 값',
                2 => '일 사용량 전체 평균 값'
            ),
            5 =>array(//시간
                1 => '이용 시간 전체 합계'
            ),
            6 =>array(//개수
                1 => '개수 전체 합계'
            )
        );

        $datainfo = (isset($data['type']) && isset($new_arr[$dataType][$data['type']])) ? $new_arr[$dataType][$data['type']]:'';
        if(isset($data['value']) && count($data['value']) > 0)
        {
          foreach($data['value'] as $key => $val)  $datainfo = str_replace ('$'.$key, $val, $datainfo);
        }

        return $datainfo;
    }





    #========================================================================================
    # 일별 사용량 데이터 추출
    #========================================================================================
    private function _get_day_usagedate($bill_data)
    {
        $sProductType  = $bill_data['sProductType'];
        $nBillDataType = $bill_data['nBillDataType'];
        $nContractSeq  = $bill_data['nContractSeq'];
        $arContract    = chg_arrayfield($bill_data['arContractInfo']);
        list($usestart, $useend)= $this->_set_usage_date($arContract, $bill_data['dtUseStart'], $bill_data['dtUseEnd']);

        # 사용량 Day 정보 분류
        if($sProductType=='IX' || $sProductType=='IDC')
        {
            $sServiceType = "M";
        }
        elseif($sProductType=='CDN')
        {
            $sServiceType = "C";
        }
        elseif($sProductType=='Cloud' && $nBillDataType == '5' )
        {
            $sServiceType = "I";
        }

        switch($nBillDataType)
        {
            case 2://대역폭
                $table  = ($sServiceType=="C") ? "AGGREGATE.tCdnTrafficDay" : "AGGREGATE.tMrtgDay";
                $select = ($sServiceType=="C") ? "nTraffic AS nUdata, dtCreate" : "(CASE WHEN (nTrafficInMax > nTrafficOutMax) THEN nTrafficInMax ELSE nTrafficOutMax end) AS nUdata, dtCreate";
                $unit   = ($sServiceType=="C") ? 'Mbps' : 'bps';
                break;
            case 3://전송량
                $table  = "AGGREGATE.tCdnDataDay";
                $select = "nTotalSize AS nUdata, dtCreate" ;
                $unit   = "Kbyte";
                break;
            case 4://스토리지
                $table  = "AGGREGATE.tCdnStorageDay";
                $select = "nTotalSize AS nUdata, dtCreate";
                $unit   = "Gbyte";
                break;
            case 6://건수
                $table  = "AGGREGATE.tCdnRequestDay";
                $select = "nTotalCount AS nUdata, dtCreate";
                $unit   = "EA";
                break;
            case 7://시간
                $table  = "AGGREGATE.tCdnDatetimeDay";
                $select = "nTotalSize AS nUdata, dtCreate, unit";
                $unit   = "Min";
                break;
            default://이외는 사용량 추출하지 않는다.
                $table  = FALSE;
        }


        # 일간 사용 데이터 추출 및 가공
        $usage_list = NULL;
        if($table)
        {
            $usage_list = $this->md_aggregate->get_usage_data_list($table, $select, $nContractSeq, $usestart, $useend);
        }

        $return_usage_list = $this->_set_usage_listdata($usage_list, $usestart, $useend, $unit);

        # 데이터는 $this->_set_usage_listdata에서 변경하고, 표시 명칭만 요기서 바꾼다.
        $unit = ($unit=='bps')?'Mbps':$unit;
        $unit = ($unit=='Kbyte')?'Gbyte':$unit;



        # 데이터 단위설정
        $return_unit = $unit;
        if($nBillDataType==7 && !empty($usage_list))
        {
            $return_unit = ucfirst(substr($usage_list[0]['unit'],0,3));
        }

        # 변수 정리... 휘리릭~
        unset($usage_list, $usestart, $useend, $table, $select,$unit);

        return array($return_usage_list, $return_unit);
    }


}