<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH ."controllers/Common.php");
class Request extends Common
{
    public function __construct() {
        parent::__construct();


        $this->load->model("payment_ism_model","md_payment");

        //kclean 은 INTRANET 쪽에 그대로 유지. 따로 ISM 쪽으로 돌리지 않는다.
        $this->load->model('Kclean_model', 'md_kclean', TRUE);
        $this->load->model('Code_model','md_code',TRUE);
    }

    public function index() {
        redirect('errors/error404');
        die;

        $data = null;
        $this->_print($data);
    }

    public function testConsult()
    {
        $temp = date('Y-m-d');
        $aryParam = Array(
                'sManagername'=>"Mng_Lee",
                'hp_first_select'    => '010',
                'hp_second'        => '123',
                'hp_third'            => '4567',
                'email_1'            => 'lizzy337',
                'email_2'            => 'kinx.net',
                'nServiceType'    => 1,
                'sTitle'                => 'consult title'.$temp,
                'sQuestion'        => 'consult question'
                );


        //$url = 'sms/send.json';
        $url = 'consult/send';
        $Param = json_encode($aryParam);
        //print_r($Param);

        //config 로 변경. 170706
        $this->load->config('kinxconfig');
        $api = $this->config->item('kinx_api_host', 'kinxconfig');
        /*
        $api = 'http://api.kinx.net';
        if(ENVIRONMENT=='development')
            $api = 'http://apidev.kinx.net';
        else
            $api = 'http://api.kinx.net';
        */

        $this->load->library('rest');
        $this->rest->initialize(array('server'=>$api));
        $this->rest->http_header('content-type','application/json;charset=UTF-8');
        $retStdClass = $this->rest->post($url, $Param, 'json');
        //print_r($retStdClass);
        //stdClass Object ( [success] => 1 [http_code] => 200 [results] => 363 [errormsg] => Insert is success )
        //stdClass Object ( [success] => [http_code] => 500 [results] => 366 [errormsg] => Insert is fail )
        exit;
    }

    Public Function request_step1()
    {
        //$this->tinyjs->pageRedirect("/");//my.kinx.net 홈으로 간다. 만약 적용하려면 request_step2, request_step3 까지 해야함
        //die;
        if(isPost()) # 데이터 처리 요청일 경우
        {
            $postdata = $this->input->post(NULL, TRUE);

            //echo "<br /> request  step1 post data before <br />";
            //print_r($postdata);

            //-start 자동입력방지 관련
            if($_POST['g-recaptcha-response'])
            {
                $recaptcha = $_POST['g-recaptcha-response'];
            }
            if(!$recaptcha)
            {
                $decoded= array("success"=>false,"message"=>"로봇이 아니면 체크해주세요.");
                echo(json_encode($decoded));
                exit;
            }
            else
            {

                $ip = $_SERVER['REMOTE_ADDR'];
                //print_r("ip "); print_r($ip); die;

                $secretKey = $_POST['secretkey'];

                $url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$recaptcha."&remoteip=".$ip;
                //print_r("url "); print_r($url);

                $resource =  file_get_contents( $url );
                $response = json_decode($resource, true);
                //print_r("response "); print_r($response);
                //die;

                if(intval($response["success"]) !== 1)
                {
                    $errorMessages = array(
                            'missing-input-secret' => 'The secret parameter is missing.',
                            'invalid-input-secret' => 'The secret parameter is invalid or malformed.',
                            'missing-input-response' => 'The response parameter is missing.',
                            'invalid-input-response' => 'The response parameter is invalid or malformed.',
                    );
                    if( isset($response["error-codes"][0]) )
                            $msg = ($response["error-codes"][0])? $errorMessages[$response["error-codes"][0]] : "정상적인 접속이 아닌 것으로 판단됩니다." ;
                        else
                            $msg = "정상적인 접속이 아닌 것으로 판단됩니다." ;
                        $decoded= array("success"=>false,"message"=>$msg);
                        echo(json_encode($decoded));
                        exit;
                }
            }
            //-end google recaptcha


            #데이터 가공
            //주민등록번호 암호화
            if( !empty($postdata['sPersonalNumber']) )
            {
                $postdata["sPersonalNumber"] = chgPnumber($postdata["sPersonalNumber"], FALSE, "e");
            }

            //$postdata['sZipcode'] = $postdata['sZipcode1'].$postdata['sZipcode2'];//161114 우편번호 5자리
            $postdata['sURL']= array();
            foreach($postdata['URL'] as $key=>$val)
            {
                $postdata['sURL'][]= array('URL'=>$postdata['URL'][$key], 'IP'=>$postdata['IP'][$key], 'TTL'=>$postdata['TTL'][$key]);
            }
            $postdata['sURL'] = serialize($postdata['sURL']);

            //sZipcode
            $postdata['sZipcode'] = $postdata['zip_code'];//161114 우편번호 5자리


            //sPhone
            $postdata['sPhone'] = $postdata['tel_first_select'].'-'.$postdata['tel_Phone2'].'-'.$postdata['tel_Phone3'];
            //sFax
            if( !empty($postdata['fax2']) && !empty($postdata['fax3']) )
            {
                $postdata['sFax'] = $postdata['fax_first_select'].'-'.$postdata['fax2'].'-'.$postdata['fax3'];
            }
            //sInternalPhone - 담당자 전화
            $postdata['sInternalPhone'] = $postdata['tel2_first_select'].'-'.$postdata['tel2_Phone2'].'-'.$postdata['tel2_Phone3'];
            //sMobilePhone - 담당자 휴대전화
            $postdata['sMobilePhone'] = $postdata['hp_first_select'].'-'.$postdata['hp_second'].'-'.$postdata['hp_third'];

            //unset($postdata['sZipcode1']); unset($postdata['sZipcode2']);
            unset($postdata['zip_code']);//161114 우편번호 5자리
            unset($postdata['tel_first_select']); unset($postdata['tel_Phone2']); unset($postdata['tel_Phone3']);
            unset($postdata['fax_first_select']); unset($postdata['fax2']); unset($postdata['fax3']);
            unset($postdata['tel2_first_select']); unset($postdata['tel2_Phone2']); unset($postdata['tel2_Phone3']);
            unset($postdata['hp_first_select']); unset($postdata['hp_second']); unset($postdata['hp_third']);
            unset($postdata['chkag01']); unset($postdata['chkag02']);

            //port
            $postdata['nPort'] = $postdata['sPort'];
            $postdata['sPort'] = $postdata['sPortRadio'];

            //va('kclean insert');va($postdata); die;

            $ret = array('success'=>NULL,"message"=>NULL);
            $ret['success'] = $this->md_kclean->_insert($postdata);

            //1차 신청에 대한 sms
            //문제있어 SMS 발송안함.
            //$this->sendsms_req(1,$postdata['sCompanyName']);

            $this->output->set_content_type('application/json')->set_output(json_encode($ret));


        }
        else #초기화면 출력일 경우
        {

            //업체 유형
            $data["sOwnerType"] = $this->md_code->get_code_list_array('G065', FALSE, null);
            //해외 구분
            $data["sForeignYn"] = $this->md_code->get_code_list_array('G126', FALSE, null);
            //글로벌 서비스 사용 여부
            $data["sGlobal"] = $this->md_code->get_code_list_array('K001', FALSE, null);
            //기간
            $data["nServiceMonth"] = $this->md_code->get_code_list_array('K002', FALSE, null);
            //DNS 운영
            $data["sDNS"] = $this->md_code->get_code_list_array('K003', FALSE, null);
            //방화벽
            $data["sWall"] = $this->md_code->get_code_list_array('K004', FALSE, null);
            //인증서
            $data["sSSL"] = $this->md_code->get_code_list_array('K005', FALSE, null);
            //서비스 port
            $data["sPort"] = $this->md_code->get_code_list_array('K006', FALSE, null);
            //웹서버 os 정보
            $data["sOS"] = $this->md_code->get_code_list_array('K007', FALSE, null);
            //L4사용유무
            $data["sL4"] = $this->md_code->get_code_list_array('K008', FALSE, null);
            //DB정보
            $data["sDB"] = $this->md_code->get_code_list_array('K009', FALSE, null);
            //대역폭
            $data["sBandwidth"] = $this->md_code->get_code_list_array('K010', FALSE, null);
            //로그인페이지 유무
            $data["sLoginpage"] = $this->md_code->get_code_list_array('K011', FALSE, null);
            //해외 서비스 필요여부
            $data["sGlobalService"] = $this->md_code->get_code_list_array('K012', FALSE, null);

            //echo "<br />step1 data : <br />";print_r ($data);die();

            $data['style'] = Array(
                    ".chkperinput{background:#ddd;color:#aaa}",
                    ".errMsg{color:red;font-size:11px;}"
            );

            $this->scripts= array("my_utility.js", "my_kclean.js");
            $this->_print($data);
        }
    }









    Public Function request_step2()
    {

        //post 데이터 추출
        $postdata = $this->input->post(NULL, TRUE);


        //글로벌 서비스 사용여부
        $temp['sGlobal'] = $this->md_code->get_code_list_array('K001', FALSE, null);

        $sGlobal = "";
        foreach ($temp['sGlobal'] as $key=>$value)
        {
            if($key == $postdata['sGlobal'])
            {
                $sGlobal = $value['sCodeName'];
            }
        }

        //db insert id
        $data["skcleanid"]         = seed($postdata["nInsertId"]);

        //sGlobal
        $data['sGlobal']           = $sGlobal;

        //추가 IP : 도메인 갯수 - 1
        $data['nAddDomainCount']   = $postdata['nDomainCount']-1;

        //기간
        $data['nServiceMonth']     = $postdata['nServiceMonth'];

        //가격
        $data['nServicePrice']     = $postdata['nServicePrice'];
        $data['nVat']              = $data['nServicePrice']/10;
        $data['totalPrice']        = $data['nServicePrice'] + $data['nVat'];

        //서비스정보
        $data['strServiceTypeEtc'] = $postdata['sCompanyName'].' SOS 서비스신청'; //기존(기술지원-결제사유 값)값 대치
        $data['strServiceType']    = '신규'; //기존(기술지원-서비스유형'신규' 값)값 대치

        //카드결제 정보
        $data['ts_ordername']      = 'K-클린';
        $data['ts_productinfo']    = $data['strServiceTypeEtc'];
        $data['ts_amount']         = $data['totalPrice'];
        $data['ts_buyer']          = $postdata['sPresidentName'];
        $data['ts_customername']   = $postdata['sCompanyName'];
        $data['ts_customeremail']  = $postdata['sEmail'];
        $data['ts_customermobile'] = $postdata['hp_first_select'].$postdata['hp_second'].$postdata['hp_third'];




        $this->scripts = ['https://js.tosspayments.com/v1','tosspayment.js', 'kclean/msgelement.js','kclean/payment.js','my_utility.js'];
        $this->_print($data);
    }







    Public Function request_step3($result=NULL)
    {

        $data             = [];
        $data['respcode'] = '1111';

        # 카드 결제창 확인 완료일 경우--------------------------------------------------------------
        if($result=='success')
        {
            $getdata = $this->input->get(NULL, TRUE);

            $this->load->library('tosscard');
            $data['charge_result'] = $this->tosscard->run_charge($getdata);


            # 결제 성공 시 카드결제 정보 추가
            if($data['charge_result']['sResultCode']=='0000')
            {
                $kclean_seq       = seed($getdata['skcleanid'], FALSE);
                $where            = ['nKCleanSeq'=>$kclean_seq];
                $change_data      = ['nXpayApprovalSeq'=>$data['charge_result']['nXpayApprovalSeq']];

                # kclean 카드결제 정보 추가
                $this->md_kclean->_update($change_data, $where);

                # 등록된 kclean 조회
                $data['kclean']   = $this->md_kclean->_select_row($where, 0);

                # 결과코드
                $data['respcode'] = $data['charge_result']['sResultCode'];

                # 메일발송
                $this->kcleanRequest_sendmail($data['charge_result'], $data['kclean']);
            }
        }
        elseif($result=='fail')
        {
            $getdata = $this->input->get(NULL, TRUE);
            $data['charge_result'] = [
                'sResultCode'    => $getdata['code'],
                'sResultMessage' => $getdata['message']
            ];
        }
        # 잘못된 접근
        elseif(!is_null($result))
        {
            redirect('errors/error404');
        }


        //vad($data);

        $this->scripts = ["my_utility.js", "my_kclean.js"];
        $this->_print($data);
    }









    Public Function request_step3_copy()//support/payment의 payment02_res 함수
    {
        /*
         * [최종결제요청 페이지(STEP2-2)]
        *
        * LG유플러스으로 부터 내려받은 LGD_PAYKEY(인증Key)를 가지고 최종 결제요청.(파라미터 전달시 POST를 사용하세요)
        */
        //print_r($_REQUEST);
        $configPath                 = APPPATH."../lgdacom";
        /*
         *************************************************
        * 1.최종결제 요청 - BEGIN
        *  (단, 최종 금액체크를 원하시는 경우 금액체크 부분 주석을 제거 하시면 됩니다.)
        *************************************************
        */
        //print_r($_REQUEST);
        $CST_PLATFORM               = $_REQUEST["CST_PLATFORM"];
        $CST_MID                    = $_REQUEST["CST_MID"];
        $LGD_MID                    = (("test" == $CST_PLATFORM)?"t":"").$CST_MID;
        $LGD_PAYKEY                 = $_REQUEST["LGD_PAYKEY"];
        $strServiceTypeEtc             = $_REQUEST["strServiceTypeEtc"];
        $strServiceType                = $_REQUEST["strServiceType"];
        $LGD_DELIVERYINFO                = $_REQUEST["LGD_DELIVERYINFO"];
        $LGD_BUYERPHONE                = $_REQUEST["LGD_BUYERPHONE"];
        $LGD_PRODUCTCODE            = $_REQUEST["LGD_PRODUCTCODE"];
        $LGD_PRODUCTINFO            = $_REQUEST["LGD_PRODUCTINFO"];
        $LGD_CUSTOM_USABLEPAY            = $_REQUEST["LGD_CUSTOM_USABLEPAY"];//신용카드만 사용211019

        $msg = "CST_PLATFORM = " . $CST_PLATFORM . "<br>";
        $msg .= "CST_MID = " . $CST_MID . "<br>";
        $msg .= "LGD_MID = " . $LGD_MID . "<br>";
        $msg .= "LGD_PAYKEY = " . $LGD_PAYKEY . "<br>";
        $msg .= "LGD_DELIVERYINFO = " . $LGD_DELIVERYINFO . "<br>";
        $msg .= "LGD_BUYERPHONE = " . $LGD_BUYERPHONE . "<br>";
        $msg .= "strServiceTypeEtc = " . $strServiceTypeEtc . "<br>";
        $msg .= "strServiceType = " . $strServiceType . "<br>";
        $msg .= "LGD_PRODUCTCODE = " . $LGD_PRODUCTCODE . "<br>";
        $msg .= "LGD_PRODUCTINFO = " . $LGD_PRODUCTINFO . "<br>";
        $msg .= "LGD_CUSTOM_USABLEPAY = " . $LGD_CUSTOM_USABLEPAY . "<br>";
        //print_r($msg);

        //require_once("./lgdacom/XPayClient.php");
        require_once(APPPATH."../lgdacom/XPayClient.php");
        $xpay = new XPayClient($configPath, $CST_PLATFORM);
        $xpay->Init_TX($LGD_MID);

        $xpay->Set("LGD_TXNAME", "PaymentByKey");
        $xpay->Set("LGD_PAYKEY", $LGD_PAYKEY);

        //금액을 체크하시기 원하는 경우 아래 주석을 풀어서 이용하십시요.
        //$DB_AMOUNT = "DB나 세션에서 가져온 금액"; //반드시 위변조가 불가능한 곳(DB나 세션)에서 금액을 가져오십시요.
        //$xpay->Set("LGD_AMOUNTCHECKYN", "Y");
        //$xpay->Set("LGD_AMOUNT", $DB_AMOUNT);

        /*
         *************************************************
        * 1.최종결제 요청(수정하지 마세요) - END
        *************************************************
        */

        /*
         * 2. 최종결제 요청 결과처리
        *
        * 최종 결제요청 결과 리턴 파라미터는 연동메뉴얼을 참고하시기 바랍니다.
        */

        if ($xpay->TX())
        {
            //1)결제결과 화면처리(성공,실패 결과 처리를 하시기 바랍니다.)

            $msg = "결제요청이 완료되었습니다.  <br>";
            $msg .= "TX Response_code = " . $xpay->Response_Code() . "<br>";
            $msg .= "TX Response_msg = " . $xpay->Response_Msg() . "<p>";
            $msg .= "거래번호(LGD_TID) : " . $xpay->Response("LGD_TID",0) . "<br>";
            $msg .= "상점아이디(LGD_MID) : " . $xpay->Response("LGD_MID",0) . "<br>";
            $msg .= "상점주문번호(LGD_OID) : " . $xpay->Response("LGD_OID",0) . "<br>";
            $msg .= "상품코드(LGD_PRODUCTCODE) : " . $xpay->Response("LGD_PRODUCTCODE",0) . "<br>";
            $msg .= "결제금액(LGD_AMOUNT) : " . $xpay->Response("LGD_AMOUNT",0) . "<br>";
            $msg .= "결과코드(LGD_RESPCODE) : " . $xpay->Response("LGD_RESPCODE",0) . "<br>";
            $msg .= "결과메세지(LGD_RESPMSG) : " . $xpay->Response("LGD_RESPMSG",0) . "<p>";
            //va('요청완료 msg ');va($msg);

            $keys = $xpay->Response_Names();
            /*
            foreach($keys as $name) {
                echo $name . " = " . $xpay->Response($name, 0) . "<br>";
            }
            echo "<p>";
            */

            if( "0000" == $xpay->Response_Code() )
            {
                //최종결제요청 결과 성공 DB처리
                //echo "최종결제요청 결과 성공 DB처리하시기 바랍니다.<br>";

                //최종결제요청 결과 성공 DB처리 실패시 Rollback 처리
                $isDBOK = true; //DB처리 실패시 false로 변경해 주세요.
                //1. db 처리
                $nXpayApprovalSeq = $this->md_payment->xpay_input($xpay);
                //va('insert nXpayApprovalSeq');va($nXpayApprovalSeq);

                //2. 신청정보 등록
                $this->md_payment->lead_customer_direct_input($xpay, $strServiceTypeEtc, $strServiceType);

                //3-1. 등록된 kclean 내용 추출함. 메일발송 데이터를 위해
                $data['nInsertId'] = decryptIt($_REQUEST["nInsertId"]);
                $where["nKCleanSeq"] = $data['nInsertId'];
                $data['kclean'] = $this->md_kclean->_select_row($where);
                //echo "<br />kclean ";print_r($data['kclean']);


                //3-2. $nXpayApprovalSeq 값을 tKClean 에 update
                $where = array(
                        'nKCleanSeq'=>$data['nInsertId']
                );

                $change_data = array(
                        'nXpayApprovalSeq'=>$nXpayApprovalSeq
                );
                $this->md_kclean->_update($change_data,$where);


                //3-3. 메일발송
                $this->kcleanRequest_sendmail($xpay,$data['kclean']);

                //4. 결제확인
                $data['payment02result'] = $this->_payment02_finish($xpay);
                $data['respcode'] = $xpay->Response_Code();
                //print_r ($data['payment02result'] );
                $data['LGD_PRODUCTINFO'] = $LGD_PRODUCTINFO;
                //결제수단. sPaymentType => SC0010 : 카드결제, SC0030 : 실시간 계좌이체, SC0040 : 가상계좌
                $data['LGD_PAYTYPE'] = $xpay->Response("LGD_PAYTYPE",0);
                switch ($data['LGD_PAYTYPE'])
                {
                    case "SC0010" :
                    {
                        $finance = $data['payment02result'][0]['sFinanceName'];
                        $data['str_LGD_PAYTYPE'] = "신용카드(".$finance.")";
                        break;
                    }
                    case "SC0030" :
                    {
                        $data['str_LGD_PAYTYPE'] = "계좌이체";
                        break;
                    }
                    case "SC0040" :
                    {
                        $data['str_LGD_PAYTYPE'] = "무통장입금";
                        break;
                    }
                    case "SC0060" :
                    {
                        $data['str_LGD_PAYTYPE'] = "휴대폰결제";
                        break;
                    }
                    default :
                    {
                        $data['str_LGD_PAYTYPE'] = "결제수단(".$data['LGD_PAYTYPE'].")";
                        break;
                    }
                }
                //처리결과
                if( $data['LGD_PAYTYPE'] == "SC0040")
                {
                    $sFlag = $data['payment02result'][0]['sCasFlag'];
                    if($sFlag == "R"){
                        $sCasFlag = "가상계좌 입금대기";
                    }else if($sFlag == "I"){
                        $sCasFlag = "가상계좌 입금완료";
                    }else if($sFlag == "C"){
                        $sCasFlag = "취소";
                    }else{
                        $sCasFlag = "결제실패";
                    }
                    $data['str_Pay_Result'] = $sCasFlag;
                }
                else{
                    $data['str_Pay_Result'] = "결제완료";
                }


                //4-1.데이타 가공
                //글로벌 서비스 사용 여부
                $this->load->model('Code_model');
                $temp['sGlobal'] = $this->Code_model->get_code_list_array('K001', FALSE, null);
                $sGlobal = "";
                foreach ($temp['sGlobal'] as $key=>$value)
                {
                    if($key == $data['kclean']['sGlobal'])
                    {
                        $sGlobal = $value['sCodeName'];
                    }
                }
                $data['sGlobal'] = $sGlobal;

                //4-2. sms
                //문제있어 SMS 발송안함.
                //$this->sendsms_req(3,$xpay->Response("LGD_DELIVERYINFO",0));


                //5. 결제처리내역 페이지 나타내기
                $this->scripts= array("my_utility.js", "my_kclean.js");

                $this->_print($data);
            }
            else
            {
                //최종결제요청 결과 실패 DB처리
                //echo "최종결제요청 결과 실패 DB처리하시기 바랍니다.<br>";

                $msg = "결제요청이 실패하였습니다(X001)\\n";
                $msg .= "내용(" .$xpay->Response_Code(). ") : " .$xpay->Response_Msg();
                //$this->tinyjs->alert($msg);

                $data['payment02_result'] = "(001)";
                $data['payment02_result_msg'] = $xpay->Response_Msg();
                $data['respcode'] = $xpay->Response_Code();

                $data['LGD_PRODUCTINFO'] = $LGD_PRODUCTINFO; //lizzy

                $this->scripts= array("my_utility.js", "my_kclean.js");

                $this->_print($data);
            }
        }
        else// if ($xpay->TX())의 else
        {
            /*
             //2)API 요청실패 화면처리
            echo "결제요청이 실패하였습니다.  <br>";
            echo "TX Response_code = " . $xpay->Response_Code() . "<br>";
            echo "TX Response_msg = " . $xpay->Response_Msg() . "<p>";
            //최종결제요청 결과 실패 DB처리
            echo "최종결제요청 결과 실패 DB처리하시기 바랍니다.<br>";
            */

            $msg = "결제요청이 실패하였습니다(X002)\\n";
            $msg .= "내용(" .$xpay->Response_Code(). ") : " .$xpay->Response_Msg();
            //$this->tinyjs->alert($msg);

            $data['payment02_result'] = "(002)";
            $data['payment02_result_msg'] = $xpay->Response_Msg();
            $data['respcode'] = $xpay->Response_Code();

            $data['LGD_PRODUCTINFO'] = $LGD_PRODUCTINFO; //lizzy

            $this->scripts= array("my_utility.js", "my_kclean.js");

            $this->_print($data);
        }
        //view 에서 내용 철. finish도 처리하며 ㄴ될듯
    }



    private Function _payment02_finish($xpay)
    {
        //db에서 정보 가져오기
        $tmpProductType = "C";
        if( $xpay->Response("LGD_PRODUCTINFO",0) == "코로케이션" || $xpay->Response("LGD_PRODUCTINFO",0) == "기타" )
        {
            $tmpProductType = "C";
        }
        else if( $xpay->Response("LGD_PRODUCTINFO",0) == "서버호스팅" )
        {
            $tmpProductType = "S";
        }
        else if( $xpay->Response("LGD_PRODUCTINFO",0) == "K-Clean" ) //150514 lizzy
        {
            $tmpProductType = "K";
        }
        else
        {
            $tmpProductType = "C";
        }
        $oid = $xpay->Response("LGD_OID",0);
        $tid = $xpay->Response("LGD_TID",0);

        return $this->md_payment->payment02_finish($oid , $tid , $tmpProductType);
    }




    Public Function kcleanRequest_sendmail($xpay, $kclean)
    {

        $this->load->library('email');
        $this->load->helper('email');

        //이메일 목록 추출
        $this->load->model("Counsel_sendmail_model");
        $result = $this->Counsel_sendmail_model->get_counsel_servicemanager_email(12);//12 : K-Clean 수신대상자 메일
        $alarm = array();
        foreach( $result as $key=>$val)
        {
            $alarm[] = $val;
            //echo "</br>".$val["sMemberName"].",  ".$val["sMemberId"].",  ".$val["sInternalEmail"];
        }

        if( count($alarm) >0 )
        {
            foreach($alarm as $key=>$val)
            {
                $tolist[] = $val["sInternalEmail"];
            }
        }
        else
        {
            $tolist = MAIL_TO_PAYMENT;
        }

        //보내는 사람 - EMAIL_HELP

        //받는 사람
        $to_mail = MAIL_TO_PAYMENT;

        //메일제목 설정
        $title = "K-clean SOS 서비스 신청,결제가 접수되었습니다.";
        $mailtitle = "[알림]".$title;

        //메일 본문 내용
        $sMailDocumentBody    = '';

        //메일 본문 내용
        $sMailDocumentBody    = '
                  <!-- 본문 내용 -->
                  <table border="0" cellpadding="0" cellspacing="0" width="640">
                   <tr>
                    <td><font style="font-family:맑은 고딕,돋움; font-size:13px;">
                    안녕하세요.<br />
                    인터넷 인프라 전문기업 ㈜케이아이엔엑스입니다.<br />
                    K-Clean SOS 서비스 신청, 결제가 접수되었습니다.<br />
                    자세한 내용은 <a href="https://'.INTRANET_HOMEPAGE.'/login?code=SERVICE&url=https://ismframe.kinx.net/#/card.html" target="_blank">인트라넷&gt;서비스 관리&gt;매출관리&gt;카드결제 현황</a>에서 확인할 수 있습니다.</font><br /><br />
                ';

        $sMailDocumentBody.='
                    <table border="2" cellpadding="6" cellspacing="0" width="640" bordercolor="#222222" bordercolordark="#222222" bordercolorlight="#222222"  style="border-collapse:collapse;">
                    <caption style="font-family:맑은 고딕,돋움; font-size:12px; font-weight:bold; text-align: left;">[결제 정보]</caption>
                     <tr>
                      <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>업체명</b></font></td>
                      <td width="250"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$xpay['sDeliveryInfo'].'</font></td>
                      <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>업체아이디</b></font></td>
                      <td width="166"><font style="font-family:맑은 고딕,돋움; font-size:12px;"></font></td>
                     </tr>
                     <tr>
                      <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>신청자(주문번호)</b></font></td>
                      <td width="250"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$xpay['sBuyerName'].'('.$xpay['sOrderNumber'].')</font></td>
                      <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>결제수단</b></font></td>';

        //$PAYTYPE = "SC0040";
        //switch($PAYTYPE)
        switch($xpay['sPaymentType'])
        {
            case "SC0010" :
                $sPaytype = "신용카드";
                break;
            case "SC0030" :
                $sPaytype = "계좌이체";
                break;
            case "SC0040" :
                $sPaytype = "무통장입금";
                break;
            case "SC0060" :
                $sPaytype = "휴대폰결제";
                break;
        }

        $sMailDocumentBody.='
                      <td width="166"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$sPaytype.'</font></td>
                    </tr>
                    <tr>
                      <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>처리결과</b></font></td>';

        $sResult = $xpay['sResultMessage'];

        $sMailDocumentBody.='
                      <td width="250"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$sResult.'</font></td>
                      <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>결제금액</b></font></td>
                      <td width="166"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.number_format($xpay['nAmount'],0).'</font></td>
                     </tr>
                  ';
        //if($PAYTYPE == "SC0040")
        if($xpay['sPaymentType'] == "SC0040")
        {
            $sMailDocumentBody.='
                <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>입금은행</b></font></td>
                  <td width="250"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$xpay['sFinanceName'].'(은행)</font></td>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>입금계좌번호</b></font></td>
                  <td width="166"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$xpay['nAccountNumber'].'</font></td>
                </tr>
                ';
        }
        $sMailDocumentBody    .= "</table>";

        //고객정보
        $sMailDocumentBody .= "<br>";
        $sMailDocumentBody .= $this->make_kcleanRequest_email_customerinfo($kclean);
        //서비스정보
        $sMailDocumentBody .= "<br>";
        $sMailDocumentBody .= $this->make_kcleanRequest_email_serviceinfo($kclean);
        //사이트 정보
        $sMailDocumentBody .= "<br>";
        $sMailDocumentBody .= $this->make_kcleanRequest_email_siteinfo($kclean);


        $sMailDocumentBody .= '<br/>';
        $sMailDocumentBody .= '<br><br>';

        $data['bcc']        = "lizzy337@kinx.net";
        $data['title']      = $title; //내용의 제목
        $data['message']    = $sMailDocumentBody;
        $data['fromemail']  = EMAIL_HELP;
        $data['fromname']   = EMAIL_SENDMAIL_NAME;
        $data['to']         = $to_mail;
        $data['subject']    = $mailtitle; //메일 제목
        $data['footertype'] = 1;//발신전용
        $data['inout']      = 'in';//in or out // out이면 외부, 고객 발신, 고객센터 내용 추가되어야 함.
        $status = kinxMailSendFnc($data,FALSE);

    }


    public function make_kcleanRequest_email_customerinfo($kclean)
    {
        $sMailDocumentBody = '';
        $sMailDocumentBody.='
            <table border="2" cellpadding="6" cellspacing="0" width="640" bordercolor="#222222" bordercolordark="#222222" bordercolorlight="#222222"  style="border-collapse:collapse;">
            <caption style="font-family:맑은 고딕,돋움; font-size:12px; font-weight:bold; text-align: left;">[고객 정보]</caption>
                 <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>업체명</b></font></td>
                  <td width="250"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$kclean['sCompanyName'].'</font></td>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>업체 유형</b></font></td>
                  <td width="166"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$this->get_strCodeName($kclean["sOwnerType"],'G065').'</font></td>
                 </tr>
                              ';
        if($kclean['sOwnerType']=='B')
        {//개인
            $sMailDocumentBody.='
                <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>주민등록번호</b></font></td>
                  <td width="250"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$kclean['sPersonalNumber'].'</font></td>';
        }
        else
        {
            $sMailDocumentBody.='
                <tr>
                <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>사업자번호</b></font></td>
                <td width="250"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$kclean['sBusinessNumber'].'</font></td>';
        }
        $sMailDocumentBody.='
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>해외 구분</b></font></td>
                  <td width="166"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$this->get_strCodeName($kclean["sForeignYn"],'G126').'</font></td>
                 </tr>
                ';
        $sMailDocumentBody.='
            <tr>
                <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>대표자명</b></font></td>
                <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$kclean['sPresidentName'].'</font></td>
            </tr>';
        $sMailDocumentBody.='
            <tr>
                <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>주소</b></font></td>';

        $sAddress = null;
        if($kclean['sZipcode'] !='') { $sAddress    .= "(".$kclean['sZipcode'].") "; }
        if($kclean['sAddress1'] !='') { $sAddress .= $kclean['sAddress1']; }
        if($kclean['sAddress2'] !='') { $sAddress .= $kclean['sAddress2']; }
        $sMailDocumentBody.='
                <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$sAddress.'</font></td>
            </tr>
            <tr>
              <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>전화번호</b></font></td>
              <td width="250"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$kclean['sPhone'].'</font></td>
              <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>팩스번호</b></font></td>
              <td width="166"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$kclean['sFax'].'</font></td>
             </tr>
             <tr>
              <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>담당자 이름</b></font></td>
              <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$kclean['sName'].'</font></td>
             </tr>
             <tr>
              <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>담당자 전화</b></font></td>
              <td width="250"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$kclean['sInternalPhone'].'</font></td>
              <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>담당자 휴대전화</b></font></td>
              <td width="166"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$kclean['sMobilePhone'].'</font></td>
             </tr>
             <tr>
              <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>담당자 이메일</b></font></td>
              <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$kclean['sEmail'].'</font></td>
             </tr>
        </table>
                ';
        return $sMailDocumentBody;

    }

    public function make_kcleanRequest_email_serviceinfo($kclean)
    {
        $sMailDocumentBody = '';
        $sMailDocumentBody.='
            <table border="2" cellpadding="6" cellspacing="0" width="640" bordercolor="#222222" bordercolordark="#222222" bordercolorlight="#222222"  style="border-collapse:collapse;">
            <caption style="font-family:맑은 고딕,돋움; font-size:12px; font-weight:bold; text-align: left;">[서비스 정보]</caption>
                 <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>서비스</b></font></td>
                  <td width="250"><font style="font-family:맑은 고딕,돋움; font-size:12px;">K-클린</font></td>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>서비스 타입</b></font></td>
                  <td width="166"><font style="font-family:맑은 고딕,돋움; font-size:12px;">SOS 서비스</font></td>
                 </tr>
                <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>글로벌서비스</b></font></td>
                  <td width="250"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$this->get_strCodeName($kclean["sGlobal"],'K001').'</font></td>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>도메인 개수</b></font></td>
                  <td width="166"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$kclean['nDomainCount'].'개</font></td>
                 </tr>
                 <tr>
                   <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>기간</b></font></td>
                   <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$kclean['nServiceMonth'].'개월</font></td>
                 </tr>
                 <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>설치비</b></font></td>
                  <td width="250"><font style="font-family:맑은 고딕,돋움; font-size:12px;">무료</font></td>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>서비스 비용</b></font></td>
                  <td width="166"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.number_format($kclean['nServicePrice'],0).'원</font></td>
                 </tr>
        </table>
                              ';

             return $sMailDocumentBody;
    }

    public function make_kcleanRequest_email_siteinfo($kclean)
    {
        $sMailDocumentBody = '';
        $sMailDocumentBody.='
            <table border="2" cellpadding="6" cellspacing="0" width="640" bordercolor="#222222" bordercolordark="#222222" bordercolorlight="#222222"  style="border-collapse:collapse;">
            <caption style="font-family:맑은 고딕,돋움; font-size:12px; font-weight:bold; text-align: left;">[운영 사이트 정보]</caption>
                 <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>DNS운영</b></font></td>
                  <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$this->get_strCodeName($kclean["sDNS"],"K003").'</font></td>
                 </tr>
                <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>서비스 URL</b></font></td>';

        $arrURL = unserialize($kclean['sURL']);
        $strURL = "";

        if(isset($arrURL) && $arrURL != "")
        {
            foreach($arrURL as $key => $value )
            {
                foreach($value as $key2 => $value2)
                {
                    if($key2 == "URL") { $strURL .= " URL : ".$value2; }
                    if($key2 == "IP") { $strURL .= " IP : ".$value2; }
                    if($key2 == "TTL") { $strURL .= " TTL : ".$value2; }
                }
                $strURL .= "<br />";
            }
        }

        $sMailDocumentBody.='
                  <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$strURL.'</font></td>
                 </tr>
                          ';

        $sMailDocumentBody.='
                <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>테스트 계정</b></font></td>';

        $sTestInfo = "";
        if($kclean['sTestID'] !='') { $sTestInfo    .= " ID : ".$kclean['sTestID']; }
        if($kclean['sTestPW'] !='') { $sTestInfo    .= " PW : ".$kclean['sTestPW']; }

        $sMailDocumentBody.='
                  <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$sTestInfo.'</font></td>
                 </tr>
                 <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>방화벽(보안장비)</b></font></td>
                  <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$this->get_strCodeName($kclean["sWall"],"K004").'</font></td>
                 </tr>
                 <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>사이트 설명</b></font></td>
                  <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$kclean["sSiteDesc"].'</font></td>
                 </tr>
                 <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>일간 peak Traffic량 (out)/세션량</b></font></td>
                  <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$kclean["sTraffic"].'</font></td>
                 </tr>
                 <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>인증서(SSL)사용</b></font></td>
                  <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$this->get_strCodeName($kclean["sSSL"],"K005").'</font></td>
                 </tr>
                 <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>서비스 port</b></font></td>
                ';
        $sPort = "";
        if($kclean['sPort'] == 0 )
        {
            $sPort    .= $this->get_strCodeName($kclean['sPort'],'K006');
        }else{
            $sPort    .= $kclean['nPort'];
        }

        $sMailDocumentBody.='
                  <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$sPort.'</font></td>
                 </tr>
                 <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>웹서버 O/S정보</b></font></td>
                  <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$this->get_strCodeName($kclean["sOS"],"K007").'</font></td>
                 </tr>
                 <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>L4사용 유무</b></font></td>
                  <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$this->get_strCodeName($kclean["sL4"],"K008").'</font></td>
                 </tr>
                 <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>DB정보</b></font></td>
                  <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$this->get_strCodeName($kclean["sDB"],"K009").'</font></td>
                 </tr>
                 <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>대역폭</b></font></td>
                  <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$this->get_strCodeName($kclean["sBandwidth"],"K010").'</font></td>
                 </tr>
                 <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>로그인 페이지</b></font></td>
                  <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$this->get_strCodeName($kclean["sLoginpage"],"K011").'</font></td>
                 </tr>
                 <tr>
                  <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>해외 서비스</b></font></td>
                  <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$this->get_strCodeName($kclean["sGlobalService"],"K012").'</font></td>
                 </tr>
                ';
        $sMailDocumentBody.='</table>';


        return $sMailDocumentBody;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////




    private function get_strCodeName($code,$groupCode)
    {
        $strCodeName = '';

        $data["codelist"] = $this->md_code->get_code_list_array($groupCode, FALSE, null);
        //print_r($data["codelist"]);

        foreach ($data["codelist"] as $key=>$value)
        {
            //echo "<br /> key : ".$key." ,  code : ".$code." , value code :".$value['sCode'];
            if($value['sCode'] == $code)
            {
                $strCodeName = $value['sCodeName'];
                //echo "<br /> codename : ".$strCodeName." , code : ".$value['sCode'];
            }
        }
        return $strCodeName;
    }

    public function sendsms_req($step,$sCompName)
    {
        if($step == 1 )
        {
            $db_data['sc_msg'] = "[K클린] ".$sCompName."에서 SOS 서비스 신청 1단계 진행했음.";
        }
        else if($step == 3){
            $db_data['sc_msg'] = "[K클린] ".$sCompName."에서 SOS 서비스  신청, 결제함.";
        }
        $db_data['sc_sender'] = "02-526-0900";
        //대상자의 휴대폰 추출
        $this->load->model("Counsel_sendmail_model");
        $result = $this->Counsel_sendmail_model->get_counsel_servicemanager_email(12);//12 : K-Clean 수신대상자 전화번호

        $aryReceivers = array();
        if(!is_null($result))
        {
            if(ENVIRONMENT== 'development')
            {
                $aryReceivers[] = Array('name'=>'SYZ', 'number'=>'01026092132');
            }
            else
            {
                foreach($result as $key => $member)
                {
                    $aryReceivers[] = Array('name'=>$member['sMemberName'], 'number'=>$member['sMobilePhone']);
                }
            }
        }

        $aryParam = Array(
                'phones'=>$aryReceivers,
                'subject'=>'',
                'message'=>$db_data['sc_msg'],
                'callback'=>$db_data['sc_sender'],
                'type'=>'etc'
        );

        /* 170223 kclean 대량 sms 발송 사건 발생.
         * 페이지오픈 후 2년간 사용내역 없고 게시판 접수 및 영업을 통해서도 서비스 가능하므로 sms 발송 부분 주석처리함
        $url = 'sms/send.json';
        $smsParam = json_encode($aryParam);

        $this->load->library('rest');
        $this->rest->initialize(array('server'=>'http://api.kinx.net'));
        $this->rest->http_header('content-type','application/json;charset=UTF-8');
        $retStdClass = $this->rest->post($url, $smsParam, 'json');
        */

    }
}