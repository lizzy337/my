<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH ."controllers/Common.php");
class Testapicloud extends Common
{
	function __construct()
	{
            
		parent::__construct(); 
                
	}
        
        public function index()
        {
            redirect('errors/error404');
        }
        
        public function send_smstest()
        {
            echo "send_smstest";
            //if(ENVIRONMENT=='development')
            {
                
                
                $arReceivers = array();
                $arReceivers[0] = Array('name'=>'Nzy', 'number'=>'010-2774-7674');
 
                $date = date("Y-m-d H:i:s");
                $data = array(
                    'phones' => $arReceivers,
                    'subject' => 'stest',
                    'message'=>'msgtest_my_200_:'.$date,
                    'callback'=>'010-123-4567',
                    'type'=>'etc'
                    ); 
		$url = 'sms/send.json';
		$smsParam = json_encode($data);
		
		$this->load->library('rest');
		$this->rest->initialize(array('server'=>'http://apicloud1.kinx.net'));
		$this->rest->http_header('content-type','application/json;charset=UTF-8');
		$retStdClass = $this->rest->post($url, $smsParam, 'json');
            }  
        }
        
        Public Function send_consult(  )
	{
            echo "send_consult";
            $date = date("Y-m-d H:i:s");
            $postdata = array(
                    'sManagerName'     => 'LZY_TEST_MNG',
                    'sPhone' 		  => '010-2774-7674',
                    'sEmail'		      => 'lizzy337@kinx.net',
                    'nServiceType'	  => 3,
                    'sServiceName'	  => 'test_service_name',
                    'sTitle' 		  => 'apicloud_test',
                    'sQuestion'		  => 'apicloud test 중입니다. [200]'.$date,
                    'nContractSeq'	  => 0,
                    'nStatus'		  => 0,
                    'sAnswer'		  => '',
                    'nManagerSeq'      => 0,
                    'sOfficehours'	  => 0,
                    'sForeignYn'       => 'N',
                    'sCompanyName'     => 'TEST_COMPANYNAME',
                    'nVisitRoute'      => 6,
                    'sVisitRouteOther' => 0,
                         'nServiceType2'	  => null,
                 );
            $curl_post_data = http_build_query($postdata);
            $service_url = 'http://apicloud1.kinx.net/consult/send';//test
            $curl = curl_init($service_url);
            //$curl_post_data = http_build_query($_POST);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);

            $curl_response = curl_exec($curl);

                if ($curl_response === false) 
                {
                        $info = curl_getinfo($curl);
                         curl_close($curl);
                         die('error occured during curl exec. Additioanl info: ' . var_export($info));
                }
                curl_close($curl);
                //print_r("curl response"); print_r($curl_response); die;
                $decoded = json_decode($curl_response);

                header("Access-Control-Allow-Origin: *");
                header("Content-Type: application/json; charset=UTF-8");

            echo(json_encode($decoded));
            
            exit;
            
        }
        
         
        Public Function send_mailtest(  )
	{
            echo "send_mailtest";
            //if(ENVIRONMENT=='development')
            {
               $this->load->helper('email');

                //받는 사람
                //$to_name = EMAIL_SENDMAIL_NAME;
                $to_mail = "lizzy337@kinx.net";

                //메일제목 설정
                $date = date("Y-m-d H:i:s");
                $title = "APICLOUD 테스트";
                $mailtitle = "[알림]".$title."  : ".$date;

                //메일 본문 내용
                $sMailDocumentBody	= '';
                $sMailDocumentBody = '
                                <!-- 본문 내용 -->
                            <table border="0" cellpadding="0" cellspacing="0" width="640">
                           <tr>
                            <td><font style="font-family:맑은 고딕,돋움; font-size:13px;">
                            안녕하세요.<br />
                            인터넷 인프라 전문기업 ㈜케이아이엔엑스입니다.<br /> APICLOUD TEST 입니다.['.$date.']</font><br /><br />
                            </td>
                            </tr>
                            </table>';

                $sMailDocumentBody .= '<br><br>';

                $data['bcc'] = "lizzy337@kinx.net";
                $data['title'] = $title; //내용의 제목
                $data['message'] = $sMailDocumentBody;
                //$data['files'] = $rowfiles;
                $data['fromemail'] = EMAIL_HELP;
                $data['fromname'] = EMAIL_SENDMAIL_NAME;
                $data['to'] = $to_mail;
                $data['subject'] = $mailtitle; //메일 제목
                $data['footertype'] = 1;//발신전용
                $data['inout'] = 'in';//in or out // out이면 외부, 고객 발신, 고객센터 내용 추가되어야 함.

                $status = kinxMailSendFnc($data,TRUE); 
            }
	}
                 
}