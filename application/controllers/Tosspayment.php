<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/Common.php");
class Tosspayment extends Common{

    private $checkinarray = array(); //ISM의 tAccount.nMyCardPayment 값을 참고함

    // 생성자
    public function __construct() {

        parent::__construct();
        $this->load->helper('common');
    }





    /**
     * 결제안내 - 신용카드 결제 데이터 등록
     */
    Public Function card_post($result=NULL)
    {
        $postdata = $this->input->post(NULL, TRUE);
        $json = $this->_card_post_check($postdata);

        if($json['status'])
        {
            $this->load->library('tosscard');
            $json['approvaldata'] = $this->tosscard->set_olineapproval_data($postdata);
        }


        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($json));
    }






    /**
     *
     */
    private Function _card_post_check($postdata)
    {
        $this->load->helper('checkdata_helper');
        $json["status"] = TRUE;

        # post 정보 확인
        if(!chk_element('ts_ordername', $postdata))
        {
            $json = [
                "status"    => FALSE,
                "error_cd"  => 'TSSP01',
                "error_msg" => '서비스 구분을 선택해주세요.'
            ];
        }
        elseif(!chk_element('ts_productinfo', $postdata))
        {
            $json = [
                "status"    => FALSE,
                "error_cd"  => 'TSSP02',
                "error_msg" => '결제 사유를 입력해 주세요.'
            ];
        }
        elseif(!chk_element('ts_amount', $postdata) || !chk_param_numeric($postdata['ts_amount']) || $postdata['ts_amount'] < 1)
        {
            $json = [
                "status"    => FALSE,
                "error_cd"  => 'TSSP03',
                "error_msg" => '서비스요금을 선택해주세요.'
            ];
        }
        elseif(!chk_element('ts_buyer', $postdata))
        {
            $json = [
                "status"    => FALSE,
                "error_cd"  => 'TSSP04',
                "error_msg" => '결제자명을 입력해주세요.'
            ];
        }
        elseif(!chk_element('ts_customername', $postdata))
        {
            $json = [
                "status"    => FALSE,
                "error_cd"  => 'TSSP05',
                "error_msg" => '업체명를 입력해주세요.'
            ];
        }
        elseif(!chk_element('ts_customeremail', $postdata) || !chk_param_email($postdata['ts_customeremail']))
        {
            $json = [
                "status"    => FALSE,
                "error_cd"  => 'TSSP06',
                "error_msg" => '이메일주소를 입력해주세요.'
            ];
        }
        elseif(chk_element('ts_customermobile', $postdata) && (!chk_param_numeric($postdata['ts_customermobile']) || strlen($postdata['ts_customermobile']) < 9))
        {
            $json = [
                "status"    => FALSE,
                "error_cd"  => 'SPPP07',
                "error_msg" => '휴대폰 번호를 입력해주세요.'
            ];
        }
        elseif(!chk_element('orderid', $postdata) || !chk_param_numunderbar($postdata['orderid']) || strlen($postdata['orderid']) < 20)
        {
            $json = [
                "status"    => FALSE,
                "error_cd"  => 'TSSP08',
                "error_msg" => '주문ID가 확인되지 않습니다.'
            ];
        }

        return $json;

    }


}