<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/Common.php");

class Auth extends Common{

	public function __construct() {
		parent::__construct();

		$this->load->model("account_member_ism_model","md_accountmember");
		$this->load->model("account_ism_model","md_account");
		$this->load->model("contract_ism_model","md_contract");

	}

	// 로그인
	public function sign_in()
	{
		$returnType = "http";
		$result = $this->_proc_sign_in();

		//result : [result] => 1 [msg] => [url] => http://localhost:8820
		//print_r($result);
		//print_r($this->is_login);
		return $this->_return_result($result, $returnType);
	}

	public function sign_in_cloud()
	{
		$this->load->library("form_validation");

		$postdata = $this->input->post(NULL, TRUE);
		//print_r($postdata);
		if($this->form_validation->run("sign_in_cloud"))
		{
			$this->_saveid_cloud();//id 저장 관련
			/*
			$this->load->helper('cookie');
			//아이디 저장- check : 저장, uncheck : 쿠키삭제
			$postdata = $this->input->post(NULL, TRUE);

			$ret = set_cookie("saveid_mykinx_cloud", $postdata['userid'], 0);//0 : 브라우저가 열려있는 동안은 계속 유효\

			//아이디 저장, 비밀번호찾기, 클라우드 회원가입, 로그인 등 모든 기능은 장경수차장님

			 */

		}
	}

	private function _saveid_cloud()
	{
		$this->load->helper('cookie');
		//아이디 저장- check : 저장, uncheck : 쿠키삭제
		$postdata = $this->input->post(NULL, TRUE);
		//print_r($postdata);
		//print_r($postdata['saved_cloud_id']);
		$setFlag = false;
		$setFlag = ( isset($postdata['saved_cloud_id']) ) ? true : false;
		if( $setFlag )
		{
			$extime = time() + ( 86400 * 30 );//한달

			//저장확인 test
			//$ckid = get_cookie("saveid_mykinx_cloud");
			//print_r($ckid);

			$ret = set_cookie("saveid_mykinx_cloud", $postdata['userid'], $extime);
		}
		else
		{
			delete_cookie("saveid_mykinx_cloud");  //해당 id cookie 지우기
		}
	}

	public function sign_out()
	{
            $this->session->unset_userdata($this->cfg["session_key"]);
            $this->tinyjs->pageRedirect("/");
            exit;
	}

	public function sign_out_cloud() //클라우드 로그아웃 (클라우드 id로 로그인된 상태에서 '로그아웃'링크 눌렀을때)
	{
		$this->load->helper('cookie');
		delete_cookie('cloud_user');  //해당 id cookie 지우기
		$next = $this->input->get('next', TRUE);
		if(!$next){
			$next = '/';
		}
		$this->tinyjs->pageRedirect($next);
		exit;
	}

	public function index()
	{

		if($this->is_login)
		{
			redirect("/service/index");
		}
		else
		{
			$data = null;
			$this->_print($data);
		}
	}



	public function findidpw() { //id/pw 찾기 페이지  !!
                //redirect('https://www.kinx.net/error');
		$segment4 = $this->uri->segment(4);

		if(is_null($segment4)) $segment4 = "findid";
		$data['tabselect'] = $segment4;

 		$this->_print($data);
	}

	public function findid()//id 찾기버튼 클릭시. !!. sendmail 보내지 않음. 화면에 바로 띄워준다 (view/auth/findid)
	{
                //redirect('https://www.kinx.net/error');
		$data = $this->input->post();

		/*
		print_r("post data : ");
		print_r($data);
		die;
		*/

		//200928 값 체크 추가
		if($data['srch_name'] && $data['srch_email'] && $data['srch_duty'] )
		{
			//해당 업체의 id를 찾음
			$find['info'] = $this->md_accountmember->findId($data['srch_name'], $data['srch_email'], $data['srch_duty']);
			//va('findid findinfo');va($find['info']);die;
		}

		if( isset($find['info']) && $find['info']!=null )
		{
			//echo $find['info'][0]['sMemberId'];
			//echo "<br>".$find['info'][0]['sCompanyName'];

			$data['findid'] = $find['info'][0]['sMemberId'];
			$data['sCompanyName'] = $find['info'][0]['sCompanyName'];

 			$this->_print_uri($data,'/auth/findid_ok');
		}
		else
		{
 			$this->_print_uri($data, '/auth/findid_fail');
		}
	}

	public function findpw(){//비밀번호 찾기 버튼 클릭시. (mykinx 접속 id의 pw를 임시 발급하기) !!
		$postdata = $this->input->post();
		//print_r($postdata); echo "</br>"; die;

		$this->load->helper('string');

		//임시 비밀번호 발급. intranet, ism 동일하게 반영
		//비밀번호 생성
		$data['sPassword'] = random_string('alnum',10);
		$pwd['sPassword'] = $data['sPassword'];
		//echo ("<br>randompw : ".$data['sPassword']);

		$this->load->helper ( 'encryption' );
		//$encPw = password_hash($data['sPassword'], PASSWORD_BCRYPT,array("cost" => 10, 'salt'=>PASSWORD_SALT) );
		//php7 버전에서 salt 옵션 제거. 171108
		$encPw = password_hash($data['sPassword'], PASSWORD_BCRYPT);
		$pwd['sPassword'] = $encPw;
		//echo ("<br>encPw : ".$encPw);
		//die;

		///////////////////////////////////////////////////////////////////////////
                /*20200106 외부공격 query에서 해당 테이블을 접근하는 것으로 판단.
                 * 기능상 현재 contract_member_model->_findName 에서 바라보는 table( 인트라넷의 tContract ) 은 사용하지 않는다고 판단하므로 우선은 주석처리함.
                 * 만약 필요하다면 _fineName 부분에서 query 에 '?' 로 교체하기 - 20200108
		//start for intranet
		$this->load->model("contract_member_model");

		//해당 업체의 이름을 찾음
		$find_intra['info'] = $this->contract_member_model->_findName($postdata['srch_id'], $postdata['srch_email'], $postdata['srch_duty']);
		$bFindName = false;
		if( isset($find_intra['info']) && $find_intra['info']!=null )
		{
			//email, duty 비교해서 같으면 sContractName 값 이용하기
			if($find_intra['info'][0]['sEmail'] == $postdata['srch_email'] && $find_intra['info'][0]['sDutyType'] == $postdata['srch_duty'] )
			{
				$data['sCompanyName'] = $find_intra['info'][0]['sCompanyName'];
				$bFindName = true;
			}
			else {
				$data['sCompanyName'] = null;
				$bFindName = false;
			}
		}
		else
		{
			$bFindName = false;
		}

		if( $bFindName == true )
		{
			//contract_model 의 _update
			$this->load->model("contract_model");
			$where["nContractSeq"] = $find_intra['info'][0]['nContractSeq'];

			$this->contract_model->_update($pwd, $where);
		}
		//end for intranet
                */
		////////////////////////////////////////////////////////////////////////

		//start for ism-----------------------------------------------------
		//해당 업체의 이름을 찾음
		if($postdata['srch_id'] && $postdata['srch_email'] && $postdata['srch_duty'])
		{
			$find['info'] = $this->md_accountmember->findName($postdata['srch_id'], $postdata['srch_email'], $postdata['srch_duty']);
		}
		//print_r($find['info']); echo "</br>"; die;

		$bFindName = false;
		if( isset($find['info']) && $find['info']!=null )
		{
			//email, duty 비교해서 같으면 sContractName 값 이용하기
			if($find['info'][0]['sEmail'] == $postdata['srch_email'] && $find['info'][0]['sDutyType'] == $postdata['srch_duty'] )
			{
				$data['sCompanyName'] = $find['info'][0]['sCompanyName'];
				$bFindName = true;
			}
			else {
				$data['sCompanyName'] = null;
				$bFindName = false;
			}
		}
		else
		{
			$bFindName = false;
		}

		if( $bFindName == true )
		{

			//고객명이 있을경우 임시 비밀번호 발급 및 저장, email 발송

		 	//Account_model 의 _update
			$where = array(
					'nAccountSeq'=>$find['info'][0]['nAccountSeq']
			);

			$change_data = array(
					'sPassword'=>$data['sPassword']
			);

			$this->md_account->update($pwd, $where);

			//email로 비밀번호 전송
			$this->findInfo_sendmail_pw($find['info'][0]['sCompanyName'], $find['info'][0]['sEmail'], $data['sPassword'] );
			//by test. intranet 에서 쓰일 예정.
			//$this->findInfo_sendmail_id($find['info'][0]['sContractName'], $find['info'][0]['sEmail'], $data['sPassword'] );


			//고객사명, 메일발송 이메일 주소
			$data['sCompanyName'] = $find['info'][0]['sCompanyName'];
			$data['sEmail'] = $find['info'][0]['sEmail'];


			$this->_print_uri($data, '/auth/findpw_ok');

		}
		else
		{
			$data = null;
			$this->_print_uri($data, '/auth/findpw_fail');
		}
	}


	//toname = 받는 사람(회사명), $toemail = ui에 입력한 이메일정보, info = 임시pw
	public function findInfo_sendmail_pw($toname, $toemail, $info)
	{
		$this->load->helper('email');

		//받는 사람
		$tolist = $toemail;

		//받는 사람
		$infotype = "비밀번호";

		$reqName = $toname;
		$title =  "문의하신 ".$infotype."가 생성되었습니다.";
		$mailtitle = "[KINX]".$reqName."님 ".$title; //subject


		//메일 본문 내용
		$fontStyle = '<font style="font-family:맑은 고딕,돋움; font-size:13px;">';

		$sMailDocumentBody	= '';
		//메일 인사말
		$sMailDocumentBody = '<!-- 본문 내용 -->';
		$sMailDocumentBody .= '<table border="0" cellpadding="0" cellspacing="0"  style="margin:0 auto;  width:640px;">';
		$sMailDocumentBody .= '<tr>';
		$sMailDocumentBody .= '<td>'.$fontStyle;
		$sMailDocumentBody .= '안녕하세요.<br />';
		$sMailDocumentBody .= '인터넷 인프라 전문기업 ㈜케이아이엔엑스입니다.<br />';
		$sMailDocumentBody .= '문의하신 내용에 답변드립니다. 아래 내용을 확인해주십시오.</font><br><br>';
		//메일 본문
		$sMailDocumentBody .= '<table border="2" cellpadding="6" cellspacing="0"   bordercolor="#222222" bordercolordark="#222222" bordercolorlight="#222222"  style="margin:0 auto;  width:640px; border-collapse:collapse;">';
		$sMailDocumentBody .= '<tr>';
		$sMailDocumentBody .= '<td width="120" bgcolor="#eeeeee" align="center">'.$fontStyle.'<b>임시비밀번호</b></font></td>';
		$sMailDocumentBody .= '<td width="520">'.$fontStyle.$info.'</font></td>';
		$sMailDocumentBody .= '</tr>';
		$sMailDocumentBody .= '</table><br />';
		$sMailDocumentBody .= $fontStyle;
		$sMailDocumentBody .= '- 회원님의 비밀번호를 암호화하여 관리하기 때문에 임시 비밀번호를 발급하여 보내드립니다.<br />';
		$sMailDocumentBody .= '- 임시 비밀번호로 로그인 하신 후, <font color="#ff620c">반드시 비밀번호를 변경해 주시기 바랍니다.</font><br />';
		$sMailDocumentBody .= '- 비밀번호는 KINX &gt; MY KINX &gt; 회원정보관리 메뉴에서 변경하실 수 있습니다.<br /><br />';
		$sMailDocumentBody .= '감사합니다.</font>';
		$sMailDocumentBody .= '<br>';
		$sMailDocumentBody .= '<br>';

		$data['title'] = $title; //내용의 제목
		$data['message'] = $sMailDocumentBody;
		//$data['files'] = $rowfiles;
		$data['fromemail'] = EMAIL_HELP;
		$data['fromname'] = EMAIL_SENDMAIL_NAME;
		$data['to'] = $tolist;
		$data['subject'] = $mailtitle; //메일 제목
		$data['footertype'] = 2;//발신전용
		$data['inout'] = 'out';//in or out // out이면 외부, 고객 발신, 고객센터 내용 추가되어야 함.
		if(ENVIRONMENT == 'development')
			$data['bcc'] = "lizzy337@kinx.net";

		//메일 발송
		$this->load->helper('email');
		$status = kinxMailSendFnc($data, FALSE);
	}

	//toname = 받는 사람, info = id정보. 호출되지 않음. intranet 에서 쓰일 예정.
	public function findInfo_sendmail_id($toname, $toemail, $info){
		$this->load->library('email');
		$this->load->helper('email');

		//보내는 사람 - MAIL_FIND_INFO

		//받는 사람
		$to_name = $toname;
		$tolist = $toemail;

		//제목
		$infotype = "아이디";
		$subject = "[KINX] 문의하신 ".$infotype." 정보입니다.";

		$fontStyle = " style='font-family: dotum, Verdana; font-size: 12px; line-height: 18px; color:#393939;' ";
		$fontStyle2 = " style='font-family: dotum, Verdana; font-size: 16px; line-height: 18px; color:#393939;' ";

		$sMailDocumentBody	= "";
		$sMailDocumentBody	= mailheader();
		$sMailDocumentBody .= "<table style='width:580px;height:10% !important;vertical-align:middle;border-collapse:collapse' border='0' cellpadding='0' cellspacing='0'>";
		$sMailDocumentBody .= "<tr><td height='10' colspan='2'>&nbsp;</td></tr>";
		$sMailDocumentBody .= "<tr>";
		$sMailDocumentBody .= "  <td align='left' width='580px'>";
		$sMailDocumentBody .= "    <br><font size='4' ".$fontStyle2."><b>안녕하세요. (주)케이아이엔엑스입니다.</b></font>";
		$sMailDocumentBody .= "    <br><br><span ".$fontStyle.">KINX 서비스를 이용해 주셔서 진심으로 감사드립니다.<br>";
		$sMailDocumentBody .= "    아이디 정보를 알려드립니다.</span><br><br>";
		$sMailDocumentBody .= "    <table width='580px' border='0' cellpadding='0' cellspacing='0' bgcolor='#f1f1f1'>";
		$sMailDocumentBody .= "      <tr><td height='26px' colspan='2'>&nbsp;</td></tr>";
		$sMailDocumentBody .= "      <tr>";
		$sMailDocumentBody .= "        <td width='25px' height='30px'>&nbsp;</td>";
		$sMailDocumentBody .= "        <td width='555px'><font size='3'><b>아이디 : ".$info."</b></font></td>";
		$sMailDocumentBody .= "      </tr>";
		$sMailDocumentBody .= "      <tr><td height='26px' colspan='2'>&nbsp;</td></tr>";
		$sMailDocumentBody .= "    </table>";
		$sMailDocumentBody .= "  </td>";
		$sMailDocumentBody .= "</tr>";
		$sMailDocumentBody .= "<tr>";
		$sMailDocumentBody .= "  <td colspan='2' align='center'><br><br></td>";
		$sMailDocumentBody .= "</tr>";
		$sMailDocumentBody .= "<tr>";
		$sMailDocumentBody .= "  <td colspan='2' align='center'><p><a href='".EMAIL_SENDBASE_URL."my/' target='_blank'><img src='".EMAIL_SENDBASE_URL."images/mail/btn_mykinx.gif' width='102' height='35' style=\"margin:0;padding:0;border:0;\" alt='' /></a></p></td>";
		$sMailDocumentBody .= "</tr>";
		$sMailDocumentBody .= "</table>";
		$sMailDocumentBody .= "</div>";
		$sMailDocumentBody .= "<br><br><br>";
		$sMailDocumentBody .= mailfooter();
		//echo $sMailDocumentBody;

		$this->email->clear(TRUE);
		$this->email->from(MAIL_FIND_INFO, "문의사항");# 발신
		$this->email->to($tolist);# 수신

		//$this->email->bcc($define_bcc['BCC1'],$define_bcc['BCC2'],$define_bcc['BCC3']);	# 숨은참조
		$this->email->subject($subject);//제목
		$this->email->message($sMailDocumentBody);//content

		//$this->email->send();

		//echo $this->email->print_debugger();
	}

	public function find_idpw_req() //아이디/비밀번호 찾기 신청 페이지  !!
	{
		$data = null;
		$scripts[] = 'my_findidpw_req.js';
		$this->scripts = $scripts;
		$this->_print($data);
	}

	public function findidpwreq_finish() //아이디/비밀번호 찾기 신청 페이지
	{
		$data = null;
		$this->_print($data);
	}

	Public Function find_idpwreq_priv_doc_down()	{
		$this->load->helper('download');
		//$data = file_get_contents("/data/projects/kinx.net/www/doc/".FIND_IDPWREQ_FILE_PRV);//org
		//171107 변경. 물리서버 my 와 MOT 의 my 경로 체크.
		$ret = file_exists("/data/projects/kinx.net/www/doc/".FIND_IDPWREQ_FILE_PRV);
		if($ret === TRUE)
		{
			$data = file_get_contents("/data/projects/kinx.net/www/doc/".FIND_IDPWREQ_FILE_PRV);
		}
		else
		{
			$data = file_get_contents("/kinx/projects/kinx.net/doc/".FIND_IDPWREQ_FILE_PRV);
		}
		$name = FIND_IDPWREQ_FILE_PRV;
		force_download(mb_convert_encoding($name, 'euc-kr', 'utf-8'), $data);
	}

	Public Function find_idpwreq_comp_doc_down()	{
		$this->load->helper('download');
		//$data = file_get_contents("/data/projects/kinx.net/www/doc/".FIND_IDPWREQ_FILE_COM);//org
		//171107 변경. 물리서버 my 와 MOT 의 my 경로 체크.
		$ret = file_exists("/data/projects/kinx.net/www/doc/".FIND_IDPWREQ_FILE_COM);
		if($ret === TRUE)
		{
			$data = file_get_contents("/data/projects/kinx.net/www/doc/".FIND_IDPWREQ_FILE_COM);
		}
		else
		{
			$data = file_get_contents("/kinx/projects/kinx.net/doc/".FIND_IDPWREQ_FILE_COM);
		}
		$name = FIND_IDPWREQ_FILE_COM;
		force_download(mb_convert_encoding($name, 'euc-kr', 'utf-8'), $data);
	}

	//$reqType: C-단체, P-개인
	public function proc_find_idpw_req($reqType) //아이디 비밀번호 찾기 신청 페이지
	{

		$postdata = $this->input->post();
		//print_r ("<br>postdata<br>");
		//print_r ($postdata);
		//단체)addfile1_1img_dp : 사업자등록증 fullpath, addfile1_2img_dp : 신청자 fullpath (ex) C:\fakepath\파일명.확장자
		//개인)addfile2_1img_dp : 신청자 fullpath (ex) C:\fakepath\파일명.확장자

		//-start 자동입력방지 관련
		if($_POST['g-recaptcha-response'])
		{
		    $recaptcha = $_POST['g-recaptcha-response'];
		}

		if(!$recaptcha)
		{
		    $decoded= array("success"=>false,"message"=>"로봇이 아니면 체크해주세요.");
		    echo(json_encode($decoded));

		    exit;
		}
		else
		{

		    $ip = $_SERVER['REMOTE_ADDR'];

		    $secretKey = $_POST['secretkey'];

		    $url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$recaptcha."&remoteip=".$ip;

		    $resource =  file_get_contents( $url );
		    $response = json_decode($resource, true);

		    if(intval($response["success"]) !== 1)
		    {
		        $errorMessages = array(
		                'missing-input-secret' => 'The secret parameter is missing.',
		                'invalid-input-secret' => 'The secret parameter is invalid or malformed.',
		                'missing-input-response' => 'The response parameter is missing.',
		                'invalid-input-response' => 'The response parameter is invalid or malformed.',
		        );
		        if( isset($response["error-codes"][0]) )
		        { $msg = ($response["error-codes"][0])? $errorMessages[$response["error-codes"][0]] : "정상적인 접속이 아닌 것으로 판단됩니다." ;}
		        else
		        { $msg = "정상적인 접속이 아닌 것으로 판단됩니다." ; }
		        $decoded= array("success"=>false,"message"=>$msg);
		        echo(json_encode($decoded));
		        exit;
		    }
		}
		//-end

		//공통
		$data['sType'] = $reqType; //단체 or 개인
		//단체
		if( $reqType == "C")
		{
			$data['sCompanyName'] = $postdata['companyName']; //업체명
			$data['sBusinessNumber'] = $postdata['bizNo'];//사업자번호
			$data['sApplicantName'] = $postdata['reqName'];//신청자
			$data['sRelation'] = $postdata['relation'];//관계
			$data['sPhone'] = $postdata['phone'];//일반전화
			$data['sMobile'] = $postdata['cellphone'];//휴대전화
			$data['sEmail'] = $postdata['email_1']."@".$postdata['email_2']; //email
			$data['sFindType'] = $postdata['check_info']; //id 분실 or pw 분실 or 둘다(A)
			$data['sContent'] = $postdata['reason'];//사유
			$data['sReception'] = $postdata['check_doc'];//접수방법 (A:첨부파잉, F:팩스)
		}
		else
		{
			$data['sApplicantName'] = $postdata['userName'];//신청자 or 회원명
			$data['sBusinessNumber'] = $postdata['residentNo'];// 주민등록번호 앞자리
			$data['sPhone'] = $postdata['phone2'];//일반전화
			$data['sMobile'] = $postdata['cellphone2'];//휴대전화
			$data['sEmail'] = $postdata['email2_1']."@".$postdata['email2_2']; //email
			$data['sFindType'] = $postdata['check_info2']; //id 분실 or pw 분실 or 둘다(A)
			$data['sContent'] = $postdata['reason2'];//사유
			$data['sReception'] = $postdata['check_doc2'];//접수방법 (A:첨부파잉, F:팩스)
		}

		//1. db insert
		$this->load->model("Mykinx_findidpw_model");
		$req_insert_id = $this->Mykinx_findidpw_model->_insert($data);

                if(!$req_insert_id)
                {
                    $ret= array("success"=>false,"message"=>"신청에 실패하였습니다. 다시 문의해주세요.");
                    echo json_encode($ret); //php 배열을 JSON 문자열로 바꾼다.
                    exit;
                }
		$resultflag = false;
		if($req_insert_id > 0) //insert 가 성공했을 경우 true로 설정. 160526(이부분 안넣으면 팩스 설정시 resultflag가 false 상태임)
		{
			$resultflag = true;
		}
		$resultflag_1 = true; //fileupload, tFilesInfo  결과가 실패면 false로 설정 180516
		$resultflag_2 = true;
		$failmsg_1 = "";
		$failmsg_2 = "";
		//2. file upload
		if( $data['sReception'] == 'A') //서류첨부이면
		{
			#postdata 확인
			if(!is_null($postdata))
			{
				if( $reqType == 'C')//단체
				{
					/////첨부파일 - 1
					if(!is_null($postdata['addfile1_1img_dp'])  && strlen($postdata['addfile1_1img_dp'])>0 )
					{
						$files['substr']	= date("Ym")."/"; //파일저장 subdirectory
						//file upload
						if(!is_null($addfile1_1img	= $this->_do_upload('addfile1_1img') ) )
						{
							//echo "<br><br>addfile1_1img<br>"; print_r($addfile1_1img);
							if($addfile1_1img['result']=='error')
							{
								$resultflag_1 = false;
								//upload 실패시 요청내용 db삭제. $req_insert_id
								$where["nFindAccountSeq"] = $req_insert_id;
								$this->Mykinx_findidpw_model->_delete($where);

								$failmsg_1 = strip_tags($addfile1_1img['msg']);
								//$ret= array("success"=>false,"message"=>strip_tags($addfile1_1img['msg']));
								//echo json_encode($ret); //php 배열을 JSON 문자열로 바꾼다.
							}
							else
							{
								$files['files']['addfile1_1img'] = $addfile1_1img['msg'];

								//첨부파일 insertfile
								//db에 add - tFindAccount, tFilesInfo
								$this->load->model("Mykinx_findidpw_model");
								$ret = $this->Mykinx_findidpw_model->_insertFile($files, $req_insert_id);

								//db 등록 성공시  #파일이동 move_file
								if(!is_null($ret))
								{
                                                                    $resultflag_1 = true; //180516
                                                                    if(!is_null($addfile1_1img))
                                                                    {
                                                                            move_file($addfile1_1img['msg'], UPLOAD_TYPE_AUTH.$files['substr']);
                                                                            $resultflag_1 = true; //180516
                                                                    }
								}
                                                                else //db (tFilesInfo) insert 실패. 180516
                                                                {
                                                                    $ret= array("success"=>false,"message"=>"파일업로드에 실패하였습니다. 다시 문의해주세요.");
                                                                    echo json_encode($ret); //php 배열을 JSON 문자열로 바꾼다.
                                                                    exit;
                                                                }
							}
						}
						else
						{
							$resultflag_1 = false;
							$failmsg_1 = "파일등록에 실패하였습니다. 다시 문의해주세요.";
							//upload 실패시 요청내용 db삭제. $req_insert_id
							$where["nFindAccountSeq"] = $req_insert_id;
							$this->Mykinx_findidpw_model->_delete($where);

							$ret= array("success"=>false,"message"=>"파일등록에 실패하였습니다. 다시 문의해주세요.");
							echo json_encode($ret); //php 배열을 JSON 문자열로 바꾼다.
                                                        exit;
						}
						unset($files['files']['addfile1_1img']);
					}

					/////첨부파일 - 2
					if(!is_null($postdata['addfile1_2img_dp']) && strlen($postdata['addfile1_2img_dp'])>0 )
					{
						//echo "addfile 2 !!!!!<br>";
						$files['substr']	= date("Ym")."/"; //파일저장 subdirectory

						if(!is_null($addfile1_2img	= $this->_do_upload('addfile1_2img') ) )
						{
							//echo "<br><br>addfile1_2img<br>"; print_r($addfile1_2img);
							if($addfile1_2img['result']=='error')
							{
								$resultflag_2 = false;
								//upload 실패시 요청내용 db삭제. $req_insert_id
								$where["nFindAccountSeq"] = $req_insert_id;
								$this->Mykinx_findidpw_model->_delete($where);

								$failmsg_2 = strip_tags($addfile1_2img['msg']);
								//$ret= array("success"=>false,"message"=>strip_tags($addfile1_2img['msg']));
								//echo json_encode($ret); //php 배열을 JSON 문자열로 바꾼다.
							}
							else
							{
								$files['files']['addfile1_2img'] = $addfile1_2img['msg'];

								//첨부파일 insertfile
								//db에 add - tFindAccount, tFilesInfo
								$this->load->model("Mykinx_findidpw_model");
								$ret = $this->Mykinx_findidpw_model->_insertFile($files, $req_insert_id);

								//db 등록 성공시  #파일이동 move_file
								if(!is_null($ret))
								{
                                                                    $resultflag_2 = true; //180516
                                                                    if(!is_null($addfile1_2img))
                                                                    {
                                                                            move_file($addfile1_2img['msg'], UPLOAD_TYPE_AUTH.$files['substr']);
                                                                            $resultflag_2 = true; //180516
                                                                    }
								}
                                                                else //tFilesInfo sert 실패. 180516
                                                                {
                                                                    $ret= array("success"=>false,"message"=>"파일업로드에 실패하였습니다. 다시 문의해주세요.");
                                                                    echo json_encode($ret); //php 배열을 JSON 문자열로 바꾼다.
                                                                    exit;
                                                                }
							}
						}
						else
						{

							$resultflag_2 = false;
							$failmsg_2 = "파일등록에 실패하였습니다. 다시 문의해주세요.";
							//upload 실패시 요청내용 db삭제. $req_insert_id
							$where["nFindAccountSeq"] = $req_insert_id;
							$this->Mykinx_findidpw_model->_delete($where);

							$ret= array("success"=>false,"message"=>"파일등록에 실패하였습니다. 다시 문의해주세요.");
							echo json_encode($ret); //php 배열을 JSON 문자열로 바꾼다.
                                                        exit;
						}
                                                unset($files['files']['addfile1_2img']);
					}
					//echo "<br>flag1 ".$resultflag_1;
					//echo "<br>msg1 ".$failmsg_1;
					//echo "<br>flag2 ".$resultflag_2;
					//echo "<br>msg2 ".$failmsg_2;
					if( $resultflag_1 == true && $resultflag_2 == true)
					{
						$resultflag = true;
					}
					else
					{
						//실패시 둘중 하나라도 업로드 된 상태이므로 files에서 삭제
						unset($where);
						$where["nSubFileSeq"] = $req_insert_id;
						$this->Mykinx_findidpw_model->_delete($where,1);

						if( $resultflag_1  && $resultflag_2)
						{
							$ret= array("success"=>false,"message"=>"파일등록에 실패하였습니다. 다시 문의해주세요.");
							echo json_encode($ret); //php 배열을 JSON 문자열로 바꾼다.
                                                        exit;
						}
						else
						{
							if( $resultflag_1 == false )
                                                        {
                                                            //echo "<br>mailmsg  ".$failmsg_1;
                                                            $ret= array("success"=>false,"message"=>$failmsg_1);
                                                            echo json_encode($ret); //php 배열을 JSON 문자열로 바꾼다.
                                                            exit;
							}
                                                        else
                                                        {
                                                            $ret= array("success"=>false,"message"=>$failmsg_2);
                                                            echo json_encode($ret); //php 배열을 JSON 문자열로 바꾼다.
                                                            exit;
							}
						}
					}
				}
				else //개인. 첨부파일 1개
				{
					if(!is_null($postdata['addfile2_1img_dp']) && strlen($postdata['addfile2_1img_dp'])>0 )
					{
						//do_upload
						$files['substr']	= date("Ym")."/"; //파일저장 subdirectory

						if(!is_null($addfile2_1img	= $this->_do_upload('addfile2_1img') ) )
						{
							if($addfile2_1img['result']=='error')
							{
								$resultflag = false;
								//upload 실패시 요청내용 db삭제. $req_insert_id
								$where["nFindAccountSeq"] = $req_insert_id;
								$this->Mykinx_findidpw_model->_delete($where);

								//jsError(strip_tags($addfile2_1img['msg']));
								//return false;
								$ret= array("success"=>false,"message"=>strip_tags($addfile2_1img['msg']));
								echo json_encode($ret); //php 배열을 JSON 문자열로 바꾼다.
                                                                exit;
							}
							else
							{
								$files['files']['$addfile2_1img'] = $addfile2_1img['msg'];

								//db에 add - tFindAccount, tFilesInfo
								$this->load->model("Mykinx_findidpw_model");
								$ret = $this->Mykinx_findidpw_model->_insertFile($files, $req_insert_id);

								//db 등록 성공시  #파일이동 move_file
								if(!is_null($ret))
								{
                                                                    $resultflag = true; //180516
									if(!is_null($addfile2_1img))
									{
										move_file($addfile2_1img['msg'], UPLOAD_TYPE_AUTH.$files['substr']);

										$resultflag = true;
									}
								}
                                                                else //db(tFilesInfo) insert 실패 180516
                                                                {
                                                                    $ret= array("success"=>false,"message"=>"파일업로드에 실패하였습니다. 다시 문의해주세요.");
                                                                    echo json_encode($ret); //php 배열을 JSON 문자열로 바꾼다.
                                                                    exit;
                                                                }
							}
						}
						else
						{
                                                    $resultflag = false; //180516
							//upload 실패시 요청내용 db삭제. $req_insert_id
							$where["nFindAccountSeq"] = $req_insert_id;
							$this->Mykinx_findidpw_model->_delete($where);

							$ret= array("success"=>false,"message"=>"파일등록에 실패하였습니다. 다시 문의해주세요.");
							echo json_encode($ret); //php 배열을 JSON 문자열로 바꾼다.
                                                        exit;
						}

					}
				}//end 개인
			}
		}//end 서류첨부

		if( $resultflag == true )
		{
			//va("resultflag=======");
                        //va($ret);
			//va($data);
			//va($reqType); die;
			//3. 관리담당자에게 이메일
			$this->sendmail_findidpwreq($data, $reqType);

			//4-2 ajax 용
			$redirecturl = "/auth/findidpwreq_finish";
			$ret= array("success"=>true,"message"=>$redirecturl);
			echo json_encode($ret);
		}

	}//-end 오류처리, ajax


	//$data : db 에 입력한 내용들..., $reqType : C:단체, P:개인
	public function sendmail_findidpwreq_old($data, $reqType)
	{
		$this->load->library('email');
		$this->load->helper('email');

		//1. 해당서비스 유형=>아이디/비밀번호 찾기의 수신담당자에 해당하는 MemberSeq를 뽑아 email 추출
		$this->load->model("Counsel_sendmail_model");
		$result = $this->Counsel_sendmail_model->get_counsel_servicemanager_email('9');
		//print_r ($result);
		//1-1. 수신자가 여러명일 경우에 대한 처리
		$alarm = array();
		foreach( $result as $key=>$val)
		{
			$alarm[] = $val;
			//echo "</br>".$val["sMemberName"].",  ".$val["sMemberId"].",  ".$val["sInternalEmail"];
		}
		if( count($alarm) >0 )
		{
			foreach($alarm as $key=>$val)
			{
				$tolist[] = $val["sInternalEmail"];
			}
		}
		else
		{
			$tolist = MAIL_TO_DEFAULT;
		}
		$to_name = "아이디/비밀번호 찾기 서비스담당자";
		//1-2. 보내는 사람 - MAIL_FROM_INFO

		//제목
		$subject = "[알림] 아이디,비밀번호 찾기 접수. 인트라넷을 참고하세요.";

		$fontStyle = " style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; line-height: 20px; margin: 0; padding: 0;' ";

		$sMailDocumentBody	= "";
		$sMailDocumentBody	= mailheader();
		//$sMailDocumentBody .= "<table style='width:630px;height:10% !important;  vertical-align:middle;border-collapse:collapse'>";
		$sMailDocumentBody .= "<table style='width:600px;height:10% !important;  vertical-align:middle;border-collapse:collapse'>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td width='20%' height='1' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td width='30%' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td width='20%' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td width='30%' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= "</tr>";
		if( $reqType == 'C')
		{
			$sMailDocumentBody	.= "<tr>";
			$sMailDocumentBody	.= " <td height='30' width='20%' align='center' bgcolor='#f7f7f7' ".$fontStyle.">업체명</td>";
			$sMailDocumentBody	.= " <td width='30%' ".$fontStyle.">".$data['sCompanyName']."</td>";
			$sMailDocumentBody	.= " <td align='center' width='20%' bgcolor='#f7f7f7' ".$fontStyle.">사업자번호</td>";
			$sMailDocumentBody	.= " <td width='30%' ".$fontStyle.">".$data['sBusinessNumber']."</td>";
			$sMailDocumentBody	.= "</tr>";
			$sMailDocumentBody	.= "<tr>";
			$sMailDocumentBody	.= " <td height='1' bgcolor='#E8E8E8'></td>";
			$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
			$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
			$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
			$sMailDocumentBody	.= "</tr>";
			$sMailDocumentBody	.= "<tr>";
			$sMailDocumentBody	.= " <td height='30' width='20%' align='center' bgcolor='#f7f7f7' ".$fontStyle.">신청자명</td>";
			$sMailDocumentBody	.= " <td width='30%' ".$fontStyle.">".$data['sApplicantName']."</td>";
			$sMailDocumentBody	.= " <td align='center' width='20%' bgcolor='#f7f7f7' ".$fontStyle.">관계</td>";
			$sMailDocumentBody	.= " <td width='30%' ".$fontStyle.">".$data['sRelation']."</td>";
			$sMailDocumentBody	.= "</tr>";
			$sMailDocumentBody	.= "<tr>";
			$sMailDocumentBody	.= " <td height='1' bgcolor='#E8E8E8'></td>";
			$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
			$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
			$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
			$sMailDocumentBody	.= "</tr>";
		}
		else
		{
			$sMailDocumentBody	.= "<tr>";
			$sMailDocumentBody	.= " <td height='30' width='20%' align='center' bgcolor='#f7f7f7' ".$fontStyle.">회원명</td>";
			$sMailDocumentBody	.= " <td width='30%' ".$fontStyle.">".$data['sApplicantName']."</td>";
			$sMailDocumentBody	.= " <td align='center' width='20%' bgcolor='#f7f7f7' ".$fontStyle.">주민번호</td>";
			$sMailDocumentBody	.= " <td width='30%' ".$fontStyle.">".$data['sBusinessNumber']."</td>";
			$sMailDocumentBody	.= "</tr>";
			$sMailDocumentBody	.= "<tr>";
			$sMailDocumentBody	.= " <td height='1' bgcolor='#E8E8E8'></td>";
			$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
			$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
			$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
			$sMailDocumentBody	.= "</tr>";
		}
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='30' width='20%' align='center' bgcolor='#f7f7f7' ".$fontStyle.">일반전화</td>";
		$sMailDocumentBody	.= " <td width='30%' ".$fontStyle.">".$data['sPhone']."</td>";
		$sMailDocumentBody	.= " <td align='center' width='20%' bgcolor='#f7f7f7' ".$fontStyle.">휴대전화</td>";
		$sMailDocumentBody	.= " <td width='30%' ".$fontStyle.">".$data['sMobile']."</td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='1' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= "</tr>";

		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td align='center' height='30' width='20%' bgcolor='#f7f7f7' ".$fontStyle.">이메일</td>";
		$sMailDocumentBody	.= " <td width='80%' colspan='3' ".$fontStyle.">".$data['sEmail']."</td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='1' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= "</tr>";

		switch($data['sFindType'])
		{
			case 'I': $strFindInfo = "아이디 분실"; break;
			case 'P': $strFindInfo = "패스워드 분실"; break;
			case 'A': $strFindInfo = "아이디분실, 패스워드 분실"; break;
		}
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td align='center' height='30' width='20%' bgcolor='#f7f7f7' ".$fontStyle.">정보 선택</td>";
		$sMailDocumentBody	.= " <td width='80%' colspan='3' ".$fontStyle.">".$strFindInfo."</td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='1' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "</table>";
		$sMailDocumentBody	.= "<br><br>";

		$sMailDocumentBody .= mailfooter();
		//echo $sMailDocumentBody;

		$this->email->clear(TRUE);
		$this->email->from(MAIL_FROM_INFO, "상담접수");# 발신
		$this->email->to($tolist);# 수신
		//$define_bcc = eval('return ' . EMAIL_BLIND_CABON . ';');
		//$this->email->bcc($define_bcc['BCC1'],$define_bcc['BCC2'],$define_bcc['BCC3']);	# 숨은참조
		$this->email->subject($subject);//제목
		$this->email->message($sMailDocumentBody);//content

		$this->email->send();

		//echo $this->email->print_debugger();
	}

	public function sendmail_test()
	{
		$reqType = "C";
		if( $reqType == "C")
		{
			$data['sCompanyName'] = "업체명"; //업체명
			$data['sBusinessNumber'] = "123-45-678";
			$data['sApplicantName'] = "홍길동";
			$data['sRelation'] = "직원";
			$data['sPhone'] = "02-123-4567";
			$data['sMobile'] = "010-123-4567";
			$data['sEmail'] = "lizzy337@gmail.com";
			$data['sFindType'] = "A"; //id 분실 "I or pw 분실  "P" or 둘다(A)
			$data['sContent'] = "test";//사유
			$data['sReception'] = "A";//접수방법 (A:첨부파잉, F:팩스)
		}
		else
		{
			$data['sApplicantName'] = "회원명";
			$data['sBusinessNumber'] = "701000";
			$data['sPhone'] = "02-123-4567";
			$data['sMobile'] = "010-123-4567";
			$data['sEmail'] ="lizzy337@gmail.com";
			$data['sFindType'] = "A"; //id 분실 "I or pw 분실  "P" or 둘다(A)
			$data['sContent'] = "test";//사유
			$data['sReception'] = "A";//접수방법 (A:첨부파잉, F:팩스)
		}

		$this->sendmail_findidpwreq($data,$reqType);
	}
	//$data : db 에 입력한 내용들..., $reqType : C:단체, P:개인
	public function sendmail_findidpwreq($data, $reqType)
	{
		$this->load->helper('email');

		//1. 해당서비스 유형=>아이디/비밀번호 찾기의 수신담당자(9)에 해당하는 MemberSeq를 뽑아 email 추출
		$this->load->model("Counsel_sendmail_model");
		$result = $this->Counsel_sendmail_model->get_counsel_servicemanager_email('9');
		//print_r ($result);

		//1-1. 수신자가 여러명일 경우에 대한 처리
		$alarm = array();
		foreach( $result as $key=>$val)
		{
			$alarm[] = $val;
			//echo "</br>".$val["sMemberName"].",  ".$val["sMemberId"].",  ".$val["sInternalEmail"];
		}
		if( count($alarm) >0 )
		{
			foreach($alarm as $key=>$val)
			{
				$tolist[] = $val["sInternalEmail"];
			}
		}
		else
		{
			$tolist = EMAIL_HELP;
		}

                if(ENVIRONMENT=='development')
                {
                    $tolist = "lizzy337@kinx.net";
                }

		$to_name = EMAIL_SENDMAIL_NAME;
		//1-2. 보내는 사람 - MAIL_FROM_INFO

		//제목
		$title = "아이디,비밀번호 찾기가 접수되었습니다.";//메일 내부의 타이틀
		$subject = "[알림] ".$title;//메일을 보낼때 타이틀

		//$fontStyle = " style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; line-height: 20px; margin: 0; padding: 0;' ";

		$sMailDocumentBody	= '';

		//메일 인사말
		$sMailDocumentBody .='
				<!-- 본문 내용 -->
  				  <table border="0" cellpadding="0" cellspacing="0" width="640">
				   <tr>
				    <td><font style="font-family:맑은 고딕,돋움; font-size:13px;">
				    안녕하세요.<br />
				    인터넷 인프라 전문기업 ㈜케이아이엔엑스입니다.<br />
				    아이디, 비밀번호 찾기 상담이 접수되었습니다.<br />
				    자세한 내용은 <a href="https://'.INTRANET_HOMEPAGE.'/login?code=MANAGE&url=https://intraframe.kinx.net/faccount/list" target="_blank">인트라넷&gt;운영 관리&gt;일반문의&gt;고객ID/PW 찾기</a>에서 확인할 수 있습니다.<br /><br /></font>';
		//메일 본문
		$sMailDocumentBody .= '<table border="2" cellpadding="6" cellspacing="0" width="640" bordercolor="#222222" bordercolordark="#222222" bordercolorlight="#222222"  style="border-collapse:collapse;">';
		if( $reqType == 'C')
		{
			$sMailDocumentBody .='
			<tr>
				<td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>업체명</b></font></td>
				<td width="250"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$data['sCompanyName'].'</font></td>
				<td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>사업자번호</b></font></td>
				<td width="170"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$data['sBusinessNumber'].'</font></td>
			</tr>
			<tr>
				<td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>신청자명</b></font></td>
				<td width="250"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$data['sApplicantName'].'</font></td>
				<td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>관계</b></font></td>
				<td width="170"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$data['sRelation'].'</font></td>
			</tr>
			';
		}
		else {
			$sMailDocumentBody .= '
				     <tr>
				      <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>회원명</b></font></td>
				      <td width="250"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$data['sApplicantName'].'</font></td>
				      <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>주민번호</b></font></td>
				      <td width="170"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$data['sBusinessNumber'].'</font></td>
				     </tr>
					';
		}
		$sMailDocumentBody .='
					     <tr>
					      <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>일반전화</b></font></td>
					      <td width="250"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$data['sPhone'].'</font></td>
					      <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>휴대전화</b></font></td>
					      <td width="170"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$data['sMobile'].'</font></td>
					     </tr>
					     <tr>
					      <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>이메일</b></font></td>
					      <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$data['sEmail'].'</font></td>
					     </tr>';

		switch($data['sFindType'])
		{
			case 'I': $strFindInfo = "아이디 분실"; break;
			case 'P': $strFindInfo = "패스워드 분실"; break;
			case 'A': $strFindInfo = "아이디분실, 패스워드 분실"; break;
		}

		$sMailDocumentBody .='
					     <tr>
					      <td bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>정보 선택</b></font></td>
					      <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$strFindInfo.'</font></td>
					     </tr>';
		$sMailDocumentBody .= '</table><br/>';
		$sMailDocumentBody .= '<br><br>';

		$data['title'] = $title; //내용의 제목
		$data['message'] = $sMailDocumentBody;
		$data['fromemail'] = EMAIL_HELP;
		$data['fromname'] = EMAIL_SENDMAIL_NAME;
		$data['to'] = $tolist;
		$data['subject'] = $subject; //메일 제목
		$data['footertype'] = 1;//발신전용
		$data['inout'] = 'in';//in or out // out이면 외부, 고객 발신, 고객센터 내용 추가되어야 함.
		$status = kinxMailSendFnc($data,FALSE);
	}

	/**
	 * DO_UPLOAD
	 * 업로드 파일 처리
	 *
	 * @param string $fildname 업로드파일 input 필드명.
	 * @return NULL|array
	 */
	private  function _do_upload($fildname) //kinxintranet/customer/customer의 _do_upload()
	{
		# 업로드 파일정보가 비어 있다면.. NULL 리턴
		if(empty($_FILES[$fildname])||empty($_FILES[$fildname]['name'])) return  NULL;

		//print_r("<br>FILES<br>");
		//print_r	($_FILES);


		# 파일 업로드 설정
		$config['upload_path'] = UPLOAD_TYPE_TMP;
		$config['max_size']	= 0; //무제한
		$config['encrypt_name'] = 'TRUE';
		$config['remove_spaces'] = 'TRUE';
		$config['max_width']  = 0;
		$config['max_height']  = 0;
		$config['allowed_types'] = 'pdf|x-pdf|jpeg|jpg|png|gif|ppt|pdf|bmp|doc|docx|pptx|doc|xls';

		# 업로드 라이브러리 로드및 업로드 설정 적용
		$this->load->library('upload');
		$this->upload->initialize($config);

		# $fildname input 명의 파일 업로드 처리
		if ( ! $this->upload->do_upload($fildname)){
			//return NULL;
			$result['result']	= 'error';
			$result['msg']		=  $this->upload->display_errors();

		}
		else {
			$result['result']	= 'succes';
			$result['msg']		= $this->upload->data();
		}

		//va("do upload result : "); va($result);
		return $result;
	}



	public function atlogin()
	{
			//$ipaddr  = $_SERVER["REMOTE_ADDR"];
                        $ipaddr = $this->input->ip_address();
			#$permiIP = array('103.6.103','203.246.160','121.78.90.18','121.78.90.19','121.78.90.20','211.115.210.80','211.115.210.81');
			#$permiIP = array('121.78.32.132','121.78.32.133','121.78.32.134','103.6.103','203.246.160','182.161.120','121.78.35','211.196.205');
			$permiIP = array(
				'121.78.32.132','121.78.32.133','121.78.32.134',
				'103.6.103','203.246.160','182.161.120',
				'211.196.205.68','211.196.205.69','211.196.205.70','211.196.205.71','211.196.205.72','211.196.205.73',
				'121.78.35','121.78.34.132'
			);

			if(ENVIRONMENT=='development') array_push($permiIP,'127.0.0.1');

			# 허용 IP 체크
			if(!in_array($ipaddr, $permiIP) && !in_array(substr($ipaddr,0,9), $permiIP) && !in_array(substr($ipaddr,0,11), $permiIP))
			{
				redirect('errors/error404');
				die;
			}

			// Get 방식으로 3번재 세그먼트를 받는다.
			//$sg3 =  ( $this->uri->segment ( 4 ) );
			//kinx.net 개편으로 인해 my 분리. my.kinx.net. URI 형태가 바뀜 (mypage/auth/atlogin => auth/atlogin/)
			$sg3 =   $this->uri->segment ( 3 );

			// decoding
			$atlogin = explode("||",decryptIt(urldecode($sg3)));



			// 유효성 확인
			if(!is_array($atlogin) || count($atlogin)<>3 || $atlogin[2] < date("YmdH", strtotime("-1 hour")))//1시간 이네의 생성된 값만 처리
			{
				// 에러메세지 출력.
				jsError("잘못된 접근입니다.", "/index.php");
			}
			else
			{
				$returnType = "http";
				$result = $this->_proc_sign_in($atlogin);

				return $this->_return_result($result, $returnType);
			}
	}

	public function atlogin_cloudhub() //cloudhub 타이틀 MYKINX 눌렀을때. 기존 인트라넷과 달리 외부에서 접근하므로  허용 ip체크 풀자
	{
		// Get 방식으로 3번재 세그먼트를 받는다.
		//$sg3 =  ( $this->uri->segment ( 4 ) );
		//kinx.net 개편으로 인해 my 분리. my.kinx.net. URI 형태가 바뀜 (mypage/auth/atlogin => auth/atlogin/)
		$sg3 =   $this->uri->segment ( 3 );

		// decoding
		$atlogin = explode("||",decryptIt(urldecode($sg3)));



		// 유효성 확인
		if(!is_array($atlogin) || count($atlogin)<>3 || $atlogin[2] < date("YmdH", strtotime("-1 hour")))//1시간 이네의 생성된 값만 처리
		{
			// 에러메세지 출력.
			jsError("잘못된 접근입니다.", "/index.php");
		}
		else
		{
			$returnType = "http";
			$result = $this->_proc_sign_in($atlogin);

			return $this->_return_result($result, $returnType);
		}
	}

	private function _saveid()
	{
		$this->load->helper('cookie');
		//아이디 저장- check : 저장, uncheck : 쿠키삭제
		$postdata = $this->input->post(NULL, TRUE);
		//print_r($postdata);
		//print_r($postdata['saveid']);
		$setFlag = false;
		$setFlag = ( isset($postdata['saveid']) ) ? true : false;
		if( $setFlag )
		{
			$extime = time() + ( 86400 * 30 );//한달

			//저장확인 test
			//$ckid = get_cookie("saveid_mykinx");

			$ret = set_cookie("saveid_mykinx", $postdata['sMemberId'], $extime);
		}
		else
		{
			delete_cookie("saveid_mykinx");  //해당 id cookie 지우기
		}

	}

	//ISM
	private function _proc_sign_in($login=FALSE)
	{
		$result = array();
		$this->load->library("form_validation");

		$postdata = $this->input->post(null, true);
		//print_r($postdata);
		$sMemberId = $this->input->post("sMemberId");

		if($this->form_validation->run("sign_in_my"))
		{
			$this->_saveid(); //아이디 저장 check 여부에 따른 쿠키처리

			$this->load->helper("encryption");

			$sMemberId = $this->input->post("sMemberId");
			$contract = $this->md_account->get_account_byId($sMemberId);
			//va('contract '); va($contract);

			$nAccountSeq = $contract['nAccountSeq'];
			$serviceone = $this->md_contract->get_ServiceCode($nAccountSeq);
			//va('serviceone'); va($serviceone);

			$companyStatus = $this->md_account->get_AccountStatus($nAccountSeq);
			//va('companyStatus'); va($companyStatus);
			//die;
			if(sizeof($companyStatus))
			{
				$sStatus = $companyStatus[0]['sStatus'];
				//회사 상태에 따른 로그인 체크
				$arrT = array("Y","E");//정상, E=>해지이지만 mykinx는 볼수 있도록 (ex:메가파일)
				$arrF = array("S","P","C","X","W","R");//해지,직권정지,명의변경,삭제,일시정지, 직권해지
				if( in_array($sStatus, $arrT))
				{
					$result["result"] = TRUE;

					/* 20210507 김중환 M요청으로 해제
					#=========================================================================
					# 뉴링크 임시 설정 고객 요청으로 로그인 제안 처리 20200624
					#=========================================================================
					if($sMemberId == 'newlinkcorp')
					{
						$result["result"] = FALSE;
						$result["url"] = $this->input->server("HTTP_REFERER");
						$result["msg"] = "고객 요청으로 이용이 잠시 중지되었습니다. 관리자에게 문의하세요.";

						return $result;
					}
					#=========================================================================
					# 뉴링크 임시 설정 고객 요청으로 로그인 제안 처리 20200624
					#=========================================================================
					*/
				}
				else if (in_array($sStatus, $arrF)){
					$result["result"] = FALSE;
					$result["url"] = $this->input->server("HTTP_REFERER");
					$result["msg"] = "해당 아이디는 사용하실 수 없는 아이디입니다. 관리자에게 문의하세요.";

					return $result;
				}
			}

			$result["msg"] = "아이디/비밀번호를 다시 확인하세요.\\n등록되지 않았거나, 아이디/비밀번호를 잘못 입력하셨습니다.";
			if(isset($contract["sPassword"]))
			{
				if(password_verify($this->input->post("sPassword"), $contract["sPassword"]))
				{
					$sessionAry[$this->cfg["session_key"]] = $contract["sAccountID"];
					$sessionAry['nContractSeq'] = $contract['nAccountSeq'];
					$sessionAry['nCompanySeq'] = $contract['nCustomerSeq'];
					$sessionAry['sServiceType'] = $serviceone['serviceType']; //1 - IX, 1 보다 크면 2 - IDC

					$this->session->set_userdata($sessionAry);

					$result["result"] = TRUE;
					$result["url"] = $this->input->server("HTTP_REFERER")."Auth/index";
					$result["msg"] = NULL;

					//print_r($sessionAry);
					//print_r($result);
				}
				else
				{
					$result["result"] = FALSE;
					$result["url"] = $this->input->server("HTTP_REFERER");
				}
			}
			else
			{
				$result["result"] = FALSE;
				$result["url"] = $this->input->server("HTTP_REFERER");
			}
		}
		else
		{
			$result["result"] = FALSE;
			$result["url"] = $this->input->server("HTTP_REFERER");
			$result["msg"] = str_replace("\n","",strip_tags(validation_errors()));
		}
		//va($login);

		//자동로그인 처리
		if(is_array($login))
		{
			//$this->load->model("contract_model");
			//$this->load->model("contract_service_model");
			$this->load->model("account_ism_model","md_account");
			$this->load->model("contract_ism_model","md_contract");
			$this->load->model("autologin_model");

			$sMemberId = $login[0];
			$contract = $this->md_account->get_account_byId($sMemberId);

			$nAccountSeq = $contract['nAccountSeq'];
			$serviceone = $this->md_contract->get_ServiceCode($nAccountSeq);

			//자동로그인 이력 생성
			//$this->autologin_model->_insert(array("sMemberId"=>$login[0],"sCustomerId"=>$login[1],"sKeyDate"=>$login[2]));
			//17720, customerid,와 memberid가 바뀌게 들어가고 있는것 수정
			//새로운 intranet open으로 인해 memberid안에 id가 아닌 사번이 들어가게 됨.
			$this->autologin_model->_insert(array("sCustomerId"=>$login[0],"sMemberId"=>$login[1],"sKeyDate"=>$login[2]));

			$companyStatus = $this->md_account->get_AccountStatus($nAccountSeq);
			$sStatus = $companyStatus[0]['sStatus'];

			//회사 상태에 따른 로그인 체크
			$arrT = array("Y","E");//정상, E=>해지이지만 mykinx는 볼수 있도록 (ex:메가파일)
			$arrF = array("S","P","C","X","W","R");//해지,직권정지,명의변경,삭제,일시정지, 직권해지
			if( in_array($sStatus, $arrT))
			{
				$result["result"] = TRUE;
			}
			else if (in_array($sStatus, $arrF))
			{
				$result["result"] = FALSE;
				$result["url"] = $this->input->server("HTTP_REFERER");
				$result["msg"] = "해당 아이디는 사용하실 수 없는 아이디입니다. 관리자에게 문의하세요.";

				return $result;
			}

			$sessionAry[$this->cfg["session_key"]] = $contract["sAccountID"];
			$sessionAry['nContractSeq'] = $contract['nAccountSeq'];
			$sessionAry['nCompanySeq'] = $contract['nCustomerSeq'];
			$sessionAry['sCloudEssexId'] = null;//$contract['sCloudEssexId'];
			$sessionAry['sServiceType'] = $serviceone['serviceType'];//1 - IX, 1 보다 크면 2 - IDC

			$this->session->set_userdata($sessionAry);

			$result["result"] = TRUE;
			$result["url"] = '/';
			$result["msg"] = NULL;
		}

		//va($result);die;
		return $result;
	}

	private function _return_result($result, $returnType)
	{
		if($returnType === "http")
		{
			if($result["result"])
			{
				$this->tinyjs->pageRedirect($result["url"]);
			}
			else
			{
				$this->tinyjs->pageBack($result["msg"]);
			}
		}
		else if ($returnType === "json")
		{
			echo json_encode($result);
		}
		return true;
	}

	/////////////////////////////////////////////////////////////////////////
	/* 본인 인증 관련 모듈 */
	/////////////////////////////////////////////////////////////////////////

	//인증 키 받기
	public function check_certification()
	{
		//**************************************************************************************************************
		//NICE평가정보 Copyright(c) KOREA INFOMATION SERVICE INC. ALL RIGHTS RESERVED

		//서비스명 :  체크플러스 - 안심본인인증 서비스
		//페이지명 :  체크플러스 - 메인 호출 페이지

		//보안을 위해 제공해드리는 샘플페이지는 서비스 적용 후 서버에서 삭제해 주시기 바랍니다.
		//방화벽 정보 : IP 121.131.196.215 , Port 80, 443
		//**************************************************************************************************************

		//session_start();

		$sitecode = NICE_SITECODE;			// NICE로부터 부여받은 사이트 코드
		$sitepasswd = NICE_SITEPASSWORD;	// NICE로부터 부여받은 사이트 패스워드
		$reqkey = NICE_REQKEY;

		// Linux = /절대경로/ , Window = D:\\절대경로\\ , D:\절대경로\
		$cb_encode_path = NICE_ENCODEPATH;

		/*
		┌ cb_encode_path 변수에 대한 설명  ──────────────────────────────────
			모듈 경로설정은, '/절대경로/모듈명' 으로 정의해 주셔야 합니다.

			+ FTP 로 모듈 업로드시 전송형태를 'binary' 로 지정해 주시고, 권한은 755 로 설정해 주세요.

			+ 절대경로 확인방법
			1. Telnet 또는 SSH 접속 후, cd 명령어를 이용하여 모듈이 존재하는 곳까지 이동합니다.
			2. pwd 명령어을 이용하면 절대경로를 확인하실 수 있습니다.
			3. 확인된 절대경로에 '/모듈명'을 추가로 정의해 주세요.
		└────────────────────────────────────────────────────────────────────
		*/

		$authtype = "M";      		// 없으면 기본 선택화면, X: 공인인증서, M: 핸드폰, C: 카드 (1가지만 사용 가능)

		$popgubun 	= "N";		//Y : 취소버튼 있음 / N : 취소버튼 없음
		$customize 	= "";		//없으면 기본 웹페이지 / Mobile : 모바일페이지 (default값은 빈값, 환경에 맞는 화면 제공)

		$gender = "";      		// 없으면 기본 선택화면, 0: 여자, 1: 남자

		//$reqseq = "REQ_0123456789";     // 요청 번호, 이는 성공/실패후에 같은 값으로 되돌려주게 되므로
										// 업체에서 적절하게 변경하여 쓰거나, 아래와 같이 생성한다.


		// 실행방법은 싱글쿼터(`) 외에도, 'exec(), system(), shell_exec()' 등등 귀사 정책에 맞게 처리하시기 바랍니다.
		$reqseq = md5($sitecode.$reqkey.round(microtime(true) * 1000));

		// CheckPlus(본인인증) 처리 후, 결과 데이타를 리턴 받기위해 다음예제와 같이 http부터 입력합니다.
		// 리턴url은 인증 전 인증페이지를 호출하기 전 url과 동일해야 합니다. ex) 인증 전 url : http://www.~ 리턴 url : http://www.~
		$returnurl = NICE_RETURNURL; // 성공시 이동될 URL
		$errorurl = NICE_ERRORURL;		// 실패시 이동될 URL

		// reqseq값은 성공페이지로 갈 경우 검증을 위하여 세션에 담아둔다.
		$_SESSION["REQ_SEQ"] = $reqseq;

		// 입력될 plain 데이타를 만든다.
		$plaindata = "7:REQ_SEQ" . strlen($reqseq) . ":" . $reqseq .
					"8:SITECODE" . strlen($sitecode) . ":" . $sitecode .
					"9:AUTH_TYPE" . strlen($authtype) . ":". $authtype .
					"7:RTN_URL" . strlen($returnurl) . ":" . $returnurl .
					"7:ERR_URL" . strlen($errorurl) . ":" . $errorurl .
					"11:POPUP_GUBUN" . strlen($popgubun) . ":" . $popgubun .
					"9:CUSTOMIZE" . strlen($customize) . ":" . $customize .
					"6:GENDER" . strlen($gender) . ":" . $gender ;
		//print_r("<br>-----------------<br>");
		//va('plaindata'); va($plaindata);
		$enc_data = `$cb_encode_path ENC $sitecode $sitepasswd $plaindata`;
		//va('<br>enc_data : '.$enc_data);
		//die;

		$returnMsg = "";

		if( $enc_data == -1 )
		{
			$returnMsg = "암/복호화 시스템 오류입니다.";
			$enc_data = "";
		}
		else if( $enc_data== -2 )
		{
			$returnMsg = "암호화 처리 오류입니다.";
			$enc_data = "";
		}
		else if( $enc_data== -3 )
		{
			$returnMsg = "암호화 데이터 오류 입니다.";
			$enc_data = "";
		}
		else if( $enc_data== -9 )
		{
			$returnMsg = "입력값 오류 입니다.";
			$enc_data = "";
		}


		$rtn['msg'] = $returnMsg;
		$rtn['data'] = $enc_data;
		$this->output->set_content_type('application/json')->set_output(json_encode($rtn));
	}

	//본인 인증 성공시 호출
	public function checkPlusSuccess()
	{
		//**************************************************************************************************************
		//NICE평가정보 Copyright(c) KOREA INFOMATION SERVICE INC. ALL RIGHTS RESERVED

		//서비스명 :  체크플러스 - 안심본인인증 서비스
		//페이지명 :  체크플러스 - 결과 페이지

		//보안을 위해 제공해드리는 샘플페이지는 서비스 적용 후 서버에서 삭제해 주시기 바랍니다.
		//인증 후 결과값이 null로 나오는 부분은 관리담당자에게 문의 바랍니다.
		//**************************************************************************************************************

		$sitecode = NICE_SITECODE;				// NICE로부터 부여받은 사이트 코드
		$sitepasswd = NICE_SITEPASSWORD;				// NICE로부터 부여받은 사이트 패스워드

		// Linux = /절대경로/ , Window = D:\\절대경로\\ , D:\절대경로\
		$cb_encode_path = NICE_ENCODEPATH;

		if(!isset($_REQUEST["EncodeData"]))
			$enc_data = $_GET["EncodeData"];		// 암호화된 결과 데이타
		else
			$enc_data = $_REQUEST["EncodeData"];		// 암호화된 결과 데이타

			//////////////////////////////////////////////// 문자열 점검///////////////////////////////////////////////
		if(preg_match('~[^0-9a-zA-Z+/=]~', $enc_data, $match))
		{
			echo "입력 값 확인이 필요합니다 : ".$match[0];
			exit;
		} // 문자열 점검 추가.
		if(base64_encode(base64_decode($enc_data))!=$enc_data)
		{
			echo "입력 값 확인이 필요합니다";
			exit;
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		if ($enc_data != "")
		{
			$plaindata = `$cb_encode_path DEC $sitecode $sitepasswd $enc_data`; // 암호화된 결과 데이터의 복호화

			//$plaindata = -4;//test

			$requestnumber = "";
			$responsenumber = "";
			$authtype = "";
			$name = "";
			$utf8name = "";
			$birthdate = "";
			$gender = "";
			$nationalinfo = "";
			$success_msg = "";
			$params = array(
				"authtype" => $authtype,
				"sname" => $name,
				"utf8name" => $utf8name,
				"birthdate" => $birthdate,
				"gender" => $gender,
				"nationalinfo" => $nationalinfo,
				"success_msg" => $success_msg,
				"plaindata" => $plaindata
			);

			if ($plaindata == -1)
			{
				// 암/복호화 시스템 오류
				$success_msg = "시스템 에러발생 에러코드 (code : -1) : 암/복호화 시스템 오류";
				$params['success_msg'] = $success_msg;
			}
			else if ($plaindata == -4)
			{
				// 복호화 처리 오류
				$success_msg = "시스템 에러발생 에러코드 (code : -4) : 복호화 처리 오류";
				$params['success_msg'] = $success_msg;
			}
			else if ($plaindata == -5)
			{
				// HASH값 불일치 - 복호화 데이터는 리턴됨
				$success_msg = "시스템 에러발생 에러코드 (code : -5) : HASH값 불일치";
				$params['success_msg'] = $success_msg;
			}
			else if ($plaindata == -6)
			{
				// 복호화 데이터 오류
				$success_msg = "시스템 에러발생 에러코드 (code : -6) : 복호화 데이터 오류";
				$params['success_msg'] = $success_msg;
			}
			else if ($plaindata == -9)
			{
				// 입력값 오류
				$success_msg = "시스템 에러발생 에러코드 (code : -9) : 입력값 오류";
				$params['success_msg'] = $success_msg;
			}
			else if ($plaindata == -12)
			{
				// 사이트 비밀번호 오류
				$success_msg = "시스템 에러발생 에러코드 (code : -12) : 사이트 비밀번호 오류";
				$params['success_msg'] = $success_msg;
			}
			else if($plaindata)
			{
				// 복호화가 정상적일 경우 데이터를 파싱합니다.
				$ciphertime = `$cb_encode_path CTS $sitecode $sitepasswd $enc_data`; // 암호화된 결과 데이터 검증 (복호화한 시간획득)

				$requestnumber = $this->GetValue($plaindata , "REQ_SEQ");
				$responsenumber = $this->GetValue($plaindata , "RES_SEQ");
				$authtype = $this->GetValue($plaindata , "AUTH_TYPE"); // 인증수단 (M:휴대폰, C:카드, X:인증서, P:삼성패스)
				$name = $this->GetValue($plaindata , "NAME"); // charset euc-kr (이름)
				$utf8name = $this->GetValue($plaindata , "UTF8_NAME"); // charset utf8 (이름)
				$birthdate = $this->GetValue($plaindata , "BIRTHDATE"); // 생년월일 (YYYYMMDD)
				$gender = $this->GetValue($plaindata , "GENDER"); // 성별코드 (0:여성 , 1:남성)
				$nationalinfo = $this->GetValue($plaindata , "NATIONALINFO"); // 내/외국인정보 (0:내국인, 1:외국인)

				$debug_msg = "";
				$debug_msg .= "<br>requestnumber : ".$requestnumber;
				$debug_msg .= "<br>responsenumber : ".$responsenumber;
				$debug_msg .= "<br>authtype : ".$authtype;
				$debug_msg .= "<br>name : ".$name;
				$debug_msg .= "<br>utf8name : ".$utf8name;
				$debug_msg .= "<br>birthdate : ".$birthdate;
				$debug_msg .= "<br>gender : ".$gender;
				$debug_msg .= "<br>nationalinfo : ".$nationalinfo;
				$debug_msg .= "<br>success_msg : ".$success_msg;
				$debug_msg .= "<br>plaindata : ".$plaindata;


				//print_r($debug_msg);die;//
				//print_r($_SESSION);die;
/*
				if(empty($_COOKIE["REQ_SEQ"]))
				{
					print_r("인증시간이 초과되었습니다.");
					exit;
				}
*/
//				if(strcmp($_COOKIE["REQ_SEQ"], $requestnumber) != 0)
				if(strcmp($_SESSION["REQ_SEQ"], $requestnumber) != 0)
				{
					$success_msg = "정보가 다릅니다. 올바른 경로로 접근하시기 바랍니다.";
					$params['success_msg'] = $success_msg;
					$params['plaindata'] = 0;
					//print_r("정보가 다릅니다. 올바른 경로로 접근하시기 바랍니다.");
					/*
					$requestnumber = "";
					$responsenumber = "";
					$authtype = "";
					$name = "";
					$utf8name = "";
					$birthdate = "";
					$gender = "";
					$nationalinfo = "";
					$success_msg = "";
					$plaindata = 0;
					*/
				}
				else
				{
					$success_msg = "인증이 완료되었습니다.";
					$plaindata_no = 1;
					$params = array(
						"authtype" => $authtype,
						"sname" => $name,
						"utf8name" => urldecode($utf8name),
						"birthdate" => $birthdate,
						"gender" => $gender,
						"nationalinfo" => $nationalinfo,
						"success_msg" => $success_msg,
						"plaindata" => $plaindata_no
					);
				}
			}

			$this->_print_uri($params,"auth/v_self_auth_success");
		}

	}

	//본인 인증 성공시 호출
	public function checkPlusFailed()
	{
		//**************************************************************************************************************
		//NICE평가정보 Copyright(c) KOREA INFOMATION SERVICE INC. ALL RIGHTS RESERVED

		//서비스명 :  체크플러스 - 안심본인인증 서비스
		//페이지명 :  체크플러스 - 결과 페이지

		//보안을 위해 제공해드리는 샘플페이지는 서비스 적용 후 서버에서 삭제해 주시기 바랍니다.
		//인증 후 결과값이 null로 나오는 부분은 관리담당자에게 문의 바랍니다.
		//**************************************************************************************************************
		$sitecode = NICE_SITECODE;				// NICE로부터 부여받은 사이트 코드
		$sitepasswd = NICE_SITEPASSWORD;				// NICE로부터 부여받은 사이트 패스워드

		// Linux = /절대경로/ , Window = D:\\절대경로\\ , D:\절대경로\
		$cb_encode_path = NICE_ENCODEPATH;


		if(!isset($_REQUEST["EncodeData"]))
			$enc_data = $_GET["EncodeData"];		// 암호화된 결과 데이타
		else
			$enc_data = $_REQUEST["EncodeData"];		// 암호화된 결과 데이타

		//////////////////////////////////////////////// 문자열 점검///////////////////////////////////////////////
		if(preg_match('~[^0-9a-zA-Z+/=]~', $enc_data, $match))
		{
			echo "입력 값 확인이 필요합니다 : ".$match[0];
			exit;
		} // 문자열 점검 추가.
		if(base64_encode(base64_decode($enc_data))!=$enc_data)
		{
			echo "입력 값 확인이 필요합니다";
			exit;
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		if ($enc_data != "")
		{

			$plaindata = `$cb_encode_path DEC $sitecode $sitepasswd $enc_data`;		// 암호화된 결과 데이터의 복호화
			//echo "[plaindata] " . $plaindata . "<br>";

			$requestnumber = "";
			$authtype = "";
			$failed_msg = "";
			$params = array(
				"authtype" => $authtype,
				"failed_msg" => $failed_msg,
				"plaindata" => $plaindata
			);

			if ($plaindata == -1)
			{
				$returnMsg  = "암/복호화 시스템 오류";
				$params['failed_msg'] = $returnMsg;
				//exit;
			}
			else if ($plaindata == -4)
			{
				$returnMsg  = "복호화 처리 오류";
				$params['failed_msg'] = $returnMsg;
				//exit;
			}
			else if ($plaindata == -5)
			{
				$returnMsg  = "HASH값 불일치 - 복호화 데이터는 리턴됨";
				$params['failed_msg'] = $returnMsg;
				//exit;
			}
			else if ($plaindata == -6)
			{
				$returnMsg  = "복호화 데이터 오류";
				$params['failed_msg'] = $returnMsg;
				//exit;
			}
			else if ($plaindata == -9)
			{
				$returnMsg  = "입력값 오류";
				$params['failed_msg'] = $returnMsg;
				//exit;
			}
			else if ($plaindata == -12)
			{
				$returnMsg  = "사이트 비밀번호 오류";
				$params['failed_msg'] = $returnMsg;
				//exit;
			}
			else if($plaindata)
			{
				// 복호화가 정상적일 경우 데이터를 파싱합니다.
				$ciphertime = `$cb_encode_path CTS $sitecode $sitepasswd $enc_data`;	// 암호화된 결과 데이터 검증 (복호화한 시간획득)

				$requestnumber = $this->GetValue($plaindata , "REQ_SEQ");
				$errcode = $this->GetValue($plaindata , "ERR_CODE");
				$authtype = $this->GetValue($plaindata , "AUTH_TYPE");

				if(strcmp($_SESSION["REQ_SEQ"], $requestnumber) != 0)
				{
					//print_r("정보가 다릅니다. 올바른 경로로 접근하시기 바랍니다.");
					$params['failed_msg'] = "정보가 다릅니다. 올바른 경로로 접근하시기 바랍니다.";
					$params['plaindata'] = 0;
					//$requestnumber = "";
					//$authtype = "";
					//exit;
				}
				/*
				else
				{
					$params = array(
						"authtype" => $authtype
					);

					$this->_print_uri($params,"auth/v_self_auth_failed");
				}
				*/
			}
			$this->_print_uri($params,"auth/v_self_auth_failed");
		}
	}

	//********************************************************************************************
    //해당 함수에서 에러 발생 시 $len => (int)$len 로 수정 후 사용하시기 바랍니다. (하기소스 참고)
    //********************************************************************************************
    public function GetValue($str , $name)
    {
        $pos1 = 0;  //length의 시작 위치
        $pos2 = 0;  //:의 위치

        while( $pos1 <= strlen($str) )
        {
            $pos2 = strpos( $str , ":" , $pos1);
            $len = substr($str , $pos1 , $pos2 - $pos1);
            $key = substr($str , $pos2 + 1 , $len);
            $pos1 = $pos2 + $len + 1;
            if( $key == $name )
            {
                $pos2 = strpos( $str , ":" , $pos1);
                $len = substr($str , $pos1 , $pos2 - $pos1);
                $value = substr($str , $pos2 + 1 , $len);
                return $value;
            }
            else
            {
                // 다르면 스킵한다.
                $pos2 = strpos( $str , ":" , $pos1);
                $len = substr($str , $pos1 , $pos2 - $pos1);
                $pos1 = $pos2 + $len + 1;
            }
        }
    }
}


