<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/Common.php");

class Request extends Common{

    public function __construct() {
		parent::__construct();
	}


	public function request_get($type)
    {
        $data = array(
            'commone_header' => $this->load->view("/request/request_header", $this->_set_params($type), true),
            'commone_footer' => $this->load->view("/request/request_footer", NULL, true)
        );

        $this->_print_uri($data,'/request/'.$type);
    }



    public function request_post($type)
    {
        $postdata = $this->input->post(NULL, TRUE);
        $result = $this->_chk_captcha($postdata);
        if($result['status']===TRUE)
        {
            $postdata['type']      = $type;
            //$postdata['contents']  = $this->load->view("/request/contents_table_tmp", $postdata, TRUE);
            $postdata['mailtitle'] = $this->_set_params($type);

            $result = $this->_send_api($postdata);

//            va($result);
//            die;
        }

        $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($result));
    }


    private function _send_api($postdata)
	{
		$aryParam = Array(
            'sCompanyName'  => $postdata['companyname'],
            'sManagername'	=> $postdata['requestorname'],
            'sEmail'		=> $postdata['email'],
            'sPhone'		=> $postdata['phonenumber'],
            'sQuestion'		=> $postdata,
            'sTitle'        => $postdata['mailtitle']['mtitle'],
            'sForeignYn'	=> 'Y',
            'nServiceType'  => 14,
            'sConsultType'  => $postdata['type']
        );

        $kinx_api_host = (ENVIRONMENT=='development') ? 'http://apidev.kinx.net' : 'http://api.kinx.net';

//        va($aryParam);
//        va('$api=>'.$kinx_api_host);


		$this->load->library('rest');
		$this->rest->initialize(array('server' => $kinx_api_host));
		$this->rest->http_header('content-type','application/json;charset=UTF-8');
		$retStdClass = $this->rest->post('consult/send', json_encode($aryParam), 'json');

        if($retStdClass->success == 1)
        {
            return array('status' => TRUE, 'msg' => '');
        }
        else{
            return array('status' => FALSE, 'msg' => $retStdClass->errormsg);
        }




        //print_r($retStdClass);
		//stdClass Object ( [success] => 1 [http_code] => 200 [results] => 363 [errormsg] => Insert is success )
		//stdClass Object ( [success] => [http_code] => 500 [results] => 366 [errormsg] => Insert is fail )

	}








    /**
     * url segment를 확인하여 화면에 표시할 title, notice 정보를 설정한다.
     * @param string $type
     * @return array $params
     */
    private function _set_params($type)
    {
        switch($type)
        {
            case 'tech_spec_sheet':
                $params = array(
                    'mtitle' => 'IDC 스펙시트 신규 상담',
                    'title'  => 'Technical Specification Sheet Request Form',
                    'ctitle' => 'Colocation requirements',
                    'notice' => '<strong class="notice-title">&lt;NOTICE&gt; Network equipment rental / purchase for international customers</strong><br/>
                                When customers are in needs of deploying racks with network equipment but not enough resources to find the right equipment, KINX help to get a quotation and find the right specification of network equipment. When international customers request inbound shipment, customs clearance/import taxes/electromagnetic wave testing/ etc. can be an issue and cause an unexpected delay. If you’d like to avoid this type of issue, contact to KINX to get a quotation. It takes approximately 3 business days to get a quotation and shipping for devices can take from 3 – 15 business days.'
                );
                break;
            case 'datacenter_tour':
                $params = array(
                    'mtitle' => 'IDC 센터 투어 신청 신규 상담',
                    'title'  => 'Data Center Tour Request Form',
                    'ctitle' => 'Request Details',
                    'notice' => '<strong class="notice-title">&lt;NOTICE&gt; </strong><br/>
                                Sales AM arranges Data Center Tour and let customers know which Data Center would fit the most.<br/>
                                Data Center Tour shall be requested at least prior to 5 business days of the tour date. <br/>
                                On the tour date, visitors must have a photo ID(gov’t issued ID) to gain an access to Data Center.'
                );
                break;
            case 'cross_connect':
                $params = array(
                    'mtitle' => 'IDC 크로스 커넥트 신규 상담',
                    'title'  => 'Cross-Connect Request Form',
                    'ctitle' => 'Request Details',
                    'notice' => '<strong class="notice-title">&lt;NOTICE&gt; Cross connection lead time</strong><br/>
                                After receiving a signed Order Form from customer, it usually takes about 5 business days to complete cross connection installation. If cross connection request form is not fully filled by missing some information (i.e. demarcation point, cable/transceiver type), lead time can be longer than usual.'
                );
                break;
            default:
                throw new Exception('잘못된 접근입니다.');
        }

        return $params;
    }






    /**
     * google captcha 값을 확인 처리
     * @param array $postdata
     * @return array
     */
    private function _chk_captcha($postdata){
        if(!isset($postdata['g-recaptcha-response']) || empty($postdata['g-recaptcha-response']))
        {
            return array("status"=>FALSE, "message"=>"Please verify that you are not a robot.");
        }
        else
        {
            $remoteip  = $_SERVER['REMOTE_ADDR'];
            $secretKey = '6LfUNlIUAAAAAOrAMeoM-mcFjT2l23Zco8tsjKVj';
            $chkurl    = "https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$postdata['g-recaptcha-response']."&remoteip=".$remoteip;
            $resource  = file_get_contents($chkurl);
            $response  = json_decode($resource, true);


            if(intval($response["success"]) !== 1)
            {
                $errorMessages = array(
                    'missing-input-secret'   => 'The secret parameter is missing.',
                    'invalid-input-secret'   => 'The secret parameter is invalid or malformed.',
                    'missing-input-response' => 'The response parameter is missing.',
                    'invalid-input-response' => 'The response parameter is invalid or malformed.',
                    'invalid-keys'           => 'The response parameter is invalid.'
                );

                if( isset($response["error-codes"][0]) )
                    $msg = (isset($errorMessages[$response["error-codes"][0]]))? $errorMessages[$response["error-codes"][0]] : "The request is invalid or malformed." ;
                else
                    $msg = "Wrong connection." ;
                return array("status"=>FALSE,"message"=>$msg);
            }
        }

        return array('status' => TRUE, 'msg' => '');
    }
}
