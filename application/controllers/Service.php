<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/Common.php");
class Service extends Common{

	public function __construct()
	{
		parent::__construct();

		$this->load->model("contract_ism_model","md_contract");
		$this->load->model("product_ism_model","md_product");
		$this->load->model("account_ism_model","md_account");
		$this->load->model("customer_ism_model","md_customer");
		$this->load->model("account_member_ism_model","md_accouontmember");

		if($this->is_login === false) {
            $this->output->set_status_header('410');
			$this->tinyjs->pageRedirect("/", "로그인 후 접근가능합니다");
            exit;
		}
	}


	public function system_info() {
		$this->load->model("Mykinx_systeminfo_model");
		$this->_set_sec();
		$secParams = $this->_get_sec();
		$secParams["offset"] = isset($secParams["page"]) ? $secParams["page"] : "0";
		$secParams["limit"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];

		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		$secParams["oKey"][0] = "sEquipmentName";
		$secParams["oType"][0] = "asc";
		$secParams["oKey"][1] = "sBrand";
		$secParams["oType"][1] = "asc";
		$secParams["oKey"][2] = "sModel";
		$secParams["oType"][2] = "asc";
		$secParams["oKey"][3] = "dtCreateDate";
		$secParams["oType"][3] = "desc";

                $secParams["sStatus"] = "I";// O => 반출, I : 반입. 반출된 목록은 가져오지 않도록 하기 위해 조건 추가. 180102

		$data["cnt"] = $this->Mykinx_systeminfo_model->_select_cnt($secParams);
		$data["list"] =  $this->Mykinx_systeminfo_model->_select_list($secParams);

		$pager["CNT"] = $data["cnt"];
		$pager["PRPAGE"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$pagerHtm = $this->_set_pager($pager);

		$data["pager"] = $pagerHtm;
		$data["pagerIdx"] = $data["cnt"] - $secParams["offset"];
		$data["secParams"] = $secParams;

		$this->_print($data);
	}

	public function dns_info()
	{
		$this->load->model("Mykinx_dnsinfo_model");
		$this->_set_sec();
		$secParams = $this->_get_sec();
		$secParams["offset"] = isset($secParams["page"]) ? $secParams["page"] : "0";
		$secParams["limit"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];

		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		$secParams["nStatus"] = "Y";
		$secParams["oKey"][0] = "sDns";
		$secParams["oType"][0] = "asc";
		$secParams["oKey"][1] = "sHost";
		$secParams["oType"][1] = "asc";
		$secParams["oKey"][2] = "sIp";
		$secParams["oType"][2] = "asc";

		$data["cnt"] = $this->Mykinx_dnsinfo_model->_select_cnt($secParams);
		$data["list"] =  $this->Mykinx_dnsinfo_model->_select_list($secParams,0);

		$pager["CNT"] = $data["cnt"];
		$pager["PRPAGE"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$pagerHtm = $this->_set_pager($pager);

		$data["pager"] = $pagerHtm;
		$data["pagerIdx"] = $data["cnt"] - $secParams["offset"];
		$data["secParams"] = $secParams;

		$this->_print($data);
	}

	public function dns_request()
	{
		//$this->scripts[] = "mydns.js";
		$this->scripts[] = "my_dns.js";
		$this->load->model("Mykinx_dnsinfo_model");

		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		//1.등록자 정보 가져오기
		$secParams["oKey"][0] = "sCompanyType";
		$secParams["oType"][0] = "asc";
		$secParams["oKey"][1] = "nSort";
		$secParams["oType"][1] = "asc";
		$secParams["oKey"][2] = "sCompanyName";
		$secParams["oType"][2] = "asc";
		$secParams["oKey"][3] = "sManagerName";
		$secParams["oType"][3] = "asc";
		$data["companycontact"] = $this->Mykinx_dnsinfo_model->_select_list($secParams,1);
		//print_r($data["companycontact"]);
		//echo "<br>-----";

		//2. 보유 DNS리스트
		unset($secParams["oKey"][0]);
		unset($secParams["oType"][0]);
		unset($secParams["oKey"][1]);
		unset($secParams["oType"][1]);
		unset($secParams["oKey"][2]);
		unset($secParams["oType"][2]);
		unset($secParams["oKey"][3]);
		unset($secParams["oType"][3]);
		$secParams["nStatus"] = "Y";
		$data["companydnslist"] = $this->Mykinx_dnsinfo_model->getCompanyDNSList($secParams);
		//print_r($data["companydnslist"]);


		$this->_print($data);
	}

	public function json($mode=null)
	{
		if($mode == "registantUser") {
			$this->load->model("Mykinx_dnsinfo_model");
			$data = $this->input->post();
			$secParams["nCompanyContactSeq"] = decryptIt($data["nCompanyContactSeq"]);
			$data["registant"] = $this->Mykinx_dnsinfo_model->_select_row($secParams,1);
			$json = array("sCompanyName"=>$data["registant"]["sCompanyName"],
					"sManagerName"=>$data["registant"]["sManagerName"],
					"sRank"=>$data["registant"]["sRank"],
					"sInternalPhone"=>$data["registant"]["sInternalPhone"],
					"sfax"=>$data["registant"]["sfax"],
					"sMobilePhone"=>$data["registant"]["sMobilePhone"],
					"sEmail"=>$data["registant"]["sEmail"]);
			echo json_encode($json);
		}
	}
	public function dnsRequest()//등록하기
	{
		$data = $this->input->post();
		$data["companycontact"] = decryptIt($data["companycontact"]);//150313 decryptIt 추가
		$this->load->model("Mykinx_dnsinfo_model");
		//1-1.Service Type 구하기
		$nContractSeq = $this->member["nContractSeq"];
		$data["nContractSeq"] = $this->member["nContractSeq"];
		$data["nCompanySeq"] = $this->member["nCompanySeq"];

		//db에서 추출한 nServiceCode가 1이면 member["sServiceType"]에 1, 1보다 크면 2로 설정
		//1이면 IX, 2이면 IDC로 판별
		if( $this->member["sServiceType"]==1 )
		{
			$data["nServiceType"]= "IX";
		}else
		{
			$data["nServiceType"]= "IDC";
		}

		//2.tWorkRequest에 추가
		$data["nWorkRequestSeq"]=$this->Mykinx_dnsinfo_model->InsertWorkRequest($data);

		//3. tWorkDns 에 추가
		//$this->Mykinx_dnsinfo_model->InsertWorkDNS($data);
		for( $i=0; $i<count($data["nRequestDnsIdx"]); $i++)
		{
			unset($secParam);
			$secParam["nWorkRequestSeq"] = $data["nWorkRequestSeq"];
			if(isset($data["nRequestDnsIdx"]))$secParam["nCustomerDnsSeq"] = $data["nRequestDnsIdx"][$i];
			$secParam["nContractSeq"] = $this->member["nContractSeq"];
			$secParam["nCompanySeq"] = $this->member["nCompanySeq"];
			if(isset($data["req_dns"]))$secParam["sDns"] = $data["req_dns"][$i];
			if(isset($data["req_host"]))$secParam["sHost"] = $data["req_host"][$i];
			if(isset($data["req_ip"]))$secParam["sIp"] = $data["req_ip"][$i];
			if(isset($data["req_cmt"]))$secParam["sComment"] = $data["req_cmt"][$i];
			if(isset($data["worktype"]))$secParam["strWorkType"] = $data["worktype"][$i];
			$this->Mykinx_dnsinfo_model->InsertWorkDNS($secParam);
		}

		//4. mail 발송
		$this->sendmail_work($this->member["sContractName"],$data["nWorkRequestSeq"],"DNS");

		//5.Alert . 페이지 전환
		$this->tinyjs->alert("정상적으로 신청되었습니다. 이용해 주셔서 감사합니다.");
		$this->tinyjs->pageRedirect("/service/dns_info");
	}

	public function sendmail_work($sContactName, $nWorkRequestSeq, $worktype)
	{
		$this->load->helper('email');

		$tolist = MAIL_WORK_REQUEST;

		switch($worktype)
		{
			case "DNS":  $title = "DNS 등록 요청이 접수되었습니다."; break;
			case "모니터링IP": $title = "모니터링IP 등록 요청이 접수되었습니다."; break;
		}

		$mailtitle = "[알림]".$title;

		//메일 본문 - 작업유형
		switch($worktype)
		{
			case "DNS":  $sWorkType = " DNS 등록"; break;
			case "모니터링IP": $sWorkType = "모니터링IP 등록"; break;
		}


		//메일 본문 내용
		$sMailDocumentBody	= '';

		$sMailDocumentBody .= '
				<!-- 본문 내용 -->
			    <table border="0" cellpadding="0" cellspacing="0" width="640">
			   <tr>
			    <td><font style="font-family:맑은 고딕,돋움; font-size:13px;">
			    안녕하세요.<br />
			    인터넷 인프라 전문기업 ㈜케이아이엔엑스입니다.<br />';

		switch($worktype)
		{
			case "DNS":
				$sMailDocumentBody .= '
					DNS 등록 요청이 접수되었습니다.<br />';
				break;
			case "모니터링IP":
				$sMailDocumentBody .= '
					모니터링IP 등록요청이 접수되었습니다.<br />';
				break;
		}
		$sMailDocumentBody .= '
    				자세한 내용은  <a href="https://'.INTRANET_HOMEPAGE.'/login?code=MANAGE&url=https://intraframe.kinx.net/techSupport/list" target="_blank">인트라넷&gt;운영관리&gt;네트워크&gt;기술 요청현황</a>에서 확인할 수 있습니다.</font><br /><br />
						'; //170711 수정

		$sMailDocumentBody.='
				    <table border="2" cellpadding="6" cellspacing="0" width="640" bordercolor="#222222" bordercolordark="#222222" bordercolorlight="#222222"  style="border-collapse:collapse;">
				     <tr>
				      <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>계약명</b></font></td>
				      <td width="250"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$sContactName.'</font></td>
				      <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>접수번호</b></font></td>
				      <td width="166"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$nWorkRequestSeq.'</font></td>
				     </tr>
				     <tr>
				      <td width="110" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:12px;"><b>작업유형</b></font></td>
				      <td colspan="3"><font style="font-family:맑은 고딕,돋움; font-size:12px;">'.$sWorkType.'</font></td>
				     </tr>
				      		';

		$sMailDocumentBody	.= '</table><br/>';
		$sMailDocumentBody .= '<br><br>';


		$data['title'] = $title; //내용의 제목
		$data['message'] = $sMailDocumentBody;
		//$data['files'] = $rowfiles;
		$data['fromemail'] = EMAIL_HELP;
		$data['fromname'] = EMAIL_SENDMAIL_NAME;
		$data['to'] = $tolist;
		$data['subject'] = $mailtitle; //메일 제목
		$data['footertype'] = 1;//발신전용
		$data['inout'] = 'in';//in or out // out이면 외부, 고객 발신, 고객센터 내용 추가되어야 함.
		if(ENVIRONMENT == 'development')
			$data['bcc'] = "lizzy337@kinx.net";

		//메일 발송
		$this->load->helper('email');
		kinxMailSendFnc($data, FALSE);
	}

	public function sendmail_work_old($sContactName, $nWorkRequestSeq, $worktype)
	{
		$this->load->library('email');
		$this->load->helper('email');

		$to_name = "";
		$to_name .= $worktype." 접수담당자";
		$tolist = MAIL_WORK_REQUEST;

		$subject = $worktype." 등록 요청이 접수되었습니다.";

		$sMailDocumentBody	= "";
		$sMailDocumentBody	= mailheader();
		$sMailDocumentBody .= "<table style='width:630px;height:10% !important;vertical-align:middle;border-collapse:collapse'>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td width='20%' height='1' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td width='30%' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td width='20%' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td width='30%' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='30' width='20%' align='center' bgcolor='#f7f7f7'>계약명</td>";
		$sMailDocumentBody	.= " <td width='30%'>".$sContactName."</td>";
		$sMailDocumentBody	.= " <td align='center' width='20%' bgcolor='#f7f7f7'>접수번호</td>";
		$sMailDocumentBody	.= " <td width='30%'>".$nWorkRequestSeq."</td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='1' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='30' width='20%' align='center' bgcolor='#f7f7f7'>작업유형</td>";
		if( $worktype == "DNS")
		{
			$sMailDocumentBody	.= " <td colspan='3' width='80%'> DNS 등록</td>";
		}else{
			$sMailDocumentBody	.= " <td colspan='3' width='80%'> </td>";
		}

		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='1' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td colspan='4' align='center' width='100%'>자세한 사항은 인트라넷>운영관리에서 확인바랍니다.</td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='1' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "</table>";

		$sMailDocumentBody .= mailfooter();

		$this->email->clear(TRUE);
		if( $worktype == "DNS"){
			$this->email->from(MAIL_FROM_INFO, "DNS접수");# 발신
		}else{
			$this->email->from(MAIL_FROM_INFO, "모니터링IP접수");# 발신
		}

		//if(ENVIRONMENT== 'development') $tolist = 'yjcin13@kinx.net';

		$this->email->to($tolist);# 수신
		$this->email->subject($subject);//제목
		$this->email->message($sMailDocumentBody);//content
		//echo $sMailDocumentBody;
		$this->email->send();

		//echo $this->email->print_debugger();
	}


	public function monitoringip_info()
	{
		$this->load->model('Mykinx_mntripinfo_model');
		$this->_set_sec();
		$secParams = $this->_get_sec();
		$secParams["offset"] = isset($secParams["page"]) ? $secParams["page"] : "0";
		$secParams["limit"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];

		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		$secParams["sStatus"] = "Y";
		/*
		 * SP]reCompanyMonitoringIPList(Final인 경우 sStatus 값이 Y,P인 것을 가지고 온다.
		 * 하지만 db에는 Y/N밖에 없음....
		 * (AS-IS)monitoring-info.asp에 'Y'이면 모니터링, 아니면 일시중지로 되어 있는데 Y인것만 가져오면 이렇게 체크할 필요도 없다..
		 */

		$secParams["oKey"][0] = "sIp";
		$secParams["oType"][0] = "asc";

		$data["cnt"] = $this->Mykinx_mntripinfo_model->_select_cnt($secParams);
		$data["list"] =  $this->Mykinx_mntripinfo_model->_select_list($secParams,0);
		//print_r($data);

		$pager["CNT"] = $data["cnt"];
		$pager["PRPAGE"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$pagerHtm = $this->_set_pager($pager);



		$data["pager"] = $pagerHtm;
		$data["pagerIdx"] = $data["cnt"] - $secParams["offset"];
		$data["secParams"] = $secParams;


		$this->_print($data);
	}

	public function monitoringip_request()
	{
		$this->scripts[] = "my_mntrip.js";
		$this->load->model("Mykinx_mntripinfo_model");

		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		//1.등록자 정보 가져오기
		$secParams["oKey"][0] = "sCompanyType";
		$secParams["oType"][0] = "asc";
		$secParams["oKey"][1] = "nSort";
		$secParams["oType"][1] = "asc";
		$secParams["oKey"][2] = "sCompanyName";
		$secParams["oType"][2] = "asc";
		$secParams["oKey"][3] = "sManagerName";
		$secParams["oType"][3] = "asc";
		$data["companycontact"] = $this->Mykinx_mntripinfo_model->_select_list($secParams,1);
		//print_r($data["companycontact"]);
		//echo "<br>-----";

		//2. 모니터링IP 리스트
		unset($secParams["oKey"]);
		unset($secParams["oType"]);
		//$secParams["nStatus"] = "Y";
		$secParams["sStatus"] = "0";
		$data["companymntrIplist"] = $this->Mykinx_mntripinfo_model->getCompanyMntrIPList($secParams);
		//print_r($data["companymntrIplist"]);


		$this->_print($data);
	}

	public function mntrIpRequest()
	{
		$data = $this->input->post();

		$data["companycontact"] = decryptIt($data["companycontact"]);//150313 decryptIt 추가
		$this->load->model("Mykinx_mntripinfo_model");
		$data["nContractSeq"] = $this->member["nContractSeq"];
		$data["nCompanySeq"] = $this->member["nCompanySeq"];
		//1-1.Service Type 구하기
		//db에서 추출한 nServiceCode가 1이면 member["sServiceType"]에 1, 1보다 크면 2로 설정
		//1이면 IX, 2이면 IDC로 판별
		if( $this->member["sServiceType"]==1 )
		{
			$data["nServiceType"]= "IX";
		}else
		{
			$data["nServiceType"]= "IDC";
		}

		//2.tWorkRequest에 추가
		$data["nWorkRequestSeq"]=$this->Mykinx_mntripinfo_model->InsertWorkRequest($data);

		//3. tWorkMonitoringIP 에 추가
		for( $i=0; $i<count($data["nRequestMntrIpIdx"]); $i++)
		{
			//print_r($data);
			unset($secParam);
			$secParam["nWorkRequestSeq"] = $data["nWorkRequestSeq"];
			//if(isset($data["nRequestMntrIpIdx"]))$secParam["nCompanyMonitoringIPSeq"] = $data["nRequestMntrIpIdx"][$i];
			if(isset($data["nRequestMntrIpIdx"][$i]) && $data["nRequestMntrIpIdx"][$i] > 0)
			{
				$secParam["nCompanyMonitoringIPSeq"] = $data["nRequestMntrIpIdx"][$i];
			}
			else
			{
				$secParam["nCompanyMonitoringIPSeq"] = 0;
			}
			$secParam["nContractSeq"] = $this->member["nContractSeq"];
			$secParam["nCompanySeq"] = $this->member["nCompanySeq"];
			if(isset($data["req_ip"]))$secParam["sUseIp"] = $data["req_ip"][$i];
			if(isset($data["req_cmt"]))$secParam["sContent"] = $data["req_cmt"][$i];
			if(isset($data["worktype"]))$secParam["strWorkType"] = $data["worktype"][$i];
			$this->Mykinx_mntripinfo_model->InsertWorkMntrIp($secParam);
		}

		//4. mail 발송
		$this->sendmail_work($this->member["sContractName"],$data["nWorkRequestSeq"],"모니터링IP");

		//5.Alert . 페이지 전환
		$this->tinyjs->alert("정상적으로 신청되었습니다. 이용해 주셔서 감사합니다.");
		$this->tinyjs->pageRedirect("/service/monitoringip_info");
		exit; //두번 등록 제거
	}
	//Mykinx에서 카드정보 입력할때 클라우드 정보와 로그인시 저장한 session의 클라우드 정보를 비교
	public function check_cloudInfo()
	{
		$this->load->model("Grizzly_account_model");

		$nContractSeq = $this->session->userdata("nContractSeq");

		$result = $this->Grizzly_account_model->getCloudId($nContractSeq)->return_result('php');

		$sCloudUserId = $this->session->userdata("sCloudUserId");
		$checkflag = false;
		if( $result['results']['sCloudUserId']==$sCloudUserId)
		{
			$checkflag = true;
			$msg = "";
		}else{
			$checkflag = false;
			$msg = "연결된 계정을 찾을 수 없습니다.";
		}

		$ret= array("success"=>$checkflag,"message"=>$msg);
		echo json_encode($ret);
	}


	//////////////////////////////////////
	/*
	 * ISM 호출
	 */


	public function index()
	{
		$this->_set_sec();
		$secParams = $this->_get_sec();
		//va($secParams);

		$secParams["offset"] = isset($secParams["page"]) ? $secParams["page"] : "0";
		$secParams["limit"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$secParams["nAccountSeq"] = $this->member["nAccountSeq"];
		$secParams["sStatus"] = isset($secParams["sStatus"])? $secParams["sStatus"] : "Y";
		$secParams["main"] = 0; // 'main'이 설정되어 있으면 계약종료일(dtEnd)이 현재보다 이후인 목록을 가져온다. (0 도 가능. 설정값은 중요하지 않고 존재여부만 중요)
		$data["cnt"] = $this->md_contract->get_list($secParams, TRUE);
		$data["list"] =  $this->md_contract->get_list($secParams);


		$pager["CNT"] = $data["cnt"];
		$pager["PRPAGE"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		//va($pager);
		$pagerHtm = $this->_set_pager($pager);

		$data["pager"] = $pagerHtm;
		$data["pagerIdx"] = $data["cnt"] - $secParams["offset"];
		$data["secParams"] = $secParams;

		//va($data); die;

		$this->_print($data);
	}

	public function detail($nContractSeq=null)
	{
		if($nContractSeq)
		{
			$nContractSeq = decryptIt($nContractSeq);
			$data["basic"] = $this->md_contract->get_contract_bySeq($nContractSeq);

			//안정화기간 구하기
			$data['basic']['stabilize'] = ($data['basic']['dtCharge'] > $data['basic']['dtStart']) ? $data['basic']['dtStart'].' ~ '. date("Y-m-d", strtotime("-1 day", strtotime($data['basic']['dtCharge']))) : '';
			if($data['basic']['stabilize'] == null)
				$data['basic']['stabilize'] = '-';

			// 계약 종료일 체크
			if(!empty($data["basic"]['dtCloseDate']) && $data["basic"]['dtCloseDate'] < $data["basic"]['dtEndDate'])
			{
				$data["basic"]['dtEndDate'] = $data["basic"]['dtCloseDate'];
			}

			// 보안처리 타인의 정보를 볼수 없습니다.
			if($data["basic"]["nAccountSeq"] !== $this->member["nAccountSeq"]){
				$this->tinyjs->pageBack("정상적인 경로로 접근해주세요");
				exit;
			}
			$nAccountSeq = $data["basic"]["nAccountSeq"];

			$data["service"] = $this->md_product->get_product_bySeq($data["basic"]['nProductSeq']);

			$data["contract"] = $this->md_account->get_account_bySeq($nAccountSeq);

			$nCustomerSeq = $data["contract"]["nCustomerSeq"];
			$data["company"] = $this->md_customer->get_customer_bySeq($nCustomerSeq);
			$data["company"]['arOtherInfo'] = json_decode($data["company"]['arOtherInfo'],true);

			$data["cMember"] = $this->md_accouontmember->get_accountmem_byAccSeq($nAccountSeq, "C");//요금담당자

			$data["eMember"] = $this->md_accouontmember->get_accountmem_byAccSeq($nAccountSeq, "E");//업무담당자

			//-start 140911 카드결제 관련
			$data['lgdacom'] = Array(
					'platform' => 'service',
					'mid' => 'kinx04',
					'buyersn' => '',
					'checkssnyn' => 'N'
					);
			if($this->input->server('SERVER_PORT')==443){
				$protocol = 'https';
			}else{
				$protocol = 'http';
			}
			//$data["nav"] = $nav;
			//$this->scripts = array('cloud/join.js','cloud/utility.js',$protocol . "://xpay.lgdacom.net/xpay/js/xpay_ub_utf-8.js","service.js");
			$this->scripts = array('cloud/join.js','cloud/utility.js',$protocol . "://xpay.lgdacom.net/xpay/js/xpay_ub_utf-8.js","my_service.js");
			$jsinit = array('join.paymentinfo_add.init();');
			$data['jsinit'] = $jsinit;

			//va($data); die;
			$this->_print($data);
		} else {
			$this->tinyjs->pageBack("정상적인 경로로 접근해주세요");
		}
	}

	//e - 업무담당자, c - 요금담당자
	public function competent($mode="e")
	{
		$data["data"] = $this->md_accouontmember->get_accountmem_byAccSeq($this->member["nAccountSeq"], $mode);
		$data["mode"] = $mode;
		$this->_print($data);
	}

	public function proc($mode=null) {
		if($mode ==="competent")//업무,요금담당자 변경
		{
			$postdata = $this->input->post();
			//va($postdata);

			$row = $this->md_accouontmember->get_accountmem_byAccSeq($this->member["nAccountSeq"],$postdata['sDutyType']);
			//va('proc row'); va($row); die;

			// 있다면 업데이트
			if(isset($row["nAccountMemberSeq"]))
			{
				$where = array(
						'nAccountSeq'=>$this->member["nAccountSeq"],
						'nAccountMemberSeq'=>$row['nAccountMemberSeq'],
						'sDutyType'=>$postdata['sDutyType']
				);


				$change_data = array(
						'sName'=>$postdata['sName'],
						'sRank'=>$postdata['sRank'],
						'sPhone'=>$postdata['sInternalPhone'],
						'sDepartment'=>$postdata['sDepartment'],
						'sMobile'=>$postdata['sMobilePhone'],
						'sEmail'=>$postdata['sEmail']
				);

				$this->md_accouontmember->update_accmem_info($change_data,$where);
			}
			else // 없다면 인썰트
			{
				$this->md_accouontmember->insert_accmem_info($postdata);
			}
			redirect($this->input->server("HTTP_REFERER"));
		}
	}

}