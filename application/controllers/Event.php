<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/Common.php");

class Event extends Common{

    public function __construct()
    {
        parent::__construct();

        /*
        if($this->is_login === false)
        {
            $this->output->set_status_header('410');
            $this->tinyjs->pageRedirect("/", "로그인 후 접근가능합니다. verification");
            exit;
        }
        */

        $this->load->model("Va_event_model","md_vaevent");
    }


    public function index($args=null)
    {
        $today = date("Y-m-d");
        //$today = date("Y-m-d",strtotime('2022-8-1'));//test
        $expire = date("Y-m-d",strtotime('2022-07-31'));
        if($today > $expire)//가상계좌 이벤트는 2022-07-31 까지.
        {
            $this->tinyjs->pageRedirect("/", "이벤트가 종료되었습니다.");
            exit;
        }
        //va('event index');
        //va('args'); va($args);// /event/index/blahblah. args == blahblah


        if(empty($args))
        {
            //$check_result = 400;
            //$this->output->set_status_header('400');//410
            $this->tinyjs->alert("잘못된 접근입니다.");
            $this->tinyjs->pageRedirect("/");
            exit;
        }
        else
        {
            //cf)정보 암호화
            //$temp = urlencode(encryptIt(($nBillTypeSeq."||".$nTaxInvoiceSeq)));
            //7J7w72NhN3NXjpNQNrNLj7NG7_jbN3NV7yNpjpN_NgNjNPNMNjNb7ZNrjc7lNjNzNVNQ7uN7NDNNjANQ7I7.7rNhNXN%7E7l7BNn71NJNXNPNiN67u7SN0N9NoNP7J7.NQN_NDb7796797605384040055a6bfdb414a4f1bf3b678b3649ecd5095b94b061eb7967.7w7FNTjmNUNv7FNhNiNK

            //0. 오류처리
            /*
            //실제 발송시 /event/index7r79NXNv7UjKjMjejVNENANV7YjPN0jjNJN 이런 형식으로 나갔음
            args 의 앞에 5글자가 index 이면 문자열 잘라서 되도록 수정
            */
            $chk_index = substr($args,0,5);
            //va('chk_index ');va($chk_index);

            if($chk_index === 'index')
            {
                $len = strlen($args);
                $args = substr($args,5,$len-5);
            }
            else
            {
                //va('org arg');va($args);
            }


            //1. 정보 분리.  encoding 한 seq 그대로 유지
            $temp = explode("||",decryptIt(urldecode($args)));

            if(count($temp) != 2)
            {
                $this->tinyjs->alert("올바르지 않은 정보입니다.");
                $this->tinyjs->pageRedirect("/");
                exit;
            }
            else
            {
                $nBillTypeSeq = $temp[0];
                $nTaxInvoiceSeq = $temp[1];
                //va('nBillTypeSeq : '.$nBillTypeSeq);
                //va('nTaxInvoiceSeq : '.$nTaxInvoiceSeq);
                //die;


                //test.// LIZZY_TEST
                //$nBillTypeSeq = 3128;
                //$nTaxInvoiceSeq = 83930;


                $data['nBillTypeSeq'] = seed($nBillTypeSeq);
                $data['nTaxInvoiceSeq'] = seed($nTaxInvoiceSeq);
                $this->_print($data);
            }

        }
    }

    public function proc()
    {
        $this->load->library("form_validation");

        if($this->form_validation->run("event"))
        {
            $postdata = $this->input->post(null,TRUE);

            //LIZZY_TEST
            //$nBillTypeSeq = 3128;//넘어온 seq decode 하기
            //$nTaxInvoiceSeq = 83930;//넘어온 seq decode 하기
            $temp = $this->input->post('nBillTypeSeq');
            $nBillTypeSeq = seed($temp,FALSE);
            $temp = $this->input->post('nTaxInvoiceSeq');
            $nTaxInvoiceSeq = seed($temp,FALSE);

            //va('nBillTypeSeq : '.$nBillTypeSeq);
            //va('nTaxInvoiceSeq : '.$nTaxInvoiceSeq);


            $sCustomerName = $this->input->post('sCustomerName');
            $sCustomerEmail = $this->input->post('sCustomerEmail');
            $sPaymentName = $this->input->post('sPaymentName');
            $sPaymentPhone = $this->input->post('sPaymentPhone');


            /* 20220727 거래명세서 수신 대상 목록 확인 삭제
            - 김선영M가 이벤트가 진행될 예정이라고 고객에게 발송된 리스트가 있는데 이부분이 개발팀에게 전달되지 않았음
            - 그리고 이후에 링크가 있는 이벤트 이미지만 고객들에게 따로 보낸 것 같음
            => 메일을 받고 링크를 타고 들어오지만 거래명세서 수신 대상 목록이 아닌 사람들도 이벤트 페이지에 들어오고 있는 상황임
            => 7/27 김선영M 요청으로 거래명세서 수신 대상 목록 확인 부분 삭제

            //nBillTypeSeq, tTaxInvoiceMember table에 해당 email이 있는지 확인
            $result = $this->md_vaevent->get_taxinvoicemember($nTaxInvoiceSeq, $sCustomerEmail);
            //va('get_taxinvoicemember'); va($result); ;
            if(null == $result)//거래명세서 수신 대상 목록에 없다.
            {
                $rtn['status'] = 'F';
                $rtn['msg'] = "거래명세서 수신 대상 이메일(".$sCustomerEmail.")이 아닙니다. ";
                $this->output->set_content_type('application/json')->set_output(json_encode($rtn));
                return true;
            }
            */

            //해당 email이 event에 참여했는지 확인
            unset($result);
            $result = $this->md_vaevent->get_vaevent($sCustomerEmail);
            //va('get_vaevent'); va($result);  ;
            if(null == $result)//기존에 등록된 email이 아니다. event table 에 insert
            {
                $insertdata['sCustomerName']    = $sCustomerName;
                $insertdata['sCustomerEmail']   = $sCustomerEmail;
                $insertdata['sPaymentName']     = $sPaymentName;
                $insertdata['sPaymentPhone']    = $sPaymentPhone;
                $insertdata['nTaxInvoiceSeq']   = $nTaxInvoiceSeq;
                $insertdata['nBillTypeSeq']     = $nBillTypeSeq;
                $this->md_vaevent->insert_vaevent($insertdata);

                $rtn['status'] = 'S';
                $rtn['msg'] = "이벤트 참여에 성공하였습니다. ";
                $this->output->set_content_type('application/json')->set_output(json_encode($rtn));
            }
            else
            {
                $rtn['status'] = 'F';
                $rtn['msg'] = "이미 이벤트에 참여된 상태입니다. ";
                $this->output->set_content_type('application/json')->set_output(json_encode($rtn));
            }
        }
        else
        {
            $rtn['code'] = 'f';
            $rtn['msg'] =strip_tags(validation_errors());
            $this->output->set_content_type('application/json')->set_output(json_encode($rtn));
        }

    }

}