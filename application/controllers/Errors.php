<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/Common.php");
class Errors extends Common
{
	public function __construct() {
		parent::__construct();
        $this->load->helper('url');
        
        //redirect('/error/error404');
	}
	
	public function error404() {
		$this->load->view("errors/error404");
	}
}