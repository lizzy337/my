<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/Common.php");
class Counsel extends Common{
	public function __construct() {
		parent::__construct();
		if($this->is_login === false) {
            $this->output->set_status_header('410');
			$this->tinyjs->pageRedirect("/", "로그인 후 접근가능합니다");
            exit;
		}
	}
	public function index() {
		$this->load->model("counseling_list_model");
		$this->_set_sec();
		$secParams = $this->_get_sec();
		$secParams["offset"] = isset($secParams["page"]) ? $secParams["page"] : "0";
		$secParams["limit"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$secParams["orderby"] = "dtRegDate DESC";
		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		$secParams["nCompanySeq"] = $this->member["nCompanySeq"];
		$data["cnt"] = $this->counseling_list_model->_select_cnt($secParams);
		$data["list"] =  $this->counseling_list_model->_select_list($secParams);
		
		$pager["CNT"] = $data["cnt"];
		$pager["PRPAGE"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$pagerHtm = $this->_set_pager($pager);

		
		$data["pager"] = $pagerHtm;
		$data["pagerIdx"] = $data["cnt"] - $secParams["offset"];
		$data["secParams"] = $secParams;
		
		$this->_print($data);
		
		//$this->output->enable_profiler(TRUE);
	}
	
	public function view($seq=null) {
		$this->load->model("counseling_list_model");
		$seq = decryptIt($seq);//150313
		$where["nCounSeq"] = $seq;
		$data["basic"] = $this->counseling_list_model->_select_row($where);
		//print_r($data["basic"]);
		unset($where);
		if($data["basic"]["nContractSeq"] !== $this->member["nContractSeq"] || is_null($seq)) {
			$this->tinyjs->pageBack("정상적인 경로로 접근해주세요");
			return false;
			exit;
		}
		
		$this->_print($data);
	}
	
	//service : 1:IX, 2:IDC, 3:CDN, 4:CLOUD, 5:장애문의, 6:기타문의, 7:채용
	public function proc($nServiceType, $sServiceName, $mode=null) {
		if($mode === "write") {
			$this->load->model("counseling_list_model");
			$data = $this->input->post();
			if( isset( $data['consult_service']  )) { //140901 보안강화 ( $_REQUEST -> $data)
				$service = $data['consult_service']; //140901 보안강화 ( $_REQUEST -> $data)
				$cnt = count( $service);
				//for($i=0 ; $i<$cnt ; $i++){ echo $_REQUEST['consult_service'][$i]."<br/>";}
			} else {
				$cnt = 0;
			}
				
			//hp, email 문자열 만들기			
			$hp = $data['hp_first_select'] . "-" . $data['hp_second'] .  "-" . $data['hp_third'];
			$email = $data['email_1'] . "@" . $data['email_2'];
			$data["sPhone"] = $hp;
			$data["sEmail"] = $email;
				
			//서비스명 문자열 만들기			
			$sServiceList = $sServiceName;
			for($i=0 ; $i<$cnt ; $i++){
				if( $i == 0 ) $sServiceList .= " > ";
				$sServiceList .= $data['consult_service'][$i];//140901 보안강화 ( $_REQUEST -> $data)
				if( $i < $cnt-1 ) $sServiceList .= " / ";
			}
			$data["sServiceName"] = $sServiceList;
			$data["nServiceType"] = $nServiceType;
			$data["nContractSeq"] = $this->member["nContractSeq"];
			$servicedetail = $data["servicedetail"];
			//print_r($data);
			//insert되지 않는 내용은 unset
			unset($data["servicedetail"]);
			unset($data["hp_first_select"]);
			unset($data["hp_second"]);
			unset($data["hp_third"]);
			unset($data["email_1"]);
			unset($data["email_2"]);
			unset($data["email_select"]);
			if( isset( $_REQUEST['consult_service']  )) {
				unset($data["consult_service"]);
			}
			if( isset( $_REQUEST['consult_service_support']  )) {
				unset($data["consult_service_support"]);
			}
			unset($data["privacyassent"]);
				
			//echo "---------------------<br/>";
			//print_r($data);
				
			//db insert
			$this->counseling_list_model->_insert($data);
	
			//call finish page
			if($servicedetail == "/support/information")
			{
				$tempurl = $servicedetail . "/consultfinish";
			}
			else{
				$tempurl = "/" . $sServiceName . "/" . $servicedetail . "/consultfinish";
			}
			//echo $tempurl;
			redirect($tempurl);
		}
	}
}