<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/Common.php");
class Main extends Common{
	public function index()
	{
		if($this->is_login === false) {
			$this->load->helper('cookie');
			//저장 내용 가져오기-id
			$sid = get_cookie("saveid_mykinx");
			$data["saveid"] = $sid;

			//cloud login 여부 체크
			$data['cloud_user'] = null;
			$cloudid = get_cookie("cloud_user");
			if( $cloudid != null ) //클라우드 로그인
			{
				$data["cloud_user"] = $cloudid;
			}

			//cloud id 저장 체크 여부
			$data['saved_cloud_id'] = null;
			$saved_cloud_id = get_cookie('saved_cloud_id');
			if($saved_cloud_id){
				$data['saved_cloud_id'] = $saved_cloud_id;
			}

			$this->_print($data);
		}
		else{
			redirect("/service/index");
		}

	}
}
