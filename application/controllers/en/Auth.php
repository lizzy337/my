<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/en/Common.php");
class Auth extends Common{

	public function __construct() {
		parent::__construct();

		#intranet model중 ism으로 교체된 파일들은 en/intra_notuse/ 하부로 이동
		#Contract_model.php Contract_service_model.php


		$this->load->model("account_ism_model","md_account");
		$this->load->model("contract_ism_model","md_contract");

	}


	// 로그인
	public function sign_in()
	{
		$returnType = "http";
		$result = $this->_proc_sign_in();
		return $this->_return_result($result, $returnType);
	}

	public function sign_in_cloud()
	{
		$this->load->library("form_validation");

		$postdata = $this->input->post(NULL, TRUE);
		//print_r($postdata);
		if($this->form_validation->run("sign_in_clouden"))
		{
                    ;	//$this->_saveid_cloud();//id 저장 관련 안함

		}
    }


    private function _saveid_cloud()
	{
		$this->load->helper('cookie');
		//아이디 저장- check : 저장, uncheck : 쿠키삭제
		$postdata = $this->input->post(NULL, TRUE);
		//print_r($postdata);
		//print_r($postdata['saved_cloud_id']);
		$setFlag = false;
		$setFlag = ( isset($postdata['saved_cloud_id']) ) ? true : false;
		if( $setFlag )
		{
			$extime = time() + ( 86400 * 30 );//한달

			//저장확인 test
			//$ckid = get_cookie("saveid_mykinx_cloud");
			//print_r($ckid);

			$ret = set_cookie("saveid_mykinx_cloud", $postdata['userid'], $extime);
		}
		else
		{
			delete_cookie("saveid_mykinx_cloud");  //해당 id cookie 지우기
		}
	}


	public function sign_out()
	{
		$this->session->unset_userdata($this->cfg["session_key"]);
		$this->tinyjs->pageRedirect("/en");
		exit;
	}

	public function sign_out_cloud() //클라우드 로그아웃 (클라우드 id로 로그인된 상태에서 '로그아웃'링크 눌렀을때)
	{
		$this->load->helper('cookie');
		delete_cookie('cloud_user');  //해당 id cookie 지우기
		$this->tinyjs->pageRedirect("/en");
		exit;
	}

	public function index()
	{
		if($this->is_login)
		{
			redirect("/en/service/index");

		}
		else
		{
			$data = null;
			$this->_print($data);
		}
	}


	private function _proc_sign_in($login=FALSE)
        {
            $result = array();
            $this->load->library("form_validation");
            if($this->input->post('account')==0) //mykinx
            {
                if($this->form_validation->run("sign_in_myen"))
                {
                    $this->load->helper("encryption");

                    $sMemberId = $this->input->post("sMemberId");
                    $contract = $this->md_account->get_account_byId($sMemberId);

                    $nAccountSeq = $contract['nAccountSeq'];
                    $serviceone = $this->md_contract->get_ServiceCode($nAccountSeq);

                    $companyStatus = $this->md_account->get_AccountStatus($nAccountSeq);

                    if(sizeof($companyStatus))
                    {
                        $sStatus = $companyStatus[0]['sStatus'];

                        //회사 상태에 따른 로그인 체크
                        $arrT = array("Y","E");//정상, E=>해지이지만 mykinx는 볼수 있도록 (ex:메가파일)
                        $arrF = array("S","P","C","X","W","R");//해지,직권정지,명의변경,삭제,일시정지, 직권해지
                        if( in_array($sStatus, $arrT))
                        {
                            $result["result"] = TRUE;

                            /* 20210507 김중환 M요청으로 해제
                            #=========================================================================
                            # 뉴링크 임시 설정 고객 요청으로 로그인 제안 처리 20200624
                            #=========================================================================
                            if($sMemberId == 'newlinkcorp')
                            {
                                $result["result"] = FALSE;
                                $result["url"] = $this->input->server("HTTP_REFERER");
                                $result["msg"] = "The use was temporarily suspended due to customer requests. Please contact the administrator.";

                                return $result;
                            }
                            #=========================================================================
                            # 뉴링크 임시 설정 고객 요청으로 로그인 제안 처리 20200624
                            #=========================================================================
                            */
                        }
                        else if (in_array($sStatus, $arrF)){
                            $result["result"] = FALSE;
                            $result["url"] = $this->input->server("HTTP_REFERER");
                            $result["msg"] = "The ID is invalid. Please ask the administrator.";

                            return $result;
                        }
                    }//end sizeof

                    if(isset($contract["sPassword"]))
                    {
                        if(password_verify($this->input->post("sPassword"), $contract["sPassword"]))
                        {
                            //$sessionAry[$this->cfg["session_key"]] = $contract["sMemberId"];
                            //$sessionAry['nContractSeq'] = $contract['nContractSeq'];
                            //$sessionAry['nCompanySeq'] = $contract['nCompanySeq'];
                            //$sessionAry['sServiceType'] = $serviceone['serviceType'];

                            $sessionAry[$this->cfg["session_key"]] = $contract["sAccountID"];
                            $sessionAry['nContractSeq'] = $contract['nAccountSeq'];
                            $sessionAry['nCompanySeq'] = $contract['nCustomerSeq'];
                            $sessionAry['sServiceType'] = $serviceone['serviceType']; //1 - IX, 1 보다 크면 2 - IDC

                            $this->session->set_userdata($sessionAry);

                            $result["result"] = TRUE;
                            $result["url"] = $this->input->server("HTTP_REFERER")."/Auth/index";
                            $result["msg"] = NULL;
                        }
                        else
                                    {
                            $result["result"] = FALSE;
                            $result["url"] = $this->input->server("HTTP_REFERER");
                            $result["msg"] = "The password is incorrect.";
                        }
                    }//end isset contract sPassword
                    else
                            {
                        $result["result"] = FALSE;
                        $result["url"] = $this->input->server("HTTP_REFERER");
                        $result["msg"] = "The password is incorrect.";
                    }
                }//end for validation run sign_in_myen
                else
                {
                    $result["result"] = FALSE;
                    $result["url"] = $this->input->server("HTTP_REFERER");
                    $result["msg"] = str_replace("\n","",strip_tags(validation_errors()));
                }
            }

            //자동로그인 처리
            if(is_array($login))
            {
                    $this->load->model("account_ism_model","md_account");
                    $this->load->model("contract_ism_model","md_contract");
                    $this->load->model("autologin_model");

                    $sMemberId = $login[0];
                    $contract = $this->md_account->get_account_byId($sMemberId);

                    $nAccountSeq = $contract['nAccountSeq'];
                    $serviceone = $this->md_contract->get_ServiceCode($nAccountSeq);

                    //자동로그인 이력 생성
                    //$this->autologin_model->_insert(array("sMemberId"=>$login[0],"sCustomerId"=>$login[1],"sKeyDate"=>$login[2]));
                    //17720, customerid,와 memberid가 바뀌게 들어가고 있는것 수정
                    //새로운 intranet open으로 인해 memberid안에 id가 아닌 사번이 들어가게 됨.
                    $this->autologin_model->_insert(array("sCustomerId"=>$login[0],"sMemberId"=>$login[1],"sKeyDate"=>$login[2]));

                    $companyStatus = $this->md_account->get_AccountStatus($nAccountSeq);
                    $sStatus = $companyStatus[0]['sStatus'];

                    //회사 상태에 따른 로그인 체크
                    $arrT = array("Y","E");//정상, E=>해지이지만 mykinx는 볼수 있도록 (ex:메가파일)
                    $arrF = array("S","P","C","X","W","R");//해지,직권정지,명의변경,삭제,일시정지, 직권해지
                    if( in_array($sStatus, $arrT))
                    {
                        $result["result"] = TRUE;
                    }
                    else if (in_array($sStatus, $arrF))
                    {
                            $result["result"] = FALSE;
                            $result["url"] = $this->input->server("HTTP_REFERER");
                            $result["msg"] = "해당 아이디는 사용하실 수 없는 아이디입니다. 관리자에게 문의하세요.";

                            return $result;
                    }

                    $sessionAry[$this->cfg["session_key"]] = $contract["sAccountID"];
                    $sessionAry['nContractSeq'] = $contract['nAccountSeq'];
                    $sessionAry['nCompanySeq'] = $contract['nCustomerSeq'];
                    $sessionAry['sCloudEssexId'] = null;//$contract['sCloudEssexId'];
                    $sessionAry['sServiceType'] = $serviceone['serviceType'];//1 - IX, 1 보다 크면 2 - IDC

                    $this->session->set_userdata($sessionAry);

                    $result["result"] = TRUE;
                    $result["url"] = '/en';
                    $result["msg"] = NULL;
            }
            return $result;
	}

        public function atlogin_cloudhub() //cloudhub 타이틀 MYKINX 눌렀을때. 기존 인트라넷과 달리 외부에서 접근하므로  허용 ip체크 풀자
	{
            // Get 방식으로 4번째 세그먼트를 받는다. (영문은 /en 이 앞에 들어가서 4번째 segment)
            $sg4 =   $this->uri->segment ( 4 );

            // decoding
            $atlogin = explode("||",decryptIt(urldecode($sg4)));

            // 유효성 확인
            if(!is_array($atlogin) || count($atlogin)<>3 || $atlogin[2] < date("YmdH", strtotime("-1 hour")))//1시간 이네의 생성된 값만 처리
            {
                    // 에러메세지 출력.
                    jsError("잘못된 접근입니다.", "/en");
            }
            else
            {
                    $returnType = "http";
                    $result = $this->_proc_sign_in($atlogin);

                    return $this->_return_result($result, $returnType);
            }
	}

	private function _return_result($result, $returnType) {
		if($returnType === "http") {
			if($result["result"]) {
				$this->tinyjs->pageRedirect($result["url"]);
			} else {
				$this->tinyjs->pageBack($result["msg"]);
			}
		} else if ($returnType === "json") {
			echo json_encode($result);
		}
		return true;
	}

}
