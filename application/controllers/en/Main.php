<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/en/Common.php");
class Main extends Common{
	public function index() {
		// 로그인을 하였다면.
		
		if($this->is_login)
		{ 
			redirect("/en/service/index");
		}
		else {
			$data = null;
			$this->_print($data);
		} 
	}
}