<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/en/Common.php");
class Kinxidc extends Common{
	public function __construct() {
		parent::__construct();
		if($this->is_login === false) {
            $this->output->set_status_header('410');
			$this->tinyjs->pageRedirect("/en","Sign in to access your account");
            exit;
		}
	}

	public function index() {
		redirect("/en/kinxidc/competent");
	}

	// 담당자 관리 : 리스트
	public function competent() {

		$this->load->model("company_contact_model");//en제거. model하단 같이 사용
		$this->_set_sec();
		$secParams = $this->_get_sec();
		$secParams["offset"] = isset($secParams["page"]) ? $secParams["page"] : "0";
		$secParams["limit"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		$secParams["orderby"] = "dtcreateDate DESC";

		/*
		//start 업체분류(입주업체-M , 협력업체-C)
		//영문은 'en' 이 추가되었으므로 segment(4)로 해야함
		$str4 = $this->uri->segment(4);
		if($str4 == 'M' || $str4 == 'C')
		{
			$secParams['sCompanyType'] = $str4;
		}

		if($str4 == '')
		{
			$data["sCompanyType"] = "A";
		}

		$data["sCompanyType"] = $str4;
		//-end
				*/
		//////////////////////////
		//start 업체분류(입주업체-M , 협력업체-C). modify by cyn 20200313
		if(isset($secParams["type"]))
		{
			if($secParams["type"]=='M' || $secParams["type"]=='C')
			{
				$secParams['sCompanyType'] = $secParams["type"];
				$data["sCompanyType"] = $secParams["type"];
			}
			else
			{
				$data["sCompanyType"] = 'A';
			}
		}

		else
		{
			$data['sCompanyType'] = '';
		}
		///////////////////////////////

		$data["cnt"] = $this->company_contact_model->_select_cnt($secParams);
		$data["list"] =  $this->company_contact_model->_select_list($secParams);

		$pager["CNT"] = $data["cnt"];
		$pager["PRPAGE"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$pagerHtm = $this->_set_pager($pager);

		$data["pager"] = $pagerHtm;
		$data["pagerIdx"] = $data["cnt"] - $secParams["offset"];
		$data["secParams"] = $secParams;

                $this->scripts[] = "en/my_kinxidc.js";
		$this->_print($data);
	}

	// 담당자 관리 : 상세 정보, 추가하기, 수정하기 폼
	public function competent_write($seq=null) {
		// null 일 경우 추가하기입니다.
		if(is_null($seq)) {
			$mode = "write";
			$data["basic"]["sCompanyName"] = $this->member["sContractName"];
		}
		// 아니면 수정하기임
		else {
			$seq = decryptIt($seq);//150313
			$this->load->model("company_contact_model");//en제거. model하단 같이 사용
			$where["nCompanyContactSeq"] = $seq;
			$data["basic"] = $this->company_contact_model->_select_row($where);
			unset($where);
			if($data["basic"]["nContractSeq"] !== $this->member["nContractSeq"]) {
				$this->tinyjs->pageBack("Please access via normal path.");
				return false;
				exit;
			}
			$mode = "modify";
		}

		//print_r($this->member);

		$data["mode"] = $mode;

		$this->scripts[] = "en/my_kinxidc.js";

		$this->_print($data);
	}

	// 담당자 관련 처리단
	public function competent_proc($mode=null) {
		$result= array();
		$result['url']="";

		// 담당자 추가 또는 수정
		if($mode === "write" || $mode === "modify")
		{
			$this->load->library("form_validation");
			if($this->form_validation->run("contacten"))
			{
				$this->load->model("company_contact_model");//en제거. model하단 같이 사용
				$data = $this->input->post(NULL, TRUE);

				$data['sCompanyName']    = chg_xss_quot($data['sCompanyName']);
				$data['sManagerName']    = chg_xss_quot($data['sManagerName']);
				$data['sDepartmentName'] = chg_xss_quot($data['sDepartmentName']);
				$data['sRank']           = chg_xss_quot($data['sRank']);

				$data['sMobilePhone'] = format_phone($data['sMobilePhone']);
				$data['sInternalPhone'] = format_phone($data['sInternalPhone']);
				$data['nCompanySeq'] = $this->member["nCompanySeq"];

				if($mode === "write") {
					$data["nContractSeq"] = $this->member["nContractSeq"];
					$this->company_contact_model->_insert($data);
					$result['code'] = 's';
					$result['msg'] = "Successfully Added.";
				} else if ($mode === "modify") {
					if($data["nContractSeq"] !== $this->member["nContractSeq"]) {
						$result['code'] = 'f';
						$result['msg'] ="Please access via normal path.";
					}
					$where["nCompanyContactSeq"] = $data["nCompanyContactSeq"];
					unset($data["nCompanyContactSeq"]);
					$this->company_contact_model->_update($data, $where);
					$result['code'] = 's';
					$result['msg'] = "Successfully Revised.";
				}

				$result['url'] = "/en/kinxidc/competent";
			}
			else
			{
				$result['code'] = 'f';
				$result['msg'] =strip_tags(validation_errors());
			}
		}
		// 담당자 삭제
		else if ($mode === "delete") {
			$seq = $this->uri->rsegment(4);
			if($seq) {
				$seq = decryptIt($seq);//150316
				$this->load->model("company_contact_model");//en제거. model하단 같이 사용
				$where["nCompanyContactSeq"] = $seq;
				$data["basic"] = $this->company_contact_model->_select_row($where);

				if($data["basic"]["nContractSeq"] !== $this->member["nContractSeq"]) {
					$result['code'] = 'f';
					$result['msg'] ="Please access via normal path.";
				}

				$where["nCompanyContactSeq"] = $data["basic"]["nCompanyContactSeq"];
				$this->company_contact_model->_delete($where);
				$result['code'] = 's';
				$result['msg'] = "Successfully Deleted";
				$result['url'] = "/en/kinxidc/competent";
			}
			else
			{
				$result['code'] = 'f';
				$result['msg'] ="Please access via normal path.";
			}
		}

		echo(json_encode($result));
		exit;
	}

	// 방문신청 비지트 어플리케이션 : 리스트
	public function visitapp() {
		$this->load->model("work_request_model");//en삭제
		$this->load->model("company_visitor_model");//en제거. model하단 같이 사용
		$this->load->model("company_contact_model");//en제거. model하단 같이 사용
		$this->_set_sec();
		$secParams = $this->_get_sec();

		$secParams["offset"] = isset($secParams["page"]) ? $secParams["page"] : "0";
		$secParams["limit"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$secParams["orderby"] = "dtcreateDate DESC";
		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		//$secParams["nServiceType"] = $this->member["sServiceType"];
		$secParams["sActionType"] = "VST"; //고객방문
		$secParams['oType'] = "desc";
		$secParams['oKey'] = "dtCreateDate";

		$data["cnt"] = $this->work_request_model->_select_cnt($secParams);
		$data["list"] =  $this->work_request_model->_select_list($secParams);

		$pager["CNT"] = $data["cnt"];
		$pager["PRPAGE"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$pagerHtm = $this->_set_pager($pager);

		$data["pager"] = $pagerHtm;
		//$data["pagerIdx"] = $data["cnt"] - ($secParams["offset"]*$pager["PRPAGE"]);
        $data["pagerIdx"] = $data["cnt"] - ($pager["PRPAGE"]);
		$data["secParams"] = $secParams;
		//$data["competentList"] =  $this->company_contact_model->_select_list($secParams);
		//competentList 가져오는 부분 수정
		$tempParam = $secParams;
		unset($tempParam["limit"]);
		unset($tempParam["offset"]);
		unset($tempParam["oKey"]);
		unset($tempParam["oType"]);
		$data["competentList"] =  $this->company_contact_model->_select_list($tempParam);

		$data["chkmsg"] = (empty($data["competentList"])) ? "No person in charge is registered. Please apply for visit after registeration." : '';

        /*
		for($i=0; $i < $data["pagerIdx"] && $i < $pager["PRPAGE"]; $i++)
		{
			unset($secParams);
            $data['nCntVisitor'] = 0;
            $data['topVisitor'] = '-';

            $secParams["nWorkRequestSeq"] = $data["list"][$i]["nWorkRequestSeq"];
            echo "<br>secParams : ";
            print_r($secParams);
			$data["nCntVisitor"] = $this->company_visitor_model->_select_cnt($secParams);
			$data["topVisitor"] = $this->company_visitor_model->_select_row($secParams);

			$data["list"][$i]["nCntVisitor"] = $data["nCntVisitor"];
			$data["list"][$i]["sTopVisitorName"] = $data["topVisitor"]["sVisitorName"];
		}

        */
		$loopcnt = ( $pager["PRPAGE"] < ($data["cnt"]-$secParams["offset"]) )? $pager["PRPAGE"] : $data["cnt"]-$secParams["offset"];
		//echo "<br>loopcnt : ".$loopcnt;
        for($i=0; $i < $loopcnt; $i++)
        {
        	unset($secParams);
        	$data['nCntVisitor'] = 0;
        	$data['topVisitor'] = '-';

        	$secParams["nWorkRequestSeq"] = $data["list"][$i]["nWorkRequestSeq"];
        	$data["nCntVisitor"] = $this->company_visitor_model->_select_cnt($secParams);
        	$data["topVisitor"] = $this->company_visitor_model->_select_row($secParams);

        	$data["list"][$i]["nCntVisitor"] = $data["nCntVisitor"];
        	$data["list"][$i]["sTopVisitorName"] = $data["topVisitor"]["sVisitorName"];
        }


		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		$data["competentList"] =  $this->company_contact_model->_select_list($secParams);

		//print_r($data);

		$this->_print($data);
	}

	public function json($mode=null) {
		if($mode == "registant") {
			$this->load->model("company_contact_model");//en제거. model하단 같이 사용
			$data = $this->input->post();
			$data["nCompanyContactSeq"] = decryptIt($data["nCompanyContactSeq"]);//150313
			$secParams["nCompanyContactSeq"] = $data["nCompanyContactSeq"];
			//$secParams["nCompanySeq"] = $data["nCompanyContactSeq"]; //2014-05-11 nykim modify
			$data["registant"] = $this->company_contact_model->_select_row($secParams);

			$json = array("sCompanyName"=>$data["registant"]["sCompanyName"],
				"sManagerName"=>$data["registant"]["sManagerName"],
				"sInternalPhone"=>$data["registant"]["sInternalPhone"],
				"sMobilePhone"=>$data["registant"]["sMobilePhone"],
				"sRank"=>$data["registant"]["sRank"],
				"sfax"=>$data["registant"]["sfax"],
				"sEmail"=>$data["registant"]["sEmail"]);
			echo json_encode($json);
		}
	}

	public function jsonbyInit($mode=null) {//150313  decrypt 없는 것
		if($mode == "registant") {
			$this->load->model("company_contact_model");//en제거. model하단 같이 사용
			$data = $this->input->post();

			$secParams["nCompanyContactSeq"] = $data["nCompanyContactSeq"];
			//$secParams["nCompanySeq"] = $data["nCompanyContactSeq"]; //2014-05-11 nykim modify
			$data["registant"] = $this->company_contact_model->_select_row($secParams);

			$json = array("sCompanyName"=>$data["registant"]["sCompanyName"],
					"sManagerName"=>$data["registant"]["sManagerName"],
					"sInternalPhone"=>$data["registant"]["sInternalPhone"],
					"sMobilePhone"=>$data["registant"]["sMobilePhone"],
					"sRank"=>$data["registant"]["sRank"],
					"sfax"=>$data["registant"]["sfax"],
					"sEmail"=>$data["registant"]["sEmail"]);
			echo json_encode($json);
		}
	}

	// 방문신청 작성, 변경
	public function visitapp_write($seq=null) {
		$this->scripts[] = "en/my_visitapp.js";
		$this->load->model("company_contact_model");//en제거. model하단 같이 사용
		$this->load->model("code_model");//en 제거
		$this->_set_sec();
		$secParams = $this->_get_sec();
		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		$data["competentList"] =  $this->company_contact_model->_select_list($secParams);
		//$data["workTypeList"] = $this->code_model->get_code_list_array('T0001', FALSE, null);
		$data["workTypeList"] = $this->code_model->get_code_list_array('T0011', FALSE, null);//방문목적 영문으로.
		$data["idcList"] = $this->code_model->get_code_list_array('T0014', FALSE, null);//방문위치 영문

		//if(empty($data["competentList"])) jsError("현재 등록된 담당자가 없습니다.\\n담당자관리 페이지에서 담당자 등록 후 방문신청을 하실 수 있습니다.");
		if(empty($data["competentList"]))
		{
			$this->tinyjs->alert("No person in charge is registered.\\nPlease apply for visit after registeration.");
			$this->tinyjs->pageRedirect("/en/kinxidc/visitapp");
			return ;
		}

		// null 일 경우 추가하기입니다.
		if(is_null($seq)) {
			$mode = "write";
			$data["basic"]["dtExpectStartDate"] = date('Y-m-d');
			$data["basic"]["dtExpectEndDate"] = date('Y-m-d');
		} else {// 아니면 수정하기임
			$seq = decryptIt($seq);//150313
			$this->load->model("work_request_model");//en삭제
			$where["nWorkRequestSeq"] = $seq;
			$data["basic"] = $this->work_request_model->_select_row($where);
			//echo $this->db->last_query();
			unset($where);

			if($data["basic"]["nContractSeq"] !== $this->member["nContractSeq"]) {
				$this->tinyjs->pageBack("Please access via normal path.");
				return false;
				exit;
			}
			$data["basic"]["dtExpectStartTime"] = date('H', strtotime($data["basic"]["dtExpectStartDate"]));
			$data["basic"]["dtExpectEndTime"] = date('H', strtotime($data["basic"]["dtExpectEndDate"]));
			$data["basic"]["dtExpectStartDate"] = date('Y-m-d', strtotime($data["basic"]["dtExpectStartDate"]));
			$data["basic"]["dtExpectEndDate"] = date('Y-m-d', strtotime($data["basic"]["dtExpectEndDate"]));

			//210611 취약점 조치사항
			$data["basic"]["nManagerSeq"] = encryptIt($data["basic"]["nManagerSeq"]);
			//
			//var_dump($data);

			// 방문자들..
			$this->load->model("company_visitor_model");//en제거. model하단 같이 사용
			$where["nWorkRequestSeq"] = $seq;
			$data["visitorList"] = $this->company_visitor_model->_select_list($where);

			for($i=0; $i < count($data["competentList"]); $i++) {
				foreach($data["visitorList"] as $visitor) {
					if($visitor["nCompanyContactSeq"] == $data["competentList"][$i]["nCompanyContactSeq"]) {
						$data["competentList"][$i]["checked"] = "Y";
						break;
					}
				}
			}
			$data["topVisitor"] = $this->company_visitor_model->_select_row($where);

			unset($where);
			$mode = "modify";
		}

		$data["mode"] = $mode;

		//debug_var($data);
		//echo "kinxidc visitapp_write ";print_r($data['basic']);die();
		//var_dump($data); ;
		$this->scripts[] = "en/my_kinxidc.js";
		$this->_print($data);
	}

	// 방문신청서 처리
	public function visitapp_proc($mode=null) {
		$result= array();
		$result['url']="";

		// 방문 신청 또는 수정
		if($mode === "write" || $mode === "modify") {

			$this->load->library("form_validation");
			$validResult = false;
			if($mode === "write")
			{
				$validResult = $this->form_validation->run("visitappen");
			}else if($mode === "modify")
			{
				$validResult = $this->form_validation->run("visitappen_modify");
			}
			//var_dump($validResult);

			//if($this->form_validation->run("visitapp"))
			if($validResult) //등록 / 수정 일때 validation 조건 나눔. 140903 lizzy
			{
				$this->load->model("company_visitor_model");//en제거. model하단 같이 사용
				$this->load->model("work_request_model");//en삭제
				$data = $this->input->post();

				if($mode === "write")
				{
					$data["nManagerSeq"] = decryptIt($data["nManagerSeq"]);//150313
				}
				else if($mode === "modify")
				{
					//$data["nManagerSeq"] = $data["nManagerSeq"];//161104
					$data["nManagerSeq"] = decryptIt($data["nManagerSeq"]);//210611
				}


				$this->load->model("company_contact_model");//en제거. model하단 같이 사용
				$secParam["nCompanyContactSeq"] = $data["nManagerSeq"];
				$resitant =  $this->company_contact_model->_select_row($secParam); // 등록자 정보를 가져온다.
				//echo $this->db->last_query();
				//echo "data : "; var_dump($data);
				//echo "resitant : "; var_dump($resitant);
				unset($secParam);


				$data["dtExpectStartDate"] .= " ";
				$data["dtExpectStartDate"] .= $data["dtExpectStartTime"];
				$data["dtExpectStartDate"] .= ":00:00";
				$data["dtExpectEndDate"] .= " ";
				$data["dtExpectEndDate"] .= $data["dtExpectEndTime"];
				$data["dtExpectEndDate"] .= ":00:00";


				if($data["dtExpectStartDate"] > $data["dtExpectEndDate"])
				{
					$result['code'] = 'f';
					$result['msg'] ="방문예정일시와 예상종료일시를 확인해 주세요.";
				}
				else
				{
					$secParam = $data;


					//echo "secParam : "; var_dump($secParam);

					// write, update 공통
					$secParam["sCompanyName"] = $resitant["sCompanyName"];
					$secParam["sManagerName"] = $resitant["sManagerName"];
					$secParam["sDepartmentName"] = $resitant["sDepartmentName"];
					$secParam["sInternalPhone"] = $resitant["sInternalPhone"];
					$secParam["sMobilePhone"] = $resitant["sMobilePhone"];
					$secParam["sRank"] = $resitant["sRank"];
					$secParam["sfax"] = $resitant["sfax"];
					$secParam["sEmail"] = $resitant["sEmail"];
					unset($secParam["nCompanyContactSeq"]);
					//unset($secParam["nWorkType"]);
					unset($secParam["dtExpectStartTime"]);
					unset($secParam["dtExpectEndTime"]);

					if($mode === "write") {
						$secParam["nContractSeq"] = $this->member["nContractSeq"];
						$secParam["nCompanySeq"] = $this->member["nCompanySeq"];
						$secParam["nServiceType"] = $this->member["sServiceType"];
						$secParam["sActionType"] = "VST";
						$secParam["sResultStatus"] = "Y";

						$data["nWorkRequestSeq"] = $this->work_request_model->_insert($secParam);

						//3. email 발송
						$this->sendmail_work($this->member["sContractName"],$data["nWorkRequestSeq"],$data["nWorkType"],"VST",
											$data["dtExpectStartDate"], $data["dtExpectStartTime"]);
						//방문자 관련 추가 하는 작업
						$msg = "Successfully Added.";
						$result['code'] = 's';
						$result['msg'] = $msg;
						$result['url'] = "/en/kinxidc/visitapp";
					} else if ($mode === "modify") {
						if($data["nContractSeq"] !== $this->member["nContractSeq"]) {
							$result['code'] = 'f';
							$result['msg'] ="Please access via normal path.";
						}
						$where["nWorkRequestSeq"] = $data["nWorkRequestSeq"];
						//echo "secParam : "; var_dump($secParam);
						//echo "where : "; var_dump($where); die;
						$this->work_request_model->_update($secParam, $where);

						$msg = "Successfully Revised.";
						$result['code'] = 's';
						$result['msg'] = $msg;
						$result['url'] = "/en/kinxidc/visitapp";

						//방문자 관련 추가 하는 작업
						$this->company_visitor_model->_delete($where);
					}


					foreach($data["nCompanyContactSeq"] as $nCompanyContactSeq) {
						$sContent = $secParam['sContent'];
						unset($secParam);
						$secParam["nCompanyContactSeq"] = $nCompanyContactSeq;
						$contact =  $this->company_contact_model->_select_row($secParam);
						$secParam["nWorkRequestSeq"] = $data["nWorkRequestSeq"];
						$secParam["nContractSeq"] = $contact["nContractSeq"];
						$secParam["nCompanySeq"] = $contact["nCompanySeq"];
						$secParam["sCompanyName"] = $contact["sCompanyName"];
						$secParam["sVisitorName"] = $contact["sManagerName"];
						$secParam["sInternalPhone"] = $contact["sInternalPhone"];
						$secParam["sMobilePhone"] = $contact["sMobilePhone"];
						$secParam["nWorkType"] = $data["nWorkType"];
						$secParam["sContent"] = $sContent;
						$this->company_visitor_model->_insert($secParam);
					}
				}

			}
			else
			{
				$result['code'] = 'f';
				$result['msg'] =strip_tags(validation_errors());
			}
		}

		// 방문 취소
		else if ($mode === "cancel") {
			$seq = $this->uri->rsegment(4);
			if($seq) {
				$seq = decryptIt($seq);//150316
				$this->load->model("work_request_model");//en삭제
				$where["nWorkRequestSeq"] = $seq;
				$data["basic"] = $this->work_request_model->_select_row($where);


				if($data["basic"]["nContractSeq"] !== $this->member["nContractSeq"]) {
					$result['code'] = 'f';
					$result['msg'] ="Please access via normal path.";
				}

				$where["nWorkRequestSeq"] = $data["basic"]["nWorkRequestSeq"];
				$secParam["sResultStatus"] = "C";
				$this->work_request_model->_update($secParam, $where);
				$msg = "취소가 완료되었습니다.";
				$result['code'] = 's';
				$result['msg'] = $msg;
				$result['url'] = "/en/kinxidc/visitapp";

			} else {
				$result['code'] = 'f';
				$result['msg'] ="Please access via normal path.";
			}
		}

		//debug_var($data);
 		//debug_var($result);



		echo(json_encode($result));
		exit;
	}

	// 장비 입반출 exported the equipment
	public function equipment() {
		$this->load->model("work_request_model");//en삭제
		$this->load->model("company_contact_model");//en제거. model하단 같이 사용
		$this->_set_sec();
		$secParams = $this->_get_sec();

		$secParams["offset"] = isset($secParams["page"]) ? $secParams["page"] : "0";
		$secParams["limit"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$secParams["orderby"] = "dtcreateDate DESC";
		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		$secParams["nServiceType"] = $this->member["sServiceType"];
		$secParams["sActionType"] = "EIO"; //장비입반출

		$data["cnt"] = $this->work_request_model->_select_cnt($secParams);
		$data["list"] =  $this->work_request_model->_select_list($secParams);
		$pager["CNT"] = $data["cnt"];
		$pager["PRPAGE"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$pagerHtm = $this->_set_pager($pager);

		$data["pager"] = $pagerHtm;
		$data["pagerIdx"] = $data["cnt"] - $secParams["offset"];
		$data["secParams"] = $secParams;

		$data["secParams"] = $secParams;
		//$data["competentList"] =  $this->company_contact_model->_select_list($secParams);
		//competentList 가져오는 부분 수정
		$tempParam = $secParams;
		unset($tempParam["limit"]);
		unset($tempParam["offset"]);
		unset($tempParam["oKey"]);
		unset($tempParam["oType"]);
		$data["competentList"] =  $this->company_contact_model->_select_list($tempParam);

		$data["chkmsg"] = (empty($data["competentList"])) ? "No person in charge is registered. Please apply for visit after registeration." : '';


		unset($secParams);
		$this->load->model("work_equipment_model");//en삭제
		for($i=0; $i < count($data["list"]); $i++) {
			$secParams["nWorkRequestSeq"] = $data["list"][$i]["nWorkRequestSeq"];
			$secParams["sWorkType"] = "I";
			$icount = $this->work_equipment_model->_select_cnt($secParams);
			$secParams["sWorkType"] = "O";
			$ocount = $this->work_equipment_model->_select_cnt($secParams);
			if($icount > 0 && $ocount) {
				$workType = "Equipment Transportation";
			} else if($icount > 0) {
				$workType = "Equipment Carry-in";
			} else if($ocount > 0) {
				$workType = "Equipment Carry-out";
			} else {
				$workType = "";
			}
			$data["list"][$i]["workType"] = $workType;
		}
		//var_dump($data);die;
		$this->_print($data);
	}

	// 장비
	public function equipment_write($seq=null) {

		$this->scripts[] = "en/my_equipment.js";
		$this->load->model("company_contact_model");//en제거. model하단 같이 사용
		$this->load->model("company_equipment_model");//en제거. model하단 같이 사용

		$this->_set_sec();
		$secParams = $this->_get_sec();
		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		$data["competentList"] =  $this->company_contact_model->_select_list($secParams);
		//debug_last_query();
		//var_dump($secParams);
		$tempCnt = 0;
		if(isset($data["competentList"]))
		{
			$tempCnt = count($data["competentList"]);
		}

		if(!isset($data["competentList"]) or $tempCnt <= 0)
		{
			$this->tinyjs->alert("No person in charge is registered.\\nPlease apply for visit after registeration.");
			$this->tinyjs->pageRedirect("/en/kinxidc/equipment");
			return ;
		}

		//210611 취약점 조치사항
		for($i = 0 ; $i < $tempCnt ; $i++)
		{
			$data["competentList"][$i]["nCompanyContactSeq_org"] = $data["competentList"][$i]["nCompanyContactSeq"];
			$data["competentList"][$i]["nCompanyContactSeq"] = encryptIt($data["competentList"][$i]["nCompanyContactSeq"]);
		}
		//end

		$secParams["nStatus"] = 0;

		// null 일 경우 추가하기입니다.
		if(is_null($seq)) {
			$mode = "write";
			$data["basic"]["dtExpectStartDate"] = date('Y-m-d');
			$where = array("sStatus"=>"I", "nCompanySeq"=> $this->member["nCompanySeq"]);
			$data["equipmentList"] = $this->company_equipment_model->_select_list($where);
			//print_r($where);
			//print_r($data);
			//debug_last_query();
			if(count($data["equipmentList"])>0)
			{
				foreach($data["equipmentList"] as $key=>$val)
				{
					if($val['nQuantity']==1) $data["equipmentList"][$key]['nOption']=$val['nQuantity'];
					else
					{
						$data["equipmentList"][$key]['nOption']='<span class="select quantity" style="width:50px"><select id="#" title="수량">';
						for($i=1,$max=$val['nQuantity'];$i<=$max;$i++)
						{
							$data["equipmentList"][$key]['nOption'] .= "<option value=".$i.">".$i."</option>";
						}
						$data["equipmentList"][$key]['nOption'].="</select></span>";
					}
				}
			}
			unset($where);
		}
		else
		{// 아니면 수정하기임
			$this->load->model("work_request_model");//en삭제
			$seq = decryptIt($seq);//150313
			$where["nWorkRequestSeq"] = $seq;
			$data["basic"] = $this->work_request_model->_select_row($where);
			unset($where);
			if($data["basic"]["nContractSeq"] !== $this->member["nContractSeq"]) {
				$this->tinyjs->pageBack("Please access via normal path.");
				return false;
				exit;
			}
			//210611 취약점 조치사항
			$data["basic"]["nManagerSeq_org"] = $data["basic"]["nManagerSeq"];
			$data["basic"]["nManagerSeq"] = encryptIt($data["basic"]["nManagerSeq"]);
			//-end

			$data["basic"]["dtExpectStartTime"] = date('H', strtotime($data["basic"]["dtExpectStartDate"]));
			$data["basic"]["dtExpectStartDate"] = date('Y-m-d', strtotime($data["basic"]["dtExpectStartDate"]));

			unset($where);
			$this->load->model("work_equipment_model");//en삭제
			$where["nWorkRequestSeq"] = $seq;
			$data["eioList"] = $this->work_equipment_model->_select_list($where);
			//$data["ownList"] = $this->work_equipment_model->_select_list($where);

			$mode = "modify";
		}

		//echo "kinx equipment write data['basic']";print_r($data["basic"]);
		//echo "kinx equipment write data['competentList']";print_r($data["competentList"]);
		$data["mode"] = $mode;
		$this->_print($data);
	}

	// 장비입반출 처리
	public function equipment_proc($mode=null) {
		// 장비입반출 신청 또는 수정
		if($mode === "write" || $mode === "modify") {
			$this->load->model("work_request_model");//en삭제
			$this->load->model("work_equipment_model");//en삭제
			$data = $this->input->post();
			//echo "equipment_proc data(post)  :";print_r($data);
			//echo "mode : "; print_r($mode);
			//$data["nManagerSeq"] = decryptIt($data["nManagerSeq"]);//150313
			//echo "data nManagerSeq : "; print_r($data["nManagerSeq"] );  ;
			$this->load->model("company_contact_model"); //en제거. model하단 같이 사용
			$secParam["nCompanyContactSeq"] = decryptIt($data["nManagerSeq"]);//210611
			$resitant =  $this->company_contact_model->_select_row($secParam); // 등록자 정보를 가져온다.
			unset($secParam);

			$data["dtExpectStartDate"] .= " ";
			$data["dtExpectStartDate"] .= $data["dtExpectStartTime"];
			$data["dtExpectStartDate"] .= ":00:00";
			//$data["dtExpectEndDate"] = $data["dtExpectStartDate"];

			//$secParam = $data;
			$secParam["nCompanySeq"] = $this->member["nCompanySeq"];
			//$secParam["nManagerSeq"] = $data["nManagerSeq"];//161104
			$secParam["nManagerSeq"] = decryptIt($data["nManagerSeq"]);//210611
			$secParam["sContent"] = $data["sContent"];
			$secParam["dtExpectStartDate"] = $data["dtExpectStartDate"];
			$secParam["dtExpectEndDate"] = $data["dtExpectStartDate"];


			$secParam["sCompanyName"] = $resitant["sCompanyName"];
			$secParam["sManagerName"] = $resitant["sManagerName"];
			$secParam["sDepartmentName"] = $resitant["sDepartmentName"];
			$secParam["sInternalPhone"] = $resitant["sInternalPhone"];
			$secParam["sMobilePhone"] = $resitant["sMobilePhone"];
			$secParam["sRank"] = $resitant["sRank"];
			$secParam["sfax"] = $resitant["sfax"];
			$secParam["sEmail"] = $resitant["sEmail"];
			unset($secParam["nCompanyContactSeq"]);
			unset($secParam["dtExpectStartTime"]);
			unset($secParam["dtExpectEndTime"]);

 			//debug_var($secParam);
			//die;

			if($mode === "write")
			{
				//foreach ($data as $key=>$row) echo "(".$key."/".$row.")";
				$secParam["nContractSeq"] = $this->member["nContractSeq"];
				$secParam["nServiceType"] = $this->member["sServiceType"];
				$secParam["sActionType"] = "EIO";
				$secParam["sResultStatus"] = "Y";
				$secParam["nWorkType"] = 1;
				$data["nWorkRequestSeq"] = $this->work_request_model->_insert($secParam);

				//3. email 발송
				$this->sendmail_work($this->member["sContractName"],$data["nWorkRequestSeq"],"","EIO",
						$data["dtExpectStartDate"], $data["dtExpectStartTime"]);

				$msg = "Your request has been completed. Thank you.";
			}
			else if ($mode === "modify")
			{
				if($data["nContractSeq"] !== $this->member["nContractSeq"]) {
					$this->tinyjs->pageBack("Please access via normal path.");
					return false;
					exit;
				}
				$where["nWorkRequestSeq"] = $data["nWorkRequestSeq"];
				//var_dump($secParam);die;
				$this->work_request_model->_update($secParam, $where);
				$this->work_equipment_model->_delete($where);
				$msg = "Successfully Revised.";
			}
			//echo "for data  : "; print_r($data );  ;
			for($i=0; $i<count($data["nCompanyEquipmentSeq"]); $i++) {
				unset($secParam);
				$secParam["nWorkRequestSeq"] = $data["nWorkRequestSeq"];
				$secParam["nContractSeq"] = $this->member["nContractSeq"];
				$secParam["nCompanySeq"] = $this->member["nCompanySeq"];
				if(isset($data["sBrandName"])) $secParam["sBrandName"] = $data["sBrandName"][$i];
				if(isset($data["sModelName"])) $secParam["sModelName"] = $data["sModelName"][$i];
				if(isset($data["sEquipmentName"])) $secParam["sEquipmentName"] = $data["sEquipmentName"][$i];
				if(isset($data["nQuantity"])) $secParam["nQuantity"] = $data["nQuantity"][$i];
				if(isset($data["sUseIp"])) $secParam["sUseIp"] = $data["sUseIp"][$i];
				if(isset($data["sContent"])) $secParam["sContent"] = $data["sContent2"][$i];
				if(isset($data["sWorkType"])) $secParam["sWorkType"] = $data["sWorkType"][$i];
				if(isset($data["nCompanyEquipmentSeq"])) $secParam["nCompanyEquipmentSeq"] = $data["nCompanyEquipmentSeq"][$i];
				if($data['sWorkType'][$i] == "I")
					$secParam["nCompanyEquipmentSeq"] = 0;//161104
				$secParam["nServiceType"] = $this->member["sServiceType"];
				$this->work_equipment_model->_insert($secParam);
			}
			$this->tinyjs->pageRedirect(site_url("/en/kinxidc/equipment"), $msg);
		}
		// 장비입반출 취소
		else if ($mode === "cancel") {
			$seq = $this->uri->rsegment(4);
			if($seq) {
				$seq = decryptIt($seq);//150316
				$this->load->model("work_request_model");//en삭제
				$this->load->model("work_equipment_model");//en삭제
				$where["nWorkRequestSeq"] = $seq;
				$data["basic"] = $this->work_request_model->_select_row($where);

				if($data["basic"]["nContractSeq"] !== $this->member["nContractSeq"]) {
					$this->tinyjs->pageBack("Please access via normal path.");
					return false;
					exit;
				}

				$where["nWorkRequestSeq"] = $data["basic"]["nWorkRequestSeq"];
				$secParam["sResultStatus"] = "C";
				$this->work_request_model->_update($secParam, $where);
				$msg = "취소가 완료되었습니다.";
				$this->tinyjs->pageRedirect(site_url("/en/kinxidc/equipment"), $msg);
			} else {
				$this->tinyjs->pageBack("Please access via normal path.");
				return false;
			}
		}
	}


	public function sendmail_work($sContactName, $nWorkRequestSeq, $workType, $sActionType, $sDate, $sTime)
	{
		$this->load->library('email');
		$this->load->helper('email');

		switch($sActionType)
		{
			case "VST": $to_name = "방문신청 접수담당자"; break;
			case "EIO": $to_name = "장비입반출 관리 접수담당자"; break;
			case "ETC": $to_name = "기술요청/문의 접수담당자"; break;
		}

		$tolist = MAIL_WORK_REQUEST;

		$subject = "기타작업이 접수되었습니다";

		$sMailDocumentBody	= "";
		$sMailDocumentBody	= mailheader();
		$sMailDocumentBody .= "<table style='width:630px;height:10% !important;vertical-align:middle;border-collapse:collapse'>";
		$sMailDocumentBody	.= "<tr>";

		$sMailDocumentBody	.= " <td width='20%' height='1' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td width='30%' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td width='20%' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td width='30%' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='30' width='20%' align='center' bgcolor='#f7f7f7'>계약명</td>";
		$sMailDocumentBody	.= " <td width='30%'>".$sContactName."</td>";
		$sMailDocumentBody	.= " <td  width='20%' bgcolor='#f7f7f7'>접수번호</td>";
		$sMailDocumentBody	.= " <td width='30%'>".$nWorkRequestSeq."</td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='1' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='30' width='20%' align='center' bgcolor='#f7f7f7'>작업유형</td>";
		$strWorkType = "";
		switch($sActionType)
		{
			case "VST":
				switch($workType)
				{
					case "2":
						$strWorkType = "장비유지보수(S/W)"; break;
					case "3" :
						$strWorkType = "장비유지보수(H/W)"; break;
					case "4" :
						$strWorkType = "장비입반출"; break;
					case "5" :
						$strWorkType = "기술담당자방문"; break;
					case "6" :
						$strWorkType = "용업담당자방문"; break;
					case "7" :
						$strWorkType = "전용선작업"; break;
					case "8" :
						$strWorkType = "장애처리"; break;
					case "9" :
						$strWorkType = "전기점검"; break;
					case "10" :
						$strWorkType = "기타"; break;
				}
				$sMailDocumentBody	.= " <td colspan='3' width='80%'>고객 방문요청 ( ".$strWorkType.")</td>";
				break;
			case "EIO" :
				$sMailDocumentBody	.= " <td colspan='3' width='80%'>장비 입반출</td>";
				break;
			case "ETC":
				{
					switch($workType)
					{
						case "1":
							$strWorkType = "기술/운영문의"; break;
						case "2" :
							$strWorkType = "상품/요금문의"; break;
						case "3" :
							$strWorkType = "서버리부팅"; break;
						case "4" :
							$strWorkType = "기타문의"; break;
					}
					$sMailDocumentBody	.= " <td colspan='3' width='80%'>기타작업 ( ".$strWorkType.")</td>";
				}
				break;
		}

		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='1' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='30' width='20%' align='center' bgcolor='#f7f7f7'>방문(작업)예정일</td>";
		$sMailDocumentBody	.= " <td  colspan='3'width='80%'>".$sDate." ".$sTime."</td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='1' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td colspan='4' align='center' width='100%'>자세한 사항은 인트라넷>운영관리에서 확인바랍니다.</td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='1' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "</table>";

		$sMailDocumentBody .= mailfooter();

		$this->email->clear(TRUE);
		switch($sActionType)
		{
			case "VST": $this->email->from(MAIL_FROM_INFO, "방문신청 접수"); break;
			case "EIO": $this->email->from(MAIL_FROM_INFO, "장비입반출 관리 접수"); break;
			case "ETC": $this->email->from(MAIL_FROM_INFO, "기술요청/문의  접수"); break;
		}

		//if(ENVIRONMENT== 'development') $tolist = 'yjcin13@kinx.net';

		$this->email->to($tolist);# 수신
		$this->email->subject($subject);//제목
		$this->email->message($sMailDocumentBody);//content
		//echo $sMailDocumentBody;
		$this->email->send();
		//echo $this->email->print_debugger();
	}
}