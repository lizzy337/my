<?php
/**
 * @author BahamuT
 * @version 2.1.0
 * @license copyright by INDM_BahamuT
 *
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 * @property CI_Benchmark $benchmark
 * @property CI_Calendar $calendar
 * @property CI_Cart $cart
 * @property CI_Config $config
 * @property CI_Controller $controller
 * @property CI_Email $email
 * @property CI_Encrypt $encrypt
 * @property CI_Exceptions $exceptions
 * @property CI_Form_validation $form_validation
 * @property CI_Ftp $ftp
 * @property CI_Hooks $hooks
 * @property CI_Image_lib $image_lib
 * @property CI_Input $input
 * @property CI_Language $language
 * @property CI_Loader $load
 * @property CI_Log $log
 * @property CI_Model $model
 * @property CI_Output $output
 * @property CI_Pagination $pagination
 * @property CI_Parser $parser
 * @property CI_Profiler $profiler
 * @property CI_Router $router
 * @property CI_Session $session
 * @property CI_Sha1 $sha1
 * @property CI_Table $table
 * @property CI_Trackback $trackback
 * @property CI_Typography $typography
 * @property CI_Unit_test $unit_test
 * @property CI_Upload $upload
 * @property CI_URI $uri
 * @property CI_User_agent $user_agent
 * @property CI_Validation $validation
 * @property CI_Xmlrpc $xmlrpc
 * @property CI_Xmlrpcs $xmlrpcs
 * @property CI_Zip $zip
 * @property Tinyjs $tinyjs
 */
class Common extends CI_Controller {

	protected $cfg = array(
		"module_dir_name" => "_modules",
		"title" => "KINX",
		"session_key" => "sMemberId",
		"perpage" => 20,
		"is_debug" => false,
		"en_salt" => "2013kinxintranet!@abcdefgHIJKLNMopqrstuVWXYZ#$",
		"meta" => "케이아이엔엑스, KINX, IX, CDN, 클라우드, IDC, 서버호스팅, 코로케이션, 부가서비스, 백업, 전문기술지원, 글로벌인터넷서비스",
	);
	public $scripts = array();
    public $css = array('myencommon.css');
	public $is_login = false;
	public $member = array();


	/*
	 * intranet 	-> ism
	 * tCompany 	-> tCustomer
	 * tContract	-> tAccount
	 * tContractService -> tContract
	 * ism db에서 정보를 가져오도록 수정진행함(2017-07).
	 * 기존 변수를 그대로 사용함.
	 * 즉, ism 의  nAccountSeq이지만 nContractSeq
	 *
	 */

	public function __construct() {
		parent::__construct();

		if($this->cfg["is_debug"]) {
			$sessionAry[$this->cfg["session_key"]] = "sangyoon09";
			$this->session->set_userdata($sessionAry);
		}

		if($this->session->userdata($this->cfg["session_key"]))
		{
			//intranet
			//$this->load->model("contract_model");
			//$this->member = $this->contract_model->_select_row(array("sMemberId"=>$this->session->userdata($this->cfg["session_key"])));
			//$this->member['sServiceType'] =$this->session->userdata('sServiceType');
			//ism
			$this->load->model("account_ism_model","md_account");
			$this->member = $this->md_account->get_account_byId($this->session->userdata($this->cfg["session_key"]));
			//-start intranet db 쪽을 참고하는 경우를 대비해서 값을 복사함.
			$this->member['nContractSeq'] = $this->member['nAccountSeq'];//
			$this->member['nCompanySeq'] = $this->member['nCustomerSeq'];//
			$this->member['sContractName'] = $this->member['sName'];//
			$this->member['sMemberId'] = $this->member['sAccountID'];//

			$this->member['sServiceType'] =$this->session->userdata('sServiceType');


			$this->is_login = TRUE;
		} else {
			$this->is_login = FALSE;
		}

		$seg = $this->uri->uri_string();
		if( is_null($seg))//home main page
		{
			$this->cfg["title"] = "KINX (Korea Internet Neutral eXchange) 글로벌 인터넷 인프라 서비스 제공 : IX, CDN, 클라우드, IDC";
			$this->cfg["meta"] = "케이아이엔엑스, KINX, IX, CDN, 클라우드, IDC, 서버호스팅, 코로케이션, 부가서비스, 백업, 전문기술지원, 글로벌인터넷서비스";
		}
		else {
			$seg2 = explode("/",$seg);
			switch($seg2[0])
			{
				case "ix" :
					$this->cfg["title"] = "IX (Internet eXchange) : 국내유일 중립적 IX 서비스, 46개 ISP 회원사, 글로벌 POP운영, 290만 명과 직접 연결된 초고속 네트워크";
					$this->cfg["meta"] = "케이아이엔엑스, KINX, IX, 멀티호밍 인터넷서비스, 글로벌 인터넷서비스, 연동망 구축, Transit, Peering, 부가서비스, 네트워크, 해외POP, 백업, 전문기술지원";
					break;
				case "cdn" :
					$this->cfg["title"] = "CDN (Contents Delivery Network) : 대용량 파일 초고속 다운로드, 스트리밍, 캐시 서비스, 최적의 속도와 안정적인 컨텐츠 전송";
					$this->cfg["meta"] = "케이아이엔엑스, KINX, CDN , 초고속 다운로드, HTTP다운로드, Hybrid다운로드, 스트리밍, N스크린, 모바일스트리밍, 캐시, 컨텐츠 전송, 파일동기화, 부가서비스, 트랜스코딩, 플레이어, 보안, DRM, 백업, 전문기술지원";
					break;
				case "cloud" :
					$this->cfg["title"] = "CLOUD : 클라우드 서버, 클라우드 스토리지, 프라이빗 클라우드, 오픈스택 기반의 확장성과 안정성";
					$this->cfg["meta"] = "케이아이엔엑스, KINX, 클라우드, Ixcloud, 클라우드 서버, 클라우드 스토리지, 프라이빗 클라우드, 퍼블릭 클라우드, 매니지드 클라우드, 서버이미지, 인스턴스, 오픈스택, Hadoop, 전문기술지원";
					break;
				case "idc" :
					$this->cfg["title"] = "IDC (Internet Data Center) : 도곡, 가산, 분당, 상암 센터 운영, 최적의 IDC인프라와 네트워크 제공, 서버호스팅, 코로케이션";
					$this->cfg["meta"] = "케이아이엔엑스, KINX, IDC, 인터넷데이터센터, 도곡, 가산, 분당, 상암 센터 운영, 최적의 IDC인프라와 네트워크 제공, 서버호스팅, 코로케이션, 부가서비스, 보안, 관제, 백업, 소프트웨어임대, 전문기술지원, IX기반 IDC";
					break;
				case "mypage" :
					$this->cfg["title"] = "MY KINX : 서비스관리, 통계, 결제관리, 회원정보관리, IDC출입관리, 나의 상담내역, 전화번호 안내";
					$this->cfg["meta"] = "케이아이엔엑스, KINX, MY KINX, 서비스관리, 통계, 결제관리, 회원정보관리, IDC출입관리, 나의 상담내역, 전화번호 안내";
					break;
				case "company" :
					$this->cfg["title"] = "KINX (Korea Internet Neutral eXchange) 글로벌 인터넷 인프라 서비스 제공 : IX, CDN, 클라우드, IDC";
					$this->cfg["meta"] = "케이아이엔엑스, KINX, IX, CDN, 클라우드, CLOUD, IDC, 서버호스팅, 코로케이션, 부가서비스, 백업, 전문기술지원, 글로벌인터넷서비스";
					break;
				case "support" :
					//$this->cfg["title"] = "기술지원 : 서비스 상담, 기술 상담, 1:1문의, 공지사항, 결제안내, IDC위치";
					$this->cfg["title"] = "KINX (Korea Internet Neutral eXchange) 글로벌 인터넷 인프라 서비스 제공 : IX, CDN, 클라우드, IDC";
					$this->cfg["meta"] = "케이아이엔엑스, KINX, IX, CDN, 클라우드, CLOUD, IDC, 서버호스팅, 코로케이션, 부가서비스, 백업, 전문기술지원, 글로벌인터넷서비스";
					break;
				case "agreements" :
					$this->cfg["title"] = "KINX (Korea Internet Neutral eXchange) 글로벌 인터넷 인프라 서비스 제공 : IX, CDN, 클라우드, IDC";
					$this->cfg["meta"] = "케이아이엔엑스, KINX, IX, CDN, 클라우드, CLOUD, IDC, 서버호스팅, 코로케이션, 부가서비스, 백업, 전문기술지원, 글로벌인터넷서비스";
					break;
				default : //홈페이지
					$this->cfg["title"] = "KINX (Korea Internet Neutral eXchange) 글로벌 인터넷 인프라 서비스 제공 : IX, CDN, 클라우드, IDC";
					$this->cfg["meta"] = "케이아이엔엑스, KINX, IX, CDN, 클라우드, IDC, 서버호스팅, 코로케이션, 부가서비스, 백업, 전문기술지원, 글로벌인터넷서비스";
					break;
			}
		}
	}

	public function _print($data=array()) {
		$data["environment"]=ENVIRONMENT;
		$this->_init_print($data);
		$this->template->print_("tpl");
	}

	private function _init_print($data=array()){
		$this->_set_value($data);
		$this->_set_modules($data);

		$this->template->assign($data);
		$tplpath = $this->uri->rsegment(1)."/".$this->uri->rsegment(2).$this->config->item("url_suffix");
		if($this->uri->segment(1) != $this->uri->rsegment(1) && $this->uri->segment(1)) {
			$tplpath = $this->uri->segment(1)."/".$this->uri->rsegment(1)."/".$this->uri->rsegment(2).$this->config->item("url_suffix");
		}

		$this->template->define("tpl", $tplpath);
	}
        //-start by lizzy
    public function _print_uri($data=array(), $uri)
    {
		$data["environment"]=ENVIRONMENT;
		$this->_init_print_uri($data,$uri);
		$this->template->print_("tpl");
	}

	private function _init_print_uri($data=array(),$uri)
	{
		$this->_set_value($data);
		$this->_set_modules($data);

		$this->template->assign($data);
                /*
		$tplpath = $this->uri->rsegment(1)."/".$this->uri->rsegment(2).$this->config->item("url_suffix");
		if($this->uri->segment(1) != $this->uri->rsegment(1) && $this->uri->segment(1)) {
			$tplpath = $this->uri->segment(1)."/".$this->uri->rsegment(1)."/".$this->uri->rsegment(2).$this->config->item("url_suffix");
		}
                */
        $tplpath = $uri.$this->config->item("url_suffix");
		$this->template->define("tpl", $tplpath);
	}
	//--end by lizzy
	private function _set_modules(&$data) {
		$filePath = APPPATH."views/".$this->cfg["module_dir_name"];
		$map = directory_map($filePath);
		if(is_array($map)) {
			$modulesList = array();
			foreach($map as $dir => $dirRow) {
				if(is_array($dirRow)) {
					foreach($dirRow as $modulePath) {
						if(gettype($modulePath)=='array')
						{
							foreach($modulePath as $modulePath2) {
								$htmlPath = $this->cfg["module_dir_name"]."/".$dir."/".$modulePath2;
								$vowels = array("\\", "/");
								$mName = str_replace($vowels, "", $dir)."_".substr($modulePath2, 0, -1 * strlen($this->config->item("url_suffix")));
								$modulesList[$mName] = $htmlPath;
							}
						}
						else
						{
							$htmlPath = $this->cfg["module_dir_name"]."/".$dir."/".$modulePath;
							$vowels = array("\\", "/");
							$mName = str_replace($vowels, "", $dir)."_".substr($modulePath, 0, -1 * strlen($this->config->item("url_suffix")));
							$modulesList[$mName] = $htmlPath;
						}
					}
				}
			}
			$this->template->define($modulesList);
		}
		return TRUE;
	}

	private function _set_value(&$data) {
		$data["scripts"] = array_unique($this->scripts);
        $data["css"] = array_unique($this->css);
		$data["url_suffix"] = $this->config->item("url_suffix");
		$data["uri_string"] = urldecode($this->uri->uri_string());
		$data["rsegment_1"] = urldecode($this->uri->rsegment(1));
		$data["rsegment_2"] = urldecode($this->uri->rsegment(2));
		$data["host"] = $this->config->item("base_url");
		$data["referer"] = $this->input->server("HTTP_REFERER");
		$data["cfg"] = $this->cfg;
		$data["is_login"] = $this->is_login;
	}

	/**
	 * 검색세팅.
	 * @param array $exceptParams array("edDcType","edIsOver");
	 */
	public function _set_sec($exceptParams = NULL) {
		//제외항목 우선 제거
		if(is_array($exceptParams)) {
			foreach($exceptParams as $val) {
				if(isset($_POST[$val])) {
					unset($_POST[$val]);
				}
			}
		}

		$params = $this->input->post();
		$paramsData = array();
		if(is_array($params) && count($params) > 0) {
			foreach($params as $key => $val) {
				if( is_array($this->input->post($key)) ) {
					$paramsData[$key] = "Array";
					$paramsData[$key."_CNT"] = count($this->input->post($key));
					foreach($this->input->post($key) as $key1 => $data) {
						$paramsData[$key."_".$key1] = urlencode($data);
					}
					continue;
				}
				if(! $val) {
					continue;
				}
				$paramsData[$key] = urlencode($val);
			}

			$url = site_url("/".$this->uri->uri_string()."/".$this->uri->assoc_to_uri($paramsData));
			redirect($url);
		}
	}

	public function _get_sec($assoc=1) {
		$uriAry = $this->uri->ruri_to_assoc($assoc);
		$return = array();
		foreach ($uriAry as $key => $data) {
			if ($data === "Array") {
				$tmpAry = array();
				for ($i = 0; $i < $uriAry[$key . "_CNT"]; $i++) {
					$tmpAry[] = $uriAry[$key . "_" . $i];
				}
				$return[$key] = $tmpAry;
				continue;
			}
			if (strrpos($key, "_CNT")) {
				continue;
			}
			$return[$key] = $data;
		}
		return $return;
	}

	public function _set_pager($params) {
		$config["suffix"] = $this->config->item("url_suffix");
		$config["total_rows"] = $params["CNT"];
		$config["per_page"] = $params["PRPAGE"];

		$url = preg_replace("/\/page\/(.*)/", "", $this->uri->ruri_string());
		$url = (substr($url,0,1) === "/") ? $url : "/".$url;

		$config["first_url"] = $url.$config["suffix"];
		$config["base_url"] = $url."/page/";
		$config["uri_segment"] = count(explode("/", $url)) + 1;

		$config["num_links"] = 5;
		$config["first_link"] = "";
		$config["first_tag_open"] = "<span class=\"pre_end\">";
		$config["first_tag_close"] = "</span>";
		$config["prev_link"] = "";
		$config["prev_tag_open"] = "<span class=\"pre\">";
		$config["prev_tag_close"] = "</span>";

		$config["next_link"] = "";
		$config["next_tag_open"] = "<span class=\"next\">";
		$config["next_tag_close"] = "</span>";
		$config["last_link"] = "";
		$config["last_tag_open"] = "<span class=\"next_end\">";
		$config["last_tag_close"] = "</span>";

		$config["cur_tag_open"] = "<strong>";
		$config["cur_tag_close"] = "</strong>";

		$this->load->library("pagination");
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}

	Public Function CheckTextOut($CheckValue)
	{
		if (strlen($CheckValue) > 0)
		{
			$CheckValue = str_replace("&amp;" , "&" , $CheckValue);
			$CheckValue = str_replace("&lt;", "<" , $CheckValue);
			$CheckValue = str_replace("&gt;", ">" , $CheckValue);
			$CheckValue = str_replace("&quot;", "'" , $CheckValue);
			$CheckValue = str_replace("&#39;", chr(34) , $CheckValue);
			$CheckValue = str_replace("&apos;", "," , $CheckValue);
			//$CheckValue = str_replace(chr(13)&chr(10), "<br>" , $CheckValue);
			$CheckValue = str_replace("\n", "<br>" , $CheckValue);
		}
		return $CheckValue;
	}

	public function download() {
		$file_down_folder = $this->uri->segment(3);
		$file_down_name = $this->uri->segment(4);
		$file_view_name = decryptIt($this->uri->segment(5));
		$file_down_path = '../intranetfiles/'.$file_down_folder.'/'.$file_down_name;

		$this->load->helper('download');

		$data = file_get_contents($file_down_path);

		force_download($file_view_name, $data);
	}
}