<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/en/Common.php");

class Member extends Common{

    public function __construct()
    {
        parent::__construct();

        if($this->is_login === false)
        {
            $this->output->set_status_header('410');
            $this->tinyjs->pageRedirect("/en","Sign in to access your account");
            exit;
        }

        $this->load->model("customer_ism_model","md_customer");
        $this->load->model("account_member_ism_model","md_accouontmember");
        $this->load->model("account_ism_model","md_account");
    }


    public function index()
    {
        //업체정보 추출
        $data["company"] = $this->md_customer->get_customer_bySeq($this->member['nCustomerSeq']);
        $data["company"]['arOtherInfo'] = json_decode($data["company"]['arOtherInfo'],true);
        $data["member"] = $this->member;
        //회원정보 추출 . 190417
        $data["account"] = $this->md_account->get_account_bySeq($this->member["nAccountSeq"]);

        //담당자 정보 추출
        $data["managerE"] = $this->md_accouontmember->get_accountmem_byAccSeq($this->member["nAccountSeq"], 'E');//업무담당자
        $data["managerC"] = $this->md_accouontmember->get_accountmem_byAccSeq($this->member["nAccountSeq"], 'C');//요금담당자

        $this->scripts[] = "en/my_member.js";
        $this->_print($data);
    }



    public function proc($mode=null)
    {
        $result = array();
        $this->load->library("form_validation");
        if($this->form_validation->run("memberen"))
        {
            $this->load->helper("encryption");

            $postdata = $this->input->post(NULL, TRUE);
            unset($postdata['nAccountMemberSeq']); //추가된 불필요한 정보는 삭제.190417
            //$data["nContractSeq"] = $this->member["nContractSeq"];
            // 있는지 검사
            $row = $this->md_account->get_account_byId($this->member["sAccountID"]);

            //password 암호화
            //$encpw = password_hash($postdata['sPassword'], PASSWORD_BCRYPT,array("cost" => 10, 'salt'=>PASSWORD_SALT) );
            //php7 버전에서 salt 옵션 제거. 171108
            $encpw = password_hash($postdata['sPassword'], PASSWORD_BCRYPT);

            // 있다면 업데이트
            if(isset($row["nAccountSeq"]))
            {
                //for - ism
                $where = array(
                    'nAccountSeq'=>$this->member["nAccountSeq"]
                );

                $change_data = array(
                    'sPassword'=> $encpw,
                    'rePassword'=>$postdata['rePassword']
                );

                $this->md_account->update($change_data, $where);

                //for intranet
                $this->load->model("contract_model");
                $where_intra["nContractSeq"] = $this->member["nAccountSeq"];
                $postdata['sPassword'] = $encpw;
                $this->contract_model->_update($postdata, $where_intra);
            }
            else // 없다면 에러
            {
                $result['code'] = 'f';
                $result['msg'] ="Cannot confirm your information.";
            }
            $result['code'] = 's';
            $result['msg'] ="Your account information has been successfully updated.";
        }
        else
        {
            $result['code'] = 'f';
            $result['msg'] =strip_tags(validation_errors());
        }

        echo(json_encode($result));
        exit;

    }

    //e - 업무담당자, c - 요금담당자 layer 띄우기
    public function detailAM($type, $acmemseq)
    {
        $nAccountMemberSeq = decryptIt($acmemseq);
        $data["data"] = $this->md_accouontmember->get_accountmem_byAccMemberSeq($this->member["nAccountSeq"],$nAccountMemberSeq, $type);
        $data["mode"] = $type;
        $data["nAccountMemberSeq"] = $acmemseq;

        //va($data);die;
        $this->_print($data);
    }


    //e - 업무담당자, c - 요금담당자 수정
    public function modifyAM()
    {
        mysqli_report(MYSQLI_REPORT_STRICT | MYSQLI_REPORT_ALL);

        try
        {
            $result = array();
            $this->load->library("form_validation");
            if($this->form_validation->run('acmemberen'))
            {
                $postdata = $this->input->post(NULL, TRUE);

                if(isset($postdata['nAccountMemberSeq'])){ // 있다면 업데이트
                    $nAccountMemberSeq = decryptIt($postdata['nAccountMemberSeq']);

                    $where = array(
                        'nAccountSeq'       => $this->member["nAccountSeq"],
                        'nAccountMemberSeq' => decryptIt($postdata['nAccountMemberSeq']),
                        'sDutyType'         => $postdata['sDutyType']
                    );

                    $change_data = array(
                        'sName'       => chg_xss_quot($postdata['sName']),
                        'sRank'       => chg_xss_quot($postdata['sRank']),
                        'sDepartment' => chg_xss_quot($postdata['sDepartment']),
                        'sPhone'      => $postdata['sInternalPhone'],
                        'sMobile'     => $postdata['sMobilePhone'],
                        'sEmail'      => $postdata['sEmail']
                    );

                    $this->md_accouontmember->update_accmem_info($change_data,$where);
                }
                else{ // 없다면 인썰트
                    $this->md_accouontmember->insert_accmem_info($postdata);
                }

                $result['code'] = "s";
                $result['msg']  = "Your information has been successfully updated.";
            }
            else{
                $result['code'] = 'f';
                $result['msg']  = strip_tags(validation_errors());
            }
        }
        catch(mysqli_sql_exception $e)
        {
            $this->output->set_status_header('500');
            $result = array("code"=>'f', "msg"=>'The information failed to update.');
            log_message("error", $e);
        }
        catch (Exception $e) {
            $this->output->set_status_header('500');
            $result = array("code"=>'f', "msg"=>'There was a problem modifying the information.');
            log_message("error", $e);
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($result, JSON_NUMERIC_CHECK));
    }
}