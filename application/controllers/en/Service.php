<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/en/Common.php");
class Service extends Common{
	
	public function __construct() 
	{
		parent::__construct();
		
		#intranet model중 ism으로 교체된 파일들은 en/intra_notuse/ 하부로 이동
		#Company_model.php Contract_Member_model.php
		
		
		$this->load->model("contract_ism_model","md_contract"); 
		$this->load->model("product_ism_model","md_product"); 
		$this->load->model("account_ism_model","md_account");  
		$this->load->model("customer_ism_model","md_customer");  
		$this->load->model("account_member_ism_model","md_accouontmember");
		
		
		if($this->is_login === false) {
			$this->output->set_status_header('410');
			$this->tinyjs->pageRedirect("/en","Sign in to access your account");
			exit;
		}
	}

	public function index() 
	{	
		$this->_set_sec();
		$secParams = $this->_get_sec();
		
		
		$secParams["offset"] = isset($secParams["page"]) ? $secParams["page"] : "0";
		$secParams["limit"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$secParams["nAccountSeq"] = $this->member["nAccountSeq"];
		
		//$secParams["select"] = "A.*, C.sServiceType, C.sServiceName, C.sServiceDetail";
		//$secParams["orderby"] = "C.sServiceType ASC, C.sServiceName ASC, C.sServiceDetail ASC, (A.dtEndDate - current_date) ASC";
		$secParams["sStatus"] = isset($secParams["sStatus"])? $secParams["sStatus"] : "Y"; //added by lizzy 141008
		$secParams["main"] = 0;// 'main'이 설정되어 있으면 계약종료일(dtEnd)이 현재보다 이후인 목록을 가져온다. (0 도 가능. 설정값은 중요하지 않고 존재여부만 중요)
		
		$data["cnt"] = $this->md_contract->get_list($secParams, TRUE);
		$data["list"] =  $this->md_contract->get_list($secParams);
		
		$pager["CNT"] = $data["cnt"];
		$pager["PRPAGE"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$pagerHtm = $this->_set_pager($pager);

		$data["pager"] = $pagerHtm;
		$data["pagerIdx"] = $data["cnt"] - $secParams["offset"];
		$data["secParams"] = $secParams;
		//va($data);die;
		$this->_print($data);
	}
	
	public function detail($nContractSeq=null) 
	{
		if($nContractSeq) 
		{
			$nContractSeq = decryptIt($nContractSeq);//150313
			$this->scripts[] = "en/my_service.js";
			$data["basic"] = $this->md_contract->get_contract_bySeq($nContractSeq);			
			//unset($where);
			
			//안정화기간 구하기
			$data['basic']['stabilize'] = ($data['basic']['dtCharge'] > $data['basic']['dtStart']) ? $data['basic']['dtStart'].' ~ '. date("Y-m-d", strtotime("-1 day", strtotime($data['basic']['dtCharge']))) : '';
			if($data['basic']['stabilize'] == null)
				$data['basic']['stabilize'] = '-';
			
			// 보안처리 타인의 정보를 볼수 없습니다.
			if($data["basic"]["nAccountSeq"] !== $this->member["nAccountSeq"]){
				$this->tinyjs->pageBack("Please access via normal path.");
				exit;
			}
			$nAccountSeq = $data["basic"]["nAccountSeq"];
			
			$data["service"] = $this->md_product->get_product_bySeq($data["basic"]['nProductSeq']);
			$data["contract"] = $this->md_account->get_account_bySeq($nAccountSeq);
			 
			$nCustomerSeq = $data["contract"]["nCustomerSeq"];
			$data["company"] = $this->md_customer->get_customer_bySeq($nCustomerSeq);
			$data["company"]['arOtherInfo'] = json_decode($data["company"]['arOtherInfo'],true);

			$data["cMember"] = $this->md_accouontmember->get_accountmem_byAccSeq($nAccountSeq, "C");//요금담당자
			
			$data["eMember"] = $this->md_accouontmember->get_accountmem_byAccSeq($nAccountSeq, "E");//업무담당자
			
			//-start 20141013 환율Type 구하기
			//$this->load->model("/en/bill_list_model");
			$this->load->model("/bill_list_model"); //국문 model로 함침
			$data["ExchangeRateType"]= $this->bill_list_model->_get_exchangeRatetype_ism($nAccountSeq);
			//-end
			//va($data);die;
			$this->_print($data);
		} else {
			$this->tinyjs->pageBack("Please access via normal path,");
		}
	}
	
	//e - 업무담당자, c - 요금담당자
	public function competent($mode="e") 
	{ 
		$data["data"] = $this->md_accouontmember->get_accountmem_byAccSeq($this->member["nAccountSeq"], $mode);
		$data["mode"] = $mode; 
		$this->_print($data);
	}
	
	public function proc($mode=null) 
	{
		if($mode ==="competent")//업무,요금담당자 변경
		{
			//$this->load->model("/en/contract_member_model");
			
			$postdata = $this->input->post();
			$row = $this->md_accouontmember->get_accountmem_byAccSeq($this->member["nAccountSeq"],$postdata['sDutyType']);
			//va('proc row'); va($row); die;
			
			
			//$data["nContractSeq"] = $this->member["nContractSeq"];
			// 있는지 검사
			//$where["nContractSeq"] = $this->member["nContractSeq"];
			//$where["sDutyType"] = $this->input->post("sDutyType");
			//$row = $this->contract_member_model->_select_row($where);
			
			// 있다면 업데이트
			if(isset($row["nAccountMemberSeq"])) 
			{
				$where = array(
						'nAccountSeq'=>$this->member["nAccountSeq"],
						'nAccountMemberSeq'=>$row['nAccountMemberSeq'],
						'sDutyType'=>$postdata['sDutyType']
				);
				
				$change_data = array(
						'sName'=>$postdata['sName'],
						'sRank'=>$postdata['sRank'],
						'sPhone'=>$postdata['sInternalPhone'],
						'sDepartment'=>$postdata['sDepartment'],
						'sMobile'=>$postdata['sMobilePhone'],
						'sEmail'=>$postdata['sEmail']
				);
					
				$this->md_accouontmember->update_accmem_info($change_data,$where);
			}			
			else // 없다면 인썰트 
			{
				//$this->contract_member_model->_insert($data);
				$this->md_accouontmember->insert_accmem_info($postdata);
			}
			redirect($this->input->server("HTTP_REFERER"));
		}
	}
	////////////////////////////////////////////////////////////////////////////////////	
	public function system_info() {
		$this->load->model("Mykinx_systeminfo_model");//en삭제
		$this->_set_sec();
		$secParams = $this->_get_sec();
		$secParams["offset"] = isset($secParams["page"]) ? $secParams["page"] : "0";
		$secParams["limit"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		
		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		$secParams["oKey"][0] = "sEquipmentName";
		$secParams["oType"][0] = "asc";
		$secParams["oKey"][1] = "sBrand";
		$secParams["oType"][1] = "asc";
		$secParams["oKey"][2] = "sModel";
		$secParams["oType"][2] = "asc";
		$secParams["oKey"][3] = "dtCreateDate";
		$secParams["oType"][3] = "desc";
		
		
		$data["cnt"] = $this->Mykinx_systeminfo_model->_select_cnt($secParams);
		$data["list"] =  $this->Mykinx_systeminfo_model->_select_list($secParams);
		
		$pager["CNT"] = $data["cnt"];
		$pager["PRPAGE"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$pagerHtm = $this->_set_pager($pager);

		$data["pager"] = $pagerHtm;
		$data["pagerIdx"] = $data["cnt"] - $secParams["offset"];
		$data["secParams"] = $secParams;
		
		$this->_print($data);
	}
	
	public function dns_info() 
	{
		$this->load->model("Mykinx_dnsinfo_model");//en제거
		$this->_set_sec();
		$secParams = $this->_get_sec();
		$secParams["offset"] = isset($secParams["page"]) ? $secParams["page"] : "0";
		$secParams["limit"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
	
		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		$secParams["nStatus"] = "Y";
		$secParams["oKey"][0] = "sDns";
		$secParams["oType"][0] = "asc";
		$secParams["oKey"][1] = "sHost";
		$secParams["oType"][1] = "asc";
		$secParams["oKey"][2] = "sIp";
		$secParams["oType"][2] = "asc";
		
		$data["cnt"] = $this->Mykinx_dnsinfo_model->_select_cnt($secParams);
		$data["list"] =  $this->Mykinx_dnsinfo_model->_select_list($secParams,0);
	
		$pager["CNT"] = $data["cnt"];
		$pager["PRPAGE"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$pagerHtm = $this->_set_pager($pager);
	
		$data["pager"] = $pagerHtm;
		$data["pagerIdx"] = $data["cnt"] - $secParams["offset"];
		$data["secParams"] = $secParams;
	
		$this->_print($data);
	}
	
	public function dns_request()
	{
		$this->scripts[] = "en/my_dns.js";
		$this->load->model("Mykinx_dnsinfo_model");//en제거
		
		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		//1.등록자 정보 가져오기
		$secParams["oKey"][0] = "sCompanyType";
		$secParams["oType"][0] = "asc";
		$secParams["oKey"][1] = "nSort";
		$secParams["oType"][1] = "asc";
		$secParams["oKey"][2] = "sCompanyName";
		$secParams["oType"][2] = "asc";
		$secParams["oKey"][3] = "sManagerName";
		$secParams["oType"][3] = "asc";
		$data["companycontact"] = $this->Mykinx_dnsinfo_model->_select_list($secParams,1);
		//print_r($data["companycontact"]);
		//echo "<br>-----";
		
		//2. 보유 DNS리스트 
		unset($secParams["oKey"][0]);
		unset($secParams["oType"][0]);
		unset($secParams["oKey"][1]);
		unset($secParams["oType"][1]);
		unset($secParams["oKey"][2]);
		unset($secParams["oType"][2]);
		unset($secParams["oKey"][3]);
		unset($secParams["oType"][3]);
		$secParams["nStatus"] = "Y";
		$data["companydnslist"] = $this->Mykinx_dnsinfo_model->getCompanyDNSList($secParams);
		//print_r($data["companydnslist"]);

		$this->_print($data);		
	}
	
	public function json($mode=null) 
	{	
		if($mode == "registantUser") {
			$this->load->model("Mykinx_dnsinfo_model");//en제거
			$data = $this->input->post();
			$data["nCompanyContactSeq"] = decryptIt($data["nCompanyContactSeq"]);//150313
			$secParams["nCompanyContactSeq"] = $data["nCompanyContactSeq"];
			$data["registant"] = $this->Mykinx_dnsinfo_model->_select_row($secParams,1);
			$json = array("sCompanyName"=>$data["registant"]["sCompanyName"],
					"sManagerName"=>$data["registant"]["sManagerName"],
					"sRank"=>$data["registant"]["sRank"],
					"sInternalPhone"=>$data["registant"]["sInternalPhone"],
					"sfax"=>$data["registant"]["sfax"],
					"sMobilePhone"=>$data["registant"]["sMobilePhone"],
					"sEmail"=>$data["registant"]["sEmail"]);
			echo json_encode($json);
		}
		
	}
	public function dnsRequest()//등록하기 
	{
		$data = $this->input->post();
		$data["companycontact"] = decryptIt($data["companycontact"]);//150313
		 
		$this->load->model("Mykinx_dnsinfo_model");//en제거
		//1-1.Service Type 구하기 
		$nContractSeq = $this->member["nContractSeq"];
		$data["nContractSeq"] = $this->member["nContractSeq"];
		$data["nCompanySeq"] = $this->member["nCompanySeq"];

		//db에서 추출한 nServiceCode가 1이면 member["sServiceType"]에 1, 1보다 크면 2로 설정
		//1이면 IX, 2이면 IDC로 판별 
		if( $this->member["sServiceType"]==1 )
		{ 
			$data["nServiceType"]= "IX";
		}else
		{
			$data["nServiceType"]= "IDC";
		}
		
		//2.tWorkRequest에 추가
		$data["nWorkRequestSeq"]=$this->Mykinx_dnsinfo_model->InsertWorkRequest($data);
		
		//3. tWorkDns 에 추가		
		//$this->Mykinx_dnsinfo_model->InsertWorkDNS($data);
		for( $i=0; $i<count($data["nRequestDnsIdx"]); $i++)
		{
			unset($secParam);
			$secParam["nWorkRequestSeq"] = $data["nWorkRequestSeq"];
			if(isset($data["nRequestDnsIdx"]))$secParam["nCustomerDnsSeq"] = $data["nRequestDnsIdx"][$i];
			$secParam["nContractSeq"] = $this->member["nContractSeq"];
			$secParam["nCompanySeq"] = $this->member["nCompanySeq"];
			if(isset($data["req_dns"]))$secParam["sDns"] = $data["req_dns"][$i];
			if(isset($data["req_host"]))$secParam["sHost"] = $data["req_host"][$i];
			if(isset($data["req_ip"]))$secParam["sIp"] = $data["req_ip"][$i];
			if(isset($data["req_cmt"]))$secParam["sComment"] = $data["req_cmt"][$i];
			if(isset($data["worktype"]))$secParam["strWorkType"] = $data["worktype"][$i];
			$this->Mykinx_dnsinfo_model->InsertWorkDNS($secParam);
		}
		
		//4. mail 발송
		$this->sendmail_work($this->member["sContractName"],$data["nWorkRequestSeq"],"DNS");
		
		//5.Alert . 페이지 전환
		$this->tinyjs->alert("Your request has been completed. Thank you.");
		$this->tinyjs->pageRedirect("/en/service/dns_info");
	}
	
	public function sendmail_work($sContactName, $nWorkRequestSeq, $worktype)
	{
		$this->load->library('email');
		$this->load->helper('email');
		
		$to_name = "";
		$to_name .= $worktype." 접수담당자";
		$tolist = MAIL_WORK_REQUEST;
		
		$subject = $worktype." 등록 요청이 접수되었습니다.";
		
		$sMailDocumentBody	= "";
		$sMailDocumentBody	= mailheader();
		$sMailDocumentBody .= "<table style='width:630px;height:10% !important;vertical-align:middle;border-collapse:collapse'>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td width='20%' height='1' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td width='30%' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td width='20%' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td width='30%' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='30' width='20%' align='center' bgcolor='#f7f7f7'>계약명</td>";
		$sMailDocumentBody	.= " <td width='30%'>".$sContactName."</td>";
		$sMailDocumentBody	.= " <td align='center' width='20%' bgcolor='#f7f7f7'>접수번호</td>";
		$sMailDocumentBody	.= " <td width='30%'>".$nWorkRequestSeq."</td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='1' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='30' width='20%' align='center' bgcolor='#f7f7f7'>작업유형</td>";
		if( $worktype == "DNS")
		{
			$sMailDocumentBody	.= " <td colspan='3' width='80%'> DNS 등록</td>";
		}else{
			$sMailDocumentBody	.= " <td colspan='3' width='80%'> 모니터링IP 등록</td>";
		}
			
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='1' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";		
		$sMailDocumentBody	.= " <td colspan='4' align='center' width='100%'>자세한 사항은 인트라넷>운영관리에서 확인바랍니다.</td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td height='1' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "</table>";
		
		$sMailDocumentBody .= mailfooter();
		
		$this->email->clear(TRUE);
		if( $worktype == "DNS"){
			$this->email->from(MAIL_FROM_INFO, "DNS접수");# 발신
		}else{
			$this->email->from(MAIL_FROM_INFO, "모니터링IP접수");# 발신
		}
		
		//if(ENVIRONMENT== 'development') $tolist = 'yjcin13@kinx.net';
		
		$this->email->to($tolist);# 수신
		$this->email->subject($subject);//제목
		$this->email->message($sMailDocumentBody);//content
		//echo $sMailDocumentBody;
		$this->email->send();
		
		//echo $this->email->print_debugger();
	}
	
	
	public function monitoringip_info()
	{
		$this->load->model("Mykinx_mntripinfo_model");//en삭제
		$this->_set_sec();
		$secParams = $this->_get_sec();
		$secParams["offset"] = isset($secParams["page"]) ? $secParams["page"] : "0";
		$secParams["limit"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		
		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		$secParams["sStatus"] = "Y";
		/*
		 * SP]reCompanyMonitoringIPList(Final인 경우 sStatus 값이 Y,P인 것을 가지고 온다.
		 * 하지만 db에는 Y/N밖에 없음....
		 * (AS-IS)monitoring-info.asp에 'Y'이면 모니터링, 아니면 일시중지로 되어 있는데 Y인것만 가져오면 이렇게 체크할 필요도 없다..
		 */
				
		$secParams["oKey"][0] = "sIp";
		$secParams["oType"][0] = "asc";
		
		$data["cnt"] = $this->Mykinx_mntripinfo_model->_select_cnt($secParams);
		$data["list"] =  $this->Mykinx_mntripinfo_model->_select_list($secParams,0);
		//print_r($data);
		
		$pager["CNT"] = $data["cnt"];
		$pager["PRPAGE"] = isset($secParams["limit"]) ? $secParams["limit"] : $this->cfg["perpage"];
		$pagerHtm = $this->_set_pager($pager);
		
		
		
		$data["pager"] = $pagerHtm;
		$data["pagerIdx"] = $data["cnt"] - $secParams["offset"];
		$data["secParams"] = $secParams;
		
		$this->_print($data);
	}
	
	public function monitoringip_request()//모니터링 ip 등록 view page
	{
		$this->scripts[] = "en/my_mntrip.js";
		$this->load->model("Mykinx_mntripinfo_model");//en삭제
		
		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		//1.등록자 정보 가져오기
		$secParams["oKey"][0] = "sCompanyType";
		$secParams["oType"][0] = "asc";
		$secParams["oKey"][1] = "nSort";
		$secParams["oType"][1] = "asc";
		$secParams["oKey"][2] = "sCompanyName";
		$secParams["oType"][2] = "asc";
		$secParams["oKey"][3] = "sManagerName";
		$secParams["oType"][3] = "asc";
		$data["companycontact"] = $this->Mykinx_mntripinfo_model->_select_list($secParams,1);
		//print_r($data["companycontact"]);
		//echo "<br>-----";
		
		//2. 모니터링IP 리스트 
		unset($secParams["oKey"]);
		unset($secParams["oType"]);
		//$secParams["nStatus"] = "Y";
		$secParams["sStatus"] = "0";
		$data["companymntrIplist"] = $this->Mykinx_mntripinfo_model->getCompanyMntrIPList($secParams);
		//print_r($data["companymntrIplist"]);
		
		$this->_print($data);
	}
	
	public function mntrIpRequest()//ip 등록 신청시 proc.
	{
		$data = $this->input->post(); 
		$data["companycontact"] = decryptIt($data["companycontact"]);//150313
		$this->load->model("Mykinx_mntripinfo_model");//en삭제
		$data["nContractSeq"] = $this->member["nContractSeq"];
		$data["nCompanySeq"] = $this->member["nCompanySeq"];
		//1-1.Service Type 구하기		
		//db에서 추출한 nServiceCode가 1이면 member["sServiceType"]에 1, 1보다 크면 2로 설정
		//1이면 IX, 2이면 IDC로 판별 
		if( $this->member["sServiceType"]==1 )
		{ 
			$data["nServiceType"]= "IX";
		}else
		{
			$data["nServiceType"]= "IDC";
		}
		
		//2.tWorkRequest에 추가
		$data["nWorkRequestSeq"]=$this->Mykinx_mntripinfo_model->InsertWorkRequest($data);
		
		//3. tWorkMonitoringIP 에 추가
		for( $i=0; $i<count($data["nRequestMntrIpIdx"]); $i++)
		{
			unset($secParam);
			$secParam["nWorkRequestSeq"] = $data["nWorkRequestSeq"];
		//if(isset($data["nRequestMntrIpIdx"]))$secParam["nCompanyMonitoringIPSeq"] = $data["nRequestMntrIpIdx"][$i];
			if(isset($data["nRequestMntrIpIdx"][$i]) && $data["nRequestMntrIpIdx"][$i] > 0)
			{	 
				$secParam["nCompanyMonitoringIPSeq"] = $data["nRequestMntrIpIdx"][$i];
			}
			else
			{ 
				$secParam["nCompanyMonitoringIPSeq"] = 0;
			}
			$secParam["nContractSeq"] = $this->member["nContractSeq"];
			$secParam["nCompanySeq"] = $this->member["nCompanySeq"];
			if(isset($data["req_ip"]))$secParam["sUseIp"] = $data["req_ip"][$i];
			if(isset($data["req_cmt"]))$secParam["sContent"] = $data["req_cmt"][$i];
			if(isset($data["worktype"]))$secParam["strWorkType"] = $data["worktype"][$i];
			$this->Mykinx_mntripinfo_model->InsertWorkMntrIp($secParam);
		}
		
		//4. mail 발송
		$this->sendmail_work($this->member["sContractName"],$data["nWorkRequestSeq"],"모니터링IP");
		
		//5.Alert . 페이지 전환
		$this->tinyjs->alert("Your request has been completed. Thank you.");
		$this->tinyjs->pageRedirect("/en/service/monitoringip_info");
		exit; //두번 등록 제거
	}
}