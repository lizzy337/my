<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/en/Common.php");
class Stat extends Common{
	/**
	 * ############ 상세 화면이 보이는 회원들 ######################
	 * 1.     피플마인드 (보보파일)
 	 * 2.     인포제닉 (럭키월드)
	 * infozenic (zenic220)
	 * 3.     에이치제이소프트 (심디스크)
	 * hjsoft (1234)
	 * 4.     에이치투커뮤니케이션즈 (파일팜)
	 * h2comz (1234)
	 * 5.     퍼스트미디어 (조이파일)
	 * joyfile (1234)
	 * 6.     씨앤씨미디어
	 * cncmedia (112233)
	 * 7.     아이네트워크 (메가파일)
	 * megafile (1234)
	 * 8.     사과나무한그루
	 * innovillsoft (1234)
	 * 9.     이노빌소프트
	 * sodisk (eoqkrskwk)
	 * 10.   케이그리드
	 * kgrid (1234)
	 * ############ 상세 화면이 보이는 회원들 ######################
	 */
	var $allowDetail = array("infozenic", "hjsoft", "h2comz", "joyfile","cncmedia", "megafile", "innovillsoft", "sodisk",	"kgrid", "skencar");
	
	public function __construct() {
		parent::__construct();
		if($this->is_login === false) {
            $this->output->set_status_header('410');
			$this->tinyjs->pageRedirect("/en","Sign in to access your account");
            exit;
		}
	}

	public function index() {
		$this->scripts[] = "highcharts/highcharts.js";
		$this->scripts[] = "en/my_stat.js";
		$this->load->model("mrtg_info_model");//en삭제
		$secParams = array();
		$secParams["nContractSeq"] = $this->member["nContractSeq"];
		$mrtg_list = $this->mrtg_info_model->_select_list($secParams);
		  
		$data["mrtg_list_msg"] = "Please select a service.";
		$data["mrtg_list"] = $mrtg_list;
		if(count($mrtg_list)<1) $data["mrtg_list_msg"] = "No registered statistics service has been found."; 
		
     	$this->_print($data);
	}

	private function _set_date($list=array(), $format="Y-m-d"){
		$result = array();
		$korweek = array('SUN','MON','TUE','WED','THU','FRI','SAT');
		foreach($list as $data) {
			$data["date_txt"] = date($format, $data["dtCreateDate"]);
			if($format=='w') $data["date_txt"] = $korweek[$data["date_txt"]];
			$result[] = $data;
		}
		return $result;
	}

	public function get_grph() {
		//$this->output->enable_profiler(TRUE);
		$seq = $this->input->post("nMrtgInfoSeq", TRUE);
		//va('seq : '); va($seq);
		$this->load->model("mrtg_hourview_model");//en삭제
		$this->load->model("mrtg_dayview_model");//en삭제
		$flagDt = $this->input->post("flagDt", TRUE);
		$chkdate = $flagDt." ".date("H:m:i");
		$nowtime = strtotime($chkdate);
		//$nowtime = strtotime('2014-03-29');
                
                //mrtg 정보 추출
                $this->load->model("mrtg_info_model");
                $secParams = array();
		$secParams["nMrtgInfoSeq"] = $seq;                
		$mrtginfo = $this->mrtg_info_model->_select_list($secParams);
                $mrtg_cnt = count($mrtginfo);
                /*
                debug_var("mrtginfo-------------");
                debug_var($mrtg_cnt);
                debug_var($mrtginfo);
                */
                
                if($mrtg_cnt != 1)
                {
                    echo json_encode(array());
                    return TRUE;
                }
                
                //mrtginfo 로 inter_day,month, 등을 대체한다. 190325 by lizzy
                //-start
                $inter_status = 0; //0 - 국내만, 1 - 국제만, 2 - 국내+국제 둘다
                if(strlen($mrtginfo[0]['sMrtgUrl']) > 0 )//국내
                {
                    $inter_status = 0;
                    if(strlen($mrtginfo[0]['sMrtgUrl2']) > 0 )
                    {
                        $inter_status = 2;
                    }
                }
                else
                {
                    if(strlen($mrtginfo[0]['sMrtgUrl2']) > 0 ) $inter_status = 1;
                }
                //-end
		
		// 5분단위 데이터를 추출 합니다
		$params["stimestamp"] = strtotime($flagDt." 00:00:00");#$nowtime - 86400;
		$params["etimestamp"] = $nowtime;
		$params["sdate"] = $flagDt." 00:00:00";
		$params["nMrtgInfoSeq"] = $seq;
		$min_list = $this->_set_date($this->mrtg_hourview_model->_select_min_list($params), "H:i");
		
		// 일간 데이터를 추출 합니다
		$day_list = $this->_set_date($this->mrtg_hourview_model->_select_list($params), "H");
		unset($params);
		
		
		
		// 일주일 데이터이기 때문에 7일치 		
		$chkmonday = date('w', $nowtime);
		//$params["stimestamp"] = strtotime($chkdate." -168 hour"); 
		$params["stimestamp"] = strtotime($chkdate." -".$chkmonday." day");
		$params["etimestamp"] = strtotime($chkdate." +".(7-$chkmonday)." day");
		$params["sdate"] = date("Y-m-d H:i:s", strtotime($flagDt." 00:00:00"." -".($chkmonday-1)." day"));
		$params["interday"] = 7;
		$params["nMrtgInfoSeq"] = $seq;
		$week_list = $this->_set_date($this->mrtg_dayview_model->_select_list($params), "w");
		unset($params);
		
		
		// 1달간 데이터는 30일치를 가져옵시다.
		$params["stimestamp"] = strtotime(substr($flagDt,0,-2)."01 00:00:00");#$nowtime - (86400*30);
		$params["etimestamp"] = $nowtime;
		$params["sdate"] = substr($flagDt,0,-2)."01 00:00:00";
		$params["interday"] = date("t", strtotime(substr($flagDt,0,-2)."01 00:00:00"));
		$params["nMrtgInfoSeq"] = $seq;
		$month_list = $this->_set_date($this->mrtg_dayview_model->_select_list($params), "d");
		unset($params);
		


		$response = array();
		
                //국내 or 국제 or 국내+국제
                $response['inter_status'] = $inter_status;
                
		//국제트래픽 여부
		$response["inter_day"] = 0;
		$response["inter_week"] = 0;
		$response["inter_month"] = 0;
		
		$response["min"] = $min_list;
		$response["day"] = $day_list;
		$response["week"] = $week_list;
		$response["month"] = $month_list;
		$response["daySum"] = array("nVolume"=>0,"nTrafficOut_now"=>0,"nTrafficOutMax"=>0,"nTrafficOutAvg"=>0,"nTrafficIn_now"=>0,"nTrafficInMax"=>0);
		$response["weekSum"] = array("nVolume"=>0,"nTrafficOut_now"=>0,"nTrafficOutMax"=>0,"nTrafficOutAvg"=>0,"nTrafficIn_now"=>0,"nTrafficInMax"=>0);
		$response["monthSum"] = array("nVolume"=>0,"nTrafficOut_now"=>0,"nTrafficOutMax"=>0,"nTrafficOutAvg"=>0,"nTrafficIn_now"=>0,"nTrafficInMax"=>0);
		
		foreach($day_list as $row)
		{
			if($response["inter_day"]==0 && $row["nTrafficInMax2"]>0) $response["inter_day"]=1;
			
			$response["daySum"]['nVolume'] += $row['nVolume'];
			$response["daySum"]['nTrafficOut_now'] += $row['nTrafficOut_now'];
			$response["daySum"]['nTrafficOutMax'] += $row['nTrafficOutMax'];
			$response["daySum"]['nTrafficOutAvg'] += $row['nTrafficOutAvg'];
			$response["daySum"]['nTrafficIn_now'] += $row['nTrafficIn_now'];
			$response["daySum"]['nTrafficInMax'] += $row['nTrafficInMax'];
		} 
		$response["daySum"]['dtCreateDate'] = $row['dtCreateDate'];
		unset($row);
		
		$response["weekSum"]['dtStartDate']=NULL;
		foreach($week_list as $row)
		{
			if($response["inter_week"]==0 && $row["nTrafficInMax2"]>0) $response["inter_week"]=1;
			if(is_null($response["weekSum"]['dtStartDate'])) $response["weekSum"]['dtStartDate']=$row['dtCreateDate'];
			$response["weekSum"]['nVolume'] += $row['nVolume'];
			$response["weekSum"]['nTrafficOut_now'] += $row['nTrafficOut_now'];
			$response["weekSum"]['nTrafficOutMax'] += $row['nTrafficOutMax'];
			$response["weekSum"]['nTrafficOutAvg'] += $row['nTrafficOutAvg'];
			$response["weekSum"]['nTrafficIn_now'] += $row['nTrafficIn_now'];
			$response["weekSum"]['nTrafficInMax'] += $row['nTrafficInMax'];
		}
		$response["weekSum"]['dtEndDate'] = $row['dtCreateDate'];
		unset($row);
		
		$response["monthSum"]['dtStartDate']=NULL;
		foreach($month_list as $key=>$row)
		{
			if($response["inter_month"]==0 && $row["nTrafficInMax2"]>0) $response["inter_month"]=1;
			
			if($row["nTrafficInMax"]=='') $month_list[$key]['nTrafficInMax']='null';
			
			if(is_null($response["monthSum"]['dtStartDate'])) $response["monthSum"]['dtStartDate']=$row['dtCreateDate'];
			$response["monthSum"]['nVolume'] += $row['nVolume'];
			$response["monthSum"]['nTrafficOut_now'] += $row['nTrafficOut_now'];
			$response["monthSum"]['nTrafficOutMax'] += $row['nTrafficOutMax'];
			$response["monthSum"]['nTrafficOutAvg'] += $row['nTrafficOutAvg'];
			$response["monthSum"]['nTrafficIn_now'] += $row['nTrafficIn_now'];
			$response["monthSum"]['nTrafficInMax'] += $row['nTrafficInMax'];
		}
		$response["monthSum"]['dtEndDate'] = $row['dtCreateDate'];
		
// 				debug_var($month_list);
// 				die;
		
		
		$response["html"] = false;
		if(in_array($this->member["sMemberId"], $this->allowDetail)) {
			$this->template->define("daySum_table", "/en/stat/detaildaySum.html");
			$this->template->define("weekSum_table", "/en/stat/detailweekSum.html");
			$this->template->define("monthSum_table", "/en/stat/detailmonthSum.html");
			$this->template->define("day_table", "/en/stat/detailday.html");
			$this->template->define("week_table", "/en/stat/detailweek.html");
			$this->template->define("month_table", "/en/stat/detailmonth.html");
			$this->template->assign($response); 
			
			$response["html"]["day"] = $this->template->fetch("day_table");
			$response["html"]["week"] = $this->template->fetch("week_table");
			$response["html"]["month"] = $this->template->fetch("month_table");
			$response["html"]["daySum"] = $this->template->fetch("daySum_table");
			$response["html"]["weekSum"] = $this->template->fetch("weekSum_table");
			$response["html"]["monthSum"] = $this->template->fetch("monthSum_table");
		} 
		echo json_encode($response); 
		return TRUE;
	}
}