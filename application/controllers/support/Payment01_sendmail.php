<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//class Send_payment01 extends CI_Controller
require_once(APPPATH ."controllers/Common.php");
class Payment01_sendmail extends Common
{
	#생성자
	function __construct()
	{
		parent::__construct();
		$this->load->library('email');
		$this->load->helper('email');

		# 메세지 설정
		$this->msg = (object) (object) array(
		'error1'	=> '잘못된 접근입니다. Error:[code:CP001]',
		'error2'	=> '잘못된 접근입니다. Error:[code:CP002]',
		'error3'	=> '잘못된 접근입니다. Error:[code:CP003]',
		'error4'	=> '잘못된 접근입니다. Error:[code:CP004]',
		'error5'	=> '잘못된 접근입니다. Error:[code:CP005]',
		'error6'	=> '기본정보가 확인되지 않습니다.. Error:[code:CP006]',
		'msg1'		=> '정상적으로 등록되었습니다.',
		'msg2'		=> '정상적으로 변경되었습니다.',
		);
	}
	
	public function send_old()
	{
		$data = $this->input->post();
		
		//-start 자동입력방지 관련
		if($_POST['g-recaptcha-response'])
		{
		    $recaptcha = $_POST['g-recaptcha-response'];
		}
		
		if(!$recaptcha)
		{
		    $decoded= array("success"=>false,"message"=>"로봇이 아니면 체크해주세요.");
		    //echo(json_encode($decoded));
		    $this->tinyjs->alert("로봇이 아니면 체크해주세요.");
		    $this->tinyjs->pageRedirect("/support/payment/payment01");
		    exit;
		}
		else
		{
		     
		    $ip = $_SERVER['REMOTE_ADDR'];
		    //print_r("ip "); print_r($ip); die;
		     
		    $secretKey = $_POST['secretkey'];
		     
		    $url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$recaptcha."&remoteip=".$ip;
		    //print_r("url "); print_r($url);
		     
		    $resource =  file_get_contents( $url );
		    $response = json_decode($resource, true);
		    // 		    print_r("response "); print_r($response);
		    // 		    die;
		     
		    if(intval($response["success"]) !== 1)
		    {
		        $errorMessages = array(
		                'missing-input-secret' => 'The secret parameter is missing.',
		                'invalid-input-secret' => 'The secret parameter is invalid or malformed.',
		                'missing-input-response' => 'The response parameter is missing.',
		                'invalid-input-response' => 'The response parameter is invalid or malformed.',
		        );
		        if( isset($response["error-codes"][0]) )
		            $msg = ($response["error-codes"][0])? $errorMessages[$response["error-codes"][0]] : "정상적인 접속이 아닌 것으로 판단됩니다." ;
		            else
		                $msg = "정상적인 접속이 아닌 것으로 판단됩니다." ;
		                $decoded= array("success"=>false,"message"=>$msg);
		                //echo(json_encode($decoded));
		                $this->tinyjs->alert($msg);
		                $this->tinyjs->pageRedirect("/support/payment/payment01");
		                exit;
		    }
		}
		//-end
	
		if( !isset($data) ||  count($data)<=0 )//빈 값이 넘어와서 메일이 발송되는 경우가 있음
		{
			return;
		}
		$sCompanyName = $data['sCompanyName'];
		$sDepositName = $data['sDepositName'];
		$nDepositYear = $data['nDepositYear'];
		$nDepositMonth = $data['nDepositMonth'];
		$nDepositDay = $data['nDepositDay'];
		$sPaymentComment = $data['sPaymentComment'];
		$nPaymentType = $data['nPaymentType'];
		$nPaymentPrice = $data['nPaymentPrice'];
		$sEmail = $data['sEmail'];
		$sPhone = $data['sPhone'];
	
	
		$sDepositDate = $nDepositYear."-".$nDepositMonth."-".$nDepositDay;
	
		//보내는 사람 - MAIL_FROM_INFO
	
		//받는 사람
		$to_name = "결제 담당자";
		$to_mail = MAIL_TO_PAYMENT;
		//제목
		$subject = "[입금확인 요청]".$sCompanyName."의 ".$sDepositName."고객님께서 입금확인(요금문의)요청을 하셨습니다.";
	
		$fontStyle = " style='font-family: dotum, gulim, Arial, sans-serif; font-size: 12px; line-height: 20px; margin: 0; padding: 0;' ";
	
		$sMailDocumentBody	= "";
		$sMailDocumentBody	= mailheader();
		$sMailDocumentBody .= "<p ".$fontStyle."><b>아래와 같이 입금확인 및 요금관련 문의가 접수되었습니다.</b></p>";
		$sMailDocumentBody .= "<p ".$fontStyle."><b>확인후 연락 부탁드립니다.</b></p><br>";
	
		$sMailDocumentBody .= "<table style='width:630px;height:10% !important;vertical-align:middle;border-collapse:collapse'>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td width='30%' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= " <td width='70%' bgcolor='#E8E8E8'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody .= "  <td width='30%' height='25' ".$fontStyle.">업체명</td>";
		$sMailDocumentBody .= "  <td width='70%' ".$fontStyle.">".$sCompanyName."</td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8' colspan='2'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody .= "  <td width='30%' height='25' ".$fontStyle.">입금자명</td>";
		$sMailDocumentBody .= "  <td width='70%' ".$fontStyle.">".$sDepositName."</td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8' colspan='2'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody .= "  <td width='30%' height='25' ".$fontStyle.">입금일</td>";
		$sMailDocumentBody .= "  <td width='70%' ".$fontStyle.">".$sDepositDate."</td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8' colspan='2'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody .= "  <td width='30%' height='25' ".$fontStyle.">결제사유</td>";
		$sMailDocumentBody .= "  <td width='70%' ".$fontStyle.">".$sPaymentComment."</td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8' colspan='2'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody .= "  <td width='30%' height='25' ".$fontStyle.">결제유형</td>";
		$sMailDocumentBody .= "  <td width='70%' ".$fontStyle.">".$nPaymentType."</td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8' colspan='2'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody .= "  <td width='30%' height='25' ".$fontStyle.">납부금액</td>";
		$sMailDocumentBody .= "  <td width='70%' ".$fontStyle.">".number_format($nPaymentPrice)."</td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8' colspan='2'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody .= "  <td width='30%' height='25' ".$fontStyle.">이메일</td>";
		$sMailDocumentBody .= "  <td width='70%' ".$fontStyle.">".$sEmail."</td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8' colspan='2'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody .= "  <td width='30%' height='25' ".$fontStyle.">휴대폰번호</td>";
		$sMailDocumentBody .= "  <td width='70%' ".$fontStyle.">".$sPhone."</td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody	.= "<tr>";
		$sMailDocumentBody	.= " <td bgcolor='#E8E8E8' colspan='2'></td>";
		$sMailDocumentBody	.= "</tr>";
		$sMailDocumentBody .= "</table>";
		$sMailDocumentBody .= mailfooter();
		$this->email->clear(TRUE);
		$this->email->from(MAIL_FROM_INFO, "결제안내");# 발신
		$this->email->to($to_mail);# 수신
		$this->email->cc('lizzy337@kinx.net');//참조
		//$this->email->bcc();	# 숨은참조
		$this->email->subject($subject);//제목
		$this->email->message($sMailDocumentBody);//content
	
		//echo $sMailDocumentBody;
			
		$rtn = $this->email->send();
	
		$alertMsg = "문의 내용이 저장되었습니다. 담당자 확인 후 빠르게 처리해드리겠습니다.";
		$this->tinyjs->alert($alertMsg);
		$this->tinyjs->pageRedirect("/support/payment/payment01");
	}
	
	public function send(){
		$data = $this->input->post(); 
		
		if( !isset($data) ||  count($data)<=0 )//빈 값이 넘어와서 메일이 발송되는 경우가 있음
		{
			return;
		}
		$sCompanyName = $data['sCompanyName'];
		$sDepositName = $data['sDepositName'];
		$nDepositYear = $data['nDepositYear'];
		$nDepositMonth = $data['nDepositMonth'];
		$nDepositDay = $data['nDepositDay'];
		$sPaymentComment = $data['sPaymentComment'];
		$nPaymentType = $data['nPaymentType'];
		$nPaymentPrice = $data['nPaymentPrice'];
		$sEmail = $data['sEmail'];
		$sPhone = $data['sPhone'];
		
		
		$sDepositDate = $nDepositYear."-".$nDepositMonth."-".$nDepositDay;
		
		//보내는 사람 - MAIL_FROM_INFO
		
		//받는 사람
		//$to_name = "결제 담당자";
		$to_mail = MAIL_TO_PAYMENT;
		
		//메일제목 설정
		$title = $sCompanyName."의 ".$sDepositName."님의 입금확인(요금문의)이 접수되었습니다.";
		$mailtitle = "[알림]".$title;
		
		//메일 본문 내용
		$sMailDocumentBody	= '';
		
		$sMailDocumentBody = '
				<!-- 본문 내용 -->
			    <table border="0" cellpadding="0" cellspacing="0" width="640">
			   <tr>
			    <td><font style="font-family:맑은 고딕,돋움; font-size:13px;">
			    안녕하세요.<br />
			    인터넷 인프라 전문기업 ㈜케이아이엔엑스입니다.<br />
			    무통장 입금 확인 및 요금관련 문의가 접수되었습니다.<br />
    			확인 후 연락 부탁드립니다.</font><br /><br />';
		
		$sMailDocumentBody.='
				    <table border="2" cellpadding="6" cellspacing="0" width="640" bordercolor="#222222" bordercolordark="#222222" bordercolorlight="#222222"  style="border-collapse:collapse;">
				     <tr>
				      <td width="120" bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:13px;"><b>업체명</b></font></td>
				      <td width="520"><font style="font-family:맑은 고딕,돋움; font-size:13px;">'.$sCompanyName.'</font></td>
				     </tr>
				     <tr>
				      <td bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:13px;"><b>입금자명</b></font></td>
				      <td><font style="font-family:맑은 고딕,돋움; font-size:13px;">'.$sDepositName.'</font></td>
				     </tr>
				     <tr>
				      <td bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:13px;"><b>입금일</b></font></td>
				      <td><font style="font-family:맑은 고딕,돋움; font-size:13px;">'.$sDepositDate.'</font></td>
				     </tr>
				     <tr>
				      <td bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:13px;"><b>결제사유</b></font></td>
				      <td><font style="font-family:맑은 고딕,돋움; font-size:13px;">'.$sPaymentComment.'</font></td>
				     </tr>
				     <tr>
				      <td bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:13px;"><b>결제유형</b></font></td>
				      <td><font style="font-family:맑은 고딕,돋움; font-size:13px;">'.$nPaymentType.'</font></td>
				     </tr>
				     <tr>
				      <td bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:13px;"><b>납부금액</b></font></td>
				      <td><font style="font-family:맑은 고딕,돋움; font-size:13px;">'.number_format($nPaymentPrice).'</font></td>
				     </tr>
				     <tr>
				      <td bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:13px;"><b>이메일</b></font></td>
				      <td><font style="font-family:맑은 고딕,돋움; font-size:13px;"><a href="mailto:'.$sEmail.'">'.$sEmail.'</a></font></td>
				     <tr>
				      <td bgcolor="#eeeeee" align="center"><font style="font-family:맑은 고딕,돋움; font-size:13px;"><b>휴대폰번호</b></font></td>
				      <td><font style="font-family:맑은 고딕,돋움; font-size:13px;">'.$sPhone.'</font></td>
				     </tr>
				     </tr>
				    </table><br/>
				';
		
		$sMailDocumentBody .= '</td></tr></table><br><br>';
		
		$data['bcc'] = "lizzy337@kinx.net";
		$data['title'] = $title; //내용의 제목
		$data['message'] = $sMailDocumentBody;
		$data['fromemail'] = EMAIL_HELP;
		$data['fromname'] = EMAIL_SENDMAIL_NAME;
		$data['to'] = $to_mail;
		$data['subject'] = $mailtitle; //메일 제목
		$data['footertype'] = 1;//발신전용
		$data['inout'] = 'in';//in or out // out이면 외부, 고객 발신, 고객센터 내용 추가되어야 함.
		$status = kinxMailSendFnc($data,FALSE);
		
		$alertMsg = "문의 내용이 저장되었습니다. 담당자 확인 후 빠르게 처리해드리겠습니다.";
		$this->tinyjs->alert($alertMsg);
		$this->tinyjs->pageRedirect("/support/payment/payment01");
		
		
	}
	
	
}



  