<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH ."controllers/Common.php");
class Payment extends Common
{
    function __construct()
    {

        parent::__construct();


        $this->load->model("payment_ism_model","md_payment");

    }

    public function index()
    {
        redirect('errors/error404');

        // $LGD_CASFLAG = "I";
        // $LGD_PRODUCTINFO = "2018-11서버호스팅_테스트";
        // $LGD_BUYER = "이진영";
        // $LGD_OID = "tkinx2018120412345678911";
        // $LGD_AMOUNT = "220";
        // $LGD_FINANCENAME = "신한";
        // $LGD_ACCOUNTNUM = "X1234567891234";
        // $LGD_PAYTYPE = "SC0040";
        // $LGD_RESPCODE = "0000";
        // $this->payment02_casnote_sendmail_test($LGD_CASFLAG,
        //                 $LGD_PRODUCTINFO,$LGD_BUYER,$LGD_OID,
        //                 $LGD_AMOUNT,$LGD_FINANCENAME,$LGD_ACCOUNTNUM,$LGD_PAYTYPE,$LGD_RESPCODE);

    }


    /**
     * 결제안내 - 무통장입금 탭 화면
     */
    Public Function payment01()
    {
        $data = null;
        $this->_print($data);
    }


    /**
     * 결제안내 - 신용카드 탭 화면
     */
    Public Function payment02($result=NULL)
    {
        # 기본 정보 설정 set data------------------------------------------------------------------
        $data = null;
        for($i=0;$i<=49;$i++)
        {
            $data['amount_list'][]= ["amount_str" => number_format($i*100000), "amount_num" => $i*100000];
        }


        # 카드 결제창 확인 완료일 경우--------------------------------------------------------------
        if($result=='success')
        {
            $getdata = $this->input->get(NULL, TRUE);

            $this->load->library('tosscard');
            $data['charge_result'] = $this->tosscard->run_charge($getdata);
        }
        elseif($result=='fail')
        {
            $getdata = $this->input->get(NULL, TRUE);
            $data['charge_result'] = [
                'sResultCode'    => $getdata['code'],
                'sResultMessage' => $getdata['message']
            ];
        }
        # 잘못된 접근
        elseif(!is_null($result))
        {
            redirect('errors/error404');
        }
        # 초기화면 view
        else
        {
            $data['charge_result'] = FALSE;
            # load script
            $this->scripts = ['https://js.tosspayments.com/v1','tosspayment.js','support/msgelement.js','support/payment.js'];
        }


        # load view--------------------------------------------------------------------------------
        $this->_print($data);
    }




    /**
     * 결제안내 - CMS 자동이체
     */
    Public Function payment03()
    {
        $nav[] = array("link" => "/support/", "title" => "기술지원");
        $nav[] = array("link" => "/support/payment", "title" => "결제 안내");
        $data["nav"] = $nav;
        $this->_print($data);
    }


    Public Function payment03_doc_down()
    {
        $this->load->helper('download');
        //$data = file_get_contents("/data/projects/kinx.net/www/doc/".CMS_REQUEST_FILE);//org
        //171107 변경. 물리서버 my 와 MOT 의 my 경로 체크.
        $ret = file_exists("/data/projects/kinx.net/www/doc/".CMS_REQUEST_FILE);
        if($ret === TRUE)
        {
            $data = file_get_contents("/data/projects/kinx.net/www/doc/".CMS_REQUEST_FILE);
        }
        else
        {
            $data = file_get_contents("/kinx/projects/kinx.net/doc/".CMS_REQUEST_FILE);
        }

        $name = CMS_REQUEST_FILE;
        force_download(mb_convert_encoding($name, 'euc-kr', 'utf-8'), $data);
    }


    //결제 완료후 입금관련처리 API 호출. 181016
    //221017 확인 결과 해당 함수 부르는 곳이 없음.
    public function set_payments($xpay)
    {
        //va("set_payments xpay ");va($xpay);

        $skinds = 1;
        $paytype = $xpay->Response("LGD_PAYTYPE",0);
        switch($paytype)
        {
        case "SC0010" : //카드
            $skinds = 2;
            break;
        case "SC0030" : //계좌이체
            $skinds = 1;
            break;
        case "SC0040" : //무통장입금/가상계좌
            $skinds = 6;
            break;
        //case "SC0060" : //휴대폰 결제 => LG에서 제공 안함
        //    break;
        }
        $price = $xpay->Response("LGD_AMOUNT",0);
        $income_name = $xpay->Response("LGD_BUYER",0);
        $income_place = $xpay->Response("LGD_MID",0);

        $income_deliveryinfo = $xpay->Response("LGD_DELIVERYINFO",0);

        //기본 내용
        $Param = array(
            "skinds" =>$skinds,
            "price" => $price,
            "income_name" => $income_name."(".$income_deliveryinfo.")",
            "income_place" => $income_place
        );

        //카드 승인
        $auth_code = "";
        if($paytype == "SC0010")//카드결제
        {
        $auth_code = $xpay->Response("LGD_FINANCEAUTHNUM",0);//카드승인번호
        $Param["auth_code"] = $auth_code;
        }


        $nBillMonthSeq = $xpay->Response("LGD_PRODUCTCODE",0); //있으면 MY 로그인 후 카드결제(청구서결제). 없으면 일반결제
        if( !empty($nBillMonthSeq)) //청구서 결제. AccountSeq 구하기
        {
        $nAccountSeq = $this->session->userdata("nContractSeq");

        $Param["account_seq"] = $nAccountSeq;
        $Param["billmonth_seq"] = $nBillMonthSeq;
        }

        //va("xapi param ");va($Param);

        $this->load->config('kinxconfig',true);
        $api = $this->config->item('xapi_host', 'kinxconfig');

        $this->load->library('rest');
        $this->rest->initialize(array('server'=>$api,"api_key"=>EMPAPI_KEY));
        //$this->rest->http_header('content-type','application/json;charset=UTF-8');
        //$this->rest->option(CURLOPT_TIMEOUT,100);
        $url = "payments/recepits";
        //va('param');va($Param);
        $retStdClass = $this->rest->post($url, $Param);
        //va('retStdClass');va($retStdClass);
        //$this->rest->debug();

    }



    function defaultsetdata()
    {
        $type = mt_rand(1,2);
        $json = [
            'code1' => ($type==1) ? TOSS_01_CLIENT : TOSS_SALT_STR,
            'code2' => ($type==1) ? TOSS_SALT_STR : TOSS_01_CLIENT,
            'type'  => $type
        ];

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($json));
    }



}