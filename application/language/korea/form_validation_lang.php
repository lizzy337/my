<?php

$lang['required']			= "[ %s ] 필수 입력 항목입니다.";
$lang['isset']				= "[ %s ] 입력하신 내용이 확인되지 않습니다.";
$lang['valid_email']		= "[ %s ] 정확한 이메일주소를 입력해 주세요.";
$lang['valid_emails']		= "[ %s ] 정확한 이메일 주소를 입력해 주세요.";
$lang['valid_url']			= "[ %s ] 정확한 URL주소를 입력해 주세요.";
$lang['valid_ip']			= "[ %s ] 정확한 IP주소를 입력해 주세요.";
$lang['min_length']			= "[ %s ] 최소 %s 자 이상 입력해 주세요.";
$lang['max_length']			= "[ %s ] 최대 %s 자 이상 입력하실 수 없습니다.";
$lang['exact_length']		= "[ %s ] 입력한하신 내용이 %s 와 일치하지 않습니다.";
$lang['alpha']				= "[ %s ] 영문만 입력하실 수 있습니다.";
$lang['alpha_numeric']		= "[ %s ] 영문 + 숫자만 입력하실 수 있습니다.";
$lang['alpha_dash']			= "[ %s ] 영문 + 숫자, _, - 만 입력하실 수 있습니다.";
$lang['numeric']			= "[ %s ] 숫자만 입력하실 수 있습니다.";
$lang['is_numeric']			= "[ %s ] 숫자만 입력하실 수 있습니다.";
$lang['integer']			= "[ %s ] 정수값만 입력하실 수 있습니다.";
$lang['regex_match']		= "[ %s ] 올바른형식이 아닙니다.";
$lang['matches']			= "[ %s ], [ %s ] 두 값이 일치하지 않습니다.";
$lang['is_unique'] 			= "[ %s ] 고유한 값을 포함해야합니다.";
$lang['is_natural']			= "[ %s ] 자연수만 입력하실 수 있습니다.";
$lang['is_natural_no_zero']	= "[ %s ] 1이상의 자연수만 입력하실 수 있습니다.";
$lang['decimal']			= "[ %s ] 진수를 포함해야합니다.";
$lang['less_than']			= "[ %s ] 검사대상이 [ %s ] 값보다 적은 숫자만 입력하실 수 있습니다.";
$lang['greater_than']		= "[ %s ] 검사대상이 [ %s ] 값보다 큰 숫자만 입력하실 수 있습니다.";
$lang['seperate_count_max']		= "[ %s ] 최대 %s 개 까지 입력하실 수 있습니다.";
$lang['less_than_or_equal']		= "[ %s ] 검사대상이 [ %s ] 이하의 숫자만 입력하실 수 있습니다.";

/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */