<?php
$lang['send_email_title_order_question'] = '고객 상담이 등록되었습니다.';
$lang['send_email_content_order_question'] = '주문상품 [%s] 의 [%s] 문의가 등록 되었습니다.';

$lang['send_email_title_change_status'] = '상품의 전시 상태가 변경되었습니다.';
$lang['send_email_content_change_status'] = '[%s] 상품의 전시 상태가 [%s] (으)로 변경되었습니다.';

$lang['send_email_title_change_apk_status'] = 'APK의 전시 상태가 변경되었습니다.';
$lang['send_email_content_change_apk_status'] = '[%s] 상품의 APK 전시 상태가 [%s] (으)로 변경되었습니다.';

/* End of file send_email_lang.php */
/* Location: ./system/language/korea/send_email_lang.php */