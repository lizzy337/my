<?php

$lang['upload_userfile_not_set'] = "userfile라는 게시물 변수를 찾을 수 없습니다.";
$lang['upload_file_exceeds_limit'] = "업로드 된 파일은 PHP 설정 파일에 허용되는 최대 크기를 초과했습니다.";
$lang['upload_file_exceeds_form_limit'] = "업로드 한 파일이 제출 양식에 의해 허용되는 최대 크기를 초과했습니다.";
$lang['upload_file_partial'] = "파일이 부분적으로 만 업로드되었습니다.";
$lang['upload_no_temp_directory'] = "임시 폴더가 없습니다.";
$lang['upload_unable_to_write_file'] = "이 파일은 디스크에 기록 할 수 없습니다.";
$lang['upload_stopped_by_extension'] = "이 파일은 업로드 확장자에 의해 중지되었습니다.";
$lang['upload_no_file_selected'] = "업로드 할 파일을 선택하지 않았습니다.";
$lang['upload_invalid_filetype'] = "당신이 업로드하려는 파일 형식은 허용되지 않습니다.";
$lang['upload_invalid_filesize'] = "당신이 업로드하려는 파일이 허용 크기보다 큽니다.";
$lang['upload_invalid_dimensions'] = "당신이 업로드하려는 이미지가 최대 높이 또는 너비를 초과합니다.";
$lang['upload_destination_error'] = "최종 목적지에 업로드 된 파일을 이동하는 동안 문제가 발생했습니다.";
$lang['upload_no_filepath'] = "업로드 경로는 유효하지 않습니다.";
$lang['upload_no_file_types'] = "귀하는 허용 된 파일 형식을 지정하지 않았습니다.";
$lang['upload_bad_filename'] = "귀하가 제출 파일 이름은 서버에 이미 존재합니다.";
$lang['upload_not_writable'] = "업로드 대상 폴더에 쓰기 권한이 있어야 표시되지 않습니다.";


/* End of file upload_lang.php */
/* Location: ./system/language/english/upload_lang.php */