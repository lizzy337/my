<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Tosscard
 *
 * @subpackage      Libraries
 * @category        Libraries
 * @author          KINX TA_unit SYZ
 * @created         27/06/2020
 * @dscription      toss payments 시스템을 통해 카드자동결제 및 조회 기능 구현을 위한 libraries.
 * @link            https://docs.tosspayments.com/guides/windows/card //toss payments docs
 */

class Tosscard {
    protected $CI        = NULL;
    protected $tosshost  = NULL;
    protected $xapihost  = NULL;
    protected $err_msg   = NULL;
    protected $err_code  = NULL;
    protected $mid       = NULL;

    protected $t_billmonth     = 'BILL.tBillMonth';
    protected $t_xpayapproval  = 'BILL.tXpayApproval';
    protected $t_oapp_detail   = 'BILL.tOlineApprovalDetail';
    protected $t_oapp_customer = 'BILL.tOlineApprovalCustomer';


    public function __construct($mid ='kinx01')
    {
        $this->CI =& get_instance();
        $this->CI->load->config('kinxconfig');

        $config = $this->CI->config->config;
        $this->tosshost = $config['toss_host'];
        $this->xapihost = $config['xapi_host'];
        $this->mid = $mid;

        $ismdb = $this->CI->load->database('ism',true);
        $this->ismdb = $ismdb;
    }


    public function run_charge($card_data)
    {
        # 데이터 설정
        if(!$postdata = $this->set_data($card_data))
        {
            $rtn['sResultCode']    = $this->err_code;
            $rtn['sResultMessage'] = $this->err_msg;
            return (object)$rtn;
        }

        # 카드승인 요청
        $authkey  = "Basic ".base64_encode(TOSS_SECURITY_KEY.":");//SECURITY 키 암호화
        $url      = "v1/payments/".$card_data['paymentKey'];//카드결제 승인요청 URL
        $this->CI->load->library('rest');
        $this->CI->rest->initialize(array('server'=>$this->tosshost));
        $this->CI->rest->http_header('Content-Type','application/json');
        $this->CI->rest->http_header('Authorization', $authkey);
        $rest_result = $this->CI->rest->post($url, json_encode($postdata));



        # myKINX 카드 등록 정보 추출
        $oapp_cust_data = $this->get_olineapproval_data('oapp_cust_cd', $card_data['oapp_cust_cd']);
        $oapp_dtl_data  = $this->get_olineapproval_data('oapp_dtl_cd', $card_data['oapp_dtl_cd']);

        $card_data['sBuyerName']    = $oapp_cust_data->sName;
        $card_data['sBuyerPhone']   = $oapp_cust_data->sPhone;
        $card_data['sBuyerEmail']   = $oapp_cust_data->sEmail;
        $card_data['sDeliveryInfo'] = $oapp_cust_data->sCustomerName;
        $card_data['sProductInfo']  = $oapp_dtl_data->sServiceName;

        $card_data['nOlineApprovalCustomerSeq'] = $oapp_cust_data->nOlineApprovalCustomerSeq;
        $card_data['nOlineApprovalDetailSeq']   = $oapp_dtl_data->nOlineApprovalDetailSeq;

        // va($card_data);
        // va($postdata);
        // va($rest_result);
        // va($oapp_cust_data);
        // va($oapp_dtl_data);
        // $this->CI->rest->debug();
        // die;


        if($this->CI->rest->status() == 200)
        {
            if($rtn = $this->set_xpayapproval_data($rest_result, $card_data))
            {
                # 사업G 알람Email 발송
                $this->_send_email($rtn);

                # 카드결제 거래내역 등록
                //$this->_send_recepits($rtn);
                $this->_send_recepits_new($rtn);//221018

                $rtn = (array)$rtn;
            }
            else
            {
                $rtn['sResultCode']    = 500;
                $rtn['sResultMessage'] = $this->err_msg;
            }
        }
        else
        {
            $rtn['sResultCode']    = $rest_result->code;
            $rtn['sResultMessage'] = $rest_result->message;
        }

        return $rtn;
    }


    public function set_data($card_data)
    {
        # post 정보 확인
        if(!chk_element('amount', $card_data))
        {
            $this->err_code = 'GET001';
            $this->err_msg  = '청구금액이 확인 되지 않습니다.';
            return FALSE;
        }
        if(!chk_element('orderId', $card_data))
        {
            $this->err_code = 'GET002';
            $this->err_msg  = '주문ID 정보가 확인 되지 않습니다.';
            return FALSE;
        }
        if(!chk_element('paymentKey', $card_data))
        {
            $this->err_code = 'GET003';
            $this->err_msg  = '카드결제key정보가 확인 되지 않습니다.';
            return FALSE;
        }


        $getdata = [
            'amount'  => $card_data['amount'],
            'orderId' => $card_data['orderId']
        ];

        return $getdata;
    }



    function set_xpayapproval_data($rest_result, $card_data)
    {
        $result = (array)$rest_result;
        $card   = (array)element('card',$result, []);
        $status = element('status', $result, "");
        $status = $status=='DONE' ? '0000':$status;

        $buyerid   = (isset($card_data['buyerid']))    ? $card_data['buyerid']    : '';
        $productcd = (isset($card_data['product_cd'])) ? $card_data['product_cd'] : '';

        $xpay_data = array(
            "sOrderNumber"         => element('orderId',     $result, ""), //kinx 거래번호(주문번호)
            "sUplusId"             => $this->mid,
            "sUplusOrderNumber"    => "toss",
            "sResultCode"          => $status, //응답코드(0000:성공)
            "sResultMessage"       => $this->get_status_kor(element('status', $result, 'READY')), //응답메시지(ex:결제성공)
            "sPaymentType"         => 'SC0010', //결제수단(신용카드:SC0010, 계좌이체:SC0030 ...)
            "nAmount"              => element('totalAmount', $result, ""), //결제금액
            "dtPayDate"            => substr(str_replace(["-",":"," ","T"],["","","",""],element('requestedAt', $result, "")),0,14), //결제일시
            "sBuyerSsn"            => '', //구매자 주민등록번호
            "sBuyerName"           => element('sBuyerName',           $card_data, ""), //구매자
            "sBuyerPhone"          => element('sBuyerPhone',          $card_data, ""), //구매자휴대폰번호
            "sBuyerEmail"          => element('sBuyerEmail',          $card_data, ""), //구매자이메일
            "sBuyerId"             => $buyerid, //구매자아이디(nAccountSeq)
            "sDeliveryInfo"        => element('sDeliveryInfo',        $card_data, ""), //배송정보
            "nProductCode"         => $productcd, //상품코드(nBillMonthSeq)
            "sProductInfo"         => element('sProductInfo',         $card_data, ""), //상품정보
            "sBuyerAddress"        => element('receiptUrl',           $card,      ""), //toss 매출전표 URL
            "sCardNumber"          => element('number',               $card,      ""), //신용카드번호
            "sCardInstalmentMonth" => element('installmentPlanMonths',$card,      ""), //신용카드할부개월
            "sCardNointYn"         => element('isInterestFree',       $card,      ""), //신용카드무이자여부
            "sFinanceCode"         => "", //결제기관코드
            "sFinanceName"         => element('company',              $card,      ""), //결제기관명
            "sFinanceAuthNumber"   => element('approveNo',            $card,      ""), //결제기관승인번호
            "sHashData"            => element('paymentKey',           $result,    ""), //해쉬데이타
            "nTransAmount"         => element('totalAmount',          $result,    ""), //환율적용금액
            "nExchangeRate"        => 1.0,  //적용환율
            "sAffiliateCode"       => "******" // 합병코드
        );

        $this->ismdb->trans_start(); # transection 시작한다. ================================

        # tXpayapproval 데이터 insert
        $this->ismdb->insert($this->t_xpayapproval, $xpay_data);
        $nXpayApprovalSeq = $this->ismdb->insert_id();

        # 등록된 tXpayapproval 조회
        $xpay_dbdata = $this->ismdb
            ->from($this->t_xpayapproval.' As t_xpay')
            ->where('t_xpay.nXpayApprovalSeq', $nXpayApprovalSeq)
            ->get()
            ->row()
        ;
        $this->ismdb->reset_query();


        # 청구기준 결제일 경우 청구정보에 카드결제seq 추가
        if(!empty($productcd))
        {
            $this->ismdb->where('nBillMonthSeq', $productcd)->update($this->t_billmonth, ['nXpayApprovalSeq'=>$nXpayApprovalSeq ]);
        }



        # tOlineApprovalDetail update
        $this->ismdb->where('nOlineApprovalDetailSeq', $card_data['nOlineApprovalDetailSeq']);
        if(!$this->ismdb->update($this->t_oapp_detail, ['sStatus'=>'Y']))
        {
            $error = $this->ismdb->error();
            $this->err_msg   = (ENVIRONMENT == 'development') ? $error['message'] : '';
            $this->err_code  = 'TOSS01';
        }
        $this->ismdb->reset_query();


        # tOlineApprovalCustomer update
        $this->ismdb->where('nOlineApprovalCustomerSeq', $card_data['nOlineApprovalCustomerSeq']);
        if(!$this->ismdb->update($this->t_oapp_customer, ['sStatus'=>'Y']))
        {
            $error = $this->ismdb->error();
            $this->err_msg   = (ENVIRONMENT == 'development') ? $error['message'] : '';
            $this->err_code  = 'TOSS02';
        }
        $this->ismdb->reset_query();

        $this->ismdb->trans_complete(); #transection 종료한다. ===============================
        return ($this->ismdb->trans_status()) ? $xpay_dbdata : FALSE;
    }





    function get_status_kor($status)
    {
        $status_array = [
            'READY'               => '준비됨',
            'IN_PROGRESS'         => '진행중',
            'WAITING_FOR_DEPOSIT' => '입금 대기 중',
            'DONE'                => '결제성공',
            'CANCELED'            => '결제가 취소됨',
            'PARTIAL_CANCELED'    => '결제가 부분 취소됨',
            'ABORTED'             => '카드 자동결제 혹은 키인 결제를 할 때 결제승인에 실패함',
            'EXPIRED'             => '유효 시간(30분)이 지나 거래가 취소됨'
        ];

        return $status_array[$status];
    }







    function set_olineapproval_data($data)
    {
        $return_seq  = [];
        $servicetype = ['서버호스팅'=>'S','코로케이션'=>'C', 'K-Clean'=>'K', '기타'=>'O'];

        # tOlineApprovalCustomer 데이터 insert
        $oap_customer = array(
            "sOrderNumber"  => $data['orderid'],
            "sCustomerName" => $data['ts_customername'],
            "sName"         => $data['ts_buyer'],
            "sPhone"        => $data['ts_customermobile'],
            "sEmail"        => $data['ts_customeremail'],
            "sServiceType"  => chk_element($data['ts_ordername'], $servicetype, 'O'),
            "sStatus"       => 'R'
        );
        $this->ismdb->insert($this->t_oapp_customer, $oap_customer);//org
        $return_seq['oapp_cust_cd'] = seed($this->ismdb->insert_id());
        $this->ismdb->reset_query();


        # tOlineApprovalDetail 데이터 insert
        $data['ts_cargetype'] = isset($data['ts_productCode']) ? '' : chk_element('ts_cargetype', $data, '신규신청');
        $oap_detail = array(
            "sOrderNumber" => $data['orderid'],
            "sServiceName" => $data['ts_productinfo']."(".$data['ts_ordername']." ".$data['ts_cargetype'].")", // 업체명
            "nMonthPrice"  => $data['ts_amount'],
            "sStatus"      => 'R'
        );
        $this->ismdb->insert($this->t_oapp_detail, $oap_detail);//org
        $return_seq['oapp_dtl_cd'] = seed($this->ismdb->insert_id());


        return $return_seq;
    }





    function get_olineapproval_data($table_cd, $table_seq)
    {
        $table  = ($table_cd=='oapp_cust_cd') ? $this->t_oapp_customer : $this->t_oapp_detail;
        $culumn = ($table_cd=='oapp_cust_cd') ? 'nOlineApprovalCustomerSeq' : 'nOlineApprovalDetailSeq';
        $seq    = seed($table_seq, FALSE);


        # 등록된 data 조회
        $olineapproval_dbdata = $this->ismdb
        ->where($culumn, $seq)
        ->get($table)
        ->row()
        ;

        return $olineapproval_dbdata;
    }






    private function _send_email($data)
    {
        # Email 내용 파싱 데이터
        $email_contents = [
            "sProductInfo"   => $data->sProductInfo,                //상품정보 ex) KINX IxCloud 2018-06 사용료
            "sDeliveryInfo"  => $data->sDeliveryInfo,               // 업체명
            "sUplusId"       => $data->sUplusId,                    // 상점ID
            "sFinanceName"   => '신용카드('.$data->sFinanceName.')', // 결제수단
            "sOrderNumber"   => $data->sOrderNumber,                // 주문번호
            "sBuyerName"     => $data->sBuyerName,                  // 신청자명
            "sResultMessage" => $data->sResultMessage,              // 처리결과
            "nAmount"        => number_format($data->nAmount)       // 결제금액
        ];

        # 발송자 정보
        $senderinfo = [
            'sName'   => EMAIL_SENDMAIL_NAME,
            'sEmail'  => EMAIL_HELP,
            'cc'      => NULL
        ];


        # 수신자 정보
        $receptionlist = [
            [
                "sName"  => '사업Group',
                "sEmail" => MAIL_TO_PAYMENT
            ]
        ];

        # Email 템플릿 코드
        $template_code = 'ISM_CARD_AR_EMAIL';

        // va($data);
        // va($email_contents);
        // va($senderinfo);
        // va($receptionlist);
        // vad($template_code);


        # 알람메일 발송
        $this->CI->load->library('kmms');
        $this->CI->kmms->kmms_send_email($email_contents, $senderinfo, $receptionlist, $template_code, NULL, 'each');

        return NULL;
   }





   private function _send_recepits($data)
   {
        $param = [
            "skinds"       => 2,//신용카드, mykinx는 신용카드 결제만 처리
            "price"        => (int)$data->nAmount,
            "income_name"  => $data->sBuyerName."(".$data->sDeliveryInfo.")",
            "income_place" => $data->sUplusId,
            "auth_code"    => $data->sFinanceAuthNumber,
        ];

        if(!empty($data->sBuyerId)) //청구서 결제 일 경우 청구seq 설정
        {
            $param["account_seq"]   = $data->sBuyerId;
            $param["billmonth_seq"] = $data->nProductCode;
        }

        $this->CI->load->library('rest');
        $this->CI->rest->initialize(array('server'=>$this->xapihost,"api_key"=>EMPAPI_KEY));
        $this->CI->rest->http_header('content-type','application/json;charset=UTF-8');
        $url = "payments/recepits";
        $result_recepits = $this->CI->rest->post($url, json_encode($param));

        return NULL;
   }


   //20221018 반제 api 변경
   private function _send_recepits_new($data)
   {
       $ar_billmonth = array();
        if(!empty($data->sBuyerId)) //청구서 결제 일 경우. billmonth seq로 정보 추출. BillMonth 1개만 결제하는 기준으로 진행
        {
            $nBillMonthSeq = $data->nProductCode;
            //security deposit, 선수수익 확인위해 tBillDetail 확인. -> 상품 정보 확인하여 Security Deposit / 선수수익 관련 상품 확인

            $CI =& get_instance();
            $CI->load->model('Payment_ism_model',"md_payment_ism");
            $rtn_product = $CI->md_payment_ism->get_billmonth_product($nBillMonthSeq);

            $save_securitydeposit = 0;
            $save_forwardprofit = 0;

            //Security Deposit / 예치금(선수수익) 처리
            if ($rtn_product && count($rtn_product) == 1)
            {
                if ($rtn_product[0]['nProductSeq'] == PRODUCT_SECURITY_DEPOSIT) {
                    $save_securitydeposit = (int)$data->nAmount; //반제 및 Security Deposit 에 적립
                    $save_forwardprofit = 0;
                }
                if ($rtn_product[0]['nProductSeq'] == PRODUCT_DEPOSIT) {
                    $save_securitydeposit = 0;
                    $save_forwardprofit = (int)$data->nAmount; //반제 및 선수수익에 적립
                }
            }
            else //값이 없거나 여러개이면 Security Deposit 혹은 예치금(선수수익)이 아니다. 이 두 경우는 해당 청구형태에 하나의 계약만 물려야 하기 때문에 여러개면 처리하지 않는다.
            {
                $save_securitydeposit = 0;
                $save_forwardprofit = 0;
            }

            $ar_billmonth[0] = array(
                'billmonth_seq'     => $nBillMonthSeq,
                'bill_price'        => (int)$data->nAmount, //사용 반제금액
                'bill_deposit'      => 0, //초과입금액은 이용하지 않는다.
                'bill_lostprice'    => 0, //잡손실은 이용하지 않는다.
                'save_securitydeposit'  => $save_securitydeposit, //적립 Security Deposit
                'save_forwardprofit'    => $save_forwardprofit //적립 선수수익
            );
        }


        if(!empty($data->sBuyerId))
            $nAccountSeq = $data->sBuyerId;
        else
            $nAccountSeq = '';//무기명일 경우 공백 전송


        unset($param);
        $param = array(
            'account_seq'       =>$nAccountSeq,
            'income_date'       => date("Y-m-d"),
            'income_method'     => 2,
            'bank_name'         => '',
            'bank_account'      => '',
            'pay_id'            => $data->sUplusId,
            'income_name'       => $data->sBuyerName."(".$data->sDeliveryInfo.")",
            'income_currency'   => 'KRW',//실제)화폐단위
            'income_price'      => (int)$data->nAmount,//실제) 고객 입금액
            'income_real'       => (int)$data->nAmount,//실제) 고객 입금회계액
            'income_fee'        => 0,
            'process_currency'          => 'KRW', //처리)화폐단위
			'process_price'             => (int)$data->nAmount, //처리) 입금액
			'process_real'              => (int)$data->nAmount, //처리) 입금회계액
			'process_fee'               => 0, //처리) 수수료
            'process_save_deposit'      => 0, //처리) 적립 초과입금액.비로그인시 초과액 없고. 로그인시는 미수금만큼 결제하므로.
            'process_etc_price'         => 0, //처리) 잡이익
            'remark'                    => $data->sProductInfo.'('.$data->sOrderNumber.')',
            'employee_no'               => 'SYSTEM_MY',
            'employee_name'             => 'SYSTEM_MY',
            'auth_code'                 => $data->sFinanceAuthNumber
        );
        if(!empty($data->sBuyerId))//청구형태 있으면 BillMonth array 추가
            $param['billmonth'] = $ar_billmonth;

        //va('send_recepits_new data');va($data);
        //va('send_recepits_new param');va($param);
        //die;

        $this->CI->load->library('rest');
        $this->CI->rest->initialize(array('server'=>$this->xapihost,"api_key"=>EMPAPI_KEY));
        $this->CI->rest->http_header('content-type','application/json;charset=UTF-8');
        $url = "/finances/incomes";
        $result_recepits = $this->CI->rest->post($url, json_encode($param));

        return NULL;
   }
}

/* End of file TossCard.php */
/* Location: ./thaad_app/libraries/TossCard.php */