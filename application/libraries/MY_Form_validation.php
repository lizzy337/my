<?php

class MY_Form_validation extends CI_Form_validation {
    /**
     *  @param	string
     * @return	bool
     */
    public function password_string($str) {
    
    	$CI =& get_instance();
    	$CI->form_validation->set_message('password_string','%s는 영문+숫자 혼합하여 8~15자로 입력해주세요.');
    
    	return (bool) preg_match(' /^(?=.*[a-zA-Z])(?=.*[0-9]).{8,15}$/', $str);
    
    }
    
    // --------------------------------------------------------------------

}

/* End of file Form_validation.php */
/* Location: ./system/libraries/Form_validation.php */