<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter Kmms Class
 *
 * @package         CodeIgniter
 * @subpackage      Libraries
 * @category        Libraries
 * @author          KINX TA_unit SYZ
 * @created         27/06/2020
 * @dscription      KMMS(KINX 메일발송 시스템)을 통한 Email 발송 및 상태 확인을 위한 libraries.
 */

class Kmms
{
    protected $CI           = NULL;
    protected $kmmsapihost  = NULL;


    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->config('kinxconfig');
        $config = $this->CI->config->config;
        $this->kmmsapihost = $config['kmms_api_host'];
    }




    /**
    * Emial 발송 등록
    * @param array  $email_contents : Email template에 설정한 변수 값
    * @param array  $senderinfo : 발송자 정보
    * @param string $receptionlist : 수신자 Email 목록. array 구조 array(array("sName"=>"수신자 명", "sEmail"=>"수신자 Email"));
    * @param string $template_code : Email template
    * @param array  $filekeys : 첨부파일 목록
    * @param string $sendtype : 발생 방식, each: 각각 메일 발송, one: 한번에 여러명 같이 발송
    * @return array
    */
    public function kmms_send_email($email_contents, $senderinfo, $receptionlist, $template_code, $filekeys=NULL, $sendtype='each')
    {
        # parameter 확인
        if(empty($senderinfo) || empty($email_contents) || empty($template_code) || empty($receptionlist))
        {
            throw new Exception('Email 발송을 위한 기본 데이터가 확인되지 않습니다.');
        }


        # $senderinfo 발송자 정보 구성
        /*----------------------------------------------------------------------------------------
        * sName  => 발송자 명
        * sEmail => 발송자 Email 주소
        * cc     => 참조 Email 주소, 다중일 경우 "," 로 구분.
        * bcc    => 숨김참조 Email 주소, 다중일 경우 "," 로 구분.
        ----------------------------------------------------------------------------------------*/
        # $email_contents는 Email template에 설정한 변수에 적용될 정보이며, template별 구성이 다르게 적용됩니다.


        # Email발송 KMMS기본정보 설정
        $arSendmaillist = array(
            'template_code' => $template_code,
            'sender_name'   => $senderinfo['sName'],
            'sender_email'  => $senderinfo['sEmail'],
            'booking_flag'  => 1,
            'emails'        => array()
        );



        # 발송 방식에 따라 발송정보 설정 구분
        if($sendtype=='each')
        {
            # KMMS 발송정보 설정
            for($i=0, $max=count($receptionlist); $i < $max; $i++)
            {
                # KMMS 발송정보 설정
                $arSendmaillist['emails'][] = array(
                    "address" => $receptionlist[$i]['sEmail'],
                    "name"    => $receptionlist[$i]['sName'],
                    "data"    => $email_contents,
                    "bcc"     => '', // 각각 Email 발송 시에는 숨은 참조를 지운다. 같은 메일을 다중 받을 수 있어서..
                    "cc"      => $senderinfo['cc'], // 같은 메일을 다중 받을 수 있지만 수신자에게 참조받은 email를 표시할 필요가 있어 추가한다.
                    "filekey" => is_null($filekeys) ? '' : implode(',', $filekey)
                );
            }

        }
        elseif($sendtype=='one')
        {
            # 수신자 정보 설정 각 상세 정보
            $customeremail = array();
            for($i=0, $max=count($receptionlist); $i < $max; $i++)
            {
                $customeremail[] = $receptionlist[$i]['sEmail'];
            }

            # KMMS 발송정보 설정
            $arSendmaillist['emails'][] = array(
                "address" => implode(',', $customeremail),
                "name"    => '', // 한번에 여러명 동시 발송으로 수신자 이름을 설정 할 수 없다.
                "data"    => $email_contents,
                "cc"      => $senderinfo['cc'],
                "bcc"     => $senderinfo['sEmail'],
                "filekey" => is_null($filekeys) ? '' : implode(',', $filekeys)
            );
            unset($customeremail);
        }
        else
        {
            throw new Exception('정의 되지 않는 Email 발송 방식을 요청하셨습니다.');
        }



        # Email 발송등록
        $this->CI->load->library('rest');
        $this->CI->rest->initialize(array('server'=>$this->kmmsapihost));
        $this->CI->rest->http_header('content-type','application/json;charset=UTF-8');
        $this->CI->rest->http_header('x-api-key',EMPAPI_KEY);
        $result =  $this->CI->rest->post("api/kmms/emails_nodb",json_encode($arSendmaillist));

        #return
        return $result;
    }





}

/* End of file Kmms.php */
/* Location: ./application/libraries/Kmms.php */
