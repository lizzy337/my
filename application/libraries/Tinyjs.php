<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 자바스크립트 관련 helper 모음.
 * @author gabia_한규남
 * @since version 1.0 - 2009. 7. 7.
 */
class Tinyjs {

	function js($content) {
		return "
		<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">\n
		<script type=\"text/javascript\" charset=\"utf-8\">".$content."</script>
		";
	}

	function alert($msg) {
		$msg = str_replace("\n","",$msg);
		echo $this->js("alert('".$msg."');\n");
	}

	function pageRedirect($url, $msg = '', $target = 'self') {
		if ($msg) {
			$this->alert($msg);
		}
		echo $this->js($target . ".location.replace('$url')");
	}

	function pageBack($msg = '') {
		if ($msg) {
			$this->alert($msg);
		}
		echo $this->js("history.back();");
	}

	function pageReload($msg = '', $target = 'self') {
		if ($msg) {
			$this->alert($msg);
		}
		echo $this->js($target . ".location.reload();");
	}

	function pageClose($msg = '') {
		if ($msg) {
			$this->alert($msg);
		}
		echo $this->js("self.close();");
	}

	function openerRedirect($url, $msg = '') {
		if ($msg) {
			$this->alert($msg);
		}
		echo $this->js("opener.location.replace('$url')");
	}
}
?>
