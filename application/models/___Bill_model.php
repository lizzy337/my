<?php
class Bill_model extends CI_Model {
	protected $table = array("tBill", "tContractService", "tServiceItem");
	private function _query($params) {
		$this->db->from($this->table[0]." as A");
		$this->db->join($this->table[1]." as B", "A.nContractSeq = B.nContractSeq");
		$this->db->join($this->table[2]." as C", "B.nServiceCode = C.nServiceCode");
		if(isset($params["nContractSeq"])) {
			$where["A.nContractSeq"] = $params["nContractSeq"];
		}
		
		if(isset($params["select"])) {
			$this->db->select($params["select"]);
		}
		
		if(isset($params["secKey"]) && isset($params["secTxt"])) {
			$this->db->like($params["secKey"], $params["secTxt"]);
		}
		$where["1"] = "1";
		return $where;
	}

	public function _select_cnt($params=array()) {
		$where = $this->_query($params);
		$this->db->where($where);
		return $this->db->count_all_results();
	}

	public function _select_list($params=array()) {
		$limit = (isset($params["limit"])) ? $params["limit"] : NULL;
		$offset = (isset($params["offset"])) ? $params["offset"] : NULL;
		$where = $this->_query($params);
		// 정렬관련
		if(isset($params["oType"]) && isset($params["oKey"])){
			$this->db->order_by($params["oKey"], $params["oType"]);
		}
		return $this->db->get_where(null, $where, $limit, $offset)->result_array();
	}

	public function _select_row($where) {
		return $this->db->where($where)->get($this->table[0], 1)->row_array();
	}

	public function _insert($data) {
		return $this->db->insert($this->table[0], $data);
	}

	public function _delete($where) {
		$this->db->where($where);
		$this->db->delete($this->table[0]);
	}

	public function _update($data, $where) {
		return $this->db->update($this->table[0], $data, $where);
	}
	
	public function _set($params) {
		$this->db->set($params, NULL, FALSE);
		return $this->db->update($this->table[0]);
	}

	public function _replace($data) {
		return $this->db->replace($this->table[0], $data);
	}
}