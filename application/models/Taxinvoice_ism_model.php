<?php
class Taxinvoice_ism_model extends CI_Model { //기존 intranet의 Taxinvoice_model의 대체.


	protected $t_taxinvoice	      = 'BILL.tTaxInvoice';
	protected $t_billmonth        = 'BILL.tBillMonth';
	protected $t_XpayApproval     = 'BILL.tXpayApproval';
	protected $t_billtype         = 'BILL.tBillType';
	protected $t_taxinvoicemember = 'BILL.tTaxInvoiceMember';
	protected $t_taxdetail        = 'BILL.tTaxInvoiceDetail';
	protected $t_billdetail       = "BILL.tBillDetail";
	protected $v_vProduct         = "CONTRACT.vProduct";
	protected $v_BillAdjustments  = "BILL.vBillAdjustments";
	protected $v_BillDiscount     = "BILL.vBillDiscount";


	function __construct(){
		//생성자
		parent::__construct();


		//ism 실DB 적용
		$ismdb = $this->load->database('ism',true);
		$this->ismdb = $ismdb;
	}

	//_select_list
        public function get_list_old($data, $onlycount = FALSE)
	{
		if(isset($data["sDeleteYN"]) && $data["sDeleteYN"]) {
			$this->ismdb->where(" A.sDeleteYN = '". $data["sDeleteYN"]."'");
		}
		if(isset($data["nTaxInvoiceSeq"]) && $data["nTaxInvoiceSeq"]) {
			$this->ismdb->where(" A.nTaxInvoiceSeq =". $data["nTaxInvoiceSeq"]);
		}

		if(isset($data["nAccountSeq"]) && $data["nAccountSeq"]) {
			$this->ismdb->where(" A.nAccountSeq =". $data["nAccountSeq"]);
		}



		if(isset($data["datetype"]))
		{
			switch ($data["datetype"])
			{
				case "udate": //사용월 기준
					if(isset($data["sdate"])) 	$this->ismdb->where(" A.sUseDate >='".$data["sdate"]."'");
					if(isset($data["edate"])) 	$this->ismdb->where(" A.sUseDate <='".$data["edate"]."'");
					break;
				case "cdate": //청구월 기준
					if(isset($data["sdate"])) 	$this->ismdb->where(" A.dtBilling >='".$data["sdate"]."'");
					if(isset($data["edate"])) 	$this->ismdb->where(" A.dtBilling <='".$data["edate"]."'");
					break;
				case "pdate": //결제월 기준
					if(isset($data["sdate"])) 	$this->ismdb->where(" B.dtReceipt >='".$data["sdate"]."'");
					if(isset($data["edate"])) 	$this->ismdb->where(" B.dtReceipt <='".$data["edate"]."'");
					break;
			}
		}

		if(isset($data["sStatus"]))
		{
			switch ($data["sStatus"])
			{
				case "A": //세금계산서 발행
					$this->ismdb->where( "A.sTaxStatus =  'A' ");
					break;
				case "X": //세금계산서 미발행
					$this->ismdb->where( "A.sTaxStatus =  'X' ");
					break;
				case "Z": //신용카드결제
					$this->ismdb->where(" C.sPaymentType =  'SC0010' ");
					break;
			}
		}
		if($onlycount==FALSE)
		{
			$this->ismdb->limit($data['limit'], $data['offset']);
		}


		$select = " A.*, A.sTaxStatus as sStatus, A.dtBilling as dtChargeDate, A.dtPayment as dtPaymentDate, A.sCompanyNm as sCompanyName ";
		$select .= " , B.nBillMonthSeq as nBillListSeq, B.nRemainder as nRemainderPrice, B.nReceipt as nReceiptPrice, B.dtReceipt as dtReceiptDate ";
                $select .= " , B.nRemainderVat as nRemainderVat, B.nReceiptVat as nReceiptVat ";
		$select .= " , C.sPaymentType, D.sDeadLine";
		$select .= " , M.sName as sName, M.sEmail as sEmail, M.sPhone as sPhone ";
		$query = $this->ismdb
                                    ->select($select, FALSE)
                                    ->from($this->t_taxinvoice."       as A ")
                                    ->join($this->t_billmonth."        as B"," A.nBillMonthSeq = B.nBillMonthSeq ")//, left 제거
                                    ->join($this->t_billtype."         as D"," B.nBillTypeSeq = D.nBillTypeSeq ") //A.nBillTypeSeq=>B.nBillTypeSeq, left 제거
                                    ->join($this->t_XpayApproval."     as C"," B.nXpayApprovalSeq = C.nXpayApprovalSeq ","left")
                                    ->join($this->t_taxinvoicemember." as M"," A.nTaxInvoiceSeq = M.nTaxInvoiceSeq ", "left")
                                    ->group_by(" A.nBillMonthSeq ") //요금담당자가 ISM에는 최대 두명까지 들어간다. 그래서 taxinvoice가 두개 추가됨. 따라서 group by로 묶어줌. 어짜피 월별 정보는 하나.
                                    ->order_by("A.dtBilling", "DESC")
                                    ->get();
		if ($query !== FALSE)
		{
			if($onlycount)
			{
				return ($query && $query->num_rows() > 0) ? $query->num_rows() : 0;
			}
			elseif ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				$this->errMsg = "조회된 데이터가 없습니다.";
			}
		}
		else
		{
			$error = $this->ismdb->error();
			$this->errMsg = $error['message'];
		}
	}



        //입금처리 관련하여 금액 추출 부분 수정. 결제관리 리스트
	public function get_list($data, $onlycount = FALSE)
	{
		if(isset($data["sDeleteYN"]) && $data["sDeleteYN"]) {
			$this->ismdb->where(" A.sDeleteYN = '". $data["sDeleteYN"]."'");
		}
		if(isset($data["nTaxInvoiceSeq"]) && $data["nTaxInvoiceSeq"]) {
			$this->ismdb->where(" A.nTaxInvoiceSeq =". $data["nTaxInvoiceSeq"]);
		}

		if(isset($data["nAccountSeq"]) && $data["nAccountSeq"]) {
			$this->ismdb->where(" A.nAccountSeq =". $data["nAccountSeq"]);
		}



		if(isset($data["datetype"]))
		{
			switch ($data["datetype"])
			{
				case "udate": //사용월 기준
					if(isset($data["sdate"])) 	$this->ismdb->where(" A.sUseDate >='".$data["sdate"]."'");
					if(isset($data["edate"])) 	$this->ismdb->where(" A.sUseDate <='".$data["edate"]."'");
					break;
				case "cdate": //청구월 기준
					if(isset($data["sdate"])) 	$this->ismdb->where(" A.dtBilling >='".$data["sdate"]."'");
					if(isset($data["edate"])) 	$this->ismdb->where(" A.dtBilling <='".$data["edate"]."'");
					break;
				case "pdate": //결제월 기준
					if(isset($data["sdate"])) 	$this->ismdb->where(" B.dtReceipt >='".$data["sdate"]."'");
					if(isset($data["edate"])) 	$this->ismdb->where(" B.dtReceipt <='".$data["edate"]."'");
					break;
			}
		}

		if(isset($data["sStatus"]))
		{
			switch ($data["sStatus"])
			{
				case "A": //세금계산서 발행
					$this->ismdb->where( "A.sTaxStatus =  'A' ");
					break;
				case "X": //세금계산서 미발행
					$this->ismdb->where( "A.sTaxStatus =  'X' ");
					break;
				case "Z": //신용카드결제
					$this->ismdb->where(" C.sPaymentType =  'SC0010' ");
					break;
			}
		}
		if($onlycount==FALSE)
		{
			$this->ismdb->limit($data['limit'], $data['offset']);
		}


		$select = " A.*, A.sTaxStatus as sStatus, A.dtBilling as dtChargeDate, A.dtPayment as dtPaymentDate, A.sCompanyNm as sCompanyName ";
		$select .= " , B.nBillMonthSeq as nBillListSeq, B.nRemainder as nRemainderPrice, B.nReceipt as nReceiptPrice, B.dtReceipt as dtReceiptDate ";
		$select .= " , B.nRemainderVat as nRemainderVat, B.nReceiptVat as nReceiptVat, B.dtIncome, B.sIncomeFlag "; //dtIncome - 입금일, sIncomeFlag - 입금여부
        $select .= " , C.sPaymentType, D.sDeadLine ";
        $select .= " , D.sShowName ";//MyKINX 노출용 청구명
		$select .= " , M.sName as sName, M.sEmail as sEmail, M.sPhone as sPhone, M.sMobile as sMobile ";
		//-start 20181018
		$select .= " , BILL.fn_getRecepit('M',B.sSendType, B.sCurrencyType, B.nCharge, B.nVatPrice, B.nUSDCharge) as billmonth_price "; //청구액
		$select .= " , BILL.fn_getRecepit('C',B.sSendType, B.sCurrencyType, B.nCharge, B.nVatPrice, B.nUSDCharge) as billmonth_currency "; //환율
		$select .= " , ifnull((select sum(nPrice) from BILL.tFinanceUnit where nBillMonthSeq = B.nBillMonthSeq and bDel='N'),0) as income_finance"; //받은돈
		$select .= " , ifnull((select sum(nPrice) from BILL.tBillMonthDeposit where nBillMonthSeq = B.nBillMonthSeq and bDel='N'),0) as income_deposit ";//예치금
		//-end
		//-start 20200326 tTaxINvoiceDivision 이 존재하는지 갯수 확인
		$select .= " ,chkdv.dvcnt";

		$dvquery = " SELECT count(1) as dvcnt, dt.nTaxInvoiceSeq ";
		$dvquery .= " FROM BILL.tTaxInvoiceDivision as dv ";
		$dvquery .= " JOIN BILL.tTaxInvoiceDetail as dt on dv.nTaxInvoiceDetailSeq = dt.nTaxInvoiceDetailSeq ";
		$dvquery .= " JOIN BILL.tTaxInvoice as iv on dt.nTaxInvoiceSeq=iv.nTaxInvoiceSeq and iv.nAccountSeq=".$data["nAccountSeq"];
		$dvquery .= " WHERE dv.sDeleteYN='N' ";
		$dvquery .= " GROUP BY iv.nTaxInvoiceSeq";
		//$subquery = $this->ismdb->query($dvquery)->get_compiled_select();
		//-end
		$query = $this->ismdb
            ->select($select, FALSE)
            ->from($this->t_taxinvoice."       as A ")
            ->join($this->t_billmonth."        as B"," A.nBillMonthSeq = B.nBillMonthSeq ")//, left 제거
            ->join($this->t_billtype."         as D"," B.nBillTypeSeq = D.nBillTypeSeq ") //A.nBillTypeSeq=>B.nBillTypeSeq, left 제거
            ->join($this->t_XpayApproval."     as C"," B.nXpayApprovalSeq = C.nXpayApprovalSeq ","left")
            ->join($this->t_taxinvoicemember." as M"," A.nTaxInvoiceSeq = M.nTaxInvoiceSeq ", "left")
            ->join("(".$dvquery.") as chkdv"," A.nTaxInvoiceSeq=chkdv.nTaxInvoiceSeq", "left")
            ->group_by(" A.nBillMonthSeq ") //요금담당자가 ISM에는 최대 두명까지 들어간다. 그래서 taxinvoice가 두개 추가됨. 따라서 group by로 묶어줌. 어짜피 월별 정보는 하나.
            ->order_by("A.dtBilling", "DESC")
            ->get()
        ;
		//echo $this->ismdb->last_query();die;
		if ($query !== FALSE)
		{
			if($onlycount)
			{
				return ($query && $query->num_rows() > 0) ? $query->num_rows() : 0;
			}
			elseif ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				$this->errMsg = "조회된 데이터가 없습니다.";
			}
		}
		else
		{
			$error = $this->ismdb->error();
			$this->errMsg = $error['message'];
		}
	}

        /*
        //결제관리 상세 리스트 추출. 181018
        public function get_billdetail_list($param)
        {
            $select = "D.*, BM.sSendType ";
            //$select .= ", TD.sProductType as sServiceType, TD.sProuctDetailType as sServiceName, TD.sProductNm as sServiceDetail, TD.nQuantity, TD.sUnit ";
            $select .= ", Product.sProductType as sServiceType, Product.sDetailType as sServiceName, Product.sName as sServiceDetail "; //nQuantity, sUnit 은 tBillDetail의arChargeinfo 에서 추출
            $select .= ", BA.nAdjCharge as Adj_nAdjCharge ,BA.nExcCharge as Adj_nExcCharg ";
            $select .= ",BDisc.nCharge as Disc_nCharge ,BDisc.nAdjCharge as Disc_nAdjCharge, BDisc.nDiscount as Disc_nDiscount ";
            $select .= ", BILL.fn_getRecepit('C',BM.sSendType, BM.sCurrencyType, BM.nCharge, BM.nVatPrice, BM.nUSDCharge) as billmonth_currency "; //환율
            #b=>tBillMonth


            $query = $this->ismdb
                    ->select($select, FALSE)
                    ->from($this->t_billdetail." AS D")
                    //->join($this->t_taxdetail." AS TD"," D.nBillDetailSeq = TD.nBillDetailSeq and TD.sDeleteYN='N' ")
                    ->join($this->t_billmonth." AS BM"," D.nBillMonthSeq = BM.nBillMonthSeq and BM.sDeleteYN='N' ")
                    ->join($this->v_vProduct." AS Product"," D.nProductSeq=Product.nProductSeq ", "LEFT")
                    ->join($this->v_BillAdjustments." AS BA"," D.nBillDetailSeq = BA.nBillDetailSeq ", "LEFT")
                    ->join($this->v_BillDiscount." AS BDisc"," D.nBillDetailSeq = BDisc.nBillDetailSeq ", "LEFT")

                    ->where("D.nBillMonthSeq",$param['nBillMonthSeq'])
                    ->where("D.sDeleteYN",$param['sDeleteYN'])
                    ->where("D.sSumYN",$param['sSumYN'])
                    ->get();
            //echo $this->ismdb->last_query();die;
            return $query->result_array();
        }
        */



	function get_taxinvoice_by_seq($seq)
        {
	    $select = "";

	    $query = $this->ismdb
                ->where(array('nTaxinvoiceSeq' => $seq, 'sDeleteYN' => 'N'))//sDeleteYN 추가
                ->get($this->t_taxinvoice)
                ;

	    if ($query !== FALSE)
            {
	        if ($query->num_rows() > 0)
                {
	            return $query->row();
	        }
                else
                {
	            $this->errMsg = "조회된 데이터가 없습니다.";
	        }
	    }
            else
            {
	        $error = $this->ismdb->error();
	        $this->errMsg = $error['message'];
	    }
	    return NULL;
	}



	function get_taxdetail_list($taxseq)
        {
	    $select ="
	       concat(sProductType,' > ',sProuctDetailType) AS sServiceName,
	       sProductNm AS sServiceDetail,
           concat(sProductTypeEng, ' > ',sProuctDetailTypeEng) AS sServiceNameEng,
           sProductNmEng AS sServiceDetailEng,
	       nCharge AS nContractPrice,
           nExcCharge AS nContractExcPrice,
	       nTaxPrice AS nTaxPrice,
	       nQuantity AS nQuantity,
	      sUnit AS sUnit
	    ";
	    $query = $this->ismdb
                ->select($select, FALSE)
                ->where(array('nTaxinvoiceSeq' => $taxseq, 'sDeleteYN' => 'N'))
                ->order_by('sProductType, sProuctDetailType, sProductNm')
                ->get($this->t_taxdetail)
                ;

	    if ($query !== FALSE)
            {
	        if ($query->num_rows() > 0)
                {
	            return $query->result();
	        }
                else
                {
	            $this->errMsg = "조회된 데이터가 없습니다.";
	        }
	    }
            else
            {
	        $error = $this->ismdb->error();
	        $this->errMsg = $error['message'];
	    }
	    return NULL;
	}


}