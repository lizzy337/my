<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Noticelist_model extends CI_Model{
	
	public function get_noticelist()
	{
		$query = $this->db->get('tNoticeList');
	}
	
	public function _select_cnt($params=array()) {		
		return $this->db->count_all_results('tNoticeList');
	}
	
	public function _select_list_main($boardType)
	{
		$sql = " SELECT nNoticeListSeq, sTitle, dtCreateDate  ";
		$sql .= " FROM tNoticeList "; 
		$sql .= " WHERE sDisplayYn='Y' and nBoardSeq=? ";//140827 보안강화
		$sql .= " order by dtCreateDate desc, nNoticeListSeq desc ";
		$sql .= " limit 4";
		
		//echo $sql;
		
		$query = $this->db->query($sql,$boardType);//140827 보안강화
		$result = $query->result_array();
		return $result;
	}
	/*
	public function _select_list($params=array(),$searchText=null, $searchType=1,$boardType=0, $sHeadTextgroupCode=null, &$result_cnt)
	{
		if( isset($searchText) && ($searchText != null ))//검색어 있을때 
		{	 
			$sql = " SELECT nNoticeListSeq, nHeadTextSeq, sTitle, sContent, nCntRead, date_format(dtCreateDate, \"%Y-%m-%d\") as dtCreateDate ";
			if( $sHeadTextgroupCode != null ){
				$sql .= ", ( SELECT sCodeName From tSysCode where sGroupCode='".$sHeadTextgroupCode."' and sCode=nHeadTextSeq) as sHeadText ";
			}
			$sql .= " FROM tNoticeList ";
			if( $searchType == 2 ){//내용
				$sql .= " WHERE sContent like '%".$searchText."%' and sDisplayYn='Y'";
			}else{//제목
				$sql .= " WHERE sTitle like '%".$searchText."%' and sDisplayYn='Y'";
			}
			$sql .= " and nBoardSeq=".$boardType." ";			
			$sql .= " order by dtCreateDate desc, nNoticeListSeq desc ";
			
			$query = $this->db->query($sql);
			$result = $query->result_array();
			$result_cnt = count($result);
			
			if(isset($params["limit"])){
				$sql .= " limit ".$params["limit"];
			}
			if(isset($params["offset"])){
				$sql .= " offset ".$params["offset"];
			}
			
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
		}
		else
		{ 
			$sql = "SELECT nNoticeListSeq, nHeadTextSeq, sTitle, sContent, nCntRead, date_format(dtCreateDate, \"%Y-%m-%d\") as dtCreateDate ";
			$sql .= ", ( SELECT sCodeName From tSysCode where sGroupCode='".$sHeadTextgroupCode."' and sCode=nHeadTextSeq) as sHeadText ";
			$sql .= " FROM tNoticeList ";
			$sql .= " WHERE sDisplayYn='Y' and nBoardSeq=".$boardType." ";
			$sql .= " order by dtCreateDate desc, nNoticeListSeq desc ";
			
			$query = $this->db->query($sql);
			$result = $query->result_array();
			$result_cnt = count($result);
			
			if(isset($params["limit"])){
				$sql .= " limit ".$params["limit"];
			}
			if(isset($params["offset"])){
				$sql .= " offset ".$params["offset"];
			}
			
			//echo $sql;
			
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
		}
	}
	*/
	//140828 보안강화
	public function _select_list($params=array(),$searchText=null, $searchType=1,$boardType=0, $sHeadTextgroupCode=null, &$result_cnt)
	{
		$result_cnt = $this->select_list_cnt($params, $searchText, $searchType, $boardType, $sHeadTextgroupCode);
		if( isset($searchText) && ($searchText != null ))//검색어 있을때
		{
			$this->db->select('nNoticeListSeq, nHeadTextSeq, sTitle, sContent, nCntRead, date_format(dtCreateDate, "%Y-%m-%d") as dtCreateDate');
			if( $sHeadTextgroupCode != null )
			{
				$this->db->select("( SELECT sCodeName From tSysCode where sGroupCode='".$sHeadTextgroupCode."' and sCode=nHeadTextSeq) as sHeadText");
			}
			$this->db->from('tNoticeList');
			if( $searchType == 2 )//내용
			{
				$this->db->where('sDisplayYn','Y');
				$this->db->like('sContent',$searchText);
			}else{//제목
				$this->db->like('sTitle',$searchText);
				$this->db->where('sDisplayYn','Y');
			}
			$this->db->where('nBoardSeq', $boardType);
			$this->db->order_by('dtCreateDate','desc');
			$this->db->order_by('nNoticeListSeq','desc');

			if(isset($params["limit"]))
			{
				$limit = $params["limit"];
				if(isset($params["offset"]))
				{
					$offset = $params["offset"];
					$this->db->limit($limit, $offset);
				}
				else{
					$this->db->limit($limit);
				}
			}
				
			$query = $this->db->get();
			$result = $query->result_array();
			
			return $result;
		}
		else
		{
			$this->db->select('nNoticeListSeq, nHeadTextSeq, sTitle, sContent, nCntRead, date_format(dtCreateDate, "%Y-%m-%d") as dtCreateDate');
			if( $sHeadTextgroupCode != null )
			{
				$this->db->select("( SELECT sCodeName From tSysCode where sGroupCode='".$sHeadTextgroupCode."' and sCode=nHeadTextSeq) as sHeadText");
			}
			$this->db->from('tNoticeList');
			$this->db->where('sDisplayYn','Y');
			$this->db->where('nBoardSeq', $boardType);
			$this->db->order_by('dtCreateDate','desc');
			$this->db->order_by('nNoticeListSeq','desc');
			
			if(isset($params["limit"]))
			{
				$limit = $params["limit"];
				if(isset($params["offset"]))
				{
					$offset = $params["offset"];
					$this->db->limit($limit, $offset);
				}
				else{
					$this->db->limit($limit);
				}
			}
			
			$query = $this->db->get();
			$result = $query->result_array();
			return $result;
		}
	}
	//140828 보안강화
	public function select_list_cnt($params=array(),$searchText=null, $searchType=1,$boardType=0, $sHeadTextgroupCode=null)
	{
		if( isset($searchText) && ($searchText != null ))//검색어 있을때
		{
			$this->db->select('nNoticeListSeq, nHeadTextSeq, sTitle, sContent, nCntRead, date_format(dtCreateDate, "%Y-%m-%d") as dtCreateDate');
			if( $sHeadTextgroupCode != null )
			{
				$this->db->select("( SELECT sCodeName From tSysCode where sGroupCode='".$sHeadTextgroupCode."' and sCode=nHeadTextSeq) as sHeadText");
			}
			$this->db->from('tNoticeList');
			if( $searchType == 2 )//내용
			{
				$this->db->where('sDisplayYn','Y');
				$this->db->like('sContent',$searchText);
			}else{//제목				
				$this->db->where('sDisplayYn','Y');
				$this->db->like('sTitle',$searchText);
			}
			$this->db->where('nBoardSeq', $boardType);
			$this->db->order_by('dtCreateDate','desc');
			$this->db->order_by('nNoticeListSeq','desc');
	
			$query = $this->db->get();
			$result = $query->result_array();
			$result_cnt = count($result);		 
		}
		else
		{
			$this->db->select('nNoticeListSeq, nHeadTextSeq, sTitle, sContent, nCntRead, date_format(dtCreateDate, "%Y-%m-%d") as dtCreateDate');
			if( $sHeadTextgroupCode != null )
			{
				$this->db->select("( SELECT sCodeName From tSysCode where sGroupCode='".$sHeadTextgroupCode."' and sCode=nHeadTextSeq) as sHeadText");
			}
			$this->db->from('tNoticeList');
			$this->db->where('sDisplayYn','Y');
			$this->db->where('nBoardSeq', $boardType);
			$this->db->order_by('dtCreateDate','desc');
			$this->db->order_by('nNoticeListSeq','desc');
	
			$query = $this->db->get();
			$result = $query->result_array();
			$result_cnt = count($result);		 
		}
		return $result_cnt;
	}
	
	public function get_noticedata($nNoticeListSeq, $sHeadTextgroupCode=null)
	{
		$this->update_readCnt($nNoticeListSeq);
		
		$sql = " SELECT a.sTitle, a.nHeadTextSeq, a.sContent, a.nCntRead, date_format(a.dtCreateDate, '%Y-%m-%d') as dtCreateDate ";
		if( $sHeadTextgroupCode != null ){
			$sql .= ", ( SELECT sCodeName From tSysCode where sGroupCode=? and sCode=nHeadTextSeq) as sHeadText ";//140827 보안강화
		}
		$sql .= " FROM tNoticeList as a ";
		$sql .= " WHERE nNoticeListSeq='".$nNoticeListSeq."'";
		if( $sHeadTextgroupCode != null )
		{
			$query = $this->db->query($sql,$sHeadTextgroupCode);//140827 보안강화
		}else{
			$query = $this->db->query($sql);
		}
		$result = $query->result_array();
		return $result;
	}
	 
	
	public function update_readCnt($nNoticeListSeq)
	{
		$query = $this->db->get_where('tNoticeList',array('nNoticeListSeq'=>$nNoticeListSeq));
		$cnt = 0;
		foreach($query->result() as $row)
		{
			$cnt = $row->nCntRead;
		}
		$cnt = $cnt+1;
	
		$data = array( 'nCntRead'=> $cnt);
		$this->db->where('nNoticeListSeq',$nNoticeListSeq);
		$this->db->update('tNoticeList',$data);
	
	}
	
	public function get_attachfileinfo($nNoticeListSeq)
	{
		$sql = "SELECT sFileName,sOriginal,sPath, nSubFileSeq, nFileSeq FROM tFilesInfo  ";
		$sql .= " WHERE sTblName='tNoticeList' and nSubFileSeq=? ";//140827 보안강화		
	
		$query = $this->db->query($sql, $nNoticeListSeq);//140827 보안강화
		$result = $query->result_array();
		return $result;
	}
	
	public function get_fileinfo($nFileSeq)
	{
		$sql = "SELECT sFileName,sOriginal,sPath, nSubFileSeq, nFileSeq FROM tFilesInfo  ";
		$sql .= " WHERE sTblName='tNoticeList' and nFileSeq=? ";//140827 보안강화
	
		$query = $this->db->query($sql, $nFileSeq);//140827 보안강화
		$result = $query->result_array();
		return $result;
	}
	
}
