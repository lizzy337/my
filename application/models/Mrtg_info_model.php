<?php
class Mrtg_info_model extends CI_Model {
	protected $table = array("tMrtgInfo");
	
	function __construct(){
		//생성자
		parent::__construct();
	
		$CI =& get_instance(); //CodeIgniter에 내장된 리소스 할당
	
		$this->usdb = $this->load->database('op',true);
		$this->ismdb = $this->load->database('ism',true);
	
	}
	
	private function _query($params) {
		if(isset($params["secKey"]) && isset($params["secTxt"])) {
			$this->db->like($params["secKey"], $params["secTxt"]);
		}
		if(isset($params["nContractSeq"])) {
			$where["nContractSeq"] = $params["nContractSeq"];
		}
                if(isset($params["nMrtgInfoSeq"])) {
			$where["nMrtgInfoSeq"] = $params["nMrtgInfoSeq"];
		}
		$where["1"] = "1";
		$where["sDeleteYn"] = "N";
		return $where;
	}

	public function _select_cnt($params=array()) {
		$where = $this->_query($params);
		$this->usdb->where($where);
		$this->usdb->from($this->table[0]);
		return $this->usdb->count_all_results();
	}
	
	public function select_tResource($args)
	{
		//========== 받은정보 ==========
		$nAccountSeq = $args['nAccountSeq'];
		$sServiceType = isset($args['sServiceType']) ? $args['sServiceType'] : "";
		//========== 받은정보 ==========
		
		$this->ismdb->select("a.nAccountSeq");
		$this->ismdb->select("c.nContractSeq");
		$this->ismdb->select("r.mID");
		
		$this->ismdb->from("CONTRACT.tAccount a");
		$this->ismdb->join("CONTRACT.tContract c", "a.nAccountSeq = c.nAccountSeq");
		$this->ismdb->join("CONTRACT.tResource r", "c.nContractSeq = r.nContractSeq");
		
		$this->ismdb->where("a.nAccountSeq", $nAccountSeq);
		$this->ismdb->where("r.sDeleteYN", "N");
//		$this->ismdb->where("r.dtEnd >=", date('Y-m-d'));
		if(!empty($args['sServiceType'])) $this->ismdb->where("r.sServiceType", $sServiceType);
		
		return $this->ismdb->get()->result_array();
	}
	
	public function select_tServiceBridge($args)
	{
		//========== 받은정보 ==========
		$nAccountSeq = $args['nAccountSeq'];
		//========== 받은정보 ==========
		
		$this->ismdb->select("nBridgeSeq");
		$this->ismdb->select("nAccountSeq");
		$this->ismdb->select("nMrtgInfoSeq");
		
		$this->ismdb->from("CONTRACT.tServiceBridge");
		
		$this->ismdb->where("bDel", "N");
		$this->ismdb->where("nAccountSeq", $nAccountSeq);
		
		return $this->ismdb->get()->result_array();
	}
	
	public function select_tMrtgInfo($args)
	{
		//========== 받은정보 ==========
		$arrMrtgInfoSeq = isset($args['arrMrtgInfoSeq']) ? $args['arrMrtgInfoSeq'] : "";
		//========== 받은정보 ==========
		
		$this->usdb->select("nMrtgInfoSeq");
		$this->usdb->select("sNickName");
		$this->usdb->select("sMrtgServerTitle");
		
		$this->usdb->from("kinxdb.tMrtgInfo");
		
		$this->usdb->where_in("nMrtgInfoSeq", $arrMrtgInfoSeq);
		$this->usdb->where("sDeleteYn", "N");
		
		return $this->usdb->get()->result_array();
	}

	public function _select_list($params=array()) {
		$limit = (isset($params["limit"])) ? $params["limit"] : NULL;
		$offset = (isset($params["offset"])) ? $params["offset"] : NULL;
		$where = $this->_query($params);
		// 정렬관련
		if(isset($params["oType"]) && isset($params["oKey"])){
			$this->usdb->order_by($params["oKey"], $params["oType"]);
		}
		return $this->usdb->get_where($this->table[0], $where, $limit, $offset)->result_array();
	}

	public function _select_row($where) {
		return $this->usdb->where($where)->get($this->table[0], 1)->row_array();
	}

	public function _insert($data) {
		return $this->usdb->insert($this->table[0], $data);
	}

	public function _delete($where) {
		$this->usdb->where($where);
		$this->usdb->delete($this->table[0]);
	}

	public function _update($data, $where) {
		return $this->usdb->update($this->table[0], $data, $where);
	}
	
	public function _set($params) {
		$this->usdb->set($params, NULL, FALSE);
		return $this->usdb->update($this->table[0]);
	}

	public function _replace($data) {
		return $this->usdb->replace($this->table[0], $data);
	}
}