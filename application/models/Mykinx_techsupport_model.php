<?php
class Mykinx_techsupport_model extends CI_Model {
	protected $table = array("tWorkRequest", "tCompanyContact");

	private function _query($params) {
		if(isset($params["secKey"]) && isset($params["secTxt"])) {
			$this->db->like($params["secKey"], $params["secTxt"]);
		}
		if(isset($params["nContractSeq"])) {
			$where["nContractSeq"] = $params["nContractSeq"];
		}
		
		if(isset($params["nWorkRequestSeq"])) {
			$where["nWorkRequestSeq"] = $params["nWorkRequestSeq"];
		}
		
		if(isset($params["sStatus"])) {
			$where["sStatus"] = $params["sStatus"];
		}
		
		$where["1"] = "1";
		return $where;
	}

	public function _select_cnt($params=array(), $index='0') {		
		$where = $this->_query($params);
		$this->db->where($where);
		$this->db->from($this->table[$index]);
		return $this->db->count_all_results();
	}

	
	public function _select_list($params=array(), $index='0') {
		$limit = (isset($params["limit"])) ? $params["limit"] : NULL;
		$offset = (isset($params["offset"])) ? $params["offset"] : NULL;
		$where = $this->_query($params);
		
		// 정렬관련
		if(isset($params["oKey"][0]) && isset($params["oType"][0]))
		{
			$orderCnt = count($params["oKey"]);
			for($i = 0; $i < $orderCnt ; $i++)
			{
				$this->db->order_by($params["oKey"][$i], $params["oType"][$i]);
			}
		}
		return $this->db->get_where($this->table[$index], $where, $limit, $offset)->result_array();
	}
	 
	/*
	public function getTechSupportList($param=array(),$secParams=array(), &$listCnt)//SP]reWorkList
	{
		$sql ="";
		$sql .= "SELECT a.nWorkRequestSeq, a.sManagerName, a.sInternalPhone, a.dtExpectStartDate, a.dtStartDate, a.dtEndDate, a.dtCreateDate,  a.nWorkType, sActionType, sResultStatus, ";
		$sql .= "(SELECT b.sVisitorName FROM tCompanyVisitor AS b WHERE b.nWorkRequestSeq = a.nWorkRequestSeq order by b.nCompanyVisitorSeq asc limit 1) as strVisitor, ";
		$sql .= "(SELECT count(b.nCompanyVisitorSeq) FROM tCompanyVisitor AS b WHERE b.nWorkRequestSeq = a.nWorkRequestSeq) as strVisitorCount ";
		$sql .= "FROM ".$this->table[0]." AS a ";
		$sql .= "WHERE a.sResultStatus IN ('Y','S','F') and a.nContractSeq=".$param['nContractSeq']." ";
        $sql .= "and datediff(now(), a.dtCreatedate) <= " . ($param["strTerm"] * 30) . " ";
		#$sql .= "and (MONTH(a.dtCreateDate)- MONTH(now())) <=".$param["strTerm"]." ";
		if($param["workType"]!= "ALL")
		{
			$sql .= "and a.sActionType='".$param["workType"]."' ";
		}
		if($param["nServiceType"]!= "ALL")
		{
		//	$sql .= "and a.nServiceType='".$param["nServiceType"]."' ";	
		}
		$sql .= "order by a.nWorkRequestSeq desc ";
		
		$query = $this->db->query($sql);
		$listCnt = $query->num_rows();
		
		if(isset($secParams["limit"]))
		{
			$limit = $secParams["limit"];
			$sql .= "limit ".$limit." ";
		}
		if(isset($secParams["offset"]))
		{
			$offset = $secParams["offset"];
			$sql .= "offset ".$offset." ";
		}
		
		//echo $sql;
		$query = $this->db->query($sql);		
		$result = $query->result_array();
		
		return $result;
	}
	*/
	
	
	//140827 보안강화
	public function getTechSupportList($param=array(),$secParams=array())//SP]reWorkList
	{
		$this->db->select('a.nWorkRequestSeq, a.sManagerName, a.sInternalPhone, a.dtExpectStartDate, a.dtStartDate, a.dtEndDate, a.dtCreateDate,  a.nWorkType, a.sActionType, a.sResultStatus');
		$this->db->select('(SELECT b.sVisitorName FROM tCompanyVisitor AS b WHERE b.nWorkRequestSeq = a.nWorkRequestSeq order by b.nCompanyVisitorSeq asc limit 1) as strVisitor');
		$this->db->select('(SELECT count(b.nCompanyVisitorSeq) FROM tCompanyVisitor AS b WHERE b.nWorkRequestSeq = a.nWorkRequestSeq) as strVisitorCount');
		//$this->db->from('tWorkRequest as a');
		$bindResultStatus = array('Y', 'S', 'F');
		$this->db->where_in('a.sResultStatus', $bindResultStatus );
		$this->db->where('a.nContractSeq',$param['nContractSeq']);
		$this->db->where('datediff(now(), a.dtCreatedate) <= ', $param["strTerm"] * 30);
		if($param["workType"]!= "ALL")
		{
			$this->db->where('a.sActionType',$param["workType"]);
		}
		$this->db->order_by('a.nWorkRequestSeq','desc');
		
		
		if(isset($secParams["limit"]))
		{
			$limit = $secParams["limit"];
		}
		if(isset($secParams["offset"]))
		{
			$offset = $secParams["offset"];
		}
		
		$query = $this->db->get('tWorkRequest as a', $limit, $offset);
		$result = $query->result_array();
		

		return $result;
		
	}
	//140827 보안강화
	public function getTechSupportListCnt($param=array(),$secParams=array())//SP]reWorkList
	{
		$this->db->select('a.nWorkRequestSeq, a.sManagerName, a.sInternalPhone, a.dtExpectStartDate, a.dtStartDate, a.dtEndDate, a.dtCreateDate,  a.nWorkType, a.sActionType, a.sResultStatus');
		$this->db->select('(SELECT b.sVisitorName FROM tCompanyVisitor AS b WHERE b.nWorkRequestSeq = a.nWorkRequestSeq order by b.nCompanyVisitorSeq asc limit 1) as strVisitor');
		$this->db->select('(SELECT count(b.nCompanyVisitorSeq) FROM tCompanyVisitor AS b WHERE b.nWorkRequestSeq = a.nWorkRequestSeq) as strVisitorCount');
		$this->db->from('tWorkRequest as a');
		$bindResultStatus = array('Y', 'S', 'F');
		$this->db->where_in('a.sResultStatus', $bindResultStatus );
		$this->db->where('a.nContractSeq',$param['nContractSeq']);
		$this->db->where('datediff(now(), a.dtCreatedate) <= ', $param["strTerm"] * 30);
		if($param["workType"]!= "ALL")
		{
			$this->db->where('a.sActionType',$param["workType"]);
		}
		$this->db->order_by('a.nWorkRequestSeq','desc');
		$query = $this->db->get();
		$listCnt = $query->num_rows();
		//echo "listcnt : ";echo $listCnt; echo "<br>---<br>";	
		return $listCnt;
	
	}
	 
	public function InsertWorkRequest($data)//SP]reWorkIDCInsert
	{
		$sql = "SELECT nCompanyContactSeq, sCompanyName, sManagerName, sDepartmentName, sRank, sInternalphone, sfax, sMobilephone, sEmail ";
		$sql .= "FROM tCompanyContact ";
		//$sql .= "WHERE nContractSeq='".$data["nContractSeq"]."' and nCompanyContactseq='".$data["companycontact"]."' ";
		$sql .= "WHERE nContractSeq=? and nCompanyContactseq=? ";//140827 보안강화
		
		$binds = array($data["nContractSeq"], $data["companycontact"]);
		$query = $this->db->query($sql,$binds);//140827 보안강화
		$result = $query->result_array();
		//print_r($result);
	
		$insertData = array(
				'nCompanySeq'=>$data["nCompanySeq"],
				'nContractSeq'=>$data["nContractSeq"],
				'nServiceType'=>$data["nServiceType"],
				'sCompanyName'=>$result[0]["sCompanyName"],
				'sManagerName'=>$result[0]["sManagerName"],
				'sDepartmentName'=>$result[0]["sDepartmentName"],
				'sRank'=>$result[0]["sRank"],
				'sfax'=>$result[0]["sfax"],
				'sInternalphone'=>$result[0]["sInternalphone"],
				'sMobilephone'=>$result[0]["sMobilephone"],
				'sEmail'=>$result[0]["sEmail"],
				'nWorkType'=>$data["techWorkType"],
				'sActionType'=>"ETC",
				'sResultStatus'=>"Y",
				'nMemberSeq'=>$result[0]["nCompanyContactSeq"],
				'sContent'=>$data["request"]//,
				//'dtExpectStartDate'=>"",
				//'dtExpectEndDate'=>""
		);
		$this->db->insert($this->table[0],$insertData);
		return $this->db->insert_id();
	}
	
	public function _select_row($where, $index='0') {
		//return $this->db->where($where)->get($this->table[$index], 1)->row_array();
		$query = $this->db->select('WR.*, Mem.sMemberName')
						->from($this->table[$index]." as WR")
						->join("tMember as Mem","WR.nMemberSeq=Mem.nMemberSeq")
						->where($where)
						->limit(1)
						->get();
		return $query->row_array();
	}
	
	//171106. ---------------------------------------------------------------
	//작업이 완료되어야 작업자 정보가 등록되어 join 의 조건이 성립된다. 
	//status 에 따라서 query 분리
	//Status : 처리결과(Y:신청완료, S:작업시작, F:작업완료, C:작업취소, X:삭제)
	public function _select_row_status($where, $index='0', $Status = 'F') 
	{	
		if($Status=='Y')
		{
			$query = $this->db->select('WR.*')
					->from($this->table[$index]." as WR")
					//->join("tMember as Mem","WR.nMemberSeq=Mem.nMemberSeq")
					->where($where)
					->limit(1)
					->get();
		}
		else 
		{
			$query = $this->db->select('WR.*, Mem.sMemberName')
			->from($this->table[$index]." as WR")
			->join("tMember as Mem","WR.nMemberSeq=Mem.nMemberSeq")
			->where($where)
			->limit(1)
			->get();
		}
		
		//echo $this->db->last_query(); 
		return $query->row_array();
	}
	
	//해당 작업의 상태값 반환 171106
	public function _select_Resultstatus($nWorkRequestSeq, $index='0')
	{
		$query = $this->db->select('WR.sResultStatus')
				->from($this->table[$index]." as WR")
				//->join("tMember as Mem","WR.nMemberSeq=Mem.nMemberSeq")
				->where('WR.nWorkRequestSeq=',$nWorkRequestSeq)
				->limit(1)
				->get();
	
		//echo $this->db->last_query();
		return $query->row_array();
	}
	/////-----------------------------------

	public function _insert($data, $index='0') {
		return $this->db->insert($this->table[$index], $data);
	}

	public function _delete($where, $index='0') {
		$this->db->where($where);
		$this->db->delete($this->table[$index]);
	}

	public function _update($data, $where, $index='0') {
		return $this->db->update($this->table[$index], $data, $where);
	}
	
	public function _set($params) {
		$this->db->set($params, NULL, FALSE);
		return $this->db->update($this->table[0]);
	}

	public function _replace($data) {
		return $this->db->replace($this->table[0], $data);
	}
}