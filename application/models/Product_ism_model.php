<?php
class Product_ism_model extends CI_Model { //intranet Service_item_model.php 대체
	protected $table = array("tServiceItem");
	
	private $t_product     		= 'CONTRACT.tProduct';
	private $t_productCate    = 'CONTRACT.tProductCategory';
	private $v_product			= 'CONTRACT.vProduct';
	
	function __construct(){
		//생성자
		parent::__construct();
	
			
		//ism 실DB 적용
		$ismdb = $this->load->database('ism',true);
		$this->ismdb = $ismdb;
	}
	 
	//select_row
	public function get_product_bySeq($nSeq)
	{
		$query = $this->ismdb
		->select('VP.sProductType as sServiceType,VP.sEngProductType as sEngServiceType, VP.sDetailType as sServiceName, VP.sEngDetailType as sEngServiceName, VP.sName as sServiceDetail, VP.sEngName as sEngServiceDetail, VP.*', FALSE)
		->from($this->v_product.' as VP')
		->where('nProductSeq',$nSeq)
		->limit(1)
		->get();
	
		if($query != FALSE)
		{
			if($query->num_rows() > 0)
				return $query->row_array();
				else
					$this->errMsg = "조회된 데이터가 없습니다.";
		}
		else
		{
			$error = $this->ismdb->error();
			$this->errMsg = $error['message'];
		}
	}
	////////////////////////////////////////////////////
	
	private function _query($params) {
		if(isset($params["secKey"]) && isset($params["secTxt"])) {
			$this->db->like($params["secKey"], $params["secTxt"]);
		}
		$where["1"] = "1";
		return $where;
	}

	public function _select_cnt($params=array()) {
		$where = $this->_query($params);
		$this->db->where($where);
		$this->db->from($this->table[0]);
		return $this->db->count_all_results();
	}

	public function _select_list($params=array()) {
		$limit = (isset($params["limit"])) ? $params["limit"] : NULL;
		$offset = (isset($params["offset"])) ? $params["offset"] : NULL;
		$where = $this->_query($params);
		// 정렬관련
		if(isset($params["oType"]) && isset($params["oKey"])){
			$this->db->order_by($params["oKey"], $params["oType"]);
		}
		return $this->db->get_where($this->table[0], $where, $limit, $offset)->result_array();
	}

	/*
	public function _select_row($where) {
		return $this->db->where($where)->get($this->table[0], 1)->row_array();
	}
	*/

	public function _insert($data) {
		return $this->db->insert($this->table[0], $data);
	}

	public function _delete($where) {
		$this->db->where($where);
		$this->db->delete($this->table[0]);
	}

	public function _update($data, $where) {
		return $this->db->update($this->table[0], $data, $where);
	}
	
	public function _set($params) {
		$this->db->set($params, NULL, FALSE);
		return $this->db->update($this->table[0]);
	}

	public function _replace($data) {
		return $this->db->replace($this->table[0], $data);
	}
}