<?php
class Va_event_model extends CI_Model { //가상계좌 event 관련 model


	private $t_vaevent    	 	= 'CONTRACT.tVirtualAccountEvent';
	private $t_taxinvmem		= 'BILL.tTaxInvoiceMember';

	function __construct(){
		//생성자
		parent::__construct();


		//ism 실DB 적용
		$ismdb = $this->load->database('ism',true);
		$this->ismdb = $ismdb;
	}

	//////////////////////////////////////
	//거래명세서/세금계산서 수신자 목록 검색
	public function get_taxinvoicemember($nTaxInvoiceSeq, $sEmail)
	{
		$query = $this->ismdb
					->select('*', FALSE)
					->from($this->t_taxinvmem)
					->where('nTaxInvoiceSeq',$nTaxInvoiceSeq)
					->where('sEmail',$sEmail)
					->where('sDeleteYN','N')
					->limit(1)
					->get();

		//echo  $this->ismdb->last_query();

		if ($query !== FALSE)
		{
			if($query->num_rows() > 0)
			{
				return $query->row_array();
			}
			else
			{
				$this->errMsg = "조회내용이 없습니다.";
			}
		}
		else
		{
			$error = $this->ismdb->error();
			$this->errMsg = $error['message'];
		}
	}

	//event table 검색
	public function get_vaevent($sEmail)
	{
		$query = $this->ismdb
					->select('*', FALSE)
					->from($this->t_vaevent)
					->where('sCustomerEmail',$sEmail)
					->where('bDel','N')
					->limit(1)
					->get();

		//echo  $this->ismdb->last_query(); die;

		if ($query !== FALSE)
		{
			if($query->num_rows() > 0)
			{
				return $query->row_array();
			}
			else
			{
				return null;
				//$this->errMsg = "조회내용이 없습니다.";
			}
		}
		else
		{
			//$error = $this->ismdb->error();
			//$this->errMsg = $error['message'];
			return null;
		}
	}

	public function insert_vaevent($data)
	{
		return $this->ismdb->insert($this->t_vaevent,$data);
	}
}