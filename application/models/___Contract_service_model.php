<?php
class Contract_service_model extends CI_Model {
	protected $table = array("tContractService", "tContract", "tServiceItem");
	private function _query($params) {
		$this->db->from($this->table[0]." as A");
		$this->db->join($this->table[1]." as B", "A.nContractSeq = B.nContractSeq");
		$this->db->join($this->table[2]." as C", "A.nServiceCode = C.nServiceCode");
		if(isset($params["nContractSeq"])) {
			$where["A.nContractSeq"] = $params["nContractSeq"];
		}
		
		if(isset($params["sServiceType"])) {
			$where["C.sServiceType"] = $params["sServiceType"];
		}
		
		if(isset($params["sStatus"]) && $params["sStatus"]<>'A') {
			$where["A.sStatus"] = $params["sStatus"];
		}
		
		
		if(isset($params["main"])) {
			$this->db->where("(A.dtEndDate - current_date > 0 or A.dtEndDate='0000-00-00')" , NULL, FALSE);
		}
		
		
		if(isset($params["sDate"])) {
			if(isset($params["termType"]) && $params["termType"] === "dtStartDate") {
				$where["A.dtStartDate >="] = $params["sDate"];
			} else if(isset($params["termType"]) && $params["termType"] === "dtEndDate") {
				$where["A.dtEndDate >="] = $params["sDate"];
			} else {
				$where["A.dtStartDate >="] = $params["sDate"];
			}
		}
		
		if(isset($params["eDate"])) {
			if(isset($params["termType"]) && $params["termType"] === "dtStartDate") {
				$where["A.dtStartDate <="] = $params["eDate"];
			} else if(isset($params["termType"]) && $params["termType"] === "dtEndDate") {
				$where["A.dtEndDate <="] = $params["eDate"];
			} else {
				$where["A.dtStartDate <="] = $params["eDate"];
			}
		}
		
		if(isset($params["select"])) {
			$this->db->select($params["select"]);
		}
		
		if(isset($params["orderby"])) {
			$this->db->order_by($params["orderby"]);
		}
		
		
		
		if(isset($params["secKey"]) && isset($params["secTxt"])) {
			$this->db->like($params["secKey"], $params["secTxt"]);
		}
		$where["1"] = "1";
		return $where;
	}

	public function _select_cnt($params=array()) {
		$where = $this->_query($params);
		$this->db->where($where);
		return $this->db->count_all_results();
	}

	public function _select_list($params=array()) {
		$limit = (isset($params["limit"])) ? $params["limit"] : NULL;
		$offset = (isset($params["offset"])) ? $params["offset"] : NULL;
		$where = $this->_query($params);
		// 정렬관련
		if(isset($params["oType"]) && isset($params["oKey"])){
			$this->db->order_by($params["oKey"], $params["oType"]);
		}
        $retObj = $this->db->get_where(null, $where, $limit, $offset)->result_array();
      
		return $retObj;
	}
	
	public function _select_row2($where) {
		$this->db
		->limit(1)
		->select("IF(substring(nServiceCode,1,1)=1, 1, 2) serviceType ", FALSE)
		->where($where)
		->order_by("nContractServiceSeq DESC");
		
		return $this->db->get($this->table[0], 1)->row_array();
	}

	public function _select_row($where) {
		return $this->db->where($where)->get($this->table[0], 1)->row_array();
	}

	public function _insert($data) {
		return $this->db->insert($this->table[0], $data);
	}

	public function _delete($where) {
		$this->db->where($where);
		$this->db->delete($this->table[0]);
	}

	public function _update($data, $where) {
		return $this->db->update($this->table[0], $data, $where);
	}
	
	public function _set($params) {
		$this->db->set($params, NULL, FALSE);
		return $this->db->update($this->table[0]);
	}

	public function _replace($data) {
		return $this->db->replace($this->table[0], $data);
	}
	
	
}