<?php
class Mykinx_findidpw_model extends CI_Model {
	protected $table = array("tFindAccount","tFilesInfo");
 
	public function _insertFile($data, $req_insert_id, $index='1') { 
		
		# 1) upload((사업자등록증사본/통장사본)) 파일정보를 등록한다.
		if(!is_null($data['files']))
		{
			foreach($data['files'] as $key=>$row)
			{
				$fileinfo = array(
						"sFileName"	=> $row['file_name'],
						"sOriginal"	=> $row['orig_name'],
						"sPath"		=> UPLOAD_TYPE_AUTH.$data['substr'],
						"sFileType"	=> $row['file_type'],
						"nSubFileSeq" => $req_insert_id,
						"sTblName" => "tFindAccount" 
				);
				$this->db->insert($this->table[$index], $fileinfo);
			}
		}
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
 
	
	public function _insert($data, $index='0') 
	{
		$this->db->insert($this->table[$index], $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	
	public function _delete($where, $index='0') {
		$this->db->where($where);
		$this->db->delete($this->table[$index]);
		//echo "delete insertreq <br>";
		//echo $this->db->last_query();
	}
	
}