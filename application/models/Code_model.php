<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Code_model
 *
 * 서비스 정보, 각종 공통 코드정보를 처리한다.
 *
 * @package		KINX intranet
 * @author		SYZ
*/
class Code_model extends CI_Model {
	private $t_syscode			= 'tSysCode';
	public  $errMsg				= '';

	function __construct(){
		//생성자
		parent::__construct();
		
		//$CI =& get_instance(); //CodeIgniter에 내장된 리소스 할당		
		//$this->t_syscode = $CI->config->item('db_table_prefix', 'kinxconfig').$this->t_syscode;
		//$this->t_member = $CI->config->item('db_table_prefix', 'kinxconfig').$this->t_member;
	}
	 
	/**
	 * GET_CODE_LIST
	 *
	 * 공통코드 목록을 추출한다.
	 *
	 * @param string		$group_cd		코드그룹코드
	 * @param string		$dropdown		검색창에 select박스로 활용할 경우 TRUE
	 * @param string		$default			select박스일경우 default value값. NULL값이면 default  삭제.
	 * @return NULL or Object
	 */
	public function get_code_list($group_cd, $dropdown, $default)
	{
	
		if(empty($group_cd) || is_null($group_cd)) $this->errMsg = "기본데이터가 넘어오지 않았습니다.";
 		else
 		{
			#쿼리추출
			$query = $this->db	->select("sCode, sCodeName")
									->where('sGroupCode',	$group_cd)
									->where('sSearchFlag','Y')
									->order_by('nOrderNum', 'ASC')
									->get($this->t_syscode);
				
			#쿼리 결과 확인및 결과값 return.
			if ($query->num_rows() > 0)
			{
				$result = $query->result();
				#select박스에 사용될 경우
				if($dropdown)
				{
					return $this->set_dropdown($result, array("sCode", "sCodeName"), $default);
				}
				#결과목록을 전송할 경우
				else
				{
					return $result;
				}
			}
			else
			{
				$this->errMsg = "조회된 데이터가 없습니다.";
			}
		}
		return NULL;
	}
	
	/**
	 * GET_CODE_LIST_ARRAY
	 *
	 * 공통코드 목록을 추출한다. 
	 *
	 * @param string		$group_cd		코드그룹코드
	 * @param string		$dropdown		검색창에 select박스로 활용할 경우 TRUE
	 * @param string		$default			select박스일경우 default value값. NULL값이면 default  삭제.
	 * @return NULL or Object
	 */
	public function get_code_list_array($group_cd, $dropdown, $default)
	{
	
		if(empty($group_cd) || is_null($group_cd)) $this->errMsg = "기본데이터가 넘어오지 않았습니다.";
		else
		{
		#쿼리추출
		$query = $this->db	->select("sCode, sCodeName")
							->where('sGroupCode',	$group_cd)
							->where('sSearchFlag','Y')
							->order_by('nOrderNum', 'ASC')
							->get($this->t_syscode);
	
		#쿼리 결과 확인및 결과값 return.
		if ($query->num_rows() > 0)
		{
			$result = $query->result_array();
			#select박스에 사용될 경우
			if($dropdown)
			{
			return $this->set_dropdown($result, array("sCode", "sCodeName"), $default);
			}
			#결과목록을 전송할 경우
			else
			{
				return $result;
			}
		}
		else
		{
			$this->errMsg = "조회된 데이터가 없습니다.";
		}
	}
		return NULL;
	}
	
	
	 
	 
	/**
	 * get_month_list
	 *
	 *
	 * @param boolean	$month			여부
	 * @param string		$dropdown		검색창에 select박스로 활용할 경우 TRUE
	 * @param string		$default			select박스일경우 default value값. NULL값이면 default  삭제.
	 * @return NULL or Object
	 */
	public function get_month_list($month=TRUE, $dropdown, $default)
	{
		
		if($dropdown)
		{
			//$codelist = (is_null($default)) ? array() : array(''=>$default);
			for($i=1;12>=$i;$i++){
				$codelist[$i] = $i;
			}
		}
		#결과목록을 전송할 경우
		else{
			return NULL;
		}
		return $codelist;
	}

	public function get_day_list($day=TRUE, $dropdown, $default)
	{
		
		if($dropdown)
		{
			//$codelist = (is_null($default)) ? array() : array(''=>$default);
			for($i=1;$i<=31;$i++){
				$codelist[$i] = $i;
			}
		}
		#결과목록을 전송할 경우
		else{
			return NULL;
		}
		return $codelist;
	}
    
	public function get_time_list($type, $dropdown)
	{
		if($dropdown)
		{
            if($type == 'hh') {
                $cnt = 24;
                for($i=0;$i<=$cnt;$i++){
                    if($i<10) {
                        $codelist[$i] = "0".$i;
                    } else {
                        $codelist[$i] = $i;
                    }
                }                
            } else {
                $cnt = 60;
                for($i=0;$i<=$cnt;$i+=10){
                    if($i<10) {
                        $codelist[$i] = "0".$i;
                    } else {
                        $codelist[$i] = $i;
                    }                    
                }                
            }
		}
		#결과목록을 전송할 경우
		else{
			return NULL;
		}
		return $codelist;
	}    
	
	private function set_dropdown_year($result, $fildset, $default)
	{
	#default 값이 NULL이 아닌경우만 설정
	$codelist = (is_null($default)) ? array() : array(''=>$default);
	
	foreach($result as $row){
		$fildset_tmp = array();
		$fildset_tmp = explode("-", $row->$fildset[1]);
		$codelist[$row->$fildset[0]] = $fildset_tmp[0];
	}
	return $codelist;
	}
	
	private function set_dropdown($result, $fildset, $default)
	{
		#default 값이 NULL이 아닌경우만 설정
		$codelist = (is_null($default)) ? array() : array(''=>$default);
		foreach($result as $row){
			$codelist[$row->$fildset[0]] = $row->$fildset[1];
		}
		return $codelist;
	}

	
	/**
	 * Get error message.
	 *  저장된 에러 메세지를 전달한다.
	 *
	 * @return	string
	 */
	function get_error_message()
	{
		return $this->errorMsg;
	}
	
}

/* End of file Code_model.php */
