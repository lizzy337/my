<?php
class Contract_model extends CI_Model {
	protected $table = array("tContract");
	
	
	function __construct(){
		//생성자
		parent::__construct();
	
		 
		//ism 실DB 적용
		$ismdb = $this->load->database('ism',true);
		$this->ismdb = $ismdb;
	}
	
	private function _query($params) 
	{
		if(isset($params["secKey"]) && isset($params["secTxt"])) {
			$this->db->like($params["secKey"], $params["secTxt"]);
		}
		$where["1"] = "1";
		return $where;
	}

	public function _select_cnt($params=array()) {
		$where = $this->_query($params);
		$this->db->where($where);
		$this->db->from($this->table[0]);
		return $this->db->count_all_results();
	}

	public function _select_list($params=array()) {
		$limit = (isset($params["limit"])) ? $params["limit"] : NULL;
		$offset = (isset($params["offset"])) ? $params["offset"] : NULL;
		$where = $this->_query($params);
		// 정렬관련
		if(isset($params["oType"]) && isset($params["oKey"])){
			$this->db->order_by($params["oKey"], $params["oType"]);
		}
		return $this->db->get_where($this->table[0], $where, $limit, $offset)->result_array();
	}

	public function _select_row($where) {
		return $this->db->where($where)->get($this->table[0], 1)->row_array();
	}

	public function _insert($data) {
		return $this->db->insert($this->table[0], $data);
	}

	public function _delete($where) {
		$this->db->where($where);
		$this->db->delete($this->table[0]);
	}

	public function _update($data, $where) {
		
		if(isset($data['sPassword']))
		{
			//intranet, ism 동시 업데이트 해야하므로 password hash 처리는 controller에서 처리 170720
			//$data['sPassword'] = password_hash($data['sPassword'], PASSWORD_BCRYPT,array("cost" => 10, 'salt'=>PASSWORD_SALT) );
			unset($data['rePassword']);	
		}		
		
		return $this->db->update($this->table[0], $data, $where);
	}
	
	public function _set($params) {
		$this->db->set($params, NULL, FALSE);
		return $this->db->update($this->table[0]);
	}

	public function _replace($data) {
		return $this->db->replace($this->table[0], $data);
	}
	
	public function get_companyStatus($params){
		$query = $this->db->get_where('tCompany', array('nCompanySeq'=>$params), 0, 0);
		return $query->result_array();
	}
	
	public function get_contractStatus($params){
		$query = $this->db->get_where('tContract', array('nContractSeq'=>$params), 0, 0);
		return $query->result_array();
	}
	
	public function get_myCardPayment(){
		$query = $this->db->select('sMemberId')->get_where('tContract', array('nMyCardPayment'=>1), 0, 0);
		return $query->result_array();
	}
	
}