<?php
class Bill_list_model extends CI_Model {	
	protected $table = array("tBillList", "tBill", "tBillListDetail", "tEngServiceItem", "tContract", "tExchangeRate");//20140716 tServiceItem->tEngServiceItem
	
	private $t_billType     = 'BILL.tBillType';
	
	function __construct(){
		//생성자
		parent::__construct();
			
		//ism 실DB 적용
		$ismdb = $this->load->database('ism',true);
		$this->ismdb = $ismdb;
	}
	
	private function _query($params) {
		$this->db->from($this->table[0]." as A");
		$this->db->join($this->table[1]." as B", "A.nBillSeq = B.nBillSeq");
		$this->db->join($this->table[2]." as C", "B.nBillSeq = C.nBillSeq");
		$this->db->join($this->table[3]." as D", "C.nServiceCode = D.nServiceCode");
		if(isset($params["nContractSeq"])) {
			$where["B.nContractSeq"] = $params["nContractSeq"];
		}

		if(isset($params["nBillListSeq"])) {
			$where["A.nBillListSeq"] = $params["nBillListSeq"];
		}
		
		if(isset($params["select"])) {
			$this->db->select($params["select"]);
		}
		
		if(isset($params["secKey"]) && isset($params["secTxt"])) {
			$this->db->like($params["secKey"], $params["secTxt"]);
		}
		$where["1"] = "1";
		return $where;
	}

	public function _select_cnt($params=array()) {
		$where = $this->_query($params);
		$this->db->where($where);
		return $this->db->count_all_results();
	}

	public function _select_list($params=array()) {
		$limit = (isset($params["limit"])) ? $params["limit"] : NULL;
		$offset = (isset($params["offset"])) ? $params["offset"] : NULL;
		$where = $this->_query($params);
		// 정렬관련
		if(isset($params["oType"]) && isset($params["oKey"])){
			$this->db->order_by($params["oKey"], $params["oType"]);
		}
		return $this->db->get_where(null, $where, $limit, $offset)->result_array();
	}

	public function _select_row($where, $key=0) {
		return $this->db->where($where)->get($this->table[$key], 1)->row_array();
	}

	public function _insert($data) {
		return $this->db->insert($this->table[0], $data);
	}

	public function _delete($where) {
		$this->db->where($where);
		$this->db->delete($this->table[0]);
	}

	public function _update($data, $where) {
		return $this->db->update($this->table[0], $data, $where);
	}
	
	public function _set($params) {
		$this->db->set($params, NULL, FALSE);
		return $this->db->update($this->table[0]);
	}

	public function _replace($data) {
		return $this->db->replace($this->table[0], $data);
	}
	
        //사용안함.
	public function _get_payment_count($contractSeq)
	{
		$query = $this->db
		->select("count(1) cnt", false)
		->from($this->table[0]." as A")
		->join($this->table[1]." as B", "A.nBillSeq = B.nBillSeq")
		->join($this->table[4]." as C", "B.nContractSeq = C.nContractSeq")
		->where(array(	"A.sStatus =" => "Y","A.nReceiptPrice >" => 0,"C.nContractSeq=" => $contractSeq))
		->where("A.dtReceiptDate >= now() - interval 3 month")
		->order_by("A.dtReceiptDate DESC")
		->get();
		
		return $query->row()->cnt;
	}
	
	public function _get_chargeprice($contractSeq)
	{
		$query = $this->db
		->select("nChargePrice", false)
		->from($this->table[0]." as A")
		->join($this->table[1]." as B", "A.nBillSeq = B.nBillSeq")
		->join($this->table[4]." as C", "B.nContractSeq = C.nContractSeq")
		->where(array(	"A.sStatus =" => "Y", "C.nContractSeq=" => $contractSeq))
		->where("A.dtChargeDate <= now()")
		->order_by("A.dtChargeDate DESC")
		->limit(1)
		->get();
		
		if ($query->num_rows() > 0) return $query->row()->nChargePrice;
		else return 0; 
	}
	//added by lizzy 20141007. 환율타입 구하기
	public function _get_exchangeRatetype($contractSeq)
	{
		$query = $this->db
		->select("sExchangeRateType", false)
		->from($this->table[1]." as A")
		->where("A.nContractSeq=" ,$contractSeq)
		->limit(1)
		->get();
	
		if ($query->num_rows() > 0) return $query->row()->sExchangeRateType;
		else return 0;
	}
	
	//ism 에서 가져오기
	public function _get_exchangeRatetype_ism($nAccountSeq)
	{
		$query = $this->ismdb
		->select("sCurrencyType, sCurrencyType as  sExchangeRateType", false)
		->from($this->t_billType." as A")
		->where("A.nAccountSeq=" ,$nAccountSeq)
		->limit(1)
		->get();
	
		if ($query->num_rows() > 0) return $query->row()->sExchangeRateType;
		else return 0;
	}
	
	//added by lizzy 20141007 환율적용 history에서 가장 최근에 적용된 환율갑값 구하기
	public function _get_exchangeRecentApplyRate($contractSeq)
	{
		$query = $this->db
		->select("nMoney", false)
		->from($this->table[5]." as A")
		->where("A.nContractSeq=" ,$contractSeq)
		->where("A.dtChargeDate <= now()")
		->order_by("A.dtChargeDate DESC")
		->limit(1)
		->get();
	
		if ($query->num_rows() > 0) return $query->row()->nMoney;
		else return 0;
	}
	
	//added by lizzy 20141013 지정된 chargeDate의 가장최근 환율 구하기
	public function _get_chargeDateApplyRate($contractSeq, $chargedate)
	{
		$query = $this->db
		->select("nMoney", false)
		->from($this->table[5]." as A")
		->where("A.nContractSeq=", $contractSeq)
		->like("A.dtChargeDate", $chargedate,'after')
		->order_by("A.dtCreateDate DESC")
		->limit(1)
		->get();
		
		if ($query->num_rows() > 0) return $query->row()->nMoney;
		else return 0;
	}
	
	public function _get_lq()
	{
		return	$this->db->last_query();
	}
	
}