<?php
class Zipcode_model extends CI_Model {
    private $success;
    private $results;
    private $error;
    private $http_code;
    public function __construct(){
        parent::__construct();
        $this->load->library('rest');
        $config = array('server' => 'http://www.kinx.net/');
        $this->rest->initialize($config);
    }
    //결과값 초기화
    private function _resetReturn(){
        $this->success = true;
        $this->http_code= null;
        $this->results = null;
        $this->error = null;
    }
    //결과값 반환
    public function return_result($mime){
        $rtnArray = array(
            'success' => $this->success,
            'http_code' => $this->http_code,
            'results' => $this->results,
            'error' => $this->error
        );
        switch($mime){
            case 'json':
                return json_encode($rtnArray);
                break;
            case 'php':
                return $rtnArray;
                break;
        }
    }
    public function getSuccess(){
        return $this->success;
    }
    
	/* prg 161114 ㅇ우편번호 관련
    //도로명
    function get_post_code_xml_by_api2($query){
    	$this->_resetReturn();
    	$query = urlencode($query);
    	$api_key = urlencode("1SV1550Vhv3eegTYQW2b6vt6a8DsdEbXNEJ8rmU760iu7pvgKG+tYSMJ/rqnmhQvmUVlglAh4B3vjk755TCjQQ==");
    	$epost_url = "/postal/retrieveNewAdressService/retrieveNewAdressService/getNewAddressList?ServiceKey=$api_key&searchSe=road&srchwrd=".$query;
    
    	$config = array('server' => "http://openapi.epost.go.kr");
    	$this->load->library('rest');
    	$this->rest->initialize($config);
    	$xml = $this->rest->get($epost_url);
    	if($this->rest->status()==200){
    		$aryRet = json_decode(json_encode($xml),true);
    		$this->http_code = $this->rest->status();
    		if(isset($aryRet['error_code'])){
    			$this->success = false;
    			$this->error = $aryRet;
    		}else{
    			$this->success = true;
    			$this->http_code = $this->rest->status();
    			$this->results = $aryRet;
    		}
    	}else{
    		$this->success = false;
    		$this->http_code = $this->rest->status;
    		$this->error = Array(
    				'http_code' => $this->http_code,
    				'message' => '주소 정보를 가져오는 중 문제가 발생하였습니다.'
    				);
    	}
    	return $this;
    	//$this->rest->debug();
    }
    */
    
	//도로명
    function get_post_code_xml_by_api2($query){
    	$this->_resetReturn();
    	$epost_url = "/postal/retrieveNewAdressAreaCdService/retrieveNewAdressAreaCdService/getNewAddressListAreaCd";
    	$epost_url .= "?" . urlencode("ServiceKey") ."=pmbPq%2FJV3zbwmd3MJiVz6KaH7EOlQDYC5OqHXok98p5%2BqvckxTWLAad7JyZ6hIYJtMH1jsVQgF1UwyTuXFsjDw%3D%3D";//servicekey
    	$epost_url .= "&". urlencode("searchSe")."=". urlencode("road"); /*검색구분(읍/면/동, 도로명, 우편번호)*/
    	$epost_url .= "&". urlencode("srchwrd")."=". urlencode($query); /*검색어*/
    	
        
    	$config = array('server' => "http://openapi.epost.go.kr");
    	$this->load->library('rest');
    	$this->rest->initialize($config);
    	$xml = $this->rest->get($epost_url);
    	if($this->rest->status()==200){
    		$aryRet = json_decode(json_encode($xml),true);
    		$this->http_code = $this->rest->status();
    		if(isset($aryRet['error_code'])){
    			$this->success = false;
    			$this->error = $aryRet;
    		}else{
    			$this->success = true;
    			$this->http_code = $this->rest->status();
    			$this->results = $aryRet;
    		}
    	}else{
    		$this->success = false;
    		$this->http_code = $this->rest->status;
    		$this->error = Array(
    				'http_code' => $this->http_code,
    				'message' => '주소 정보를 가져오는 중 문제가 발생하였습니다.'
    		);
    	}
    	return $this;
    	//$this->rest->debug();
    }
    
    //지번검색
    function get_post_code_xml_by_api($query){
    	$this->_resetReturn();
    	//$query = urlencode(iconv('utf-8','euc-kr',$query));
    	//$api_key = "5b1369615524f75af1336615865922";
    	//$epost_url = "/KpostPortal/openapi?regkey=$api_key&target=post&query=" . $query;
    	$epost_url = "/postal/retrieveLotNumberAdressAreaCdService/retrieveLotNumberAdressAreaCdService/getDetailListAreaCd";
    	$epost_url .= "?" . urlencode("ServiceKey") ."=pmbPq%2FJV3zbwmd3MJiVz6KaH7EOlQDYC5OqHXok98p5%2BqvckxTWLAad7JyZ6hIYJtMH1jsVQgF1UwyTuXFsjDw%3D%3D";//servicekey
    	$epost_url .= "&". urlencode("searchSe")."=". urlencode("dong"); /*검색구분(읍/면/동, 도로명, 우편번호)*/
    	$epost_url .= "&". urlencode("srchwrd")."=". urlencode($query); /*검색어*/
    	   
    
    	$config = array('server' => "http://openapi.epost.go.kr");
    	$this->load->library('rest');
    	$this->rest->initialize($config);
    	$xml = $this->rest->get($epost_url);
    	if($this->rest->status()==200){
    		$aryRet = json_decode(json_encode($xml),true);
    		$this->http_code = $this->rest->status();
    		if(isset($aryRet['error_code'])){
    			$this->success = false;
    			$this->error = $aryRet;
    		}else{
    			$this->success = true;
    			$this->http_code = $this->rest->status();
    			$this->results = $aryRet;
    		}
    	}else{
    		$this->success = false;
    		$this->http_code = $this->rest->status;
    		$this->error = Array(
    				'http_code' => $this->http_code,
    				'message' => '주소 정보를 가져오는 중 문제가 발생하였습니다.'
    				);
    	}
    	return $this;
    }
    
    /*
    //지번검색
    function get_post_code_xml_by_api($query){
        $this->_resetReturn();
        $query = urlencode(iconv('utf-8','euc-kr',$query));
        $api_key = "5b1369615524f75af1336615865922";
        $epost_url = "/KpostPortal/openapi?regkey=$api_key&target=post&query=" . $query;
        
        $config = array('server' => "http://biz.epost.go.kr");
        $this->load->library('rest');
        $this->rest->initialize($config);
        $xml = $this->rest->get($epost_url);
        if($this->rest->status()==200){
            $aryRet = json_decode(json_encode($xml),true);
            $this->http_code = $this->rest->status();
            if(isset($aryRet['error_code'])){
                $this->success = false;
                $this->error = $aryRet;
            }else{
                $this->success = true;
                $this->http_code = $this->rest->status();
                $this->results = $aryRet;
            }
        }else{
            $this->success = false;
            $this->http_code = $this->rest->status;
            $this->error = Array(
                'http_code' => $this->http_code,
                'message' => '주소 정보를 가져오는 중 문제가 발생하였습니다.'
            );
        }
        return $this; 
    }   
    */
}