<?php
class Bill_detail_ism_model extends CI_Model { //기존 intranet의 Contract_Service_model의 대체.

    private $t_billdetail    = 'BILL.tBillDetail';
    private $t_contract      = 'CONTRACT.tContract';
    private $t_account       = 'CONTRACT.tAccount';
    private $v_product       = 'CONTRACT.vProduct';

    function __construct()
    {
        //생성자
        parent::__construct();

        //ism 실DB 적용
        $ismdb = $this->load->database('ism',true);
        $this->ismdb = $ismdb;
    }



    /**
     * 청구 사용량 목록 추출
     */
    public function get_usage_list($data, $onlycount = FALSE)
    {
        # select 절 설정----------------------------------------------------------------------------------
        $select = "
            PD.sProductType   AS sServiceType,
            PD.sDetailType    AS sServiceName,
            PD.sName          AS sServiceDetail,
            BD.nBillDetailSeq AS nBillDetailSeq,
            BD.dtBilling      AS dtBilling,
            BD.nAmount        AS nUsageSize,
            BD.sUnit          AS sUsageUnit,
            BD.arContractInfo AS arContractInfo,
            BD.dtUseStart     AS dtUseStart,
            BD.dtUseEnd       AS dtUseEnd
        ";
        $this->ismdb->select($select, FALSE);


        # where 설정--------------------------------------------------------------------------------------
        if($billing_date = element('dtBillMonth', $data))
        {
            $this->ismdb->where("BD.dtBilling", $billing_date);//청구월
        }

        $where = array(
            "BD.nAccountSeq" => $data['nAccountSeq'],
            "BD.sConfirmYN"  => 'Y', //확정 된 건만
            "BD.sSumYN"      => 'Y', //청구에 포함된 것만
            "BD.sDeleteYN"   => 'N', //삭제 데이터 제외
        );
        $this->ismdb->where($where);
        $this->ismdb->where_in('BD.nBillDataType', [2,3,4,6,7]);


        # from 절 설정-----------------------------------------------------------------------------------
        $this->ismdb
            ->from($this->t_billdetail." as BD ")
            ->join($this->v_product."    as PD", "BD.nProductSeq = PD.nProductSeq ", "left")
        ;


        # 목록 데이터 출력일 경우 limit 설정---------------------------------------------------------------
        if($onlycount==FALSE)
        {
            $this->ismdb->limit($data['limit'], $data['offset']);
        }


        # oder by 절 설정---------------------------------------------------------------------------------
        $this->ismdb->order_by('BD.dtBilling desc, PD.sProductType ASC, PD.sDetailType ASC, PD.sName ASC');


        # 쿼리 생성
        $query = $this->ismdb->get();


        # 결과 추출 및 확인
        if ($query !== FALSE)
        {
            if($onlycount)
            {
                return ($query && $query->num_rows() > 0) ? $query->num_rows() : 0;
            }
            elseif ($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                $this->errMsg = "조회된 데이터가 없습니다.";
            }
        }
        else
        {
            $error = $this->ismdb->error();
            $this->errMsg = $error['message'];
        }
    }




    /**
     * 청구 사용량 상세정보
     * @param $seqs array('nAccountSeq'=>'회원고유번호', 'nBilldetailSeq'=>'청구상세 고유번호')
     */
    public function get_bill_usage_by_billdetailseq($data)
    {
        # select 절 설정----------------------------------------------------------------------------------
        $select = "
            BD.nBillDetailSeq AS nBillDetailSeq,
            BD.nContractSeq   AS nContractSeq,
            BD.dtBilling      AS dtBilling,
            BD.nAmount        AS nUsageSize,
            BD.sUnit          AS sUsageUnit,
            BD.arContractInfo AS arContractInfo,
            BD.dtUseStart     AS dtUseStart,
            BD.dtUseEnd       AS dtUseEnd,
            BD.nBillDataType  AS nBillDataType,
            BD.nProductSeq    AS nProductSeq,
            PD.sProductType   AS sProductType
        ";
        $this->ismdb->select($select, FALSE);


        # where 설정--------------------------------------------------------------------------------------
        $where = array(
            "BD.nAccountSeq"    => $data['nAccountSeq'],
            "BD.nBillDetailSeq" => $data['nBillDetailSeq'],
            "BD.sConfirmYN"     => 'Y', //확정 된 건만
            "BD.sSumYN"         => 'Y', //청구에 포함된 것만
            "BD.sDeleteYN"      => 'N', //삭제 데이터 제외
        );
        $this->ismdb->where($where);
        $this->ismdb->where_in('BD.nBillDataType', [2,3,4,6,7]);


        # from 절 설정-----------------------------------------------------------------------------------
        $this->ismdb
            ->from($this->t_billdetail." as BD")
            ->join($this->v_product."    as PD","BD.nProductSeq = PD.nProductSeq")
        ;


        # 쿼리 생성
        $query = $this->ismdb->get();


        # 결과 추출 및 확인
        if (!$query)
        {
            $error = $this->ismdb->error();
            $this->errMsg = $error['message'];
        }


        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
        {
            $this->errMsg = "조회된 데이터가 없습니다.";
        }

        return NULL;
    }
}