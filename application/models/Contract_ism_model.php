<?php
class Contract_ism_model extends CI_Model { //기존 intranet의 Contract_Service_model의 대체.

	private $t_contract    		= 'CONTRACT.tContract';
	private $t_account     		= 'CONTRACT.tAccount';
	private $t_product     		= 'CONTRACT.tProduct';
	private $t_productCate    = 'CONTRACT.tProductCategory';
	private $v_product			= 'CONTRACT.vProduct';

	function __construct(){
		//생성자
		parent::__construct();


		//ism 실DB 적용
		$ismdb = $this->load->database('ism',true);
		$this->ismdb = $ismdb;
	}

	//////////////////////////////////////
	/*
	 * ISM 호출
	 */
	//_select_row2
	public function get_ServiceCode($nAccountSeq)
	{
		$sql = "
						SELECT vPd.sProductType, vPd.nProductSeq,
						    IF(vPd.sProductType='IX',1,2) as serviceType
						FROM ".$this->v_product." as vPd
						JOIN
						(
						  	SELECT * FROM CONTRACT.tContract
						  	WHERE nAccountSeq= ?
						  	ORDER BY nContractSeq desc limit 1
						)AS CT on vPd.nProductSeq=CT.nProductSeq
				";

		$query = $this->ismdb->query($sql,array($nAccountSeq));

		if ($query !== FALSE)
		{
			if($query->num_rows() > 0)
			{
				return $query->row_array();
			}
			else
			{
				$this->errMsg = "조회내용이 없습니다.";
			}
		}
		else
		{
			$error = $this->ismdb->error();
			$this->errMsg = $error['message'];
		}
	}


	public function get_list($data, $onlycount = FALSE)
	{
		if(isset($data['main']))
			$this->ismdb->where(" (A.dtEnd - current_date > 0 or A.dtEnd = '0000-00-00' ) ", NULL, FALSE);
		if(isset($data['sStatus']) && $data["sStatus"]<>'A') //서비스 상태
			$this->ismdb->where(" A.sStatus='".$data['sStatus'] ."'  ", NULL, FALSE);
		if(isset($data["sServiceType"])) // 서비스 구분
			$this->ismdb->where("VP.sProductType = '".$data["sServiceType"]."'");
		if(isset($data["sDate"])) //기간 시작일자
		{
			if(isset($data["termType"]) && $data["termType"] === "dtStartDate") { //계약일
				$this->ismdb->where("A.dtStart >= '".$data["sDate"]."'");
			} else if(isset($data["termType"]) && $data["termType"] === "dtEndDate") {//만기일
				$this->ismdb->where("A.dtEnd >='".$data["sDate"]."'");
			} else {
				$this->ismdb->where("A.dtStart >='".$data["sDate"]."'");
			}
		}
		if(isset($data["eDate"])) //기간 종료일자
		{
			if(isset($data["termType"]) && $data["termType"] === "dtStartDate") {//계약일
				$this->ismdb->where("A.dtStart <='".$data["eDate"]."'");
			} else if(isset($data["termType"]) && $data["termType"] === "dtEndDate") {
				$this->ismdb->where("A.dtEnd <='".$data["eDate"]."'");
			} else {
				$this->ismdb->where("A.dtStart <='".$data["eDate"]."'");
			}
		}
		if($onlycount==FALSE)
			$this->ismdb->limit($data['limit'], $data['offset']);

		$select = "A.*,A.nContractSeq as nContractServiceSeq, A.dtEnd as dtEndDate, A.dtStart as dtStartDate, A.dtCharge as dtChargeDate, ";
		$select .= "VP.sProductType as sServiceType, VP.sEngProductType as sEngServiceType, VP.sDetailType as sServiceName, VP.sEngDetailType as sEngServiceName, VP.sName as sServiceDetail,  VP.sEngName as sEngServiceDetail ";
		$query = $this->ismdb
					->select($select, FALSE)
					->from($this->t_contract." as A ")
					->join($this->t_account." as B"," A.nAccountSeq = B.nAccountSeq ", "left")
					->join($this->v_product." as VP"," A.nProductSeq = VP.nProductSeq ", "left")
					->where(" A.nAccountSeq=".$data['nAccountSeq'] )
					->where("A.sMykinxViewFlag", "Y")
					->order_by(' VP.sProductType ASC, VP.sDetailType ASC, VP.sName ASC, (A.dtEnd - current_date) ASC ')
					->get();
		//echo $this->ismdb->last_query();
		if ($query !== FALSE)
		{

			if($onlycount)
			{
				return ($query && $query->num_rows() > 0) ? $query->num_rows() : 0;
			}
			elseif ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				$this->errMsg = "조회된 데이터가 없습니다.";
			}
		}
		else
		{
			$error = $this->ismdb->error();
			$this->errMsg = $error['message'];
		}
	}

	//select_row
	public function get_contract_bySeq($nSeq)
	{
		$select = "
			A.*,
			A.nContractSeq AS nContractServiceSeq,
			A.dtEnd        AS dtEndDate,
			A.dtStart      AS dtStartDate,
			A.dtCharge     AS dtChargeDate,
			A.dtStatus     AS dtCloseDate,
			A.nCharge      AS nPrice
		";

		$query = $this->ismdb
			->select($select, FALSE)
			->from($this->t_contract." as A ")
			->where('nContractSeq='.$nSeq)
			->limit(1)
			->get()
		;

		if($query != FALSE)
		{
			if($query->num_rows() > 0)
				return $query->row_array();
			else
				$this->errMsg = "조회된 데이터가 없습니다.";
		}
		else
		{
			$error = $this->ismdb->error();
			$this->errMsg = $error['message'];
		}
	}
}