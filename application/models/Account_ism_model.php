<?php
class Account_ism_model extends CI_Model { //기존 intranet의 tContract_model의 대체. (contract_model 은 member 에서 호출한다. (member/proc()). 삭제 금지)


	private $t_account    	 	= 'CONTRACT.tAccount';
	private  $t_accountuseinfo = 'CONTRACT.tAccountUseInfo';

	function __construct(){
		//생성자
		parent::__construct();


		//ism 실DB 적용
		$ismdb = $this->load->database('ism',true);
		$this->ismdb = $ismdb;
	}

	//////////////////////////////////////
	/*
	 * ISM 호출
	*/
	//intranet Contract_model -> _select_row
	public function get_account_byId($sMemberId)
	{
		$query = $this->ismdb
					->select('*', FALSE)
					->from($this->t_account)
					->where('sAccountID',$sMemberId)
					->limit(1)
					->get();

		//echo  $this->ismdb->last_query();

		if ($query !== FALSE)
		{
			if($query->num_rows() > 0)
			{
				return $query->row_array();
			}
			else
			{
				$this->errMsg = "조회내용이 없습니다.";
			}
		}
		else
		{
			$error = $this->ismdb->error();
			$this->errMsg = $error['message'];
		}
	}

	public function get_account_bySeq($seq)
	{
		$query = $this->ismdb
		->select('*', FALSE)
		->from($this->t_account)
		->where('nAccountSeq',$seq)
		->limit(1)
		->get();

		if ($query !== FALSE)
		{
			if($query->num_rows() > 0)
			{
				return $query->row_array();
			}
			else
			{
				$this->errMsg = "조회내용이 없습니다.";
			}
		}
		else
		{
			$error = $this->ismdb->error();
			$this->errMsg = $error['message'];
		}
	}

	//intranet Contract_model -> get_contractStatus
	public function get_AccountStatus($nAccountSeq)
	{
		$query = $this->ismdb
		->select('*', FALSE)
		->from($this->t_account)
		->where('nAccountSeq',$nAccountSeq)
		->get();

		if ($query !== FALSE)
		{
			if($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				$this->errMsg = "조회내용이 없습니다.";
			}
		}
		else
		{
			$error = $this->ismdb->error();
			$this->errMsg = $error['message'];
		}
	}

	//intranet Contract_model get_myCardPayment
	public function get_myCardPayment()
	{
		$query = $this->ismdb
						->select('sAccountID', FALSE)
						->from($this->t_account)
						->where('nMyCardPayment=1')
						->get();
		return $query->result_array();
	}

	public function update($data, $where)
	{
		if(isset($data['sPassword']))
		{
			if(isset($data['rePassword']))
				unset($data['rePassword']);
		}
		$this->ismdb->update($this->t_account, $data, $where);

	}

	public function get_accountuseinfo_byKey($key)
	{
		$query = $this->ismdb
		->select('*', FALSE)
		->from($this->t_accountuseinfo)
		->where('sKey',$key)
		->where('bDel','N')
		->limit(1)
		->get();

		if ($query !== FALSE)
		{
			if($query->num_rows() > 0)
			{
				return $query->row_array();
			}
			else
			{
				$this->errMsg = "조회내용이 없습니다.";
			}
		}
		else
		{
			$error = $this->ismdb->error();
			$this->errMsg = $error['message'];
		}
	}

	public function get_accountuseinfo_recent($nAccountSeq)
	{
		$query = $this->ismdb
		->select('*', FALSE)
		->from($this->t_accountuseinfo)
		->where('bDel','N')
		->where('nAccountSeq',$nAccountSeq)
		->order_by('nSeq','desc')
		->limit(1)
		->get();

		if ($query !== FALSE)
		{
			if($query->num_rows() > 0)
			{
				return $query->row_array();
			}
			else
			{
				$this->errMsg = "조회내용이 없습니다.";
			}
		}
		else
		{
			$error = $this->ismdb->error();
			$this->errMsg = $error['message'];
		}
	}

	public function update_useinfo($data, $where)
	{
		$this->ismdb->update($this->t_accountuseinfo, $data, $where);

	}

	//--------------------------------------------------
	public function test()
	{

		$this->ismdb->select("*", NULL, FALSE);
		$this->ismdb->where("sAccountID = 'test023s'", NULL, FALSE);
		$query = $this->ismdb->get("CONTRACT.tAccount");
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}

	/*
	 public function my_json_encode($arr)
	 {
	 //convmap since 0x80 char codes so it takes all multibyte codes (above ASCII 127). So such characters are being "hidden" from normal json_encoding
	 array_walk_recursive($arr, function (&$item, $key) { if (is_string($item)) $item = mb_encode_numericentity($item, array (0x80, 0xffff, 0, 0xffff), 'UTF-8'); });
	 return mb_decode_numericentity(json_encode($arr), array (0x80, 0xffff, 0, 0xffff), 'UTF-8');

	 }
	 */


	function fnUnicodeToUTF8($string) {

		return html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($string)), null, 'UTF-8');

	}

	public function get_ContractInfo($sMemberId)
	{
		$this->load->config('kinxconfig',true);
		$api = $this->config->item('ism_api_host', 'kinxconfig');
		//va($api);

		$this->load->library('rest');
		$this->rest->initialize(array('server'=>$api));
		$this->rest->http_header('content-type','text/html;charset=UTF-8');
		$this->rest->http_header('content-type','application/json;charset=UTF-8');

		$url = 'v2/accounts';
		$Param = Array(
				'sAccountID'=>$sMemberId
				);

		//$Param = json_encode($Param);
		//$retStdClass = $this->rest->get($url, $Param,'json');

		$retStdClass = $this->rest->get($url, $Param);
		va('retStdClass');
		va($retStdClass);

		$strjsonencode =  json_encode($retStdClass,JSON_UNESCAPED_UNICODE);
		va($strjsonencode) ;
		va(json_decode(utf8_decode($strjsonencode)));
		//$temp3 = iconv("EUC-KR", "UTF-8", $strjsonencode);
		//va($temp3);
		//$temp = utf8_decode( $strjsonencode) ;
		//va($temp) ;
		//$temp2 = $this->my_json_encode($retStdClass);
		//va($temp2);
		//va(json_decode( $retStdClass)) ;


		/*
		$str = json_encode($retStdClass);
		va('json_encode unsecaped');
		va( $str );
		$temp = json_decode($str);
		$str2 =  utf8_decode($temp);
		va('utf8_decode'); va( $str );
		 */
		//$temp4 = utf8_encode($str);
		//va($temp4);
		//$temp5 = utf8_decode($temp4);
		//va($temp5);

		//$temp2 = $this->fnUnicodeToUTF8($str);
		//va( $temp2 );

		//$str3 = $this->my_json_encode($retStdClass);
		//va('my_json_encode '); va($str3);

		//va('json decode');
		//va(json_decode(($str)));



		//return $retStdClass;
		//return json_decode( json_encode($retStdClass,JSON_UNESCAPED_UNICODE));
		//return json_decode( json_encode($retStdClass,JSON_UNESCAPED_UNICODE));

	}
}