<?php
class Kclean_model extends CI_Model {
	protected $table = array("tKClean", "tSysCode");
	private function _query($params) {
		if(isset($params["secKey"]) && isset($params["secTxt"])) {
			$this->db->like($params["secKey"], $params["secTxt"]);
		}
		if(isset($params["nContractSeq"])) {
			$where["nContractSeq"] = $params["nContractSeq"];
		}
		if(isset($params["nStatus"])) {
			$where["nStatus"] = $params["nStatus"];
		}
		$where["1"] = "1";
		return $where;
	}

	public function _select_cnt($params=array(), $index='0') {
		$where = $this->_query($params);
		$this->db->where($where);
		$this->db->from($this->table[$index]);
		return $this->db->count_all_results();
	}

	
	public function _select_list($params=array(), $index='0') {
		$limit = (isset($params["limit"])) ? $params["limit"] : NULL;
		$offset = (isset($params["offset"])) ? $params["offset"] : NULL;
		$where = $this->_query($params);
		
		// 정렬관련
		if(isset($params["oKey"][0]) && isset($params["oType"][0]))
		{
			$orderCnt = count($params["oKey"]);
			for($i = 0; $i < $orderCnt ; $i++)
			{
				$this->db->order_by($params["oKey"][$i], $params["oType"][$i]);
			}
		}
		return $this->db->get_where($this->table[$index], $where, $limit, $offset)->result_array();		
	}
	
	public function _insert($data, $index='0') {
		$db_data = filter_keys($data, $this->db->list_fields($this->table[$index]));
		//return $this->db->insert($this->table[$index], $db_data);
		$this->db->insert($this->table[$index], $db_data);
		return $this->db->insert_id();
	}
	 
	
	public function _select_row($where, $index='0') {
		return $this->db->where($where)->get($this->table[$index], 1)->row_array();		
	}
	
	public function _update($data, $where) {
		return $this->db->update($this->table[0], $data, $where);
	}
	
}