<?php
class Contract_member_model extends CI_Model {
	protected $table = array("tContractMember");
	private function _query($params) {
		if(isset($params["secKey"]) && isset($params["secTxt"])) {
			$this->db->like($params["secKey"], $params["secTxt"]);
		}
		
		if(isset($params["nContractSeq"])) {
			$where["nContractSeq"] = $params["nContractSeq"];
		}
		
		if(isset($params["sDutyType"])) {
			$where["sDutyType"] = $params["sDutyType"];
		}
		
		
		$where["1"] = "1";
		return $where;
	}

	public function _select_cnt($params=array()) {
		$where = $this->_query($params);
		$this->db->where($where);
		$this->db->from($this->table[0]);
		return $this->db->count_all_results();
	}

	public function _select_list($params=array()) {
		$limit = (isset($params["limit"])) ? $params["limit"] : NULL;
		$offset = (isset($params["offset"])) ? $params["offset"] : NULL;
		$where = $this->_query($params);
		// 정렬관련
		if(isset($params["oType"]) && isset($params["oKey"])){
			$this->db->order_by($params["oKey"], $params["oType"]);
		}
		return $this->db->get_where($this->table[0], $where, $limit, $offset)->result_array();
	}

	public function _select_row($where) {
		return $this->db->where($where)->get($this->table[0], 1)->row_array();
	}

	public function _insert($data) {
		return $this->db->insert($this->table[0], $data);
	}

	public function _delete($where) {
		$this->db->where($where);
		$this->db->delete($this->table[0]);
	}
	
	public function _findId($name, $email, $duty)
	{
		/*
		 //seq를 구해서 해당하는 id를 반환-계약명 찾음
		$sql = "";
		$sql .= " SELECT b.sMemberId, b.sContractName FROM tContract AS b ";
		$sql .= " INNER JOIN ( ";
		$sql .= " SELECT nContractSeq FROM tContractMember WHERE sName='".$name."' and sEmail='".$email."' and sDutyType='".$duty."' ";
		$sql .= " ) AS a ON a.nContractSeq=b.nContractSeq ";
		*/
		
		//seq를 구해서 해당하는 id를 반환-고객사명 찾음
		//140827 보안강화
		$sql = "";
		$sql .= " SELECT c.sMemberId, c.sContractName, d.sCompanyName FROM tCompany AS d";
		$sql .= " INNER JOIN ( ";
		$sql .= " 	SELECT b.sMemberId, b.sContractName, b.nCompanySeq, b.nContractSeq FROM tContract AS b ";
		$sql .= " 	INNER JOIN ( ";
		$sql .= " 		SELECT nContractSeq FROM tContractMember WHERE sName=? and sEmail=? and sDutyType=? ";
		//$sql .= " 	) AS a ON a.nContractSeq=b.nContractSeq ";
		$sql .= " 	) AS a ON a.nContractSeq=b.nContractSeq and b.sStatus='Y' ";//조건(sStatus.Y:정상)추가 20150521
		$sql .= " ) AS c ON c.nCompanySeq=d.nCompanySeq ;";
		
		$binds = array($name, $email, $duty);
		$query = $this->db->query($sql,$binds);	
		//-end
		$result = $query->result_array();
		return $result;
	}
	
	public function _findName($id, $email, $duty)
	{	
		//고객사명 반환
		$sql = "";
		$sql .= " SELECT c.sEmail, c.sDutyType, c.sContractName, c.nContractSeq, d.sCompanyName FROM tCompany AS d ";
		$sql .= " INNER JOIN ( ";
		$sql .= " 	SELECT b.sEmail, b.sDutyType, a.sContractName, b.nContractSeq, a.nCompanySeq FROM tContractMember AS b ";
		$sql .= " 	INNER JOIN ( ";
		//$sql .= " 		SELECT nContractSeq, sContractName, nCompanySeq FROM tContract where sMemberId='".$id."' ";
		$sql .= " 		SELECT nContractSeq, sContractName, nCompanySeq FROM tContract where sMemberId='".$id."' and sStatus='Y' ";//조건(sStatus.Y:정상)추가 20150521
		$sql .= " 	) AS a on a.nContractSeq=b.nContractSeq ";
		$sql .= " 	WHERE b.sDutyType='".$duty."' and b.sEmail='".$email."' ";
		$sql .= " ) AS c ON c.nCompanySeq=d.nCompanySeq ";
		/*
		//계약명 반환
		$sql = "";
		$sql .= " SELECT b.sEmail, b.sDutyType, a.sContractName, b.nContractSeq FROM tContractMember AS b ";
		$sql .= " INNER JOIN ( ";
		$sql .= " SELECT nContractSeq, sContractName FROM tContract where sMemberId='".$id."' ";
		$sql .= " ) AS a on a.nContractSeq=b.nContractSeq ";
		$sql .= " WHERE b.sDutyType='".$duty."' and b.sEmail='".$email."' ";
		*/
	
		$query = $this->db->query($sql);  
		$result = $query->result_array();
		return $result;
	}

	public function _update($data, $where) {
		return $this->db->update($this->table[0], $data, $where);
	}
	
	public function _set($params) {
		$this->db->set($params, NULL, FALSE);
		return $this->db->update($this->table[0]);
	}

	public function _replace($data) {
		return $this->db->replace($this->table[0], $data);
	}
}