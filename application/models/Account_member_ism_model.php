<?php
class Account_member_ism_model extends CI_Model {//tContractMember
    protected $table = array("tContractMember");

    private $t_accmember             = 'CONTRACT.tAccountMember';
    private $t_acc                   = 'CONTRACT.tAccount';
    private $t_custm                 = 'CONTRACT.tCustomer';

    function __construct(){
        //생성자
        parent::__construct();


        //ism 실DB 적용
        $ismdb = $this->load->database('ism',true);
        $this->ismdb = $ismdb;
    }

    //nAccountSeq 로 AccountMember 를 조회함
        //190417 sDeleteYN 조건 추가, limit 조건 삭제
    public function get_accountmem_byAccSeq($nSeq,$sDutyType)//sDutyTpe E-업무담당자,C-요금담당자
    {
        $query = $this->ismdb
        ->select("A.*, concat(A.sName, if(A.sRank is null or A.sRank='', '', concat('(',A.sRank,')'))) AS sNameRank, A.sPhone as sInternalPhone, A.sMobile as sMobilePhone", FALSE)
        ->from($this->t_accmember.' as A')
        ->where('nAccountSeq',$nSeq)
        ->where('sDutyType',$sDutyType)
        ->where('sDeleteYN','N')
        ->get();


        if($query != FALSE)
        {
            if($query->num_rows() > 0)
                            return $query->result_array(); //$query->row_array();
                        else
                                $this->errMsg = "조회된 데이터가 없습니다.";
        }
        else
        {
            $error = $this->ismdb->error();
            $this->errMsg = $error['message'];
        }
    }

        //190417 nAccountMemberSeq 로 AccountMember 를 조회함. 1명
    public function get_accountmem_byAccMemberSeq($nAccountSeq, $nAccountMemberSeq, $sDutyType)//sDutyTpe E-업무담당자,C-요금담당자
    {
        $query = $this->ismdb
        ->select('A.*, A.sPhone as sInternalPhone, A.sMobile as sMobilePhone', FALSE)
        ->from($this->t_accmember.' as A')
        ->where('nAccountSeq',$nAccountSeq)
                ->where('nAccountMemberSeq',$nAccountMemberSeq)
        ->where('sDutyType',$sDutyType)
                ->where('sDeleteYN','N')
                ->limit(1)
        ->get();


        if($query != FALSE)
        {
            if($query->num_rows() > 0)
                            return $query->row_array();
                        else
                                $this->errMsg = "조회된 데이터가 없습니다.";
        }
        else
        {
            $error = $this->ismdb->error();
            $this->errMsg = $error['message'];
        }
    }

    //_update
    public function update_accmem_info($data,$where)
    {
        $this->ismdb->update($this->t_accmember, $data, $where);

    }

    //_insert
    public function insert_accmem_info($data)
    {
        $this->ismdb->update($this->t_accmember, $data);

    }

    public function findId($name, $email, $duty)
    {
        $sql = "";
        $sql .= " SELECT C.sAccountID, C.sAccountID as sMemberId, C.sName AS sAccountName, D.sName, D.sName as sCompanyName FROM ".$this->t_custm." AS D";
        $sql .= " INNER JOIN ( ";
        $sql .= "     SELECT B.sAccountID, B.sName as sName, B.nCustomerSeq, B.nAccountSeq FROM ".$this->t_acc." AS B ";
        $sql .= "     INNER JOIN ( ";
        $sql .= "         SELECT nAccountSeq FROM ".$this->t_accmember." WHERE sName=? and sEmail=? and sDutyType=? and sDeleteYN='N' ";
        $sql .= "     ) AS A ON A.nAccountSeq=B.nAccountSeq and B.sStatus='Y' ";
        $sql .= " ) AS C ON C.nCustomerSeq=D.nCustomerSeq ;";

        $binds = array($name, $email, $duty);
        $query = $this->ismdb->query($sql,$binds);
        //echo $this->ismdb->last_query();die;

        $result = $query->result_array();
        return $result;
    }


    public function findName($id, $email, $duty)
    {
        //고객사명 반환
        $sql = "";
        $sql .= " SELECT C.sEmail, C.sDutyType, C.sAccountName, C.nAccountSeq, D.sName as sCustomerName, D.sName as sCompanyName  FROM ".$this->t_custm." AS D ";
        $sql .= " INNER JOIN ( ";
        $sql .= "     SELECT B.sEmail, B.sDutyType, A.sName as sAccountName, B.nAccountSeq, A.nCustomerSeq FROM ".$this->t_accmember." AS B ";
        $sql .= "     INNER JOIN ( ";
        $sql .= "         SELECT nAccountSeq, sName, nCustomerSeq FROM ".$this->t_acc." where sAccountID=? and sStatus='Y' ";
        $sql .= "     ) AS A on A.nAccountSeq=B.nAccountSeq ";
        $sql .= "     WHERE B.sDutyType=? and B.sEmail=? ";
        $sql .= " ) AS C ON C.nCustomerSeq=D.nCustomerSeq ";

        $binds = array($id, $duty, $email);
        $query = $this->ismdb->query($sql,$binds);
        //echo $this->ismdb->last_query();
        $result = $query->result_array();
        return $result;
    }
}