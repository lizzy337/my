<?php
class Work_request_model extends CI_Model {
	protected $table = array("tWorkRequest", "tSysCode");
	private function _query($params) {
		if(isset($params["nContractSeq"])) {
			$where["nContractSeq"] = $params["nContractSeq"];
		}
		if(isset($params["nServiceType"])) {
			$where["nServiceType"] = $params["nServiceType"];
		}
		if(isset($params["sActionType"])) {
			$where["sActionType"] = $params["sActionType"];
		}

		if(isset($params["dtCreateDate"])) {
			$where["dtCreateDate >"] = $params["dtCreateDate"];
		}

		if(isset($params["secKey"]) && isset($params["secTxt"])) {
			$this->db->like($params["secKey"], $params["secTxt"]);
		}

		if(isset($params["orderby"])) {
			$this->db->order_by($params["orderby"]);
		}

		//$where["1"] = "1";
		$where["sDeleteYN"] = "N";
		return $where;
	}

	public function _select_cnt($params=array()) {
		$where = $this->_query($params);
		$this->db->where($where);
		return $this->db->count_all_results($this->table[0]);
	}

	public function _select_list($params=array()) {
		$limit = (isset($params["limit"])) ? $params["limit"] : NULL;
		$offset = (isset($params["offset"])) ? $params["offset"] : NULL;
		$where = $this->_query($params);
		// 정렬관련
		if(isset($params["oType"]) && isset($params["oKey"])){
			$this->db->order_by($params["oKey"], $params["oType"]);
		}
		$this->db->where($where);
		$this->db->limit($limit);
		$this->db->offset($offset);
		$this->db->from($this->table[0]);
		$this->db->select($this->table[0].".*");
		$this->db->select("fnc_getCodeName('T0001', nWorkType)  sWorkType, fnc_getCodeName('G043', sResultStatus)  sStatus", FALSE);
		return $this->db->get()->result_array();//org
		//return $this->db->get_where($this->table[0], $where, $limit, $offset)->result_array();
	}

	public function _select_row($where) {
		return $this->db->where($where)->get($this->table[0], 1)->row_array();
	}

	public function _insert($data) {
		$this->db->insert($this->table[0], $data);
		return $this->db->insert_id();
	}

	public function _delete($where) {
		$this->db->where($where);
		$this->db->delete($this->table[0]);
	}

	public function _update($data, $where) {
		return $this->db->update($this->table[0], $data, $where);
	}

	public function _set($params) {
		$this->db->set($params, NULL, FALSE);
		return $this->db->update($this->table[0]);
	}

	public function _replace($data) {
		return $this->db->replace($this->table[0], $data);
	}
}