<?php
class Mrtg_hourview_model extends CI_Model {
	protected $table = array("vTrafficHour", "tTempCount", "vTrafficMinute");
	
	function __construct(){
		//생성자
		parent::__construct();
	
		$CI =& get_instance(); //CodeIgniter에 내장된 리소스 할당
	
		$this->usdb = $this->load->database('op',true);
	
	}
	
	private function _query($params) {
		if(isset($params["secKey"]) && isset($params["secTxt"])) {
			$this->db->like($params["secKey"], $params["secTxt"]);
		}

		if(isset($params["stimestamp"])) {
			$where["dtCreateDate >="] = $params["stimestamp"];
		}

		if(isset($params["etimestamp"])) {
			$where["dtCreateDate <="] = $params["etimestamp"];
		}

		if(isset($params["nMrtgInfoSeq"])) {
			$where["nMrtgInfoSeq"] = $params["nMrtgInfoSeq"];
		}
		$where["1"] = "1";
		return $where;
	}

	public function _select_cnt($params=array()) {
		$where = $this->_query($params);
		$this->usdb->where($where);
		$this->usdb->from($this->table[0]);
		
		
		return $this->usdb->count_all_results();
	}

	public function _select_list($params=array()) {
		$limit = (isset($params["limit"])) ? $params["limit"] : NULL;
		$offset = (isset($params["offset"])) ? $params["offset"] : NULL;
		//$where = $this->_query($params);
		
		$this->usdb
		->select(" B.dtCreateDate, A.sServiceType, A.sBillingType, A.nVolume, A.nVolume2, A.sNickName, A.nMrtgInfoSeq, A.sMrtgUrl, A.sMrtgUrl2, A.sMrtgFilePath, A.sMrtgFilePath2, A.nTrafficInMax, A.nTrafficInAvg, A.nTrafficOutMax, A.nTrafficOutAvg, A.nTrafficInMax_avg, A.nTrafficInAvg_avg, A.nTrafficOutMax_avg, A.nTrafficOutAvg_avg, A.nTrafficInMax2, A.nTrafficInAvg2, A.nTrafficOutMax2, A.nTrafficOutAvg2, A.nTrafficInMax2_avg, A.nTrafficInAvg2_avg, A.nTrafficOutMax2_avg, A.nTrafficOutAvg2_avg, A.nTrafficIn_now, A.nTrafficOut_now, A.nTrafficIn2_now, A.nTrafficOut2_now ",FALSE)
		->from(" (select UNIX_TIMESTAMP( '".$params['sdate']."' + interval cnt hour) dtCreateDate from ".$this->table[1]." where cnt < 24  order by cnt ASC) B ")
		->join($this->table[0]." A", "A.dtCreateDate = B.dtCreateDate AND A.nMrtgInfoSeq = ".$params['nMrtgInfoSeq'], "left")
		;
		
		// 정렬관련
		if(isset($params["oType"]) && isset($params["oKey"])){
			$this->usdb->order_by($params["oKey"], $params["oType"]);
		}
		
		$result = $this->usdb->get();
		
		return $result->result_array();
	}
	
	public function _select_min_list($params=array()) {
		$limit = (isset($params["limit"])) ? $params["limit"] : NULL;
		$offset = (isset($params["offset"])) ? $params["offset"] : NULL;
		//$where = $this->_query($params);
	
		$this->usdb
		->select(" B.dtCreateDate, A.sServiceType, A.sBillingType, A.nVolume, A.nVolume2, A.sNickName, A.nMrtgInfoSeq, A.sMrtgUrl, A.sMrtgUrl2, A.sMrtgFilePath, A.sMrtgFilePath2, A.nTrafficInMax, A.nTrafficInAvg, A.nTrafficOutMax, A.nTrafficOutAvg, A.nTrafficInMax_avg, A.nTrafficInAvg_avg, A.nTrafficOutMax_avg, A.nTrafficOutAvg_avg, A.nTrafficInMax2, A.nTrafficInAvg2, A.nTrafficOutMax2, A.nTrafficOutAvg2, A.nTrafficInMax2_avg, A.nTrafficInAvg2_avg, A.nTrafficOutMax2_avg, A.nTrafficOutAvg2_avg, A.nTrafficIn_now, A.nTrafficOut_now, A.nTrafficIn2_now, A.nTrafficOut2_now ",FALSE)
		->from(" (select UNIX_TIMESTAMP( '".$params['sdate']."' + interval cnt*5 minute) dtCreateDate from ".$this->table[1]."  where cnt < 288  order by cnt ASC ) B ")
		->join($this->table[2]." A", "A.dtCreateDate = B.dtCreateDate AND A.nMrtgInfoSeq = ".$params['nMrtgInfoSeq'], "left")
		;
	
		// 정렬관련
		if(isset($params["oType"]) && isset($params["oKey"])){
			$this->usdb->order_by($params["oKey"], $params["oType"]);
		}
	
		$result = $this->usdb->get();
	
		return $result->result_array();
	}
	
	

	public function _select_row($where) {
		return $this->db->where($where)->get($this->table[0], 1)->row_array();
	}

	public function _insert($data) {
		return $this->db->insert($this->table[0], $data);
	}

	public function _delete($where) {
		$this->db->where($where);
		$this->db->delete($this->table[0]);
	}

	public function _update($data, $where) {
		return $this->db->update($this->table[0], $data, $where);
	}
	
	public function _set($params) {
		$this->db->set($params, NULL, FALSE);
		return $this->db->update($this->table[0]);
	}

	public function _replace($data) {
		return $this->db->replace($this->table[0], $data);
	}
}