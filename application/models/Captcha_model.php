<?php
class Captcha_model extends CI_Model {
    function regCaptchaInfo($cap){
        $data = array(
            'captcha_time'=> $cap['time'],
            'ip_address'=> $this->input->ip_address(),
            'word' => $cap['word']
        );
        $query = $this->db->insert_string('tCaptcha', $data);
        return $this->db->query($query);
    }
    public function checkCaptcha($word, $ipaddr, $captcha_time){
        $captcha_time = round($captcha_time);
        $expiration = time()-7200; // Two hour limit
        //-start 140827 보안강화
        //$this->db->query("DELETE FROM tCaptcha WHERE captcha_time < ".$expiration); //org
        
        $sql = "DELETE FROM tCaptcha WHERE captcha_time < ? ";
        $query = $this->db->query($sql, $expiration);
        //-end
        // Then see if a captcha exists:
        $sql = "SELECT COUNT(*) AS count FROM tCaptcha WHERE lower(word) = ? AND ip_address = ? AND captcha_time= ?";
        $binds = array(strtolower($word), $ipaddr, $captcha_time);
        $query = $this->db->query($sql, $binds);
        $row = $query->row();
        return $row->count;
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

