<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Ixcloud_grizzly_model extends CI_Model {
    private $success;
    private $http_code;
    private $results;
    private $error;
    private $apihost;
    private $dbKinx;

    public function __construct() {
		parent::__construct();
        $this->load->library('rest', 'restAccount');
        $this->dbKinx = $this->load->database('default',true);

        $this->load->config('kinxconfig', true);
        $this->apihost = Array(
            'grizzly'=> $this->config->item('host_api_grizzly', 'kinxconfig')
        );
	}
    function __destruct(){

    }
    # 결과값 초기화
    private function _resetReturn(){
        $this->success = true;
        $this->http_code= null;
        $this->results = null;
        $this->error = null;
    }
    # 결과값 반환
    public function return_result($mime='php'){
        $rtnArray = array(
            'success' => $this->success,
            'status' => $this->http_code,
            'results' => $this->results,
            'error' => $this->error
        );

        switch($mime){
            case 'json':
                $ret= json_encode($rtnArray);
                break;
            case 'php':
                $ret= $rtnArray;
                break;
        }
        return $ret;
    }
    public function getSuccess(){
        return $this->success;
    }
    public function stdClassToArray($d) {
		$tmp = json_encode($d);
        return json_decode($tmp,true);
	}
    public function arrayToStdClass($array){
        return (object) $array;
    }
    #회원가입시 임시 세션
    public function setJoinTempSession($userid,$username, $user_id, $account_id){
        $arySession = array(
                    'grizzly_username' => $username,
                    'grizzly_userid' => $userid,
                    'grizzly_account_id' => $account_id,
                    'grizzly_user_id' => $user_id,
        );
        $this->session->set_userdata($arySession);
    }
    #회원가입시 임시 세션 삭제
    public function clearOpenStackSession(){
        $arySession = array(
            'grizzly_username' => "",
            'grizzly_userid' => "",
            'grizzly_account_id' => "",
            'grizzly_user_id' => "",
        );
        $this->session->unset_userdata($arySession);
    }
    #회원가입-계정 기본 정보 등록
    public function regUserInfo($userid, $params){
        $path = 'account/grizzly/join/user';
        $jsonParam = json_encode($params);
        $config = array('server'=>$this->apihost['grizzly']);
        $this->rest->initialize($config);
        $this->rest->http_header('content-type','application/json; charset=UTF-8');
        $retStdClass = $this->rest->post($path, $jsonParam, 'json');
        $status = $this->rest->status();
        $result = $this->stdClassToArray($retStdClass);
        if($result){
            $this->success = (isset($result['success'])?$result['success']:false);
            $this->http_code = $status;
            $this->results = (isset($result['results'])?$result['results']:null);
            $this->error = (isset($result['error'])?$result['error']:null);
            //회원가입용 세션 저장
            if($this->success){
                $this->setJoinTempSession($this->results['userid'], $this->results['name'], $this->results['user_id'], $this->results['account_id']);
            }
            if(ENVIRONMENT!='development'){
                $this->results = array();
            }
        }
        return $this;
    }
    #회원가입-카드결제정보 입력
    public function regPaymentInfo($params){
        $path = 'account/grizzly/join/paymentinfo';
        $jsonParam = json_encode($params);
        $config = array('server'=>$this->apihost['grizzly']);
        $this->rest->initialize($config);
        $this->rest->http_header('content-type','application/json; charset=UTF-8');
        $retStdClass = $this->rest->post($path, $jsonParam, 'json');
        $status = $this->rest->status();
        $result = $this->stdClassToArray($retStdClass);
        if($result){
            $this->success = (isset($result['success'])?$result['success']:false);
            $this->http_code = $status;
            $this->results = (isset($result['results'])?$result['results']:null);
            $this->error = (isset($result['error'])?$result['error']:null);
        }
        return $this;
    }
    #Mykinx-카드결제정보 입력
    public function updatePaymentInfo($params){
    	$path = 'account/grizzly/join/paymentinfoupdate';
    	$jsonParam = json_encode($params);
    	$config = array('server'=>$this->apihost['grizzly']);
    	$this->rest->initialize($config);
    	$this->rest->http_header('content-type','application/json; charset=UTF-8');
    	$retStdClass = $this->rest->post($path, $jsonParam, 'json');
    	$status = $this->rest->status();
    	$result = $this->stdClassToArray($retStdClass);
    	if($result){
    		$this->success = (isset($result['success'])?$result['success']:false);
    		$this->http_code = $status;
    		$this->results = (isset($result['results'])?$result['results']:null);
    		$this->error = (isset($result['error'])?$result['error']:null);
    	}
    	return $this;
    }

    #회원가입-인증메일 재발송
    public function sendConfirmMail($account_id, $user_id, $userid, $email){
        $path = 'account/grizzly/join/mail';
        $jsonParam = json_encode(array(
            'account_id'=>$account_id,
            'user_id'=>$user_id,
            'userid'=>$userid,
            'email'=>$email
        ));
        $config = array('server'=>$this->apihost['grizzly']);
        $this->rest->initialize($config);
        $this->rest->http_header('content-type','application/json; charset=UTF-8');
        $retStdClass = $this->rest->post($path, $jsonParam, 'json');
        $status = $this->rest->status();
        $result = $this->stdClassToArray($retStdClass);
        if($result){
            $this->success = (isset($result['success'])?$result['success']:false);
            $this->http_code = $status;
            $this->results = (isset($result['results'])?$result['results']:null);
            $this->error = (isset($result['error'])?$result['error']:null);
        }
        return $this;
    }
    #회원가입-인증메일 확인
    public function confirmJoin($params){
        $path = 'account/grizzly/join/confirm';
        $jsonParam = json_encode($params);
        $config = array('server'=>$this->apihost['grizzly']);
        $this->rest->initialize($config);
        $this->rest->http_header('content-type','application/json; charset=UTF-8');
        $retStdClass = $this->rest->post($path, $jsonParam, 'json');
        $status = $this->rest->status();
        $result = $this->stdClassToArray($retStdClass);
        if($result){
            $this->success = (isset($result['success'])?$result['success']:false);
            $this->http_code = $status;
            $this->results = (isset($result['results'])?$result['results']:null);
            $this->error = (isset($result['error'])?$result['error']:null);
        }
        return $this;
    }
    #아이디/패스워드찾기
    public function findidpw($type, $param){
        $this->success = true;
        if($type=='id'){
            $this->findId($param);
        }elseif($type=='password'){
            $this->findPassword($param);
        }else{
            $this->success = false;
            $this->http_code = 400;
            $this->results = null;
            $this->error = Array('message'=>'알 수 없는 형식 입니다.');
        }
        return $this;
    }
    #아이디/패스워드찾기-아이디
    public function findId($params){
        $path = 'account/grizzly/auth/findid';
        if(isset($params) && isset($params['username']) && isset($params['useremail'])){
            $jsonParam = json_encode($params);
            $config = array('server'=>$this->apihost['grizzly']);
            $this->rest->initialize($config);
            $this->rest->http_header('content-type','application/json; charset=UTF-8');
            $retStdClass = $this->rest->post($path, $jsonParam, 'json');
            $status = $this->rest->status();
            $result = $this->stdClassToArray($retStdClass);
            if($result){
                $this->success = (isset($result['success'])?$result['success']:false);
                $this->http_code = $status;
                $this->results = (isset($result['results'])?$result['results']:null);
                $this->error = (isset($result['error'])?$result['error']:null);
            }
        }else{
            $this->success = false;
            $this->http_code = 400;
            $this->results = null;
            $this->error = Array('message'=>'필요한 인자가 전달되지 않았습니다.');
        }
        return $this;
    }
    #아이디/패스워드찾기-패스워드
    public function findPassword($params){
        $path = 'account/grizzly/auth/findpw';
        if(isset($params) && isset($params['username']) && isset($params['userid'])){
            $jsonParam = json_encode($params);
            $config = array('server'=>$this->apihost['grizzly']);
            $this->rest->initialize($config);
            $this->rest->http_header('content-type','application/json; charset=UTF-8');
            $retStdClass = $this->rest->post($path, $jsonParam, 'json');
            $status = $this->rest->status();
            $result = $this->stdClassToArray($retStdClass);
            if($result){
                $this->success = (isset($result['success'])?$result['success']:false);
                $this->http_code = $status;
                $this->results = (isset($result['results'])?$result['results']:null);
                $this->error = (isset($result['error'])?$result['error']:null);
            }
        }else{
            $this->success = false;
            $this->http_code = 400;
            $this->results = null;
            $this->error = Array('message'=>'필요한 인자가 전달되지 않았습니다.');
        }
        return $this;

    }
    #자동 로그인
    public function autoLogin($sCloudUserId){
        if($sCloudUserId){
            $path = 'account/grizzly/auth/auto';
            $jsonParam = json_encode(array('userid'=>$sCloudUserId));
            $config = array('server'=>$this->apihost['grizzly']);
            $this->rest->initialize($config);
            $this->rest->http_header('content-type','application/json; charset=UTF-8');
            $retStdClass = $this->rest->post($path, $jsonParam, 'json');
            $status = $this->rest->status();
            $result = $this->stdClassToArray($retStdClass);

            if($result){
                $this->success = (isset($result['success'])?$result['success']:false);
                $this->http_code = $status;
                $this->results = (isset($result['results'])?$result['results']:null);
                $this->error = (isset($result['error'])?$result['error']:null);

                if($this->http_code==302){
                    $account = $this->results['account'];
                    $this->setJoinTempSession($account['userid'], $account['name'],$account['user_id'],$account['account_id']);
                    if(ENVIRONMENT!='development'){
                        unset($this->results['account']);
                    }else{
                        $this->results['session'] = json_encode($this->session->all_userdata());
                    }
                }
            }
        }else{
            $this->success = false;
            $this->http_code = 401;
            $this->results = null;
            $this->error = Array('message'=>'필요한 인자가 전달되지 않았습니다.');
        }
        return $this;
    }
    #로그인(Grizzly)
    public function login($userid, $password){
        if($userid && $password){
            /*Grizzly Login*/
            $path = 'account/grizzly/auth/login';
            $jsonParam = json_encode(array('userid'=>$userid, 'password'=>$password));

            // 서버 상태 체크
            $fp = @fsockopen(str_replace('http://', '', $this->apihost['grizzly']), 80, $errno, $errstr, 5);
            if(!$fp){
                $this->success = false;
                $this->http_code = 503;
                $this->results = null;
                $this->error = Array('message'=>'Service Unavailable.');
                return $this;
            }

            $config = array('server'=>$this->apihost['grizzly']);
            $this->rest->initialize($config);
            $this->rest->http_header('content-type','application/json; charset=UTF-8');
            $retStdClass = $this->rest->post($path, $jsonParam, 'json');
            $status = $this->rest->status();
            $result = $this->stdClassToArray($retStdClass);
            if($result){
                $this->success = (isset($result['success'])?$result['success']:false);
                $this->http_code = $status;
                $this->results = (isset($result['results'])?$result['results']:null);
                $this->error = (isset($result['error'])?$result['error']:null);
            }
        }else{
            $this->success = false;
            $this->http_code = 401;
            $this->results = null;
            $this->error = Array('message'=>'필요한 인자가 전달되지 않았습니다.');
        }
        return $this;
    }
    #연결 계정 정보 가져오기
    public function getCloudId($nContractSeq){
        $this->dbKinx->select('*');
        $this->dbKinx->from('tCloudLink');
        $this->dbKinx->where(array('nContractSeq'=>$nContractSeq,'nCloudType'=>2));
        $objDB = $this->dbKinx->get();
        if($objDB->num_rows()==0){
            $this->success = false;
            $this->http_code = 400;
            $this->results = null;
            $this->error = Array('message'=>'연결된 계정을 찾을 수 없습니다.');
        }else{
            $this->success = true;
            $this->http_code = 200;
            $this->results = $objDB->row_array();
            $this->error = null;
        }
        return $this;
    }
    #연결 계정 정보 가져오기
    public function getContractInfo($nCloudAccountId, $nCloudUserId){
        $this->dbKinx->select('*');
        $this->dbKinx->from('tCloudLink');
        $this->dbKinx->where(array('nCloudAccountId'=>$nCloudAccountId, 'nCloudUserId'=>$nCloudUserId));
        $objDB = $this->dbKinx->get();
        if($objDB->num_rows()==0){
            $this->success = false;
            $this->http_code = 400;
            $this->results = null;
            $this->error = Array('message'=>'연결된 계정을 찾을 수 없습니다.');
        }else{
            $this->success = true;
            $this->http_code = 200;
            $this->results = $objDB->row_array();
            $this->error = null;
        }
        return $this;
    }
    #계정인증
    public function validateLogin($userid, $password){
        if($userid && $password){
            $path = 'account/grizzly/auth/validate';
            $jsonParam = json_encode(array('userid'=>$userid, 'password'=>$password));
            $config = array('server'=>$this->apihost['grizzly']);
            $this->rest->initialize($config);
            $this->rest->http_header('content-type','application/json; charset=UTF-8');
            $retStdClass = $this->rest->post($path, $jsonParam, 'json');
            $status = $this->rest->status();
            $result = $this->stdClassToArray($retStdClass);
            if($result){
                $this->success = (isset($result['success'])?$result['success']:false);
                $this->http_code = $status;
                $this->results = (isset($result['results'])?$result['results']:null);
                //if(ENVIRONMENT=='development'){
                //    $this->results = (isset($result['results'])?$result['results']:null);
                //}else{
                //    $this->results = array();
                //}
                $this->error = (isset($result['error'])?$result['error']:null);

                if($this->http_code==302){
                    $account = $this->results['account'];
                    $this->setJoinTempSession($account['userid'], $account['name'],$account['user_id'],$account['account_id']);
                    if(ENVIRONMENT!='development'){
                        unset($this->results['account']);
                    }else{
                        $this->results['session'] = json_encode($this->session->all_userdata());
                    }
                }
            }
        }else{
            $this->success = false;
            $this->http_code = 401;
            $this->results = null;
            $this->error = Array('message'=>'필요한 인자가 전달되지 않았습니다.');
        }
        return $this;
    }

    #컨텐츠 진흥원 자원 제공(2014.08.27)
    public function regKoccaUser($params){
        $path = 'account/grizzly/join/kocca';
        $params['register_type'] = 'kocca_type1';

        $jsonParam = json_encode($params);
        $config = array('server'=>$this->apihost['grizzly']);
        $this->rest->initialize($config);
        $this->rest->http_header('content-type','application/json; charset=UTF-8');
        $retStdClass = $this->rest->post($path, $jsonParam, 'json');
        $status = $this->rest->status();
        $result = $this->stdClassToArray($retStdClass);
        if($result){
            $this->success = (isset($result['success'])?$result['success']:false);
            $this->http_code = $status;
            $this->results = (isset($result['results'])?$result['results']:null);
            $this->error = (isset($result['error'])?$result['error']:null);
            //회원가입용 세션 저장
            if($this->success){
                //$this->setJoinTempSession($this->results['userid'], $this->results['name'], $this->results['user_id'], $this->results['account_id']);
            }
            if(ENVIRONMENT!='development'){
                //$this->results = array();
            }
        }
        return $this;
    }
}
