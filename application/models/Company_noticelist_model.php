<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Company_noticelist_model extends CI_Model{
	
	public function get_noticelist()
	{
		$query = $this->db->get('tRecruitList');
	}
	
	public function _select_cnt($params=array()) {		
		return $this->db->count_all_results('tRecruitList');
	}
	
	public function get_currRecruitCnt($nBoardSeq)
	{	
		//start 140827 보안강화
		/* org
		$sql = "";
		$sql .= "SELECT * FROM tNoticeIr AS a ";
		$sql .= " Inner Join (";
		$sql .= " SELECT nNoticeListSeq FROM tNoticeList WHERE sDisplayYn='Y' AND nBoardSeq='4' ";
		$sql .= ") AS b on a.nNoticeListSeq = b.nNoticeListSeq ";
		$sql .= "WHERE a.sState ='I' ";
		$query = $this->db->query($sql);
		*/
		$sql = "";
		$sql .= "SELECT * FROM tNoticeIr AS a ";
		$sql .= " Inner Join (";
		$sql .= " SELECT nNoticeListSeq FROM tNoticeList WHERE sDisplayYn='Y' AND nBoardSeq=? ";
		$sql .= ") AS b on a.nNoticeListSeq = b.nNoticeListSeq ";
		$sql .= "WHERE a.sState ='I' ";
		$query = $this->db->query($sql,$nBoardSeq); 
		//-end
		return $query->num_rows();
				
	}
	
	public function get_closeCheckRecruit()
	{
		$this->db->select('*');
		$this->db->from('tNoticeIr');
		$this->db->where('sState','I');
		$this->db->where('nRecruitType','0');
		
		$query = $this->db->get();		
		$result = $query->result_array();
		return $result;
	}
	
	public function set_closeRecruit($nNoticeIrSeq)
	{
		$data = array('sState'=>'C');
		$this->db->where('nNoticeIrSeq',$nNoticeIrSeq);
		$this->db->update('tNoticeIr',$data);
	}
	
	public function _select_list($params=array(),$searchText=null, $searchType=1,$jobType=-1, $sHeadTextgroupCode=null, &$result_cnt) 
	{
		if( isset($searchText) && ($searchText != null ))
		{
			$sql = "SELECT b.nNoticeListSeq, b.nHeadTextSeq, b.sTitle, b.sContent, a.dtRecruitEndDate, a.nRecruitType,  ";
			$sql .= " a.nNoticeIrSeq, a.sRecruitSite1, a.sRecruitSite2, a.sState, a.dtRecruitEndDate, a.nRecruitType ";
			if( $sHeadTextgroupCode != null ){
				$sql .= ", ( SELECT sCodeName From tSysCode where sGroupCode='".$sHeadTextgroupCode."' and sCode=b.nHeadTextSeq) as sHeadText ";
			}
			$sql .= "FROM tNoticeList as b ";
			$sql .= "Inner Join ( ";
			$sql .= " SELECT nNoticeIrSeq, nNoticeListSeq, sJobType, sRecruitSite1, sRecruitSite2, sState, dtRecruitEndDate, nRecruitType FROM tNoticeIr ";
			$sql .= " ORDER BY CASE sState ";
			$sql .= " WHEN 'I' then 1 ";
			$sql .= " WHEN 'C' then 2 ";
			$sql .= " ELSE 3";
			$sql .= " END";
			$sql .= ") as a on b.nNoticeListSeq=a.nNoticeListSeq ";
			$whereflag = false;
			if( $searchType == 2 )//내용
			{
				$sql .= "WHERE b.sContent like '%".$searchText."%' ";
				$whereflag = true;
			}
			else{//제목
				$sql .= "WHERE b.sTitle like '%".$searchText."%' ";
				$whereflag = true;
			}
			//직군
			if( $jobType != -1) //직군선택. 전체가 아니면
			{
				if($whereflag == false){
					$sql .="WHERE ";
				}else{
					$sql .=" and "; 
				}
				$sql .= " b.nHeadTextSeq='".$jobType."' ";
			}
			
			if($whereflag == false){
				$sql .="WHERE ";
			}else{
				$sql .=" and ";
			}
			$sql .= " b.sDisplayYn='Y' and b.nBoardSeq='4'";
			
			$query = $this->db->query($sql);
			$result = $query->result_array();
			$result_cnt = count($result);
			
			
			if(isset($params["limit"])){
				$sql .= " limit ".$params["limit"];
			}
			
			if(isset($params["offset"])){
				$sql .= " offset ".$params["offset"];
			}
			$sql .= " order by b.nNoticeListSeq desc ";    //by lizzy 20160314
			
 			//debug_var($sql);
 			//exit;
		
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
		}
		else
		{
			$sql = "SELECT b.nNoticeListSeq, b.nHeadTextSeq, b.sTitle, b.sContent,  ";
			$sql .= " a.nNoticeIrSeq, a.sRecruitSite1, a.sRecruitSite2, a.sState, a.dtRecruitEndDate, a.nRecruitType ";
			if( $sHeadTextgroupCode != null ){
				$sql .= ", ( SELECT sCodeName From tSysCode where sGroupCode='".$sHeadTextgroupCode."' and sCode=b.nHeadTextSeq) as sHeadText ";
			}
			$sql .= "FROM tNoticeList as b ";
			$sql .= "Inner Join ( ";
			$sql .= " SELECT nNoticeIrSeq, nNoticeListSeq, sJobType, sRecruitSite1, sRecruitSite2, sState, dtRecruitEndDate, nRecruitType FROM tNoticeIr";
			$sql .= " ORDER BY CASE sState ";
			$sql .= " WHEN 'I' then 1 ";
			$sql .= " WHEN 'C' then 2 ";
			$sql .= " ELSE 3";
			$sql .= " END";
			$sql .= ") as a on b.nNoticeListSeq=a.nNoticeListSeq ";
			//직군
			$whereflag = false;
			if( $jobType != -1) //직군선택. 전체가 아니면
			{
				$sql .= "WHERE b.nHeadTextSeq='".$jobType."' ";
				$whereflag = true;
			}
			
			if($whereflag == false){
				$sql .="WHERE ";
			}else{
				$sql .=" and ";
			}
			$sql .= " b.sDisplayYn='Y' and b.nBoardSeq='4'";
			
			$query = $this->db->query($sql);
			$result = $query->result_array();
			$result_cnt = count($result);
			
			$sql .= " order by b.nNoticeListSeq desc "; //by lizzy 20160314
			if(isset($params["limit"])){
				$sql .= " limit ".$params["limit"];
			}
			if(isset($params["offset"])){
				$sql .= " offset ".$params["offset"];
			}
			
			
 			//debug_var($sql);
 			//exit;
			
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
		}
	}
	
	public function get_noticedata($nNoticeListSeq)
	{
		$sql = " SELECT sContents ";		
		$sql .= " FROM tNoticeList ";
		$sql .= " WHERE nNoticeListSeq='".$nNoticeListSeq."'";
		
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}
}
