<?php
class Sms_model extends CI_Model {
    protected $table = array("MMS_MSG", "SC_TRAN");
    private $success;
    private $results;
    private $kinxAPIHost = 'http://api.kinx.net';
    /*
     * LG Dacom 서버 모듈 SMS 발송
     */
    public function send($SID, $phonenumber, $msg, $name='', $callback='02-526-0965', $subject='', $msgtype=0){
        $url = 'sms/send.json';
        $smsParam = Array(
            'phones'=>array(
             Array('name'=>$name,'number'=>$phonenumber)
            ),
            'subject'=>$subject,
            'message'=>$msg,
            'callback'=> $callback,
            'type'=>$msgtype
        );
        $smsParam = json_encode($smsParam);
        $config = array('server'=>$this->kinxAPIHost);
        $this->rest->initialize($config);
        $this->rest->http_header('content-type','application/json;charset=UTF-8');
        $retStdClass = $this->rest->post($url, $smsParam, 'json');
        $status = $this->rest->status();
        
        return array('success'=>true);
    }
}
?>