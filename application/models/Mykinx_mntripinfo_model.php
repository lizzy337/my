<?php
class Mykinx_mntripinfo_model extends CI_Model {
	protected $table = array("tCompanyMonitoringIP", "tCompanyContact", "tWorkMonitoringIP", "tWorkRequest");

	private function _query($params) {
		if(isset($params["secKey"]) && isset($params["secTxt"])) {
			$this->db->like($params["secKey"], $params["secTxt"]);
		}
		if(isset($params["nContractSeq"])) {
			$where["nContractSeq"] = $params["nContractSeq"];
		}
		
		if(isset($params["sStatus"])) {
			$where["sStatus"] = $params["sStatus"];
		}
		
		$where["1"] = "1";
		return $where;
	}

	public function _select_cnt($params=array(), $index='0') {		
		$where = $this->_query($params);
		$this->db->where($where);
		$this->db->from($this->table[$index]);
		return $this->db->count_all_results();
		
	}

	
	public function _select_list($params=array(), $index='0') {
		$limit = (isset($params["limit"])) ? $params["limit"] : NULL;
		$offset = (isset($params["offset"])) ? $params["offset"] : NULL;
		$where = $this->_query($params);
		
		// 정렬관련
		if(isset($params["oKey"][0]) && isset($params["oType"][0]))
		{
			$orderCnt = count($params["oKey"]);
			for($i = 0; $i < $orderCnt ; $i++)
			{
				$this->db->order_by($params["oKey"][$i], $params["oType"][$i]);
			}
		}
		return $this->db->get_where($this->table[$index], $where, $limit, $offset)->result_array();		
	}
	
	public function getCompanyMntrIPList($params=array())//SP]reCompanyMonitoringIPList. 고객 모니터링 ip
	{
		/*
		 * 정상상태(nStatus가 Y인것만 가져오는것으로 수정)
		*/
		$sql = "";
		$sql .= "SELECT nCompanyMonitoringIPSeq, sIp, sComment, dtCreateDate ";
		$sql .= "FROM ".$this->table[0]." ";//tCompanyMonitoringIP
		$sql .= "WHERE sStatus='Y' and nContractSeq=? ";//140827 보안강화
		
		$query = $this->db->query($sql,$params["nContractSeq"]);//140827 보안강화
		$result = $query->result_array();
		return $result;
	}

	public function InsertWorkRequest($data)//SP]reWorkIDCInsert
	{
		$sql = "SELECT sCompanyName, sManagerName, sDepartmentName, sRank, sInternalphone, sfax, sMobilephone, sEmail ";
		$sql .= "FROM tCompanyContact ";
		$sql .= "WHERE nContractSeq=? and nCompanyContactseq=? ";//140827 보안강화 
		
		$binds = array($data["nContractSeq"], $data["companycontact"]);//140827 보안강화 
		$query = $this->db->query($sql,$binds);
		$result = $query->result_array();
	
		$insertData = array(
				'nCompanySeq'=>$data["nCompanySeq"],
				'nContractSeq'=>$data["nContractSeq"],
				'nServiceType'=>$data["nServiceType"],
				'sCompanyName'=>$result[0]["sCompanyName"],
				'sManagerName'=>$result[0]["sManagerName"],
				'sDepartmentName'=>$result[0]["sDepartmentName"],
				'sRank'=>$result[0]["sRank"],
				'sfax'=>$result[0]["sfax"],
				'sInternalphone'=>$result[0]["sInternalphone"],
				'sMobilephone'=>$result[0]["sMobilephone"],
				'sEmail'=>$result[0]["sEmail"],
				'sActionType'=>"MIP",
				'sResultStatus'=>"Y",
				'sContent'=>$data["request"]//,
				//'dtExpectStartDate'=>"",
				//'dtExpectEndDate'=>""
		);
		$this->db->insert($this->table[3],$insertData);
		return $this->db->insert_id();
	}
	
	public function InsertWorkMntrIp($params=array())
	{
		$data = array(
				'nCompanyMonitoringIPSeq'=>$params["nCompanyMonitoringIPSeq"],
				'nWorkRequestSeq'=>$params["nWorkRequestSeq"],
				'nCompanySeq'=>$params["nCompanySeq"],
				'nContractSeq'=>$params["nContractSeq"],
				'sUseIp'=>$params["sUseIp"],
				'sContent'=>$params["sContent"],
				'sWorkType'=>$params["strWorkType"]
		);
		 
		$this->db->insert($this->table[2],$data);
		return $this->db->insert_id();
	}

	 
	
	public function _select_row($where, $index='0') {
		return $this->db->where($where)->get($this->table[$index], 1)->row_array();		
	}

	public function _insert($data, $index='0') {
		return $this->db->insert($this->table[$index], $data);
	}

	public function _delete($where, $index='0') {
		$this->db->where($where);
		$this->db->delete($this->table[$index]);
	}

	public function _update($data, $where, $index='0') {
		return $this->db->update($this->table[$index], $data, $where);
	}
	
	public function _set($params) {
		$this->db->set($params, NULL, FALSE);
		return $this->db->update($this->table[0]);
	}

	public function _replace($data) {
		return $this->db->replace($this->table[0], $data);
	}
}