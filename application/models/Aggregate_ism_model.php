<?php
class Aggregate_ism_model extends CI_Model { //기존 intranet의 Contract_Service_model의 대체.

    // private $t_cdn_data_day     = 'AGGREGATE.tCdnDataDay';
    // private $t_cdn_datetime_day = 'AGGREGATE.tCdnDatetimeDay';
    // private $t_cdn_request_day  = 'AGGREGATE.tCdnRequestDay';
    // private $t_cdn_storage_day  = 'AGGREGATE.tCdnStorageDay';
    // private $t_cdn_traffic_day  = 'AGGREGATE.tCdnTrafficDay';
    // private $t_mrtg_day         = 'AGGREGATE.tMrtgDay';


    function __construct()
    {
        //생성자
        parent::__construct();

        //ism 실DB 적용
        $ismdb = $this->load->database('ism',true);
        $this->ismdb = $ismdb;
    }



    /**
     * AGGREGATE 통한 사용량 추출
     * @param $table        string
     * @param $select       string
     * @param $contractseq  integer
     * @param $startdate    date
     * @param $enddate      date
     */
    public function get_usage_data_list($table, $select, $contractseq, $startdate, $enddate)
    {

        # select 절 설정----------------------------------------------------------------------------------
        $this->ismdb->select($select, FALSE);

        # where 설정--------------------------------------------------------------------------------------
        $this->ismdb->where('nContractSeq', $contractseq);
        $this->ismdb->where('dtCreate >=',  $startdate);
        $this->ismdb->where('dtCreate <=',  $enddate);

        # from 절 설정-----------------------------------------------------------------------------------
        $this->ismdb->from($table);

        # oder by 절 설정---------------------------------------------------------------------------------
        $this->ismdb->order_by('dtCreate ASC');

        # 쿼리 생성
        $query = $this->ismdb->get();

        # 결과 추출 및 확인
        if(!$query)
        {
            $error = $this->ismdb->error();
            $this->errMsg = $error['message'];
        }

        if($query->num_rows() >0)
        {
            return $query->result_array();
        }
        else
        {
            $this->errMsg = "조회된 데이터가 없습니다.";
        }

        return NULL;
    }


}