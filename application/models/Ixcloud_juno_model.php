<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Ixcloud_juno_model extends CI_Model {
    private $success;
    private $http_code;
    private $results;
    private $error;
    private $apihost;
    private $dbKinx;

    public function __construct() {
		parent::__construct();
        $this->load->library('rest', 'restAccount');
        $this->dbKinx = $this->load->database('default',true);

        $this->load->config('kinxconfig', true);
        $this->apihost = Array(
            'juno'=> $this->config->item('host_api_juno', 'kinxconfig')
        );
	}
    function __destruct(){

    }
    # 결과값 초기화
    private function _resetReturn(){
        $this->success = true;
        $this->http_code= null;
        $this->results = null;
        $this->error = null;
    }
    # 결과값 반환
    public function return_result($mime='php'){
        $rtnArray = array(
            'success' => $this->success,
            'status' => $this->http_code,
            'results' => $this->results,
            'error' => $this->error
        );

        switch($mime){
            case 'json':
                $ret= json_encode($rtnArray);
                break;
            case 'php':
                $ret= $rtnArray;
                break;
        }
        return $ret;
    }
    public function getSuccess(){
        return $this->success;
    }
    public function stdClassToArray($d) {
		$tmp = json_encode($d);
        return json_decode($tmp,true);
	}
    public function arrayToStdClass($array){
        return (object) $array;
    }

    #로그인(juno)
    public function login($userid, $password){
        if($userid && $password){
            $path = 'v1/session/';
            $jsonParam = json_encode(array('username'=>$userid, 'password'=>$password));
            $config = array('server'=>$this->apihost['juno']);

            // 서버 상태 체크
            $fp = @fsockopen(str_replace('http://', '', $this->apihost['juno']), 80, $errno, $errstr, 5);
            if(!$fp){
                $this->success = false;
                $this->http_code = 503;
                $this->results = null;
                $this->error = Array('message'=>'Service Unavailable.');
                return $this;
            }

            $this->rest->initialize($config);
            $this->rest->http_header('content-type','application/json; charset=UTF-8');
            $this->rest->http_header('X-Forwarded-for',$this->input->ip_address());
            $retStdClass = $this->rest->post($path, $jsonParam, 'json');
            $status = $this->rest->status();
            $result = $this->stdClassToArray($retStdClass);
            if($result){
                if($status==200){
                    $result['projects'] = Array();
                    $groups = $this->get_group_list($result['key']);
                    if(count($groups)){
                        foreach($groups as $group){
                            $projects = $this->get_project_list($result['key'], ($group->id));
                            if(count($projects)){
                                foreach($projects as $project){
                                    array_push(
                                        $result['projects'],
                                        array('id'=>$project->id, 'name'=>$project->name)
                                    );
                                }
                            }
                        }
                    }
                    #print_r($groups);
                }
                //status : 200 -> 성공(일반), 202 -> otp 설정 사용자 성공. by lizzy
                //$this->success = ($status===200?true:false);                
                $this->success = ( ($status===200 || $status===202 )?true:false);
                $this->http_code = $status;
                $this->results = ($this->success?$result:null);
                $this->error = (!($this->success)?array('message'=>$result['detail']):null);
            }
        }else{
            $this->success = false;
            $this->http_code = 401;
            $this->results = null;
            $this->error = Array('message'=>'필요한 인자가 전달되지 않았습니다.');
        }
        return $this;
    }

    public function get_group_list($token){
        $path = 'v1/accounts/';
        $config = array('server'=>$this->apihost['juno']);
        $this->rest->initialize($config);
        $this->rest->http_header('content-type','application/json; charset=UTF-8');
        $this->rest->http_header('X-Forwarded-for',$this->input->ip_address());
        $this->rest->http_header('Authorization', "Token $token");
        $retStdClass = $this->rest->get($path, 'json');
        return $retStdClass;
    }

    public function get_project_list($token, $group_id){
        $path = "v1/groups/$group_id/projects/";
        $config = array('server'=>$this->apihost['juno']);
        $this->rest->initialize($config);
        $this->rest->http_header('content-type','application/json; charset=UTF-8');
        $this->rest->http_header('X-Forwarded-for',$this->input->ip_address());
        $this->rest->http_header('Authorization', "Token $token");
        $retStdClass = $this->rest->get($path, 'json');
        return $retStdClass;
    }
}
