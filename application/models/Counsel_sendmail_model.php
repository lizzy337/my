<?php
class Counsel_sendmail_model extends CI_Model {
	
	public function get_counsel_servicemanager_email($serviceCode)
	{
		//start 140827 보안강화
		$sql = "SELECT a.nMemberSeq, a.sMemberName, a.sMemberId, a.sInternalEmail, a.sMobilePhone   ";
		$sql .= "FROM tMember as a ";
		$sql .= "Inner Join ( ";
		$sql .= "select nMemberSeq from tNoticeMailList where sCode=? ";
		$sql .= ") as b on a.nMemberSeq = b.nMemberSeq";		
		$query = $this->db->query($sql,$serviceCode);
		//end
		$result = $query->result_array();
		return $result;  
	} 
	
}