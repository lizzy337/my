<?php
class Taxinvoice_detail_ism_model extends CI_Model //intranet Taxinvoice_detail_model 대체
{
    protected $t_billmonth          = 'BILL.tBillMonth';
    protected $t_billdetail         = 'BILL.tBillDetail';
    protected $t_taxinvoicedetail   = 'BILL.tTaxInvoiceDetail';
    protected $t_taxinvoicedivision = 'BILL.tTaxInvoiceDivision';
    protected $v_billadjustments    = 'BILL.vBillAdjustments';
    protected $t_billdetldiscount   = 'BILL.tBillDetailDiscount';
    protected $t_billcloudmanaged   = 'BILL.tBillCloudManaged';
    protected $t_billcloudresorce   = 'BILL.tBillCloudResource';
    protected $t_billadjresource    = 'BILL.tBillAdjustmentsResource';
    protected $t_billdiscount       = 'BILL.tBillDiscount';

    protected $t_contract           = 'CONTRACT.tContract';
    protected $t_resource           = 'CONTRACT.tResource';
    protected $v_product            = 'CONTRACT.vProduct';
    protected $v_customeraccount    = 'CONTRACT.vCustomerAccount';


    function __construct()
    {
        //생성자
        parent::__construct();

        //ism 실DB 적용
        $ismdb = $this->load->database('ism',true);
        $this->ismdb = $ismdb;
    }


    //_select_list
    public function get_list_bySeq($data)
    {
        if(isset($data["sDeleteYN"]) && $data["sDeleteYN"])
        {
            $this->ismdb->where( "A.sDeleteYN = '".$data["sDeleteYN"]."'") ;
        }

        if(isset($data["nTaxInvoiceSeq"]) && $data["nTaxInvoiceSeq"])
        {
            $this->ismdb->where( "A.nTaxInvoiceSeq = ".$data["nTaxInvoiceSeq"]);
        }

        $select = "
            A.*,
            A.sProductType as sServiceType,
            A.sProuctDetailType as sServiceName,
            A.sProductNm as sServiceDetail,
            B.nBillDataType
        ";

        $query = $this->ismdb
            ->select($select, FALSE)
            ->from($this->t_taxinvoicedetail." as A")
            ->join($this->t_billdetail."       as B","A.nBillDetailSeq = B.nBillDetailSeq", "left")
            ->get()
        ;

        if ($query == FALSE)
        {
            $error = $this->db->error();
            $this->errMsg = $error['message'];
        }

        if ($query->num_rows() > 0)
        {
            $this->errMsg = "조회된 데이터가 없습니다.";
        }

        return $query->result_array();

    }




    public function get_BillMonthInfo($nBillMonthSeq)
    {
        $select  = "
            B.nBillMonthSeq as nBillListSeq,
            B.nRemainder as nRemainderPrice,
            B.nReceipt as nReceiptPrice,
            B.dtReceipt as dtReceiptDate,
            B.nRemainderVat as nRemainderVat,
            B.nReceiptVat as nReceiptVat,
            BILL.fn_getRecepit('M', B.sSendType, B.sCurrencyType, B.nCharge, B.nVatPrice, B.nUSDCharge) as billmonth_price,
            BILL.fn_getRecepit('C', B.sSendType, B.sCurrencyType, B.nCharge, B.nVatPrice, B.nUSDCharge) as billmonth_currency,
            ifnull((select sum(A.nPrice) from BILL.tFinanceUnit AS A where A.nBillMonthSeq = B.nBillMonthSeq and A.bDel='N'),0) as income_finance,
            ifnull((select sum(C.nPrice) from BILL.tBillMonthDeposit AS C where C.nBillMonthSeq = B.nBillMonthSeq and C.bDel='N'),0) as income_deposit,
        ";

        $query = $this->ismdb
            ->select($select, FALSE)
            ->from($this->t_billmonth.' AS B')
            ->where("nBillMonthSeq","$nBillMonthSeq")
            ->get()
        ;

        return $query->result_array();
    }





    public function get_taxinvoicedivision_bySeq($data)
    {
        $select  = "dt.nTaxInvoiceDetailSeq, dt.nTaxInvoiceSeq, dt.nBillDetailSeq, dt.sProductType, dt.sProuctDetailType, dt.sProductNm, ";
        $select .= "dv.sBillType, dv.nContractQty, dv.sContractUnit, dt.dtUseStart, dt.dtUseEnd, dv.nQty, dv.nQty as nQuantity, dv.sUnit, dv.nContractCharge, dv.nCharge, dv.sCurrencyType, dv.sDetailItemflag ";
        $query = $this->ismdb
            ->select($select, FALSE)
            ->from($this->t_taxinvoicedetail." as dt")
            ->join($this->t_taxinvoicedivision." as dv","dt.nTaxInvoiceDetailSeq=dv.nTaxInvoiceDetailSeq and dv.sDeleteYN='N' and dv.sDetailItemflag in ('Y') ")
            ->where("dt.nTaxInvoiceSeq",$data["nTaxInvoiceSeq"])
            ->where("dt.sDeleteYN",$data["sDeleteYN"])
            ->order_by("dt.sProductType, dt.sProuctDetailType, dt.sProductNm")
            ->get();
        ;

        return $query->result_array();
    }





    public function get_taxinvoicedivision_byDetailSeq($data)
    {
        $select  = "dt.nTaxInvoiceDetailSeq, dt.nTaxInvoiceSeq, dt.nBillDetailSeq, dt.sProductType, dt.sProuctDetailType, dt.sProductNm, ";
        $select .= "dv.sBillType, dv.sSubject, dv.nContractQty, dv.sContractUnit, dt.dtUseStart, dt.dtUseEnd, dv.nQty, dv.nQty as nQuantity, dv.sUnit, dv.nContractCharge, dv.nCharge, dv.sCurrencyType, dv.sDetailItemflag ";
        $query = $this->ismdb
            ->select($select, FALSE)
            ->from($this->t_taxinvoicedetail." as dt")
            ->join($this->t_taxinvoicedivision." as dv","dt.nTaxInvoiceDetailSeq=dv.nTaxInvoiceDetailSeq and dv.sDeleteYN='N' and dv.sDetailItemflag in ('Y') ")
            ->where("dt.nTaxInvoiceSeq",$data["nTaxInvoiceSeq"])
            ->where("dt.nTaxInvoiceDetailSeq",$data["nTaxInvoiceDetailSeq"])
            ->where("dt.sDeleteYN",$data["sDeleteYN"])
            ->where("dt.nDivisionFlag",$data["nDivisionFlag"])
            ->order_by("dt.sProductType, dt.sProuctDetailType, dt.sProductNm")
            ->get()
        ;

        return $query->result_array();
    }





    /**
     * tBillDetailSeq로 Cloud 계약에 기본 정보를 추출한다.
     * #### ISM에도 동일하게 사용중 수정시 같이 수정 필요 ######
     *
     * @param integer $seq
     * @return array/NULL
     */
    public function get_contract_for_cloud($seq)
    {
        # ISM과 다른 점은 ISM은 확정하기 위한 데이터 추출이고, myKinx는 확정 후 데이터 추출이다.
        # - tBillDetail의 nVatPrice를 구한다.
        # - vCustomerAccount 조인해 고객명을 구한다.
        # - tBillDetail에서 dtBilling을 구한다.
        $select = "
            CT.sName, CT.arDetailInfo,
            AC.sCName as sCustomerName,
            BD.arDiscountOpt, BD.sCurrencyType, BD.nProductSeq, BD.nVatPrice, BD.dtBilling,
            if(BD.sCurrencyType='USD', BD.nExcCharge, BD.nCharge) AS nCharge,
            if(BA.nAdjCharge is null, 0, if(BD.sCurrencyType='USD', BA.nExcCharge, BA.nAdjCharge)) AS nAdjCharge,
            ifnull(BA.nCnt,0) AS nCnt,
            ifnull(DS.nDiscount,0) AS nDiscount,
            ifnull(MN.nManagedCharge,0) AS nManagedCharge,
        ";

        $query = $this->ismdb
            ->select($select, FALSE)
            ->from($this->t_billdetail.'       AS BD')//tBillDetail
            ->join($this->t_contract.'         AS CT', "BD.nContractSeq = CT.nContractSeq")//tContract
            ->join($this->v_customeraccount.'  AS AC', "CT.nAccountSeq = AC.nAccountSeq")//vCustomerAccount
            ->join($this->v_billadjustments.'  AS BA', "BD.nBillDetailSeq = BA.nBillDetailSeq","left")//tBillAdjustments
            ->join($this->t_billdetldiscount.' AS DS', "BD.nBillDetailSeq = DS.nBillDetailSeq","left")//tBillDetailDiscount
            ->join($this->t_billcloudmanaged.' AS MN', "BD.nBillDetailSeq = MN.nBillDetailSeq","left")//tBillCloudManaged
            ->where(array('BD.nBillDetailSeq' => $seq))
            ->get()
        ;


        if ($query && $query->num_rows() > 0)
        {
            return $query->row_array();
        }
        return NULL;

    }



    /**
     * Seq로 Cloud 계약에 속한 리소스 정보(할인 및 청구 금액)를 추출한다.
     * #### ISM에도 동일하게 사용중 수정시 같이 수정 필요 ######
     *
     * @param integer $seq
     * @return array/NULL
     */
    public function get_contract_cloudresource($seq)
    {
        $select = "
            RS.nResourceSeq,
            RS.sType,
            RS.arDetailInfo,
            BC.dtUseStart,
            BC.dtUseEnd,
            BC.nPrice,
            BC.nCharge,
            BC.arDetailInfo AS arResourceDetail,
            PD.sName, AR.nAdjCharge,
            (BC.nDiscountBasic + BC.nDiscountStabilization) AS nDiscount
        ";
        //$t_bars

        $query = $this->ismdb
            ->select($select, FALSE)
            ->from($this->t_resource.'         RS')
            ->join($this->t_billcloudresorce.' BC', "BC.nResourceSeq = RS.nResourceSeq")
            ->join($this->t_billdetail.'       BD', "BC.nContractSeq = BD.nContractSeq and BD.sDeleteYN='N' and concat(substring(BD.dtUseStart, 1,8), '01') = concat(substring(BC.dtUseStart, 1,8), '01') ")
            ->join($this->v_product.'          PD', "RS.nProductSeq  = PD.nProductSeq", 'left')
            ->join($this->t_billadjresource.'  AR', "BC.nResourceSeq = AR.nResourceSeq and BD.nBillDetailSeq = AR.nBillDetailSeq", 'left')
            ->where(array('BD.nBillDetailSeq' => $seq, 'RS.sServiceType' => 'I'))
            ->get()
        ;

        if ($query && $query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return NULL;
    }




    /**
     * Seq로 Cloud 계약에 속한 리소스 정보(할인 및 청구 금액)를 추출한다.
     * #### ISM에도 동일하게 사용중 수정시 같이 수정 필요 ######
     *
     * @param integer $seq
     * @return array/NULL
     */
    public function get_contract_cloudresource_group($seq)
    {
        $query = $this->ismdb
            ->where(array('nBillDetailSeq' => $seq, 'nGroupFlag'=>1))
            ->get($this->t_billadjresource);

        if ($query && $query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return NULL;
    }




    /**
     * BillDetailSeq로 Cloud 계약에 속한 계약할인정보를 추출한다.
     * #### ISM에도 동일하게 사용중 수정시 같이 수정 필요 ######
     *
     * @param integer $seq
     * @return array/NULL
     */
    public function get_discount_by_billdetailseq($seq)
    {
        $query = $this->ismdb
            ->where(array('nBillDetailSeq' => $seq))
            ->get($this->t_billdiscount);

        if ($query && $query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return NULL;
    }
}