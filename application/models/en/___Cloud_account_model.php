<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH ."models/Captcha_model.php");

class Cloud_account_model extends CI_Model {
    private $adminToken = null;
    private $host_ixcloud = Array(
        'account' => 'http://182.161.113.112:4031',
        'utility' => 'http://182.161.113.105:8010'
    );
    protected $table = array("tCloudLink");
     public function __construct() {
		parent::__construct();
        $this->load->library('rest', 'restAccount');
	}        
    function stdClassToArray($d) {
		$tmp = json_encode($d);
        return json_decode($tmp,true);
	}
    //클라우드 계정 정보 가져오기
    public function getCloudLink($where){
       return $this->db->where($where)->order_by('nCloudType asc, sCloudUserId asc')->get($this->table[0])->result_array();
    }

    //비밀번호 찾기 SMS발송용 코드 생성
    public function getFindpasswordCode($params=array()){
        /*
         * params{user_name, user_cellphone, flag=null, put_type='password'} flag!=reset
         */
        $userid = $params['userid'];        
        $user_cellphone = $params['user_cellphone'];
        unset($params['userid']);
        //회원정보 확인
        $url = 'register/' . $userid;
        $params = json_encode($params);

        $config = array('server' => $this->host_ixcloud['account']);
        $this->rest->initialize($config);
        $this->rest->http_header('content-type','application/json; charset=UTF-8');
        $retStdClass = $this->rest->put($url, $params, 'json');
        $status = $this->rest->status();
        $result = $this->stdClassToArray($retStdClass);
        $success = false;
        $error = array();
  
        if($status==200 && $result['result']==1){ //sms 발송
            $this->load->helper('string');
            $code = random_string('numeric',6);
            $success=true;
            $this->load->model('sms_model');
            $SID = "ixc" . date('ymdHis');
            $msg = '[ixCloud] 회원님의 인증번호는 "' . $code . '" 입니다.';
            $smsRet = $this->sms_model->send($SID, $user_cellphone, $msg);
            $this->session->set_userdata('ixcloud_findpwd', json_encode(Array('code'=>$code,'phone'=>$user_cellphone)));
            
        }
        return Array('success'=> $success, 'status' => $status, 'result'=>'');
    }
    //패스워드 찾기
    public function resetPassword($params=array()){
        /*
         * params{user_name, user_cellphone, flag=null, put_type='password'} flag=reset
         */
        $userid = $params['userid'];
        $username = $params['user_name'];
        $user_cellphone = $params['user_cellphone'];
        unset($params['userid']);
        $url = 'register/' . $userid;
        $params = json_encode($params);
        
        $config = array('server' => $this->host_ixcloud['account']);
        $this->rest->initialize($config);
        $this->rest->http_header('content-type','application/json; charset=UTF-8');
        $retStdClass = $this->rest->put($url, $params, 'json');
        $status = $this->rest->status();
        $result = $this->stdClassToArray($retStdClass);
        $success = false;
        $error = array();
        if($status=='200' && is_array($result['result']) && isset($result['result']['password'])){
            $new_password = $result['result']['password'];
            unset($result['result']['password']);
            $mailParam = Array('to_name'=>$username,'to_mail'=> $userid,'password'=>$new_password);
            $mailret = $this->email_findpassword($mailParam);
            $this->session->unset_userdata('ixcloud_findpwd');
        }
        return Array('success'=> $success, 'status' => $status, 'result'=>'');
    }
    //관리자 토큰 가져오기(사용안될듯..)
    public function getManagerToken(){
        $url = 'manage/auth';
        $params = Array(
            'managerid'=>'blueface@kinx.net',
            'password'=>'111111'
        );
        $params = json_encode($params);
        
        $config = array('server' => $this->host_ixcloud['account']);
        $this->rest->initialize($config);
        $this->rest->http_header('content-type','application/json; charset=UTF-8');
        $retStdClass = $this->rest->post($url,$params, 'json');
        $ret = $this->stdClassToArray($retStdClass);
        $token = $ret['result']['token'];
        $this->adminToken = $token;
        echo $this->adminToken;
    }
    
    //자동로그인(Essex)
    public function runAuthEssex($userid='', $type=''){#{type:{1:essex,2:grizzly}}
        //계정정보 확인
        $nContractSeq = $this->session->userdata('nContractSeq');
        $existsChk = $this->getCloudLink(Array('nContractSeq'=>$nContractSeq, 'sCloudUserId'=>$userid, 'nCloudType'=>$type));
        if(count($existsChk)==0){
			$this->tinyjs->pageClose("계정 정보를 찾을 수 없습니다.");
            exit;
        }        
        $authHost = 'http://auth.ixcloud.net';
        $url = 'login/';
        $params = Array(
            'userid'=>$userid,
            'password'=>$this->getUserPassword($userid)
        );
        $config = array('server' => $authHost);
        $this->rest->initialize($config);
        
        $params = json_encode($params);
        //SessionId Get
        $this->rest->option('HEADER',1);
        $this->rest->format('application/json');
        
        $retStr = $this->rest->post($url, $params);

        //Get SessionId
        preg_match('/Set-Cookie:.*connectsid=(.*);/',$retStr, $sessionPreg);
        $retArray = explode(";", $sessionPreg[1]);
        $strSessionId = $retArray[0];
       
        $token = base64_encode($strSessionId);
        $this->load->helper('url');
        $next = urlencode('/account/user/info/usage_info.php');
        redirect('https://www.ixcloud.net/account/user/token.php?t=' . $token . '&next=' . $next,'location',301);
        exit;
    }
    //사용자 패스워드 가져오기(ixcloud.net 서버에서 패스워드 가져와서 decrypt)
    public function getUserPassword($userid=''){
        $url = 'p/' . $userid;
        $config = array('server'=>$this->host_ixcloud['utility']);
        $this->rest->initialize($config);
        $getPassword = $this->rest->post($url,null,'text');
        
        $this->load->library('crypto');
        $ret = $this->crypto->decrypt($getPassword);
        return $ret;
     }
    //사용자 기본 정보 가져오기
    private function getUserDefault($userid=''){
        $password = $this->getUserPassword($userid);
        $url = 'auth';
        $params = json_encode(Array('userid'=>$userid, 'password'=>$password));
        
        $config = array('server'=>$this->host_ixcloud['account']);
        $this->rest->initialize($config);
        $retStdClass = $this->rest->post($url,$params,'json');
        if($this->rest->status()==200){
            $ret = $this->stdClassToArray($retStdClass);
            $ret = $ret['result'];
        }else{
            $ret = null;
        }
        return $ret;
    }
    //사용자 정보 가져오기
    public function getUserInfo($userid='', $type='1'){ #{type:{1:essex,2:grizzly}}
        //계정정보 확인
        $nContractSeq = $this->session->userdata('nContractSeq');
        $existsChk = $this->getCloudLink(Array('nContractSeq'=>$nContractSeq, 'sCloudUserId'=>$userid, 'nCloudType'=>$type));
        if(count($existsChk)==0){
			$this->tinyjs->pageBack("계정 정보를 찾을 수 없습니다.");
            exit;
        }
        $infoDef = $this->getUserDefault($userid);
        $status;
        $error = array();
        $result = array();
        $success=false;
        
        if(!is_null($infoDef)){
            $user_id = $infoDef['user_id'];
            $account_id = $infoDef['account_id'];
            $token = $infoDef['token'];
            
            $config = array('server'=>$this->host_ixcloud['account']);
            $this->rest->initialize($config);
            
            $this->rest->http_header('content-type','application/json; charset=UTF-8');
            $this->rest->http_header('ix-auth-token',$token);
            
            $aryUserInfo = Array();
            //User Info가져오기
            $url = 'user/'. $user_id;
            $retStdClass = $this->rest->get($url,null,'json');
            if($this->rest->status()=='200'){
                $ret = $this->stdClassToArray($retStdClass);
                $ret = $ret['result'][0];
                $aryUserInfo = Array(
                    'cellphone' => $ret['cellphone'],
                    'email' => $ret['email'],
                    'phone' => $ret['phone'],
                    'name' => $ret['name']
                );
            }
            //Account Info 가져오기
            $url = 'account/'. $account_id;
            $this->rest->http_header('content-type','application/json; charset=UTF-8');
            $this->rest->http_header('ix-auth-token',$token);
            $retStdClass = $this->rest->get($url,null,'json');
            if($this->rest->status()=='200'){
                $ret = $this->stdClassToArray($retStdClass);
                $ret = $ret['result'][0];
                $aryUserInfo['zipcode'] = $ret['zipcode'];
                $aryUserInfo['address'] = $ret['address'];
                $aryUserInfo['address2'] = $ret['address2'];
            }
            $status = '200';
            $success = true;
            $result = $aryUserInfo;
        }else{
            //사용자 정보 찾을 수 없음.
            $status='400';
            $success = false;
            $error = Array('message'=>'사용자 정보를 찾을 수 없습니다.');
        }
        return Array('success'=>$success,'status'=>$status,'error'=>$error, 'result'=>$result);
    }
    //패스워드 변경
    public function chgUserPassword($userid='',$params=array()){
        
    }
    //사용자 정보 업데이트
    public function chgUserInfo($userid='', $params=array()){
        
    }
    //청구서 목록 가져오기
    public function getBillList($userid='',$start,$end){
        
    }
    //청구서 목록 세부내역
    public function getBillDetail($userid='',$start,$end){
        
    }    
    //서비스 정보 목록
    //서비스 정보 상세보기
    
    //패스워드 찾기 이메일 발송
    public function email_findpassword($params = array()){
        $to_name = $params['to_name'];
        $to_mail = $params['to_mail'];
        $password = $params['password'];
        
        $from_mail = 'support@ixcloud.net';
        $from_name = 'ixCloud 인증담당자';        
        
        if($this->input->server('SERVER_PORT')==443){
            $protocol = 'https';
        }else{
            $protocol = 'http';
        }
        $this->load->library('email');
        $title = '[ixClolud] 임시비밀번호가 발급되었습니다';
        $body = "".
                "<html>".
                "<head>".
                "<title>KINX</title> ".
                "<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=utf-8\">".
                "<style>".
                "	<!-- body, td  { font-family:Dotum, Verdana; font-size:12px; line-height:18px; color:#393939;} -->".
                "</style>".
                "</head> ".
                "<body leftmargin=\"0\" topmargin=\"0\">".
                " 고객님의 패스워드는	" . $password . "입니다".
                "</body> ".
                "</html> ";
        $this->email->set_mailtype('html');
        
        $this->email->clear();
        $this->email->from($from_mail, $from_name);
        $this->email->to($to_name . '<' . $to_mail . '>');
        $this->email->subject($title);
        $this->email->message($body);
        $ret = $this->email->send();
        //print_r($this->email->print_debugger());
        return $ret;
    }
}
