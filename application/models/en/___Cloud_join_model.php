<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH ."models/Captcha_model.php");

class Cloud_join_model extends CI_Model {
    private $joinStep1Status;
    private $joinStep2Status;
    private $joinStep1Result;
    private $joinStep2Result;
    public $account_id;
    
    function stdClassToArray($d) {
		$tmp = json_encode($d);
        return json_decode($tmp,true);
	}
    function joinStart(){
        $this->load->model('captcha_model');
        $captcha_time = $this->input->post('captcha_time');
        $captcha_code = $this->input->post('captcha_code');
        //$captcha_chk = $this->captcha_model->checkCaptcha($captcha_code, $this->input->ip_address(),$captcha_time);
        $captcha_chk = 1;
        if(!$captcha_chk){
            $this-> output->set_output('captcha check fail!!');
            return false;
        }
        if($captcha_chk) {
            $this->load->library('rest');
            $config = array('server' => 'http://182.161.113.112:4031');
            $this->rest->initialize($config);
            
            $this->joinStep1();
            if($this->joinStep1Status!=200){
                $retAryJoinStep1 = $this->stdClassToArray($this->joinStep1Result);
                //print_r($retAry);
                return false;
            }
            $this->joinStep2();
            if($this->joinStep2Status!=200){
                $retAryJoinStep2 = $this->stdClassToArray($this->joinStep2Result);
                return false;
            }
            return true;
        }
    }
    function joinStep1(){
        //Step1 가입시작 - Start
        $post_type = 'step1';
        $name = $this->input->post('name');         
        $userid = $this->input->post('userid');
        $password = $this->input->post('password');
        $aryStep1 = array(
            "post_type" => $post_type,
            "userid" => $userid,
            "name" => $name,
            "password" => $password
        );
        $paramsStep1 = json_encode($aryStep1);
        $this->rest->http_header('content-type','application/json; charset=UTF-8');
        $retStep1StdClass = $this->rest->post('register', $paramsStep1, 'json');
        $retStep1Status = $this->rest->status();
        
        $this->joinStep1Status = $retStep1Status;
        $this->joinStep1Result = $retStep1StdClass;
        return true;
    }
    function joinStep2(){ //세부정보 입력
        $post_type = 'step2';
        $user_id = '';
        $userid = $this->input->post('userid');
        $user_name = $this->input->post('name');
        $company_type = $this->input->post('user_type');
        $personal_number = '0000001111111';
        
        if($company_type != 'A'){
            $account_name = $this->input->post('account_name');
            $business_number_01 = $this->input->post('$business_number_01');
            $business_number_03 = $this->input->post('$business_number_02');
            $business_number_02 = $this->input->post('$business_number_03');
            $ceo_name = $this->input->post('ceo_name');
            $business_number = $business_number_01 . "-" . $business_number_02 . "-" . $business_number_03;
        }else{
            $account_name = '';
            $business_number_01 = '';
            $business_number_03 = '';
            $business_number_02 = '';
            $ceo_name = '';
            $business_number = '';
        }
        $zipcode1 = $this->input->post('zipcode1');
        $zipcode2 = $this->input->post('zipcode2');
        $address1 = $this->input->post('address1');
        $address2 = $this->input->post('address2');
        $phone_01 = $this->input->post('phone_01');
        $phone_02 = $this->input->post('phone_02');
        $phone_03 = $this->input->post('phone_03');
        $mobile_01 = $this->input->post('mobile_01');
        $mobile_02 = $this->input->post('mobile_02');
        $mobile_03 = $this->input->post('mobile_03');        
        $zipcode = $zipcode1 . $zipcode2;
        $address = $address1;
        $address2 = $address2;
        $user_phone = $phone_01 . $phone_02 . $phone_03;
        $user_cellphone = $mobile_01 . $mobile_02 . $mobile_03;
        $aryStep2 = array(
            'post_type' => 'step2',
            'user_id' => '',
            'userid' => $userid,
            'company_type' => $company_type,
            'user_name' => $user_name,
            'personal_number' => '0000001111111',
            'account_name' => $account_name,
            'business_number' => $business_number,
            'ceo_name' => $ceo_name,
            'zipcode' => $zipcode,
            'address' => $address,
            'address2' => $address2,
            'user_phone' => $user_phone,
            'user_cellphone' => $user_cellphone,
            'register_type' => null,
            'description' => ''
        );
        $paramsStep2 = json_encode($aryStep2);
        $this->rest->http_header('content-type','application/json; charset=UTF-8');
        $retStep2StdClass = $this->rest->post('register', $paramsStep2, 'json');
        $retStep2Status = $this->rest->status();
        
        $this->joinStep2Status = $retStep2Status;
        $this->joinStep2Result = $retStep2StdClass;
        
        $retStep2Arry = $this->stdClassToArray($retStep2StdClass);
        $account_id = $retStep2Arry['result']['account_id'];
        $this->session-> set_userdata('cloud_userid', $userid);
        $this->session-> set_userdata('cloud_account_id', $account_id);
        return true;
    }
    function joinStep3(){
        $userid = $this->session->userdata('cloud_userid');
        $account_id = $this->session->userdata('cloud_account_id');
        
    }
    public function checkEmailDupl($email){
        $this->load->library('rest');
        $config = array('server' => 'http://182.161.113.112:4031');
        $this->rest->initialize($config);
        $chkmail = $this->rest->get('register/'.$email);        
        return $chkmail;
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

