<?php
class Inquiry_model extends CI_Model {
	protected $table = array("tEngCounselingList");
	
	function __construct(){
		//생성자
		parent::__construct();
	}

	
	public function insert($data){
		return $this->db->insert($this->table[0], $data);
	}
	
	public function getSubjectName($subject)
	{	
		$sql = "SELECT sCodeName FROM tSysCode WHERE sGroupCode='G133' and sCode=? ";//140828 보안강화
		$query = $this->db->query($sql,$subject);////140828 보안강화
		return $query->result_array();
	}
	
	//최근 CounSeq를 반환한다. (질문이 생성될때마다 그 질문의 CounSeq를 만들기 위해.)
	public function getMaxCounSeq()
	{
		$query = $this->db->select("MAX(nCounSeq) maxCounSeq")->get(" tEngCounselingList  ");
		if ($query->num_rows() > 0) $maxGroup = $query->first_row()->maxCounSeq;
		return $maxGroup;
	}
	 
}

