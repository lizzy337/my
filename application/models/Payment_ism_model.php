<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Payment_ism_model extends CI_Model {

	//public function payment02_req(){} //controller/support/payment/payment02_req에서 처리
	//public function payment02_res(){} //controller/support/payment/payment02_res에서 처리

	protected $t_XpayApproval          = 'BILL.tXpayApproval';
	protected $t_billmonth             = 'BILL.tBillMonth';
	protected $t_billtype              = 'BILL.tBillType';
	protected $t_OlineApprovalCustomer = 'BILL.tOlineApprovalCustomer';
	protected $t_OlineApprovalDetail   = 'BILL.tOlineApprovalDetail';
    protected $t_log                   = 'CONTRACT.tLog';

	function __construct(){
		//생성자
		parent::__construct();


		//ism 실DB 적용
		$ismdb = $this->load->database('ism',true);
		$this->ismdb = $ismdb;
	}

	//1. db 처리
	Public function xpay_input($xpay){
		$tmpPayType = $xpay->Response("LGD_PAYTYPE",0);
		switch($xpay->Response("LGD_PAYTYPE",0))
		{
			case "SC0010" : //신용카드
				$data = array(
					"sOrderNumber" => $xpay->Response("LGD_OID",0), //kinx 거래번호(주문번호)
					"sUplusOrderNumber" => $xpay->Response("LGD_TID",0), //LG 데이콤 거래번호
					"sUplusId" => $xpay->Response("LGD_MID",0), //LG 데이콤 발급아이디(ex:kinx0#)
					"sResultCode" => $xpay->Response("LGD_RESPCODE",0), //응답코드(0000:성공)
					"sResultMessage" => $xpay->Response("LGD_RESPMSG",0), //응답메시지(ex:결제성공)
					"sPaymentType" => $xpay->Response("LGD_PAYTYPE",0), //결제수단(신용카드:SC0010, 계좌이체:SC0030 ...)
					"nAmount" => $xpay->Response("LGD_AMOUNT",0), //결제금액
					"dtPayDate" => $xpay->Response("LGD_PAYDATE",0), //결제일시
					"sBuyerName" => $xpay->Response("LGD_BUYER",0), //구매자
					"sBuyerSsn" => $xpay->Response("LGD_BUYERSSN",0), //구매자 주민등록번호
					"sBuyerPhone" => $xpay->Response("LGD_BUYERPHONE",0), //구매자휴대폰번호
					"sBuyerAddress" => $xpay->Response("LGD_BUYERADDRESS",0), //구매자주소
					"sBuyerEmail" => $xpay->Response("LGD_BUYEREMAIL",0), //구매자이메일
					"sBuyerId" => $xpay->Response("LGD_BUYERID",0), //구매자아이디
					"sDeliveryInfo" => $xpay->Response("LGD_DELIVERYINFO",0), //배송정보
					"nProductCode" => $xpay->Response("LGD_PRODUCTCODE",0), //상품코드
					"sProductInfo" => $xpay->Response("LGD_PRODUCTINFO",0), //상품정보
					"sCardNumber" => $xpay->Response("LGD_CARDNUM",0), //신용카드번호
					"sCardInstalmentMonth" => $xpay->Response("LGD_CARDINSTALLMONTH",0), //신용카드할부개월
					"sCardNointYn" => $xpay->Response("LGD_CARDNOINTYN",0), //신용카드무이자여부
					"sFinanceCode" => $xpay->Response("LGD_FINANCECODE",0), //결제기관코드
					"sFinanceName" => $xpay->Response("LGD_FINANCENAME",0), //결제기관명
					"sFinanceAuthNumber" => $xpay->Response("LGD_FINANCEAUTHNUM",0), //결제기관승인번호
					"sHashData" => $xpay->Response("LGD_HASHDATA",0), //해쉬데이타
					"nTransAmount" => $xpay->Response("LGD_TRANSAMOUNT",0), //환율적용금액
					"nExchangeRate" => $xpay->Response("LGD_EXCHANGERATE",0), //적용환율
					"sAffiliateCode" => $xpay->Response("LGD_AFFILIATECODE",0), //합병코드
				);
				break;
			case "SC0030" : //계좌이체
				$data = array(
					"sOrderNumber" => $xpay->Response("LGD_OID",0), //kinx 거래번호(주문번호)
					"sUplusOrderNumber" => $xpay->Response("LGD_TID",0), //LG 데이콤 거래번호
					"sUplusId" => $xpay->Response("LGD_MID",0), //LG 데이콤 발급아이디(ex:kinx0#)
					"sResultCode" => $xpay->Response("LGD_RESPCODE",0), //응답코드(0000:성공)
					"sResultMessage" => $xpay->Response("LGD_RESPMSG",0), //응답메시지(ex:결제성공)
					"sPaymentType" => $xpay->Response("LGD_PAYTYPE",0), //결제수단(신용카드:SC0010, 계좌이체:SC0030 ...)
					"nAmount" => $xpay->Response("LGD_AMOUNT",0), //결제금액
					"dtPayDate" => $xpay->Response("LGD_PAYDATE",0), //결제일시
					"sBuyerName" => $xpay->Response("LGD_BUYER",0), //구매자
					"sBuyerSsn" => $xpay->Response("LGD_BUYERSSN",0), //구매자 주민등록번호
					"sBuyerPhone" => $xpay->Response("LGD_BUYERPHONE",0), //구매자휴대폰번호
					"sBuyerAddress" => $xpay->Response("LGD_BUYERADDRESS",0), //구매자주소
					"sBuyerEmail" => $xpay->Response("LGD_BUYEREMAIL",0), //구매자이메일
					"sBuyerId" => $xpay->Response("LGD_BUYERID",0), //구매자아이디
					"sDeliveryInfo" => $xpay->Response("LGD_DELIVERYINFO",0), //배송정보
					"nProductCode" => $xpay->Response("LGD_PRODUCTCODE",0), //상품코드
					"sProductInfo" => $xpay->Response("LGD_PRODUCTINFO",0), //상품정보
					"sAccountOwner" => $xpay->Response("LGD_ACCOUNTOWNER",0), //가상계좌계좌주명
					"sCashReceiptNumber" => $xpay->Response("LGD_CASHRECEIPTNUM",0), //현금영수증승인번호
					"sCashReceiptSelfYn" => $xpay->Response("LGD_CASHRECEIPTSELFYN",0), //현금영수증자진발급제유무
					//"sCashReceiptKind" => $xpay->Response("LGD_CASHRECEIPTKIND",0), //현금영수증종류(미발행이면 이 값을 구할때 에러 발생하므로 주석처리. 150603)
					"sFinanceCode" => $xpay->Response("LGD_FINANCECODE",0), //결제기관코드
					"sFinanceName" => $xpay->Response("LGD_FINANCENAME",0), //결제기관명
					"sHashData" => $xpay->Response("LGD_HASHDATA",0), //해쉬데이타
				);
				break;
			case "SC0040" : //가상계좌
				$data = array(
						"sOrderNumber" => $xpay->Response("LGD_OID",0), //kinx 거래번호(주문번호)
						"sUplusOrderNumber" => $xpay->Response("LGD_TID",0), //LG 데이콤 거래번호
						"sUplusId" => $xpay->Response("LGD_MID",0), //LG 데이콤 발급아이디(ex:kinx0#)
						"sResultCode" => $xpay->Response("LGD_RESPCODE",0), //응답코드(0000:성공)
						"sResultMessage" => $xpay->Response("LGD_RESPMSG",0), //응답메시지(ex:결제성공)
						"sPaymentType" => $xpay->Response("LGD_PAYTYPE",0), //결제수단(신용카드:SC0010, 계좌이체:SC0030 ...)
						"nAmount" => $xpay->Response("LGD_AMOUNT",0), //결제금액
						"dtPayDate" => $xpay->Response("LGD_PAYDATE",0), //결제일시
						"sBuyerName" => $xpay->Response("LGD_BUYER",0), //구매자
						"sBuyerSsn" => $xpay->Response("LGD_BUYERSSN",0), //구매자 주민등록번호
						"sBuyerPhone" => $xpay->Response("LGD_BUYERPHONE",0), //구매자휴대폰번호
						"sBuyerAddress" => $xpay->Response("LGD_BUYERADDRESS",0), //구매자주소
						"sBuyerEmail" => $xpay->Response("LGD_BUYEREMAIL",0), //구매자이메일
						"sBuyerId" => $xpay->Response("LGD_BUYERID",0), //구매자아이디
						"sDeliveryInfo" => $xpay->Response("LGD_DELIVERYINFO",0), //배송정보
						"nProductCode" => $xpay->Response("LGD_PRODUCTCODE",0), //상품코드
						"sProductInfo" => $xpay->Response("LGD_PRODUCTINFO",0), //상품정보
						"nAccountNumber" => $xpay->Response("LGD_ACCOUNTNUM",0), //가상계좌발급번호
						"nCastAmount" => $xpay->Response("LGD_CASTAMOUNT",0), //가상계좌입금누적금액
						"nCascAmount" => $xpay->Response("LGD_CASCAMOUNT",0), //가상계좌현입금금액
						"sCasFlag" => $xpay->Response("LGD_CASFLAG",0), //가상계좌거래종류(R:할당,I:입금,C:취소)
						"sPayer" => $xpay->Response("LGD_PAYER",0), //가상계좌입금자명
						"sCasseqNumber" => $xpay->Response("LGD_CASSEQNO",0), //가상계좌일련번호
						//"sCashReceiptKind" => $xpay->Response("LGD_CASHRECEIPTKIND",0), //현금영수증종류(0:소득공제용, 1:지출증빙용, 미발행이면 없음. 미발행이면 이 값을 구할때 에러 발생하므로 주석처리. 150603)
						"sFinanceCode" => $xpay->Response("LGD_FINANCECODE",0), //결제기관코드
						"sFinanceName" => $xpay->Response("LGD_FINANCENAME",0), //결제기관명
						"sHashData" => $xpay->Response("LGD_HASHDATA",0), //해쉬데이타
				);
				break;
		}//end switch

		$this->ismdb->insert($this->t_XpayApproval,$data);//org
		return $this->ismdb->insert_id();
	}



	//get_tBillList ->
	Public function get_tBillMonth($nSeq, $onlycount = FALSE)
	{
		$query = $this->ismdb->select('A.*, A.nCharge as nChargePrice, A.nRemainder as nRemainderPrice, A.nReceipt as nReceiptPrice ', FALSE)
							->from($this->t_billmonth." as A")
							->where('nBillMonthSeq', $nSeq)
					 		->get();
 		if ($query !== FALSE)
 		{
 			if($onlycount)
 			{
 				return ($query && $query->num_rows() > 0) ? $query->num_rows() : 0;
 			}
 			elseif ($query->num_rows() > 0)
 			{
 				return $query->result_array();
 			}
 			else
 			{
 				$this->errMsg = "조회된 데이터가 없습니다.";
 			}
 		}
 		else
 		{
 			$error = $this->ismdb->error();
 			$this->errMsg = $error['message'];
 		}
	}

	Public function get_xPayApprovalSeq($sOrderNumber, $onlycount = FALSE)
	{
		$query = $this->ismdb->select('nXpayApprovalSeq', FALSE)
										->from($this->t_XpayApproval)
										->where('sOrderNumber', $sOrderNumber)
										->get();

		if ($query !== FALSE)
		{
			if($onlycount)
			{
				return ($query && $query->num_rows() > 0) ? $query->num_rows() : 0;
			}
			elseif ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				$this->errMsg = "조회된 데이터가 없습니다.";
			}
		}
		else
		{
			$error = $this->ismdb->error();
			$this->errMsg = $error['message'];
		}

	}

	//xPayApprovalSeq, BillListSeq, 결제액, 미수액
	//update_tBillList -> update_tBillMonth
	Public function update_tBillMonth($nXPayApprovalSeq, $nSeq, $nAmount, $nRemainPrice)
	{
        # 1) 현재 데이터를 추출한다.
        $query    = $this->ismdb->where('nBillMonthSeq', $nSeq)->get($this->t_billmonth);
        $cur_data = $query->row_array();

        if($nAmount == $cur_data){
            $nRemainderVat = 0;
            $nReceiptVat   = ($cur_data['sSendType']=='C') ? 0 : $cur_data['nVatPrice'];
        }
        else{
            $nReceiptVat   = ($cur_data['sSendType']=='C') ? 0 : vat($nAmount, TRUE, TRUE, TRUE) - $nAmount;
            $nRemainderVat = ($cur_data['sSendType']=='C') ? 0 : $cur_data['nVatPrice'] - $nReceiptVat;
        }


        # 2) 현데이터와 변경할 데이터를 비교해서 변경된 데이터를 확인한다.
        $change_data = array(
            'nReceipt'          => $nAmount,
            'nReceiptVat'       => $nReceiptVat,
            'nRemainder'        => $nRemainPrice,
            'nRemainderVat'     => $nRemainderVat,
            'nXpayApprovalSeq'  => $nXPayApprovalSeq,
            'sMemo'             => $cur_data['sMemo'].PHP_EOL.date("YmdH").' - 카드결제 입금',
            'dtReceipt'         => date("Y-m-d H:i:s.u")
        );


        $this->ismdb->trans_start(); # transection 시작한다. ================================
        # 3) 월청구 정보를 수정한다.
        $this->ismdb->where('nBillMonthSeq', $nSeq)->update($this->t_billmonth, $change_data);
        $this->ismdb->reset_query();

        # 4) 변경 이력을 등록한다.
        $this->_insert_change_info($this->t_billmonth, $change_data, $cur_data, $nSeq);
        $this->ismdb->reset_query();

        $this->ismdb->trans_complete(); #transection 종료한다. ===============================

        if ($this->ismdb->trans_status()) {
            return TRUE;
        } else {
            return FALSE;
        }
	}



	//2. 신청정보 등록 ($strServiceTypeEtc: 결제사유, $strServiceType : 서비스 유형(radio:신규))
	Public function lead_customer_direct_input($xpay, $strServiceTypeEtc, $strServiceType)
	{
		$tmpProductType = "C";
		if( $xpay->Response("LGD_PRODUCTINFO",0) == "코로케이션" || $xpay->Response("LGD_PRODUCTINFO",0) == "기타" )
		{
			$tmpProductType = "C";
		}
		else if( $xpay->Response("LGD_PRODUCTINFO",0) == "서버호스팅" )
		{
			$tmpProductType = "S";
		}
		else if( $xpay->Response("LGD_PRODUCTINFO",0) == "K-Clean" ) //150514 lizzy
		{
			$tmpProductType = "K";
		}
		else
		{
			$tmpProductType = "C";
		}
		$data = array(
				"sOrderNumber" => $xpay->Response("LGD_OID",0), //작성번호
				"sCustomerName" => $xpay->Response("LGD_DELIVERYINFO",0) , // 업체명
				"sName" => $xpay->Response("LGD_BUYER",0), //신청인
				"sPhone" => $xpay->Response("LGD_BUYERPHONE",0), //전화
				"sEmail" => $xpay->Response("LGD_BUYEREMAIL",0), //이메일
				//"dtCreateDate" => date("Y-m-d H:i:s.u"), //작성일
				"sServiceType" => $tmpProductType, //서비스타입(C:코로케이션, S:서버호스팅)
		);
		$this->ismdb->insert($this->t_OlineApprovalCustomer,$data);//org

		$data = array(
				"sOrderNumber" => $xpay->Response("LGD_OID",0), //작성번호
				"sServiceName" => $strServiceTypeEtc."(".$xpay->Response("LGD_PRODUCTINFO",0)." ".$strServiceType.")" , // 업체명
				"nMonthPrice" => $xpay->Response("LGD_AMOUNT",0), //전화
		);
		$this->ismdb->insert($this->t_OlineApprovalDetail,$data);//org
	}


	public function payment02_finish($oid,$tid,$productType){

		//1. 이전데이타 확인(asp버전에 있으나 불필요하다고 판단되어 진행하지 않음. 따라서 $productType 안쓰임)
		//2. 결제처리데이터 확인
		 $sql = "";
		 $sql = "SELECT nXpayApprovalSeq, sOrderNumber, sUplusOrderNumber, sResultCode, sResultMessage, sPaymentType, nAmount, dtPayDate, sBuyerName ";
		 $sql .= " ,sBuyerSsn, sBuyerPhone, sBuyerAddress, sBuyerEmail, sBuyerId ";
		 $sql .= " ,sDeliveryInfo, nProductCode, sProductInfo, sCardNumber, sCardInstalmentMonth, sCardNointYn ";
		 $sql .= " ,nAccountNumber, nCastAmount, nCascAmount, sCasFlag, sAccountOwner, sPayer, sCasseqNumber ";
		 $sql .= " ,sCashReceiptNumber, sCashReceiptSelfYn, sCashReceiptKind ";
		 $sql .= " ,sFinanceCode, sFinanceName, sFinanceAuthNumber, sHashData ";
		 $sql .= " ,nTransAmount, nExchangeRate, sAffiliateCode";

		 if( $tid == '' )
		 {
		 	$sql .= " FROM ".$this->t_XpayApproval." WHERE sOrderNumber = ? order by nXpayApprovalSeq desc  limit 1 "; //org

		 	$query = $this->ismdb->query($sql, array($oid));
		 	$result = $query->result_array();
		 	return $result;
		 }
		 else
		 {
		 	$sql .= " FROM ".$this->t_XpayApproval." WHERE sOrderNumber = ? AND sUplusOrderNumber = ?  limit 1 "; //org

		 	$query = $this->ismdb->query($sql, array($oid, $tid));
		 	$result = $query->result_array();
		 	return $result;
		 }
	}


	public function payment02_casnote($LGD_TID,$LGD_CASFLAG){
		//가상계좌 처리 관련 (cf: manual/cas_noteurl.php , asp/lib/xpay_noteurl.asp 참고)
		$data = array('sCasFlag' => $LGD_CASFLAG);
		$this->ismdb->where('sUplusOrderNumber',$LGD_TID);
		$this->ismdb->update($this->t_XpayApproval,$data);//org
	}//end payment02_casnote

        /*
        public function payment02_casnote_api($LGD_TID,$LGD_CASFLAG){ //test 181203 lizzy
		//가상계좌 처리 관련 (cf: manual/cas_noteurl.php , asp/lib/xpay_noteurl.asp 참고)
		$data = array('sCasFlag' => $LGD_CASFLAG);
		$this->ismdb_test->where('sUplusOrderNumber',$LGD_TID);
		$this->ismdb_test->update($this->t_XpayApproval,$data);//org
	}//end payment02_casnote
        */


    private function _insert_change_info($onlytablename, $changdata, $currentdata, $seq)
    {
    	$emp_number = '00000000';
        $emp_name   = 'mykinx';
    	$table      = $this->t_log;

    	if(!is_null($changdata))
    	{
            foreach($changdata as $key=>$val)
            {
                $chginfo = array(
                    'sTable'        =>	$onlytablename,
                    'sField'        =>	$key,
                    'nSeq'			=>	$seq,
//                  'nEmployeeSeq'	=>	$emp_number,
                    'sEmployeeNo'	=>	$emp_number,
                    'sEmployeeName'	=>	$emp_name,
                    'sOldValue'		=>	$currentdata[$key],
                    'sNewValue'		=>	$val,
                    'sComment'      =>  'mykinx카드결제'
                );

                if(!$this->ismdb->insert($table, $chginfo)){return FALSE;}
            }
    	}

    	return TRUE;
	}

	//해당 BillMonth의 BillDetail의 상품을 구함
	public function get_billmonth_product($nBillMonthSeq)
	{
		$this->ismdb->select("p.sName, p.sEngName, bd.nProductSeq ");
		$this->ismdb->from("BILL.tBillDetail as bd");
		$this->ismdb->join("CONTRACT.tProduct as p","bd.nProductSeq=p.nProductSeq", "LEFT");
		$this->ismdb->where("bd.nBillMonthSeq",$nBillMonthSeq);

		$query = $this->ismdb->get();

		if($query && $query->num_rows())
			return $query->result_array();
		else
			return NULL;
	}
}

