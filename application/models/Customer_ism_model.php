<?php
class Customer_ism_model extends CI_Model { //Company_model
	protected $table = array("tCompany");
	
	
	private $t_customer     		= 'CONTRACT.tCustomer';
	
	function __construct(){
		//생성자
		parent::__construct();
	
			
		//ism 실DB 적용
		$ismdb = $this->load->database('ism',true);
		$this->ismdb = $ismdb;
	}
	 
	//select_row
	public function get_customer_bySeq($nSeq)
	{
		$query = $this->ismdb
									->select('A.*, A.sName as sCompanyName, A.sBusinessNo as sBusinessNumber, A.sOwnerNm as sPresidentName, ', FALSE)
									->from($this->t_customer." as A ")
									->where('nCustomerSeq',$nSeq)
									->limit(1)
									->get();
	
		if($query != FALSE)
		{
			if($query->num_rows() > 0)
				return $query->row_array();
				else
					$this->errMsg = "조회된 데이터가 없습니다.";
		}
		else
		{
			$error = $this->ismdb->error();
			$this->errMsg = $error['message'];
		}
	}
	
}