<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Support_noticelist_model extends CI_Model{
	
	public function get_noticelist()
	{
		$query = $this->db->get('tNoticeList');
	}
	
	public function _select_cnt($params=array()) {		
		return $this->db->count_all_results('tNoticeList');
	}
	
	public function _select_list($params=array(),$searchText=null, $searchType=1,&$result_cnt)//호출하는 곳 없음  
	{
		if( isset($searchText) && ($searchText != null ))//검색어 있을때 
		{	 
			/* org
			$sql = " SELECT nNoticeListSeq, sTitle, sContent, nCntRead, date_format(a.dtCreateDate, \"%Y-%m-%d\") as dtCreateDate ";
			$sql .= " FROM tNoticeList ";
			if( $searchType == 2 ){//내용
				$sql .= " where sContent like '%".$searchText."%' and sDisplayYn='Y'";
			}else{//제목
				$sql .= " where sTitle like '%".$searchText."%' and sDisplayYn='Y'";
			}
			*/
			///////////////temp sql
			$sql = " SELECT nNoticeListSeq, sTitle, nCntRead, date_format(dtCreateDate, \"%Y-%m-%d\") as dtCreateDate ";
			$sql .= " FROM tNoticeList ";
			if( $searchType == 2 ){//내용
				$sql .= " where sTitle like '%".$searchText."%'";
			}else{//제목
				$sql .= " where sTitle like '%".$searchText."%'";
			}
			///////////////temp sql
			
			$sql .= " order by dtCreateDate desc, nNoticeListSeq desc ";
			
			$query = $this->db->query($sql);
			$result = $query->result_array();
			$result_cnt = count($result);
			
			if(isset($params["limit"])){
				$sql .= " limit ".$params["limit"];
			}
			if(isset($params["offset"])){
				$sql .= " offset ".$params["offset"];
			}
			
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
		}
		else
		{ 
			//org
			/*
			$sql = "SELECT nNoticeListSeq, sTitle, sContent, nCntRead, date_format(dtCreateDate, \"%Y-%m-%d\") as dtCreateDate ";
			$sql .= " FROM tNoticeList ";
			$sql .= " WHERE sDisplayYn='Y' ";
			$sql .= " order by dtCreateDate desc, nNoticeListSeq desc ";
			*/
			///////////////temp sql
			$sql = "SELECT nNoticeListSeq, sTitle, nCntRead, date_format(dtCreateDate, \"%Y-%m-%d\") as dtCreateDate ";
			$sql .= "FROM tNoticeList ";
			$sql .= " order by dtCreateDate desc, nNoticeListSeq desc ";
			///////////////temp sql
			
			$query = $this->db->query($sql);
			$result = $query->result_array();
			$result_cnt = count($result);
			
			if(isset($params["limit"])){
				$sql .= " limit ".$params["limit"];
			}
			if(isset($params["offset"])){
				$sql .= " offset ".$params["offset"];
			}
			
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
		}
	}
	
	public function get_noticedata($nNoticeListSeq)//호출하는 곳 없음  
	{
		/* org
		$sql = " SELECT sTitle, sContent, nCntRead, date_format(dtCreateDate, '%Y-%m-%d') as dtCreateDate ";
		$sql .= " from tNoticeList ";
		$sql .= " where nNoticeListSeq='".$nNoticeListSeq."'";
		*/
		/////////////temp sql
		$sql = " Select sTitle, ";
		$sql .= " (Select sContent from tNoticeContent where nNoticeListSeq='".$nNoticeListSeq."') as sContent, ";
		$sql .= " nCntRead, ";
		$sql .= " date_format(dtCreateDate, '%Y-%m-%d') as dtCreateDate ";
		$sql .= " from tNoticeList ";
		$sql .= " where nNoticeListSeq='".$nNoticeListSeq."'";
		/////////////temp sql
		
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}
}
