<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Idc_estimate_model extends CI_Model
{	
	protected $testStatus = FALSE; //true 이면 테스트테이블 
	Public Function estimate_insert()
	{
		//start 140827 보안강화
		$postdata = $this->input->post();
		$data = array(
			"sOrderNumber" 		=> $postdata["strOfferOrder"], //작성번호
			"sCompanyName" 		=> $postdata["sCompanyName"],//업체명
			"sDepartment"		=> $postdata["sDepartment"],//부서
			"sName"				=> $postdata["sName"],//신청인
			"sRank"				=> $postdata["sRank"],//직함
			"sInternalPhone"	=> $postdata["sInternalPhone"],//일반전화
			"sMobilePhone"		=> $postdata["sMobilePhone"],//휴대전화
			"sEmail"			=> $postdata["sEmail"],//email
			"sOpenWishDate"		=> $postdata["sOpenWishDate"],//입주희망일
			//"nRackQuantity"		=> $postdata["nRackQuantity"],//상면1U갯수
			//"nQuarterRackQuantity"		=> $postdata["nQuarterRackQuantity"],//상면 1/4Rack
			"nHalfRackQuantity"			=> (isset($postdata["nHalfRackQuantity"]))?(int)$postdata["nHalfRackQuantity"]:0,//상면 Half Rack
			"nFullRackQuantity"			=> (isset($postdata["nFullRackQuantity"]))?(int)$postdata["nFullRackQuantity"]:0,//상면 Full Rack
			"sServerType"				=> (isset($postdata["sServerType"]))? $postdata["sServerType"]:'',//서버
			"nServerTypeQuantity"		=> (isset($postdata["nServerQuantity"]))?(int)$postdata["nServerQuantity"]:0,//서버 갯수
			"sBandwidth"				=> (isset($postdata["sBandwidth"]))?$postdata["sBandwidth"]:'',//네트워크
			"nBandwidthQuantity"		=> (isset($postdata["nBandwidthQuantity"]))?(int)$postdata["nBandwidthQuantity"]:0,//네트워크 갯수
			"sTotalManage"				=> (isset($postdata["sTotalManage"]))? $postdata["sTotalManage"]:'',//통합운영관리
			"nTotalManageQuantity"		=> (isset($postdata["nTotalManageQuantity"]))?(int)$postdata["nTotalManageQuantity"]:0,//통합운영관리 갯수
			"sDDos"						=> (isset($postdata["sDDos"]))?$postdata["sDDos"]:'',//ddos
			"nDDosQuantity"				=> (isset($postdata["nDDosQuantity"]))?(int)$postdata["nDDosQuantity"]:0,//ddos 갯수
			"sSecure"					=> (isset($postdata["sSecure"]))? $postdata["sSecure"]:'',//일반보안
			"nSecureQuantity"			=> (isset($postdata["nSecureQuantity"]))?(int)$postdata["nSecureQuantity"]:0,//일반보안갯수
			"sEem7"						=> (isset($postdata["sEem7"]))?$postdata["sEem7"]:'',//app - solution
			"nEm7Quantity"				=> (isset($postdata["nEm7Quantity"]))?(int)$postdata["nEm7Quantity"]:0,//app - solution갯수
			"sAspOs"					=> (isset($postdata["sAspOs"]))?$postdata["sAspOs"]:'',//sw-임대 -os
			"nAspOsQuantity"			=> (isset($postdata["nAspOsQuantity"]))?(int)$postdata["nAspOsQuantity"]:0,//sw-임대 -os갯수
			"sAspBb"					=> (isset($postdata["sAspBb"]))? $postdata["sAspBb"]:'',//sw-임대 -db
			"nAspBbQuantity"			=> (isset($postdata["nAspBbQuantity"]))?(int)$postdata["nAspBbQuantity"]:0,//sw-임대 -db갯수
			"sBackup"					=> (isset($postdata["sBackup"]))?$postdata["sBackup"]:'',//백업
			"nBackupQuantity"			=> (isset($postdata["nBackupQuantity"]))?(int)$postdata["nBackupQuantity"]:0,//백업갯수
			"sContent"					=> (isset($postdata["sContent"]))?$postdata["sContent"]:'',//기타문의
			"sEstimationType"			=> $postdata["sEstimationType"],//150907, 견적서 Type / H:서버호스팅,C:colocation
		);
		//-end
		
		//print_r ($data);
		if( $this->testStatus == TRUE ){
			$this->db->insert('tEstimation_test',$data);//test
		}else{
			$this->db->insert('tEstimation',$data);//org
		} 
	}//end estimate_insert
	Public Function estimate_get($sOrderNumber){
		$sql = "";
		$sql = "SELECT sOrderNumber, sCompanyName, sDepartment, sName, sRank, sInternalPhone, sMobilePhone, sEmail, sOpenWishDate ";
		//$sql .= " ,nRackQuantity, nQuarterRackQuantity, nHalfRackQuantity, nFullRackQuantity, sBandwidth, nBandwidthQuantity ";
		$sql .= " , nHalfRackQuantity, nFullRackQuantity, sBandwidth, nBandwidthQuantity ";//150907
		$sql .= " , sServerType, nServerTypeQuantity ";//150908
		$sql .= " ,sTotalManage, nTotalManageQuantity, sDDos, nDDosQuantity, sSecure, nSecureQuantity ";
		$sql .= " ,sEem7, nEm7Quantity, sAspOs, nAspOsQuantity, sAspBb, nAspBbQuantity ";
		$sql .= " ,sBackup, nBackupQuantity, sContent ";
		///
		$sql .= ",  (SELECT sCodeName From tSysCode WHERE sUseTable='tEstimation' and sGroupNameEng='sBandwidth' and sCode=t.sBandwidth) AS sBandwidthStr ";
		$sql .= ",  (SELECT sCodeName From tSysCode WHERE sUseTable='tEstimation' and sGroupNameEng='sTotalManage' and sCode=t.sTotalManage) AS sTotalManageStr ";
		$sql .= ",  (SELECT sCodeName From tSysCode WHERE sUseTable='tEstimation' and sGroupNameEng='sDDos' and sCode=t.sDDos) AS sDDosStr ";
		$sql .= ",  (SELECT sCodeName From tSysCode WHERE sUseTable='tEstimation' and sGroupNameEng='sSecure' and sCode=t.sSecure) AS sSecureStr ";
		$sql .= ",  (SELECT sCodeName From tSysCode WHERE sUseTable='tEstimation' and sGroupNameEng='sEem7' and sCode=t.sEem7) AS sEem7Str ";
		$sql .= ",  (SELECT sCodeName From tSysCode WHERE sUseTable='tEstimation' and sGroupNameEng='sAspOs' and sCode=t.sAspOs) AS sAspOsStr ";
		$sql .= ",  (SELECT sCodeName From tSysCode WHERE sUseTable='tEstimation' and sGroupNameEng='sAspBb' and sCode=t.sAspBb) AS sAspBbStr ";
		$sql .= ",  (SELECT sCodeName From tSysCode WHERE sUseTable='tEstimation' and sGroupNameEng='sBackup' and sCode=t.sBackup) AS sBackupStr ";
		$sql .= ",  (SELECT sCodeName From tSysCode WHERE sUseTable='tEstimation' and sGroupNameEng='sServerType' and sCode=t.sServerType) AS sServerStr ";
		///
			
		if( $this->testStatus == TRUE ){
			$sql .= " FROM tEstimation_test WHERE sOrderNumber = ? limit 1 "; //test
		}else{
			$sql .= " FROM tEstimation AS t WHERE sOrderNumber = ? limit 1 "; //org
		}
	
		$query = $this->db->query($sql, array($sOrderNumber));
		$result = $query->result_array();
		return $result;
	}//end estimate_get
}
