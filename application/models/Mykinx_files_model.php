<?php
class Mykinx_files_model extends CI_Model {
	private $t_files		= 'tFilesInfo';
	private $t_userinfoinquiry = 'tUserinfoInquiry';
	function __construct(){
		//생성자
		parent::__construct();
	
		
		$CI =& get_instance(); //CodeIgniter에 내장된 리소스 할당
		$this->prefix		= $CI->config->item('db_table_prefix', 'kinxconfig');
		$this->t_files		= $this->prefix.$this->t_files;
		$this->t_userinfoinquiry = $this->prefix.$this->t_userinfoinquiry;
	}
	

	/**
	 * 첨부파일정보를 추출한다.
	 *
	 * @param integer $seq
	 * @return NULL/array
	 */
	public  function get_fileinfo_by_seq($seq)
	{
		if(is_null($seq) || empty($seq)) $this->errorMsg = "기본데이터가 넘어오지 않았습니다.";
		else
		{
			try
			{
				$query = $this->db->where('nFileSeq', $seq)->get($this->t_files);
				if ($query->num_rows() == 1) return $query->row_array();
			}
			catch (Exception $e)
			{
				$this->errorMsg= $e->getMessage();
			}
		}
		return NULL;
	}
	 

	/**
	 * 사용자정보조회 이력생성 by lizzy
	 *
	 * ISMS 로그) 개인정보조회, 신분증 view or 다운로드 로그 남기기 by lizzy 20161206
	 * sType : file => 파일다운로드/view , view => 개인정보조회(ex: 고객현황 detail)
	 * sEvent : inquiry 페이지 => file인 경우는 intranetfiles 경로 밑의 폴더명으로 알 수 있음
	 *                                        view인 경우는 segment1/segment2 값을 저장하여 어떤 페이지인지 파악
	 * memberseq : intraold 에서 파일을 보기 위해 클릭한 임직원의 seq
	 * @param array $data
	 */
	public function setUserinfoInquiry($sType, $sEvent, $nDetailSeq, $memberseq)
	{
		//1. 값 설정. 로그인 사용자 정보 -> session에서 구하기
		$data = array(
				'nMemberSeq'	=> $memberseq,
				'sType'		=> $sType,
				'sEvent'	=> $sEvent,
				'nDetailSeq' =>$nDetailSeq
		);
		//va($data);  ;
		$this->db->insert($this->t_userinfoinquiry, $data);
		 
	}
}