<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Idc_serverhosting_model extends CI_Model{
	
	
	protected $table = array("tServer", "tServerOption");
	
	public function server_list_view($nServerSeq)
	{	 
		$sql = "SELECT sStatus, sBrandName, sServerName, nCpuSeq, nRamSeq, nHddSeq, nCntCpu, nCntRam, nCntHdd, ";
		$sql .= "nPrice, nMonthPrice, nRentalPrice, nRentalTerm, nProvideTraffic, sContent, dtCreateDate, ";
		$sql .= "sMainImage, sSubImage, sIcon1Image, sIcon2Image, sProcessorType, sProcessorLimit, sProcessorCache, ";
		$sql .= "sSystemBus, sMemoryType, sMemoryLimit, sDiskType, sDiskOption, ";
		$sql .= "sNetwork, sVideoType, sCdRom, sInterfaceVideo, sInterfaceUsb, sInterfaceMouse, sPower, ";
		$sql .= "(SELECT tServerOption.sComponentFullName FROM tServerOption WHERE tServerOption.nServerOptionSeq = A.nCpuSeq) as sCpuName,  ";
		$sql .= "(SELECT tServerOption.sComponentFullName FROM tServerOption WHERE tServerOption.nServerOptionSeq = A.nRamSeq) as sRamName,  ";
		$sql .= "(SELECT tServerOption.sComponentFullName FROM tServerOption WHERE tServerOption.nServerOptionSeq = A.nHddSeq) as sHddName  ";
		$sql .= "FROM ";
		$sql .= "tServer as A ";
		/*
		$sql .= "where nServerSeq = '" . $nServerSeq . "'";
		$query = $this->db->query($sql);
		*/
		$sql .= "where nServerSeq = ? ";
		$query = $this->db->query($sql, $nServerSeq);	//140827 보안강화	
		//echo $this->db->last_query();
		$result = $query->result_array();
		return $result;
	}
	
	//140827 보안강화. 
	public function server_list($sBrandName)
	{
		/*
		$where = "where ";
		if(isset($sBrandName)){
				
			if( $sBrandName != "ALL"){
				$cond = "A.sBrandName = '" . $sBrandName . "'";
				$where .= $cond;
				$where .= " and ";
			}
		}
		$where .= " A.sStatus = 'Y' OR A.sStatus = 'E'";
		*/
		
		$this->db->select('nServerSeq, sStatus, sBrandName, sServerName, nCpuSeq, nRamSeq, nHddSeq');
		/*
		$this->db->select('(SELECT tServerOption.sComponentName FROM tServerOption WHERE tServerOption.nServerOptionSeq = A.nCpuSeq) as sCpuName');
		$this->db->select('(SELECT tServerOption.sComponentName FROM tServerOption WHERE tServerOption.nServerOptionSeq = A.nRamSeq) as sRamName');
		$this->db->select('(SELECT tServerOption.sComponentName FROM tServerOption WHERE tServerOption.nServerOptionSeq = A.nHddSeq) as sHddName');
		*/
		//축약된 이름이 아닌 fullname으로 교체
		$this->db->select('(SELECT tServerOption.sComponentFullName FROM tServerOption WHERE tServerOption.nServerOptionSeq = A.nCpuSeq) as sCpuName');
		$this->db->select('(SELECT tServerOption.sComponentFullName FROM tServerOption WHERE tServerOption.nServerOptionSeq = A.nRamSeq) as sRamName');
		$this->db->select('(SELECT tServerOption.sComponentFullName FROM tServerOption WHERE tServerOption.nServerOptionSeq = A.nHddSeq) as sHddName');
		$this->db->select('nPrice, nMonthPrice, nRentalPrice, sMainImage, sSubImage, sIcon1Image, sIcon2Image');
		$this->db->from('tServer as A');
		if(isset($sBrandName)){
			if( $sBrandName != "ALL"){
				$this->db->where('A.sBrandName',$sBrandName);
			}
		}
		$this->db->where('A.sStatus','Y');
		$this->db->or_where('A.sStatus','E');
		//$this->db->order_by('sBrandName','DESC');//150911 조건 제외
		$this->db->order_by('nPopularSort','ASC');
		//$this->db->order_by('sServerName','ASC');//150911 조건제외
		$query = $this->db->get();
		//echo $this->db->last_query();
		$result = $query->result_array();
		return $result;		
	}
	
	private function _query($params) {
		$this->db->from($this->table[0]." as A");
		//$this->db->join($this->table[1]." as B", "B.sComponentName = A.nCpuSeq");
		//$this->db->join($this->table[1]." as B", "B.sComponentName = A.nRamSeq");
		//$this->db->join($this->table[1]." as B", "B.sComponentName = A.nHddSeq");
		if(!is_null($params["sBrandName"])) {
			$where["A.sBrandName"] = $params["sBrandName"];
		}
		
		if(isset($params["select"])) {			
			$this->db->select($params["select"]);
			$this->db->select("(SELECT tServerOption.sComponentName FROM tServerOption WHERE tServerOption.nServerOptionSeq = A.nCpuSeq) as sCpuName,  ");
		}
		//---------------------------------
//		$secParams["select"] .= "(SELECT tServerOption.sComponentName FROM tServerOption WHERE tServerOption.nServerOptionSeq = A.nCpuSeq) as sCpuName,  ";
//		$secParams["select"] .= "(SELECT tServerOption.sComponentName FROM tServerOption WHERE tServerOption.nServerOptionSeq = A.nRamSeq) as sRamName,  ";
//		$secParams["select"] .= "(SELECT tServerOption.sComponentName FROM tServerOption WHERE tServerOption.nServerOptionSeq = A.nHddSeq) as sHddName,  ";
		
		/*
		$this->db->select($this->table[1]." sComponentName");
		$this->db->from($this->table[1]);
		$sub = $this->subquery->start_subquery("join","left", "tServerOption.nServerOptionSeq = tServer.nCpuSeq");
		
		
		$sub = $this->subquery->_start_subquery("select");
		$sub->select("sComponentName")->from($this->table[1]." as B")->where($this->table[1].nServerOptionSeq, $this->table[0].nCpuSeq);
		$this->subquery->_end_subquery("$this->table[1]");
		*/
		//---------------------------------
		if(isset($params["wherein"])) {
			$this->db->where_in("A.sStatus", $params["wherein"]);
		}
		
		$where["1"] = "1";
		return $where;
	}
	
	public function select_list($params=array()) {
		$where = $this->_query($params);
		
		return $this->db->get_where(null, $where)->result_array();
	}
	
	
	public function select_cpuname($nItemSeq) {
		$this->db->from($this->table[1]." as B");
		$this->db->select("sComponentName");
		$this->db->where("nServerOptionSeq", $nItemSeq);
	
		return $this->db->get_where(null)->result_array();
	}
}
