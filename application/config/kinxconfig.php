<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| password 암호화 키
|--------------------------------------------------------------------------
| 패스워드 암축시 사용되는 암호화 키
|
*/
$config['salt']="2013kinxintranet!@abcdefgHIJKLNMopqrstuVWXYZ#$";

/*
|--------------------------------------------------------------------------
| ixcloud api 세팅
|--------------------------------------------------------------------------
| 'db_table_prefix'= 테이블 접두사
| ( 'ci_sessions'테이블 제외).
|
*/
if(ENVIRONMENT=='development')
{
    $config['host_api_grizzly']     = 'http://api.ixcloud.net';
    $config['host_api_juno']        = 'http://1.201.137.199:8424';
    $config['kinx_api_host']        = 'http://apidev.kinx.net';
    $config['ism_api_host']         = 'http://lizzy337_ismapi.kinx.net/'; //https
    $config['xapi_host']            = 'https://stage-xapi.kinx.net/';
    $config['db_table_prefix']      = 'kinx_db_11.';
    $config['mrtg_db_table_prefix'] = 'kinx_db_11.';// 'kinxdb_intra.';//org
    $config['toss_host']            = 'https://api.tosspayments.com/';//# toss API
    $config['kmms_api_host']        = 'https://kmms.kinx.net/';
}
else
{
    $config['host_api_grizzly']     = 'http://api.ixcloud.net';
    $config['host_api_juno']        = 'http://junoapi.ixcloud.net';
    $config['kinx_api_host']        = 'http://api.kinx.net';
    $config['ism_api_host']         = 'https://ismapi.kinx.net/';
    $config['xapi_host']            = 'https://xapi.kinx.net/';
    $config['db_table_prefix']      = 'kinxdb_intra.';
    $config['mrtg_db_table_prefix'] = 'kinxdb.';
    $config['toss_host']            = 'https://api.tosspayments.com/';//# toss API
    $config['kmms_api_host']        = 'https://kmms.kinx.net/';
}




/* End of file kinxconfig.php */
/* Location: ./intranetapp/config/kinxconfig.php */
