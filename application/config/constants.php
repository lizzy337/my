<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.2.4 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Academic Free License version 3.0
 *
 * This source file is subject to the Academic Free License (AFL 3.0) that is
 * bundled with this package in the files license_afl.txt / license_afl.rst.
 * It is also available through the world wide web at this URL:
 * http://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to obtain it
 * through the world wide web, please send an email to
 * licensing@ellislab.com so we can send you a copy immediately.
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2013, EllisLab, Inc. (http://ellislab.com/)
 * @license		http://opensource.org/licenses/AFL-3.0 Academic Free License (AFL 3.0)
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE',                      0644);
define('FILE_WRITE_MODE',                     0666);
define('DIR_READ_MODE',                       0755);
define('DIR_WRITE_MODE',                      0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',                          'rb');
define('FOPEN_READ_WRITE',                    'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',      'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',                  'ab');
define('FOPEN_READ_WRITE_CREATE',             'a+b');
define('FOPEN_WRITE_CREATE_STRICT',           'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',      'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
if(ENVIRONMENT== 'development') {
    define('SHOW_DEBUG_BACKTRACE',          TRUE);
}
else {
    define('SHOW_DEBUG_BACKTRACE',          FALSE);
}

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS',                 0); // no errors
define('EXIT_ERROR',                   1); // generic error
define('EXIT_CONFIG',                  3); // configuration error
define('EXIT_UNKNOWN_FILE',            4); // file not found
define('EXIT_UNKNOWN_CLASS',           5); // unknown class
define('EXIT_UNKNOWN_METHOD',          6); // unknown class member
define('EXIT_USER_INPUT',              7); // invalid user input
define('EXIT_DATABASE',                8); // database error
define('EXIT__AUTO_MIN',               9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX',               125); // highest automatically-assigned error code



/*
|--------------------------------------------------------------------------
| Payment mail const Codes
|--------------------------------------------------------------------------

*/

if(ENVIRONMENT== 'development')
{
    define('MAIL_FROM_INFO',            'lizzy337@kinx.net');
    define('MAIL_TO_PAYMENT',           'yjcin13@kinx.net');
    define('MAIL_TO_ESTIMATE',          'lizzy337@kinx.net');
    define('MAIL_TO_DEFAULT',           'lizzy337@kinx.net');
    define('MAIL_WORK_REQUEST',         'lizzy337@kinx.net');
    define('MAIL_FIND_INFO',            'lizzy337@kinx.net');
    define('MAIL_TO_PEERINGFORUM',      'lizzy337@kinx.net');
    define('EMAIL_SENDMAIL_NAME',       'KINX');
    define('EMAIL_HELP',	            'lizzy337@kinx.net');   //mail form 개선 20170103
    define('MAIL_WORK_REQUEST_BD',      'lizzy337bd@kinx.net'); //분당 외주업체 대상 그룹메일
    define('MAIL_SERVICE_MNG_GROUP',    'lizzy337service@kinx.net');//서비스 운영 그룹
}
else{
    define('MAIL_FROM_INFO',            'info@kinx.net');
    define('MAIL_TO_PAYMENT',           'bizm@kinx.net');//온라인결제, 입금확인 담당자. 161005 shryu -> bizm 으로 수정
    define('MAIL_TO_ESTIMATE',          'sales@kinx.net');//'conbiz@kinx.net');//코로케이션 견적담당자
    define('MAIL_TO_DEFAULT',           'sales@kinx.net');//
    define('MAIL_WORK_REQUEST',         'noc@kinx.net');
    define('MAIL_FIND_INFO',            'id@kinx.net');//id, pw 찾는 경우
    define('MAIL_TO_PEERINGFORUM',      'mjjang@kinx.net');
    define('EMAIL_SENDMAIL_NAME',       'KINX');
    define('EMAIL_HELP',	            'help@kinx.net');   //mail form 개선 20170103
    define('MAIL_WORK_REQUEST_BD',      'bdnoc@kinx.net'); //분당 외주업체 대상 그룹메일
    define('MAIL_SERVICE_MNG_GROUP',    'system@kinx.net');//서비스 운영 그룹
}

define('EMAIL_SENDBASE_URL',            'http://www.kinx.net/'); //160829 https -> http



/*
|--------------------------------------------------------------------------
| Payment doc const Codes
|--------------------------------------------------------------------------
*/
define('CMS_REQUEST_FILE',              'KINX_CMS자동이체 신청서.docx');
define('IDC_AWS_REQ_10DOWNFILE',        'AWS 다이렉트 커넥트_신청절차_01.pdf');
define('IDC_AWS_REQ_1DOWNFILE',         'AWS 다이렉트 커넥트_신청절차_02.pdf');
define("CST_MID",                       'kinx01'); //'상점아이디(LG데이콤으로 부터 발급받으신 상점아이디를 입력하세요)
define('WWW_DOMAIN_NAME',	            'www.kinx.net');

if(ENVIRONMENT== 'development')
{
    define("CST_PLATFORM",              'test'); //"service" 'LG데이콤 결제 서비스 선택(test:테스트, service:서비스)
    define("LGD_MID",                   't'.CST_MID);//상점아이디(자동생성)
    define('MY_DOMAIN_NAME',            'lizzy-mydev-mot.kinx.net');
    define('CASNOTEURL_NAME',           'my.kinx.net'); //KINX.NET 개편. 160829
    define('UPLUS_JS_URL',              'https://pretest.uplus.co.kr:9443/xpay/js/xpay_ub_utf-8.js');//test 20200207
    define('LGD_CUSTOM_USABLEPAY',      'SC0010');//신용카드만 사용211019
}
else
{
    define("CST_PLATFORM",              'service'); //"service" 'LG데이콤 결제 서비스 선택(test:테스트, service:서비스)
    define("LGD_MID",                   CST_MID);
    define('MY_DOMAIN_NAME',            'my.kinx.net');
    define('CASNOTEURL_NAME',           'my.kinx.net'); //KINX.NET 개편. 160829
    define('UPLUS_JS_URL',              'https://xpay.uplus.co.kr/xpay/js/xpay_ub_utf-8.js');//:7443 제거 20200207
    define('LGD_CUSTOM_USABLEPAY',      'SC0010');//신용카드만 사용211019
}




define("LGD_MERTKEY",                   "e8258b350a622cbc5b267af3c7a3a34c"); //[반드시 세팅]상점MertKey(mertkey는 상점관리자 -> 계약정보 -> 상점정보관리에서 확인하실수 있습니다')
define("LGD_CUSTOM_SKIN",               "red"); //상점정의 결제창 스킨 (red, blue, cyan, green, yellow)
define("LGD_CUSTOM_PROCESSTIMEOUT",     "600");//인증후 승인요청까지 가능 허용 시간(초단위), 디폴트는 10min
define("LGD_CUSTOM_BUSINESSNUM",        "101-81-59273");//상점사업자등록번호
define("LGD_CUSTOM_PROCESSTYPE ",       "TWOTR");


//가상계좌(무통장) 결제 연동을 하시는 경우 아래 LGD_CASNOTEURL 을 설정하여 주시기 바랍니다.
//LGD_CASNOTEURL                  = "http://" . FIX_WWW_DOMAIN_NAME . "/lib/xpay_noteurl.asp";
//MYKINX 개편으로 인해.
//define("LGD_CASNOTEURL","http://".WWW_DOMAIN_NAME."/support/payment/payment02_casnoteurl");//기술지원-결제진행시
//define("LGD_CASNOTEURL","http://".CASNOTEURL_NAME."/support/payment/payment02_casnoteurl");//기술지원-결제진행시. 160829


define("LGD_CASNOTEURL",                "https://".CASNOTEURL_NAME."/support/payment/payment02_casnoteurl");//기술지원-결제진행시. 181203 http -> https org
//define("LGD_CASNOTEURL_KCLEAN","https://".WWW_DOMAIN_NAME."/support/payment/payment02_casnoteurl");//kclean 결제 진행시


/*
|--------------------------------------------------------------------------
| recruit doc const Codes
|--------------------------------------------------------------------------

*/
define('RECRUIT_FILE',                 'KINX_입사지원서양식.doc');
//define('RECRUIT_FILE', 'KINX_Recruit.doc');

define('FIND_IDPWREQ_FILE_PRV',        'KINX_아이디,비밀번호찾기 양식_개인용.docx');//아이디, 비밀번호 찾기 신청서. 개인
define('FIND_IDPWREQ_FILE_COM',        'KINX_아이디,비밀번호찾기 양식_단체용.docx');//아이디, 비밀번호 찾기 신청서. 개인




/*
|--------------------------------------------------------------------------
| find account(id, pw) add file path
|--------------------------------------------------------------------------
*/
if(ENVIRONMENT== 'development')
{
    //local test
    //define('UPLOAD_TYPE_AUTH', '../../kinxintranet/intranetfiles/auth/'); // ID,PW 신청관련 서류 폴더
    //define('UPLOAD_TYPE_TMP', '../../kinxintranet/intranetfiles/tmp/');

    $ret = file_exists('../../kinxintranet/intranetfiles/auth/');
    if($ret === true)//기존 물리서버
    {
        define('UPLOAD_TYPE_AUTH',    '../../kinxintranet/intranetfiles/auth/'); // ID,PW 신청관련 서류 폴더
        define('UPLOAD_TYPE_TMP',     '../../kinxintranet/intranetfiles/tmp/');
    }
    else //mot
    {
        define('UPLOAD_TYPE_AUTH',    '../intranetfiles/auth/'); // ID,PW 신청관련 서류 폴더
        define('UPLOAD_TYPE_TMP',     '../intranetfiles/tmp/');
    }

}
else{
    //org
    //define('UPLOAD_TYPE_AUTH',      '/data/projects/kinx.net/intranet/intranetfiles/auth/');    // ID,PW 신청관련 서류 폴더
    //define('UPLOAD_TYPE_TMP',       '/data/projects/kinx.net/intranet/intranetfiles/tmp/');           //

    $ret = file_exists('/data/projects/kinx.net/intranet/intranetfiles/auth/');
    if($ret === true)//기존 물리서버
    {
        define('UPLOAD_TYPE_AUTH',    '/data/projects/kinx.net/intranet/intranetfiles/auth/');    // ID,PW 신청관련 서류 폴더
        define('UPLOAD_TYPE_TMP',     '/data/projects/kinx.net/intranet/intranetfiles/tmp/');           //
    }
    else //mot
    {
        define('UPLOAD_TYPE_AUTH',    '../intranetfiles/auth/');    // ID,PW 신청관련 서류 폴더
        define('UPLOAD_TYPE_TMP',     '../intranetfiles/tmp/');           //
    }
}

/*kclean brouchure filename*/
define('KCLEAN_BROUCHURE', 'kclean_brochure.pdf');//kclean brouchure

/*kclean request*/
define('KCLEAN_LGD_PRODUCTINFO','K-Clean');// views/support_payment_payment02_req.html, views/kclean/request/request_step2.html

define('INTRANET_HOMEPAGE','intra.kinx.net'); //pokhara 오픈으로 pohkara가 intra.kinx.net 으로.  기존 intra.kinx.net -> intraold로 변경됨.

//xapi key 설정
defined('EMPAPI_KEY')         OR define('EMPAPI_KEY', 'W8Dke2VfpG9nTWjFmq4Fy8SOQd4hLLRJLjTc6a8x');
/* End of file constants.php */
/* Location: ./application/config/constants.php */





/*
|--------------------------------------------------------------------------
| NICE 본인인증
|--------------------------------------------------------------------------
*/
defined('NICE_SITECODE') OR define('NICE_SITECODE', 'H0027');
defined('NICE_SITEPASSWORD') OR define('NICE_SITEPASSWORD', 'RJXQKU1PPG6P');
defined('NICE_REQKEY') OR define('NICE_REQKEY', 'kinxnicecheckplus');
if(ENVIRONMENT == 'development')
{
	defined('NICE_ENCODEPATH') OR define('NICE_ENCODEPATH', '/kinx/projects/lizzy337/my_mot/application/third_party/CPClient');
	defined('NICE_RETURNURL')  OR define('NICE_RETURNURL',  'http://'.MY_DOMAIN_NAME.'/auth/checkPlusSuccess');
	defined('NICE_ERRORURL')   OR define('NICE_ERRORURL',   'http://'.MY_DOMAIN_NAME.'/auth/checkPlusFailed');
}
else
{
	defined('NICE_ENCODEPATH') OR define('NICE_ENCODEPATH', '/kinx/projects/kinx.net/my/application/third_party/CPClient');
	defined('NICE_RETURNURL')  OR define('NICE_RETURNURL',  'https://'.MY_DOMAIN_NAME.'/auth/checkPlusSuccess');
	defined('NICE_ERRORURL')   OR define('NICE_ERRORURL',   'https://'.MY_DOMAIN_NAME.'/auth/checkPlusFailed');
}



/*
|--------------------------------------------------------------------------
| toss client key
|--------------------------------------------------------------------------
| https://onboarding.tosspayments.com/4524/integration 에 통해서 client key 발급
| 로그인 요청은 클라우드개발Group 장경수M, 업무자동화Unit 신용주M에 문의
|
| myKINX는 kinx01 아이디 사용
|
| key 보호를 위해 암호화 사용 - 아래는 암호화된 key 정보
|   CryptoJS.AES 방식으로 암호화 처리
|   https://github.com/brix/crypto-js 참조
|   var 암호화키 = CryptoJS.AES.encrypt('Client Key', 'salt string').toString();
|
*/
if(ENVIRONMENT == 'development')
{
    defined('TOSS_01_CLIENT')    OR define('TOSS_01_CLIENT', 'U2FsdGVkX193tbN2MtnK3da/lSnfOCA7RgTNuMDBc5FZgrQzIR4cwh8/Lu4Oy9ZNXVaehEQKn9G4xKMCI9xA+w==');
    defined('TOSS_SECURITY_KEY') OR define('TOSS_SECURITY_KEY', 'test_sk_7DLJOpm5QrlJAvWjgGQ3PNdxbWnY');
}
else
{
    defined('TOSS_01_CLIENT')    OR define('TOSS_01_CLIENT', 'U2FsdGVkX1/IIEmw6+La4dKJ6dcqFbx571R1dnDEARX983r/cGSuDrZ7zNxZJ3w1089daR+oiNjSAUZpvBM51g==');
    defined('TOSS_SECURITY_KEY') OR define('TOSS_SECURITY_KEY', 'live_sk_YZ1aOwX7K8m6ANkzmDP3yQxzvNPG');
}

defined('TOSS_SALT_STR') OR define('TOSS_SALT_STR', 'U2FsdGVkX19+lppvGStKLoTo9ohqCQN9OZ0oRi72+chUWN4mVuJLm4xjdkf6Homa');// 임의로 설정한 암호화 salt


//Security Deposit 상품번호
defined('PRODUCT_SECURITY_DEPOSIT') OR define('PRODUCT_SECURITY_DEPOSIT','285');
//예치금(선수수익용) 상품번호
defined('PRODUCT_DEPOSIT') OR define('PRODUCT_DEPOSIT','297');
