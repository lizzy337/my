<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.2.4 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Academic Free License version 3.0
 *
 * This source file is subject to the Academic Free License (AFL 3.0) that is
 * bundled with this package in the files license_afl.txt / license_afl.rst.
 * It is also available through the world wide web at this URL:
 * http://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to obtain it
 * through the world wide web, please send an email to
 * licensing@ellislab.com so we can send you a copy immediately.
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2013, EllisLab, Inc. (http://ellislab.com/)
 * @license		http://opensource.org/licenses/AFL-3.0 Academic Free License (AFL 3.0)
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']                 = 'main';
$route['404_override']                       = 'errors/error404';
$route['translate_uri_dashes']               = TRUE;


/*-----------------------------------------------------------------------------------------------
| common
-------------------------------------------------------------------------------------------------*/
$route['preview/(:any)/(:any)']              = "common/preview";


/*-----------------------------------------------------------------------------------------------
| payment03 doc download
-------------------------------------------------------------------------------------------------*/
$route['payment_doc_down']                   = 'support/payment/payment03_doc_down';
$route['recruit_doc_down']                   = 'company/Recruit/notice_list_info_doc_down';


/*-----------------------------------------------------------------------------------------------
| idpw req doc download
-----------------------------------------------------------------------------------------------*/
$route['idpwreq_priv_doc_down']              = '/auth/find_idpwreq_priv_doc_down';
$route['idpwreq_comp_doc_down']              = '/auth/find_idpwreq_comp_doc_down';


/*-----------------------------------------------------------------------------------------------
 | ETC
 -----------------------------------------------------------------------------------------------*/
$route['mykinx(.*)'] = 'mykinx';

$route['down/print/detail_account.asp?(.*)']   = 'utility/bill/get_billaccount$1';
$route['down/print/detail_bill.php?(.*)']      = 'utility/bill/get_billtax$1'; //복호화 추가하여 새로 작성된 함수. 170207

$route['mypage/auth/atlogin/(:any)']           = 'mypage/auth/atlogin';


/*-----------------------------------------------------------------------------------------------
 | 피어링포럼 다운로드
 -----------------------------------------------------------------------------------------------*/
$route['peeringforum_down/(:any)']             = 'peeringforum2015/main/peeringforum_down';


/*-----------------------------------------------------------------------------------------------
 | kclean_brochure 다운로드 링크
 -----------------------------------------------------------------------------------------------*/
$route['kclean_brochure']                      = 'kclean/Main/down_kclean_brochure';
$route['kclean/request/request_step3']['get']  = 'kclean/request/request_step3/$1';




/*-----------------------------------------------------------------------------------------------
 | 2015 대학교 채용공지 brouchure 다운로드 링크
 -----------------------------------------------------------------------------------------------*/
$route['2015recruit']                          = 'company/Recruit/down_2015recruit_brochure';


/*-----------------------------------------------------------------------------------------------
 | idc aws 신청절차 다운로드
 -----------------------------------------------------------------------------------------------*/
$route['idc_aws_req_10down']                   = 'idc/aws/idc_aws_req_10down';
$route['idc_aws_req_1down']                    = 'idc/aws/idc_aws_req_1down';


/*-----------------------------------------------------------------------------------------------
 | datacenter
 -----------------------------------------------------------------------------------------------*/
$route['IDC/(:any)']['get']                    = 'request/request_get/$1';
$route['IDC/(:any)']['post']                   = 'request/request_post/$1';


/*-----------------------------------------------------------------------------------------------
 | usage
 -----------------------------------------------------------------------------------------------*/
 $route['usage/detail/(:any)']['get']          = 'usage/detail/$1';
 $route['usage/detail/(:any)']['post']         = 'usage/usage_graph_data/$1';

 /*-----------------------------------------------------------------------------------------------
 | verification
 -----------------------------------------------------------------------------------------------*/
 $route['verification/(:any)']                 = 'verification/index/$1';
 $route['verification/proc']['post']           = 'verification/proc';



/*-----------------------------------------------------------------------------------------------
 | support/payment
 -----------------------------------------------------------------------------------------------*/
 $route['support/payment/payment02']['get']        = 'support/payment/payment02';
 $route['support/payment/payment02/(:any)']['get'] = 'support/payment/payment02/$1';



 /*-----------------------------------------------------------------------------------------------
 | tosspayment
 -----------------------------------------------------------------------------------------------*/
 $route['tosspayment/card']['post']                = 'tosspayment/card_post';


/*-----------------------------------------------------------------------------------------------
 | payment
 -----------------------------------------------------------------------------------------------*/
 $route['payment/detail_new/(:any)']['get']        = 'payment/detail_new/$1';
 $route['payment/detail_new/(:any)/(:any)']['get'] = 'payment/detail_new/$1/$2';



/*-----------------------------------------------------------------------------------------------
 | 입금계좌 event
 -----------------------------------------------------------------------------------------------*/
 $route['event/(:any)']       = 'event/index/$1';
 $route['event/proc']       = 'event/proc';