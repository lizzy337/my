<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| Validation Rules
| -------------------------------------------------------------------
|
*/

$config["sign_in"] = array(
	array(
		"field"   => "sMemberId",
		"label"   => "아이디",
		"rules"   => "trim|xss_clean|required"
	),
	array(
		"field"   => "sPassword",
		"label"   => "비밀번호",
		"rules"   => "trim|xss_clean|required"
	),
	array(
		"field"   => "account",
		"label"   => "계정유형",
		"rules"   => "trim|xss_clean|required"
	),
);

$config["sign_in_my"] = array(
		array(
				"field"   => "sMemberId",
				"label"   => "아이디",
				"rules"   => "trim|xss_clean|required"
		),
		array(
				"field"   => "sPassword",
				"label"   => "비밀번호",
				"rules"   => "trim|xss_clean|required"
		)
);

$config["sign_in_myen"] = array(
		array(
				"field"   => "sMemberId",
				"label"   => "ID",
				"rules"   => "trim|xss_clean|required"
		),
		array(
				"field"   => "sPassword",
				"label"   => "Password",
				"rules"   => "trim|xss_clean|required"
		),
);
/*
$config['sign_in_cloud'] = array(
    array(
		"field"   => "sMemberId",
		"label"   => "아이디",
		"rules"   => "trim|valid_email|xss_clean|required"
	),
    array(
		"field"   => "sPassword",
		"label"   => "비밀번호",
		"rules"   => "trim|min_length[6]|max_length[12]|xss_clean|required"
	),
    array(
		"field"   => "account",
		"label"   => "계정유형",
		"rules"   => "trim|xss_clean|required"
	),
);
*/
//my 개편
$config['sign_in_cloud'] = array(
		array(
				"field"   => "userid",
				"label"   => "아이디",
				"rules"   => "trim|valid_email|xss_clean|required"
		),
		array(
				"field"   => "sPassword",
				"label"   => "비밀번호",
				"rules"   => "trim|min_length[6]|max_length[12]|xss_clean|required"
		),
);
//170406
$config['sign_in_clouden'] = array(
		array(
				"field"   => "userid",
				"label"   => "ID",
				"rules"   => "trim|valid_email|xss_clean|required"
		),
		array(
				"field"   => "sPassword",
				"label"   => "Password",
				"rules"   => "trim|min_length[6]|max_length[12]|xss_clean|required"
		),
);




$config["member"] = array(
		array(
				'field'=>'sPassword',
				'label'=>'비밀번호',
				'rules'=>'password_string|required|trim|xss_clean|alpha_numeric|min_length[8]|max_length[15]|matches[rePassword]'
		),
		array(
				'field'=>'rePassword',
				'label'=>'비밀번호확인',
				'rules'=>'trim|xss_clean'
		),
);

$config["memberen"] = array(
		array(
				'field'=>'sPassword',
				'label'=>'Password',
				'rules'=>'required|trim|xss_clean|alpha_numeric|min_length[4]|max_length[15]|matches[rePassword]'
		),
		array(
				'field'=>'rePassword',
				'label'=>'Confirm Password',
				'rules'=>'trim|xss_clean'
		),
);




$config["acmember"] = array(
	array(
		'field'=>'sName',
		'label'=>'이름',
		'rules'=>'trim|xss_clean|required'
	),
	array(
		'field'=>'sRank',
		'label'=>'호칭',
		'rules'=>'trim|xss_clean'
	),
	array(
		'field'=>'sDepartment',
		'label'=>'부서',
		'rules'=>'trim|xss_clean'
	),
	array(
		'field'=>'sEmail',
		'label'=>'이메일',
		'rules'=>'trim|xss_clean|valid_email|required'
	),
	array(
		'field'=>'sInternalPhone',
		'label'=>'전화번호',
		'rules'=>'trim|xss_clean|regex_match[/^[0-9+()]+[+0-9- ()]+[0-9]$/]|max_length[20]'
	),
	array(
		'field'=>'sMobilePhone',
		'label'=>'휴대전화번호',
		'rules'=>'trim|xss_clean|regex_match[/^[0-9+()]+[+0-9- ()]+[0-9]$/]|max_length[20]'
	)
);



$config["acmemberen"] = array(
	array(
		'field'=>'sName',
		'label'=>'Name',
		'rules'=>'trim|xss_clean|required'
	),
	array(
		'field'=>'sRank',
		'label'=>'Title',
		'rules'=>'trim|xss_clean'
	),
	array(
		'field'=>'sDepartment',
		'label'=>'Team/Division',
		'rules'=>'trim|xss_clean'
	),
	array(
		'field'=>'sEmail',
		'label'=>'Email',
		'rules'=>'trim|xss_clean|valid_email|required'
	),
	array(
		'field'=>'sInternalPhone',
		'label'=>'Phone',
		'rules'=>'trim|xss_clean|regex_match[/^[0-9+()]+[+0-9- ()]+[0-9]$/]|max_length[20]'
	),
	array(
		'field'=>'sMobilePhone',
		'label'=>'Mobile Number',
		'rules'=>'trim|xss_clean|regex_match[/^[0-9+()]+[+0-9- ()]+[0-9]$/]|max_length[20]'
	)
);








$config["contact"] = array(
		array(
				'field'=>'sCompanyName',
				'label'=>'업체명',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'sManagerName',
				'label'=>'담당자명',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'sDepartmentName',
				'label'=>'부서명',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'sRank',
				'label'=>'직급',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'sInternalPhone',
				'label'=>'일반전화',
				'rules'=>'trim|xss_clean|required|alpha_dash|max_length[15]'
		),
		array(
				'field'=>'sMobilePhone',
				'label'=>'휴대전화',
				'rules'=>'trim|xss_clean|required|alpha_dash|max_length[15]'
		),
		array(
				'field'=>'sEmail',
				'label'=>'이메일',
				'rules'=>'trim|xss_clean|required|valid_email'
		),
		array(
				'field'=>'privacyassent',
				'label'=>'개인정보 수집,이용 동의',
				'rules'=>'trim|xss_clean|required'
		)
);


$config["contacten"] = array(
		array(
				'field'=>'sCompanyName',
				'label'=>'Company name',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'sManagerName',
				'label'=>'Name',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'sDepartmentName',
				'label'=>'Department',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'sRank',
				'label'=>'Position',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'sInternalPhone',
				'label'=>'Tel',
				'rules'=>'trim|xss_clean|required|alpha_dash|max_length[15]'
		),
		array(
				'field'=>'sMobilePhone',
				'label'=>'Mobile',
				'rules'=>'trim|xss_clean|required|alpha_dash|max_length[15]'
		),
		array(
				'field'=>'sEmail',
				'label'=>'E-mail',
				'rules'=>'trim|xss_clean|required|valid_email'
		)
);

$config["visitapp"] = array(
		array(
				'field'=>'nManagerSeq',
				'label'=>'등록자정보',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'nCompanyContactSeq[]',
				'label'=>'출입대상자',
				'rules'=>'trim|xss_clean|required'
		),
		array(
			'field'=>'sIDC',
			'label'=>'방문위치',
			'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'nWorkType',
				'label'=>'방문목적',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'sContent',
				'label'=>'작업내용',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'dtExpectStartDate',
				'label'=>'방문예정일시',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'dtExpectStartTime',
				'label'=>'방문예정시간',
				'rules'=>'trim|xss_clean|required|numeric|max_length[2]'
		),
		array(
				'field'=>'dtExpectEndDate',
				'label'=>'예상종료일시',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'dtExpectEndTime',
				'label'=>'예상종료시간',
				'rules'=>'trim|xss_clean|required|numeric|max_length[2]'
		),
		array(
			'field'=>'visit_alert',
			'label'=>'신종 코로나 바이러스 안내',
			'rules'=>'trim|xss_clean|required'
		),
);


$config["visitappen"] = array(
		array(
				'field'=>'nManagerSeq',
				'label'=>'Registant',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'nCompanyContactSeq[]',
				'label'=>'Access personnel',
				'rules'=>'trim|xss_clean|required'
		),
		array(
			'field'=>'sIDC',
			'label'=>'Location to visit',
			'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'nWorkType',
				'label'=>'Reason for access',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'sContent',
				'label'=>'Work to be performed',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'dtExpectStartDate',
				'label'=>'Visit schedule',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'dtExpectStartTime',
				'label'=>'Visit schedule',
				'rules'=>'trim|xss_clean|required|numeric|max_length[2]'
		),
		array(
				'field'=>'dtExpectEndDate',
				'label'=>'Finish/Complete schedule',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'dtExpectEndTime',
				'label'=>'Finish/Complete schedule',
				'rules'=>'trim|xss_clean|required|numeric|max_length[2]'
		),
);


$config["visitapp_modify"] = array(
		array(
				'field'=>'nCompanyContactSeq[]',
				'label'=>'출입대상자',
				'rules'=>'trim|xss_clean|required'
		),
		array(
			'field'=>'sIDC',
			'label'=>'방문위치',
			'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'nWorkType',
				'label'=>'방문목적',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'sContent',
				'label'=>'작업내용',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'dtExpectStartDate',
				'label'=>'방문예정일시',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'dtExpectStartTime',
				'label'=>'방문예정시간',
				'rules'=>'trim|xss_clean|required|numeric|max_length[2]'
		),
		array(
				'field'=>'dtExpectEndDate',
				'label'=>'예상종료일시',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'dtExpectEndTime',
				'label'=>'예상종료시간',
				'rules'=>'trim|xss_clean|required|numeric|max_length[2]'
		),
		array(
			'field'=>'visit_alert',
			'label'=>'신종 코로나 바이러스 안내',
			'rules'=>'trim|xss_clean|required'
		),
);


$config["visitappen_modify"] = array(
		array(
				'field'=>'nCompanyContactSeq[]',
				'label'=>'Access personnel',
				'rules'=>'trim|xss_clean|required'
		),
		array(
			'field'=>'sIDC',
			'label'=>'Location to visit',
			'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'nWorkType',
				'label'=>'Reason for access',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'sContent',
				'label'=>'Work to be performed',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'dtExpectStartDate',
				'label'=>'Visit schedule',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'dtExpectStartTime',
				'label'=>'Visit schedule',
				'rules'=>'trim|xss_clean|required|numeric|max_length[2]'
		),
		array(
				'field'=>'dtExpectEndDate',
				'label'=>'Finish/Complete schedule',
				'rules'=>'trim|xss_clean|required'
		),
		array(
				'field'=>'dtExpectEndTime',
				'label'=>'Finish/Complete schedule',
				'rules'=>'trim|xss_clean|required|numeric|max_length[2]'
		),
);


//220530 입금계좌 변경 이벤트 페이지 validation
$config["event"] = array(
	array(
		'field'=>'sCustomerName',
		'label'=>'고객명(회사명)',
		'rules'=>'required|trim|xss_clean'
	),
	array(
		'field'=>'sCustomerEmail',
		'label'=>'이메일',
		'rules'=>'required|trim|xss_clean|valid_email'
	),
	array(
		'field'=>'sPaymentName',
		'label'=>'이벤트 참여자',
		'rules'=>'required|trim|xss_clean'
	),
	array(
		'field'=>'sPaymentPhone',
		'label'=>'참여자 전화번호',
		'rules'=>'required|trim|xss_clean|numeric'
	)
);


