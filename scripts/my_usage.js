let graphObj = Object;

function _initGrph(){
    graphObj = {
        title: {text : "일별 사용량",style : {fontSize : "12px",fontWeight : "700",color : "#606060",fontFamily : "Nanum Gothic"}},
        subtitle: {text : "",style : {fontSize : "11px",fontFamily : "Nanum Gothic"}},
        chart : {renderTo: "",type: "",width: 750,height: 300},
        credits : {enabled : false},
        yAxis : {title: {enabled: true,text: ''}},
        xAxis : {categories: [], tickInterval:1, title: {enabled: false,text: ''},},
        legend: { enabled: false},
        tooltip : {formatter: function() { return "<b>"+ this.series.name +"</b><br/>"+ this.x +": "+ number_format(this.y); }},
        plotOptions: {series: {marker: {enabled: false}}},
        series : [{data:[]}],
        exporting: {enabled: false}
    };
}

function setGrph(_targetObj, _series, _categories, _tickInterval, _plotflag, _unit) {
    _initGrph();
    graphObj.plotOptions.series.marker.enabled = _plotflag;
    graphObj.series[0]          = new Object;
    graphObj.series[0].type     = "line";
    graphObj.series[0].name     = "일별 사용량";
    graphObj.series[0].data     = _series;
    graphObj.xAxis.tickInterval = _tickInterval;
    graphObj.xAxis.categories   = _categories;
    graphObj.yAxis.title.text   = _unit;
    graphObj.tooltip.formatter= function() { return "<b>"+ this.series.name +"</b><br/><b>"+ this.x +"</b>: "+ number_format(this.y)+_unit; }

    $(_targetObj).highcharts(graphObj);
}

$(document).ready(function() {
    $.post($(location).attr('href'), null)
    .done(function(_response){
        if(_response.status){
            setGrph("#bill_day_data_graph_container" , _response.data.graph_day_series, _response.data.graph_day_category, 7, true, _response.data.graph_y_text);
        }
        else{
            alert(_response.msg);
        }
    });
});