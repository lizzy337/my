// cf) cloud_join.js
if($.validator){
	$.validator.addMethod('businessnum', function (value, element) {
        return this.optional(element) || /^\d{3}-\d{2}-\d{5}$/.test(value);
    }, "사업자 등록번호 형식이 일치하지 않습니다('-' 포함 입력).");
	$.validator.addMethod('cellphone', function (value, element) {
        return this.optional(element) || /^\d{3}-\d{3,4}-\d{4}$/.test(value);
    }, "휴대폰 형식이 일치하지 않습니다.");
    $.validator.addMethod('phone', function (value, element) {
        return this.optional(element) || /^(\d{2,3}-)?\d{3,4}-\d{4}$/.test(value);
    }, "전화번호 형식이 일치하지 않습니다.");
 }


var request = {
	'step1':{
		init:function(){
			var form = $('form[name="request1Form"]');
			$('input:radio[name="sOwnerType"]', form).click(function(){ //업체유형
				var OwnerType = this.value;
				if(OwnerType == 'B'){//개인
					 $(form).find('.biznum').hide();
					 $(form).find('.personnum').show();
				}else{
					$(form).find('.biznum').show();
					 $(form).find('.personnum').hide();
				}
			});

			request.step1.valid($('form').eq(0));
			request.step1.setPrice();

		    //$("#nDomainCount").keydown(function (e) {
			$("#nDomainCount").keyup(function (e) {
		        // Allow: backspace, delete, tab, escape, enter and .
		        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
		             // Allow: Ctrl+A, Command+A
		            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
		             // Allow: home, end, left, right, down, up
		            (e.keyCode >= 35 && e.keyCode <= 40))
		        { // let it happen, don't do anything
		        	request.step1.setPrice();
		            return;//원본
		        }
		        // Ensure that it is a number and stop the keypress
		        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105))
		        {
		        	e.preventDefault();//원본
		        }
		        //input number key. 숫자 입력시 자동 계산
		        if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)) //0-9 only
		        {
		        	request.step1.setPrice();
		        }

		    });
		    //기간
		    $("input:radio[name=nServiceMonth]").on("click", function(){
		    	//var _m = request.step1.calServicePrice();
	        	//$(".serviceprice").text(request.step1.setComma(_m));
		    	request.step1.setPrice();
		    });
		    //글로벌 서비스
		    $("input:radio[name=sGlobal]").on("click", function(){
		    	request.step1.setPrice();
		    });

		    $(".urlpart").on("click", '.btn_addurl', function()
		    {
		    	var dmnCnt = $("input:text[name=nDomainCount]").val();
		    	var urlCnt = $(".urlgroup").length;
		    	if( dmnCnt <= urlCnt )
		    	{
		    		alert("서비스 정보의 도메인 개수만큼 입력할 수 있습니다.\r\n추가하려면 도메인 개수를 늘리세요.");
		    		return;
		    	}

		    	var _g = $(".urlgroup").last().clone();
		    	$(".urlgroup").last().find('button').remove();
		    	$(_g).children('input').val('');
		    	$(".urlgroup").last().after(_g);
		    });

		    $("input[name=nDomainCount]").val("1");

		    //test 편의를 위해
		    /*
		    $("input[name=sCompanyName]").val("test업체");
		    //$("input[name=sBusinessNumber]").val("123-45-78963");
			$("input[name=sPresidentName]").val("홍길동");
			$("input[name=tel_Phone2]").val("1111");
			$("input[name=tel_Phone3]").val("1111");
			$("input[name=sName]").val("담당자");
			$("input[name=tel2_Phone2]").val("2222");
			$("input[name=tel2_Phone3]").val("2222");
			$("input[name=hp_second]").val("3333");
			$("input[name=hp_third]").val("3333");
			$("input[name=sEmail]").val("lizzy337@kinx.net");
			$("input[id=url]").val("test.co.kr");
			$("input[id=ip]").val("22.22.22.22");
			$("input[id=ttl]").val("7");
			$("input[name=sSiteDesc]").val("test업체 설명입니다.");
			$("input[name=sTraffic]").val("123456");
			*/

		}//end init
		, calServicePrice:function calServicePrice(){//서비스 비용 계산하기
			var nPrice = 0;
			var form = $('form[name="request1Form"]');
			//글로벌 서비스 사용여부
			var nGlobal = 1;
			var glb = $("input:radio[name=sGlobal]");
			var nTempCnt = glb.length;
			for( var i = 0 ; i < nTempCnt ; i++ )
			{
				if(glb[i].checked == true)
				{
					nGlobal = glb[i].value;
					break;
				}
			}
			if( nGlobal == 0 ){ //사용
				nPrice = 500000;
			}
			else{
				nPrice = 0;
			}
			//개월에 따른 이용료
			var svcMonth = $("input:radio[name=nServiceMonth]");
			var nSvcMonth = 1;
			nTempCnt = svcMonth.length;
			for( var i = 0 ; i < nTempCnt ; i++ )
			{
				if(svcMonth[i].checked == true)
				{
					nSvcMonth = svcMonth[i].value;
					break;
				}
			}
			if( nSvcMonth == 12 ){ nSvcMonth = 10;}//2개월 무료이므로 차감.
			nPrice += (nSvcMonth * 1500000);
			// 도메인 갯수에 따른 계산
			var dmnCnt = $("input:text[name=nDomainCount]").val();
			dmnCnt = dmnCnt - 1;//1개는 기본임. 추가갯수에 대해 300,000원 부가
			if( dmnCnt >= 0)
			{
				nPrice += (  dmnCnt * 300000 );
			}
			//nPrice = nPrice / 10000;//test
			//nPrice = 110;//test
			return nPrice;
		}
		,setPrice:function setPrice(){
			var _m = request.step1.calServicePrice();
        	$(".serviceprice").text(request.step1.setComma(_m));
        	$("input:hidden[name=nServicePrice]").val(_m);
		}
		,setComma:function setComma(nStr){
			nStr += '';
			x = nStr.split('.');
			x1 = x[0];
			x2 = x.length > 1 ? '.' + x[1] : '';
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(x1)) {
				x1 = x1.replace(rgx, '$1' + ',' + '$2');
			}
			return x1 + x2;
		}//end setComma

		,valid:function valid(form){
		    var validOption = {
		        ignore:'.ignore',
		        debug:true,//false,
		        errorClass:'errMsg',
		        submitHandler:function(form){
		        	return request.step1.action(form);
		        },
		        rules:{
		        	sCompanyName:{required:true},
		        	sBusinessNumber:{
		        		required:function(){
		        			var ownertype = $('input:radio[name="sOwnerType"]:checked').val();
		        			//console.log('ownertype'+ownertype);
			        		if(ownertype != 'B')
			        		{
			        			return true;
			        		}
			        		else{
			        			return false;
			        		}
		        		}
		        		,businessnum:true
		        	},
		        	//sBusinessNumber:{required:true, businessnum:true},
		        	sPresidentName:{required:true},
		        	tel_Phone2		: {required:true, number:true },
		        	tel_Phone3		: {required:true, number:true },
					fax2 : {number:true},
					fax3 : {number:true},
		        	sName:{required:true},
		        	tel2_Phone2		: {required:true, number:true },
		        	tel2_Phone3		: {required:true, number:true },
		        	hp_second		: {required:true, number:true },
					hp_third		: {required:true, number:true },
		        	sEmail:{required:true, email:true},
		        	nDomainCount    : {number:true, min:1},
		        	sSiteDesc:{required:true},
		        	sTraffic:{required:true},
		        	sPort    : {number:true}
		        },
		        messages:{
		        	sCompanyName:{required:'업체명을 입력해 주세요.'},
		        	sBusinessNumber:{required:'사업자번호를 입력해 주세요.', businessnum:'정상적인 사업자번호를 입력해주세요.'},
		        	sPresidentName:{required:'대표자명을 입력해 주세요.'},
		        	tel_Phone2 : {required:'전화번호를 입력해 주세요.', number:'숫자를 입력해주세요'},
		        	tel_Phone3 : {required:'전화번호를 입력해 주세요.', number:'숫자를 입력해주세요'},
		        	fax2 : {number:'번호를 입력해주세요.'},
					fax3 : {number:'번호를 입력해주세요.'},
		        	sName:{required:'담당자명을 입력해 주세요.'},
		        	tel2_Phone2 : {required:'전화번호를 입력해 주세요.', number:'숫자를 입력해주세요'},
		        	tel2_Phone3 : {required:'전화번호를 입력해 주세요.', number:'숫자를 입력해주세요'},
		        	hp_second		: {required:'전화번호를 입력해 주세요.', number:'숫자를 입력해주세요'},
					hp_third		: {required:'전화번호를 입력해 주세요.', number:'숫자를 입력해주세요'},
		        	sEmail:{required:'담당자 이메일을 입력해 주세요.', email:'정상적인 Email을 입력해 주세요.'},
		        	nDomainCount    : {number:'숫자를 입력해주세요', min:'1개는 기본입니다.'},
		        	sSiteDesc:{required:'  사이트 설명을 입력해 주세요.'},
		        	sTraffic:{required:'일간 트래픽량을 입력해 주세요.'},
		        	sPort    : {number:'숫자를 입력해주세요'}
		        },
		        groups:{
		        	tel_Phone : 'tel_Phone2 tel_Phone3',
		        	tel2_Phone : 'tel2_Phone2 tel2_Phone3',
		        	hp_Phone : 'hp_second hp_third'
				},
				errorClass:"errMsg",
		        errorPlacement:function(error, element){
		            var n = element.attr("name");

		            if( element.attr("name") == "tel_Phone2" || element.attr("name") == "tel_Phone3")
		            {
		            	error.insertAfter("#tel_Phone3");
					}
		            else if( element.attr("name") == "tel2_Phone2" || element.attr("name") == "tel2_Phone3"){
		            	error.insertAfter("#tel2_Phone3");
					}
		            else if( element.attr("name") == "hp_second" || element.attr("name") == "hp_third"){
		            	error.insertAfter("#hp_third");
					}else{
						var target = $(element);
			            target.after(error);
					}
		        }
		    };//end validOption
		    var validator = $(form).validate(validOption);
		    $(form).data('validator',validator);
		}//end valid

		,submit:function(){
			request.step1.action(form);
	        return false;
			//step2 페이지 나타내기?step3나타내기?
		}//end submit
		,action:function action(form){
			//console.log('step1 action');
			//서비스 URL 관련
			var _url = '';
			$("[name^=URL]").each(function () {
				_url += $(this).val();
			});

			if(_url == ''){
				alert("서비스 URL을 입력해 주세요..");
				$("[name^=URL]").eq(0).focus();
				return false;
			}
			var _ip = '';
			$("[name^=IP]").each(function () {
				_ip += $(this).val();
			});

			if(_ip == ''){
				alert("서비스 URL IP를 입력해 주세요..");
				$("[name^=IP]").eq(0).focus();
				return false;
			}
			var _ttl = '';
			$("[name^=TTL]").each(function () {
				_ttl += $(this).val();
			});

			if(_ttl == ''){
				alert("서비스 URL TTL정보를 입력해 주세요..");
				$("[name^=TTL]").eq(0).focus();
				return false;
			}
			//서비스 port
			var portType = $("input:radio[name=sPortRadio]");
			var nPortType = 1;
			var nTempCnt = portType.length;
			for( var i = 0 ; i < nTempCnt ; i++ )
			{
				if(portType[i].checked == true)
				{
					nPortType = portType[i].value;
					break;
				}
			}

			var sPort = $("input:text[name=sPort]").val();
			if( nPortType == 1 && ( sPort === null || sPort === "" ) )
			{
				alert("서비스 제공중인 port를 입력하세요 .");
				$("[name=sPort]").focus();
				return false;
			}

			//약관 관련
			if($("#use_agreement").is(":checked") != true){
				alert("서비스 이용약관을 확인하시고, 동의해 주세요.");
				$("#use_agreement").focus();
				return false;
			}
			if($("#per_agreement").is(":checked") != true){
				alert("개인정보 수집, 이용안내를 확인하시고, 동의해 주세요.");
				$("#per_agreement").focus();
				return false;
			}
			//console.log('step1 action 2');
            $(form).ajaxSubmit({
                dataType:'json',
                success:function(data){
                    if(data.success){
                    	//console.log("step1 insert success");
                    	//console.log(data);
                    	var insertId = data.success;
                    	var data = $(form).serialize()+"&nInsertId="+insertId;
                		$.dynamicForm('/kclean/request/request_step2', data, 'post');

                    }else{
                    	//console.log("step1 insert fail");
                    	//console.log(data);
                        alert("등록에 실패하였습니다.");
                    }
                },
                error:function(xhr, status, error){
                    alert('알 수 없는 오류 입니다.\n같은 현상이 반복하여 발생시 관리자에게 문의 바랍니다.');
                }
            });

        }//end action
	}//end step1
	,'step2':{
		init:function(){
			request.step2.valid($('form').eq(0));
		}//end init
		,valid:function valid(form){
			var validOption = {
		        ignore:'.ignore',
		        debug:false,
		        errorClass:'errMsg',
		        submitHandler:function(form){
		        	return request.step2.submit(form);
		        },
		        rules:{
		        },
		        messages:{
		        },
		        errorPlacement:function(error, element){
		            var n = element.attr("name");
		            var target = $(element);
		            target.after(error);
		        }
		    };
		    var validator = $(form).validate(validOption);
		    $(form).data('validator',validator);
		}

		,submit:function(form){
			request.step2.action(form);
            return false;
		}//end submit
		,action:function action(form){
			//약관 관련
			if($("#chkpayment").is(":checked") != true){
				alert("결제할 서비스 내용을 확인하시고, 동의해 주세요.");
				$("#chkpayment").focus();
				return false;
			}
			parent.window.name="requestStep2";
			window.open('', 'paymentreq_popup', 'width=400,height=250,resizeable,scrollbars');
			form.target = "paymentreq_popup";
			form.action = "/support/payment/payment02_req";
			form.submit();

	    }//end action
	}//end step2
	,'step3':{
		close:function(){
			/*
			request.requestPopup.close();
			if(request.requestPopup)
			{
				console.log('popwindow close');
				request.requestPopup.close();
			}
			else
			{
				console.log('popwindow not');
				console.log(request.requestPopup);
			}
			*/
		}
	}//end step3
};//end request


function isActiveXOK(){
  if(lgdacom_atx_flag == true){
    document.getElementById('LGD_BUTTON1').style.display='none';
    document.getElementById('LGD_BUTTON2').style.display='';
  }else{
    document.getElementById('LGD_BUTTON1').style.display='';
    document.getElementById('LGD_BUTTON2').style.display='none';
  }
}

const array = new Uint16Array(1);
function makeOfferOrder(){
	let d             = new Date();
	let curr_msec     = d.getMilliseconds();
	let result        = window.crypto.getRandomValues(array);
	let strOfferOrder = d.getFullYear().toString() + (d.getMonth()+1).toString()+d.getDate().toString()+"_"+result.toString()+"_"+curr_msec.toString();
	return strOfferOrder;
}

/*
function payment02_req_popup(form){
	window.open('/support/payment/payment02_req', 'paymentreq_popup', 'width=400,height=250,resizeable,scrollbars');
    form.target = 'paymentreq_popup';
}
*/
