//116.211) new cloud 의 join.js
/*jquery.vaildate extend*/
if($.validator){
    $.validator.addMethod('cellphone', function (value, element) {
        return this.optional(element) || /^\d{3}-\d{3,4}-\d{4}$/.test(value);
    }, "휴대폰 형식이 일치하지 않습니다.");
    $.validator.addMethod('phone', function (value, element) {
        return this.optional(element) || /^(\d{2,3}-)?\d{3,4}-\d{4}$/.test(value);
    }, "전화번호 형식이 일치하지 않습니다.");
    $.validator.addMethod('businessnum', function (value, element) {
        return this.optional(element) || /^\d{3}-\d{2}-\d{5}$/.test(value);
    }, "사업자 등록번호 형식이 일치하지 않습니다('-' 포함 입력).");
    $.validator.addMethod('passowrd', function(value, element){
        return this.optional(element) || /^(?=.*[a-zA-Z])(?=.*[0-9]).{8,15}$/.test(value);
    }, "영문+숫자 혼합하여 8~15자로 입력해주세요.");
 }
var join = {
    'doPay_ActiveX':function doPay_ActiveX(){
        if(document.LGD_PAYINFO.agree_chk.checked == false){
            alert('신용카드 자동결제 동의가 필요합니다.');
            return false;
        }
        var ret = xpay_check(document.getElementById('LGD_PAYINFO'), '<?= $CST_PLATFORM ?>');
        //LGD_RESPCODE = "0000";
        if (ret=="00"){     //ActiveX 로딩 성공
            var LGD_RESPCODE        = dpop.getData('LGD_RESPCODE');       //결과코드
            var LGD_RESPMSG         = dpop.getData('LGD_RESPMSG');        //결과메세지

            if("0000" == LGD_RESPCODE ) { //인증성공
                //실재 결제모듈 동작일때
                var LGD_BILLKEY         = dpop.getData('LGD_BILLKEY');        //추후 빌링시 카드번호 대신 입력할 값입니다
                var LGD_PAYTYPE         = dpop.getData('LGD_PAYTYPE');        //인증수단
                var LGD_PAYDATE         = dpop.getData('LGD_PAYDATE');        //인증일자
                var LGD_FINANCECODE     = dpop.getData('LGD_FINANCECODE');    //인증기관코드
                var LGD_FINANCENAME     = dpop.getData('LGD_FINANCENAME');    //인증기관이름        

                var frm = document.frmPayment;
                frm.LGD_RESPMSG.value = LGD_RESPMSG;
                frm.LGD_BILLKEY.value = LGD_BILLKEY;
                frm.LGD_PAYTYPE.value = LGD_PAYTYPE;
                frm.LGD_PAYDATE.value = LGD_PAYDATE;
                frm.LGD_FINANCECODE.value = LGD_FINANCECODE;
                frm.LGD_FINANCENAME.value = LGD_FINANCENAME;
                //frm.action = "/public/join/payment/payment.php";
                
                $(frm).ajaxSubmit({
                    dataType:'json',
                    success:function(data){
                        if(data.success){
                           document.location.href="/cloud/public/join/welcome/";
                        }else{
                           alert(data.error['message']);
                        };
                    },
                    error:function(xhr, error){
                        switch(xhr.status){
                            case 401:
                                alert('가입절차가 지연되었거나, 알수 없는 오류로 인해 더이상의 진행이 불가능합니다.\n\n처음부터 다시 등록해 주세요. 불편을 드려 죄송합니다.');
                                break;
                            case 403:
                                alert('잘못된 경로로 접근하셨습니다.');
                                break;
                            default:
                                alert('알 수 없는 오류 입니다.');
                                break;
                        }
                    }
                });
                return false;
            } else { //인증실패
                alert("인증이 실패하였습니다. " + LGD_RESPMSG);
                return false;
            }
        } else {
            alert("LG U+ 전자결제를 위한 XPayPlugin 모듈이 설치되지 않았습니다.");
            xpay_showInstall();
            return false;
        }
        return false;
    },
    'register':{
        init:function init(){
            var form = $('form[name="register"]');
            form.find('input[name="account_company_type"]').click(function(){
                var corp_type = this.value;
                if(corp_type=='A'){
                    form.find('.input_corp').hide();
                }else{
                    form.find('.input_corp').show();
                }
            });
            join.register.valid(form.eq(0));            
        },
        valid:function(form){
            var validOption = {
                ignore:'.ignore',
                debug:true,
                errorClass:'errMsg',
                submitHandler:function(form){
                  return join.register.action(form);
                },
                rules:{
                    userid:{required:true,email:true},
                    confirm_userid:{equalTo:'input[name="userid"]',email:true},
                    password:{required:true,passowrd:true},
                    confirm_password:{equalTo:'input[name="password"]'},
                    user_name:{required:true},
                    user_email:{required:true,email:true},
                    user_cellphone:{required:true,cellphone:true},
                    user_phone:{phone:true},
                    account_zipcode:{},
                    account_address:{},
                    account_address2:{},
                    
                    account_name:{required:function(){return (form.find('input[name="account_company_type"]:checked').val()!='A')?true:false;}},
                    account_ceo_name:{required:function(){return (form.find('input[name="account_company_type"]:checked').val()!='A')?true:false;}},
                    account_business_number:{required:function(){return (form.find('input[name="account_company_type"]:checked').val()!='A')?true:false;},businessnum:true}
                },
                messages:{
                    userid:{required:'아이디를 입력해 주세요.',email:'아이디 형식이 일치하지 않습니다.'},
                    confirm_userid:{equalTo:'이메일이 일치하지 않습니다.',email:'아이디 형식이 일치하지 않습니다.'},
                    password:{required:'비밀번호를 입력해 주세요.'},
                    confirm_password:{equalTo:'비밀번호가 일치하지 않습니다.'},
                    user_name:{required:'사용자명을 입력해 주세요'},
                    user_email:{required:'이메일주소를 입력해 주세요',email:'이메일 형식이 일치하지 않습니다.'},
                    user_cellphone:{required:'휴대폰 번호를 입력해 주세요'},
                    
                    account_name:{required:'업체명을 입력해 주세요'},
                    account_ceo_name:{required:'대표자명을 입력해 주세요'},
                    account_business_number:{required:'사업자번호를 입력해 주세요'}
                }
            } //end of validoption
            var validator = $(form).validate(validOption);//end validate
            $(form).data('validator',validator);
        },
        submit:function submit(form){
            join.register.action(form);
            return false;
        },
        action:function action(form){
            $(form).ajaxSubmit({
                dataType:'json',
                success:function(data){
                    if(data.success){
                        document.location.href = '/cloud/public/join/payment';
                    }else{
                        if(data.error && data.error['message']){
                            alert(data.error['message']);
                        }else{
                            alert('알 수 없는 오류 입니다.');
                        }
                    }
                },
                error:function(xhr, status, error){
                    alert('알 수 없는 오류 입니다.\n같은 현상이 반복하여 발생시 관리자에게 문의 바랍니다.');
                }
            });
        }
    },
    'payment':{
        init:function init(){
            var form = $('form[name="LGD_PAYINFO"]');
            //join.doPay_ActiveX();
            form.on('submit',function(){
                return join.doPay_ActiveX();
            });
        },
        action:function action(){
            
        }
    },
    'find':{
        init:function init(){
            var frmId = $('form[name="findid"]').get(0);
            var frmPwd = $('form[name="findpassword"]').get(0);
            join.find.findid.init(frmId);
            join.find.findpassword.init(frmPwd);
        },
        findid:{
            init:function init(form){
               this.valid(form);                
            },
            valid:function valid(form){
                var validOption = {
                    ignore:'.ignore',
                    debug:true,
                    errorClass:'errMsg',
                    submitHandler:function(form){
                        return join.find.findid.action(form);
                    },
                    rules:{
                        username:{required:true},
                        useremail:{required:true,email:true}
                    },
                    messages:{
                        username:{required:'이름을 입력해 주세요'},
                        useremail:{required:'이메일을 입력해 주세요',email:'이메일 형식이 일치하지 않습니다.'}
                    }
                };
                var validator = $(form).validate(validOption);//end validate
                $(form).data('validator',validator);
            },
            action:function action(form){
                $(form).ajaxSubmit({
                    url:'/cloud/public/join/find/id/',
                    dataType:'json',
                    success:function(data){
                        if(data.success){
                            alert(data.results.message);
                        }else{
                            if(data.error){
                                alert(data.error.message);
                            }else{
                                alert('알 수 없는 오류 입니다.');
                            }
                        }
                    },
                    error:function(xhr, error){
                        alert('알 수 없는 오류 입니다.');
                        
                    }
                });
                return false;
            }
        },
        findpassword:{
            init:function init(frmPwd){
               this.valid(frmPwd);
            },
            valid:function valid(frmPwd){
                 var validOption = {
                    ignore:'.ignore',
                    debug:true,
                    errorClass:'errMsg',
                    submitHandler:function(form){
                        if(confirm('임시 비밀번호를 메일로 보내 드립니다.\n\n원치 않으시면 취소를 눌러 주세요')){
                            join.find.findpassword.action(form);
                        }
                      //return join.register.action(form);
                    },
                    rules:{
                        username:{required:true},
                        userid:{
                            required:function(){return ($(frmPwd).find('input:radio:checked').val()=='email')?true:false},
                            email:function(){return ($(frmPwd).find('input:radio:checked').val()=='email')?true:false}
                        },
                        userphone:{
                            required:function(){return ($(frmPwd).find('input:radio:checked').val()=='phone')?true:false},
                            cellphone:function(){return ($(frmPwd).find('input:radio:checked').val()=='email')?true:false}
                        }
                    },
                    messages:{
                        username:{required:'이름을 입력해 주세요.'},
                        userid:{required:'아이디를 입력해 주세요.', email:'이메일 형식이 일치하지 않습니다.'},
                        userphone:{required:'휴대전화 번호를 입력해 주세요', cellphone:'휴대전화 형식이 일치하지 않습니다.'}
                    }
                }
                var validator = $(frmPwd).validate(validOption);
                $(frmPwd).data('validator',validator);
            },
            action:function action(form){
                $(form).ajaxSubmit({
                    url:'/cloud/public/join/find/password/',
                    dataType:'json',
                    success:function(data){
                        if(data.success){
                            document.location.href = '/cloud/public/join/find/password/email';
                        }else{
                            if(data.error){
                                alert(data.error.message);
                            }else{
                                alert('알 수 없는 오류 입니다.');
                            }
                        }
                    },
                    error:function(xhr, error){
                        alert('알 수 없는 오류 입니다.');
                        
                    }
                });
                return false;
            }
        }        
    },
    'resendMail':function(){
        $.ajax({
           url:'/cloud/public/join/mail/',
           dataType:'json',
           success:function(data){
               if(data.success){
                   alert('메일을 발송하였습니다.');
               }else{
                   alert('알 수 없는 오류 입니다(Code:001).');
               }
           },error:function(xhr, status, error){
                var res = xhr.responseText;
                if($.isJSON(res)){
                    res = $.parseJSON(res);
                    if(res.error && res.error.message){
                        alert(res.error.message);
                    }
                }else{
                    alert('알 수 없는 오류 입니다(Code:002).');
                };
           }
        });
        return false;
    },
    //-start 20140911 mykinx 카드정보 등록 관련 by lizzy
    'paymentinfo_add':{
        init:function init(){
            var form = $('form[name="LGD_PAYINFO"]');
            //join.doPay_ActiveX();
            form.on('submit',function(){
                return join.doPay_ActiveX_infoadd();
            });
        },
        action:function action(){
            
        }
    },
    'doPay_ActiveX_infoadd':function doPay_ActiveX(){
    	//동의 체크
        if(document.LGD_PAYINFO.agree_chk.checked == false){
            alert('카드 자동 결제 동의 항목에 체크 해주셔야 \r\n카드 인증 페이지로 넘어 갑니다.');
            return false;
        }
        //tCloudLink에서 확인하기 
		$url = "/mypage/Service/check_cloudInfo";
		$.ajax({
			url:$url,
			type:'POST',
			dataType:'JSON',
			success:function(data){
				if( data.success == true){ 
					//console.log("check cloudinfo success");
				}
				else{
					alert(data.message);
					return false;
				}	
			},
			error:function(data){
				alert(data.message);
				return false;
			}
		}); 
		 
        //등록
        var ret = xpay_check(document.getElementById('LGD_PAYINFO'), '<?= $CST_PLATFORM ?>');
        //LGD_RESPCODE = "0000";
        if (ret=="00"){     //ActiveX 로딩 성공
            var LGD_RESPCODE        = dpop.getData('LGD_RESPCODE');       //결과코드
            var LGD_RESPMSG         = dpop.getData('LGD_RESPMSG');        //결과메세지

            if("0000" == LGD_RESPCODE ) { //인증성공
                //실재 결제모듈 동작일때
                var LGD_BILLKEY         = dpop.getData('LGD_BILLKEY');        //추후 빌링시 카드번호 대신 입력할 값입니다
                var LGD_PAYTYPE         = dpop.getData('LGD_PAYTYPE');        //인증수단
                var LGD_PAYDATE         = dpop.getData('LGD_PAYDATE');        //인증일자
                var LGD_FINANCECODE     = dpop.getData('LGD_FINANCECODE');    //인증기관코드
                var LGD_FINANCENAME     = dpop.getData('LGD_FINANCENAME');    //인증기관이름        

                var frm = document.frmPayment;
                frm.LGD_RESPMSG.value = LGD_RESPMSG;
                frm.LGD_BILLKEY.value = LGD_BILLKEY;
                frm.LGD_PAYTYPE.value = LGD_PAYTYPE;
                frm.LGD_PAYDATE.value = LGD_PAYDATE;
                frm.LGD_FINANCECODE.value = LGD_FINANCECODE;
                frm.LGD_FINANCENAME.value = LGD_FINANCENAME;
                //frm.action = "/public/join/payment/payment.php";
                   
                $(frm).ajaxSubmit({
                    dataType:'json',
                    success:function(data){
                        if(data.success){
                           //document.location.href="/cloud/public/join/welcome/";
                        	alert("결제 카드 변경이 성공적으로 완료 되었습니다.");
                        }else{
                        	console.log("ajaxsubmit fail");
                           alert(data.error['message']);
                        };
                    },
                    error:function(xhr, error){
                        switch(xhr.status){
                            case 401:
                                alert('알수 없는 오류로 인해 더이상의 진행이 불가능합니다. 처음부터 다시 등록해 주세요. 불편을 드려 죄송합니다.');
                                break;
                            case 403:
                                alert('잘못된 경로로 접근하셨습니다.');
                                break;
                            default:
                                alert('알 수 없는 오류 입니다.');
                                break;
                        }
                    }
                });
                return false;
            } else { //인증실패
                alert("인증이 실패하였습니다. " + LGD_RESPMSG);
                return false;
            }
        } else {
            alert("LG U+ 전자결제를 위한 XPayPlugin 모듈이 설치되지 않았습니다.");
            xpay_showInstall();
            return false;
        }
        return false;
    }
    //-end 20140911
}