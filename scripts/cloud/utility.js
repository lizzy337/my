var utility = {
    openPopup:function openPopup(theURL, winName, width, height, remFeatures) {
        var features = "";
        if (typeof winName == "undefined") winName = "";
        if (typeof width != "undefined") features += ((features) ? "," : "")+"width="+width;
        if (typeof height != "undefined") features += ((features) ? "," : "")+"height="+height;
        if (typeof remFeatures != "undefined") features += ((features) ? "," : "")+remFeatures;
        if (features.indexOf("status") < 0) features += ",status=yes";
        var popup = window.open(theURL, winName, features);
        popup.focus();
        return popup;
    },
    openZipcode:function openZipcode() {
        utility.openPopup("/utility/zipcode/", "ZipcodeSearch", 520, 400, "left=100, top=100");
        return false;
    },
    getParameterByName:function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}
$(document).ready(function(){
    if($.browser.msie && $.browser.version < 10){
        $('.browser_notice').bind('click',function(){$(this).hide();}).show().delay(2000).fadeOut(1500);
    }else{
        $('.browser_notice').hide();
        //$('.browser_notice').bind('click',function(){$(this).hide();}).show().delay(2000).fadeOut(1500); //for test:
    };
});

