;(function($, window, document, screen){
    "use static";
    $.fn.serializeJSON = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    var _ixcAccount = {
        /**
         * 팝업 창 이름 정의
         * @type {Object}
         */
        target:{
            grizzly: 'grizzly_dashboard',
            juno:'myixcloud'
        },
        /**
         * 클라우드 서비스 호스트 정의
         * @type {Object}
         */
        hosts:{
            grizzly:'https://new.ixcloud.net',
            juno:'https://my.ixcloud.net' //org
            //juno:'http://1.201.137.199:8322' //test, otp 
        },
        /**
         * ajax url 정의
         * @type {Object}
         */
        urls:{
            'auth':{
                'grizzly': '/auth/auto/',
                'juno': '/user/login/'///account/login/',190820
            },
            'join':{
                'juno': '/user/join'
            },
            'findpwd':{
                'juno': '/user/findpwd'
            }
        },
        /**
         * 로그인 폼 정보
         * @type {[type]}
         */
        login_form:null,
        /**
         * 로그인 성공시 userid 저장 쿠키 명
         * CI의 cookie prefix 설정을 맞추기 위해 kinxnet_을 붙여줌
         * @type {String}
         */
        cookie_name:"kinxnet_cloud_user",
        /**
         * 아이디 저장 을 위한 쿠키 명
         * CI의 cookie prefix 설정을 맞추기 위해 kinxnet_을 붙여줌
         * @type {String}
         */
        cookie_name_saved:'kinxnet_saved_cloud_id', //ci_prefix = kinxnet_
        /**
         * 언어 코드 설정
         * @type {String}
         */
        language_code: 'kr',
        /**
         * 언어 사전 정의
         * @type {Object}
         */
        language: {
            'kr':{
                'common.popup.block': '팝업이 차단 되었습니다.',
                'common.popup.block.notice': '공지 팝업이 차단되었습니다.',
                'common.error': '알 수 없는 오류입니다.',
                'required': '필수항목 입니다.',
                'required.id': '아이디를 입력해 주세요.',
                'required.login.info': '로그인 정보를 입력해 주세요.',
                'required.password': '패스워드를 입력해 주세요.',
                'manage.open.console.title': '클라우드 관리 콘솔 열기',
                'manage.open.page.title': '클라우드 관리 페이지 열기',
                'login.select.service.title': '클라우드 서비스 선택',
                'login.cancel': '로그인 취소',
                'login.fail.limit': '로그인 실패 횟수 초과로 해당 계정에 대한 로그인이 제한 되었습니다.\n\n비밀번호 찾기를 통해 비밀번호를 변경하거나 담당자에게 문의하시기 바랍니다.',
                'login.fail.notfound': '일치하는 회원 정보를 찾을 수 없습니다.',
                'provider.project.desc': '{{project_name}}',
                'provider.projects.desc': '{{project_name}} 외 {{count}} 개'
            },
            'en':{
                'common.popup.block': 'Pop-up blocked.',
                'common.popup.block.notice': 'Notice pop-up blocked.',
                'common.error': 'Unknown error.',
                'required': 'This field cannot be blank.',
                'required.id': 'Enter your email.',
                'required.login.info': 'Please enter your login information.',
                'required.password': 'Enter your password.',
                'manage.open.console.title': 'Open the cloud management console',
                'manage.open.page.title': 'Open the Manage Clouds page',
                'login.select.service.title': 'Select a cloud service',
                'login.cancel': 'Cancel Login',
                'login.fail.limit': 'Sign-in to this account was restricted because the number of login failures was exceeded.\n\nPlease change your password via password assistance or contact your representative.',
                'login.fail.notfound': 'No matching login information found.',
                'provider.project.desc': '{{project_name}}',
                'provider.projects.desc': '{{project_name}} and {{count}} others'
            }
        },
        /**
         * 페이지 로딩 시 초기화
         * @return {[type]} [description]
         */
        init:function(){
            var _this = this;

            // 언어 코드 설정
            var prefix_pathname = window.location.pathname.substring(1, window.location.pathname.length).split('/')[0];
            if(prefix_pathname && ['kr', 'en'].indexOf(prefix_pathname)){
                _this.language_code = prefix_pathname;
            }else{
                _this.language_code = 'kr';
            }

            // CloudG modal dialog 초기화
            $('#id_modal_cloud_grizzly_auth').dialog({
               'autoOpen':false,
               'title':_this.translate('manage.open.console.title'),
               'width':400,
               'height':150
            });

            // CloudJ/K modal dialog 초기화
            $('#id_modal_cloud_juno_auth').dialog({
               'autoOpen':false,
               'title':_this.translate('manage.open.page.title'),
               'width':400,
               'height':150
            });

            // CloudG/J/K modal diaklog 초기화
            var modal_all_auth = $('#id_modal_cloud_all_auth').dialog({
               autoOpen:false,
               modal: true,
               resizable:true,
               title:_this.translate('login.select.service.title'),
               width:600,
               height:330,
               buttons:{
                    'CANCEL':function(){
                        modal_all_auth.dialog('close');
                    }
               },
               dialogClass: 'multiauth-dialog',
               close: function(){

               }
            });
            $('.multiauth-dialog .ui-button-text:contains(CANCEL)').text(_this.translate('login.cancel'));

            // CloudG 관리콘솔 바로가기 버튼 설정
            $('button[name="btn_grizzly_dashboard"]')
                .button()
                .click(function(event){
                    event.preventDefault();
                    try{
                        var win = ixcAccount.open_default_win('grizzly');
                        $('form[name="grizzly_auth"]').submit();
                        win.focus();
                        return false;
                    }catch(e){
                        alert(_this.translate('common.popup.block'));
                        return false;
                    }
                });

            // CloudJ/K 관리 메뉴 바로가기 버튼 설정
            $('button[name="btn_myixcloud_manage"]')
                .button()
                .click(function(event){
                    event.preventDefault();
                    try{
                        var win = ixcAccount.open_default_win('juno');
                        var form = $('form[name="juno_auth"]');
                        form.submit();
                        win.focus();
                        return false;
                    }catch(e){
                        alert(_this.translate('common.popup.block'));
                        return false;
                    }
                });
            $('#save_cloud_id').change(function(){
                if($(this).prop('checked')===false){
                    Cookies.remove(ixcAccount.cookie_name_saved);
                }
            });
            this.login_form = $('form[name="cloud_login"]');
            this.set_login_section();
            this.login.init();
        },
        /**
         * 클라우드 로그인 영역 template 적용
         *
         */
        set_login_section:function(){
            var target = $('.cloud_login_section');
            var userid = Cookies.get(_ixcAccount.cookie_name);
            var saved_id = Cookies.get(_ixcAccount.cookie_name_saved);
            if(userid){
                if(target.find('.cloud_login_section_signed').length===0){
                    var template = '/templates/cloud_login_section_signed.html';
                    if(this.language_code !=='kr'){
                        template = '/templates/cloud_login_section_signed_en.html';
                    }
                    $.get(template, function(data){
                        data = data.replace(/\{cloud_user\}/gi, userid);
                        data = $(data);
                        target.html(data);
                    });
                }
            }else{
                if(target.find('.cloud_login_section_form').length===0){
                    var template = '/templates/cloud_login_section_form.html';
                    if(this.language_code !=='kr'){
                        template = '/templates/cloud_login_section_form_en.html';
                    }
                    $.get(template, function(data){
                        data = $(data);
                        target.html(data);
                        if(saved_id){
                            $('#save_cloud_id').prop('checked', true);
                            data.find('input[name="userid"]').val(saved_id);
                        }else{
                            $('#save_cloud_id').prop('checked', false);
                            data.find('input[name="userid"]').val('');
                        }
                    });
                }
            }
        },
        /**
         * 팝업 기본 설정
         * @param  {[type]} type [description]
         * @return {[type]}      [description]
         */
        open_default_win: function(type){
            var win = null;
            if(type==='grizzly'){
                var win_width = parseInt((screen.availWidth) * 0.75);
                var win_height = parseInt((screen.availHeight) * 0.85);
                win = window.open(
                    'about:blank',
                    ixcAccount.target.grizzly,
                    'width=' + win_width + ',height=' + win_height + ',top=20,left=20,resizable=1,scrollbars=1');
            }else if(type==='juno'){
                win = window.open(
                    'about:blank',
                    ixcAccount.target.juno,
                    'width=1024,height=700,top=20,left=20,resizable=1,scrollbars=1');
            }
            return win;
        },
        /**
         * 로그인 관련 설정
         * @type {Object}
         */
        login: {
            init:function init(){
                var this_ = ixcAccount.login;
                this_.valid(_ixcAccount.login_form);
            },
            valid:function valid(form){
                var _this = this;
                var validOption = {
                    ignore: '.ignore',
                    debug: false,
                    errorClass: 'errMsg',
                    submitHandler: function(form, event){
                        event.preventDefault();
                       ixcAccount.login.action(form);
                    },
                    rules: {
                        userid:{required:true},
                        password:{required:true}
                    },
                    messages: {
                        login:{required:ixcAccount.translate('required.login.info')},
                        userid:{required:ixcAccount.translate('required.login.info')},
                        password:{required:ixcAccount.translate('required.login.info')}
                    },
                    errorPlacement: function(error, element){
                        var target = $(form).find('.error_place');
                        target.append(error);
                    },
                    groups: {
                        login:'userid password'
                    },
                    highlight: function(element, errorClass) {
                        /*
                        $(element).fadeOut(function() {
                          $(element).fadeIn();
                        });
                        */
                    }
                }; //end of validoption
                var validator = $(form).validate(validOption);//end validate
                $(form).data('validator',validator);
            },
            action:function action(form){                
                var data = $(form).serializeJSON();
                $.ajax({
                    url:'/cloud/auth/login',
                    type:'post',
                    dataType:'json',
                    data:data,
                    success:function(data){
                        //202 : OTP 로그인 성공  by lizzy337 180109
                        if(data.juno.status==202)
                        {
                            var otpCallback = function (data) { ixcAccount.login.set_provider_param(data); }
                            var authOTP = new Function(data.juno.results.view)();
                            authOTP.open(otpCallback);
                            
                            return;
                        }
                        else
                        {
                            ixcAccount.login.set_provider_param(data);
                        }
                    },
                    error:function(xhr, status, error){
                        console.log('error data', xhr);
                        var res = xhr.responseText;
                        //alert(_this.translate('common.error') + '(Code:003).');//test 190820
                    }
                });
            },
            /**
             * 계정 보유 상태에 따라 레이어 내용 변경
             * @param  {[type]} data [description]
             * @return {[type]}      [description]
             */
            set_provider_param: function(data){
                console.log('sett_privider_param'); console.log(data);
                var dialog = null;
                var userid = _ixcAccount.login_form.find('input[name="userid"]').val();
                var save_id_check = $('#save_cloud_id').prop('checked');
                Cookies.set(ixcAccount.cookie_name, userid);

                $('#id_ixc_g_project_desc').val('');
                $('#id_ixc_j_project_desc').val('');

                if(save_id_check){
                    Cookies.set(ixcAccount.cookie_name_saved, userid, { expires: 365 });
                }

                //Grizzly
                if(data.grizzly.success && !data.juno.success){
                    this.set_provider_param_grizzly(data.grizzly.results);
                    dialog = $('#id_modal_cloud_grizzly_auth').dialog('open');
                    // 첫번째 버튼 자동 forcus 제거
                    $('.ui-dialog :button').blur();
                }
                //Juno/Koala
                else if(!data.grizzly.success && data.juno.success){
                    this.set_provider_param_juno(data.juno.results);
                    dialog = $('#id_modal_cloud_juno_auth').dialog('open');
                    // 첫번째 버튼 자동 forcus 제거
                    $('.ui-dialog :button').blur();
                }
                //Grizzly, Juno/Koala 둘 다 있을 때
                else if(data.grizzly.success && data.juno.success){
                    this.set_provider_param_grizzly(data.grizzly.results);
                    this.set_provider_param_juno(data.juno.results);
                    dialog = $('#id_modal_cloud_all_auth').dialog('open');
                    // 첫번째 버튼 자동 forcus 제거
                    $('.ui-dialog :button').blur();
                }
                //없을 때
                else{
                    Cookies.remove(ixcAccount.cookie_name);
                    Cookies.remove(ixcAccount.cookie_name_saved);
                    if(data.juno.error.message == 'failed login count is limited'){
                        alert(_ixcAccount.translate('login.fail.limit'));
                    }else{
                        alert(_ixcAccount.translate('login.fail.notfound'));
                    }

                }
                _ixcAccount.set_login_section();

                /* 그리즐리 빠짐. 체크하는 부분 주석처리함. 170906
                // 서버 상태 이상
                if (!data.grizzly.success && data.grizzly.status==503){
                    try{
                        var announce_pop = window.open('/announce/','announce_pop' + Math.random().toString(36).substring(7),
                            'width=405,height=600,top=20,left=20,scrollbars=no,toolbar=no');
                        announce_pop.focus();
                    }catch(e){
                        alert(_ixcAccount.translate('common.popup.block.notice'));
                    }
                }
                */
            },
            set_provider_param_grizzly:function(data){
                var key = data.key;
                var project_desc = ixcAccount.login_form.find('input[name="userid"]').val();
                var form = $('form[name="grizzly_auth"]');
                    form.find('input[name="key"]').val(key);
                    form.attr('action',ixcAccount.hosts.grizzly + ixcAccount.urls.auth.grizzly);
                    form.attr('target',ixcAccount.target.grizzly);
                    form.attr('method','post');
                $('#id_ixc_g_project_desc').text(project_desc);

            },
            set_provider_param_juno: function(data){
                var key = data.key;
                var projects = data.projects;
                var projects_length = projects.length;
                var project_desc = null;
                if(projects_length == 1){
                    project_desc = _ixcAccount.translate(
                        'provider.project.desc',
                        {'project_name': projects[0].name});
                }else if(projects_length > 1){
                    project_desc = _ixcAccount.translate(
                        'provider.projects.desc',
                        {'project_name': projects[0].name, 'count': projects_length-1});
                }

                $('#id_ixc_j_project_desc').text(project_desc);

                var form = $('form[name="juno_auth"]');
                    form.find('input[name="key"]').val(key);
                    //form.attr('action', ixcAccount.hosts.juno + ixcAccount.urls.auth.juno);
                    form.attr('action', ixcAccount.app_prefix_to_path(ixcAccount.urls.auth.juno));
                    form.attr('target',ixcAccount.target.juno);
                    form.attr('method','GET');
            }
        },
        /**
         * 비밀번호 찾기 팝업
         * @return {[type]} [description]
         */
        findpwd: function(){
            var win = _ixcAccount.open_default_win('juno');
            win.location.href = this.app_prefix_to_path(ixcAccount.urls.findpwd.juno);
        },
        /**
         * 회원 가입 팝업
         * @return {[type]} [description]
         */
        join: function(){
            var win = _ixcAccount.open_default_win('juno');
            win.location.href = this.app_prefix_to_path(ixcAccount.urls.join.juno);
            win.focus();
        },
        /**
         * API 경로 가져오기
         * @param  {[type]} path [description]
         * @return {[type]}      [description]
         */
        app_prefix_to_path: function(path){
            if (this.language_code && this.language_code !== 'kr'){
                var prefix = this.language_code;
                if (path.substring(0,1) == '/'){
                    prefix = "/" + prefix;
                }
                path = prefix + path;
            }
            return ixcAccount.hosts.juno + path;
        },
        translate: function(code, params){
            var language_code = (this.language_code === 'kr'?'kr':'en');
            try{
                var message = this.language[language_code][code];
                if(params){
                    $.each(params, function(key, value){
                        var pattern = new RegExp('{{' + key + '}}', "g");
                        message = message.replace(pattern, value);
                    });
                }
                return message;
            }catch(e){
                return code;
            }
        }
    };
    window.ixcAccount = _ixcAccount;
    $(document).ready(function(){
        _ixcAccount.init();
    });
})(jQuery, window, document, screen);
