var graphObj = Object;
function _initGrph(_w){
	graphObj = {
		title: {
			text : "",
			style : {
				fontSize : "12px",
				fontWeight : "700",
				color : "#606060",
				fontFamily : "Nanum Gothic"
			}
		},
		subtitle: {
			text : "",
			style : {
				fontSize : "11px",
				fontFamily : "Nanum Gothic"
			}
		},
		chart : {
			renderTo: "",
			type: "",
			width: _w,
			height: 300
		},
		credits : {
			enabled : false
		},
		yAxis : {
			title: {
				text: ""
			}
		},
		xAxis : {
			categories : [],
			tickInterval:12,
			title: {
                enabled: true,
                text: 'time'
            },
		}, 
		legend: {
			itemStyle: {
				font: "11px Nanum Gothic",
				color: "#3E576F"
			},
			itemHoverStyle: {
				color: "black"
			},
			itemHiddenStyle: {
				color: "silver"
			}
		},
		tooltip : {
			formatter: function() { return "<b>"+ this.series.name +"</b><br/>"+ this.x +": "+ number_format(this.y); }
		},
		plotOptions: {
            series: {
                marker: {
                    enabled: false
                }
            }
        },
		series : [{
			data:[]			
		}],
		exporting: {
			enabled: false
		}
	}
}

function setGrph($targetObj , $data, $xtitle, $tickInterval, $inter) {
	_initGrph(_gw);

	$i = 0;
	graphObj.series[$i] = new Object;
	graphObj.series[$i].type = "line";
	graphObj.series[$i].name = $inter+" Input Traffic Max";
	graphObj.series[$i].data = new Array;

	$j = 1;
	graphObj.series[$j] = new Object;
	graphObj.series[$j].type = "line";
	graphObj.series[$j].name = $inter+" Output Traffic Max";
	graphObj.series[$j].data = new Array;

	graphObj.xAxis.title.text = $xtitle;
	graphObj.xAxis.tickInterval=$tickInterval;
	var l01 = ''
	if($xtitle=='day') l01 ='day';
	else if($xtitle=='day of week') l01 ='day of week';
	
	graphObj.tooltip.formatter= function() { return "<b>"+ this.series.name +"</b><br/><b>"+ this.x +l01+"</b>: "+ number_format(this.y); }
	
	graphObj.xAxis.categories = new Array;
	$($data).each(function (idx, data){
		var $inmax = ($inter=="Domestic") ?data.nTrafficInMax : data.nTrafficInMax2;
		var $outmax = ($inter=="Domestic") ?data.nTrafficOutMax : data.nTrafficOutMax2;
		graphObj.series[$i].data[idx] = $inmax!=null ? Number($inmax) : $inmax;
		graphObj.series[$j].data[idx] = $inmax!=null ? Number($outmax) : $outmax;
		graphObj.xAxis.categories[idx] = data.date_txt;
	});
	
	if($($targetObj).attr("id")=="day_graph_container") console.log(graphObj);
	$($targetObj).highcharts(graphObj);
}



$(document).ready(function() {
	_initGrph(_gw);
        /*org
	$("#searchFm").submit(function (){
		$seq = $("#searchFm select").val();
		if($seq) {
			$params = $(this).serializeArray();
			$.post("/en/stat/get_grph"+url_suffix, $params, function (response){
				setGrph("#day_graph_container" , response.min, "Time",12, "Domestic");
				setGrph("#week_graph_container" , response.week, "day of week",1, "Domestic");
				setGrph("#month_graph_container" , response.month, "day",1, "Domestic");
				$("#day_graph_container2").hide();
				$("#week_graph_container2").hide();
				$("#month_graph_container2").hide();
				if(response.inter_day===1){
					$("#day_graph_container2").show();
					setGrph("#day_graph_container2" , response.min, "Time",12, "International");
				}
				if(response.inter_week===1){
					$("#week_graph_container2").show();
					setGrph("#week_graph_container2" , response.week, "day of week",1, "International");
				}
				if(response.inter_month===1){
					$("#month_graph_container2").show();
					setGrph("#month_graph_container2" , response.month, "day",2, "International");
				}
				
				$("#day_graph_container, #week_graph_container, #month_graph_container").removeClass("null");
				
				if(response.html) {
					$("#day_graph_container").parent().find("div.graphdetail").html(response.html.daySum);
					$("#week_graph_container").parent().find("div.graphdetail").html(response.html.weekSum);
					$("#month_graph_container").parent().find("div.graphdetail").html(response.html.monthSum);
					$("div.graphdetail").show();
					
					$(".pop_stat").is(function(){
						$(".pop_stat div.pop_stat_day").html(response.html.day);
						$(".pop_stat div.pop_stat_week").html(response.html.week);
						$(".pop_stat div.pop_stat_month").html(response.html.month);
					});
					
				}
			},"json")

		} else {
			$("#day_graph_container, #week_graph_container, #month_graph_container").html("select.");
			$("#day_graph_container, #week_graph_container, #month_graph_container").addClass("null");
			$("div.graphdetail").hide().html('');
		}
		
		return false;
	});
        */
       $("#searchFm").submit(function (){
		$seq = $("#searchFm select").val();
		if($seq) {
			$params = $(this).serializeArray();
			$.post("/en/stat/get_grph"+url_suffix, $params, function (response){
                            //console.log('get graph response en'); console.log(response);
                            //190325
                            //response.inter_status : 0 - 국내만, 1 - 국제만, 2 - 국내+국제
                            
                            if(response.inter_status==0)
                            {
                                $("#day_graph_container").show();
				$("#week_graph_container").show();
				$("#month_graph_container").show();
                                setGrph("#day_graph_container" , response.min, "Time",12, "Domestic");
				setGrph("#week_graph_container" , response.week, "day of week",1, "Domestic");
				setGrph("#month_graph_container" , response.month, "day",1, "Domestic");
				$("#day_graph_container2").hide();
				$("#week_graph_container2").hide();
				$("#month_graph_container2").hide();
                                $("#day_graph_container, #week_graph_container, #month_graph_container").removeClass("null");
                            }
                            if(response.inter_status==1)
                            {
                                $("#day_graph_container2").show();
                                $("#week_graph_container2").show();
                                $("#month_graph_container2").show();
                                setGrph("#day_graph_container2" , response.min, "Time",12, "International");
                                setGrph("#week_graph_container2" , response.week, "day of week",1, "International");
                                setGrph("#month_graph_container2" , response.month, "day",2, "International");
                                $("#day_graph_container").hide();
				$("#week_graph_container").hide();
				$("#month_graph_container").hide();
                                $("#day_graph_container2, #week_graph_container2, #month_graph_container2").removeClass("null");
                            }
                            if(response.inter_status===2)
                            {
                                $("#day_graph_container").show();
				$("#week_graph_container").show();
				$("#month_graph_container").show();
                                setGrph("#day_graph_container" , response.min, "Time",12, "Domestic");
				setGrph("#week_graph_container" , response.week, "day of week",1, "Domestic");
				setGrph("#month_graph_container" , response.month, "day",1, "Domestic");
                                $("#day_graph_container, #week_graph_container, #month_graph_container").removeClass("null");
                                
                                $("#day_graph_container2").show();
                                $("#week_graph_container2").show();
                                $("#month_graph_container2").show();
                                setGrph("#day_graph_container2" , response.min, "Time",12, "International");
                                setGrph("#week_graph_container2" , response.week, "day of week",1, "International");
                                setGrph("#month_graph_container2" , response.month, "day",2, "International");
                                $("#day_graph_container2, #week_graph_container2, #month_graph_container2").removeClass("null");
                                
                            } 
				//상세정보를 보여주는 회원들(allowDetail 의 id에 해당하는 회원)
				if(response.html) {
					$("#day_graph_container").parent().find("div.graphdetail").html(response.html.daySum);
					$("#week_graph_container").parent().find("div.graphdetail").html(response.html.weekSum);
					$("#month_graph_container").parent().find("div.graphdetail").html(response.html.monthSum);
					$("div.graphdetail").show();
					
					$(".pop_stat").is(function(){
						$(".pop_stat div.pop_stat_day").html(response.html.day);
						$(".pop_stat div.pop_stat_week").html(response.html.week);
						$(".pop_stat div.pop_stat_month").html(response.html.month);
					});
					
				}
			},"json")

		} else {
			$("#day_graph_container, #week_graph_container, #month_graph_container").html("select.");
			$("#day_graph_container, #week_graph_container, #month_graph_container").addClass("null");
			$("div.graphdetail").hide().html('');
		}
		
		return false;
	});

	$("#searchFm").on("change","select, input",function (){
		$("#searchFm").submit();
	});
	
	$("#container").on("click",".btn_stat_detail",function(e) {
		e.preventDefault();
		$(".pop_stat_grid").hide();
		
		if($(this).hasClass("chkday")) $(".pop_stat_day").show();
		else if($(this).hasClass("chkmonth")) $(".pop_stat_month").show();
		else if($(this).hasClass("chkweek")) $(".pop_stat_week").show();
		$("#dialog-stat").dialog("open");
	});
	
});