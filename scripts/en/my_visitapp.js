function getRegistantInfo(value) {
	if(value == '') {
		$("#registant_companyName").text("");
		$("#registant_managerName").text("");
		$("#registant_internalPhone").text("");
		$("#registant_mobilePhone").text("");
	} else {
		$.post("/en/kinxidc/json/registant.html", {"nCompanyContactSeq":value}, function(data) {
			data = JSON.parse(data);
			$("#registant_companyName").text(data.sCompanyName);
			$("#registant_managerName").text(data.sManagerName);
			$("#registant_internalPhone").text(data.sInternalPhone);
			$("#registant_mobilePhone").text(data.sMobilePhone);
		});
	}
}

function getRegistantInfobyInit(value) { //150313
	if(value == '') {
		$("#registant_companyName").text("");
		$("#registant_managerName").text("");
		$("#registant_internalPhone").text("");
		$("#registant_mobilePhone").text("");
	} else {
		//console.log('getRegistantInfobyInit');
		$.post("/en/kinxidc/jsonbyInit/registant.html", {"nCompanyContactSeq":value}, function(data) {
			data = JSON.parse(data);
			$("#registant_companyName").text(data.sCompanyName);
			$("#registant_managerName").text(data.sManagerName);
			$("#registant_internalPhone").text(data.sInternalPhone);
			$("#registant_mobilePhone").text(data.sMobilePhone);
		});
	}
}

$(document).ready(function(){
	$("#workRequestForm").is(function(){
		$("#workRequestForm").submit(function() {
            if($(this).find("select[name=nManagerSeq]").val() == "") {
				alert("등록자를 선택해주세요.");
				return false;
			}
			if($(this).find("input[name='nCompanyContactSeq[]']:checked").val() == undefined) {
				alert("출입대상자를 선택해주세요.");
				return false;
			}
			if($(this).find("select[name=nWorkType]").val() == "") {
				alert("방문목적을 선택해주세요.");
				return false;
			}
			if($(this).find("input[name=dtExpectStartDate]").val() == "") {
				alert("방문예정 일자를 입력해주세요.");
				return false;
			}
			if($(this).find("input[name=dtExpectStartTime]").val() == "") {
				alert("방문예정 일시를 입력해주세요.");
				return false;
			}
			if($(this).find("input[name=dtExpectEndDate]").val() == "") {
				alert("방문예정 일자를 입력해주세요.");
				return false;
			}
			if($(this).find("input[name=dtExpectEndTime]").val() == "") {
				alert("방문예정 일시를 입력해주세요.");
				return false;
			}
		});
    });

	try {
		$(".dateYMD").datepicker({
			"dateFormat" : "yy-mm-dd",
			"selectOtherMonths" : true
		});
	} catch (e) {
		console.error(e);
	}
});