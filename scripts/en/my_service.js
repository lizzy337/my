$(document).ready(function() {
	$("a.memberChgBtn").click(function() {
		$type = $(this).attr("data-type");
		$.get("/en/service/competent/"+$type+url_suffix, function(response) {
			$("body").append(response);
			
			var maskH = $(document).height();
			var maskW = $(document).width();
			$('.layerMask').css({'width':maskW,'height':maskH});
			$('.layerMask').fadeTo('300',0.5);
			
			$(".service_detail").show();
		});
	});
	
	$("body").on("click",".btn_pop_close",function(e){
		$(".service_detail").hide();
		$('.layerMask').hide();
	});
});