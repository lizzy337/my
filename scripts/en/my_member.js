$(document).ready(function() {
    $(".btn_submit").on("click",function(){
        var _u = $(this).parents("form").attr("action");
        var _d = $(this).parents("form").serialize();
        $.post(_u, _d,function(data){
            alert(data.msg);
            if(data.code=='s') location.replace("/en/member/index");
        },"json");
    });

    //업무담당자, 요금담당자 수정 layer 띄우기
    $("a.btn_modify_AM").click(function() {
        var type = $(this).attr("data-type");
        var amseq = $(this).closest('tr').find('input[name="nAccountMemberSeq"]').val();
        console.log("btn_modify_AM] type : "+type);
        console.log("btn_modify_AM] amseq : "+amseq);
        $.get("/en/member/detailAM/"+type+"/"+amseq+url_suffix, function(response) {
            $("body").append(response);

            var maskH = $(document).height();
            var maskW = $(document).width();
            $('.layerMask').css({'width':maskW,'height':maskH});
            $('.layerMask').fadeTo('300',0.5);

            $(".service_detail").show();

            $(".btn_ac_submit").unbind().on("click",function(){
                $.post($(this).parents("form").attr("action"), $(this).parents("form").serialize(),function(data){},"json")
                .done(function(data ){
                    alert(data.msg);
                    if(data.code=='s') location.replace("/en/member");
                })
                .fail(function(data ) {
                    console.log(data);
                    alert(data.responseJSON.msg);
                });
            });
        });
    });


    //수정 레이어 submit
    $('form[name="frm_modifyAm"]').submit(function(){
        console.log('frm_modifyAm submit');
        document.frm_modifyAm.action = "/en/member/modifyAM";
        document.frm_modifyAm.submit();
    });


    //레이어 팝업 닫기
    $("body").on("click",".btn_pop_close",function(e){
        $(".service_detail").hide();
        $('.layerMask').hide();
    });
});