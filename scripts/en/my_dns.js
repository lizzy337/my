
//등록자 선택시 등록자 정보 설정
function getRegistantUserInfo(nCompanyContactSeq) 
{
	//console.log(nCompanyContactSeq);
	if(nCompanyContactSeq == '-1') {
		$("#sCompanyName").text('');
		$("#sManagerName").text("");
		$("#sRank").text("");
		$("#sInternalPhone").text("");
		$("#sfax").text("");
		$("#sMobilePhone").text("");
		$("#sEmail").text("");
	} else {
		$.post("/en/service/json/registantUser.html", {"nCompanyContactSeq":nCompanyContactSeq}, function(data) {			
			data = JSON.parse(data);
			$("#sCompanyName").text(data.sCompanyName);
			$("#sManagerName").text(data.sManagerName);
			$("#sRank").text(data.sRank);
			$("#sInternalPhone").text(data.sInternalPhone);
			$("#sfax").text(data.sfax);
			$("#sMobilePhone").text(data.sMobilePhone);
			$("#sEmail").text(data.sEmail);
		});    
	}
}

//보유 DNS리스트의 등록버튼.삭제요청
function delRegistant(nCompanyDnsSeq)//json 타지 않고 바로 DNS 신청리스트에 추가하기 
{
	var objForm= $("form[name='DnsRequestForm']");
	
	var sDns = $(objForm).find('#dnsList > #'+nCompanyDnsSeq).find('.dns').text();
	var sHost = $(objForm).find('#dnsList > #'+nCompanyDnsSeq).find('.host').text();
	var sIp = $(objForm).find('#dnsList > #'+nCompanyDnsSeq).find('.ip').text();
	var sCmt = $(objForm).find('#dnsList > #'+nCompanyDnsSeq).find('.comment').text();
	
	//내용추가
	$(objForm).find('#requestList').find('.request_list').removeAttr('style');
	var html = "<tr class=\"request_list\">";	
	html += "<td class=\"left\">"+"<input type=\"hidden\" name=\"req_dns[]\" value=\""+sDns+"\"/>"+sDns+"<input type=\"hidden\" name=\"nRequestDnsIdx[]\" value=\""+nCompanyDnsSeq+" \"/></td>";
	html += "<td class=\"left\">"+"<input type=\"hidden\" name=\"req_host[]\" value=\""+sHost+"\"/>"+sHost+"</td>";
	html += "<td class=\"left\">"+"<input type=\"hidden\" name=\"req_ip[]\" value=\""+sIp+"\"/>"+sIp+"</td>";
	html += "<td class=\"left\">"+"<input type=\"hidden\" name=\"req_cmt[]\" value=\""+sCmt+"\"/>"+sCmt+"</td>";
	html += "<td><input type=\"hidden\" name=\"worktype[]\" value=\"O\"/>"+"Delete"+"</td>";	
	html += "</tr>";
	//console.log(html);
	$(objForm).find('#requestList').append(html);
	
	//DNS 신청 리스트의 안내메시지 삭제
	$(objForm).find('#requestList').find('#no_request_list').css('display','none');
	
	//등록버튼 누른 항목을 제거
	$(objForm).find('#dnsList').find('#'+nCompanyDnsSeq).remove();
	
	//DNS 신청리스트에 추가되었다는 flag를 셋팅
	$(objForm).find('#strDNSChoice').val('Y');	
}

//신규 DNS 등록의 등록버튼
function addDNSInfo()
{
	//form check
	var objForm = $("form[name='DnsRequestForm']");
	var dnsname = $(objForm).find('#add_dnsinfo').find('#add_dns').val();
	var host = $(objForm).find('#add_dnsinfo').find('#add_host').val();
	var ip = $(objForm).find('#add_dnsinfo').find('#add_ip').val();
	var cmts = $(objForm).find('#add_dnsinfo').find('#add_cmt').val();
	if( !dnsname || !host || !ip || !cmts)
	{
		alert('Please enter information.');
		return false;
	}
	
	$(objForm).find('#requestList').find('.request_list').removeAttr('style');
	var html = "<tr class=\"request_list\">";	
	html += "<td class=\"left\">"+"<input type=\"hidden\" name=\"req_dns[]\" value=\""+dnsname+"\"/>"+dnsname+"<input type=\"hidden\" name=\"nRequestDnsIdx[]\" value=\"\"/></td>";
	html += "<td class=\"left\">"+"<input type=\"hidden\" name=\"req_host[]\" value=\""+host+"\"/>"+host+"</td>";
	html += "<td class=\"left\">"+"<input type=\"hidden\" name=\"req_ip[]\" value=\""+ip+"\"/>"+ip+"</td>";
	html += "<td class=\"left\">"+"<input type=\"hidden\" name=\"req_cmt[]\" value=\""+cmts+"\"/>"+cmts+"</td>";
	html += "<td><input type=\"hidden\" name=\"worktype[]\" value=\"I\"/>"+"Add"+"</td>";
	html += "</tr>";
	console.log(html);
	$(objForm).find('#requestList').append(html);
	
	//DNS 신청 리스트의 안내메시지 삭제
	$(objForm).find('#requestList').find('#no_request_list').css('display','none');
	
	//등록버튼 누른 항목의 값을 초기화.
	$(objForm).find('#add_dnsinfo').find('#add_dns').val('');
	$(objForm).find('#add_dnsinfo').find('#add_host').val('');
	$(objForm).find('#add_dnsinfo').find('#add_ip').val('');
	$(objForm).find('#add_dnsinfo').find('#add_cmt').val('');
	
	//DNS 신청리스트에 추가되었다는 flag를 셋팅
	$(objForm).find('#strDNSChoice').val('Y');
}

$(document).ready(function(){
	$("#DnsRequestForm").is(function(){
		$("#DnsRequestForm").submit(function(){
			console.log('mydns submit');
			if($(this).find("select[name='companycontact']").val() == -1 ){
				alert("Please select user.");
				return false;
			}
			if($(this).find('#strDNSChoice').val() == 'N'){
				alert('Please enter DNS information.');
				return false;
			}
			
			document.DnsRequestForm.action = "/en/Service/dnsRequest";
			document.DnsRequestform.submit();
		});
		
	});
});

