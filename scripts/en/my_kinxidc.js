$(document).ready(function() {
	$(".btn_submit").on("click",function(){
		var _u = $(this).parents("form").attr("action");
		var _d = $(this).parents("form").serialize();
		$.proc(_u, _d);
	});
	
	
	$(".btn_delete").on("click",function(event){
		event.preventDefault();
		var _u = $(this).attr("href");
		var _d = $(this).parents("form").serialize();
		var _t = $(this).attr("title");
		
		if(confirm("정말 "+_t+"하시겠습니까?")) $.proc(_u, _d);
		else return false;
	});
	
	$.proc = function(_u, _d){
		$.post(_u, _d,function(data){
			alert(data.msg);
			if(data.code=='s') location.replace(data.url);
		},"json");
	};
        
        
        $("#sCompanyType").unbind('change').on('change',function(){
            var sCType = $("#sCompanyType option:selected").val(); 
            document.location.href="/en/kinxidc/competent/type/"+sCType; //type 추가 by cin 20200313
        })
	
	
});