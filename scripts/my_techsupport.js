//등록자 선택시 등록자 정보 설정
function getRegistantUserInfo(nCompanyContactSeq)
{
	//console.log(nCompanyContactSeq);
	if(nCompanyContactSeq == '-1') {
		$("#sCompanyName").text('');
		$("#sManagerName").text("");
		$("#sRank").text("");
		$("#sInternalPhone").text("");
		$("#sfax").text("");
		$("#sMobilePhone").text("");
		$("#sEmail").text("");
	} else {
		$.post("/kinxidc/json/registant.html", {"nCompanyContactSeq":nCompanyContactSeq}, function(data) {
			data = JSON.parse(data);
			$("#sCompanyName").text(data.sCompanyName);
			$("#sManagerName").text(data.sManagerName);
			$("#sRank").text(data.sRank);
			$("#sInternalPhone").text(data.sInternalPhone);
			$("#sfax").text(data.sfax);
			$("#sMobilePhone").text(data.sMobilePhone);
			$("#sEmail").text(data.sEmail);
		});
	}
}

//작업구분 선택시
function selectWorkType(nWorkType)
{
	if( nWorkType == 3) //서버리부팅
	{
		document.getElementById('tr_confirm').style.display = '';
	}
	else
	{
		document.getElementById('tr_confirm').style.display = 'none';
	}
}

//160603 작업시간,날짜, 내용, 서버리부팅일때 주의문구 체크 부분 추가 및 수정
$(document).ready(function(){
	$("#techSupportForm_my").is(function(){
		$("#techSupportForm_my").submit(function() {
			console.log('techsupport _my submit');
            if($(this).find("select[name='companycontact']").val() == -1) {
				alert("등록자를 선택해주세요.");
				return false;
			}
			if($(this).find("select[name='techWorkType']").val() == -1) {
				alert("작업구분을 선택하세요");
				return false;
			}
			if($(this).find("input[name='dtExpectStartDate']").val() == "") {
				alert("작업예정 날짜를  입력해주세요.");
				return false;
			}
			if($(this).find("input[name='dtExpectStartTime']").val() == "") {
				alert("작업예정 시간을  입력해주세요.");
				return false;
			}
			//if($(this).find("#workCmt").val() == "") {
			if($(this).find("textarea[name='request']").val() == "") {
				alert("작업내용을  입력해주세요.");
				return false;
			}


			if($(this).find("select[name='techWorkType']").val() == 3) {//서버리부팅
				var chk = $(this).find("input[name='confirm_reboot']").is(":checked");
				if( chk==false)
				{
					alert("[주의]문구를 확인하세요");
					return false;
				}
			}

			document.techSupportForm_my.action = "/kinxidc/techsupportRequest";
			document.techSupportForm_my.submit();
			return false;

		});
    });

	try {
		$(".dateYMD").datepicker({
			"dateFormat" : "yy-mm-dd",
			"selectOtherMonths" : true
		});
	} catch (e) {
		console.error(e);
	}
});