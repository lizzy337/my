

function isActiveXOK(){
  if(lgdacom_atx_flag == true){
    document.getElementById('LGD_BUTTON1').style.display='none';
    document.getElementById('LGD_BUTTON2').style.display='';
  }else{
    document.getElementById('LGD_BUTTON1').style.display='';
    document.getElementById('LGD_BUTTON2').style.display='none';
  }
}



const array = new Uint16Array(1);
function makeOfferOrder(){
	let d             = new Date();
	let curr_msec     = d.getMilliseconds();
	let result        = window.crypto.getRandomValues(array);
	let strOfferOrder = d.getFullYear().toString() + (d.getMonth()+1).toString()+d.getDate().toString()+"_"+result.toString()+"_"+curr_msec.toString();
	return strOfferOrder;
}

function payment02_req_popup(form){
    window.open('/support/payment/payment02_req', 'paymentreq_popup', 'width=400,height=250,resizeable,scrollbars');
    form.target = 'paymentreq_popup';
}

function detail_colspan(){
    $("#taxinvoice_detail").rowspan_ex(2,'detail_info');
    $("#taxinvoice_detail").rowspan_ex(1,'detail_info');
}



var payment01 = {
    init:function(){
        payment01.valid($('form').eq(0));
    },
    valid:function(form){
        $(form).validate({
            submitHandler:function(form){
                return payment01.submit(form);
            }
            ,rules:{
                sCompanyName    : {required:true},
                sDepositName    : {required:true},
                sPaymentComment : {required:true},
                nPaymentPrice   : {required:true, number:true},
                sEmail          : {required:true, email:true},
                sPhone          : {required:true}
            },
            errorClass:"errMsg",
            errorPlacement : function(errMsg, element) {
                if(element.attr("name") == "sCompanyName"){ errMsg.insertAfter("#sCompanyName"); }
                if(element.attr("name") == "sDepositName"){ errMsg.insertAfter("#sDepositName"); }
                if(element.attr("name") == "sPaymentComment"){ errMsg.insertAfter("#errmsg1"); }
                if(element.attr("name") == "nPaymentPrice"){ errMsg.insertAfter("#errmsg2"); }
                if(element.attr("name") == "sEmail"){ errMsg.insertAfter("#sEmail"); }
                if(element.attr("name") == "sPhone"){ errMsg.insertAfter("#sPhone"); }
                if(element.attr("name") == "privacyassent" ){ errMsg.insertBefore("#privacyassent"); }
            },
            messages:{
                sCompanyName    : {required: " 업체명을 입력하세요."},
                sDepositName    : {required: " 입금자명을 입력하세요."},
                sPaymentComment : {required: " 결제사유를 입력하세요."},
                nPaymentPrice   : {required: " 납부금액을 입력하세요."},
                sEmail          : {required: " 이메일을 입력하세요."},
                sPhone          : {required: " 휴대폰번호를 입력하세요."}
            }
        });
    },
    submit:function (form){
        form.action = "/support/payment01_sendmail/send";
        form.submit();
        return false;
    }
}


var payment02={
    init:function(){
        payment02.valid($('form').eq(0));
    },
    valid:function(form){
        $(form).validate({
            rules:{
                strServiceTypeEtc : {required:true},
                LGD_AMOUNT        : {required:true, number:true },
                LGD_BUYER         : {required:true},
                LGD_DELIVERYINFO  : {required:true},
                LGD_BUYEREMAIL    : {required:true, email:true},
                LGD_BUYERPHONE    : {required:true},
            },
            errorClass:"errMsg",
            errorPlacement : function(errMsg, element) {
                if(element.attr("name") == "strServiceTypeEtc"){ errMsg.insertAfter("#strServiceTypeEtc"); }
                if(element.attr("name") == "LGD_AMOUNT"){ errMsg.insertAfter("#errmsg"); }
                if(element.attr("name") == "LGD_BUYER"){ errMsg.insertAfter("#LGD_BUYER"); }
                if(element.attr("name") == "LGD_DELIVERYINFO"){ errMsg.insertAfter("#LGD_DELIVERYINFO"); }
                if(element.attr("name") == "LGD_BUYEREMAIL"){ errMsg.insertAfter("#LGD_BUYEREMAIL"); }
                if(element.attr("name") == "LGD_BUYERPHONE"){ errMsg.insertAfter("#LGD_BUYERPHONE"); }
            },
            messages:{
                strServiceTypeEtc : {required: "결제사유를 입력하세요."},
                LGD_AMOUNT        : {required: "서비스요금을 입력하세요.(숫자만 가능)"},
                LGD_BUYER         : {required: "결제자명을 입력하세요."},
                LGD_DELIVERYINFO  : {required: "업체명을 입력하세요."},
                LGD_BUYEREMAIL    : {required: "이메일을 입력하세요."},
                LGD_BUYERPHONE    : {required: "휴대폰 번호를 선택하세요."},
            }
        });
    }
};

$.payemnt03=({
    init:function(){
        $('button.btn-cloud-detail').unbind('click').on('click', function(){
            let _qesbilldetail = ($(this).data('target'));
            $.get("/payment/clouddetail/"+ _qesbilldetail +url_suffix, function(response) {
                $(".cloud_detail").remove();

                let maskH = $(document).height();
                let maskW = $(document).width();
                let winH  = $(window).height();

                $("body").append(response);
                $('.div-detail-scroll').css('height', (winH-300));
                $('.layerMask').css({'width':maskW,'height':maskH});
                $('.layerMask').fadeTo('300',0.5);
                $('.cloud_detail').show();

                $(".btn_pop_close").unbind('click').on("click", function(e){
                    $('.cloud_detail').remove();
                    $('.layerMask').hide();
                });

                $('.btn_excel_download').on("click", function(e){
                    e.preventDefault;
                    let _fdata = {"qesdetailbill":_qesbilldetail};
                    $.payemnt03.dynamicForm('/payment/clouddetail_excel', _fdata, 'post', null);
                });
            });

        });
    },
    dynamicForm: function dynamicForm(url, data, method, target) {
        if (url && data) {
            data = typeof data == 'string' ? data : jQuery.param(data);
            var inputs = '';
            jQuery.each(data.split('&'), function () {
                var pair = this.split('=');
                inputs += '<input type="hidden" name="' + pair[0] + '" value="' + pair[1] + '" />';
            });
            target = (target != null) ? ' target="' + target + '" ' : '';
            jQuery('<form action="' + url + '" method="' + (method || 'post') + '" ' + target + '>' + inputs + '</form>')
                    .appendTo('body').submit().remove();
        }
    }
});

$(function () {
    $.payemnt03.init();
});


