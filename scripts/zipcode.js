var zipcode = {
    search:{
        init:function init(){
            var form = $('form[name="zipcode"]').get(0);
            zipcode.search.valid(form);
        },
        valid:function valid(form){
            var validOption = {
                ignore:'.ignore',
                debug:false,
                errorClass:'errMsg',
                submitHandler:function(form){
                   zipcode.search.action(form);
                },
                rules:{
                    word:{required:true}
                },
                messages:{
                    word:{required:'검색어를 입력해 주세요.'}
                },
                errorPlacement:function(error, element){
                    var n = element.attr("name");
                    var target = $(element).parent('.input_box');
                    target.after(error);
                }
            };
            var validator = $(form).validate(validOption);//end validate
            $(form).data('validator',validator);
        },
        action:function action(form){ //161114
            var target =  $('#zipcode_list');
            target.empty();
            $(form).ajaxSubmit({
               dataType:'json',
               success:function(data){
            	   console.log(data);
                   if(data.success)
                   {
                	   target.append('<ul style="margin:5px;">');
                	   
                	   var typeval = $(form).find('input[name="type"]').val();//0 : 도로명검색, 1 : 지번검색
                	   
                	   
                	   if(typeof data.results.cmmMsgHeader != 'undefined')
                	   {
                		   var _log = data.results.cmmMsgHeader;
                		   if(_log.successYN=='Y')
                		   {
                			   if(data.results.cmmMsgHeader.totalCount > 0 )
            				   {
                				   if(typeval==0)////도로명 검색
                				   {
                					  //console.log( data.results.newAddressListAreaCd );
                    				   var list = data.results.newAddressListAreaCd;
                    				   
                                       if($.isArray(list)==false){ //결과값이 1일때 object 반환
                                           list = [list]; 
                                       }
                                        
                                       $.each(list, function (i, v){                				   
                                           var item = v;
                                           console.log(v);
                                           var tag = "<li id='" + v.zipNo + "' address='" + v.lnmAdres + "'>" +  v.zipNo + "     "+ v.lnmAdres +  "</li>";                                   
                                           target.find('ul').append(tag);
                                       });
                				   }//end 도로명 검색
                				   else //지번검색
                				   {
                					   var list = data.results.detailListAreaCd;
                					   if($.isArray(list)==false){ //결과값이 1일때 object 반환
                                           list = [list]; 
                                       }
                					   $.each(list, function (i, v){                				   
                                           var item = v;
                                           console.log(v);
                                           var tag = "<li id='" + v.zipNo + "' address='" + v.adres + "'>" +  v.zipNo + "     "+ v.adres +  "</li>";                                   
                                           target.find('ul').append(tag);
                                       });
                				   }
            				   }
                			   else
            				   {
                				   var tag = "<li>※ 조회된 데이터가 없습니다.</li>";                                   
                                   target.find('ul').append(tag);
            				   }
                		   }
                		   else
                		   {
                			   alert(_log.errMsg);
                		   }     
                	   }//end  
                       target.find('li').css('cursor','pointer').on('click',function(){
                    	   
                           var ret = $(this).attr('id');
                           console.log(ret);
                           console.log($(this).attr('address'));
                            window.opener.document.getElementById('zip_code').value = (ret);
                            window.opener.document.getElementById('address_default').value = $(this).attr('address');
                            window.close();
                    	  
                       });
                       
                   }
                   else
                   {
                       if(data.error){
                           alert(data.error.message);
                       }else{
                           alert('알 수 없는 오류 입니다.');
                       }
                   }
               },
               error:function(data){
                    alert('오류가 발생하였습니다.');
               }
            });
            return false;
        }
        
        /*
        action:function action(form){
            var target =  $('#zipcode_list');
            target.empty();
            $(form).ajaxSubmit({
               dataType:'json',
               success:function(data){
                   if(data.success){
                	   target.append('<ul style="margin:5px;">');
                	   //도로명 검색
                	   if(typeof data.results.cmmMsgHeader != 'undefined')
                	   {
                		   var _log = data.results.cmmMsgHeader;
                		   if(_log.successYN=='Y'){
                			   console.log(Object.prototype.toString.call(data.newAddressListcd));
                			   if( Object.prototype.toString.call(data.results.newAddressList) === "[object Object]" )
                			   {//object로 변환하는 경우는 검색결과가 1개일때
                				   var item = data.results.newAddressList;
                				   var tag = "<li id='" + item.zipNo.replace("-","") + "' address='" + item.lnmAdres + "'>" + item.lnmAdres + "</li>";                                   
                                   target.find('ul').append(tag);
                			   }
                			   else
                			   {
                				   $.each(data.results.newAddressList, function (i, v){                				   
                                       var item = v;
                                       //var tag = "<li><a href='#' onclick=\"return zipcode.setAddress('" + item.postcd + "','" + item.address + "');\">" + item.address + "</a></li>";
                                       var tag = "<li id='" + item.zipNo.replace("-","") + "' address='" + item.lnmAdres + "'>" + item.lnmAdres + "</li>";                                   
                                       target.find('ul').append(tag);
                                   });
                			   }
                		   }
                		   else{
                			   alert(_log.errMsg);
                		   }
                	   }//end 도로명 검색
                	   //지번검색
                	   else{
                		   var list = data.results.itemlist.item;
                		   //if(Object.prototype.toString.call(list) !== '[Object Array]'){
                           //    list = [list];
                           //};
                           
                		   $.each(data.results.itemlist.item, function (i, v){
                               var item = v;
                               //var tag = "<li><a href='#' onclick=\"return zipcode.setAddress('" + item.postcd + "','" + item.address + "');\">" + item.address + "</a></li>";
                               var tag = "<li id='" + item.postcd + "' address='" + item.address + "'>" + item.address + "</li>";
                               target.find('ul').append(tag);
                           });
                	   }
                       
                       target.find('li').css('cursor','pointer').on('click',function(){
                           var reg= /(\d{3})(\d{3})/;
                           var ret = reg.exec(this.id);
                           console.log(ret);
                            window.opener.document.getElementById('zip_code').value = (ret[1] + "-" + ret[2]);
                            window.opener.document.getElementById('zipcode1').value = (ret[1]);
                            window.opener.document.getElementById('zipcode2').value = (ret[2]);
                            window.opener.document.getElementById('address_default').value = $(this).attr('address');
                            window.close();
                       });
                       
                   }else{
                       if(data.error){
                           alert(data.error.message);
                       }else{
                           alert('알 수 없는 오류 입니다.');
                       }
                   }
               },
               error:function(data){
                    alert('오류가 발생하였습니다.');
               }
            });
            return false;
        }
        */
    }
};