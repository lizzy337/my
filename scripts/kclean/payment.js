var payemnt=({
    form:null,
    orderid:'',
    init:function(){
        let _this = this;
        _this.form = $("#form-payment");
        _this.orderid = TossPayment.getOrderid();
        _this.setTossPayment();
        _this.formvalid();
    },
    setTossPayment:function(){
        let _this = this;
        TossPayment.getDefaultSet(
            function(){
                $("#span-payment-btn01").html('<button type="submit" id="ts-payment-btn">결제하기</button>');
            },
            function(){
                alert(msgelement.default_faild);
            }
        );
    },
    formvalid:function(){
        let _this = this;
        let _form = _this.form;

        $.validator.setDefaults({
            onkeyup: false,
            onclick: false,
            onfocusout: false,
            showErrors: function(errorMap,errorList){
                if(this.numberOfInvalids()){ // 에러가 있으면
                    alert(errorList[0].message); // 경고창으로 띄움
                }
            }
        });

        var validator = _form.validate({
            submitHandler:function(_form){
                if($("#chkpayment").is(":checked") != true){
                    alert("결제할 서비스 내용을 확인하시고, 동의해 주세요.");
                    $("#chkpayment").focus();
                }
                else{
                    _this.payemntDataSend();
                }
                return false;
            },
            ignore:[],
            rules:{
                ts_ordername:     {required:true},
                ts_productinfo:   {required:true},
                ts_amount:        {required:true, number:true},
                ts_buyer:         {required:true},
                ts_customername:  {required:true},
                ts_customeremail: {required:true, email:true},
                ts_customermobile:{required:true, number:true, minlength:10}
            },
            errorPlacement : function(errMsg, element) {},
            messages:{
                ts_ordername:     {required:msgelement.required.ts_ordername},
                ts_productinfo:   {required:msgelement.required.ts_productinfo},
                ts_amount:        {required:msgelement.required.ts_amount,
                                   number:msgelement.valid.ts_customeremail_number},
                ts_buyer:         {required:msgelement.required.ts_buyer},
                ts_customername:  {required:msgelement.required.ts_customername},
                ts_customeremail: {required:msgelement.required.ts_customeremail,
                                   email:msgelement.valid.ts_customeremail_email},
                ts_customermobile:{required:msgelement.required.ts_customermobile,
                                   number:msgelement.valid.ts_customermobile_number,
                                   minlength:msgelement.valid.ts_customermobile_minlength}
            }
        });

        validator.element($('#ts_ordername'));
        validator.element($('#ts_productinfo'));
        validator.element($('#ts_amount'));
        validator.element($('#ts_buyer'));
        validator.element($('#ts_customername'));
        validator.element($('#ts_customeremail'));
        validator.element($('#ts_customermobile'));
    },
    payemntDataSend:function()
    {
        var _this = this,
            _url  = window.location.origin+msgelement.cardposturl,
            _data = _this.form.serialize()
        ;
        _data += "&orderid="+_this.orderid //add ordderid

        $.post(_url, _data, function(){},'json')
        .done(function (_response) {
            if (_response.status) {
                let _addparam = $.param(_response.approvaldata);
                _addparam += "&skcleanid="+$("#skcleanid").val();

                let _paymentdata = {
                    amount: $("#ts_amount").val(),
                    orderId: _this.orderid,
                    orderName: $("#ts_ordername").val(),
                    customerName: $("#ts_customername").val(),
                    customerEmail: $("#ts_customeremail").val(),
                    customerMobilePhone: $("#ts_customermobile").val(),
                    successUrl: window.location.origin + '/kclean/request/request_step3/success?'+_addparam,
                    failUrl: window.location.origin + '/kclean/request/request_step3/fail?'+_addparam,
                };

                //console.log(_paymentdata);

                TossPayment.sendTossPayment(_paymentdata, 'CARD');
                return false;
            }
            else { alert(_response.error_msg+'['+_response.error_cd+']'); return false;}
        })
        .fail(function (data) {
            alert(msgelement.post.faild01);
            return false;
        });
    }
});

$(function () {
    payemnt.init();
});