var msgelement =  new Object();

msgelement.cardposturl = '/tosspayment/card';
msgelement.default_faild = "카드결제 기본정보가 설정되지 못하였습니다.\n고객센터에 문의 바랍니다.";
msgelement.chk_agree = "결제할 서비스 내용을 확인하시고, 동의해 주세요.";
msgelement.required = {
    "ts_ordername":      "서비스 구분을 선택해주세요.",
    "ts_productinfo":    "결제사유를 입력해주세요.",
    "ts_amount":         "서비스요금을 선택해주세요.",
    "ts_buyer":          "결제자명을 입력해주세요.",
    "ts_customername":   "업체명을 입력해주세요.",
    "ts_customeremail":  "이메일를 입력해주세요.",
    "ts_customermobile": "휴대폰 번호를 입력해주세요"
};
msgelement.valid = {
    "ts_customeremail_email":     "정확한 이메일 형식으로 입력해주세요.",
    "ts_amount_number":           "숫자형식으로 입력해주세요.",
    "ts_customermobile_number":   "0~9 숫자로 휴대폰 번호를 입력해주세요.",
    "ts_customermobile_minlength":"정확한 휴대폰 번호를 입력해주세요."
};
msgelement.post = {
    faild01: "신용카드 결제요청이 실패하였습니다. 다시 시도해주세요.\n지속적으로 발생되면 고객센터에 연락주세요.[error:SPP01]",
    faild02: "신용카드 결제요청이 실패하였습니다. 다시 시도해주세요.\n지속적으로 발생되면 고객센터에 연락주세요.[error:SPP02]",
    success: ""
}

