var cloud_join = {
    init:function init(){
        var path = location.pathname;
        //var path = "/cloud/join";
        var patternAuth = /\/cloud\/(.)*(\/)?auth((\/|\.html)?)$/; //cloud/ixServer/auth/
        var patternJoinStep1 = /\/cloud\/(.)*(\/)?join((\/|\.html)?)$/; //cloud/ixServer/join/
        var patternFindPassword = /\/cloud\/(.)*(\/)?account\/findpassword((\/|\.html)?)$/; //cloud/ixServer/join/findpassword/
        var patternFindPasswordMy = /\/mypage\/auth\/findidpw((\/|\.html)?)$/;
        var patternEdit = /\/mypage\/member\/cloud((\/|\.html)?)$/;
        
        //console.log(paternJoinStep1.test(path));
        if(patternJoinStep1.test(path)){//회원가입 step1
            var form = $('form#cloudJoinForm').eq(0);
            form.find('input[name="user_type"]').on('change',function(){
                var val = $(this).val();
                if(val != 'A'){
                    $(form).find('.corp_row').show();
                    $(form).find('.corp_row').find('input,select').removeClass('ignore');
                }else{
                    $(form).find('.corp_row').hide();
                    $(form).find('.corp_row').find('input,select').addClass('ignore');
                }
            });
            form.find('input#userid').on('change', function(){
                $('#check_email').val(0);
            });
            cloud_join.getCaptcha();
            formcheck_cloud.join_step1.init(form);
        }else if(patternAuth.test(path)){
            var form = $('form[name="loginForm"]').eq(0);
            formcheck_cloud.auth.init(form);
        }else if(patternFindPassword.test(path) || patternFindPasswordMy.test(path)){
            var form = $('form[name="findpasswordForm"]').eq(0);
            formcheck_cloud.findpassword.init(form);
            $('#get_sms_code').click(function(){
               
                var form = $(this).closest('form');
                var param = form.formSerialize();
                $.ajax({
                    type:'POST',
                    data:param,
                    dataType:'JSON',
                    success:function(data){
                        if(data.success){
                            alert('인증 번호가 발송 되었습니다.');
                            form.find('input[name="flag"]').val(1);
                            form.find('input').attr('readonly','readonly');
                            form.find('input[name="keychk"]').val('1');
                            form.find('input[name="auth_no"]').removeAttr('readonly');
                        }else{
                            alert('일치하는 정보를 찾을 수 없습니다.');
                        }
                    }
                });
                return false;
            });
        }else if(patternEdit.test(path)){
            var form = $('form[name="cloudUserForm"]');
            formcheck_cloud.editAccount.init(form);
        }
    },
    checkEmailDupl:function checkEmailDupl(form, ele){
        var form = $('#cloudJoinForm');
        var ele = $('#userid')
        var val = ele.val();
        var validator = form.data('validator');
        if(validator.element('#userid')){
            $.ajax({
                dataType:'json',
                url:'/util/cloud/checkmail?email=' + val,
                success:function(data){
                    var dupl = data.result;
                    if(dupl > 0){
                        alert('이미 사용중인 이메일입니다');
                        $('#check_email').val(0);
                    }else{
                        alert('사용 가능한 이메일 입니다.');
                        $('#check_email').val(1);
                    }
                }
            });            
        };
        return false;
     },
    cardReg:{ //LG Dacom Card 정보 입력
        action:function cardRegAction(param){
            $.ajax({
                dataType:'json',
                url:'/cloud/join/card',
                type:'post',
                data:param,
                success:function(data){
                    if(data.status=='200'){
                        console.log('here');
                    }else{
                        alert('결제정보가 정상적으로 등록되지 않았습니다');
                    }
                }
            })
            
        }
    },
    getCaptcha:function getCapcha(){
        $.ajax({
            datatype:'json',
            url:'/util/captcha/',
            success:function(data){
                $('#captcha_time').val(data.time);
                $('#captchaimg').attr('src','/captcha_img/' + data.filename);
            }
        });
        return false;
    },
    auth:{
        login:function login(form){
            form.action = 'https://auth.ixcloud.net/login';
            form.next = 'https://dashboard.ixcloud.net';
            form.target = '_blank';
            form.submit();
           
        }
    }
}
$(document).ready(function(){
    cloud_join.init();
});

