/* 
 * @since version 1.0
 * @desc 공용 js
 */
$(document).ready(function($) {
	
	
	/* ---------------------------------------------------- */
	/* layout seting
	/* ---------------------------------------------------- */
	 
	
	$( document ).tooltip({
	      items: "[memo]",
	      content: function() {
	        var element = $( this ); 
	        if ( element.is( "[memo]" ) ) {
	        	var m = element.attr( "memo" ); 
	          return element.attr( "memo" );
	        }
	      },
	      tooltipClass: "entry-tooltip-positioner",
	      position: {
	          my: "center bottom-20",
	          at: "center top",
	          using: function( position, feedback ) {
	        	  console.log(position); 
	            $( this ).css( position );
	            $( "<div>" )
	              .addClass( "arrow" ) 
	              .addClass( feedback.vertical )
	              .addClass( feedback.horizontal )
	              .appendTo( this );
	          }
	        }
	    });
	
});

/**
 * 콤마 붙이기 함수
 * @param value int
 */
function setComma(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

/**
 * 콤마 제거 함수
 * @param value int
 */
function RemoveComma(str) {
    return parseInt(str.replace(/,/g, ""));
}

function RemoveCommaFloat(str) {
    return parseFloat(str.replace(/,/g, ""));
}

/**
 * 숫자입력
 * @param value int
 */
$(document).on("keyup", ".numberonly", function() {
	$(this).numeric({
	    allowMinus   : true,
	    allowThouSep : true
	});
	$(this).focusout(function() { $(this).val($(this).val().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")); });
	$(this).focusin(function() { $(this).val($(this).val().replace(/\,/g, '')); });
 });

 
/**
 * 앞에 '0' 붙이기
 * @param i
 * @returns
 */
function addZero(i){
	var rtn = i + 100;
	return rtn.toString().substring(1,3);
}


function chr_byte(chr){
    if(escape(chr).length > 4)      return 2;
    else                            return 1;
}    

/**
 * 문자열 자르기
 * @param str
 * @param limit
 * @returns {String}
 */
function cutStr(str, limit){
    var tmpStr = str;
    var byte_count = 0;
    var len = str.length;
    var dot = "";

    for(i=0; i<len; i++){
        byte_count += chr_byte(str.charAt(i)); 
        if(byte_count == limit-1){
            if(chr_byte(str.charAt(i+1)) == 2){
                tmpStr = str.substring(0,i+1);
                dot = "...";
            }
            else {
                if(i+2 != len) dot = "...";
                tmpStr = str.substring(0,i+2);
            }
            break;
        }
        else if(byte_count == limit){
            if(i+1 != len) dot = "...";
            tmpStr = str.substring(0,i+1);
            break;
        }
    }
    return (tmpStr+dot);
}

/**
 * console.log
 */
if(window.console==undefined){console={log:function(){}};}
