var estimate={
	init:function(){ 
		estimate.valid($('form').eq(0));
	}
	,valid:function(form){
		$(form).validate({
	        submitHandler:function(form){
	        	return estimate.submit(form);
	        },//end submitHandler
	        rules:{	
	        	sCompanyName 		: {required:true},
	        	sDepartment 		: {required:true},
	        	sName 				: {required:true},
	        	sRank 				: {required:true},
	        	sInternalPhone 		: {required:true},
	        	sMobilePhone 		: {required:true},
	        	sEmail 				: {required:true, email:true},
	        	privacyassent		: {required:true},
	        	/*
	        	nRackQuantity : {
	        		required:{
	        			depends: function()
	        			{
	        				$tmp1 = $('input[name=nRackQuantity]').val();
	        				$tmp2 = $('input[name=nQuarterRackQuantity]').val();
	        				$tmp3 = $('input[name=nHalfRackQuantity]').val();
	        				$tmp4 = $('input[name=nFullRackQuantity]').val();
	        				$sum = $tmp1 + $tmp2 + $tmp3 + $tmp4;
	        				$result = ( $sum.length == 0 )? true : false;
	        				return $result;
	        			}
	        		},
	        		number:true
	        	},
	        	nQuarterRackQuantity : {
	        		required:{
	        			depends: function()
	        			{
	        				$tmp1 = $('input[name=nRackQuantity]').val();
	        				$tmp2 = $('input[name=nQuarterRackQuantity]').val();
	        				$tmp3 = $('input[name=nHalfRackQuantity]').val();
	        				$tmp4 = $('input[name=nFullRackQuantity]').val();
	        				$sum = $tmp1 + $tmp2 + $tmp3 + $tmp4;
	        				$result = ( $sum.length == 0 )? true : false;
	        				return $result;
	        			}
	        		},
	        		number:true
	        	},
	        	nHalfRackQuantity : {
	        		required:{
	        			depends: function()
	        			{
	        				$tmp1 = $('input[name=nRackQuantity]').val();
	        				$tmp2 = $('input[name=nQuarterRackQuantity]').val();
	        				$tmp3 = $('input[name=nHalfRackQuantity]').val();
	        				$tmp4 = $('input[name=nFullRackQuantity]').val();
	        				$sum = $tmp1 + $tmp2 + $tmp3 + $tmp4;
	        				$result = ( $sum.length == 0 )? true : false;
	        				return $result;
	        			}
	        		},
	        		number:true
	        	},
	        	nFullRackQuantity : {
	        		required:{
	        			depends: function()
	        			{
	        				$tmp1 = $('input[name=nRackQuantity]').val();
	        				$tmp2 = $('input[name=nQuarterRackQuantity]').val();
	        				$tmp3 = $('input[name=nHalfRackQuantity]').val();
	        				$tmp4 = $('input[name=nFullRackQuantity]').val();
	        				$sum = $tmp1 + $tmp2 + $tmp3 + $tmp4;
	        				$result = ( $sum.length == 0 )? true : false;
	        				return $result;
	        			}
	        		},
	        		number:true
	        	},
	        	nBandwidthQuantity 			: {required:true, number:true},
	        	nTotalManageQuantity 		: {required:true, number:true},
	        	nDDosQuantity 				: {required:true, number:true},
	        	nSecureQuantity				: {required:true, number:true},
	        	nEm7Quantity				: {required:true, number:true},
	        	nAspOsQuantity				: {required:true, number:true},
	        	nAspBbQuantity				: {required:true, number:true},
	        	nBackupQuantity				: {required:true, number:true},
	        	*/
			},
			errorClass:"errMsg",
			errorPlacement : function(errMsg, element) {
				/*
				if( element.attr("name") == "nRackQuantity"){ errMsg.insertAfter("#errmsg1"); }
				if( element.attr("name") == "nQuarterRackQuantity"){ errMsg.insertAfter("#errmsg2"); }
				if( element.attr("name") == "nHalfRackQuantity"){ errMsg.insertAfter("#errmsg3"); }
				if( element.attr("name") == "nFullRackQuantity"){ errMsg.insertAfter("#errmsg4"); }
				if( element.attr("name") == "nBandwidthQuantity"){ errMsg.insertAfter("#errmsg5"); }
				if( element.attr("name") == "nTotalManageQuantity"){ errMsg.insertAfter("#errmsg6"); }
				if( element.attr("name") == "nDDosQuantity"){ errMsg.insertAfter("#errmsg7"); }
				if( element.attr("name") == "nSecureQuantity"){ errMsg.insertAfter("#errmsg8"); }
				if( element.attr("name") == "nEm7Quantity"){ errMsg.insertAfter("#errmsg9"); }
				if( element.attr("name") == "nAspOsQuantity"){ errMsg.insertAfter("#errmsg10");}
				if( element.attr("name") == "nAspBbQuantity"){ errMsg.insertAfter("#errmsg11");}
				if( element.attr("name") == "nBackupQuantity"){ errMsg.insertAfter("#errmsg12");}
				*/
				if( element.attr("name") == "privacyassent" ){ errMsg.insertBefore("#privacyassent"); }
			},
			messages:{
				sCompanyName			: {required: " 업체명을 입력하세요"},
				sDepartment				: {required: " 부서를 입력하세요"},
				sName					: {required: " 신청인을 입력하세요"},
				sRank					: {required: " 직함을 입력하세요"},
				sInternalPhone			: {required: " 일반전화를 입력하세요"},
				sMobilePhone			: {required: " 휴대전화를 입력하세요"},
				sEmail					: {required: " 이메일을 입력하세요"},
				/*
				nRackQuantity			: {required: " 반드시 입력(숫자)"},
				nQuarterRackQuantity	: {required: " 반드시 입력(숫자)"},
				nHalfRackQuantity		: {required: " 반드시 입력(숫자)"},
				nFullRackQuantity		: {required: " 반드시 입력(숫자)"},
				nBandwidthQuantity		: {required: " 반드시 입력(숫자)"},
				nTotalManageQuantity	: {required: " 반드시 입력(숫자)"},
				nDDosQuantity			: {required: " 반드시 입력(숫자)"},
				nSecureQuantity			: {required: " 반드시 입력(숫자)"},
				nEm7Quantity			: {required: " 반드시 입력(숫자)"},
				nAspOsQuantity			: {required: " 반드시 입력(숫자)"},
				nAspBbQuantity			: {required: " 반드시 입력(숫자)"},
				nBackupQuantity			: {required: " 반드시 입력(숫자)"},
				*/
				privacyassent			: {required: "개인정보 수집 이용에 동의해주세요. "},
			}
	      });//end validate
	}//end valid
	,submit:function (form){
		//console.log("estimate submit");
		var type = $('#sEstimationType').val();
		if(type=='C')
		{
			form.action = "/idc/colocation/estimate_proc";
		}	
		else
		{
			form.action = "/idc/serverhosting/estimate_proc";
		}
		//console.log('type : '+type);
		//console.log('action : '+ form.action);
	    form.submit();
	    return false;
	  }//end submit
}; //end estimate

