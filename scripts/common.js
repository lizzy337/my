
/*
Author: GABIA
Author URL: http://www.gabia.com
Creation Date : 2013-11-22
Modify Date : 2014-02-17
*/

$(function() {
	// layout seting-----------------------------------------
	$("#dialog-phoninfo").is(function(){
		$("#dialog-phoninfo").dialog({
			autoOpen : false,
			height : "auto",
			width : 650,
			modal : true,
			resizable : false
		});
	});

	$("#dialog-stat").is(function(){
		$("#dialog-stat").dialog({
			autoOpen : false,
			height : "auto",
			width : 850,
			modal : true,
			resizable : false
		});
	});

});

function getInternetExplorerVersion()
// Returns the version of Internet Explorer or a -1
// (indicating the use of another browser).
{
  var rv = -1; // Return value assumes failure.
  if (navigator.appName == 'Microsoft Internet Explorer')
  {
    var ua = navigator.userAgent;
    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
    if (re.exec(ua) != null)
      rv = parseFloat( RegExp.$1 );
  }
  return rv;
}

/* jQuery */
$(function() {
	/* GNB 메뉴 - 2depth 확장 */
	$.fn.nav = function(args){
		var elkha = jQuery.extend({
			show : {height:"show"},
			hide : {opacity:"hide"},
			speed: "fast",
			delay: 0,
			click: false
		}, args);

		// ieSelect
		var ieSelect = false;
		//if($.browser.msie == true) ieSelect = parseInt($.browser.version);
		//jquery 1.9 에후에는 msie 쓰이지 않음.
		var browser = navigator.userAgent.toLowerCase();
		var version = navigator.appVersion;
		if ( -1 != browser.indexOf('msie') )
		{
			ieSelect = getInternetExplorerVersion();
		}

		this.each(function(){
			var gnb = $(this);

			gnb.find("li:has(div)").hover(function(){
				$(this).addClass("hover");
				if(elkha.click && $(this).children("a").attr("href")=="#") return;
				if(elkha.speed==0 || (ieSelect && ieSelect <= 6)) $(this).children("div").show();
				else $(this).children("div:not(:animated)").animate(elkha.show, elkha.speed);
			}, function(){
				$(this).removeClass("hover");
				if(elkha.click && $(this).children("a").attr("href")=="#") return;
				var hover = $(this);
				setTimeout(function(){
					if(hover.hasClass("hover")) return;
					if(elkha.speed==0 || (ieSelect && ieSelect <= 6)) hover.children("div").hide();
					else hover.children("div").animate(elkha.hide, elkha.speed);
				}, elkha.delay);
			});
			gnb.find("a").focus(function(){
				$(this).parent("li").prev("li").find("div").hide();
				$(this).parent("li").next("li").find("div").hide();
				if(elkha.click && $(this).attr("href")=="#") return;
				$(this).next("div").show();
			});
			gnb.find("a").click(function(){
				if($(this).attr("href")=="#"){
					if(elkha.click){
						if($(this).next("div").css("display")=="block") $(this).next("div").hide();
						else $(this).next("div").show();
					}
					return false;
				}
			});
		});
		return this;
	};
	$(document).ready(function(){
		$(".lnb").nav({delay:0});
	});
});

$(document).ready(function() {
	$("a.js-confirm").click(function (){
		$msg = $(this).attr("data-msg");
		if(confirm($msg)) {
			return true;
		} else {
			return false;
		}
	});

	/*
	Author: GABIA
	Author URL: http://www.gabia.com
	Creation Date : 2013-11-22
	Modify Date : 2013-12-09
	*/
	$('a[href^="#"]').on('click', function(e){
		var anchorid = $(e.currentTarget);
		var select_id = $(anchorid.attr('href'));
		select_id.attr("tabindex","-1").css('outline','0').focus();
		select_id.on('focusout',function(){
			$(this).removeAttr("tabindex");
		});
	});
	/*
	// 모달 레이어
	$(".layerLink").click(function() {
		var maskH = $(document).height();
		var maskW = $(document).width();
		var lyrH = $(window).height();
		var lyrW = $(window).width();
		var lyr = $(this).attr("title");
		$('html').css("overflow","hidden");
		$('.layerMask').css({'width':maskW,'height':maskH});
		$('.layerMask').fadeTo('300',0.5);
		$('.'+lyr).css('top', $(window).scrollTop()+lyrH/2-$('.'+lyr).height()/2);
		$('.'+lyr).css('left', lyrW/2-$('.'+lyr).width()/2).show();
	});

	$("body").on( "click", ".layer .close", function() {
		$type = $(this).attr("data-hide-type");
		$('html').css("overflow","auto");
		if($type == "remove") {
			$('.layerMask, .layer').remove();
		} else {
			$('.layerMask, .layer').hide();
		}
	});
	*/

	/* 탭 메뉴 */
	$(".tab_menu li").click(function () {
		$(".tab_menu li").removeClass("active");
		$(this).addClass("active");
		var $boxVar = $(this).index() + 1;
		$(".tab_sub").css("display","none");
		$(".tab_box"+$boxVar).css("display","block");
	});

	$(".tab_menu2 li").click(function () {
		$(".tab_menu2 li").removeClass("active");
		$(this).addClass("active");
		var $boxVar = $(this).index() + 1;
		$(".tab_sub2").css("display","none");
		$(".tab_box2"+$boxVar).css("display","block");
	});




	$(".tab2_menu li").click(function () {
		$(".tab2_menu li").removeClass("active");
		$(this).addClass("active");
		var $boxVar = $(this).index() + 1;
		$(".tab2_sub").css("display","none");
		$(".tab2_box"+$boxVar).css("display","block");
	});

	/* 서브 인덱스 펼침/닫힘 */
	var $listEI = $(".consult");
	$listEI.find("dt").bind('click', function(){
		$listEI.find("dd").hide();
		$listEI.find("dt").removeClass("active");
		$(this).addClass("active");
		$(this).next().show();
	});

	/* 박스 테두리 변경 */
    $(".product").children().each(function(idx){
        $(this).mouseover(function(){
            $(".product dl").removeClass("onborder");
            $(this).addClass("onborder");
        });
		$(this).mouseout(function(){
			$(".product dl").removeClass("onborder");
		 });
    });
	$(".product2").each(function(idx){
        $(this).mouseover(function(){
            $(".product2").removeClass("onborder");
            $(this).addClass("onborder");
        });
		$(this).mouseout(function(){
			$(".product2").removeClass("onborder");
		 });
    });

	$(".kclean_product").children().each(function(idx){
        $(this).mouseover(function(){
            $(".kclean_product dl").removeClass("onborder");
            $(this).addClass("onborder");
        });
		$(this).mouseout(function(){
			$(".kclean_product dl").removeClass("onborder");
		 });
    });


	/* 레이어 보이기/감추기 */
	$('.email').mouseover(function() { // 마우스 오버시 좌표를 얻고, 다음 코드를 실행한다.
		$('#layerEmail').show(); // 레이어를 보여준다.
	});
	$('.email').mouseout(function() { // 마우스 아웃시 실행한다.
		$('#layerEmail').hide(); // 레이어를 숨긴다.
	});
	$('.pass').mouseover(function() {
		$('#layerPass').show();
	});
	$('.pass').mouseout(function() {
		$('#layerPass').hide();
	});
	/* 레이어 보이기/감추기 - K clean 신청 페이지 tooltip */
	$('#layer_kclean_global').mouseover(function() {
		$('#layer_kclean_globalinfo').show();
	});
	$('#layer_kclean_global').mouseout(function() {
		$('#layer_kclean_globalinfo').hide();
	});
	$('#layer_kclean_domain').mouseover(function() {
		$('#layer_kclean_domaininfo').show();
	});
	$('#layer_kclean_domain').mouseout(function() {
		$('#layer_kclean_domaininfo').hide();
	});
	$('#layer_kclean_money').mouseover(function() {
		$('#layer_kclean_moneyinfo').show();
	});
	$('#layer_kclean_money').mouseout(function() {
		$('#layer_kclean_moneyinfo').hide();
	});

	/* 레이어 팝업 */
	$(window).load(function(e) {
		$(".layerLink").click(function(e) {
			e.preventDefault();
			var maskH = $(document).height();
			var maskW = $(document).width();
			var lyrH = $(window).height();
			var lyrW = $(window).width();
			var lyr = $(this).attr("title");
			$('html').css("overflow","hidden");
			$('.layerMask').css({'width':maskW,'height':maskH});
			$('.layerMask').fadeTo('300',0.5);
			$('.'+lyr).css('top', $(window).scrollTop()+lyrH/2-$('.'+lyr).height()/2);
			$('.'+lyr).css('left', lyrW/2-$('.'+lyr).width()/2).show();
			$('.close').click(function () {
				$('html').css("overflow","auto");
				$('.layerMask,.'+lyr).hide();
			});
		});
		$(".layerLink2").click(function(e) {
			e.preventDefault();
			$("#dialog-phoninfo").dialog("open");
		});
		$(".sitemapLink").click(function() {
			var maskH = $(document).height();
			var maskW = $(document).width();
			var lyrH = $(window).height();
			var lyrW = $(window).width();
			var lyr = $(this).attr("title");
			$('.layerMask').css({'width':maskW,'height':maskH});
			$('.layerMask').fadeTo('300',0.5);
			$('.'+lyr).css('left', lyrW/2-$('.'+lyr).width()/2).show();
			$('.close').click(function () {
				$('.layerMask,.'+lyr).hide();
			});
		});
	});


	try {
		$(".dateYMD").datepicker({
			showOn: 'both', buttonImage: '/images/include/ico_date.gif', buttonImageOnly: true,
			"dateFormat" : "yy-mm-dd",
			"selectOtherMonths" : true
		});

		$(".dateYMDStart").is(function(){
			$(".dateYMDStart").datepicker({
			    showOn: 'both', buttonImage: '/images/include/ico_date.gif', buttonImageOnly: true,
		        onClose : function(selectedDate) {
	                $(".dateYMDEnd").datepicker("option", "minDate", selectedDate);
	            }
		    });
		});

		$(".dateYMDEnd").is(function(){
			$(".dateYMDEnd").datepicker({
			    showOn: 'both', buttonImage: '/images/include/ico_date.gif', buttonImageOnly: true,
		        onClose : function(selectedDate) {
	                $(".dateYMDStart").datepicker("option", "maxDate", selectedDate);
	            }
		    });
		});


	} catch (e) {
		console.error(e);
	}

	/*휴대폰+일반전화Mix 맨 앞자리*/
	var phone_first_list ='';
	//hp_first_list += '<option value="" selected="selected"></option>';
	phone_first_list += '<option value="010" selected="selected">010</option>';
	phone_first_list += '<option value="011">011</option>';
	phone_first_list += '<option value="016">016</option>';
	phone_first_list += '<option value="017">017</option>';
	phone_first_list += '<option value="018">018</option>';
	phone_first_list += '<option value="019">019</option>';
	phone_first_list += '<option value="02" >02</option>';
	phone_first_list += '<option value="031">031</option>';
	phone_first_list += '<option value="032">032</option>';
	phone_first_list += '<option value="033">033</option>';
	phone_first_list += '<option value="041">041</option>';
	phone_first_list += '<option value="042">042</option>';
	phone_first_list += '<option value="043">043</option>';
	phone_first_list += '<option value="044">044</option>';
	phone_first_list += '<option value="051">051</option>';
	phone_first_list += '<option value="052">052</option>';
	phone_first_list += '<option value="053">053</option>';
	phone_first_list += '<option value="054">054</option>';
	phone_first_list += '<option value="055">055</option>';
	phone_first_list += '<option value="061">061</option>';
	phone_first_list += '<option value="062">062</option>';
	phone_first_list += '<option value="063">063</option>';
	phone_first_list += '<option value="064">064</option>';
	phone_first_list += '<option value="070">070</option>';
	$('select[name="phone_first_select"]').html(phone_first_list);

	/*휴대폰 맨 앞자리*/
	var hp_first_list ='';
	//hp_first_list += '<option value="" selected="selected"></option>';
	hp_first_list += '<option value="010" selected="selected">010</option>';
	hp_first_list += '<option value="011">011</option>';
	hp_first_list += '<option value="016">016</option>';
	hp_first_list += '<option value="017">017</option>';
	hp_first_list += '<option value="018">018</option>';
	hp_first_list += '<option value="019">019</option>';
	$('select[name="hp_first_select"]').html(hp_first_list);

	/*email*/
	var email_list ='';
	email_list += '<option value="" selected="selected">직접 입력</option>';
	email_list += '<option value="naver.com">naver.com</option>';
	email_list += '<option value="hanmail.net">hanmail.net</option>';
	email_list += '<option value="daum.net">daum.net</option>';
	email_list += '<option value="nate.com">nate.com</option>';
	email_list += '<option value="gmail.com">gmail.com</option>';
	email_list += '<option value="hotmail.com">hotmail.com</option>';
	email_list += '<option value="korea.com">korea.com</option>';
	email_list += '<option value="paran.com">paran.com</option>';
	email_list += '<option value="dreamwiz.com">dreamwiz.com</option>';
	$('select[name="email_select"]').html(email_list);

	$('#email_select').bind('change',function() {
		$('#email_2').val($('#email_select').val());
		$('#email2_2').val($('#email_select').val());//id,pw신청
	});

	/*전화번호*/
	var tel_list ='';
	tel_list += '<option value="02" selected="selected">02</option>';
	tel_list += '<option value="031">031</option>';
	tel_list += '<option value="032">032</option>';
	tel_list += '<option value="033">033</option>';
	tel_list += '<option value="041">041</option>';
	tel_list += '<option value="042">042</option>';
	tel_list += '<option value="043">043</option>';
	tel_list += '<option value="044">044</option>';
	tel_list += '<option value="051">051</option>';
	tel_list += '<option value="052">052</option>';
	tel_list += '<option value="053">053</option>';
	tel_list += '<option value="054">054</option>';
	tel_list += '<option value="055">055</option>';
	tel_list += '<option value="061">061</option>';
	tel_list += '<option value="062">062</option>';
	tel_list += '<option value="063">063</option>';
	tel_list += '<option value="064">064</option>';
	tel_list += '<option value="070">070</option>';

	$('select[name="tel_first_select"]').html(tel_list);

	/*전화번호 2 */
	var tel_2_list ='';
	tel_2_list += '<option value="02" selected="selected">02</option>';
	tel_2_list += '<option value="031">031</option>';
	tel_2_list += '<option value="032">032</option>';
	tel_2_list += '<option value="033">033</option>';
	tel_2_list += '<option value="041">041</option>';
	tel_2_list += '<option value="042">042</option>';
	tel_2_list += '<option value="043">043</option>';
	tel_2_list += '<option value="044">044</option>';
	tel_2_list += '<option value="051">051</option>';
	tel_2_list += '<option value="052">052</option>';
	tel_2_list += '<option value="053">053</option>';
	tel_2_list += '<option value="054">054</option>';
	tel_2_list += '<option value="055">055</option>';
	tel_2_list += '<option value="061">061</option>';
	tel_2_list += '<option value="062">062</option>';
	tel_2_list += '<option value="063">063</option>';
	tel_2_list += '<option value="064">064</option>';
	tel_2_list += '<option value="070">070</option>';

	$('select[name="tel2_first_select"]').html(tel_2_list);

	/*팩스번호*/
	var fax_list ='';
	fax_list += '<option value="02" selected="selected">02</option>';
	fax_list += '<option value="031">031</option>';
	fax_list += '<option value="032">032</option>';
	fax_list += '<option value="033">033</option>';
	fax_list += '<option value="041">041</option>';
	fax_list += '<option value="042">042</option>';
	fax_list += '<option value="043">043</option>';
	fax_list += '<option value="044">044</option>';
	fax_list += '<option value="051">051</option>';
	fax_list += '<option value="052">052</option>';
	fax_list += '<option value="053">053</option>';
	fax_list += '<option value="054">054</option>';
	fax_list += '<option value="055">055</option>';
	fax_list += '<option value="061">061</option>';
	fax_list += '<option value="062">062</option>';
	fax_list += '<option value="063">063</option>';
	fax_list += '<option value="064">064</option>';

	$('select[name="fax_first_select"]').html(fax_list);

	 $('.tooltip').tooltip({
	    track: true,
	    delay: 0,
	    showURL: false,
	    showBody: " - ",
	    fade: 250
	  });

	 /* ---------------------------------------------------- */
	/* 동적 form 전송 처리기능
	/* ---------------------------------------------------- */
	$.dynamicForm = function(url, data, method, target){
	    // url과 data를 입력받음
	    if( url && data ){
	        // data 는  string 또는 array/object 를 파라미터로 받는다.
	        data = typeof data == 'string' ? data : jQuery.param(data);
	        // 파라미터를 form의  input으로 만든다.
	        var inputs = '';
	        jQuery.each(data.split('&'), function(){
	            var pair = this.split('=');
	            inputs+='<input type="hidden" name="'+ pair[0] +'" value="'+ pair[1] +'" />';
	        });
	        target = (target != null)? ' target="'+target+'" ' : '';
	        // send request
	        jQuery('<form action="'+ url +'" method="'+ (method||'post') +'" '+target+'>'+inputs+'</form>')
	        .appendTo('body').submit().remove();
	    };
	};
});

/**
 * 콤마 붙이기 함수
 * @param value int
 */
function number_format(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}


//Chrome에서 Skip네비게이션
jQuery(document).ready(function($) {
    $('a[href^="#"]').on('click', function(e){
        var anchorid = $(e.currentTarget);
        var select_id = $(anchorid.attr('href'));
        select_id.attr("tabindex","-1").css('outline','0').focus();
        select_id.on('focusout',function(){
            $(this).removeAttr("tabindex");
        });
    });
});

/* 쿼리스트링 가져오기*/
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

/* 팝업창 호출 */
function openPopup(url, name, width, height) {
	var l = (screen.width) ? (screen.width-width)/2 : 0;
	var t = (screen.height) ? (screen.height-height)/2 : 0;
	window.open(url, "", "width="+ width +",height="+ height +",toolbar=no,status=no,location=no,scrollbars=yes,menubar=no,resizable=yes,left="+l+",top="+t);
}

/* TR onMouseOver/onMouseOut 효과주기 */
function overColor(myColor) {
	myColor.style.backgroundColor="#f4f7f9";
}
function outColor(myColor) {
	myColor.style.backgroundColor="";
}

/* TR 열고 닫기 */
function OpenTable(strDescId) {
	var objDescData = document.getElementById(strDescId);

	if(objDescData.style.display == 'none') {
		//objDescData.style.display = 'table-row';
		objDescData.style.display = 'block';
	} else {
		objDescData.style.display = 'none';
	}
}
/* 채용정보 - TR 열고 닫기 */
function OpenTable2(strDescId) {
	var objDescData = document.getElementById(strDescId);

	if(objDescData.style.display == 'none') {
		objDescData.style.display = 'table-row';
	} else {
		objDescData.style.display = 'none';
	}
}

/* IDC - 부가서비스 */
function OpenDesc(strTitleId, strDescId) {
	var objTitleData = document.getElementById(strTitleId);
	var objDescData = document.getElementById(strDescId);

	if(objDescData.style.display == 'none') {
		objDescData.style.display = 'block';
		objTitleData.innerHTML = '<img src="../../images/idc/btn_detail_close.gif"  width="105" height="22" alt="세부기능접기" />';
	} else {
		objDescData.style.display = 'none';
		objTitleData.innerHTML = '<img src="../../images/idc/btn_detail_open.gif"  width="105" height="22" alt="세부기능보기" />';
	}
}

/* 라디오버튼 선택 - IDC 위치 */

function radio_click00(){
	var objRadio = document.getElementsByName("idc_select");
	for (i=0;i<objRadio.length;i++) {
		document.getElementById("idc_center"+(i+1)).style.display = "none";
	}
	for (i=0;i<objRadio.length;i++) {
		if (objRadio[i].checked) {
			document.getElementById("idc_center"+(i+1)).style.display = "block";
		}
	}
}

function radio_click0(){
	var objRadio = document.getElementsByName("list");
	for (i=0;i<objRadio.length;i++) {
		document.getElementById("view"+(i+1)).style.display = "none";
	}
	for (i=0;i<objRadio.length;i++) {
		if (objRadio[i].checked) {
			document.getElementById("view"+(i+1)).style.display = "block";
		}
	}
}

/* IDC - 부가서비스 */
function radio_click(){
	var objRadio = document.getElementsByName("list");
	for (i=0;i<objRadio.length;i++) {
		document.getElementById("view"+(i+1)).style.display = "none";
	}
	for (i=0;i<objRadio.length;i++) {
		if (objRadio[i].checked) {
			document.getElementById("view"+(i+1)).style.display = "block";
		}
	}
}

/* IDC - 부가서비스 */
function radio_click2(){
	var objRadio = document.getElementsByName("list2");
	for (i=0;i<objRadio.length;i++) {
		document.getElementById("view2_"+(i+1)).style.display = "none";
	}
	for (i=0;i<objRadio.length;i++) {
		if (objRadio[i].checked) {
			document.getElementById("view2_"+(i+1)).style.display = "block";
		}
	}
}

/* IDC - 부가서비스 */
function radio_click3(){
	var objRadio = document.getElementsByName("list3");
	for (i=0;i<objRadio.length;i++) {
		document.getElementById("view3_"+(i+1)).style.display = "none";
	}
	for (i=0;i<objRadio.length;i++) {
		if (objRadio[i].checked) {
			document.getElementById("view3_"+(i+1)).style.display = "block";
		}
	}
}
/*------설/추석 팝업 관련 ------------------------------------------------*/


function closeWin(chkName,chkCookie)
{
	if(chkCookie == "check"){
		$.post('/main/setCookie_popup')
        .done(function( data ){
        })
        .fail(function(data) {
        	//console.log('close display fail');
        })
        .always(function(data) {
        	//console.log('close display always');
        });
	}
   	document.getElementById(chkName).style.display='none';
}


/*------설/추석 팝업 관련 end------------------------------------------------*/


/*------배너 관련 ------------------------------------------------*/
function closeBannerWin(chkName,chkCookie)
{
	if(chkCookie == "check_banner"){
		$.post('/main/setCookie_bannerpopup')
        .done(function( data ){
        })
        .fail(function(data) {
        	//console.log('close display fail');
        })
        .always(function(data) {
        	//console.log('close display always');
        });
	}
   	document.getElementById(chkName).style.display='none';
}


/*------설/추석 팝업 관련 end------------------------------------------------*/

/* CLOUD->대시보드 열기*/
$(function(){
    $('.layer_cloud_dashboard').dialog({
       'autoOpen':false,
       'title':'클라우드 관리 페이지 열기',
       'width':400,
       'height':150
    });
});
/* CLOUD - 부가서비스 by blueface*/
function cloud_addservice_set(){

}
/* MYPAGE-> 클라우드 통계보기 & 링크핸들링 by blueface*/
$(function(){
   var cloudAuthUrl = "https://new.ixcloud.net/auth/auto"
   $('.layer_cloud_account').dialog({
       'autoOpen':false,
       'title':'클라우드 관리',
       'width':400,
       'height':230,
       'focus':function(event, ui){
           $(this).find('a').blur();
       }
   });
   $('.btn_cloud_stat').on('click',function(){
      //$('.layer_cloud_account').dialog('open');
      var form = $('form[name="cloud_auth"]');
      $(form).ajaxSubmit({
            url:'/cloud/auth/auto',
            type:'post',
            dataType:'json',
            success:function(data){
                if(data.success){
                    var form = $('form[name="cloud_auth"]');
                    form.find('input[name="key"]').val(data.results['key']);
                    form.find('input[name="next"]').val('/dashboard/billing');
                    form.attr('action',cloudAuthUrl);
                    form.attr('target','dashboard');
                    form.attr('method','post');

                    var dialog = $(".layer_cloud_dashboard");
                    dialog.find('button[name="open_dashboard"]').click(function(){
                        try{
                            var win = window.open('about:blank','dashboard');
                            $('form[name="cloud_auth"]').submit();
                            $(".layer_cloud_dashboard").dialog('close');
                            win.focus();
                            return false;
                        }catch(e){
                            alert('팝업이 차단 되었습니다.');
                            return false;
                        }
                    });
                    $(".layer_cloud_dashboard").dialog("open");
                }
            },
            error:function(xhr, status, error){
                var res = xhr.responseText;
                if($.isJSON(res)){
                    res = $.parseJSON(res);
                    if(res.error && res.error.message){
                        if(xhr.status===401){
                            alert('일치하는 정보를 찾을 수 없습니다.');
                        }else{
                            alert(res.error.message);
                        }
                    }else if(xhr.status===302){
                        var alertmsg = (res.results.message).replace(/\\n/gi,'\n');
                        alert(alertmsg);
                        document.location.href = res.results.location;
                    }else{
                        alert('알 수 없는 오류 입니다(Code:002).');
                    }
                }else{
                    alert('알 수 없는 오류 입니다(Code:003).');
                };
            }
        });
      return false;
   });
   $('.btn_cloud_account').on('click',function(){
var form = $('form[name="cloud_auth"]');
      $(form).ajaxSubmit({
            url:'/cloud/auth/auto',
            type:'post',
            dataType:'json',
            success:function(data){
                if(data.success){
                    var form = $('form[name="cloud_auth"]');
                    form.find('input[name="key"]').val(data.results['key']);
                    form.attr('action',cloudAuthUrl);
                    form.find('input[name="next"]').val('/dashboard/billing');
                    form.attr('target','dashboard');
                    form.attr('method','post');

                    var dialog = $(".layer_cloud_dashboard");
                    dialog.find('button[name="open_dashboard"]').click(function(){
                        try{
                            var win = window.open('about:blank','dashboard');
                            $('form[name="cloud_auth"]').submit();
                            $(".layer_cloud_dashboard").dialog('close');
                            win.focus();
                            return false;
                        }catch(e){
                            alert('팝업이 차단 되었습니다.');
                            return false;
                        }
                    });
                    $(".layer_cloud_dashboard").dialog("open");
                }
            },
            error:function(xhr, status, error){
                var res = xhr.responseText;
                if($.isJSON(res)){
                    res = $.parseJSON(res);
                    if(res.error && res.error.message){
                        if(xhr.status===401){
                            alert('일치하는 정보를 찾을 수 없습니다.');
                        }else{
                            alert(res.error.message);
                        }
                    }else if(xhr.status===302){
                        var alertmsg = (res.results.message).replace(/\\n/gi,'\n');
                        alert(alertmsg);
                        document.location.href = res.results.location;
                    }else{
                        alert('알 수 없는 오류 입니다(Code:002).');
                    }
                }else{
                    alert('알 수 없는 오류 입니다(Code:003).');
                };
            }
        });
      return false;
   });
   $('.link_cloud_account').on('click',function(){
       var form = $('form[name="cloudEssexForm"]');
       form.find('input[name="userid"]').val($(this).data('userid'));
       form.find('input[name="type"]').val($(this).data('type'));
       form = form.get(0);

       var path = location.pathname;
       if(/(.)*\/member\//.test(path)){
           form.action = '/mypage/member/cloud.html';
           form.submit();
       }else{
           form.action = '/mypage/essex/auth.html';
           form.target = 'essex_main';
           form.submit();
       }
       return false;
   });
});


/* 프린트 관련 스크립트 div 영역만 프린트함 */
var win=null;
function print2(printThis) {
	win = window.open();
	self.focus();
	win.document.open();
	win.document.write("<"+"html"+"><"+"head"+"><"+"style"+">");
	win.document.write("body, td { font-family: Verdana; font-size: 10pt;}");
	win.document.write("<"+"/"+"style"+"><"+"/"+"head"+"><"+"body"+">");
	win.document.write(printThis);
	win.document.write("<"+"/"+"body"+"><"+"/"+"html"+">");
	win.document.close();
	win.print();
	win.close();
}

//jquery 확장
(function ($) {
    $.isJSON = function (json) {
        json = json.replace(/\\["\\\/bfnrtu]/g, '@');
        json = json.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']');
        json = json.replace(/(?:^|:|,)(?:\s*\[)+/g, '');
        return (/^[\],:{}\s]*$/.test(json))
    }

    $.fn.isJSON = function () {
        var json = this;
        if (jQuery(json).is(":input")) {
            json = jQuery(json).val();
            json = new String(json);
            return jQuery.isJSON(json)
        } else {
            throw new SyntaxError("$(object).isJSON only accepts fields!");
        }
    }
    String.prototype.isJSON = function () {
        var y = this;
        return jQuery.isJSON(y);
	}

    //html 이 같으면 합침
    $.fn.rowspan = function(colIdx, isStats)
    {
        return this.each(function(){
            var that;
            $('tr', this).each(function(row) {
                $('td:eq('+colIdx+')', this).filter(':visible').each(function(col) {

                    if ($(this).html() == $(that).html() && (!isStats || isStats && $(this).prev().html() == $(that).prev().html()))
                    {
                        rowspan = $(that).attr("rowspan") || 1;
                        rowspan = Number(rowspan)+1;

                        $(that).attr("rowspan",rowspan);

                        //do your action for the colspan cell here
                        $(this).hide();

                        $(this).remove();
                        // do your action for the old cell here
                    }
                    else
                    {
                        that = this;
                    }

                    // set the that if not already set
                    that = (that == null) ? this : that;
                });
            });
        });
    };

    //html 과 class가 같으면 합침
    $.fn.rowspan_ex = function(colIdx, eqclass, isStats)
    {
        return this.each(function(){
            var that;
            $('tr', this).each(function(row) {
                $('td:eq('+colIdx+')', this).filter(':visible').each(function(col) {

                    if ($(this).html() == $(that).html() &&
                        (!isStats || isStats && $(this).prev().html() == $(that).prev().html()) &&
                        ( ( $(this).hasClass(eqclass) == $(that).hasClass(eqclass) ) && ($(this).hasClass(eqclass)) )//this,that 이 동일한 class명을 가지고 있고 hasClass가 true
                    )
                    {
                        //console.log('this html '+$(this).html());
                        //console.log('that html '+$(that).html());
                        rowspan = $(that).attr("rowspan") || 1;
                        rowspan = Number(rowspan)+1;

                        $(that).attr("rowspan",rowspan);

                        //do your action for the colspan cell here
                        $(this).hide();

                        $(this).remove();
                        // do your action for the old cell here
                    }
                    else
                    {
                        that = this;
                    }

                    // set the that if not already set
                    that = (that == null) ? this : that;
                });
            });
        });
    };


})(jQuery);