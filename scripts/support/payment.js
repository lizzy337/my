var payemnt=({
    form:null,
    orderid:'',
    init:function(){
        let _this = this;
        _this.form = $("#form-payment");
        _this.orderid = TossPayment.getOrderid();
        _this.setTossPayment();
        _this.formvalid();

    },
    setTossPayment:function(){
        let _this = this;
        TossPayment.getDefaultSet(
            function(){
                $("#div-payment-btn01").html('<span class="btn_pack xlarge" ><input type="submit"  id="ts-payment-btn" value="결제하기"  ></span>');
            },
            function(){
                alert(msgelement.default_faild);
            }
        );
    },
    formvalid:function(){
        let _this = this;
        let _form = _this.form;
        _form.validate({
            submitHandler:function(_form){
                _this.payemntDataSend();
                return false;
            },
            rules:{
                ts_ordername:     {required:true},
                ts_productinfo:   {required:true},
                ts_amount:        {required:true, number:true, min:100000},
                ts_buyer:         {required:true},
                ts_customername:  {required:true},
                ts_customeremail: {required:true, email:true},
                ts_customermobile:{required:true, number:true, minlength:10}
            },
            errorClass:"errMsg",
            errorPlacement : function(errMsg, element) {
                if(element.attr("name") == "ts_ordername"){ errMsg.insertAfter("#ts_ordername"); }
                if(element.attr("name") == "ts_productinfo"){ errMsg.insertAfter("#ts_productinfo"); }
                if(element.attr("name") == "ts_amount"){ errMsg.insertAfter("#ts_amount"); }
                if(element.attr("name") == "ts_buyer"){ errMsg.insertAfter("#ts_buyer"); }
                if(element.attr("name") == "ts_customername"){ errMsg.insertAfter("#ts_customername"); }
                if(element.attr("name") == "ts_customeremail"){ errMsg.insertAfter("#ts_customeremail"); }
                if(element.attr("name") == "ts_customermobile" ){ errMsg.insertAfter("#ts_customermobile"); }
            },
            messages:{
                ts_ordername:     {required:msgelement.required.ts_ordername},
                ts_productinfo:   {required:msgelement.required.ts_productinfo},
                ts_amount:        {required:msgelement.required.ts_amount,
                                   number:msgelement.valid.ts_customeremail_number,
                                   min:msgelement.valid.ts_amount_min},
                ts_buyer:         {required:msgelement.required.ts_buyer},
                ts_customername:  {required:msgelement.required.ts_customername},
                ts_customeremail: {required:msgelement.required.ts_customeremail,
                                   email:msgelement.valid.ts_customeremail_email},
                ts_customermobile:{required:msgelement.required.ts_customermobile,
                                   number:msgelement.valid.ts_customermobile_number,
                                   minlength:msgelement.valid.ts_customermobile_minlength}
            }
        });
    },
    payemntDataSend:function()
    {
        var _this = this,
            _url  = msgelement.cardposturl,
            _data = _this.form.serialize()
        ;
        _data += "&orderid="+_this.orderid //add ordderid

        $.post(_url, _data, function(){},'json')
        .done(function (_response) {
            if (_response.status) {
                let _addparam = $.param(_response.approvaldata);

                let _paymentdata = {
                    amount: $("#ts_amount").val(),
                    orderId: _this.orderid,
                    orderName: $("input[name='ts_ordername']:checked").val(),
                    customerName: $("#ts_customername").val(),
                    customerEmail: $("#ts_customeremail").val(),
                    customerMobilePhone: $("#ts_customermobile").val(),
                    successUrl: window.location.origin + '/support/payment/payment02/success?'+_addparam,
                    failUrl: window.location.origin + '/support/payment/payment02/fail?'+_addparam,
                };

                TossPayment.sendTossPayment(_paymentdata, 'CARD');
                return false;
            }
            else { alert(_response.error_msg+'['+_response.error_cd+']'); return false;}
        })
        .fail(function (data) {
            alert(msgelement.post.faild01);
            return false;
        });
    }
});

$(function () {
   payemnt.init();
});