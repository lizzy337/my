/*
 |--------------------------------------------------------------------------
 | $.idcrequest
 |--------------------------------------------------------------------------
 | 데이터센터 입주관련 온라인 접수기능
 |
 */

$.idcrequest = ({
    init: function () {
        var _this = this;
        _this.initLoadScript();
        _this.initButton();
    },
    initLoadScript:function(){
        $.getScript('/scripts/vendor/parsleyjs/dist/parsley.min.js');
        $.getScript('/scripts/vendor/sweetalert/dist/sweetalert.min.js');
    },
    initButton:function(){
        var _this = this;
        $('button.btn-request').unbind('click').on('click',function(e){
            e.preventDefault();
            _this.formsubmit();
        });
    },
    formsubmit:function(){
        var _form  = $('#request_form'),
            _url   = $(window.location).attr('pathname'),
            _param = _form.serialize(),
            _chkcha= true
        ;

        $('#captcha_element div:first').css('border','0').removeClass('parsley-error');
        $('#captcha_element ul').remove();

        if($('#g-recaptcha-response').val() =='')
        {
            $('#captcha_element div:first').css('border','1px solid').addClass('parsley-error');
            $('#captcha_element div:first').after('<ul class="parsley-errors-list filled"><li class="parsley-required">Please verify that you are not a robot.</li></ul>');

            _chkcha = false;
        }

        if (_form.parsley().validate() && _chkcha==true) {
            $.post(_url, _param, function () {}, 'json')
            .done(function (resdata) {
                if (resdata.status === true) {
                    swal("Request registration completed.", resdata.message, "success");
                }
                else {
                    swal("Failed to register request.", resdata.message, "warning");
                }
            })
            .fail(function (resdata) {
                swal("Request registration error", "Please try again.\nPlease contact the person in charge continuously.", "error");
            })
            .always(function(resdata){
                grecaptcha.reset(krecaptcha);
            });
        }

        return false;
    },
    setCaptcha:function(){
        $('#captcha_element div:first').css('border','0').removeClass('parsley-error');
        $('#captcha_element ul').remove();
    }
});


var krecaptcha;
var setCaptcha = function(){
    $('#captcha_element div:first').css('border','0').removeClass('parsley-error');
    $('#captcha_element ul').remove();
};
var onloadCallback = function() {
    krecaptcha = grecaptcha.render('captcha_element', {
      'sitekey' : '6LfUNlIUAAAAAAoHjufjfZg4RK8drdm-RC8P2CsR',
      'callback': setCaptcha
    });
};


$(document).ready(function () {
    $.idcrequest.init();
});