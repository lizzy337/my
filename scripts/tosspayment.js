var TossPayment = {
    clientkey:null,
    paymentdata:{
        amount: 0,
        orderId: '',
        orderName: '',
        customerName: '',
        customerEmail: '',
        customerMobilePhone: '',
        successUrl: '',
        failUrl: ''
    },
    getDefaultSet:function(_callback1, _callback2){
        let _this = this;
        $.getScript("/scripts/crypto-js.3.1.2.rollups.aes.js")
        .done(function(){
            $.getJSON('/support/payment/defaultsetdata')
            .done(function( json ) {
                let code1 = (json.type==1)?json.code1:json.code2;
                let code2 = (json.type==1)?json.code2:json.code1;
                let bytes = CryptoJS.AES.decrypt(code1, code2);
                _this.clientkey = bytes.toString(CryptoJS.enc.Utf8);
                _callback1();
                return true;
            })
            .fail(function( jqxhr, textStatus, error ) {
                var err = textStatus + ", " + error;
                // console.log( "getjson Failed: " + err );
                _callback2();
            });
        })
        .fail(function( jqxhr, textStatus, error ) {
            var err = textStatus + ", " + error;
            // console.log( "getscript Failed: " + err );
            _callback2();
        });
    },
    sendTossPayment:function(_paymentdata, _type){
        let _this = this;
        var tossPayments = TossPayments(_this.clientkey);
        _this.paymentdata = $.extend({},_this.paymentdata, _paymentdata);
        tossPayments.requestPayment(_type, _this.paymentdata);
    },
    sendTossPayment2:function(_paymentdata, _type){
        let _this = this;
        var tossPayments = TossPayments(_this.clientkey);
        _this.paymentdata = $.extend({},_this.paymentdata, _paymentdata);
        delete _this.paymentdata.customerMobilePhone;
        tossPayments.requestPayment(_type, _this.paymentdata);
    },
    getOrderid:function(){
        let _this = this;
        let date = new Date();
        let year = date.getFullYear().toString();

        let month = date.getMonth() + 1;
        month = month < 10 ? '0' + month.toString() : month.toString();

        let day = date.getDate();
        day = day < 10 ? '0' + day.toString() : day.toString();

        let hour = date.getHours();
        hour = hour < 10 ? '0' + hour.toString() : hour.toString();

        let minites = date.getMinutes();
        minites = minites < 10 ? '0' + minites.toString() : minites.toString();

        let seconds = date.getSeconds();
        seconds = seconds < 10 ? '0' + seconds.toString() : seconds.toString();

        let array = new Uint16Array(1);
        let randomcd = window.crypto.getRandomValues(array);

        return year + month + day + "_" + randomcd + "_" + hour + minites + seconds;
    }
};
