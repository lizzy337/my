$(document).ready(function(){


	//단체
	$("#idpwReqCompForm").is(function(){
		$("#idpwReqCompForm").submit(function(){
			find_idpw_comp.valid($("#idpwReqCompForm"));
		});
	});

	$('#addfile1_1img').is(function(){
		$('#addfile1_1img').unbind().change(function(){
			var path = $(this).val();
			$("#addfile1_1img_dp").val(path);
		});
	});

	$('#addfile1_2img').is(function(){
		$('#addfile1_2img').unbind().change(function(){
			var path = $(this).val();
			$("#addfile1_2img_dp").val(path);
		});
	});

	////////////////////////////////////
	//개인
	$("#idpwReqPrivForm").is(function(){
		$("#idpwReqPrivForm").submit(function(){
			find_idpw_priv.valid($("#idpwReqPrivForm"));
		});
	});

	$('#addfile2_1img').is(function(){
		$('#addfile2_1img').unbind().change(function(){
			var path = $(this).val();
			$("#addfile2_1img_dp").val(path);
		});
	});
});

 //단체
function radio_click_docinfo_addfile(){
	$('#doc_addfile').css("display","");
	$('#doc_fax').css("display","none");

	$('#check_doc').val("A");

}

function radio_click_docinfo_fax(){
	$('#doc_addfile').css("display","none");
	$('#doc_fax').css("display","");

	$('#check_doc').val("F");
}


function click_info_id()
{
	var chkval = $('#check_info').val();
	var val = $('#find_id').is(":checked");
	if( val == true ) //check한 경우
	{
		if(chkval.length == 0){ $('#check_info').val("I"); }
		else{ $('#check_info').val("A"); }

		$('#errmsg2').html("");
	}else
	{
		if(chkval.length > 0)
		{
			if(chkval == 'I') { $('#check_info').val(""); }
			else { $('#check_info').val("P"); }
		}
	}
}


function click_info_pw()
{
	var chkval = $('#check_info').val();
	var val = $('#find_pw').is(":checked");
	if( val == true ) //check한 경우
	{
		if( chkval.length == 0){ $('#check_info').val("P"); }
		else{ $('#check_info').val("A"); }

		$('#errmsg2').html("");
	}
	else
	{
		if(chkval.length > 0)
		{
			if(chkval == 'P'){ $('#check_info').val(""); }
			else{ $('#check_info').val("I"); }
		}
	}
}

/////////////////////////////////////////////////
//개인
function radio_click_docinfo2_addfile(){
	$('#doc_addfile2').css("display","");
	$('#doc_fax2').css("display","none");

	$('#check_doc2').val("A");

}

function radio_click_docinfo2_fax(){
	$('#doc_addfile2').css("display","none");
	$('#doc_fax2').css("display","");

	$('#check_doc2').val("F");
}

function click_info2_id()
{
	var chkval = $('#check_info2').val();
	var val = $('#find_id2').is(":checked");
	if( val == true ) //check한 경우
	{
		if(chkval.length == 0){ $('#check_info2').val("I"); }
		else{ $('#check_info2').val("A"); }

		$('#errmsg5').html("");
	}else
	{
		if(chkval.length > 0)
		{
			if(chkval == 'I') { $('#check_info2').val(""); }
			else { $('#check_info2').val("P"); }
		}
	}
}

function click_info2_pw()
{
	var chkval = $('#check_info2').val();
	var val = $('#find_pw2').is(":checked");
	if( val == true ) //check한 경우
	{
		if( chkval.length == 0){ $('#check_info2').val("P"); }
		else{ $('#check_info2').val("A"); }

		$('#errmsg5').html("");
	}
	else
	{
		if(chkval.length > 0)
		{
			if(chkval == 'P'){ $('#check_info2').val(""); }
			else{ $('#check_info2').val("I"); }
		}
	}
}


$.validator.addMethod("check_self_auth",function (value,element)
{
	var _v = $(element).val();
	//console.log('check_self_auth _v : '+_v);

	if(_v == 'YES')
		return true;
	else
		return false;
},"실명 인증을 완료해 주세요.");



var find_idpw_comp = {
		init:function(){

			/*
			//test - 단체.
			$("input[name=companyName]").val("test업체");
		    $("input[name=bizNo]").val("123-45-78963");
			$("input[name=reqName]").val("홍길동");
			$("input[name=relation]").val("팀원");
			$("input[name=phone]").val("02-123-4567");
			$("input[name=sName]").val("담당자");
			$("input[name=cellphone]").val("010-2774-7674");
			$("input[name=email_1]").val("lizzy337");
			$("input[name=email_2]").val("kinx");
			$("input:checkbox[id=find_id]").attr('checked', true) ;
			$("input[name=check_info]").val("I");//아이디분실
			$("textarea[name=reason]").text("테스트중");
			//-end test
			*/

			find_idpw_comp.valid($('form').eq(0));
		}
		,valid:function(form){
			$(form).validate({
		        submitHandler:function(form){
		          return find_idpw_comp.submit(form);
				}//end submitHandler
				,ignore : ":hidden:not('#chk_comp_certification')"
		        ,rules:{
		        	companyName	: {required:true},
		        	bizNo		: {required:true},
		        	reqName		: {required:true},
		        	relation    : {required:true},
		        	phone		: {required:true},
		        	cellphone	: {required:true},
					email_1		: {required:true},
					email_2		: {required:true},
					find_idpw   : {required:true},
					reason		: {required:true},
					//addfile1_1_dp   : {required:function(){ return form.find('#addfile1').is(":checked");} },
					privacyassent	: {required:true},
					chk_comp_certification : {check_self_auth:true,required:true}
				},
				groups:{
					email_info : 'email_1 email_2'
				},
				errorClass:"errMsg",
				errorPlacement : function(errMsg, element) {
					if( element.attr("name") == "companyName"){ errMsg.insertAfter("#companyName"); }
					if( element.attr("name") == "bizNo"){ errMsg.insertAfter("#bizNo"); }
					if( element.attr("name") == "reqName"){ errMsg.insertAfter("#reqName"); }
					if( element.attr("name") == "relation"){ errMsg.insertAfter("#relation"); }
					if( element.attr("name") == "phone"){ errMsg.insertAfter("#phone"); }
					if( element.attr("name") == "cellphone"){ errMsg.insertAfter("#msg_comp_cellphone_validate"); }

					if( element.attr("name") == "email_1" || element.attr("name") == "email_2")
					{
						var email1 = $('#idpwReqCompForm').find('input[name="email_1"]').val();
						var email2 = $('#idpwReqCompForm').find('input[name="email_2"]').val();
						if( !email1.length && !email2.length)
						{
							$("#email_errmsg").html('<label class="errMsg">이메일 아이디, 주소를 입력하세요.</label>');
						}
						else
						{
							if( !email1.length)
								$("#email_errmsg").html('<label class="errMsg"> 이메일 아이디를 입력하세요.</label>');
							if(!email2.length)
								$("#email_errmsg").html('<label class="errMsg"> 이메일 주소를 입력하세요.</label>');
						}
					}
					if( element.attr("name") == "find_idpw"){ errMsg.insertAfter("#errmsg2"); }
					if( element.attr("name") == "reason"){ errMsg.insertAfter("#reason"); }
					//if( element.attr("name") == "addfile1_1_dp"){ errMsg.insertAfter("#errmsg3"); }
					if( element.attr("name") == "privacyassent" ){ errMsg.insertBefore("#privacyassent");}
					if( element.attr("name") == "chk_comp_certification") { $("#msg_comp_self_auth").html(errMsg);}
				},
				messages:{
					companyName	: {required: " 업체명을 입력하세요."},
					bizNo		: {required: " 사업자번호를 입력하세요."},
					reqName		: {required: " 신청자를 입력하세요."},
					relation	: {required: " 관계를 입력하세요."},
					phone		: {required: " 일반전화를 입력하세요."},
					cellphone	: {required: " 휴대전화를 입력하세요."},
					//email_1  : {required: " 이메일 아이디를 입력하세요."},
					//email_2  : {required: " 이메일 주소를 입력하세요"},
					find_idpw   : {required: " 신청할 정보를 선택하세요."},
					reason		: {required: " 사유를 입력하세요."},
					//addfile1_1_dp	: {required: " 서류를 첨부해 주세요."},
					privacyassent	: {required: "개인정보 수집 이용에 동의해주세요. "}
				}

		      });//end validate
		}//end valid
		,submit:function (form){

			///ajax
			//완료시 완료페이지 redirect

			//자동입력방지
			if(typeof(grecaptcha)!= 'undefined')
			{
				if(grecaptcha.getResponse(widgetId1)=="")
				{
					alert("자동입력방지 항목을 확인해주세요");
					return false;
				}
				else{
					$url = "/auth/proc_find_idpw_req/C";
					$(form).ajaxSubmit({
						url:$url,
						type:'POST',
						dataType:'JSON',
						success:function(data){
							//console.log('submit success');
							//console.log(data.success);
							//console.log(data.message);
							if( data.success == true){ //page redirect
								document.location.href = data.message;
							}
							else{
								alert(data.message);
							}
						},
						error:function(data){
							//console.log('submit error');
							//console.log(data);
							alert(data.message);
						},
						complete:function(data){
							//console.log('submit complete');
						}
					});
				}
			}
		    return false;
		  }//end submit
}; // end find_idpw_comp


var find_idpw_priv = {
		init:function(){
			find_idpw_priv.valid($('form').eq(1));
		}
		,valid:function(form){
			$(form).validate({
		        submitHandler:function(form){
		          return find_idpw_priv.submit(form);
				}//end submitHandler
				,ignore : ":hidden:not('#chk_priv_certification')"
		        ,rules:{
		        	userName	: {required:true},
		        	residentNo	: {required:true},
		        	phone2		: {required:true},
		        	cellphone2	: {required:true},
		        	email2_1	: {required:true},
		        	email2_2	: {required:true},
					find_idpw2  : {required:true},
					reason2		: {required:true},
					addfile2_1  : {required:function(){ return form.find('#addfile2').is(":checked");} },
					privacyassent2	: {required:true},
					chk_priv_certification : {check_self_auth:true,required:true}
				},
				groups:{
					email_info : 'email2_1 email2_2'
				},
				errorClass:"errMsg",
				errorPlacement : function(errMsg, element) {
					if( element.attr("name") == "userName"){ errMsg.insertAfter("#userName"); }
					if( element.attr("name") == "residentNo"){ errMsg.insertAfter("#errmsg7"); }
					if( element.attr("name") == "phone2"){ errMsg.insertAfter("#phone2"); }
					if( element.attr("name") == "cellphone2"){ errMsg.insertAfter("#msg_priv_cellphone_validate"); }
					if( element.attr("name") == "email2_1" || element.attr("name") == "email2_2")
					{
						var email1 = $('#idpwReqPrivForm').find('input[name="email2_1"]').val();
						var email2 = $('#idpwReqPrivForm').find('input[name="email2_2"]').val();
						if( !email1.length && !email2.length)
						{
							$("#email2_errmsg").html('<label class="errMsg">이메일 아이디, 주소를 입력하세요.</label>');
						}
						else
						{
							if( !email1.length)
								$("#email2_errmsg").html('<label class="errMsg"> 이메일 아이디를 입력하세요.</label>');
							if(!email2.length)
								$("#email2_errmsg").html('<label class="errMsg"> 이메일 주소를 입력하세요.</label>');
						}
					}
					if( element.attr("name") == "find_idpw2"){ errMsg.insertAfter("#errmsg5"); }
					if( element.attr("name") == "reason2"){ errMsg.insertAfter("#reason2"); }
					if( element.attr("name") == "addfile2_1"){ errMsg.insertAfter("#errmsg6"); }
					if( element.attr("name") == "privacyassent2" ){ errMsg.insertBefore("#privacyassent2");}
					if( element.attr("name") == "chk_priv_certification") { $("#msg_priv_self_auth").html(errMsg);}
				},
				messages:{
					userName	: {required: " 회원명을 입력하세요."},
					residentNo	: {required: " 주민등록번호 앞자리 6글자를 입력하세요."},
					phone2		: {required: " 일반전화를 입력하세요."},
					cellphone2	: {required: " 휴대전화를 입력하세요."},
					//email_info  : {required: " 이메일을 입력하세요."},
					//email2_1  : {required: " 이메일 아이디를 입력하세요."},
					//email2_2  : {required: " 이메일 주소를 입력하세요"},
					find_idpw2  : {required: " 신청할 정보를 선택하세요."},
					reason2		: {required: " 사유를 입력하세요."},
					addfile2_1	: {required: " 서류를 첨부해 주세요."},
					privacyassent2	: {required: "개인정보 수집 이용에 동의해주세요. "},
				}
		      });//end validate
		}//end valid
		,submit:function (form){

			//ajax
			//완료시 완료페이지 redirect

			//자동입력방지
			if(typeof(grecaptcha)!= 'undefined')
			{
				if(grecaptcha.getResponse(widgetId2)=="")
				{
					alert("자동입력방지 항목을 확인해주세요");
					return false;
				}
				else{
					$url = "/auth/proc_find_idpw_req/P";
					$(form).ajaxSubmit({
						url:$url,
						type:'POST',
						dataType:'JSON',
						success:function(data){
							//console.log('submit success');
							//console.log(data);
							if( data.success == true){ //page redirect
								document.location.href = data.message;
							}
							else{
								alert(data.message);
							}
						},
						error:function(data){
							//console.log('submit error');
							//console.log(data);
							alert(data.message);
						}
					});
				}
			}
		    return false;

		  }//end submit

}; // end find_idpw_priv

var self_auth_result =
{
	checkPlusResult: function()
	{
		var resultData = {};
		if($('#returnFlag').val() == "S")
		{
			var success_msg = $('#success_msg').val();
			var plaindata = $('#plaindata').val();
			//console.log("success_msg : "+success_msg);
			//console.log("success plaindata : "+plaindata);
			//alert(success_msg);
			//alert("plaindata : "+plaindata);
			//self.close();
			if(plaindata == 1)
			{
				//인증완료처리
				var kinds = window.opener.document.getElementById("tab_active_kinds").value;
				//console.log('checkPlusResult kinds : '+kinds);
				if(kinds == 'COMP')
				{
					window.opener.document.getElementById("chk_comp_certification").value = "YES";
					window.opener.document.getElementById("btn_comp_self_auth").style.display = "none";
					window.opener.document.getElementById("btn_comp_self_auth_success").style.display = "inline-block";
					window.opener.document.getElementById("msg_comp_self_auth").innerHTML  = "";
				}
				else if(kinds == 'PRIV')
				{
					window.opener.document.getElementById("chk_priv_certification").value = "YES";
					window.opener.document.getElementById("btn_priv_self_auth").style.display = "none";
					window.opener.document.getElementById("btn_priv_self_auth_success").style.display = "inline-block";
					window.opener.document.getElementById("msg_priv_self_auth").innerHTML  = "";
				}
			}
		}
		else
		{
			//resultData['returnFlag'] = $('#returnFlag').val();
			var failed_msg = $('#failed_msg').val();
			var plaindata = $('#plaindata').val();
			//console.log("failed_msg : "+success_msg);
			//console.log("fail plaindata : "+plaindata);
			alert('인증이 실패하였습니다.');
		}
		self.close();

	}
}

/* testing
$body = $("body");
$(document).on({
    ajaxStart: function() {
        console.log('ajaxStart');
        $('body').addClass('loading');
        //modal..???

        var loadinghtml = '<p style="text-align:center;  padding:16px 0 0 0; left:50%; top:50%; position:absolute;"><img src="/images/include/ajax-loader.gif" /></p>';
        $('.loading').append(loadinghtml);

    },
    ajaxStop: function() {
    	console.log('ajaxStop');
    	$body.find('loading').find('p').remove();

    }
});
*/
/* cf, old
$(document).on({
    ajaxStart: function() {
    	console.log('ajaxStart');
    	chkAjax = true;
    	$('body').addClass('loading');

    	$('body.loading').addClass('modal').fadeIn();

    	showloading= setInterval(function (){
			if(chkAjax){
				$('body').addClass('loading');
		    	$('body.loading').addClass('modal').fadeIn();
			}
    	}, 1000);

    },
    ajaxStop: function() {
    	console.log('ajaxStop');
    	chkAjax = false;
    	$("body.loading .modal").hide();
    	$body.removeClass("loading");
    	clearInterval(showloading);

    }
});
*/

