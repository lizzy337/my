//상담게시판 관련
var nSvrType;
var consult = {
	init:function(nServiceType){
		nSvrType = nServiceType;		
		consult.valid($('form').eq(0));
		
	}
	,valid:function(form){
		$(form).validate({
	        submitHandler:function(form){
	          return consult.submit(form);
	        }//end submitHandler
	        ,rules:{	        	
				sManagerName	: {required:true},
				hp_second		: {required:true, number:true },
				hp_third		: {required:true, number:true },				
				email_1			: {required:true},
				email_2			: {required:true},
				sTitle			: {required:true},
				privacyassent	: {required:true},
				consult_service_support : {required:true}
			},
			groups:{
				hp_number : 'hp_second hp_third',
				email_info : 'email_1 email_2' 
			},
			errorClass:"errMsg",
			errorPlacement : function(errMsg, element) {
				if( element.attr("name") == "sManagerName"){ errMsg.insertAfter("#sManagerName"); }
				if( element.attr("name") == "hp_second" || element.attr("name") == "hp_third"){
					errMsg.insertAfter("#hp_third");
				}
				if( element.attr("name") == "email_1" || element.attr("name") == "email_2"){
					errMsg.insertAfter("#errmsg");
				}
				if( element.attr("name") == "sTitle"){ errMsg.insertAfter("#sTitle"); }
				if( element.attr("name") == "privacyassent" ){
					errMsg.insertBefore("#privacyassent");
				}
				if( element.attr("name") == "consult_service_support" ){
					errMsg.insertAfter("#errmsg2");
				}
			},
			messages:{
				sManagerName	: {required: " 신청자이름을 입력하세요."},
				hp_second		: {required: " 전화번호를 입력하세요."},
				hp_third		: {required: " 전화번호를 입력하세요."},
				email_2			: {required: " 이메일을 입력하세요."},
				sTitle			: {required: " 제목을 입력하세요."},
				privacyassent	: {required: "개인정보 수집 이용에 동의해주세요. "},
				consult_service_support : {required : "서비스를 선택하세요."}
			}
	      });//end validate
	}//end valid
	
	//org
	,submit:function (form){
		if( nSvrType == 99 ){ //기술지원의 상담게시판
			nSvrType = parseInt($(":input:radio[name=consult_service_support]:checked").val());			
		}
		//sServiceName = consult.getServiceName(nSvrType);
		form.action = "/consult/Counsel/proc/" + nSvrType;
	    form.submit();
	    return false;
	  }//end submit
	
	/*
	,submit:function (form){ //lizzy test
		if( nSvrType == 99 ){ //기술지원의 상담게시판
			nSvrType = parseInt($(":input:radio[name=consult_service_support]:checked").val());			
		}
		//$url = "/consult/Counsel/proc_test/" + nSvrType;//test
		$url = "/consult/Counsel/proc2/" + nSvrType;
		$(form).ajaxSubmit({
			url:$url,
			type:'POST',
			dataType:'JSON',
			success:function(data){
				console.log('submit success');
				console.log(data.success);				
				console.log(data.message);
				//document.location.href = data.result['message].message;
			},
			error:function(data){
				console.log('submit error');
			},
			complete:function(data){
				console.log('submit complete'); 
			}
		});
	    return false;
	  }//end submit lizzy test
	*/
}; // end consult
 

$body = $("body");
var chkAjax = false;
var showloading=null;
$(document).on({
 ajaxStart: function() {
        //console.log('ajaxStart');
        $('body').addClass('loading');        
        var loadinghtml = '<p style="text-align:center;padding:16px 0 0 0;left:50%;top:50%;position:absolute;" class="ajaxLoading"><img src="/images/include/ajax-loader.gif" /></p>';
        $('.loading').append(loadinghtml);        
    },
    ajaxStop: function() {
    	//console.log('ajaxStop');
        $('body').removeClass('loading');
    	$('body').find('.ajaxLoading').remove();
    	
    }
});

/*//my_payment.js 로 이동 170720
var payment01 = {
		init:function(){	
			payment01.valid($('form').eq(0));
		}
		,valid:function(form){
			$(form).validate({
		        submitHandler:function(form){
		          return payment01.submit(form);
		        }//end submitHandler
		        ,rules:{
		        	sCompanyName	: {required:true},
		        	sDepositName	: {required:true},
		        	sPaymentComment	: {required:true},				
		        	nPaymentPrice	: {required:true, number:true},
		        	sEmail			: {required:true, email:true},
		        	sPhone			: {required:true}
				},
				errorClass:"errMsg",
				errorPlacement : function(errMsg, element) {
					if( element.attr("name") == "sCompanyName"){ errMsg.insertAfter("#sCompanyName"); }
					if( element.attr("name") == "sDepositName"){ errMsg.insertAfter("#sDepositName"); }				
					if( element.attr("name") == "sPaymentComment"){ errMsg.insertAfter("#errmsg1"); }
					if( element.attr("name") == "nPaymentPrice"){ errMsg.insertAfter("#errmsg2"); }		
					if( element.attr("name") == "sEmail"){ errMsg.insertAfter("#sEmail"); }
					if( element.attr("name") == "sPhone"){ errMsg.insertAfter("#sPhone"); }
					if( element.attr("name") == "privacyassent" ){ errMsg.insertBefore("#privacyassent"); }
				},
				messages:{
					sCompanyName	: {required: " 업체명을 입력하세요."},
					sDepositName	: {required: " 입금자명을 입력하세요."},
					sPaymentComment	: {required: " 결제사유를 입력하세요."},
					nPaymentPrice	: {required: " 납부금액을 입력하세요."},
					sEmail			: {required: " 이메일을 입력하세요."},
					sPhone			: {required: " 휴대폰번호를 입력하세요."}
				}
		      });//end validate
		}//end init
		,submit:function (form){
			//html sendmail
			//console.log('payment submit');
		    form.action = "/support/payment01_sendmail/send";	    
		    form.submit();
		    return false;
		  }	
	}//end payment01
//
*/
/*
var payment02={
	init:function(){ 
		payment02.valid($('form').eq(0));
	}
	,valid:function(form){
		$(form).validate({			
	        rules:{
	        	//LGD_PRODUCTINFO		: {required:true},
	        	strServiceTypeEtc 	: {required:true},
	        	//strServiceType		: {required:true},				
	        	LGD_AMOUNT			: {required:true, number:true },
	        	LGD_BUYER			: {required:true},
	        	LGD_DELIVERYINFO	: {required:true},
	        	LGD_BUYEREMAIL 		: {required:true, email:true},
	        	LGD_BUYERPHONE 		: {required:true},
			},
			errorClass:"errMsg",
			errorPlacement : function(errMsg, element) {
				if( element.attr("name") == "strServiceTypeEtc"){ errMsg.insertAfter("#strServiceTypeEtc"); }
				if( element.attr("name") == "LGD_AMOUNT"){ errMsg.insertAfter("#errmsg"); }
				if( element.attr("name") == "LGD_BUYER"){ errMsg.insertAfter("#LGD_BUYER"); }
				if( element.attr("name") == "LGD_DELIVERYINFO"){ errMsg.insertAfter("#LGD_DELIVERYINFO"); }
				if( element.attr("name") == "LGD_BUYEREMAIL"){ errMsg.insertAfter("#LGD_BUYEREMAIL"); }
				if( element.attr("name") == "LGD_BUYERPHONE"){ errMsg.insertAfter("#LGD_BUYERPHONE"); }
			},
			messages:{
				strServiceTypeEtc	: {required: " 결제사유를 입력하세요."},
				LGD_AMOUNT			: {required: " 서비스요금을 입력하세요.(숫자만 가능)"},
				LGD_BUYER			: {required: " 결제자명을 입력하세요."},
				LGD_DELIVERYINFO	: {required: " 업체명을 입력하세요."},
				LGD_BUYEREMAIL		: {required: " 이메일을 입력하세요."},
				LGD_BUYERPHONE 		: {required: " 휴대폰 번호를 선택하세요."},
			}
	      });//end validate
	}//end valid	
}; //end payment02
*/
 
var formcheck_cloud = {
    join_step1:{
        init:function init(form){
            formcheck_cloud.join_step1.valid(form);
        },
        valid:function valid(form){
            var validator = $(form).validate({
                ignore:'.ignore',
                debug:false,
                errorClass:'errMsg',
                submitHandler:function(form){
                  return formcheck_cloud.join_step1.submit(form);
                },
                rules:{
                    name:{required:true},
                    userid:{required:true,email:true},
                    userid_chk:{equalTo:'input[name="userid"]'},
                    password:{required:true,minlength:6,maxlength:12},
                    password_chk:{equalTo:'input[name="password"]'},
                    account_name:{required:true},
                    business_number_01:{required:true,minlength:3,maxlength:3,number:true},
                    business_number_02:{required:true,minlength:2,maxlength:2,number:true},
                    business_number_03:{required:true,minlength:5,maxlength:5,number:true},
                    zipcode1:{required:true,number:true,minlength:3,maxlength:3},
                    zipcode2:{required:true,number:true,minlength:3,maxlength:3},
                    address1:{required:true},
                    address2:{required:true},
                    phone_02:{required:true,number:true,rangelength:[3,4]},
                    phone_03:{required:true,number:true,minlength:4,maxlength:4},
                    mobile_02:{required:true,number:true,rangelength:[3,4]},
                    mobile_03:{required:true,number:true,minlength:4,maxlength:4},                  
                    captcha_code:{required:true},
                    agree_service:{required:true},
                    agree_privacy:{required:true}
                }
                ,groups:{
                    businessnum:"business_number_01 business_number_02 business_number_03",
                    zipcode:"zipcode1 zopcode2",
                    pohne:"phone_02 phone_03",
                    mobile:"mobile_02 mobile_03",
                    address:"address1 address2",
                    agree:"agree_service agree_privacy"
                }
                ,errorPlacement:function(error, element){
                    var n = element.attr("name");
                    var target = $(element).parent('.input_box');
                    target.after(error);
                }
                ,messages:{
                    name:{required:'이름을 입력해 주세요.'},
                    userid:{required:'이메일을 입력해 주세요.',email:'이메일 형식이 일치하지 않습니다.'},
                    userid_chk:{equalTo:'이메일이 일치하지 않습니다.',email:'이메일 형식이 일치하지 않습니다.'},
                    password:{required:'패스워드를 입력해 주세요.'},
                    password_chk:{equalTo:'패스워드가 일치하지 않습니다.'},
                    account_name:{required:'업체명을 입력해 주세요'},
                    business_number_01:{required:'사업자 등록번호를 입력해 주세요',minlength:'3자리 숫자를 입력해 주세요',maxlength:'3자리 숫자를 입력해 주세요',number:'3자리 숫자를 입력해 주세요'},
                    business_number_02:{required:'사업자 등록번호를 입력해 주세요',minlength:'2자리 숫자를 입력해 주세요',maxlength:'2자리 숫자를 입력해 주세요',number:'2자리 숫자를 입력해 주세요'},
                    business_number_03:{required:'사업자 등록번호를 입력해 주세요',minlength:'5자리 숫자를 입력해 주세요',maxlength:'5자리 숫자를 입력해 주세요',number:'5자리 숫자를 입력해 주세요'},
                    zipcode1:{required:'우편번호를 앞자리를 입력해 주세요',number:'숫자만 입력 가능합니다.'},
                    zipcode2:{required:'우편번호를 뒷자리를 입력해 주세요',number:'숫자만 입력 가능합니다.'},
                    address1:{required:'주소를 입력해 주세요'},
                    address2:{required:'상세주소를 입력해 주세요'},
                    phone_02:{required:'전화번호를 입력해 주세요',number:'숫자만 가능합니다',rangelength:'3 에서 4 자리 숫자만 입력해 주세요'},
                    phone_03:{required:'전화번호를 입력해 주세요',number:'숫자만 가능합니다',minlength:'4자리 숫자만 입력해 주세요',maxlength:'4자리 숫자만 입력해 주세요'},
                    mobile_02:{required:'전화번호를 입력해 주세요',number:'숫자만 가능합니다',rangelength:'3 에서 4 자리 숫자만 입력해 주세요'},
                    mobile_03:{required:'전화번호를 입력해 주세요',number:'숫자만 가능합니다',minlength:'4자리 숫자만 입력해 주세요',maxlength:'4자리 숫자만 입력해 주세요'},                
                    captcha_code:{required:'자동가입 방지코드를 입력해 주세요.'},
                    agree_service:{required:'서비스 이용 약관에 동의가 필요합니다.'},
                    agree_privacy:{required:'개인정보 취급 방침 동의가 필요합니다.'}
                }
              });//end validate
              $(form).data('validator',validator);
        },
        submit:function submit(form){
            form.action = "/cloud/join/start/";
            form.submit();
            return false;
        }
    },
    findpassword:{
        init:function init(form){
            formcheck_cloud.findpassword.valid(form);
        },
        valid:function valid(form){
            var validator = $(form).validate({
                ignore:'.ignore',
                debug:false,
                errorClass:'errMsg',
                submitHandler:function(form){
                    if(form.keychk.value!=1){
                        alert('인증번호를 받은 후 실행해 주세요');
                        return false;
                    }else{
                        return formcheck_cloud.findpassword.submit(form);
                    }
                },
                rules:{
                    name:{
                        required:true
                    }
                    ,userid:{
                        required:true
                        ,email:true
                    }
                    ,phone_no:{
                        required:true
                        ,digits:true
                        ,minlength:10
                        ,maxlength:11
                    }
                    ,auth_no:{
                        required:true
                        ,digits:true
                    }
                },
                messages:{
                    name:{required:'이름을 입력해 주세요.'}
                    ,userid:{required:'이메일을 입력해 주세요.'}
                    ,phone_no:{required:'휴대전화 번호를 입력해 주세요.'}
                    ,auth_no:{required:'인증번호를 입력해 주세요.'}
                }
                ,errorPlacement:function(error, element){
                    var target = $(element).parent('.input_box');
                    target.after(error);
                }
            });//end validate
            $(form).data('validator',validator);
        },
        submit:function submit(form){
            var path = location.pathname;
            var patternFindPasswordMy = /\/mypage\/auth\/findidpw((\/|\.html)?)$/;
            var resultArray = path.split('/');
            var url = '/cloud/' + resultArray[2] + '/account/resetpassword/';
            if(patternFindPasswordMy.test(path)){
                url = '/cloud/ixcloudServer/account/resetpassword/';
            }
            //form.action = url;
            $(form).ajaxSubmit({
                url:url,
                type:'POST',
                dataType:'JSON',
                success:function(data){
                    if(data.success){
                        alert(data.result.message);
                        if(patternFindPasswordMy.test(path)==false){
                            document.location.href = '/cloud/' + resultArray[2] + '/auth';
                        }
                        
                    }else{
                        alert(data.error.message);
                    };
                }
            });
            return false;
        }
    },
    auth:{
        init:function init(form){
            formcheck_cloud.auth.valid(form);
        },
        valid:function valid(form){
            var validator = $(form).validate({
                ignore:'.ignore',
                debug:false,
                errorClass:'errMsg',
                submitHandler:function(form){
                    return formcheck_cloud.auth.submit(form);
                },
                rules:{
                    userid:{required:true,email:true},
                    password:{required:true,minlength:6,maxlength:12},
                }
                ,errorPlacement:function(error, element){
                    //var n = element.attr("name");
                    var target = $(element).parent('.input_box');
                    target.after(error);
                }
                ,messages:{
                    userid:{required:'이메일을 입력해 주세요.',email:'이메일 형식이 일치하지 않습니다.'},
                    password:{required:'패스워드를 입력해 주세요.'},
                }
              });//end validate
              $(form).data('validator',validator);
        },
        submit:function submit(form){
            return cloud_join.auth.login(form);
        }
    },
    editAccount:{
        init:function init(form){
            formcheck_cloud.editAccount.valid(form);
        },
        valid:function valid(form){
            var validator = $(form).validate({
                ignore:'.ignore',
                debug:false,
                errorClass:'errMsg',
                submitHandler:function(form){
                    return formcheck_cloud.editAccount.submit(form);
                },
                rules:{
                    org_password:{required:true},
                    new_password:{required:true,minlength:6,maxlength:12},
                    chk_password:{equalTo:'input[name="new_password"]'}
                },
                messages:{
                    org_password:{required:'현재 비밀번호를 입력해 주세요.'},
                    new_password:{required:'변경 비밀번호를 입력해 주세요.',minlength:'6에서 12자 사이로 입력해 주세요.', maxlength:'6에서 12자 사이로 입력해 주세요.'},
                    chk_password:{equalTo:'입력값이 일치하지 않습니다.'}
                }
                ,errorPlacement:function(error, element){
                    var target = $(element).parent('.input_box');
                    target.after(error);
                }
            });//end validate
            $(form).data('validator',validator);
        },
        submit:function submit(form){
            $(form).ajaxSubmit({
               type:'POST',
               dataType:'json',
               success:function(data){
                   if(data.status=='200'){
                       alert(data.result.message);
                   }else{
                       alert(data.error.message);
                   }
               }
            });
            return false;
        }
    },
}