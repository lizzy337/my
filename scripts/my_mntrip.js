
//등록자 선택시 등록자 정보 설정
function getRegistantUserInfo(nCompanyContactSeq) 
{
	//console.log(nCompanyContactSeq);
	if(nCompanyContactSeq == '-1') {
		$("#sCompanyName").text('');
		$("#sManagerName").text("");
		$("#sRank").text("");
		$("#sInternalPhone").text("");
		$("#sfax").text("");
		$("#sMobilePhone").text("");
		$("#sEmail").text("");
	} else {
		$.post("/service/json/registantUser.html", {"nCompanyContactSeq":nCompanyContactSeq}, function(data) {			
			data = JSON.parse(data);
			$("#sCompanyName").text(data.sCompanyName);
			$("#sManagerName").text(data.sManagerName);
			$("#sRank").text(data.sRank);
			$("#sInternalPhone").text(data.sInternalPhone);
			$("#sfax").text(data.sfax);
			$("#sMobilePhone").text(data.sMobilePhone);
			$("#sEmail").text(data.sEmail);
		});    
	}
}

//보유 ip리스트의 등록버튼.삭제요청
function delRegistant(nCompanyMonitoringIPSeq)//json 타지 않고 바로 DNS 신청리스트에 추가하기 
{
	var objForm= $("form[name='MntrIpRequestForm']");
	
	var sIp = $(objForm).find('#mntripList > #'+nCompanyMonitoringIPSeq).find('.ip').text();
	var sCmt = $(objForm).find('#mntripList > #'+nCompanyMonitoringIPSeq).find('.comment').text();
	
	console.log(sIp);
	console.log(sCmt);
	
	//내용추가
	$(objForm).find('#requestList').find('.request_list').removeAttr('style');
	var html = "<tr class=\"request_list\">";	
	html += "<td>"+"<input type=\"hidden\" name=\"req_ip[]\" value=\""+sIp+"\"/>"+sIp+"<input type=\"hidden\" name=\"nRequestMntrIpIdx[]\" value=\""+nCompanyMonitoringIPSeq+" \"/></td>";
	html += "<td>"+"<input type=\"hidden\" name=\"req_cmt[]\" value=\""+sCmt+"\"/>"+sCmt+"</td>";
	html += "<td><input type=\"hidden\" name=\"worktype[]\" value=\"O\"/>"+"삭제"+"</td>";	
	html += "</tr>";
	console.log(html);
	$(objForm).find('#requestList').append(html);
	
	//IP 신청 리스트의 안내메시지 삭제
	$(objForm).find('#requestList').find('#no_request_list').css('display','none');
	
	//등록버튼 누른 항목을 제거
	$(objForm).find('#mntripList').find('#'+nCompanyMonitoringIPSeq).remove();
	
	//IP 신청리스트에 추가되었다는 flag를 셋팅
	$(objForm).find('#strMntrIPChoice').val('Y');	
}


//신규 IP 등록의 등록버튼
function addMntrIPInfo()
{
	//form check
	var objForm = $("form[name='MntrIpRequestForm']");
	var sIp = $(objForm).find('#add_MntrIpinfo').find('#add_ip').val();
	var sCmt = $(objForm).find('#add_MntrIpinfo').find('#add_cmt').val();
	if( !sIp || !sCmt)
	{
		alert('정보를 모두 입력하세요');
		return false;
	}
	
	$(objForm).find('#requestList').find('.request_list').removeAttr('style');
	var html = "<tr class=\"request_list\">";	
	html += "<td>"+"<input type=\"hidden\" name=\"req_ip[]\" value=\""+sIp+"\"/>"+sIp+"<input type=\"hidden\" name=\"nRequestMntrIpIdx[]\" value=\"\"/></td>";
	html += "<td>"+"<input type=\"hidden\" name=\"req_cmt[]\" value=\""+sCmt+"\"/>"+sCmt+"</td>";
	html += "<td><input type=\"hidden\" name=\"worktype[]\" value=\"I\"/>"+"등록"+"</td>";
	html += "</tr>";
	console.log(html);
	$(objForm).find('#requestList').append(html);
	
	//DNS 신청 리스트의 안내메시지 삭제
	$(objForm).find('#requestList').find('#no_request_list').css('display','none');
	
	//등록버튼 누른 항목의 값을 초기화.
	$(objForm).find('#add_MntrIpinfo').find('#add_ip').val('');
	$(objForm).find('#add_MntrIpinfo').find('#add_cmt').val('');
	
	//IP 신청리스트에 추가되었다는 flag를 셋팅
	$(objForm).find('#strMntrIPChoice').val('Y');
}

$(document).ready(function(){
	$('#MntrIpRequestForm').is(function(){
		$('#MntrIpRequestForm').submit(function(){
			if($(this).find("select[name='companycontact']").val() == -1 ){
				alert('등록자를 입력하세요');
				return false;
			}
			if($(this).find("#strDNSChoice").val() == 'N'){
				alert('IP 정보를 입력하세요.');
				return false;
			}
			console.log('모니터링ip 등록');
			document.MntrIpRequestForm.action = "/Service/mntrIpRequest";
			document.MntrIpRequestForm.submit();
		});
	});
});

