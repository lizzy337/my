$(document).ready(function(){
	if($mode == "modify") {
		getRegistantInfo($basic.nManagerSeq);//210611 원복
		//getRegistantInfobyInit($basic.nManagerSeq);//150313
	}

	$(document).on("click",".btn_input_O", function(e){
		outEquipment(this);
	});

	$(document).on("click",".btn_cancel", function(e){
		deleteEIO(this);
	});

	$("#workRequestForm").is(function(){
		$("#workRequestForm").submit(function() {
			if(typeof $("#equipmentIOList > tbody > tr > td.nodata").html() !== "undefined") {
				alert("입반출 장비 리스를 입력(or 선택)해 주세요.");
				return false;
			}
            if($(this).find("select[name=nManagerSeq]").val() == "") {
				alert("등록자를 선택해주세요.");
				return false;
			}
			if($(this).find("input[name=dtExpectStartDate]").val() == "") {
				alert("방문예정 일자를 입력해주세요.");
				return false;
			}
			var visitedTime = $(this).find("input[name=dtExpectStartTime]").val()
			if(visitedTime == "") {
				alert("방문예정 일시를 입력해주세요.");
				return false;
			}
			if(isNan(parseInt(visitedTime))) {
				alert("방문예정 일시는 숫자만 입력해주세요.");
				return false;
			}
			if(visitedTime > 24 && visitedTime < 0) {
				alert("방문예정 일시는 0에서 24사이의 값으로 입력해주세요.");
				return false;
			}
		});
    });

	try {
		$(".dateYMD").datepicker({
			"dateFormat" : "yy-mm-dd",
			"selectOtherMonths" : true
		});
	} catch (e) {
		console.error(e);
	}
});
function getRegistantInfo(value) {
	if (value == '') {
		$("#registant_companyName").text("");
		$("#registant_managerName").text("");
		$("#registant_internalPhone").text("");
		$("#registant_mobilePhone").text("");
	} else {
		//console.log(value);
		$.post("/kinxidc/json/registant.html", {"nCompanyContactSeq": value}, function(data) {
			data = JSON.parse(data);
			//console.log(data);
			$("#registant_companyName").text(data.sCompanyName);
			$("#registant_managerName").text(data.sManagerName);
			$("#registant_internalPhone").text(data.sInternalPhone);
			$("#registant_mobilePhone").text(data.sMobilePhone);
		});
	}
}

function getRegistantInfobyInit(value) {
	if (value == '') {
		$("#registant_companyName").text("");
		$("#registant_managerName").text("");
		$("#registant_internalPhone").text("");
		$("#registant_mobilePhone").text("");
	} else {
		//console.log('equipment getRegbyInit');
		//console.log(value);
		$.post("/kinxidc/jsonbyInit/registant.html", {"nCompanyContactSeq": value}, function(data) {
			data = JSON.parse(data);
			//console.log(data);
			$("#registant_companyName").text(data.sCompanyName);
			$("#registant_managerName").text(data.sManagerName);
			$("#registant_internalPhone").text(data.sInternalPhone);
			$("#registant_mobilePhone").text(data.sMobilePhone);
		});
	}
}
function inEquipment() {
	var t_brand = $("input[name='i_brand']").val();
	var t_model = $("input[name='i_model']").val();
	var t_name = $("input[name='i_name']").val();
	var t_quantity = $("input[name='i_quantity']").val();
	var t_rack = $("input[name='i_rack']").val();
	var t_use_ip = $("input[name='i_use_ip']").val();
	var t_etc = $("input[name='i_etc']").val();

	if(t_name == "") { alert("반입할 장비명(용도)을 입력하세요."); return ;}
	if(t_quantity == "") { alert("반입할 수량을 입력하세요."); return ;}

	if(typeof $("#equipmentIOList > tbody > tr > td.nodata").html() !== "undefined") $("#equipmentIOList > tbody").html('');

	$("input[name='i_brand']").val("");
	$("input[name='i_model']").val("");
	$("input[name='i_name']").val("");
	$("input[name='i_quantity']").val("");
	$("input[name='i_rack']").val("");
	$("input[name='i_use_ip']").val("");
	$("input[name='i_etc']").val("");

	var html = "";
	html += "<tr>";
	html += "	<td><b style=\"color:#FF0080;\">반입</b><input type=\"hidden\" name=\"sWorkType[]\" value=\"I\"/><input type=\"hidden\" name=\"nCompanyEquipmentSeq[]\" value=\"\"/></td>";
	html += "	<td>"+t_brand+"<input type=\"hidden\" name=\"sBrandName[]\" value=\""+t_brand+"\"/></td>";
	html += "	<td>"+t_model+"<input type=\"hidden\" name=\"sModelName[]\" value=\""+t_model+"\"/></td>";
	html += "	<td>"+t_name+"<input type=\"hidden\" name=\"sEquipmentName[]\" value=\""+t_name+"\"/></td>";
	html += "	<td>"+t_quantity+"<input type=\"hidden\" name=\"nQuantity[]\" value=\""+t_quantity+"\"/></td>";
	html += "	<td>"+t_rack+"<input type=\"hidden\" name=\"sRack[]\" value=\""+t_rack+"\"/></td>";
	html += "	<td>"+t_use_ip+"<input type=\"hidden\" name=\"sUseIp[]\" value=\""+t_use_ip+"\"/></td>";
	html += "	<td>"+t_etc+"<input type=\"hidden\" name=\"sContent2[]\" value=\""+t_etc+"\"/></td>";
	html += "	<td><span class=\"btn_pack small\"><a class=\"btn_cancel\" href=\"#\">취소</a></span></td>";
	html += "</tr>";
	$("#equipmentIOList > tbody").prepend(html);
}
function outEquipment(obj) {
	if(typeof $("#equipmentIOList > tbody > tr > td.nodata").html() !== "undefined") $("#equipmentIOList > tbody").html('');
	for(; obj.nodeName.toUpperCase() !== "TR"; obj = obj.parentNode);
	//json으로 가져와서..
	//var value = $(obj).attr("kinxrow");
    var value = $(obj).attr("kinxequimentrow");
	var t_brand = $(obj).children(".brand").html();
	var t_model = $(obj).children(".model").html();
	var t_name = $(obj).children(".equipment").html();
	var t_quantity = $(obj).find(".quantity select").val(); //org
	if( t_quantity != undefined)
	{
		var max_quantity = $(obj).find(".quantity select option:last").val(); //org
	}
	else
	{
		var t_quantity = $(obj).children(".quantity").html();
	}
	var t_rack = $(obj).children(".rack").html();
	var t_use_ip = $(obj).children(".ip").html();
	var t_etc = $(obj).children(".etc").html();

	var html = "";
	html += "<tr>";
	html += "	<td><b style=\"color:#008000;\">반출</b><input type=\"hidden\" name=\"sWorkType[]\" value=\"O\"/><input type=\"hidden\" name=\"nCompanyEquipmentSeq[]\" value=\""+value+"\"/></td>";
	html += "	<td>"+t_brand+"<input type=\"hidden\" name=\"sBrandName[]\" value=\""+t_brand+"\"/></td>";
	html += "	<td>"+t_model+"<input type=\"hidden\" name=\"sModelName[]\" value=\""+t_model+"\"/></td>";
	html += "	<td>"+t_name+"<input type=\"hidden\" name=\"sEquipmentName[]\" value=\""+t_name+"\"/></td>";
	html += "	<td>"+t_quantity+"<input type=\"hidden\" name=\"nQuantity[]\" value=\""+t_quantity+"\"/><input type=\"hidden\" name=\"maxQuantity[]\" value=\""+max_quantity+"\"/></td>";
	html += "	<td>"+t_rack+"<input type=\"hidden\" name=\"sRack[]\" value=\""+t_rack+"\"/></td>";
	html += "	<td>"+t_use_ip+"<input type=\"hidden\" name=\"sUseIp[]\" value=\""+t_use_ip+"\"/></td>";
	html += "	<td>"+t_etc+"<input type=\"hidden\" name=\"sContent2[]\" value=\""+t_etc+"\"/></td>";
	html += "	<td><span class=\"btn_pack small\"><a class=\"btn_cancel\" href=\"#\" >취소</a></span></td>";
	html += "</tr>";
	//console.log(html);
	$("#equipmentIOList > tbody").prepend(html);

	var parent = obj.parentNode;
	parent.removeChild(obj);
	if($(parent).children().length == 0) $(parent).prepend("<tr><td colspan=\"8\" class=\"nodata\">해당 내용이 없습니다.</td></tr>");
}

function deleteEIO(obj, type) {
	for(; obj.nodeName.toUpperCase() !== "TR"; obj = obj.parentNode);
	var parent = obj.parentNode;

	if($(obj).find("input[name='sWorkType[]']").val() == 'O') {
		if(typeof $("#equipmentOwnList > tbody > tr > td.nodata").html() !== "undefined") $("#equipmentOwnList > tbody").html('');

		var value = $(obj).find("input[name='nCompanyEquipmentSeq[]']");
		var t_brand = $(obj).find("input[name='sBrandName[]']").val();
		var t_model = $(obj).find("input[name='sModelName[]']").val();
		var t_name = $(obj).find("input[name='sEquipmentName[]']").val();
		//var t_quantity = $(obj).find("input[name='nQuantity[]']").val();
		var max_quantity = $(obj).find("input[name='maxQuantity[]']").val();
		var t_Rack = $(obj).find("input[name='sRack[]']").val();
		var t_use_ip = $(obj).find("input[name='sUseIp[]']").val();
		var t_etc = $(obj).find("input[name='sContent2[]']").val();

		var html = "";
		html += "<tr kinxrow=\"value\">";
		html += "	<td class=\"left brand\">"+t_brand+"</td>";
		html += "	<td class=\"left model\">"+t_model+"</td>";
		html += "	<td class=\"left equipment\">"+t_name+"</td>";

		html += "	<td class=\"quantity\"><span class=\"select\" style=\"width:50px\"><select id=\"#\" title=\"수량\">";
		for(var i=1; i<=max_quantity; i++) html += "<option value=\""+i+"\">"+i+"</option>";
		html += "</select></span></td>";

		html += "	<td class=\"rack\"></td>";
		html += "	<td class=\"ip\">"+t_use_ip+"</td>";
		html += "	<td class=\"etc\">"+t_etc+"</td>";
		html += "	<td><span class=\"btn_pack small\"><a class=\"btn_input_O\" href=\"#\">등록</a></span></td>";
		html += "</tr>";
		$("#equipmentOwnList").prepend(html);
	}

	parent.removeChild(obj);
	if($(parent).children().length == 0) $(parent).prepend("<tr><td colspan=\"9\" class=\"nodata\">입반출 장비를 입력(or 선택)해 주세요.</td></tr>");
}
